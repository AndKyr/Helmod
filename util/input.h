/*
 * input.h
 *
 *  Created on: 3.7.2012
 *      Author: dlandau
 */

#ifndef INPUT_H_
#define INPUT_H_

#include <stdio.h>
#include <stdint.h>

#include "types.h"

int32_t get_int32(uint8_t** ptr, enum byte_order file_order);
int64_t get_int64(uint8_t** ptr, enum byte_order file_order);
float get_real32(uint8_t** ptr, enum byte_order file_order);
double get_real64(uint8_t** ptr, enum byte_order file_order);


void my_read(void* buf, size_t size, FILE* fp);
enum byte_order get_endianness_from_prot_real(uint8_t * ptr);

void read_procs_info(const struct fixed_header* fhdr,
        const struct variable_header* vhdr, FILE* fp);
int read_types_info(const struct fixed_header* fhdr,
        const struct variable_header* vhdr, FILE* fp);
int read_fields_info(const struct fixed_header* fhdr,
        const struct variable_header* vhdr, FILE* fp);

void read_fixed_header(FILE* fp, struct fixed_header* fhdr,
        char* progname, char* fname);


void parse_atom(const struct configuration* conf,
        const struct fixed_header* fhdr, struct atom* atom,
        uint8_t** ptr, int iteration);

FILE* try_fopen(char * fname, const char * mode);
void try_fread(uint8_t* buffer, int file_atomsize, int count, FILE* fp,
        struct configuration* conf);
void try_fseek(off_t file_offset, FILE* fp,
        struct configuration* conf);

void calculate_offsets(const struct variable_header* vhdr,
        const struct fixed_header* fhdr);


#endif /* INPUT_H_ */
