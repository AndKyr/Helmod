/*
 * output.h
 *
 *  Created on: 3.7.2012
 *      Author: dlandau
 */

#ifndef OUTPUT_H_
#define OUTPUT_H_

#include "types.h"

FILE * open_output_file_with_correct_name(const struct configuration * conf);

void print_headers(const struct fixed_header * fhdr_in,
        const struct variable_header * vhdr_in,
        const struct configuration * conf,
        FILE * out_fp);

void print_atom(const struct configuration* conf,
        const struct variable_header* vhdr, const struct fixed_header* fhdr,
        const struct atom* atom,
        FILE * out_fp);

#endif /* OUTPUT_H_ */
