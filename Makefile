
all: release

install: libhelmod/Makefile libhelmod/femocs/Makefile
	@cd libhelmod/femocs && make install-ubuntu

libhelmod/Makefile:
	@git submodule init && git submodule update

libhelmod/femocs/Makefile:
	@cd libhelmod && git submodule init && git submodule update

release: tipmaker/maketip libhelmod/femocs/lib/libfemocs.a libhelmod/libhelmod.a src/*.f90
	@make -f make.parcas --no-print-directory

tipmaker/maketip: tipmaker/maketip.f90
	@cd tipmaker && make --no-print-directory

libhelmod/libhelmod.a: libhelmod/femocs/lib/libfemocs.a libhelmod/src/*.f90
	@cd libhelmod && make all --no-print-directory

libhelmod/femocs/lib/libfemocs.a: libhelmod/femocs/src/*.cpp libhelmod/femocs/include/*.h
	@cd libhelmod/femocs && make lib --no-print-directory

clean:
	@make -s -f make.parcas clean
	@cd tipmaker && make clean --no-print-directory
	@cd libhelmod && make clean --no-print-directory
	@cd libhelmod/femocs && make clean --no-print-directory

help:
	@echo ''
	@echo 'make install   initialize and install dependencies'
	@echo 'make           compile & link Helmod, Femocs and Parcas'
