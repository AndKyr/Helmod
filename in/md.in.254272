Input file for PARCAS MD - Cu near melting point

Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nprocsreq= 1             Number of processors requested from computer - IGNORED

nsteps   = 99999999      Max number of time steps
tmax     = 2000.0        Maximum time in fs 

seed     = 723762        Seed for random number generator

Time step criteria and related parameters. Internal time unit
is 10.1805*sqrt(mass) = 81.15 fs for Cu
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
mass     = 63.5460       Atom mass in u (needed in time step calculation)

Simulation cell
---------------

latflag  = 1             Lattice flag (0=gen fcc, 1=read from mdlat.in)
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.30         Neighbour list skin thickness ratio, use ~ 1.15 

natoms   = 254272        Number of atoms in simulation
box(1)   = 241.0         Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 120.0         Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 120.0         Box size in the Z-dir (Angstroms, sigma for LJ)
ncell(1) = 64            Number of unit cells along X-direction
ncell(2) = 32            Number of unit cells along Y-direction
ncell(3) = 32            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 1             Temp. control (0 none,1=linear,4=set,5=borders)
temp0    = 3000.0        Initial T for mtemp=6
ntimeini = 100           Time steps at temp0 before quench
temp     = 1220.0        Desired temperature (Kelvin)
toll     = 10.00000      Tolerance for the temperature control
btctau   = 0.0           Berendsen temperature control tau (fs)
trate    = 5.0           Quench rate for mtemp=6 (K/fs)

bpcbeta  = 7.2569d-4     Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 100.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)

tscaleth = 3.61          Min. thickness of border region at which T is scaled 
Ebscale  = 0.0           Energy by which border thickness determined

damp     = 0.00000       Damping factor
amp      = 0.01000       Amplitude of initial displacement (Angstroms)

ndump    = 1             Print data every ndump steps
nmovie   = 200           Number of steps btwn writing to md.movie (0=no movie)
ndefmovie= 1000          Number of steps btwn writing to defects.out 
nisolmov = 1000          Number of steps btwn writing to isolateddefects.out
nrestart = 500

nintanal = 1000            Number of steps between interstitial analysis

Epdef    = -3.0         Epot threshold for labeling an atom a defect
Ekdef    = 5.0         Ekin threshold for labeling an atom a defect


Parameters for parallell operation
----------------------------------

mx Xnodes= 0             Max number of nodes in the X-dir (0 means no max)
mx Ynodes= 0             Max number of nodes in the Y-dir (0 means no max)


Recoil calculation definitions
------------------------------

irec     = 0          Index of recoiling atom
xrec     = -5.0          Desired initial position
yrec     = -5.0    
zrec     = 5.0    

recen    = 2.0        Initial recoil energy in eV
rectheta = 78.0          Initial recoil direction in degrees
recphi   = 35.0          Initial recoil direction in degrees
