Input file for PARCAS 

Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug: 1 true, 0 false

nprocsreq= 4            Number of processors requested from computer

nsteps   = 200           Max number of time steps
tmax     = 50000.0       Maximum time in fs 

seed     = 723762        Seed for random number generator

Time step criteria and related parameters. Internal time unit
is 10.1805*sqrt(mass) = 142.88 fs for Au.

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
timekt   = 0.1           Maximum distance (in �) an atom may move over 1 step
timeEt   = 100.0         Time step selection criterion for collisions
timeCh   = 1.1           Maximum allowable one-step change in time step
mass     = 196.97        Atom mass in u (needed in time step calculation)


natoms   = 4000          Number of atoms in simulation
box(1)   = 40.8          Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 40.8          Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 40.8          Box size in the Z-dir (Angstroms, sigma for LJ)
ncell(1) = 10            Number of unit cells along X-direction
ncell(2) = 10            Number of unit cells along Y-direction
ncell(3) = 10            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

mtemp    = 5             Temp. control (0 none,1=linear,4=set,5=borders)
temp(K)  = 0.0           Desired temperature (Kelvin)
toll(K)  = 10.00000      Tolerance for the temperature control
tscaleth = 6.12          Min. thickness of border region at which T is scaled 
Ebscale  = 0.0           Energy by which border thickness determined

damp     = 0.00000       Damping factor
amp      = 0.00000       Amplitude of initial displacement (Angstroms)

latflag  = 0             Lattice flag (0=gen fcc, 1=read from mdlat.in)
nprtble  = 100000        Number of steps between pair table calculations
neiskinr = 1.20          Neighbour list skin thickness ratio, use ~ 1.15 

ndump    = 1             Print data every ndump steps
nmovie   = 20             Number of steps btwn writing to md.movie (0=no movie)
ndefmovie= 0             Number of steps btwn writing to defects.out 
nisolmov = 0             Number of steps btwn writing to isolateddefects.out

Epdef    = 0.0         Epot threshold for labeling an atom a defect
Ekdef    = 0.0           Ekin threshold for labeling an atom a defect


Parameters for parallell operation
----------------------------------

mx Xnodes= 0             Max number of nodes in the X-dir (0 means no max)
mx Ynodes= 0             Max number of nodes in the Y-dir (0 means no max)


Recoil calculation definitions
------------------------------

irec     = -1           Index of recoiling atom
xrec     = -5.0          Desired initial position
yrec     = -5.0    
zrec     = 5.0    

recen    = 200.0        Initial recoil energy in eV
rectheta = 78.0          Initial recoil direction in degrees
recphi   = 35.0          Initial recoil direction in degrees
