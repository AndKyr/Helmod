#!/bin/bash

PATH=$PWD/..:$PATH
start=$PWD

# To add a test, just add it between the proper parentheses, either parcas_tests or parcas_eamal_tests
# You need to also first produce the "correct" answers. The way to produce them is to simply run parcas
# once on your test input (with 4 processes) and remove the file out/time.
parcas_tests=(./tersoff_compound/gaas/ ./sw/ \
	./tersoff/ ./watanabe_samela/ ./eam/ ./EDIP/)
# missing some input files at the moment: ./silica_ohta/ ./tersoff_compound/wurtzite/ ./sio2/ 
parcas_eamal_tests=(./FeCr_eamal/ ./ameinand/ ./apohjone_eamal)

all_tests=(${parcas_tests[@]} ${parcas_eamal_tests[@]}) 

n_failed=0

for i in $(seq 0 $((${#all_tests[@]} - 1)) )
do
    dir=${all_tests[$i]}
    if [[ $i -lt ${#parcas_tests[@]} ]]
    then
	  prog=parcas
    else
	  prog=parcas_eamal
    fi
    cd $start
    cd $dir
    printf '%-25s: ' $dir
    m4 -DSTEPS=10 in/md.in.in > in/md.in
    mpirun -np 4 $prog >/dev/null
    rm out/time
    if diff -q out out_correct -x .svn &>/dev/null
    then
	printf 'Ok\n'
    else
	printf 'FAIL\n'
	n_failed=$((n_failed+1))
    fi
    rm -rf out
done

if [[ $n_failed -gt 0 ]]
then
	echo """
********************************
* a total of $(printf "%3d" $n_failed) tests failed *
*******************************
"""
fi
