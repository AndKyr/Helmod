Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = STEPS
tmax     = 20000.0        Maximum time in fs 
restartt = 0.0
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

Atom type-dependent parameters. 
Time step is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.01          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 3             Number of atom types
mass(0)  = 39.948     
name(0)  = Ar            
mass(1)  = 28.0855       Atom mass in u
name(1)  = Si            Name of atom type
mass(2)  = 15.9994       Atom mass in u
name(2)  = O             Name of atom type
substrate= Si            Substrate name, for use in elstop file name

iac(0,0) = 2             Interaction types: -1 kill 0 none 1 potmode 2 pair 
iac(0,1) = 2             - automatic symmetry used: iac(1,0) = iac(0,1)
iac(0,2) = 2
iac(1,1) = 1
iac(1,2) = 1
iac(2,2) = 1
potmode  = 200           0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW

rcrit    = 3.772         R cutoff: 3.772 for Si, 3.93 for Ge, N/A for EAM
reppotcut= 10.0

Simulation cell
---------------

latflag  = 1             Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
nprtbl   = 30000         Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

# For 111 DIA a=3.84026157  b=6.65152816    c=9.4066813
# At 77 K   a ~= 3.8422  b ~= 6.6550 c ~= 9.4117

natoms   = 900           Number of atoms in simulation
box(1)   = 25.1328       Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 43.2877       Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 22.0346       Box size in the Z-dir (Angstroms, sigma for LJ)
 ncell(1) = 5             Number of unit cells along X-direction
 ncell(2) = 5             Number of unit cells along Y-direction
 ncell(3) = 4             Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

nvac     = 0             Number of vacancies to create
vaczmin  = 0.0

Simulation
----------

mtemp    = 0             Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 10000.0       Initial T for mtemp=6
timeini  = 1000.0        Time at temp0 before quench
ntimeini = 999900        Time steps at temp0 before quench
initemp  = 0.0          Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 0.0           Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 100.0          Berendsen temp. control tau (fs), if 0 not used
trate    = 0.12          Quench rate for mtemp=6/8 (K/fs)

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 625.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 10.0          Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcP0z   = 0.0           Berendsen pres. control. desired P_z (kbar)
bpcmode  = 0             Pressure control mode: 0 none, 1 xyz, 2 isotropic
30

 tscaleth = 6.65         Min. thickness of border region at which T is scaled 
 Ebscale  = 0.0           Energy by which border thickness determined

 fixzmin  = -18.0         Atom fixing at bottom of cell. For EAM pots. need to
 fixzmax  = -14.0         fiox at least three layers
 tscalzmin= -15.6862     Bottom T scaling region for mtemp=5/7/8
 tscalzmax= -9.4117     

damp     = 0.00000       Damping factor

ndump    = 10             Print data every ndump steps

moviemode= 2
nmovie   = 500           Number of steps btwn writing to md.movie (0=no movie)
dtmov(1) = 100.0         Time interval for movie output before tmov(1) (fs)
tmov(1)  = 50000.0       Time at which to change output interval (fs)
dtmov(2) = 20000.0       Time interval for movie output before tmov(1) (fs)
tmov(2)  = 20000.0       Time at which to change output interval (fs)
dtmov(3) = 20000.0       Time interval for movie output for rest of run (fs)
ndefmovie= 200           Number of steps btwn writing to defects.out 
nisolmov = 1000          Number of steps btwn writing to isolateddefects.out
nrestart = 1000

 nintanal = 1000          Number of steps between interstitial analysis
 nliqanal = 100           Nsteps between liquid analysis; works in parallell !

Epdef    = -3.0          Epot threshold for labeling an atom a defect
Ekdef    = 0.22          Ekin threshold for labeling an atom defect/liquid 


Parameters for parallell operation
----------------------------------

mx Xnodes= 0             Max number of nodes in the X-dir (0 means no max)
mx Ynodes= 0             Max number of nodes in the Y-dir (0 means no max)


Recoil calculation definitions
------------------------------

  taddvel  = 10.0         Time at which to add atom energies, in fs
  zaddvel  = 0.38           Add energy for atoms above this in z in A
  eaddvel  = 100.0 0.005          Energy per atom to add, in eV.

  irec     = -2             Index of recoiling atom (-1 closest, -2 create)
  recatype = 0              Recoil atom type
  xrec     = 0.0          Desired initial position
  yrec     = 0.0    
  zrec     = 40.0

  recen    = 100.0       Initial recoil energy in eV
  rectheta = 180.0           Initial recoil direction in degrees
  recphi   = 0.0          Initial recoil direction in degrees

melstop  = 0             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek>10
