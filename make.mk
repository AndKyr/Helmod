
# include libraries and their paths for Femocs
include libhelmod/femocs/share/makefile.femocs

#
#  Makefile parameters for PARCAS
#
#  When changing systems, you should at least:
#  1) select the right compilation commands
#  2) Select the right options in defs.f90
#
#  - Sometimes it may also be necessary to modify Makefile
# 
#
# Compilation for Linux using gfortran and MPI
# e.g. alcyone, Ubuntu workstation (ask your admin to do "apt-get install openmpi-bin libopenmpi-dev")
#

# generate proper flags for Femocs libraries by adding src/libhelmod/femocs/ in front of their paths
LDFLAGS= $(patsubst -L%, -Llibhelmod/femocs/%, $(FEMOCS_LIBPATH)) $(FEMOCS_LIB) 
    
    
F90=mpif90 -O3 -fno-automatic -Isrc -Ilibhelmod/mod -Ilibhelmod/femocs/lib -mcmodel=large
LINK=mpif90 -fno-automatic -mcmodel=large
#F90=mpif90 -g -fbounds-check -fno-automatic -Isrc
#LINK=mpif90 -g -fbounds-check -fno-automatic

#
# Compilation for korundi.grid.helsinki.fi usint Intel compilers and MPI
# Remember: On korundi, you should have the following Intel setup command in your '.bashrc':
#    source /etc/intelvars.sh
#
#LDFLAGS= 
#F90=mpiifort -fc=ifort -O2 -Isrc
#LINK=mpiifort 

#  Parallel on Cray XT @ csc.fi (louhi) Vesa Kolhinen 2008-09-22
#
#  PGI compiler
#F90=ftn -module $(OBJ) -g -O3  -Munroll=c:1 -Mnoframe -Mlre -Mautoinline
#LINK=$(F90)
#LDFLAGS=
#  
#  Cray compiler
#F90=ftn -em -p $(OBJ) -J $(OBJ)
#LINK=$(F90)
#LDFLAGS=

###########################################################################
# The following versions will not work unmodified, since they are         #
# for serial compilation. They are still here because of the compiler     #
# flags, so they can be adjusted for parallel compilation in case needed. #
###########################################################################

#
# Compilation using Pathscale EKO Compiler Suite and MpiCH
#
# Note: -O3 may cause problems with some force subroutines (KN 13.3.2007)
#
#LDFLAGS=
#F90=mpif90 -O2 -ipa -fno-math-errno -m64 -Isrc
#LINK=mpif90 -static -O2 -ipa -fno-math-errno -m64

#
# Compilation using PGF Fortran (Opteron, e.g. ametisti)
#
#CFLAGS= -O3 -Bstatic -fastsse -Mipa=fast
#LDFLAGS= 
#F90=pgf90 $(CFLAGS) -Isrc
#LINK=pgf90 $(CFLAGS)

#
# Compilation for Linux using Intel Fortran (ifort)
#
#LDFLAGS= 
#F90=ifort -O3 -w 
#LINK=ifort -static-libcxa

#
# Compilation using Pathscale EKO Compiler Suite, e.g. on ametisti
# Note: -O3 may cause problems with some force subroutines (KN 13.3.2007)
#
#LDFLAGS= 
#F90=pathf90 -O2 -ipa  -fno-math-errno -m64 -march=opteron -Isrc
#LINK=pathf90 -static -O2 -ipa  -fno-math-errno -m64 -march=opteron

#
# Compilation using openf90
# Note: -O3 may cause problems with some force subroutines (KN 13.3.2007)
#
#LDFLAGS= 
#F90=openf90 -O3 -ipa  -fno-math-errno -m64 -march=xeon -Isrc
#LINK=openf90 -static -O3 -ipa  -fno-math-errno -m64 -march=xeon
