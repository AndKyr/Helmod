!
!  Subroutines related to cascade calculations for PARCAS
!
!  Added to code beginning 05/07/96
!

subroutine Init_Units()

  use typeparam
  use my_mpi
  use defs  

  use para_common

  implicit none

!include 'para_common.f90'

  !
  !     Calculate time, vel. etc. unit conversion factors from
  !     standard MD units (with internal masses==1) to
  !     the real unit system defined by eV, � and fs
  !     Note that the PARCAS x0,x1,x2... arrays do not
  !     contain pure MD units !
  !

  integer i

  IF(iprint)WRITE(6,'(A)') '*** Units ***'
  do i=itypelow,itypehigh
     if (mass(i) <= 0.0) then
        write(6,*) 'ERROR Invalid mass found',i
        call my_mpi_abort('Zero mass', INT(i))
     endif
     timeunit(i)=10.1805*SQRT(mass(i))
     vunit(i)=0.0982266/SQRT(mass(i))
     aunit(i)=0.0096485/mass(i) 
     IF(iprint)WRITE(6,'(A,I2,5G12.5)')                                          &
          'type mass tunit vunit aunit',                                &
          i,mass(i),timeunit(i),vunit(i),aunit(i)
  enddo
  IF(iprint)WRITE(6,'(A)') '---       ---'

end subroutine Init_Units

!*********************************************************************
!
! Calculate time step, following kt and Et criteria in
! Nordlund, Comp. Mat. Sci. 3 (1995) 448
!
!  Time step units are sqrt(atom_mass*u*Angstrom**2/e) where u and e
!  are the constant values.
!
!  Returns the internal time step delta(ntype) and deltas(ntype),
!  the real time step deltat_fs and the time step ratio
!
!
subroutine Get_Tstep(irec,vmax,Fmax,timekt,timeEt,timeCh,             &
     deltat_fs,deltamax_fs,delta,deltas,deltaratio,rstmax)

  use typeparam

  use defs  

  use para_common

  implicit none

!include 'para_common.f90'

  integer irec
  real*8 vmax,Fmax,timekt,timeEt,timeCh
  real*8 delta(itypelow:itypehigh),deltas(itypelow:itypehigh)
  real*8 deltaratio
  real*8 rstmax
  real*8 deltat_fs,deltamax_fs

  real*8 deltatv_fs,deltate_fs,deltatinc_fs

  real*8 prevdeltat_fs
  save prevdeltat_fs

  logical, save :: firsttime = .true.
  !      data firsttime /.true./
  !      save firsttime

  integer i

  deltaratio=1.0d0
  if (irec /= 0) then
     !        
     !        First get time step in units of fs. vmax is max velocity in �/fs
     !
     if (vmax <= 0.0) then
        write (6,*) 'Warning: vmax=0.0, wont modify tstep',myproc,deltat_fs
        return
     endif

     deltate_fs=1d30
     deltatv_fs=timekt/vmax
     if (Fmax > 0.0) deltate_fs=timeEt/(vmax*Fmax)

     deltat_fs=deltatv_fs
     deltat_fs = min(deltat_fs, deltate_fs)

     if (.not. firsttime) then
        deltatinc_fs=1d30

        deltatinc_fs=timeCh*prevdeltat_fs
        if (deltatinc_fs < deltat_fs) deltat_fs=deltatinc_fs
     endif
     if (deltat_fs > deltamax_fs) deltat_fs=deltamax_fs
     ! Necessary for recoil sequences, otherwise not needed
     ! Constant 0.01 comes from the fact that it should be larger
     ! than the ratio timeCh, which is 1.1 per default
     if (deltat_fs > rstmax/(timeCh+0.01)) deltat_fs=rstmax/(timeCh+0.01);
  else
     deltat_fs=deltamax_fs
  endif
  !
  !     ... then transform into internal units
  !
  if (.not. firsttime) then
     deltaratio=deltat_fs/prevdeltat_fs
  endif
  prevdeltat_fs=deltat_fs
  do i=itypelow,itypehigh

     delta(i)=deltat_fs/timeunit(i)
     deltas(i)=0.5*delta(i)*delta(i)
     ! print *,'dt',deltat_fs,timeunit(i),delta(i),deltaratio

     if (firsttime) then
        IF(iprint)WRITE (6,'(A,I3,A,2G13.6)')                                    &
             'Atom type',i,                                             &
             ' Initial time step (fs, internal un.)'                    &
             ,deltat_fs,delta(i)
     endif

  enddo


  firsttime=.false.

end subroutine Get_Tstep



!***********************************************************************
!***********************************************************************
!***********************************************************************

subroutine getcascproperties(istep,irec,time_fs,                        &
     Fmax,Fmaxnosput,timesputlim,nisputlim,                             &
     Ekdef,Ekrec,ndefmovie,nborlist,xnp,x0,atomindex,Epair,P,Fp,Ekin,box,pbc, &
     Tsh,wxxsh,wyysh,wzzsh,natsh,nsh,nliqanal,unitcell,                 &
     atpassflag,nngbrproc,natoms,irecproc,ECM,ECMfix,                   &
     dslice,latflag,EAM,AnySemi)

  use typeparam
  use datatypes
  use my_mpi

  use PhysConsts

  use defs  

  use para_common

  implicit none

!include 'para_common.f90'

  !     Variables passed in and out

  integer istep,irec,irecproc
  real*8 time_fs,Fmax,Fmaxnosput,timesputlim,nisputlim,Ekdef,Ekrec
  integer ndefmovie
  logical EAM,AnySemi

  integer*4 nborlist(*)
  integer nliqanal,nliqat,natoms
  !     Somewhat confusingly, Fp holds the EAM potential per atom !
  real*8 xnp(*),x0(*),Epair(*),P(*),Fp(*),Ekin(*),box(*),pbc(*)
  real*8 unitcell(*)

  integer atpassflag(*),nngbrproc(*),atomindex(*)

  integer nsh
  integer natsh(*)
  real*8 Tsh(*),wxxsh(*),wyysh(*),wzzsh(*)

  integer latflag


  !     Local variables

  integer i,i3,j,jj,in
  integer nbr,nnbors,mij,j3

  integer nnsh
  real*8 r,rs,maxr,T,Pxx,Pyy,Pzz,Vol,F,epot
  real*8 dx,dy,dz,boxs(3)

  integer np0,d,nsumnbrs,node
  real*8 Eksum,Ekmax,rsumcrit,rsumcritsq,xp(3)
  real*8 ECM(3),Esum,dslice(3)
  integer nECM,ECMfix
  logical notsputtered

  ! Local variables for parallell interface
  integer dfrom,ifrom,ii,myi

  integer(kind=mpi_parameters) :: ierror  ! MPI error code

  !
  !     Get maximum absolute force, in units of eV/A
  !

  boxs(1)=box(1)*box(1)
  boxs(2)=box(2)*box(2)
  boxs(3)=box(3)*box(3)

  Fmax=-1.0d0
  Fmaxnosput=-1.0d0
  do i=1,myatoms
     i3=i*3-3
     F=SQRT(xnp(i3+1)*xnp(i3+1)*boxs(1)                               &
          +xnp(i3+2)*xnp(i3+2)*boxs(2)                               &
          +xnp(i3+3)*xnp(i3+3)*boxs(3))
     if (F > Fmax) Fmax=F

     if (.not.(x0(i3+1)<-timesputlim .or. x0(i3+1)>timesputlim .or.    &
          x0(i3+2)<-timesputlim .or. x0(i3+2)>timesputlim .or.         &
          x0(i3+3)<-timesputlim .or. x0(i3+3)>timesputlim)) then
        ! Atom inside timesputlim
        notsputtered=.true.
        if (nisputlim < 1.0d30) then
           if ((AnySemi .eqv. .true. ).and. (EAM .eqv. .false.)) then
              epot=P(i)+Epair(i)
           else
              epot=Fp(i)+Epair(i)
           end if
           if (x0(i3+1)<-nisputlim .or. x0(i3+1)>nisputlim .or.        &
                x0(i3+2)<-nisputlim .or. x0(i3+2)>nisputlim .or.       &
                x0(i3+3)<-nisputlim .or. x0(i3+3)>nisputlim) then
              ! Atom outside nisputlim
              ! Count as sputtered if epot==0
              if (epot==zero) then
                 notsputtered=.false.
              endif
           endif
        endif

        if (notsputtered) then
           if (F > Fmaxnosput) Fmaxnosput=F
        endif
     endif


  enddo
  call my_mpi_dmax(Fmax, 1)      
  call my_mpi_dmax(Fmaxnosput, 1)      

  !
  ! Calculate temperature and pressure in regions of cell
  !
  if (ndefmovie > 0) then
     if (MOD(istep,ndefmovie) == 0) then


        IF(debug)PRINT *,'pressure output',myproc
        T=zero
        do j=1,nsh
           Tsh(j)=zero
        enddo

        maxr=SQRT(3.0)*0.5
        do i=1,myatoms
           i3=i*3-3
           dx=x0(i3+1)-ECM(1)/box(1)
           dy=x0(i3+2)-ECM(2)/box(2)
           dz=x0(i3+3)-ECM(3)/box(3)
           r=SQRT(dx*dx+dy*dy+dz*dz)
           nnsh=INT(1.0d0+r/maxr*(nsh-1))
           if (nnsh > nsh) nnsh=nsh
           do j=nsh,nnsh,-1
              Tsh(j)=Tsh(j)+Ekin(i)
           enddo
        enddo
        call my_mpi_dsum(Tsh, nsh)
        !
        !     The pressure terms have been summed over processors in Calc_force
        !
        do j=1,nsh

           !     r=j*0.5*sqrt(box(1)*box(1)+box(2)*box(2)+box(3)*box(3))/nsh
           !     Vol=4.0*pi*r**3/3.0*1.0d-30
           !     if (r>box(1)/2.0.or.r>box(2)/2.0.or.r>box(3)/2.0)

           Vol=natsh(j)*box(1)*box(2)*box(3)/natoms*1.0d-30
           if (natsh(j) > 0) then
              Tsh(j)=Tsh(j)*two/three/natsh(j)*e/kB
           endif
           T=Tsh(j)
           !
           !  Pressure terms. The factor in front is (Pascal -> kBar)
           !  This calculation assumes E_k is isotropic in x, y and z.
           !
           Pxx=0.0
           Pyy=0.0
           Pzz=0.0
           if (Vol > 0.0) then
              Pxx=(1./1.d8)*(natsh(j)*kB*T+wxxsh(j)*boxs(1)*e)/Vol
              Pyy=(1./1.d8)*(natsh(j)*kB*T+wyysh(j)*boxs(2)*e)/Vol
              Pzz=(1./1.d8)*(natsh(j)*kB*T+wzzsh(j)*boxs(3)*e)/Vol
           endif
           IF(iprint)WRITE (17,'(A,F10.1,I3,I8,1X,F9.2,3F8.2)')                 &
                't shell N T Pxx Pyy Pzz',                             &
                time_fs,j,natsh(j),T,Pxx,Pyy,Pzz
        enddo
         
     endif
  endif

  !     Print out recoil position

  if (irec /= 0) then
     IF(debug)PRINT *,'recoilat output',myproc,irecproc
     if (irecproc == myproc) then
        i3=irec*3-3
        xsendbuf(1)=x0(i3+1)
        xsendbuf(2)=x0(i3+2)
        xsendbuf(3)=x0(i3+3)
        xsendbuf(4)=Ekin(irec)
        xsendbuf(5)=1.0d0*irec
        xsendbuf(6)=1.0d0*myproc
        ! Send recoil atom to proc. 0 for printing
        !if (myproc/=0) CALL mpi_send(xsendbuf, 6, mpi_double_precision, 0, 873, mpi_comm_world, ierror)
     endif
     call mpi_bcast(xsendbuf, 6, mpi_double_precision, irecproc, &
          mpi_comm_world, ierror)
     if (myproc == 0) then
        !if (irecproc/=0) CALL mpi_recv(xsendbuf, 6, mpi_double_precision, mpi_any_source, 873, mpi_comm_world, mpi_status_ignore, ierror)
        in=nliqanal; 
        if (in==0) in=1000000
        if ((xsendbuf(4)>Ekrec .or. MOD(istep,in)==0) .and. nliqanal>0) then
           write (22,110) xsendbuf(1)*box(1),                           &
                xsendbuf(2)*box(2),xsendbuf(3)*box(3),                  &
                time_fs,INT(xsendbuf(5)+0.5),xsendbuf(4),               &
                INT(xsendbuf(6)+0.5)
110        format (3F10.4,F10.1,I8,F15.2,I3)
            
        endif
        if (nliqanal > 0) then
           i=nliqanal; if (i==0) i=100000
           if (i > 10) i=i/5
           if (MOD(istep,i) == 0) then
              write (6,111) 'rec ',xsendbuf(1)*box(1),                  &
                   xsendbuf(2)*box(2),xsendbuf(3)*box(3),               &
                   time_fs,INT(xsendbuf(5)+0.5),xsendbuf(4),            &
                   INT(xsendbuf(6)+0.5)
111           format (A,3F10.4,F10.1,I8,F15.2,I3)
           endif
        endif
     endif
  endif

  !
  !     Get ECM position of cascade
  !
  if (irec /= 0 .and. ECMfix == 0 .and. (nliqanal > 0 .or.              &
       (dslice(1)>=0.0.or.dslice(2)>=0.0.or.dslice(3)>=0.0))) then
     IF(debug)PRINT *,'ECM output',myproc
     i=nliqanal; if (i==0) i=100000
     if (i > 10) i=i/5
     if (MOD(istep,i) == 0) then
        ECM(1)=0.0; ECM(2)=0.0; ECM(3)=0.0;
        nECM=0; Esum=0.0;
        do i=1,myatoms
           i3=i*3-3
           Esum=Esum+Ekin(i)
           ECM(1)=ECM(1)+x0(i3+1)*Ekin(i)
           ECM(2)=ECM(2)+x0(i3+2)*Ekin(i)
           ECM(3)=ECM(3)+x0(i3+3)*Ekin(i)
           nECM=nECM+1
        enddo
        call my_mpi_dsum(ECM, 3)
        call my_mpi_dsum(Esum, 1)
        call my_mpi_isum(nECM, 1)
        if (Esum>zero .and. nECm>0.0) then
           ECM(1)=ECM(1)/Esum*box(1)
           ECM(2)=ECM(2)/Esum*box(2)
           ECM(3)=ECM(3)/Esum*box(3)
        endif
        if (iprint) then
           write(6,121) 't',time_fs,' ECM pos.',                        &
                ECM(1),ECM(2),ECM(3),' Eksum',Esum
           write(7,121) 't',time_fs,' ECM pos.',                        &
                ECM(1),ECM(2),ECM(3),' Eksum',Esum
121        format(A,F10.1,A,3F10.2,A,3F12.2)
        endif
     endif
  endif

  call mpi_barrier(mpi_comm_world, ierror)   ! Don't remove this sync !
  if (irec /=  0 .and. nliqanal > 0) then
     if (MOD(istep,nliqanal) == 0) then

        !
        !  Do liquid analysis for both parallell and scalar modes
        !

        !
        !  Pass and receive Ekin and coordinates for atoms in different directions.
        !  The passing of atoms in this piece of code should be identical 
        !  in the neighbour list and force calculations.
        !  
        !  In principle the idea is that the input is the local Ekin(myatoms),
        !  and the output is buf(3*np0) and pbuf(np0), which contain x0 and Ekin
        !  of the atoms both in this node and within cut_nei
        !  from this in the adjacent nodes.!
        !
        !  Note that since the neighbour list calc. for EAM
        !  potentials includes only half the neighbours, we.f90ave to
        !  get Ekin for all _pairs_ into ebuf first, then decide
        !  which atoms are liquid

        !           
        !  The atoms which get passed here must be exactly the
        !  same and in the same order in the force calculations
        !  as in the original neighbour list calculation !
        !
        IF(debug)PRINT *,'liquidat output',myproc

        ! Buffers for pairwise logic:
        ! dbuf contains sum of Ekin over neighbours
        ! pbuf contains max of Ekin over neighbours
        ! ibuf(np0-) contains number of neighbours

        do i=1,3*myatoms
           buf(i)=x0(i)
        enddo
        do i=1,myatoms
           ebuf(i)=Ekin(i)
        enddo
        do i=1,np0pairtable
           dirbuf(i)=0
           if (i <= myatoms) then
              dbuf(i)=ebuf(i)
              pbuf(i)=ebuf(i)
              ibuf(i)=i
              ibuf(np0pairtable+i)=1
           else
              dbuf(i)=zero
              pbuf(i)=zero
              ibuf(i)=0
              ibuf(np0pairtable+i)=0
           endif
        enddo

        np0=myatoms


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!       
        if (nprocs > 1) then
           ! Loop over directions
           do d=1,8
              ! print *,myproc,d,myatoms
              j=0; jj=0;
              do i=1,myatoms
                 if (IAND(atpassflag(i),passbit(d)) /= 0) then
                    j=j+1
                    i3=i*3-3
                    j3=j*3-3
                    if (j >= NPPASSMAX) then
                       print *,'NPPASSMAX overflow1',NPPASSMAX
                       call my_mpi_abort('NPPASSMAX overflow1', INT(myproc))
                    endif
                    xsendbuf(j3+1)=x0(i3+1)
                    xsendbuf(j3+2)=x0(i3+2)
                    xsendbuf(j3+3)=x0(i3+3)
                    psendbuf(j)=Ekin(i)
                    isendbuf(j)=i
                 endif
              enddo
              if (sendfirst(d)) then
                 call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                      mpi_comm_world, ierror)
                 if (j > 0) then
                    call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                         nngbrproc(d), d+8, mpi_comm_world, ierror)
                    call mpi_send(psendbuf, j, mpi_double_precision, &
                         nngbrproc(d), d+16, mpi_comm_world, ierror)
                    call mpi_send(isendbuf, j, my_mpi_integer, &
                         nngbrproc(d), d+24, mpi_comm_world, ierror)
                 endif
                 call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                      mpi_comm_world, mpi_status_ignore, ierror)
                 if (jj >= NPPASSMAX) then
                    print *,'NPPASSMAX overflow1b',jj,NPPASSMAX
                    call my_mpi_abort('NPPASSMAX overflow1b', INT(myproc))
                 endif
                 if (jj > 0) then
                    call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                         mpi_any_source, d+8, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                    call mpi_recv(ebuf(np0+1), jj, mpi_double_precision, &
                         mpi_any_source, d+16, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                    call mpi_recv(ibuf(np0+1), jj, my_mpi_integer, &
                         mpi_any_source, d+24, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                 endif
              else
                 call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                      mpi_comm_world, mpi_status_ignore, ierror)
                 if (jj >= NPPASSMAX) then
                    print *,'NPPASSMAX overflow1c',jj,NPPASSMAX
                    call my_mpi_abort('NPPASSMAX overflow1c', INT(myproc))
                 endif
                 if (jj > 0) then
                    call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                         mpi_any_source, d+8, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                    call mpi_recv(ebuf(np0+1), jj, mpi_double_precision, &
                         mpi_any_source, d+16, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                    call mpi_recv(ibuf(np0+1), jj, my_mpi_integer, &
                         mpi_any_source, d+24, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                 endif
                 call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                      mpi_comm_world, ierror)
                 if (j > 0) then
                    call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                         nngbrproc(d), d+8, mpi_comm_world, ierror)
                    call mpi_send(psendbuf, j, mpi_double_precision, &
                         nngbrproc(d), d+16, mpi_comm_world, ierror)
                    call mpi_send(isendbuf, j, my_mpi_integer, &
                         nngbrproc(d), d+24, mpi_comm_world, ierror)
                 endif
              endif
              j=jj
              do i=1,j
                 dirbuf(np0+i)=d
              enddo
              np0=np0+j
           enddo
        endif
        if (np0 /= np0pairtable) then
           write (6,*) 'mdcasc np0 problem ',myproc,np0,np0pairtable
        endif
        if (2*np0 >= MAX(PARRATIO,18)*2*NPMAX/2) then
           write (6,*) 'mdcasc ibufsize problem ',2*np0,MAX(PARRATIO,18)*2*NPMAX/2
           call my_mpi_abort('mdcasc ibuf', INT(np0))
        endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !
        !  Now we have all x0 and Ekin information we need, let's get Eksum
        !  for all atoms and their nearest neighbours within rsumcrit
        !  Store the Ekin sums for each pair into dbuf, remembering that
        !  AnySemi have all neighbours, EAM only half !
        !
        if (latflag == 0) then ! FCC
           rsumcrit=unitcell(1)*(1.0d0/(2.0*SQRT(2.0))+0.5)
        else if (latflag == 2) then ! DIA
           rsumcrit=unitcell(1)*(SQRT(3.0)/4.0+1.0d0/SQRT(2.0))/2.0
        else
           rsumcrit=(unitcell(1)+unitcell(2)+unitcell(3))/3.0-0.1
        endif
        rsumcritsq=rsumcrit*rsumcrit

        ! Get Ekinsum of all pairs into dbuf, Ekinmax into pbuf
        mij=0
        do i=1,myatoms
           i3=3*i-3
           mij = mij + 1 
           if (mij > NNMAX*NPMAX) then
              print *,'nborlist HORROR ERROR',mij
              call my_mpi_abort('nborlist HORROR ERROR', INT(myproc))
           endif
           nnbors = nborlist(mij)
           do nbr=1,nnbors
              mij=mij+1
              j=nborlist(mij)
              j3=3*j-3

              xp(1)=x0(i3+1)-buf(j3+1)
              xp(2)=x0(i3+2)-buf(j3+2)
              xp(3)=x0(i3+3)-buf(j3+3)

              if (xp(1) >=  half) xp(1)=xp(1)-pbc(1)
              if (xp(1) <  -half) xp(1)=xp(1)+pbc(1)
              if (xp(2) >=  half) xp(2)=xp(2)-pbc(2)
              if (xp(2) <  -half) xp(2)=xp(2)+pbc(2)
              if (xp(3) >=  half) xp(3)=xp(3)-pbc(3)
              if (xp(3) <  -half) xp(3)=xp(3)+pbc(3)

              rs=xp(1)*xp(1)*boxs(1)+xp(2)*xp(2)*boxs(2)+xp(3)*xp(3)*boxs(3)
              if (rs < rsumcritsq) then
                 dbuf(i)=dbuf(i)+ebuf(j)
                 if (ebuf(j)>pbuf(i)) pbuf(i)=ebuf(j)
                 ibuf(np0+i)=ibuf(np0+i)+1

                 if ((EAM .eqv. .true.) .and. (AnySemi .eqv. .false.)) then
                    dbuf(j)=dbuf(j)+ebuf(i)
                    if (ebuf(i)>pbuf(j)) pbuf(j)=ebuf(i)
                    ibuf(np0+j)=ibuf(np0+j)+1
                 endif
              endif
           enddo
        enddo


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        ! PASSING BACK stage: pass back dbuf, pbuf and ibuf
        ! of atoms myatoms+1 - np0
        ! to get total dbuf and pbuf
        ! Only necessary for EAM

        call mpi_barrier(mpi_comm_world, ierror)  

        i=0

        if ((nprocs > 1) .and. (EAM .eqv. .true.) .and. (AnySemi .eqv. .false.)) then
           !     Loop over directions
           do d=1,8
              !     Loop over neighbours
              i=0
              do j=myatoms+1,np0
                 dfrom=dirbuf(j)+4
                 if (dfrom > 8) dfrom=dfrom-8
                 if (d == dfrom) then
                    i=i+1
                    if (i >= NPPASSMAX) then
                       print *,'NPPASSMAX overflow2',i
                       call my_mpi_abort('NPPASSMAX overflow2', INT(myproc))
                    endif
                    ifrom=ibuf(j)
                    isendbuf(2*i-1)=ifrom
                    isendbuf(2*i)=ibuf(np0+j)
                    psendbuf(i)=pbuf(j)
                    xsendbuf(i)=dbuf(j)
                 endif
              enddo
              ! print *,'send',myproc,d,i,nngbrproc(d)
              if (sendfirst(d)) then
                 call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                      mpi_comm_world, ierror)
                 if (i > 0) then
                    call mpi_send(isendbuf, 2*i, my_mpi_integer, &
                         nngbrproc(d), d+8, mpi_comm_world, ierror)
                    call mpi_send(psendbuf, i, mpi_double_precision, &
                         nngbrproc(d), d+16, mpi_comm_world, ierror)
                    call mpi_send(xsendbuf, i, mpi_double_precision, &
                         nngbrproc(d), d+24, mpi_comm_world, ierror)
                 endif
                 call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                      mpi_comm_world, mpi_status_ignore, ierror)
                 if (ii > 0) then
                    call mpi_recv(isendbuf2, 2*ii, my_mpi_integer, &
                         mpi_any_source, d+8, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                    call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                         mpi_any_source, d+16, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                    call mpi_recv(xsendbuf2, ii, mpi_double_precision, &
                         mpi_any_source, d+24, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                 endif
              else 
                 call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                      mpi_comm_world, mpi_status_ignore, ierror)
                 if (ii > 0) then
                    call mpi_recv(isendbuf2, 2*ii, my_mpi_integer, &
                         mpi_any_source, d+8, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                    call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                         mpi_any_source, d+16, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                    call mpi_recv(xsendbuf2, ii, mpi_double_precision, &
                         mpi_any_source, d+24, mpi_comm_world, &
                         mpi_status_ignore, ierror)
                 endif
                 call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                      mpi_comm_world, ierror)
                 if (i > 0) then
                    call mpi_send(isendbuf, 2*i, my_mpi_integer, &
                         nngbrproc(d), d+8, mpi_comm_world, ierror)
                    call mpi_send(psendbuf, i, mpi_double_precision, &
                         nngbrproc(d), d+16, mpi_comm_world, ierror)
                    call mpi_send(xsendbuf, i, mpi_double_precision, &
                         nngbrproc(d), d+24, mpi_comm_world, ierror)
                 endif
              endif
              i=ii

              ! Sum in ebuf and pbuf into my local arrays
              do ii=1,i
                 myi=isendbuf2(2*ii-1)
                 if (myi<1 .or. myi > myatoms) then
                    print *,myproc,myatoms,myi,ii,i
                    call my_mpi_abort('mdcasc i problem', INT(myproc))
                 endif
                 dbuf(myi)=dbuf(myi)+psendbuf2(ii)
                 if (psendbuf2(ii) > pbuf(myi)) pbuf(myi)=psendbuf2(ii)
                 ibuf(np0+myi)=ibuf(np0+myi)+isendbuf2(2*ii)
              enddo
           enddo
        endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        call mpi_barrier(mpi_comm_world, ierror)   ! Don't remove this sync !

        ! Now, finally, find actual liquid atoms and pack them in buffers
        ! for sending to processor 0.
        nliqat=0
        do i=1,myatoms
           Eksum=dbuf(i)
           Ekmax=pbuf(i)
           nsumnbrs=ibuf(np0+i)
           !               
           !              Store liquid atom info in sending buffers
           !
           if (nsumnbrs > 0) then
              if (Eksum/nsumnbrs>Ekdef.and.(Eksum-Ekmax)/nsumnbrs>Ekdef/2.0) then
                 i3=3*i-3 
                 nliqat=nliqat+1
                 j=nliqat
                 j3=nliqat*3-3
                 if (j >= NPPASSMAX) then
                    print *,'nliqat overflow',j,NPPASSMAX,nliqat
                    call my_mpi_abort('nliqat overflow', INT(myproc))
                 endif
                 xsendbuf(j3+1)=x0(i3+1)
                 xsendbuf(j3+2)=x0(i3+2)
                 xsendbuf(j3+3)=x0(i3+3)
                 psendbuf(j)=Ekin(i)
                 isendbuf(j)=atomindex(i)
                 ! print *,myproc,i,Ekin(i),Eksum
              endif
           endif
        enddo
        !
        !           Send back liquid atoms to proc. 0 for printing
        !            
        j=nliqat
        if (j >= NPPASSMAX) then
           print *,'nliqat overflow2',j,NPPASSMAX,nliqat
           call my_mpi_abort('nliqat overflow2', INT(myproc))
        endif
        do node=0,nprocs-1
           if (node>0.and.node==myproc) then
              call mpi_send(j, 1, my_mpi_integer, 0, myproc, &
                   mpi_comm_world, ierror)
              if (j > 0) then
                 call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                      0, nprocs+myproc, mpi_comm_world, ierror)
                 call mpi_send(psendbuf, j, mpi_double_precision, &
                      0, nprocs*2+myproc, mpi_comm_world, ierror)
                 call mpi_send(isendbuf, j, my_mpi_integer, &
                      0, nprocs*3+myproc, mpi_comm_world, ierror)
              endif
           endif
           if (myproc == 0) then
              if (node>0) then
                 call mpi_recv(j, 1, my_mpi_integer, mpi_any_source, &
                      node, mpi_comm_world, mpi_status_ignore, ierror)
                 !                    print *,'received',myproc,d,j
                 if (j >= NPPASSMAX) then
                    print *,'nliqat overflow2b',j,NPPASSMAX,nliqat
                    call my_mpi_abort('nliqat overflow2b', INT(myproc))
                 endif
                 if (j > 0) then
                    call mpi_recv(xsendbuf, j*3, mpi_double_precision, &
                         mpi_any_source, nprocs+node, &
                         mpi_comm_world, mpi_status_ignore, ierror)
                    call mpi_recv(psendbuf, j, mpi_double_precision, &
                         mpi_any_source, nprocs*2+node, &
                         mpi_comm_world, mpi_status_ignore, ierror)
                    call mpi_recv(isendbuf, j, my_mpi_integer, &
                         mpi_any_source, nprocs*3+node, &
                         mpi_comm_world, mpi_status_ignore, ierror)
                 endif
              endif
              !                  
              !                 Print liquid atom
              !                 
              do i=1,j
                 i3=i*3-3
                 IF(iprint)WRITE (21,210)                                        &
                      xsendbuf(i3+1)*box(1),                            &
                      xsendbuf(i3+2)*box(2),                            &
                      xsendbuf(i3+3)*box(3),                            &
                      time_fs,i,psendbuf(i),node,isendbuf(i)
210              format (F10.4,1X,F10.4,1X,F10.4,1X,F10.1,1X,I7,1X,F10.4,1X,I4,1X,I8)
              enddo
           endif
        enddo
         

        !
        !  Liquid analysis done
        !

        call my_mpi_isum(nliqat, 1)
        if (iprint) then
           write (6,215) 'Nliq',nliqat,' at time',time_fs
215        format (A,I12,A,F10.1)
        endif
     endif ! if (mod(istep,nliqanal) == 0) then
  endif ! if (nliqanal) 

  return
end subroutine getcascproperties


!**********************************************************************
!     Recoil sequence subroutines
!**********************************************************************

subroutine getnextrec(rsnum,rsn,irec,xrec,yrec,zrec,                  &
     rectheta,recphi,recen,rstmax)

  use datatypes
  use my_mpi

  use defs  

  use para_common

  implicit none
  !
  !     On firsttime, subroutine reads in file recoildata.in 
  !     Every time: returns recoil # rsn info in irec etc.
  !
  !     File recoildata.in format:
  !
  !     irec1 xrec1 yrec1 zrec1 theta1 phi1 E1 tmax1
  !     irec2 xrec2 yrec2 zrec2 theta2 phi2 E2 tmax2
  !     ...
  !
  !     If tmax<0 it loses its meaning, and endtemp is used instead.
  !     If tmax is used, recoil ends whenever tmax is reached.
  !     Otherwise it ends when T<endtemp
  !
  integer rsnum,rsn,irec
  real*8 xrec,yrec,zrec,rectheta,recphi,recen,rstmax

!include 'para_common.f90'

  !     arrays of recoil data
  integer rsirec(MAXREC)     
  real*8 rsxrec(MAXREC),rsyrec(MAXREC),rszrec(MAXREC)
  real*8 rstheta(MAXREC),rsphi(MAXREC),rsE(MAXREC)
  real*8 rst(MAXREC)

  !     recoil seq. data file name

  character*160 rsfile,str

  integer nline,i,nreadin
  logical firsttime
  data firsttime /.true./

  save

  integer(kind=mpi_parameters) :: ierror  ! MPI error code

  if (firsttime) then
     !        Read in recoil sequence data
     firsttime=.false.

     if (iprint) then
        i=0
        nline=0
        rsfile='in/recoildata.in'
        open(4,file=rsfile,status='old')
10      read(4,11,err=29) str
11      format(a)
        nline=nline+1
        if (str(1:1)=='#') goto 10
        i=i+1
        if (i > MAXREC) then
           print *,'maxrec overflow',MAXREC
           call my_mpi_abort('maxrec overflow', INT(MAXREC))
        endif
        read(unit=str,fmt=*,err=29) irec,xrec,yrec,zrec,                &
             rectheta,recphi,recen,rstmax
        rsirec(i)=irec
        rsxrec(i)=xrec
        rsyrec(i)=yrec
        rszrec(i)=zrec
        rstheta(i)=rectheta*pi/180.0
        rsphi(i)=recphi*pi/180.0
        rsE(i)=recen
        rst(i)=rstmax

        if (i >= rsnum) goto 49
        goto 10
29      write(6,*) 'recoildata read error',nline,str
49      continue
        nreadin=i
        write(6,'(A,I5,A)') 'Read in recoil information for',           &
             nreadin,' recoils'
        if (nreadin < rsnum) then
           write (6,*) 'Too few recoils found',nreadin,rsnum
           call my_mpi_abort('rsnum', INT(rsnum))
        endif

     endif

     call mpi_bcast(nreadin, 1, my_mpi_integer, &
          0, mpi_comm_world, ierror)
     call mpi_bcast(rsirec, nreadin, my_mpi_integer, &
          0, mpi_comm_world, ierror)
     call mpi_bcast(rsxrec, nreadin, mpi_double_precision, &
          0, mpi_comm_world, ierror)
     call mpi_bcast(rsyrec, nreadin, mpi_double_precision, &
          0, mpi_comm_world, ierror)
     call mpi_bcast(rszrec, nreadin, mpi_double_precision, &
          0, mpi_comm_world, ierror)
     call mpi_bcast(rstheta, nreadin, mpi_double_precision, &
          0, mpi_comm_world, ierror)
     call mpi_bcast(rsphi, nreadin, mpi_double_precision, &
          0, mpi_comm_world, ierror)
     call mpi_bcast(rsE, nreadin, mpi_double_precision, &
          0, mpi_comm_world, ierror)
     call mpi_bcast(rst, nreadin, mpi_double_precision, &
          0, mpi_comm_world, ierror)
  endif


  if (rsn > nreadin) then
     IF(iprint)WRITE (6,*) 'Too few rsn points read in',                         &
          rsn,nreadin,myproc
     call my_mpi_abort('rsn too small', INT(rsn))
  endif

  irec=rsirec(rsn)
  xrec=rsxrec(rsn)
  yrec=rsyrec(rsn)
  zrec=rszrec(rsn)
  rectheta=rstheta(rsn)
  recphi=rsphi(rsn)
  recen=rsE(rsn)
  rstmax=rst(rsn)

  return 

end subroutine getnextrec


!**********************************************************************
!     Cluster subroutines
!**********************************************************************

subroutine addvel(x0,x1,x2,x3,x4,x5,atype,box,boxs,                  &
     eaddvel,vaddvel,zaddvel,doneaddvel,                             &
     irec,vmaxnosput,timekt,timeEt,timeCh,                           &
     deltat_fs,deltamax_fs,delta,deltas,deltaratio,                  &
     addvelt, addvelp)
  use typeparam
  use my_mpi
  use defs  

  use para_common

  implicit none

!include 'para_common.f90'

  real*8 x0(*),x1(*),x2(*),x3(*),x4(*),x5(*)
  real*8 box(3),boxs(3)
  real*8 eaddvel,vaddvel,zaddvel,addvelt,addvelp,thrad,phrad, emhelp
  integer atype(*),irec
  real*8 vmaxnosput,timekt,timeEt,timeCh
  real*8 deltat_fs,deltamax_fs,deltaratio
  real*8 delta(itypelow:itypehigh),deltas(itypelow:itypehigh)
  logical doneaddvel

  ! local variables
  integer i,i1,j
  real*8 help1,help2,help3,help4,help5
  logical doneaddvelp

  if (eaddvel > zero .and. vaddvel > zero) then
     IF(iprint)WRITE (6,'(A,2F13.6)') 'ERROR: both eaddvel and vaddvel > 0. This does not make physical sense',eaddvel,vaddvel
     call my_mpi_abort('double addvel',0)
  endif

  if (eaddvel > zero) then
     IF(iprint)WRITE (6,'(A,F16.6,A,F16.6)') 'Adding kinetic energy downwards', &
          eaddvel,' to atoms above',zaddvel
     do i=itypelow,itypehigh
        IF(iprint)WRITE (6,'(A,F13.6,A,I3)') '   Energy corresponds to', &
             sqrt(2.0*eaddvel)*vunit(i),' A/fs for atype',i
     enddo
  else if (vaddvel > zero) then
     IF(iprint)WRITE (6,'(A,F16.6,A,F16.6)') 'Adding velocity downwards', &
          vaddvel,' to atoms above',zaddvel
     do i=itypelow,itypehigh
        IF(iprint)WRITE (6,'(A,F16.6,A,I3)') '   Velocity corresponds to', &
             half*(vaddvel/vunit(i))**2,' eV for atype',i
     enddo
  endif

  IF(iprint)WRITE (6,'(A,F10.3,A,F10.3)') 'Using angles theta: ',addvelt*180.0/pi,'  phi: ',addvelp*180.0/pi; 
  j=0; doneaddvelp=.false.; help2=zero; help3=zero;
  do i=1,myatoms
     if (x0(i*3)*box(3) > zaddvel) then
        if (.not. doneaddvelp) then
           IF(iprint)WRITE (6,'(A,F13.6,A,I3)') 'Energy corresponds to', &
                SQRT(2.0*eaddvel)*vunit(ABS(atype(i))),' �/fs for atype',atype(i)
           doneaddvelp=.true.
        endif
        i1=ABS(atype(i))

        if (eaddvel > zero) then
           emhelp = delta(i1)*sqrt(2.0*eaddvel);
        else if (vaddvel > zero) then
           emhelp=delta(i1)*vaddvel/vunit(i1)
        endif

        x1(i*3-2)=x1(i*3-2)+emhelp*SIN(addvelt)*COS(addvelp)/box(1)
        x1(i*3-1)=x1(i*3-1)+emhelp*SIN(addvelt)*SIN(addvelp)/box(2)
        x1(i*3)=x1(i*3)+emhelp*COS(addvelt)/box(3)

        ! Calculate maximum velocity

        help1=(x1(i*3-2)*x1(i*3-2)*boxs(1)+x1(i*3-1)*x1(i*3-1)*boxs(2) &
             +x1(i*3)*x1(i*3)*boxs(3))/(delta(i1)*delta(i1));          
        if (help1 > help2) then           
           help2=help1
           help3=SQRT(help2)*vunit(i1)
        endif
        j=j+1

     endif
  enddo
  call my_mpi_isum(j, 1)
  IF(iprint)WRITE (6,'(A,I7,A)') 'Added energy or velocity to',j,' atoms'

  ! Also change maximum velocity, otherwise Tstep will not
  ! be handled sensibly
  vmaxnosput=help3

  call my_mpi_dmax(vmaxnosput, 1)

  IF(iprint)WRITE (6,'(A,G13.6,A)') 'Maximum velocity afterwards',help3,' A/fs'   
  ! Then immediately get new time step
  call Get_Tstep(irec,vmaxnosput,0.0d0,timekt,timeEt,timeCh,     &
       deltat_fs,deltamax_fs,delta,deltas,deltaratio,1d30)
  !
  !  Correct x1 and x2 units for the new time step (!!)
  !
  help2=deltaratio*deltaratio
  help3=deltaratio*help2
  help4=deltaratio*help3
  help5=deltaratio*help4
  do i=1,3*myatoms
     x1(i)=x1(i)*deltaratio
     x2(i)=x2(i)*help2
     x3(i)=x3(i)*help3
     x4(i)=x4(i)*help4
     x5(i)=x5(i)*help5
  enddo

  doneaddvel=.true.

  return

end subroutine addvel




!**********************************************************************
!     track subroutines
!**********************************************************************

subroutine addtrack(x0,x1,x2,x3,x4,x5,atype,box,boxs,              &
     trackx,tracky,trackt,                                         &
     irec,vmaxnosput,timekt,timeEt,timeCh,                         &
     deltat_fs,deltamax_fs,delta,deltas,deltaratio)
  use typeparam
  use my_mpi
  use defs  

  use para_common
  use random

  implicit none

!include 'para_common.f90'

  real*8 x0(*),x1(*),x2(*),x3(*),x4(*),x5(*)
  real*8 box(3),boxs(3)
  real*8 trackx,tracky,trackt
  integer atype(*),irec
  real*8 vmaxnosput,timekt,timeEt,timeCh
  real*8 deltat_fs,deltamax_fs,deltaratio
  real*8 delta(itypelow:itypehigh),deltas(itypelow:itypehigh)

  ! local variables
  integer i,i1,ii,ll,ul,imax,ntrack
  real*8 help1,help2,help3,help4,help5
  real*8 rmax,trackEtot,ri,eadd,emhelp,theta,phi
  integer, parameter :: tracksize=100
  real*8 trackr(tracksize),trackE(tracksize)

!  real*8, external :: MyRanf


  IF(iprint)WRITE (6,'(A)') 'Adding kinetic energy to track atoms'

  ! Read in track input file
  open(59,file="in/track.in",status='old',err=19)
  goto 20
19 IF(iprint)WRITE (6,*) 'ERROR: track.in read error'
  call my_mpi_abort('track.in', INT(0))
20 continue

30 i=1
31 read(59,*,end=32,err=19) trackr(i),trackE(i)
  print *,'track read',trackr(i),trackE(i)
  i=i+1
  if (i .ge. tracksize) then
     write (6,*) 'Track array too big',tracksize
     call my_mpi_abort('Track array size', INT(myproc))
  endif
  goto 31
32 close(59)
  imax=i-1;
  rmax=trackr(imax)
  IF(iprint)WRITE(6,*) 'Read in track',imax,rmax

  if (trackr(1) /= 0.0) then
     IF(iprint)WRITE (6,*) 'track ERROR: first r must be 0',trackr(1)
     call my_mpi_abort('Track error', INT(0))
  endif
  if (trackE(imax) /= 0.0) then
     IF(iprint)WRITE (6,*) 'track ERROR: last E must be 0',trackE(imax)
     call my_mpi_abort('Track error', INT(0))
  endif

  ntrack=0
  trackEtot=zero
  help2 = zero
  do i=1,myatoms

     ! Distance to track center
     ! Note that periodics ARE NOT taken into account
     ri=SQRT((x0(i*3-2)*box(1)-trackx)**2+(x0(i*3-1)*box(2)-tracky)**2)
     !print *,i,ri,rmax
     if (ri < rmax) then
        ! Inside track, add E
        if (atype(i) < 0) then
           print *,'You try to add track E to a fixed atom?',i,atype(i)
        endif

        ! Binary search of track file data 
        ul=imax
        ll=1
40      ii=(ul-ll)/2
        if (ri .lt. trackr(ll+ii)) then
           ul=ll+ii
        else
           ll=ll+ii
        endif
        if (ul-ll > 1) goto 40
        if (ul<=ll .or. ul > imax) then
           print *,'track error',ll,ul,imax,trackr
           call my_mpi_abort('ELSTOP HORROR', INT(ul))
        endif

        ! Linear extrapolation between points
        eadd=(trackE(ul)-trackE(ll))/(trackr(ul)-trackr(ll))*(ri-trackr(ll))+trackE(ll)
        trackEtot=trackEtot+eadd

        i1=ABS(atype(i))
        emhelp = delta(i1)*SQRT(2.0*eadd);

        ! Get random direction in 3D
        phi=MyRanf(0)*2.0*pi
        theta=ACOS(COS(pi)+MyRanf(0)*(1.0d0-COS(pi)))

        ! Add velocity
        x1(i*3-2)=x1(i*3-2)+emhelp*SIN(theta)*COS(phi)/box(1)
        x1(i*3-1)=x1(i*3-1)+emhelp*SIN(theta)*SIN(phi)/box(2)
        x1(i*3)=x1(i*3)+emhelp*COS(theta)/box(3)

        ! Calculate maximum velocity

        help1=(x1(i*3-2)*x1(i*3-2)*boxs(1)+x1(i*3-1)*x1(i*3-1)*boxs(2) &
             +x1(i*3)*x1(i*3)*boxs(3))/(delta(i1)*delta(i1));          
        if (help1 > help2) then           
           help2=help1
           help3=SQRT(help2)*vunit(i1)
        endif
        ntrack=ntrack+1

     endif
  enddo
  call my_mpi_isum(ntrack, 1)
  call my_mpi_dsum(trackEtot, 1)
  IF(iprint)WRITE (6,'(A,F13.3,A,I7,A)') 'Added E ',trackEtot,' to',ntrack,' atoms'

  ! Also change maximum velocity, otherwise Tstep will not
  ! be handled sensibly
  vmaxnosput=help3

  call my_mpi_dmax(vmaxnosput, 1)

  IF(iprint)WRITE (6,'(A,G13.6,A)') 'Maximum velocity afterwards',help3,' A/fs'   
  ! Then immediately get new time step
  call Get_Tstep(irec,vmaxnosput,0.0d0,timekt,timeEt,timeCh,     &
       deltat_fs,deltamax_fs,delta,deltas,deltaratio,1d30)
  !
  !  Correct x1 and x2 units for the new time step (!!)
  !
  help2=deltaratio*deltaratio
  help3=deltaratio*help2
  help4=deltaratio*help3
  help5=deltaratio*help4
  do i=1,3*myatoms
     x1(i)=x1(i)*deltaratio
     x2(i)=x2(i)*help2
     x3(i)=x3(i)*help3
     x4(i)=x4(i)*help4
     x5(i)=x5(i)*help5
  enddo

  return

end subroutine addtrack
