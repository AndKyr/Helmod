  
  !***********************************************************************
  ! Support subroutines for lattice readin or creation:
  ! Get_Atoms(), FCC_Gen(), DIA_Gen(), Add2list()
  !***********************************************************************
  
  !***********************************************************************
  ! Read in the initial lattice
  !***********************************************************************
  subroutine Get_Atoms(x0,x1,atomindex,                                 &
       atype,natoms,nfixed,amp,initemp,tdebye,box,pbc,latflag,restartmode,mdlatxyz, &
       dsliceIn,ECMIn &
       )
    use typeparam
    use datatypes
    use my_mpi
    use binout
    use defs

    use para_common
    use random
    implicit none

    !      IMPLICIT REAL*8  (a-h,o-z)
    !      IMPLICIT INTEGER (i-n)

    !     ------------------------------------------------------------------
    !          Variables passed in and out
    real*8 x0(*),x1(*),amp,box(3),pbc(3),initemp,tdebye
    integer atype(*),natoms,nfixed,latflag,mdlatxyz,atomindex(*),restartmode

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     rmn(1..2),rmx(1..2) = x&y min&max boundaries for my nodes region
    !     ------------------------------------------------------------------
    !          Local variables and constants
    integer, parameter :: nsize=100000
    integer nsizetemp
    integer atypes(3,nsize)
    integer nxoutside,nyoutside,nzoutside,noutside
    real*8 xd(3,nsize),x1d(3,nsize) !,MyRanf
!    external MyRanf

    integer i,j,k,m,i3,m3,nread,indexd(nsize),index
    character*8 xyzatom
    character*160 xyzcomment

    real*8 ECMIn(3),dsliceIn(3) !slicing on binary restart reading
    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !     ------------------------------------------------------------------

    !
    !  restart mode determined by latflag:
    !     0 No restart, create FCC
    !     1 Read in only coordinates and atom type
    !     2 No restart, create DIA
    !     3 Read in velocities and atom indices as well, guess recoil
    !     4 Read in velocities and atom indices as well, don't guess recoil
    !

    !     The stupid reading in of three integers for the atom type
    !     is a remnant of the original invm() array which was never
    !     used.
    !
    !     restartmode 9 is for reading 
    if(restartmode == 9) then

       if (latflag /= 3 .and. latflag /= 4) then
          call my_mpi_abort('only latflag=3 or 4 supported by binary restarts', 0)
       end if
       myatoms=0   
       call binaryread('in/restart',1,0,nread,myatoms,atype,atomindex,x0,x1,dsliceIn,ECMIn)

       nfixed=0

       do i=1,myatoms 
          if (atype(i) < 0) nfixed=nfixed+1
       end do
       call my_mpi_isum(nfixed, 1)
       IF(iprint)WRITE (6,'(A,I7)')                                                   &
            'Number of readin fixed atoms: ',nfixed

    else
       IF(debug)PRINT *,'get_atoms syn'
       call mpi_barrier(mpi_comm_world, ierror)
        
       IF(debug)PRINT *,'get_atoms sync done'
        
       IF(debug)PRINT *,'Opening mdlat.in',myproc
       if (mdlatxyz==1) then
          print *,'Opening mdlat.in.xyz'
          IF(iprint)OPEN(5,file='in/mdlat.in.xyz',status='old',err=5)
       else
          print *,'Opening mdlat.in'         
          IF(iprint)OPEN(5,file='in/mdlat.in',status='old',err=5)
       endif
       goto 6
5      print *,'mdlat.in open error: latflag myproc',latflag,myproc
       call my_mpi_abort('mdlat open', INT(myproc))

6      IF(debug)PRINT *,'mdlat opened',myproc
        

       nread=0
       myatoms=0
       nfixed=0
       index=0
       nsizetemp=nsize
       if (iprint .and. mdlatxyz==1) then
          IF(iprint)READ(5,*) i
          if (i /= natoms) then
             print *,'mdlatxyz wrong natoms in xyz file',i
             call my_mpi_abort('natoms mismatch', INT(i))
          endif
          IF(iprint)READ(5,fmt='(A160)') xyzcomment
          print *,'mdlat.in.xyz comment:',TRIM(xyzcomment)
       endif

       do i=1,natoms,nsizetemp
          IF(iprint)WRITE (6,*) i
           
          if (i+nsizetemp-1 > natoms) nsizetemp = natoms - i + 1
          do j=1,nsizetemp
             if (latflag .eq. 1) then
                if (mdlatxyz==1) then
                   IF(iprint)READ(5,*,END=10) xyzatom,(xd(k,j),k=1,3),                   &
                        atypes(1,j)
                else
                   IF(iprint)READ(5,*,END=10) (xd(k,j),k=1,3),                           &
                        (atypes(k,j),k=1,3)
                endif
             else if (latflag .eq. 3 .or. latflag .eq. 4) then
                if (mdlatxyz==1) then                  
                   IF(iprint)READ(5,*,END=10) xyzatom,(xd(k,j),k=1,3),                   &
                        atypes(1,j),indexd(j),(x1d(k,j),k=1,3)
                else
                   IF(iprint)READ(5,*,END=10) (xd(k,j),k=1,3),                           &
                        (atypes(k,j),k=1,3),(x1d(k,j),k=1,3),indexd(j)

                endif
             endif
10           nread=nread+1
             if (iprint) then
                if (mdlatxyz==1) then
                   ! Unit transformation into mdlat.in units
                   xd(1,j)=xd(1,j)/box(1)
                   xd(2,j)=xd(2,j)/box(2)
                   xd(3,j)=xd(3,j)/box(3)
                   ! Check atom name
                   if (TRIM(xyzatom) /= TRIM(element(ABS(atypes(1,j))))) then
                      print *,'mdlatxyz atom name mismatch',nread,xyzatom,element(ABS(atypes(1,j)))
                      call my_mpi_abort('atom name mismatch', INT(i))
                   endif
                   if (latflag .eq. 3 .or. latflag .eq. 4) then
                      ! Unit transformation into mdlat.in units (note: division by delta comes later)
                      x1d(1,j)=x1d(1,j)/(vunit(ABS(atypes(1,j)))*box(1))
                      x1d(2,j)=x1d(2,j)/(vunit(ABS(atypes(1,j)))*box(2))
                      x1d(3,j)=x1d(3,j)/(vunit(ABS(atypes(1,j)))*box(3))
                   endif
                endif
                ! Random movement from ideal positions         
                if (latflag .ne. 3 .and. latflag .ne. 4) then
                   call displaceatom(xd(1,j),xd(2,j),xd(3,j),box,            &
                        atypes(1,j),amp,initemp,tdebye)
                endif
                do k=1,3
                   if (xd(k,j) ==  half) xd(k,j)=xd(k,j)-1e-10
                   if (xd(k,j) == -half) xd(k,j)=xd(k,j)+1e-10
                   if (pbc(k)==1.0d0) then
                      if (xd(k,j) > half) then
                         xd(k,j)=xd(k,j)-one
                      elseif (xd(k,j) <= -half) then
                         xd(k,j)=xd(k,j)+one
                      endif
                   endif
                enddo
             endif
          enddo
          IF(debug)PRINT *,'Doing broadcast',i,latflag,myproc
           
          call mpi_bcast(xd, 3*nsizetemp, mpi_double_precision, 0, mpi_comm_world, ierror)
          call mpi_bcast(atypes, 3*nsizetemp, my_mpi_integer, 0, mpi_comm_world, ierror)
          if (latflag .eq. 3 .or. latflag .eq. 4) then
             call mpi_bcast(x1d, 3*nsizetemp, mpi_double_precision, 0, mpi_comm_world, ierror)
             call mpi_bcast(indexd, nsizetemp, my_mpi_integer, 0, mpi_comm_world, ierror)
          endif
          IF(debug)PRINT *,'Broadcast done',i,myproc
           
          do j=1,nsizetemp
             index=index+1
             if (xd(1,j) > rmn(1) .and. xd(1,j) <= rmx(1) .and.              &
                  xd(2,j) > rmn(2) .and. xd(2,j) <= rmx(2) ) then

                !              Check to insure myatoms < NPMAX
                if (myatoms .gt. NPMAX) then
                   write(6,*) ' myatoms > NPMAX in Get_Atoms'
                   call my_mpi_abort('natoms in Get_Atoms', INT(myproc))
                endif
                myatoms=myatoms+1
                m3=3*myatoms-3
                do k=1,3
                   x0(m3+k)=xd(k,j)
                   if (latflag==3.or.latflag==4) x1(m3+k)=x1d(k,j)
                enddo
                atype(myatoms)=atypes(1,j)
                if (atype(myatoms) < 0) nfixed=nfixed+1
                if (latflag == 3 .or. latflag == 4) then
                   if (indexd(j) .lt. 0) then
                      print *,'Get_atoms warning: weird atom index'
                      print *,myproc,myatoms,indexd(j)
                   endif
                   atomindex(myatoms)=indexd(j)
                else 
                   atomindex(myatoms)=index
                endif
             endif
          enddo
          IF(debug)PRINT *,'Readin loop done',i,myproc
           
       enddo
       IF(iprint)CLOSE(5)

       call my_mpi_isum(nfixed, 1)
       IF(iprint)WRITE (6,'(A,I7)')                                                   &
            'Number of readin fixed atoms: ',nfixed
    end if

    noutside=0
    nxoutside=0; nyoutside=0; nzoutside=0; 
    do i=1,myatoms
       i3=i*3-3
       if (x0(i3+1)<-0.5 .or. x0(i3+1)>0.5) nxoutside=nxoutside+1
       if (x0(i3+2)<-0.5 .or. x0(i3+2)>0.5) nyoutside=nyoutside+1
       if (x0(i3+3)<-0.5 .or. x0(i3+3)>0.5) nzoutside=nzoutside+1
       if (x0(i3+1)<-0.5 .or. x0(i3+1)>0.5 .or.                           &
            x0(i3+2)<-0.5 .or. x0(i3+2)>0.5 .or.                          &
            x0(i3+3)<-0.5 .or. x0(i3+3)>0.5) noutside=noutside+1
    enddo
    call my_mpi_isum(noutside, 1)
    call my_mpi_isum(nxoutside, 1)
    call my_mpi_isum(nyoutside, 1)
    call my_mpi_isum(nzoutside, 1)
    if (iprint) then
       write (6,'(A,I7,A)') 'A total of ',noutside,                       &
            ' read in atoms are outside the basic cell borders.'
       write (6,'(A,3I7)') 'Per dimension:',                              &
            nxoutside,nyoutside,nzoutside
    endif



    if (nread .ne. natoms) then
       if(iprint) then
          write(6,*)
          write(6,*) 'WARNING: Total number of atoms read in does not'
          write(6,*) 'WARNING: equal the number of atoms specified. '
          write(6,*) 'WARNING: Can be ok if input slicing is used'
          write(6,*) myproc,natoms,nread
       end if
       !    CALL my_mpi_abort(' Natoms != read in Nat', INT(myproc))
       natoms=nread
    endif




    m=myatoms
    call my_mpi_isum(m, 1)
    if (m .ne. natoms) then
       if (iprint) then
          write(6,*)
          write(6,*) 'Total number of atoms distributed does not'
          write(6,*) 'equal the number of atoms specified.'
          write(6,*) myproc,natoms,m
       endif
       call my_mpi_abort('Natoms != Nat distributed', INT(myatoms))

    endif

  end subroutine Get_Atoms

  !***********************************************************************
  ! Generate arbitrary lattice from scratch
  !***********************************************************************
  subroutine  Cryst_Gen(x0,atomindex,atype,                             &
       natoms,amp,initemp,tdebye,box,pbc,                               &
       ncells,nvac,vaczmin,latflag)

    use basis
    use typeparam
    use my_mpi

    use defs

    use para_common

    implicit none

    !     ------------------------------------------------------------------
    !          Variables passed in and out
    real*8 x0(*),pbc(3), box(3), amp,initemp,tdebye,vaczmin
    integer natoms, atype(*), ncells(*),atomindex(*),nvac,latflag

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     ------------------------------------------------------------------
    !          Local variables and constants
    real*8 u(3), xlat, ylat, zlat

    integer i,j,k,l,m,index

    real*8 a(3)

    !     ------------------------------------------------------------------

    !     Unit cell offset from unit cell origin in x y z
    real*8 offset(3)

    !     Basis of atoms in unit cell, set below in if clause
    real*8 xbasis(128,3)

    !     Number of atoms in basis, set in if below
    integer nbasis 

    !     ------------------------------------------------------------------

    if (latflag==0) then
       !        FCC
       nbasis=4
       offset(1) = 0.25    ; offset(2) = 0.25;     offset(3) = 0.25;
       xbasis(1,1)= 0.0    ; xbasis(1,2)= 0.0;     xbasis(1,3)= 0.0;
       xbasis(2,1)= 0.0    ; xbasis(2,2)= 0.5;     xbasis(2,3)= 0.5;
       xbasis(3,1)= 0.5    ; xbasis(3,2)= 0.0;     xbasis(3,3)= 0.5;
       xbasis(4,1)= 0.5    ; xbasis(4,2)= 0.5;     xbasis(4,3)= 0.0;
       do i=1,nbasis
          readtype(i)=1
          changeprob(i)=zero
          changeto(i)=1
       enddo
    else if (latflag==2) then
       !        DIA
       nbasis=8
       offset(1) = 0.125   ; offset(2) = 0.125   ; offset(3) = 0.125     
       xbasis(1,1)= 0.0    ; xbasis(1,2)= 0.0    ; xbasis(1,3)= 0.0      
       xbasis(2,1)= 0.0    ; xbasis(2,2)= 0.5    ; xbasis(2,3)= 0.5      
       xbasis(3,1)= 0.5    ; xbasis(3,2)= 0.0    ; xbasis(3,3)= 0.5      
       xbasis(4,1)= 0.5    ; xbasis(4,2)= 0.5    ; xbasis(4,3)= 0.0      
       xbasis(5,1)= 0.25   ; xbasis(5,2)= 0.25   ; xbasis(5,3)= 0.25     
       xbasis(6,1)= 0.25   ; xbasis(6,2)= 0.75   ; xbasis(6,3)= 0.75     
       xbasis(7,1)= 0.75   ; xbasis(7,2)= 0.25   ; xbasis(7,3)= 0.75     
       xbasis(8,1)= 0.75   ; xbasis(8,2)= 0.75   ; xbasis(8,3)= 0.25    
       do i=1,nbasis
          readtype(i)=1
          changeprob(i)=zero
          changeto(i)=1
       enddo
    else if (latflag==5) then
       !        Use read in lattice structure
       nbasis=nreadbasis
       do i=1,3
          offset(i) = readoffset(i)
       enddo
       do l=1,nbasis
          do i=1,3
             xbasis(l,i) = readbasis(l,i)
          enddo
       enddo
    else
       write (6,*) 'Cryst_Gen ERROR: unknown lattice type',latflag
       call my_mpi_abort('Invalid latflag', INT(latflag))
    endif

    a(1)=box(1)/ncells(1);a(2)=box(2)/ncells(2);a(3)=box(3)/ncells(3)

    if (iprint) then
       write (6,'(A,I4)') 'Number of atoms in basis',nbasis
       write (6,'(A,3F10.3)') 'Unit cell size ax ay az',(a(j),j=1,3)
       write (6,'(A,3F10.3)') 'Unit cell offset',(offset(j),j=1,3)
       do i=1,nbasis
          write (6,'(A,I2,A,I2,A,3F8.4,A,I3,A,F8.5)')                     &
               'Atom',i,' type',readtype(i),                              &
               ' pos',xbasis(i,1),xbasis(i,2),xbasis(i,3),                &
               ' changed to',changeto(i),' with prob.',changeprob(i)
       enddo
    endif

    myatoms=0

    xlat=one/ncells(1)
    ylat=one/ncells(2)
    zlat=one/ncells(3)
    index=0
    do i=1,ncells(1)
       u(1)=xlat*(i-1) + xlat*offset(1) - half    
       !cycle if we are far away from the process domain, conservative safe assumptions  
       if( u(1) <  rmn(1) - xlat .or. u(1) > rmx(1) + xlat) then
          index=index+ncells(2)*ncells(3)*nbasis
          cycle 
       end if
       do j=1,ncells(2)
          u(2)=ylat*(j-1) + ylat*offset(2) - half
          if( u(2) <  rmn(2) -ylat .or. u(2) > rmx(2) + ylat)then
             index=index+ncells(3)*nbasis
             cycle 
          end if
          do k=1,ncells(3)
             u(3)=zlat*(k-1) + zlat*offset(3) - half
             do l=1,nbasis 
                index=index+1
                call add2list(x0,index,atomindex,atype,                   &
                     u(1)+xbasis(l,1)*xlat,                               &
                     u(2)+xbasis(l,2)*ylat,                               &
                     u(3)+xbasis(l,3)*zlat,                               &
                     readtype(l),changeprob(l),changeto(l),               &
                     amp,box,pbc,nvac,vaczmin,natoms,initemp,tdebye)

             enddo
          enddo
       enddo
    enddo

    m=myatoms
    call my_mpi_isum(m, 1)
    if (m .ne. natoms) then
       IF(iprint)WRITE(6,*)
       IF(iprint)WRITE(6,*) ' Total number of atoms generated does'
       IF(iprint)WRITE(6,*) ' not equal the number of atoms specified'
       write(6,*) myproc,natoms,m
       call my_mpi_abort(' natoms wrong in Cryst_Gen', INT(myproc))
    endif

    IF(iprint)WRITE(6,'(A,3I4,I9,I5)')                                             &
         'Created Lattice with nx ny nz natoms nvac',                     &
         ncells(1),ncells(2),ncells(3),natoms,nvac

  end subroutine Cryst_Gen

  !***********************************************************************
  ! Add an atom to the list, if appropriate
  !***********************************************************************
  subroutine Add2list(x0,index,atomindex,atype,                         &
       xin1,xin2,xin3,typein,changeprob,changeto,                       &
       amp,box,pbc,nvac,vaczmin,                                        &
       natoms,initemp,tdebye)
    use my_mpi
    use defs

    use para_common
    use random
    implicit none

    !     ------------------------------------------------------------------
    !          Variables passed in and out
    real*8, intent(in) :: xin1,xin2,xin3
    real*8, intent(in) :: amp,box(3),pbc(3),vaczmin
    real*8, intent(out) ::x0(*)
    integer, intent(out) :: atype(*),atomindex(*),nvac,natoms,typein
    integer, intent(in) :: index
    real*8, intent(in) :: initemp,tdebye,changeprob
    integer, intent(in) :: changeto

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     rmn(1..2),rmx(1..2) = x&y min&max boundaries for my nodes region
    !     ------------------------------------------------------------------
    !          Local variables and constants
    real*8 x1,x2,x3
    real*8 r

    integer i,m3,vacproc

    integer nvaccr
    data nvaccr /0/
    save nvaccr

    real*8 vacfact

    integer nvaccreatednow
    !     ------------------------------------------------------------------

    !                  Random movement from ideal positions

    x1=xin1
    x2=xin2
    x3=xin3
    call fix_periodic(half, pbc(1), x1)
    call fix_periodic(half, pbc(2), x2)
    call fix_periodic(half, pbc(3), x3)

    nvaccreatednow=0
    vacproc=-1

    vacfact=1.4 !TODO: what? Magic number? Magic algorithm?
    if (vaczmin > -box(3)/2.0) then
       vacfact = vacfact * box(3) / (box(3) / 2.0 - vaczmin)
    endif

    if (x1 .gt. rmn(1) .and. x1 .le. rmx(1) .and.                         &
         x2 .gt. rmn(2) .and. x2 .le. rmx(2) ) then

       if (nvac .gt. 0) then
          !     
          !           Create a vacancy. UGLY ugly ugly... (but works mostly :-) )
          !
          if (nvaccr .lt. nvac) then 
             if (MyRanf(-1) < vacfact*nvac/natoms .and.                   &
                  x3*box(3) > vaczmin ) then
                nvaccr=nvaccr+1
                write(6,'(A,I4,A,I3,A,3F9.2)') 'Created vacancy #',       &
                     nvaccr,' on proc',myproc,' at',x1,x2,x3
                !                  write(7,'(A,I4,A,I3,A,3F9.2)') 'Created vacancy #',
                !     *                 nvaccr,' on proc',myproc,' at',x1,x2,x3
                nvaccreatednow=1
                vacproc=myproc
                 
                goto 99
             endif
          endif
       endif

       call displaceatom(x1,x2,x3,box,                                    &
            1,amp,initemp,tdebye)

       myatoms=myatoms+1
       if (myatoms .gt. NPMAX) then
          write(6,*) 'myatoms > NPMAX in Add2list',myatoms
           
          call my_mpi_abort('NPMAX in Add2list', INT(myproc))
       endif
       m3=3*myatoms-3
       x0(m3+1)=x1
       x0(m3+2)=x2
       x0(m3+3)=x3
       atype(myatoms)=typein
       if (changeprob>zero) then
          r=MyRanf(-1)
          if (r<changeprob) then
             atype(myatoms)=changeto
          endif
       endif
       atomindex(myatoms)=index
    endif

    !     Share value of nvaccr with all processors. Since
    !     it was increased in one processor only, IMAX will
    !     get it's value distributed everywhere
    !
99  if (nvac > 0) then
       call my_mpi_isum(nvaccreatednow, 1)

       if (nvaccreatednow == 1) then
          call my_mpi_imax(nvaccr, 1)
       else if (nvaccreatednow > 1) then
          print *,'NVACCREATED HORROR ERROR',nvaccreatednow,myproc
          call my_mpi_abort('NVACCR', INT(myproc))
       endif
    endif


  end subroutine Add2list

  !Fixed the type of the variables 18.3.2013
  subroutine fix_periodic(half, pbc, x1)
      implicit none
      real*8 :: half
      real*8, intent(in) :: pbc
      real*8 :: x1

      if (x1 .ge. half) then
         x1=x1-pbc
      elseif (x1 .lt. -half) then
         x1=x1+pbc
      endif
  end subroutine



  !***********************************************************************
  ! Create an initial atom displacement according to tdebye or amp
  !***********************************************************************
  subroutine displaceatom(x,y,z,box,                                    &
       atype,amp,initemp,tdebye)

    use typeparam

    use PhysConsts

    use defs

    use para_common
    use random
    implicit none

!include 'para_common.f90'

    real*8 x,y,z,box(3),amp,initemp,tdebye
    real*8 tdebye_K,sd
    integer i,atype
    logical, save :: firsttime=.true.
    logical, allocatable, save :: firstthistype(:)

!    real*8, external :: MyRanf !,gasdev

    if (firsttime) then
       firsttime=.false.
       allocate(firstthistype(itypelow:itypehigh))
       do i=itypelow,itypehigh
          firstthistype(i)=.true.
       enddo
    endif

    if (atype<0) return

    if (amp == 0.0d0 .and. tdebye==0.0d0) return

    if (tdebye /= 0.0) then
       if (initemp > 0.0) then
          tdebye_K=tdebye*invkBeV
          ! 20.89 is sqrt(9 hbar^2/k_B u) 
          ! Expression comes from second order approximation of a nasty
          ! elliptic integral
          sd=20.89*SQRT(initemp*invkBeV/                                &
               mass(ABS(atype)))/(SQRT(3.0)*tdebye_K)

          if (firstthistype(atype)) then
             firstthistype(atype)=.false.
             IF(iprint)WRITE (6,'(A,F13.6,A,I3)')                                  &
                  'Giving Gaussian Debye displacements with sigma',sd,    &
                  ' for atype',atype
          endif
          x=x+sd*gasdev(0)/box(1)
          y=y+sd*gasdev(0)/box(2)
          z=z+sd*gasdev(0)/box(3)
       endif
    else
       if (firsttime) then
          firsttime=.false.
          IF(iprint)WRITE (6,'(A,F13.6)')                                          &
               'Giving amp displacements with max. displ.',amp
       endif
       x=x+amp*(MyRanf(-1)-half)/box(1)
       y=y+amp*(MyRanf(-1)-half)/box(2)
       z=z+amp*(MyRanf(-1)-half)/box(3)
    endif

    return

  end subroutine displaceatom
