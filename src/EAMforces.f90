  
  
  !***********************************************************************
  ! EAM density calculation - for SANDIA, NRL, and etp versions
  !***********************************************************************
  !
  ! KN  See file README.forcesubs for some notes on what these do
  !

  subroutine Calc_P(P, x0,atype,box,pbc,                                &
       npairs,nborlist,atpassflag,nngbrproc,rcutmax,spline,eamnbands,moviemode)

    use defs
    use typeparam
    use datatypes
    use my_mpi


    use para_common

    implicit none
    !
    !     From input x0 (from here and other nodes), return density P
    !     for all atom pairs which interact by EAM
    ! 

    !     ------------------------------------------------------------------
    !          Variables passed in and out
    real*8 P(NPMAX,EAMBANDMAX), x0(*),box(3),pbc(3),rcutmax
    integer npairs,atype(*)
    integer*4 nborlist(*)
    integer atpassflag(*),nngbrproc(*),spline,eamnbands

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     mxat = the maximum number of atoms on any node
    !     buf, ibug, pbuf = buffer space for information from other nodes
    !     xsendbuf, isendbuf = temporary passing buffers
    !     ------------------------------------------------------------------
    !          Local variables and constants
    integer j,jj,mij,nbytes,nbr,nnbors,oP,nadr
    real*8 rs,r,xp(3),den,dend,Rcuts,boxs(3),Psum

    integer,parameter:: max_active_ngbrs=2000
    real*8 :: ar_r(max_active_ngbrs)
    integer ar_j(max_active_ngbrs)
    integer active_ngbrs

    integer moviemode

    integer m,i3,i2,j3,j2,ndest,i
    integer rcv_est
    real*8 t1,dttimer

    integer d,rd,ii,ifrom,myi,dfrom,np0
    real*8 psumlocal,psumpassed

    !          EAM parameters for inlined calc_den()

    common /EAM_params/ rcutpot,dr,dri,nr,nP,dP
    common/EAM_P_params/P_r(EAMTABLESZ),dPdr_r(EAMTABLESZ),Psc(EAMTABLESZ)

    integer klo, khi
    real*8 a, b, ab, yplo, yphi, ylo, yhi

    integer np,nr
    real*8 dpdr_r,p_r,x,psc,dr,rcutpot,dri,dP
    real*8 percut2

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    real*8 halfcorr

    !     ------------------------------------------------------------------

    !
    !  Note that we must pass around P information as well here, not only
    !  x info ! This is because not all atom pairs are included in .f90e P calc.
    !
    !  Checked that this works by comparing psumlocal and psumpassed with
    !  previous version of code. Identical results for 4, 8 and 16 processors !
    !

    halfcorr=one
    if (moviemode == 15 .or. moviemode == 5 .or. moviemode == 6 .or. moviemode == 16 .or. moviemode == 17 .or. moviemode == 18) then
       halfcorr=half
    endif

    Rcuts=rcutpot*rcutpot
    do i=1,3
       boxs(i)=box(i)**2
       !         print *,'Calc_P loop 1',myproc
    enddo
    do i=1,mxat
       P(i,1)=zero
    enddo
    do i=1,3*np0pairtable
       pbuf(i)=zero
    enddo
    do i=1,np0pairtable
       dirbuf(i)=0
       i2=i*2
       if (i <= myatoms) then
          ibuf(i2-1)=i
          ibuf(i2)=atype(i)
       else
          ibuf(i2-1)=0
          ibuf(i2)=0
       endif
    enddo
    !
    !  Pass and receive atoms to different directions.
    !  The passing of atoms in this piece of code should be identical 
    !  in the neighbour list and force calculations.
    !  
    !  In principle the idea is that the input is the local x0(myatoms) 
    !  coordinates, and the output is buf(np0), which contains the 
    !  coordinates of the atoms both in this node and within cut_nei
    !  from this in the adjacent nodes.
    !
    !  The atoms which get passed here must be exactly the
    !  same and in the same order in the force calculations
    !  as in the original neighbour list calculation !
    !
    !  To be able to pass back the P information of j pairs,
    !  we also pass and receive the atom indices and direction information
    !  of the atoms in j pairs.
    !

    IF(debug)PRINT *,'EAM 1'
    do i=1,3*myatoms
       buf(i)=x0(i)
    enddo
    np0=myatoms

    if (nprocs > 1) then
       t1=mpi_wtime()
       !     Loop over directions
       do d=1,8
          rd=MOD(d+4,8) !recieve from rd and send to d
          if(rd==0) rd=8

          !WRITE(*,*) "send",j,d,rd

          j=0
          do i=1,myatoms
             !fill buffers with data we want to send to neighbour d
             if (IAND(atpassflag(i),passbit(d)) .ne. 0) then
                j=j+1
                if (j .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow1', INT(myproc))
                i3=i*3-3
                j3=j*3-3
                j2=j*2-2
                xsendbuf(j3+1)=x0(i3+1)
                xsendbuf(j3+2)=x0(i3+2)
                xsendbuf(j3+3)=x0(i3+3)
                isendbuf(j2+1)=i
                isendbuf(j2+2)=atype(i)
             endif
          enddo

          call mpi_sendrecv(j, 1, my_mpi_integer, nngbrproc(d), d, &
               jj,1, my_mpi_integer,nngbrproc(rd), d, mpi_comm_world, &
               mpi_status_ignore, ierror)

          call mpi_sendrecv(xsendbuf, j*3, mpi_double_precision, nngbrproc(d), d+8, &
               buf(np0*3+1),jj*3, mpi_double_precision,nngbrproc(rd), d+8, mpi_comm_world, &
               mpi_status_ignore, ierror)

          call mpi_sendrecv(isendbuf, j*2, my_mpi_integer,nngbrproc(d), d+16, &
               ibuf(np0*2+1),jj*2, my_mpi_integer,nngbrproc(rd), d+16, mpi_comm_world, &
               mpi_status_ignore, ierror)


          do i=1,jj
             dirbuf(np0+i)=d
          enddo
          np0=np0+jj
       enddo
       tmr(15)=tmr(15)+(mpi_wtime()-t1)
    endif

    if (np0 .ne. np0pairtable) then
       write (6,*) 'EAMforces np0 problem ',myproc,np0,np0pairtable
    endif
    !      print *,'Node received totally ',myproc,np0-myatoms

    !
    !  Step 1: get P for atom pairs indexed in my node
    !

    !      print *,myproc,nprocs
    IF(debug)PRINT *,'EAM 2'

    mij=0
    psumlocal=0.0d0
    psumpassed=0.0d0
    percut2=MIN(0.25*boxs(1),0.25*boxs(2),0.25*boxs(3))
    !check EAM table size, enough to do it once, as Rcut is the max value that r can get
    if (SQRT(Rcuts)*dri+2 >EAMTABLESZ)                                          &
         call my_mpi_abort('Potential EAM overflow', INT(Rcuts*dri)+2)

    do i=1,myatoms
       i3=3*i-3
       mij = mij + 1
       nnbors = nborlist(mij)
       active_ngbrs=0
       !check here, as a check inside would cost performance
       if (nnbors>max_active_ngbrs)                                          &
            call my_mpi_abort('Potentially too many active ngbrs in EAM, increase max_active_ngbrs', INT(nnbors))
       !for performance reasons we have fissioned the loop here, one for
       !computing active r,j pairs, and one for density

       do nbr=1,nnbors
          mij=mij+1
          j=nborlist(mij)
          ! Handle only EAM here 
          if (iac(ABS(ibuf(i*2)),ABS(ibuf(j*2))) /= 1) cycle
          j3=3*j-3

          xp(1)=buf(i3+1)-buf(j3+1)
          xp(2)=buf(i3+2)-buf(j3+2)
          xp(3)=buf(i3+3)-buf(j3+3)

          rs=xp(1)*xp(1)*boxs(1)+                                         &
               xp(2)*xp(2)*boxs(2)+                                       &
               xp(3)*xp(3)*boxs(3)

          if (rs .ge. percut2 ) then
             if (xp(1) .ge.  half) xp(1)=xp(1)-pbc(1)
             if (xp(1) .lt. -half) xp(1)=xp(1)+pbc(1)
             if (xp(2) .ge.  half) xp(2)=xp(2)-pbc(2)
             if (xp(2) .lt. -half) xp(2)=xp(2)+pbc(2)
             if (xp(3) .ge.  half) xp(3)=xp(3)-pbc(3)
             if (xp(3) .lt. -half) xp(3)=xp(3)+pbc(3)
             rs=xp(1)*xp(1)*boxs(1)+                                         &
                  xp(2)*xp(2)*boxs(2)+                                       &
                  xp(3)*xp(3)*boxs(3)
          end if

          if (rs < Rcuts) then
             active_ngbrs=active_ngbrs+1
             ar_r(active_ngbrs)=SQRT(rs)
             ar_j(active_ngbrs)=j
          end if
       end do



       !second part of fissioned loop            
       do nbr=1,active_ngbrs
          x= ar_r(nbr)*dri
          j= ar_j(nbr)

          klo = INT(x) + 1
          khi = klo + 1

          a=DBLE(khi-1)-x
          b=one-a

          ylo=P_r(klo)*a
          yhi=P_r(khi)*b

          ab=a*b
          yplo=dPdr_r(klo)*a
          yphi=dPdr_r(khi)*b
          if (spline==1) then
             den=a*ylo+b*yhi+ab*(two*(ylo+yhi)+dr*(yplo-yphi))
          else
             den=ylo+yhi
          end if

          pbuf(i)=pbuf(i)+den*halfcorr
          pbuf(j)=pbuf(j)+den*halfcorr
       enddo

    enddo

    do j=1,myatoms
       P(j,1)=P(j,1)+pbuf(j)
       psumlocal=psumlocal+pbuf(j)
    enddo

    IF(debug)PRINT *,'EAM 3'
    !
    !  Step 2: Send and get p information to/from adjacent nodes
    !
    !  From the array dirbuf, we can figure out from which direction an
    !  atom has been received, and from ibuf what index it has had there. Thus
    !  we can send the pbuf and atom index information to other nodes,
    !  and when receiving it know which atom it belongs to.
    !
    i=0
    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !         print *,'Calc_P loop 2, pbuf sendrecv',myproc
       !     Loop over directions
       do d=1,8
          rd=MOD(d+4,8) !recieve from rd and send to d
          if(rd==0) rd=8

          !     Loop over neighbours
          i=0
          do j=myatoms+1,np0
             dfrom=dirbuf(j)+4
             if (dfrom .gt. 8) dfrom=dfrom-8
             if (d .eq. dfrom) then
                i=i+1
                if (i .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow2', INT(myproc))
                ifrom=ibuf(j*2-1)
                isendbuf(i)=ifrom
                psendbuf(i)=pbuf(j)
                psumpassed=psumpassed+pbuf(j)
             endif
          enddo

          call mpi_sendrecv(i, 1, my_mpi_integer, nngbrproc(d), d, &
               ii,1, my_mpi_integer,nngbrproc(rd), d, mpi_comm_world, &
               mpi_status_ignore, ierror)

          call mpi_sendrecv(isendbuf,i, my_mpi_integer,nngbrproc(d), d+16, &
               isendbuf2,ii, my_mpi_integer,nngbrproc(rd), d+16, mpi_comm_world, &
               mpi_status_ignore, ierror)
          call mpi_sendrecv(psendbuf,i,  mpi_double_precision, nngbrproc(d), d+8, &
               psendbuf2,ii, mpi_double_precision,nngbrproc(rd), d+8, mpi_comm_world, &
               mpi_status_ignore, ierror)

          i=ii

          do ii=1,i
             myi=isendbuf2(ii)
             if (myi .gt. myatoms) then
                print *,myproc,myatoms,myi,ii,i
                call my_mpi_abort('CALC_P i too large', INT(myproc))
             endif
             P(myi,1)=P(myi,1)+psendbuf2(ii)
          enddo
       enddo
       tmr(15)=tmr(15)+(mpi_wtime()-t1)
    endif

    if (debug) then
       write (6,99) 'For proc',myproc,' local rho sum',psumlocal,         &
            ' passed rho sum',psumpassed
99     format (A,I3,A,G13.6,A,G13.6)
    endif

    IF(debug)PRINT *,'EAM 4'

     

  end subroutine Calc_P

  !***********************************************************************
  ! EAM F[p(r)] calculation
  !***********************************************************************
  subroutine Calc_Fp(atype,Fp,dFpdp,Vmany, P,spline,Fpextrap,eamnbands)
    use my_mpi
    use defs

    use para_common

    implicit none

    !
    !     For input rho array P, return F[p] and d(F[p])/dp
    !

    !      IMPLICIT REAL*8  (a-h,o-z)
    !      IMPLICIT INTEGER (i-n)

    !     ------------------------------------------------------------------
    !          Variables passed in and out
    real*8 Fp(NPMAX,EAMBANDMAX),dFpdp(NPMAX,EAMBANDMAX),Vmany, P(NPMAX,EAMBANDMAX)
    ! Atype include only for compatibility wi.f90 parcas_eamal, 
    ! not used here !
    integer atype(*)


    !          EAM parameters
    common /EAM_params/ rcutpot,dr,dri,nr,nP,dP
    common /EAM_Fp_params/ Pa(EAMTABLESZ),F_p(EAMTABLESZ),                &
         dFdp_p(EAMTABLESZ),Fsc(EAMTABLESZ)

    integer np,nr,spline,Fpextrap,eamnbands
    real*8 f_p,pa,fsc,dfdp_p,dr,rcutpot,dri,dP
    !     ------------------------------------------------------------------

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     ------------------------------------------------------------------

    integer i,i1
    integer khi,klo
    real*8 a,b,h,ab,ylo,yhi,yplo,yphi,x,xx
    logical overflow,underflow

    Vmany=zero

    do i=1,myatoms

       ! Inlined:
       ! CALL Splt2(nP,Pa,F_p,dFdp_p,Fsc,P(i,1),Fp(i,1),dFpdp(i,1))
       ! call Calc_F(Fp(i),dFpdp(i), P(i,1))

       if (P(i,1)==zero) then
          ! Must be impurity or sputtered atom
          Fp(i,1)=zero
          dFpdp(i,1)=zero
          cycle
       endif

       x=P(i,1)

       xx=P(i,1)/dP
       klo = INT(xx) + 1
       khi = klo + 1
       overflow=.false.

       underflow=.false.
       if (x < zero) then
	  print *,'EAM negative electron density??',i,x
	  if (Fpextrap/=1) then
	     print *,'... proceeding with rho=zero.'
	     x=zero
	     xx=zero
	     klo=1
	     khi=2
	  else
	     print *,'... doing downwards linear extrapolation.'
	     klo=1
	     khi=2
             underflow=.true.
	  endif
       endif

       if (khi>nP) then 
          print *,'EAM Fp overflow',i,P(i,1),dP,nP,khi
          if (Fpextrap==1) then
             ! This will effect a linear extrapolation
             khi=nP
             klo=khi-1
             overflow=.true.
          else
             call my_mpi_abort('EAM Fp overflow', INT(khi))
          endif
       endif

       h=dP
       a=((khi-1)*dP-x)/dP
       b=one-a

       ylo=F_p(klo)*a
       yhi=F_p(khi)*b

       if (spline==1 .and. .not. overflow .and. .not. underflow) then
          ab=a*b
          yplo=dFdp_p(klo)*a
          yphi=dFdp_p(khi)*b

          Fp(i,1)=a*ylo+b*yhi+ab*(two*(ylo+yhi)+h*(yplo-yphi))

          dFpdp(i,1)=ab*Fsc(klo)+yplo+yphi
       else
          Fp(i,1)=ylo+yhi
          dFpdp(i,1)=(F_p(khi)-F_p(klo))/dP
       endif
       ! write (93,*) i,P(i,1),Fp(i,1),dFpdp(i,1)

       Vmany = Vmany + Fp(i,1)
    enddo

     

  end subroutine Calc_Fp

  !***********************************************************************
  ! Calculate the force on each atom
  !***********************************************************************

  subroutine Calc_Force(xnp,Epair,Vpair,wxx,wyy,wzz,wxxi,wyyi,wzzi,     &
       x0,atype,dFpdp,box,pbc,npairs,nborlist,                          &
       wxxsh,wyysh,wzzsh,natsh,nsh,                                     &
       wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,		&
       atpassflag,nngbrproc,ECM,rcutmax,spline,eamnbands,               &
       moviemode,movietime_val,calc_avgvir,last_step_predict)

    use defs
    use typeparam
    use datatypes
    use my_mpi

    use para_common

    implicit none

    !
    ! Force calculation: The force xnp(i) obtained is scaled by 1/box,
    ! i.e. if F is the real force in units of eV/A, 
    !
    !  xnp(i) = F(i)/box(idim) 
    !
    !     ------------------------------------------------------------------
    !          Variables passed in and out
    integer atype(*),npairs,spline,eamnbands
    integer*4 nborlist(*)
    real*8 xnp(*),Epair(*),Vpair, x0(*),dFpdp(NPMAX,EAMBANDMAX),rcutmax,box(3),pbc(3)
    real*8 wxx,wyy,wzz,wxxi(*),wyyi(*),wzzi(*)

    real*8 wxy,wxyi(*),wxz,wxzi(*),wyx,wyxi(*),wyz,wyzi(*),wzx,wzxi(*),wzy,wzyi(*)
    integer moviemode
    logical movietime_val
    logical calc_vir
    logical calc_avgvir
    logical last_step_predict

    integer nsh
    integer natsh(*)
    real*8 wxxsh(*),wyysh(*),wzzsh(*),ECM(3),dx,dy,dz

    integer atpassflag(*),nngbrproc(*)

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     mxat = the maximum number of atoms on any node
    !     buf(8*NPMAX) = buffer space for information from other nodes
    !     ------------------------------------------------------------------
    !          Local variables and constants
    integer nbr,nnbors,j,jj,mij,nadr,nbytes
    real*8 V_ij,Vp_ij,vij2,r,ir,rs,xp(6),rcutpot,rcutpots,boxs(3)
    integer mijptr

    integer j3,j2,i3,i2,m,i,ndest,nsend,itype,jtype
    real*8 tmp,dend,t1,dttimer,maxr
    real*8 rcutmaxs,dummy1,dummy2

    integer nnsh

    integer d,ii,ifrom,myi,dfrom,np0,ii3
    !
    !  Parameters for inlined calc_dend and calc_pair
    !
    real*8 x,a,b,ab
    real*8 ylo,yhi,yplo,yphi

    integer,parameter:: max_active_ngbrs=2000
    real*8 :: ar_r(max_active_ngbrs)
    real*8 :: ar_xp(3,max_active_ngbrs)
    integer ar_j(max_active_ngbrs),ar_iac(max_active_ngbrs)
    integer active_ngbrs,rd

    integer klo,khi,nP,nr
    real*8 dr,dri,dP
    real*8 dVpdr_r,Vp_r,Vpsc
    real*8 P_r,dPdr_r,Psc
    real*8 percut2

    common/EAM_params/rcutpot,dr,dri,nr,nP,dP
    common/EAM_P_params/P_r(EAMTABLESZ),dPdr_r(EAMTABLESZ),Psc(EAMTABLESZ)
    common/EAM_pair_params/Vp_r(EAMTABLESZ),dVpdr_r(EAMTABLESZ),Vpsc(EAMTABLESZ)

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    real*8 halfcorr

    halfcorr=one
    if (moviemode == 15 .or. moviemode == 5 .or. moviemode == 6 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
       halfcorr=half
    endif


    !     ------------------------------------------------------------------

    !
    !     For paralell passing routines, using the same pbuf as calc_P for
    !     forces to prevent excessive waste of memory
    !
    !     How this used to work:
    !
    !     ipass=0:     Get Epair(i) and xnp(i) for my local atom pairs
    !     ipass=1:     1� Send and receive my atom coordinates and dFpdp
    !                  2� Calculate Epair and xnp into buf
    !     ipass=2:     1� Send and receive atoms, dFpdp, Epair, xnp
    !     to npass-1   2� Calculate Epair and xnp into buf
    !     ipass=npass: 1� Send final Epair and xnp.
    !
    !     How this now works:
    !
    !     � First sendrecv x0, atom indices and dFpdp of atoms in my node
    !     � Calculate all Epairs and forces  
    !     � Sendrecv Epair and forces for atoms not in my node
    !     � Sum up the received Epairs and forces in correct nodes
    !
    !     Passing buffers (all defined in para_common.h):
    !
    !     atom coords       - buf()
    !     atom indices      - ibuf()     | interlaced, index at i*2-1
    !     atom types        - ibuf()     | atype at i*2
    !     derivatives dFpdp - dbuf()
    !     forces xnp        - pbuf()
    !     energies Epair    - ebuf()
    !

    rcutpots=rcutpot*rcutpot
    rcutmaxs=rcutmax*rcutmax
    do i=1,3
       boxs(i)=box(i)**2
    enddo
    maxr=SQRT(3.0)*0.5

    calc_vir = .false.
    if (movietime_val .or. calc_avgvir .or. last_step_predict) then
     if (moviemode == 15 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
      calc_vir = .true.
     endif
    endif

    ! Initialize forces

    do i=1,mxat
       Epair(i)=zero
       wxxi(i)=zero
       wyyi(i)=zero
       wzzi(i)=zero

       if (calc_vir) then
          wxyi(i)=zero
          wxzi(i)=zero
          wyxi(i)=zero
          wyzi(i)=zero
          wzxi(i)=zero
          wzyi(i)=zero
       endif

    enddo
    do i=1,3*mxat
       xnp(i)=zero
    enddo
    Vpair=zero
    wxx=zero
    wyy=zero
    wzz=zero

    if (calc_vir) then
       wxy=zero
       wxz=zero
       wyx=zero
       wyz=zero
       wzx=zero
       wzy=zero
    endif

    ! Disabled, as also the computation with these is disabled
    !      DO j=1,nsh
    !         wxxsh(j)=zero
    !         wyysh(j)=zero
    !         wzzsh(j)=zero
    !         natsh(j)=0
    !      ENDDO


    do i=1,np0pairtable
       !         dirbuf(i)=0
       if (i .le. myatoms) then
          dbuf(i)=dFpdp(i,1)
          !            ibuf(i*2-1)=i
          !            ibuf(i*2)=atype(i)
       else
          dbuf(i)=zero
          !            ibuf(i*2-1)=0
          !            ibuf(i*2)=0
       endif
       ebuf(i)=zero
    enddo

    do i=1,3*np0pairtable
       !         if (i .le. 3*myatoms) then
       !            buf(i)=x0(i)
       !         else
       !            buf(i)=zero
       !         endif
       pbuf(i)=zero
    enddo


    !
    !  Pass and receive atoms to different directions.
    !  The passing of atoms in this piece of code should be identical 
    !  in the neighbour list and force calculations.
    !  
    !  In principle the idea is that the input is the local x0(myatoms) 
    !  coordinates, and the output is buf(np0), which contains the 
    !  coordinates of the atoms both in this node and within cut_nei
    !  from this in the adjacent nodes.
    !
    !  The atoms which get passed here must be exactly the
    !  same and in the same order in the force calculations
    !  as in the original neighbour list calculation !
    !
    !  To be able to pass back the force information of j pairs,
    !  we also pass and receive the atom indices and direction information
    !  of the atoms in j pairs.
    !
    !  NOTE: most passing buffers have already correct contents from
    !  calc_P, so no need to redo passing here !
    !
    call mpi_barrier(mpi_comm_world, ierror)

    np0=myatoms
    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !         print *,'Calc_Force loop 1',myproc
       !*     Loop over directions
       do d=1,8
          !            print *,myproc,d,myatoms
          rd=MOD(d+4,8) !recieve from rd and send to d
          if(rd==0) rd=8
          j=0
          do i=1,myatoms
             if (IAND(atpassflag(i),passbit(d)) .ne. 0) then
                j=j+1
                if (j .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow', INT(myproc))
                psendbuf(j)=dFpdp(i,1)

             endif
          enddo

          call mpi_sendrecv(j, 1, my_mpi_integer, nngbrproc(d), d, &
               jj,1, my_mpi_integer,nngbrproc(rd), d, mpi_comm_world, &
               mpi_status_ignore, ierror)

          call mpi_sendrecv(psendbuf, j  , mpi_double_precision, nngbrproc(d), d+8, &
               dbuf(np0+1),jj, mpi_double_precision,nngbrproc(rd), d+8, mpi_comm_world, &
               mpi_status_ignore, ierror)

          np0=np0+jj
       enddo
       tmr(17)=tmr(17)+(mpi_wtime()-t1)
    endif

    if (np0 .ne. np0pairtable) then
       write (6,*) 'Calc_Force np0 problem ',myproc,np0,np0pairtable
    endif
    !      print *,'Node received totally ',myproc,np0-myatoms

    !
    !  Step 1: get Epair and xnp for all atom pairs 
    !
    percut2=MIN(0.25*boxs(1),0.25*boxs(2),0.25*boxs(3))
    !check beforhand worst case EAM overflow case
    if (SQRT(rcutpots)*dri+2 >EAMTABLESZ)                                          &
         call my_mpi_abort('Potential EAM overflow', INT(rcutpots*dri)+2)

    mij=0
    do i=1,myatoms
       i3=3*i-3
       mij = mij + 1
       nnbors = nborlist(mij)
       active_ngbrs=0
       !check here, as a check inside would cost performance
       if (nnbors>max_active_ngbrs)                                          &
            call my_mpi_abort('Potentially too many active ngbrs in EAM, increase max_active_ngbrs', INT(nnbors))


       ! disabled as the computation of wxxsh stuff is disabled
       !         dx=buf(i3+1)-ECM(1)/box(1)
       !         dy=buf(i3+2)-ECM(2)/box(2)
       !         dz=buf(i3+3)-ECM(3)/box(3)
       !         r=SQRT(dx*dx+dy*dy+dz*dz)
       !         nnsh=INT(1.0d0+r/maxr*(1.0d0*nsh-1.0d0))

       itype=ABS(ibuf(i*2))
       do nbr=1,nnbors
          mij=mij+1
          j=nborlist(mij)


          jtype=ABS(ibuf(j*2))

          if (iac(itype,jtype) /= 1) then
             ! Handle other interaction types
             if (iac(itype,jtype) == 0) cycle
             if (iac(itype,jtype) == -1) then
                write (6,*) 'ERROR: IMPOSSIBLE INTERACTION'
                write (6,*) myproc,i,j,itype,jtype
                call my_mpi_abort('INTERACTION -1', INT(myproc))
             endif
          endif

          j3=j*3-3

          xp(1)=buf(i3+1)-buf(j3+1)
          xp(2)=buf(i3+2)-buf(j3+2)
          xp(3)=buf(i3+3)-buf(j3+3)

          rs=xp(1)*xp(1)*boxs(1)+                                         &
               xp(2)*xp(2)*boxs(2)+                                       &
               xp(3)*xp(3)*boxs(3)

          if (rs .ge. percut2 ) then
             if (xp(1) .ge.  half) xp(1)=xp(1)-pbc(1)
             if (xp(1) .lt. -half) xp(1)=xp(1)+pbc(1)
             if (xp(2) .ge.  half) xp(2)=xp(2)-pbc(2)
             if (xp(2) .lt. -half) xp(2)=xp(2)+pbc(2)
             if (xp(3) .ge.  half) xp(3)=xp(3)-pbc(3)
             if (xp(3) .lt. -half) xp(3)=xp(3)+pbc(3)
             rs=xp(1)*xp(1)*boxs(1)+                                         &
                  xp(2)*xp(2)*boxs(2)+                                       &
                  xp(3)*xp(3)*boxs(3)
          end if

          if (rs < rcutmaxs) then
             if (iac(itype,jtype) == 2 .or. rs < rcutpots) then
                active_ngbrs=active_ngbrs+1
                ar_r(active_ngbrs)=SQRT(rs)
                ar_j(active_ngbrs)=j
                ar_xp(:,active_ngbrs)=xp(1:3)
                ar_iac(active_ngbrs)=iac(itype,jtype)
             end if
          end if
       end do

       !second part of fissioned loop            
       do nbr=1,active_ngbrs
          r=ar_r(nbr)
          j= ar_j(nbr)

          ! Force calc
          if (ar_iac(nbr) == 2) then
             V_ij=zero; tmp=zero;
             jtype=ABS(ibuf(j*2))

             call splinereppot(r,V_ij,tmp,1d20,1d20,itype,jtype,dummy1,dummy2)
             tmp=tmp/r
          else 

             x=r*dri
             klo = INT(x) + 1
             khi = klo + 1


             a=DBLE(khi-1)-x
             ylo=Vp_r(klo)*a
             yhi=Vp_r(khi)*(1.0d0-a)

             if (spline==1) then
                !ab=a*b

                dend=a*(1.0d0-a)*Psc(klo)+ dPdr_r(klo)*a+ &
                     dPdr_r(khi)*(1.0d0-a)

                yplo=dVpdr_r(klo)*a
                yphi=dVpdr_r(khi)*(1.0d0-a)
                V_ij=a*ylo+(1.0d0-a)*yhi+a*(1.0d0-a)*(2.0*(ylo+yhi)+dr*(yplo-yphi))
                Vp_ij=a*(1.0d0-a)*Vpsc(klo)+yplo+yphi

             else
                dend=(P_r(khi)-P_r(klo))/dr                
                V_ij=ylo+yhi
                Vp_ij=(Vp_r(khi)-Vp_r(klo))/dr
             endif

             tmp=halfcorr*(dend*(dFpdp(i,1)+dbuf(j))+Vp_ij)/r
             !print *,i,j,tmp,dend,dFpdp(i,1),dbuf(j),Vp_ij,r
          endif

          !print *,r,V_ij,Vp_r(klo),Vp_r(khi)

          vij2=V_ij*half*halfcorr
          ebuf(i)=ebuf(i)+vij2
          ebuf(j)=ebuf(j)+vij2
          !KN
          !  This calculates the force in the right direction, and scales with box size
          xp(1:3)=ar_xp(:,nbr)
          xp(4)=xp(1)*tmp
          xp(5)=xp(2)*tmp
          xp(6)=xp(3)*tmp
          xnp(i3+1)=xnp(i3+1)-xp(4)
          xnp(i3+2)=xnp(i3+2)-xp(5)
          xnp(i3+3)=xnp(i3+3)-xp(6)
          j3=3*j-3
          pbuf(j3+1)=pbuf(j3+1)+xp(4)
          pbuf(j3+2)=pbuf(j3+2)+xp(5)
          pbuf(j3+3)=pbuf(j3+3)+xp(6)

          wxxi(i)=wxxi(i)-xp(4)*xp(1)
          wyyi(i)=wyyi(i)-xp(5)*xp(2)
          wzzi(i)=wzzi(i)-xp(6)*xp(3)

          if (calc_vir) then
             wyxi(i)=wyxi(i)-xp(5)*xp(1)
             wzxi(i)=wzxi(i)-xp(6)*xp(1)
             wxyi(i)=wxyi(i)-xp(4)*xp(2)
             wzyi(i)=wzyi(i)-xp(6)*xp(2)
             wxzi(i)=wxzi(i)-xp(4)*xp(3)
             wyzi(i)=wyzi(i)-xp(5)*xp(3)
          endif

       enddo
       wxx=wxx+wxxi(i)
       wyy=wyy+wyyi(i)
       wzz=wzz+wzzi(i)

       if (calc_vir) then
          wxy=wxy+wxyi(i)
          wxz=wxz+wxzi(i)
          wyx=wyx+wyxi(i)
          wyz=wyz+wyzi(i)
          wzx=wzx+wzxi(i)
          wzy=wzy+wzyi(i)
       endif

       ! SVA: Disabled wxxsh & co computing for peta-parcas
       !         DO j=nsh,nnsh,-1
       !            wxxsh(j)=wxxsh(j)+wxxi(i)
       !            wyysh(j)=wyysh(j)+wyyi(i)
       !            wzzsh(j)=wzzsh(j)+wzzi(i)
       !            natsh(j)=natsh(j)+1
       !         ENDDO
    enddo
    ! SVA: Disabled wxxsh & co computing for peta-parcas
    !      CALL my_mpi_dsum(wxxsh, nsh)
    !      CALL my_mpi_dsum(wyysh, nsh)
    !      CALL my_mpi_dsum(wzzsh, nsh)
    !      CALL my_mpi_isum(natsh, nsh)

    !
    !  E and xnp for atom pairs in this node
    !
    do j=1,myatoms
       Epair(j)=Epair(j)+ebuf(j)
    enddo
    do j=1,3*myatoms
       xnp(j)=xnp(j)+pbuf(j)
    enddo

    !do j=1,myatoms
    !    print *,j,xnp(j*3-3+1),xnp(j*3-3+2),xnp(j*3-3+3)
    !enddo

    !
    !  Step 2: Send and get e and f information to/from adjacent nodes
    !
    !  From the array dirbuf, we can figure out from which direction an
    !  atom has been received, and from ibuf what index it has had there. Thus
    !  we can send the pbuf and atom index information to other nodes,
    !  and when receiving it know which atom it belongs to.
    !
    IF(debug)PRINT *,'Calc_Force sendback',np0

    call mpi_barrier(mpi_comm_world, ierror)
    i=0
    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !         print *,'Calc_P loop 2, pbuf sendrecv',myproc
       !        Loop over directions
       nsend=myatoms
       do d=1,8
          rd=MOD(d+4,8) !recieve from rd and send to d
          if(rd==0) rd=8

          !           Loop over neighbours
          i=0
          do j=myatoms+1,np0
             j3=j*3-3
             dfrom=dirbuf(j)+4
             if (dfrom .le. 4)                                            &
                  call my_mpi_abort('dfrom HORROR ERROR !', INT(myproc))
             if (dfrom .gt. 8) dfrom=dfrom-8
             if (d .eq. dfrom) then
                i=i+1
                if (i .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow2', INT(myproc))
                i3=i*3-3
                ifrom=ibuf(j*2-1)
                isendbuf(i)=ifrom
                psendbuf(i)=ebuf(j)
                xsendbuf(i3+1)=pbuf(j3+1)
                xsendbuf(i3+2)=pbuf(j3+2)
                xsendbuf(i3+3)=pbuf(j3+3)
                nsend=nsend+1
             endif
          enddo

          call mpi_sendrecv(i, 1, my_mpi_integer, nngbrproc(d), d, &
               ii,1, my_mpi_integer,nngbrproc(rd), d, mpi_comm_world, &
               mpi_status_ignore, ierror)

          call mpi_sendrecv(isendbuf,i, my_mpi_integer,nngbrproc(d), d+8, &
               isendbuf2,ii, my_mpi_integer,nngbrproc(rd), d+8, mpi_comm_world, &
               mpi_status_ignore, ierror)

          call mpi_sendrecv(psendbuf,i, mpi_double_precision, nngbrproc(d), d+16, &
               psendbuf2,ii, mpi_double_precision,nngbrproc(rd), d+16, mpi_comm_world, &
               mpi_status_ignore, ierror)

          call mpi_sendrecv(xsendbuf,i*3, mpi_double_precision, nngbrproc(d), d+24, &
               xsendbuf2,ii*3, mpi_double_precision,nngbrproc(rd), d+24, mpi_comm_world, &
               mpi_status_ignore, ierror)

          !        

          i=ii

          do ii=1,i
             myi=isendbuf2(ii)
             if (myi .gt. myatoms) then
                print *,myproc,myatoms,myi,ii,i
                call my_mpi_abort('EAMforce i too large', INT(myproc))
             endif
             ii3=ii*3-3
             i3=myi*3-3
             Epair(myi)=Epair(myi)+psendbuf2(ii)
             xnp(i3+1)=xnp(i3+1)+xsendbuf2(ii3+1)
             xnp(i3+2)=xnp(i3+2)+xsendbuf2(ii3+2)
             xnp(i3+3)=xnp(i3+3)+xsendbuf2(ii3+3)
          enddo
       enddo
       if (nsend .ne. np0) then
          write (6,*) 'WARNING: calc_force not sending out as many'
          write (6,*) 'atoms as it received. Something fishy !?!'
          write (6,*) myproc,myatoms,nsend,np0
       endif

       tmr(17)=tmr(17)+(mpi_wtime()-t1)
    endif

    Vpair=zero
    do i=1,myatoms
       Vpair=Vpair+Epair(i)
    enddo

    call mpi_barrier(mpi_comm_world, ierror)

     

  end subroutine Calc_Force
