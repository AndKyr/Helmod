  
  
  
  !***********************************************************************
  ! Recalculate the pair table every nprtbl steps
  !***********************************************************************
  subroutine Pair_Table(npairs,nabors,nborlist,invnborlist, natoms,x0,                     &
       rcutsq,box,pbc,x0nei,                                            &
       passlimmin,passlimmax,atpassflag,npass,nngbrproc,                &
       atomindex,AnySemi,BrennerBeardmore,SortNeiList,printnow)
    use datatypes
    use my_mpi
    use defs

    use para_common

    implicit none

    !      IMPLICIT REAL*8  (a-h,o-z)
    !      IMPLICIT INTEGER (i-n)

    !     ------------------------------------------------------------------
    !          Variables passed in and out
    integer npairs, natoms
    real*8 x0(*),rcutsq,box(3),pbc(3)
    integer*4 nabors(*)
    integer*4 nborlist(*), invnborlist(*)      
    integer atomindex(*)
    logical AnySemi,SortNeiList,printnow,BrennerBeardmore

    real*8 x0nei(*)

    real*8 passlimmin(2),passlimmax(2),nodemin(3),nodemax(3)

    integer atpassflag(*)
    integer npass(8),nngbrproc(8)

    real*8 dttimer

    !KN   For parallell operation, gets neighbours in any node
    !     for atoms in my node.

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     mxat = the maximum number of atoms on any node
    !     buf(9*NPMAX) = buffer space for information from other nodes
    !     ------------------------------------------------------------------
    !          Local variables and constants
    integer nbytes, np0, mij, myatoms_max
    real*8 xp(3), xs, x, y, boxs(3), rcut(3),distsq,t1,pbcx(3)
    real*8 xsmin

    integer d,dd,i,j,k,i3,j3,ndest,jj
    real*8 npairstot

    integer mijptr

    !     Atom passing local variables

    real*8 xpmax,xpmin,ypmax,ypmin

    !     ------------------------------------------------------------------
    !     Variables for actual neighbour list calculation
    integer nborsize

    real*8 cellsize(3)
    !
    !  Note that it is only necessary to store the buf atoms into the cells
    !
    !  Size of one cell is set to exactly Rneicut.
    !  Cells are distributed so that cell 0 is between 0 and Rneicut.
    !  However, cell size may vary between calls of the subroutine.
    !

    !
    !  Parallell version: only cells really needed are used
    !

    !
    !  Allen-Tildesley linked list parameters
    !
    ! Arrays size in list() is well defined, but that in head() not.
    ! If you have trouble with it, just increase the head size to whatever
    ! constant you need, and modify the test statement below as well.
    !
    integer, save :: head(0:NPMAX/2)
    integer, save :: list(PARRATIO*NPMAX/2)
    integer icell

    !
    !  Other variables
    !

    integer Nix,Niy,Niz
    integer, save :: Nixmin,Niymin,Nizmin
    integer, save :: Nixmax,Niymax,Nizmax

    real*8 rsq
    integer ntime
    data ntime /0/
    integer ix,iy,iz
    integer dix,diy,diz
    integer dixmin,diymin,dizmin
    integer dixmax,diymax,dizmax
    integer inx,iny,inz

    real*8 minhelp,maxhelp,xhelp,yhelp,zhelp,dx,dy,dz
    real*8 Rneicut

    logical firsttime
    data firsttime /.true./
    save firsttime,ntime

    ! Is the cell less than 3 times rneicut in any direction
    logical verysmall
    logical, save :: firstverysmall=.true.

    integer round
    external round

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !     ------------------------------------------------------------------

    npairs=0
    do i=1,3
       boxs(i)=box(i)*box(i)
    enddo
    Rneicut=sqrt(rcutsq)
    if ( BrennerBeardmore ) then
      rcut(1)=Rneicut*2/box(1) !Double node's borders
      rcut(2)=Rneicut*2/box(2)
      rcut(3)=Rneicut*2/box(3)
    else
      rcut(1)=Rneicut/box(1)
      rcut(2)=Rneicut/box(2)
      rcut(3)=Rneicut/box(3)
    endif  

    !
    !     Atom passing stage between processors
    !     
    !     Since cell size may change, these must be recalculated
    !
    passlimmin(1)=rmn(1)+rcut(1)
    passlimmin(2)=rmn(2)+rcut(2)
    passlimmax(1)=rmx(1)-rcut(1)
    passlimmax(2)=rmx(2)-rcut(2)

    ! Calculate maximum possible size of node plus its neighbouring 'skin'

    maxhelp=rmx(1); if (maxhelp > 0.5) maxhelp=0.5;
    minhelp=rmn(1); if (minhelp < -0.5) minhelp=-0.5;
    maxsize(1)=(maxhelp-minhelp+2*rcut(1))*box(1)

    maxhelp=rmx(2); if (maxhelp > 0.5) maxhelp=0.5;
    minhelp=rmn(2); if (minhelp < -0.5) minhelp=-0.5;
    maxsize(2)=(maxhelp-minhelp+2*rcut(2))*box(2)

    maxsize(3)=box(3)
    if (nprocs == 1) then
       maxsize(1)=box(1)
       maxsize(2)=box(2)
    endif

    verysmall=.false.
    if (3*Rneicut>=maxsize(1).or.3*Rneicut>=maxsize(2).or.3*Rneicut>=maxsize(3)) then

       verysmall=.true.
       if (firstverysmall) then
          write(6,*) 'WARNING: Rneicut >= maxsize/3'


          write (6,*) 'linkedlist needs at least 3 cells to work...'
          write (6,*) Rneicut,maxsize(1),maxsize(2),maxsize(3)
          write (6,*) 'Reverting to N^2 neighbour calculation'
          firstverysmall=.false.
          if (nprocs>1) then
             write(6,*) 'ERROR: on multiple processors this may not work'
             call my_mpi_abort('Rneicut too large', int(Rneicut))
          endif
       endif
    endif

    if ( (pbc(1)==1.0d0 .and. 2*Rneicut>=maxsize(1)) &
         .or. (pbc(2)==1.0d0 .and. 2*Rneicut>=maxsize(2)) &
         .or. (pbc(3)==1.0d0 .and. 2*Rneicut>=maxsize(2)) ) then

       write(6,*) 'ERROR: cell size less than twice cutoff'
       call my_mpi_abort('Cell size and cutoff', int(Rneicut))
    endif

    ! Estimate an upper limit for how many atoms I may have to deal with
    ! This should be the same as the size of list() and dbuf()... in para_common.h
    np0max=PARRATIO*NPMAX/2


    if (nprocs > 1) then
       t1=mpi_wtime()

       !     First stage: find atoms to be passed, mark them.
       !     This stage is run only in pair_table, then atpassflag()
       !     is passed to the force calculation routines
       !

       do i=1,myatoms
          i3=i*3-3
          x=x0(i3+1)
          y=x0(i3+2)
          xpmax=passlimmax(1)
          xpmin=passlimmin(1)
          ypmax=passlimmax(2)
          ypmin=passlimmin(2)
          atpassflag(i)=0
          if (y > ypmax) then
             atpassflag(i)=IOR(atpassflag(i),BUP)
             if (x > xpmax) atpassflag(i)=IOR(atpassflag(i),BUPRIGHT)
             if (x < xpmin) atpassflag(i)=IOR(atpassflag(i),BUPLEFT)
          endif
          if (y < ypmin) then
             atpassflag(i)=IOR(atpassflag(i),BDOWN)
             if (x > xpmax) atpassflag(i)=IOR(atpassflag(i),BDOWNRIGHT)
             if (x < xpmin) atpassflag(i)=IOR(atpassflag(i),BDOWNLEFT)
          endif
          if (x > xpmax) atpassflag(i)=IOR(atpassflag(i),BRIGHT)
          if (x < xpmin) atpassflag(i)=IOR(atpassflag(i),BLEFT)
          !            print *,myproc,x,y,atpassflag(i)
       enddo
    endif

    !
    !  Second stage: pass and receive atoms to different directions.
    !  This piece of code should be identical in the neighbour list and
    !  force calculations.
    !  
    !  In principle the idea is that the input is the local x0(myatoms) 
    !  coordinates, and the output is buf(np0), which contains the 
    !  coordinates of the atoms both in this node and within cut_nei
    !  from this in the adjacent nodes.
    !
    !  The atoms which get passed here must be exactly the
    !  same and in the same order in the force calculations
    !  as in the original neighbour list calculation ! Since
    !  shuffle is always performed right before this routine,
    !  this should be no problem.
    !
    do i=1,3*myatoms
       buf(i)=x0(i)
    enddo
    np0=myatoms

    if (nprocs > 1) then
       !        print *,'loop 2',myproc
       !        Loop over directions
       do d=1,8
          !           print *,myproc,d
          j=0; jj=0
          do i=1,myatoms
             if (IAND(atpassflag(i),passbit(d)) /= 0) then
                j=j+1
                if (j >= NPPASSMAX)                                       &
                     call my_mpi_abort('NPPASSMAX overflow', int(myproc))
                i3=i*3-3
                j3=j*3-3
                xsendbuf(j3+1)=x0(i3+1)
                xsendbuf(j3+2)=x0(i3+2)
                xsendbuf(j3+3)=x0(i3+3)
                !                  print *,d,j,x0(i3+1),x0(i3+2)
             endif
          enddo
          npass(d)=j
          !            print *,'send',myproc,d,j,nngbrproc(d)
          ! Safe communication model - I hope
          dd=d+4; if (d>8) dd=dd-8;
          if (sendfirst(d)) then
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j > 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
             endif
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             if (jj > 0) then
                call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else 
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             if (jj > 0) then
                call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j > 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
             endif
          endif
          j=jj
          np0=np0+j

          !            print *,'received',myproc,d,j
          if (np0+j >= np0max) then
             print '(A)','Warning: np0>np0max, EAMforces will go wrong'
             print '(4i7)',myproc,np0,j,np0max
             print '(A)','You should increase parratio or npmax'
             call my_mpi_abort('Increase parratio in para_common.h', int(np0+j))
          endif
       enddo
       tmr(16)=tmr(16)+(mpi_wtime()-t1)
    endif
    np0pairtable=np0
    !      print *,'Node received totally ',myproc,np0

    !
    !  Actual neighbour list calc. (the neighbor list format was
    !  remodeled from the old one with the new passing algorithm)
    !
    t1=mpi_wtime()

    do i = 1, 3*myatoms
       x0nei(i)=0.0
    enddo

    !  This is reflected in the neighbour list calculation, which also only
    !  accepts neighbours of x0 which are to the right. (?)
    !

    mij=0
    npairs=0

    ! Tabulate all pairs for my atoms
    !
    ! myatoms is the number of myatoms, np0 total number of possible
    ! neighbours from my and adjacent nodes

    nborsize=NNMAX*NPMAX

    !*************************************************************************
    ! Linked list subroutine written by Kai Nordlund. 
    ! Written to be as self-contained as possible to make it easy to move
    ! around if needed.
    !
    ! Input:  x(i)       Atom coordinates scaled between -0.5 and 0.5
    !         N          Number of atoms to deal with
    !         box(3)     Cell size in A (or any units)
    !         pbc(3)     Periodic boundary conditions (1.0d0 if true)
    !         Rneicut    Neighbour cutoff radius in A        
    !         nborsize   Size of neighbour list
    !
    !
    ! Output: nborlist(i) Array with list of neighbours
    !         nabors(i)   Pointer to start of indices for each atom
    !         npairs      Total number of neighbour pairs      
    !x
    ! nborlist(i) has following format:
    !
    !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
    !  nborlist(2)    Index of first neighbour of atom 1
    !  nborlist(3)    Index of second neighbour of atom 1
    !   ...
    !  nborlist(N1+1)  Index of last neighbour of atom 1
    !  nborlist(N1+2)  Number of neighbours for atom 2 
    !  nborlist(N1+3)  Index of first neighbour of atom 2
    !   ...
    !   ...           And so on for all N atoms
    !

    !
    !  Present version is a version for parallell operation
    !  i atoms are taken from array x, j atoms from array xn, mij is
    !  taken from outside 
    !


    IF(debug)PRINT *,'linklist start'
    !      if (firsttime) then

    !     
    ! Figure out what range of atoms we will really need.
    !
    Nixmax=max(0,int(maxsize(1)/Rneicut)-1)
    Nixmin=0
    Niymax=max(0,int(maxsize(2)/Rneicut)-1)
    Niymin=0
    Nizmax=max(0,int(maxsize(3)/Rneicut)-1)
    Nizmin=0

    if (iprint .and. firsttime) then
       write (6,12) Nixmax,Niymax,Nizmax
12     format ('Linklist nx ny nz',3I4)
    endif

    if ((Nizmax+1)*(Niymax+1)*(Nixmax+1) > NPMAX/2) then
       write (6,*) 'linkedlist ERROR: head size too small'
       call my_mpi_abort('head size', int(NPMAX/2))
    endif
    !      endif



    !
    !  Calculate the actual cell size in all dimensions
    !
    cellsize(1)=maxsize(1)/(Nixmax-Nixmin+1)
    cellsize(2)=maxsize(2)/(Niymax-Niymin+1)
    cellsize(3)=maxsize(3)/(Nizmax-Nizmin+1)
    !
    !     Calculate the node minimum coordinate. Remember periodics !
    !     The lower node limit is exclusive, upper inclusive !     
    !

    nodemin(1)=rmn(1)-rcut(1)
    if (pbc(1)==0.0 .and. nodemin(1) <= -half) nodemin(1)=-half-rcut(1)    
    if (pbc(1)==1.0d0.and.nodemin(1) <= -half) nodemin(1)=nodemin(1)+1.0d0
    nodemin(2)=rmn(2)-rcut(2)
    if (pbc(2)==0.0.and. nodemin(2) <= -half) nodemin(2)=-half-rcut(2)    
    if (pbc(2)==1.0d0.and. nodemin(2) <= -half) nodemin(2)=nodemin(2)+1.0d0
    nodemin(3)=-half

    nodemax(1)=rmx(1)+rcut(1)
    if (pbc(1)==0.0 .and. nodemax(1) > half) nodemax(1)=half+rcut(1)
    if (pbc(1)==1.0d0 .and. nodemax(1) > half) nodemax(1)=nodemax(1)-1.0d0
    nodemax(2)=rmx(2)+rcut(2)
    if (pbc(2) == 0.0 .and. nodemax(2) > half) nodemax(2)=half+rcut(2)
    if (pbc(2) == 1.0d0 .and. nodemax(2) > half) nodemax(2)=nodemax(2)-1.0d0
    nodemax(3)=half

    if (nprocs == 1) then
       nodemin(1)=-half
       nodemin(2)=-half
       nodemax(1)=half
       nodemax(2)=half
    endif

    if (firsttime .and. iprint) then
       print *,'First time in link cell calculation in proc,',myproc
       print 26,'proc X cell n, size and range',                          &
            myproc,Nixmax+1,cellsize(1),nodemin(1),nodemax(1)
       print 26,'proc Y cell n, size and range',                          &
            myproc,Niymax+1,cellsize(2),nodemin(2),nodemax(2)
       print 26,'proc Z cell n, size and range',                          &
            myproc,Nizmax+1,cellsize(3),nodemin(3),nodemax(3)
26     format (A,2I3,3F9.4)
    endif
    IF(debug)PRINT *,'linklist2'

    do i=0,(Nizmax+1)*(Niymax+1)*(Nixmax+1)
       head(i)=0
    enddo


    if (verysmall) then
       ! Simply make list contain all atoms in order
       ! and zero in end to end list
       ! head not used at all now.
       do i=1,np0
          list(i)=i+1
       enddo
       list(np0)=0
    else

       !
       !  Arrange j atoms in cells
       !
       !  Because the Allen-Tildesley linked list puts atoms in reverse order,
       !  go through atoms from top to bottom to get them in logical order
       !
       xhelp=box(1)/cellsize(1)
       yhelp=box(2)/cellsize(2)
       zhelp=box(3)/cellsize(3)

       do i=np0,1,-1
          i3=3*i-3

          dx=buf(i3+1)-nodemin(1)
          if (pbc(1) == 1.0d0 .and. dx < 0.0) dx=dx+1.0d0
          if (pbc(1) == 1.0d0 .and. dx > 1.0d0) dx=dx-1.0d0
          ix=int(dx*xhelp)
          !if (myproc==12) print *,i,buf(i3+1),nodemin(1),dx,ix  
          if (ix < Nixmin .or. ix > Nixmax) then
             if (pbc(1) == 1.0d0) then
                print *,'This is impossible, atom x outside cell',           &
                     ix,dx,nodemin(1),buf(i3+1)
                print *,myproc,i
                call my_mpi_abort('x limit', int(myproc))
             else
                if (ix < Nixmin) ix=Nixmin
                if (ix > Nixmax) ix=Nixmax
             endif
          endif
          !if (myproc==12) print *,i,buf(i3+1),nodemin(1),dx,ix

          dy=buf(i3+2)-nodemin(2)
          if (pbc(2) == 1.0d0 .and. dy < 0.0) dy=dy+1.0d0
          if (pbc(2) == 1.0d0 .and. dy > 1.0d0) dy=dy-1.0d0
          iy=int(dy*yhelp)
          if (iy < Niymin .or. iy > Niymax) then
             if (pbc(2) == 1.0d0) then
                print *,'This is impossible, atom y outside cell',           &
                     iy,dy,nodemin(2),buf(i3+2)
                print *,myproc,i
                call my_mpi_abort('y limit', int(myproc))
             else
                if (iy < Niymin) iy=Niymin
                if (iy > Niymax) iy=Niymax
             endif
          endif

          dz=buf(i3+3)-nodemin(3)
          if (pbc(3) == 1.0d0 .and. dz < 0.0) dz=dz+1.0d0
          if (pbc(3) == 1.0d0 .and. dz > 1.0d0) dz=dz-1.0d0
          iz=int(dz*zhelp)
          if (iz < Nizmin .or. iz > Nizmax) then
             if (pbc(3) == 1.0d0) then
                print *,'This is impossible, atom z outside cell',           &
                     iz,dz,nodemin(3),buf(i3+3)
                print *,myproc,i
                call my_mpi_abort('z limit', int(myproc))
             else
                if (iz < Nizmin) iz=Nizmin
                if (iz > Nizmax) iz=Nizmax
             endif
          endif

          ! Allen-Tildesley linkcell creation

          icell=(iz*(Niymax+1)+iy)*(Nixmax+1)+ix
          list(i)=head(icell)
          head(icell)=i

       enddo

    endif ! End of choice between very small and normal cell

    tmr(24)=tmr(24)+(mpi_wtime()-t1)
    t1=mpi_wtime()

    !
    ! Find neighbours using cell list constructed above
    !
    pbcx(1)=pbc(1)
    pbcx(2)=pbc(2)
    pbcx(3)=pbc(3)
    if (nprocs >= 4) then
       pbcx(1)=0.0
       pbcx(2)=0.0
    endif
    mij=0
    xsmin=1e30

    IF(debug)PRINT *,'linklist3'


    dixmin=-1
    diymin=-1
    dizmin=-1
    dixmax=+1
    diymax=+1
    dizmax=+1
    ! EAM discards atom pairs xj < xi, hence dixmin=0
    if (.not. AnySemi) dixmin=0

    if (verysmall) then
       ! head not used at all, hence this.
       dixmin=0; Nixmin=0;
       diymin=0; Niymin=0;
       dizmin=0; Nizmin=0;
       dixmax=0; Nixmax=0;
       diymax=0; Niymax=0;
       dizmax=0; Nizmax=0;
    endif

    if (BrennerBeardmore) then
      myatoms_max = np0 ! Requares all atoms (cell and borders) to be in a list 
    else
      myatoms_max = myatoms ! Normal neighbor list
    endif

    do i=1, myatoms_max !myatoms
       i3=3*i-3
       dx=buf(i3+1)-nodemin(1)
       if (pbc(1) == 1.0d0 .and. dx < 0.0) dx=dx+1.0d0
       if (pbc(1) == 1.0d0 .and. dx > 1.0d0) dx=dx-1.0d0
       ix=int(dx*xhelp)
       if (pbc(1) /= 1.0d0 .and. ix < Nixmin) ix=Nixmin
       if (pbc(1) /= 1.0d0 .and. ix > Nixmax) ix=Nixmax

       dy=buf(i3+2)-nodemin(2)
       if (pbc(2) == 1.0d0 .and. dy < 0.0) dy=dy+1.0d0
       if (pbc(2) == 1.0d0 .and. dy > 1.0d0) dy=dy-1.0d0
       iy=int(dy*yhelp)
       if (pbc(2) /= 1.0d0 .and. iy < Niymin) iy=Niymin
       if (pbc(2) /= 1.0d0 .and. iy > Niymax) iy=Niymax

       dz=buf(i3+3)-nodemin(3)
       if (pbc(3) == 1.0d0 .and. dz < 0.0) dz=dz+1.0d0
       if (pbc(3) == 1.0d0 .and. dz > 1.0d0) dz=dz-1.0d0
       iz=int(dz*zhelp)
       if (pbc(3) /= 1.0d0 .and. iz < Nizmin) iz=Nizmin
       if (pbc(3) /= 1.0d0 .and. iz > Nizmax) iz=Nizmax

       if (verysmall) then
          ix=0; iy=0; iz=0;
       endif

       if ((ix<Nixmin.or.ix>Nixmax).or.                                   &
            (iy<Niymin.or.iy>Niymax).or.                                   &
            (iz<Nizmin.or.iz>Nizmax)) then
          print *,'i x y z index',i,buf(i3+1),                            &
               buf(i3+2),buf(i3+3),atomindex(i)
          print *,'ix iy iz',ix,iy,iz
          print 26,'proc X cell n, size and range',                       &
               myproc,Nixmax+1,cellsize(1),nodemin(1),nodemax(1)
          print 26,'proc Y cell n, size and range',                       &
               myproc,Niymax+1,cellsize(2),nodemin(2),nodemax(2)
          print 26,'proc Z cell n, size and range',                       &
               myproc,Nizmax+1,cellsize(3),nodemin(3),nodemax(3)

          call my_mpi_abort('XXX HORROR', int(myproc))
       endif

       mij=mij+1
       mijptr=mij
       nborlist(mijptr)=0
       ! Store first index of this atom
       nabors(i)=mijptr+1

       do dix=dixmin,dixmax
          inx=ix+dix
          if (inx < Nixmin .and. pbcx(1) == 1.0d0) inx=Nixmax
          if (inx > Nixmax .and. pbcx(1) == 1.0d0) inx=Nixmin
          if (inx < Nixmin .and. pbcx(1) /= 1.0d0) goto 101
          if (inx > Nixmax .and. pbcx(1) /= 1.0d0) goto 101
          do diy=diymin,diymax
             iny=iy+diy
             if (iny < Niymin .and. pbcx(2) == 1.0d0) iny=Niymax
             if (iny > Niymax .and. pbcx(2) == 1.0d0) iny=Niymin
             if (iny < Niymin .and. pbcx(2) /= 1.0d0) goto 102
             if (iny > Niymax .and. pbcx(2) /= 1.0d0) goto 102

             do diz=dizmin,dizmax
                inz=iz+diz
                if (inz < Nizmin .and. pbcx(3) == 1.0d0) inz=Nizmax
                if (inz > Nizmax .and. pbcx(3) == 1.0d0) inz=Nizmin
                if (inz < Nizmin .and. pbcx(3) /= 1.0d0) goto 103
                if (inz > Nizmax .and. pbcx(3) /= 1.0d0) goto 103

                icell=(inz*(Niymax+1)+iny)*(Nixmax+1)+inx

                if (verysmall) then
                   j=1;
                else
                   ! Get first atom in cell
                   j=head(icell)
                endif
                do 
                   if (j==0) exit
                   !
                   !                    Now the distance calculation
                   !                    between atoms i and j
                   !
                   if (AnySemi) then  
                      !                    Accept all neighbours
                      if (i == j) goto 104  
                      j3=3*j-3
                      xp(1)=buf(j3+1)-buf(i3+1)
                      if (pbc(1)==1.0d0) xp(1)=xp(1)-INT(xp(1)+xp(1))
                      xs = xp(1)*xp(1)*boxs(1)
                      if (xs < rcutsq) then
                         xp(2) = buf(j3+2)-buf(i3+2)
                         if (pbc(2)==1.0d0) xp(2)=xp(2)-INT(xp(2)+xp(2))
                         xs = xs + xp(2)*xp(2)*boxs(2)
                         if (xs < rcutsq) then
                            xp(3) = buf(j3+3)-buf(i3+3)
                            if (pbc(3)==1.0d0)                                 &
                                 xp(3)=xp(3)-INT(xp(3)+xp(3))
                            xs = xs + xp(3)*xp(3)*boxs(3)
                            if (xs < rcutsq) then
                               !
                               !                             Accepted neighbour, add to list
                               !
                               if (xs<xsmin) xsmin=xs
                               npairs=npairs+1
                               mij = mij + 1
                               if (mij > nborsize) then
                                  call my_mpi_abort('nborsize', int(myproc))
                               endif
                               nborlist(mijptr)=nborlist(mijptr)+1
                               nborlist(mij) = j
                            endif
                         endif
                      endif

                   else ! EAM, Discard half the atom pairs, xj < xi !

                      j3=3*j-3
                      xp(1)=buf(j3+1)-buf(i3+1)
                      if (xp(1) < zero) then
                         xp(1) = xp(1) + pbc(1)
                      endif
                      xs = xp(1)*xp(1)*boxs(1)
                      if (xs < rcutsq .and. xp(1) >= zero) then
                         xp(2) = buf(j3+2)-buf(i3+2)
                         if (xp(1) /= zero .or. xp(2) >= zero) then
                            if (pbc(2)==1.0d0) xp(2)=xp(2)-                    &
                                 INT(xp(2)+xp(2))
                            xs = xs + xp(2)*xp(2)*boxs(2)
                            if (xs < rcutsq) then
                               xp(3) = buf(j3+3)-buf(i3+3)
                               if ( (xp(1) /= zero) .or.                     &
                                    (xp(2) /= zero) .or.                     &
                                    (xp(3) > zero) ) then
                                  if (pbc(3)==1.0d0) xp(3)=xp(3)-              &
                                       INT(xp(3)+xp(3))
                                  xs = xs + xp(3)*xp(3)*boxs(3)
                                  if (xs < rcutsq) then
                                     !
                                     !                                   Accepted neighbour, add to list
                                     !
                                     if (xs<xsmin) xsmin=xs
                                     npairs=npairs+1
                                     mij = mij + 1
                                     if (mij > nborsize) then
                                        call my_mpi_abort('nborsize', int(myproc))
                                     endif
                                     nborlist(mijptr)=nborlist(mijptr)+1
                                     nborlist(mij) = j
                                  endif
                               endif
                            endif
                         endif
                      endif

                   endif


104                continue   ! End of this atom handling

                   !                    Get next atom j
                   j=list(j)

                enddo
103             continue
             enddo
102          continue
          enddo
101       continue
       enddo
    enddo


    nabors(myatoms+1)=mij+2





    if (SortNeiList) then
       ! Because of Stilweb mixed routines
       ! sort neighbours of all atoms in ascending order

       do i=1,myatoms
          call isort(nabors(i),nabors(i+1)-2,nborlist)
       enddo

    endif

    IF(debug)PRINT *,'nborlist construction done'

    firsttime=.false.
    !
    !     End of link cell calculation
    !***********************************************************************


    !       PRINT *,' Node ',myproc,' has ',npairs,' pairs'

    !0    FORMAT (i2,2i4,9f7.3)

    npairstot=1.0d0*npairs
    call my_mpi_dsum(npairstot, 1)
    xs=npairstot/natoms
    xsmin=sqrt(xsmin)
    call my_mpi_dmin(xsmin, 1)
    if (printnow) then
       IF(iprint)WRITE (6,1011) xs,xsmin
       IF(iprint)WRITE (7,1011) xs,xsmin
    endif
1011 format ('Neighbours per atom = ',f11.6,' drmin',f12.4)

    tmr(25)=tmr(25)+(mpi_wtime()-t1)

  end subroutine Pair_Table


  !***********************************************************************
  !***********************************************************************
  !***********************************************************************
  integer function round(x)
  use defs  
    implicit none

    real*8 x

    if (x > 0.0) then
       round=int(x+0.5d0)
    else
       round=int(x-0.5d0)
    endif

    return
  end function round



  !***********************************************************************
  !***********************************************************************
  !***********************************************************************


  !
  ! Quicksort from Numerical Recipes in Fortran77 p. 324-325.
  ! copied from PDF document by Kai Nordlund Jan 21, 2003
  ! modified for integers and in arbitrary range in arr
  !

  subroutine isort(n1,n2,iarr) 
    use my_mpi
  use defs  
    implicit none

    integer n1,n2,M,NSTACK 
    integer*4 iarr(*) 
    parameter (M=7,NSTACK=NNMAX*2)
    !     
    !     Sorts an array iarr(1:n) into ascending numerical order using the
    !     Quicksort algorithm. n is input; iarr is replaced on output by its
    !     sorted rearrangement.
    !     Parameters: M is the size of subarrays sorted by straight
    !     insertion and NSTACK is the require auxiliary storage.

    integer i,ir,j,jstack,k,l,istack(NSTACK)
    integer*4 a,temp

    jstack=0
    l=n1
    ir=n2 

1   if (ir-l.lt.M) then

       do j=l+1,ir
          a=iarr(j)

          do i=j-1,l,-1
             if(iarr(i).le.a)goto 2
             iarr(i+1)=iarr(i)
          enddo

          i=l-1
2         iarr(i+1)=a
       enddo

       if(jstack.eq.0)return
       ir=istack(jstack)

       l=istack(jstack-1)
       jstack=jstack-2
    else
       k=(l+ir)/2

       temp=iarr(k)
       iarr(k)=iarr(l+1)
       iarr(l+1)=temp
       if(iarr(l).gt.iarr(ir))then
          temp=iarr(l)
          iarr(l)=iarr(ir) 
          iarr(ir)=temp
       endif
       if(iarr(l+1).gt.iarr(ir))then
          temp=iarr(l+1)
          iarr(l+1)=iarr(ir)
          iarr(ir)=temp
       endif
       if(iarr(l).gt.iarr(l+1))then
          temp=iarr(l) 
          iarr(l)=iarr(l+1) 
          iarr(l+1)=temp
       endif
       i=l+1
       j=ir
       a=iarr(l+1)
3      continue
       i=i+1  
       if(iarr(i).lt.a)goto 3
4      continue
       j=j-1
       if(iarr(j).gt.a)goto 4
       if(j.lt.i)goto 5
       temp=iarr(i)
       iarr(i)=iarr(j)
       iarr(j)=temp
       goto 3
5      iarr(l+1)=iarr(j)
       iarr(j)=a
       jstack=jstack+2
       if(jstack .gt. NSTACK) call my_mpi_abort('NSTACK too small in sort', int(jstack))
       if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
       else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
       endif
    endif
    goto 1

  end subroutine isort
