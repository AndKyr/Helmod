  
  !
  ! Parallellization subroutines, some of them previously in mdsubs and mdinit
  !
  
  !***********************************************************************
  subroutine  Pass_Init(cut_nei,box,passlimmin,passlimmax,              &
       nngbrproc,natoms)
    use my_mpi
    use defs

    use para_common

    implicit none
    !
    !     Initialize some passing subroutine variables
    !

    !     Variables passed in and out

    real*8 cut_nei,box(3)

    real*8 passlimmin(2),passlimmax(2)      
    integer nngbrproc(8),natoms

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myproc = my relative processor number (0..nprocs-1)
    !     nprocs = total number of nodes being used
    !     nnodes(2) = number of nodes in x and y direction
    !     rmn(3) = min borders of this node, in scaled units
    !     rmx(3) = max borders of this node, in scaled units
    !     ------------------------------------------------------------------
    !
    !     Nodes are always arranged so that the x direction grows first,
    !     y then (see mdinit.f). I.e. eg.
    !
    !     2x2    8x4 configuration     2x4
    !     ---    -----------------     ---
    !     0 1     0 1 2 3 4 5 6 7      0 1
    !     2 3     8 9101112131415      2 3
    !             1617181920212223     4 5
    !             2425262728293031     6 7
    !
    !     nngbrproc(8) holds list of neighbouring processors
    !     They are arranged beginning from the up processor going
    !     clockwise around
    !      
    !     Local variables
    integer up,upright,right,downright
    integer down,downleft,left,upleft
    integer i

    if (rmx(1)-rmn(1) .lt. cut_nei/box(1) .or.                            &
         rmx(2)-rmn(2) .lt. cut_nei/box(2)) then
       write (6,*) 'Cell or node size too small compared to cut_nei'
       write (6,*) 'in node',myproc
       write (6,*) rmn(1),rmx(1),rmn(2),rmx(2),cut_nei
       write (6,*) 'Reduce number of nodes or cut_nei'
       call my_mpi_abort('Cell too small', INT(myproc))
    endif

    passlimmin(1)=rmn(1)+cut_nei/box(1)
    passlimmin(2)=rmn(2)+cut_nei/box(1)
    passlimmax(1)=rmn(1)-cut_nei/box(2)
    passlimmax(2)=rmn(2)-cut_nei/box(2)

    ! Estimate an upper limit for how many atoms I may have to deal with

    np0max=PARRATIO*NPMAX/2

    ! Figure out the node number of the nearest neighbors
    left=myproc-1
    if (MOD(myproc,nnodes(1)) .eq. 0) left=left+nnodes(1)
    upleft=left-nnodes(1)
    if (upleft .lt. 0) upleft=upleft+nprocs
    downleft=left+nnodes(1)
    if (downleft .ge. nprocs) downleft=downleft-nprocs

    right=myproc+1
    if (MOD(right,nnodes(1)) .eq. 0) right=right-nnodes(1)
    upright=right-nnodes(1)
    if (upright .lt. 0) upright=upright+nprocs
    downright=right+nnodes(1)
    if (downright .ge. nprocs) downright=downright-nprocs

    up=myproc-nnodes(1)
    if (up .lt. 0) up=up+nprocs
    down=myproc+nnodes(1)
    if (down .ge. nprocs) down=down-nprocs

    down=MOD(myproc+nnodes(1),nprocs)

    nngbrproc(IUP)=up
    nngbrproc(IUPRIGHT)=upright
    nngbrproc(IRIGHT)=right
    nngbrproc(IDOWNRIGHT)=downright
    nngbrproc(IDOWN)=down
    nngbrproc(IDOWNLEFT)=downleft
    nngbrproc(ILEFT)=left
    nngbrproc(IUPLEFT)=upleft

    ! Divide nodes into 'even and 'odds' for safe communication
    ! For diagonal directions, make every second row even
    ! For rows and columns, make a chessboard...
    if (MOD(myproc*nnodes(2)/nprocs,2)==0) then
       sendfirst(IUPRIGHT)=.true.
       sendfirst(IDOWNRIGHT)=.true.
       sendfirst(IUPLEFT)=.true.
       sendfirst(IDOWNLEFT)=.true.
       if (MOD(myproc,2)==0) then
          sendfirst(IUP)=.true.
          sendfirst(IRIGHT)=.true.
          sendfirst(ILEFT)=.true.
          sendfirst(IDOWN)=.true.
       else
          sendfirst(IUP)=.false.
          sendfirst(IRIGHT)=.false.
          sendfirst(ILEFT)=.false.
          sendfirst(IDOWN)=.false.
       endif
    else
       sendfirst(IUPRIGHT)=.false.
       sendfirst(IDOWNRIGHT)=.false.
       sendfirst(IUPLEFT)=.false.
       sendfirst(IDOWNLEFT)=.false.
       if (MOD(myproc,2)==1) then
          sendfirst(IUP)=.true.
          sendfirst(IRIGHT)=.true.
          sendfirst(ILEFT)=.true.
          sendfirst(IDOWN)=.true.
       else
          sendfirst(IUP)=.false.
          sendfirst(IRIGHT)=.false.
          sendfirst(ILEFT)=.false.
          sendfirst(IDOWN)=.false.
       endif
    endif

    ! print *,'myproc sendfirst',myproc,(sendfirst(i),i=1,8)
    return 
  end subroutine Pass_Init

  !***********************************************************************
  subroutine getminofallprocs(procmin,iproc)
    use datatypes
    use my_mpi
    use defs

    use para_common

    implicit none

    !
    !  Subroutine goes through the real variable min in all processors,
    !  and gets the minimum of it, returning the global minimum
    !  and the number of the node on which the minimum is.
    !
    !  Using MPI, this could be replaced with Allreduce and MINLOC

    real*8 procmin
    integer iproc

    !          Variables for parallel operation
!include 'para_common.f90'
    !     nprocs = total number of nodes being used
    !     nnodes(1) = number of nodes in x direction (columns)
    !     nnodes(2) = number of nodes in y direction (rows)

    real*8 globalmin
    integer globalminproc
    integer i,proctag

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    if (nprocs .lt. 2) then
       iproc=0
       return
    endif
    !      print *,'getmin in',procmin,iproc
    !
    !  First all other processors except 0 send their data to 0
    !
    if (myproc .ne. 0) then
       proctag=myproc
       call mpi_send(procmin, 1, mpi_double_precision, 0, proctag, &
            mpi_comm_world, ierror)
    endif
    if (myproc .eq. 0) then
       !
       !        0 receives data from all processors and finds minimum
       !
       globalmin=procmin
       globalminproc=0
       do i=1,nprocs-1
          proctag=i
          call mpi_recv(procmin, 1, mpi_double_precision, mpi_any_source, &
               proctag, mpi_comm_world, mpi_status_ignore, ierror)
          if (procmin .lt. globalmin) then
             globalmin=procmin
             globalminproc=proctag
          endif
       enddo
       !
       !        0 sends out answer to all other processors
       !
       do i=1,nprocs-1
          call mpi_send(globalmin, 1, mpi_double_precision, i, 0, &
               mpi_comm_world, ierror)
          call mpi_send(globalminproc, 1, my_mpi_integer, i, 0, &
               mpi_comm_world, ierror)
       enddo
    endif
    !
    !     All processors listen to answer
    !
    if (myproc .ne. 0) then
       call mpi_recv(globalmin, 1, mpi_double_precision, &
            mpi_any_source, 0, mpi_comm_world, &
            mpi_status_ignore, ierror)
       call mpi_recv(globalminproc, 1, my_mpi_integer, &
            mpi_any_source, 0, mpi_comm_world, &
            mpi_status_ignore, ierror)
    endif
    procmin=globalmin
    iproc=globalminproc
    !      print *,'getmin result',procmin,iproc,myproc

    return
  end subroutine getminofallprocs


  !***********************************************************************
  subroutine getmaxofallprocs(procmax,iproc)
    use datatypes
    use my_mpi
    use defs

    use para_common

    implicit none

    !
    !  Subroutine goes through the real variable max in all processors,
    !  and gets the maximum of it, returning the global maximum
    !  and the number of the node on which the maximum is.
    !
    !  Using MPI, this could be replaced with Allreduce and MINLOC
    !
    real*8 procmax
    integer iproc

    !          Variables for parallel operation
!include 'para_common.f90'
    !     nprocs = total number of nodes being used
    !     nnodes(1) = number of nodes in x direction (columns)
    !     nnodes(2) = number of nodes in y direction (rows)

    real*8 globalmax
    integer globalmaxproc
    integer i,proctag

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    if (nprocs .lt. 2) then
       iproc=0
       return
    endif
    !      print *,'getmax in',procmax,iproc
    !
    !  First all other processors except 0 send their data to 0
    !
    if (myproc .ne. 0) then
       proctag=myproc
       call mpi_send(procmax, 1, mpi_double_precision, 0, proctag, &
            mpi_comm_world, ierror)
    endif
    if (myproc .eq. 0) then
       !
       !        0 receives data from all processors and finds maximum
       !
       globalmax=procmax
       globalmaxproc=0
       do i=1,nprocs-1
          proctag=i
          call mpi_recv(procmax, 1, mpi_double_precision, &
               mpi_any_source, proctag, mpi_comm_world, &
               mpi_status_ignore, ierror)
          if (procmax .gt. globalmax) then
             globalmax=procmax
             globalmaxproc=proctag
          endif
       enddo
       !
       !        0 sends out answer to all other processors
       !
       do i=1,nprocs-1
          call mpi_send(globalmax, 1, mpi_double_precision, i, 0, &
               mpi_comm_world, ierror)
          call mpi_send(globalmaxproc, 1, my_mpi_integer, i, 0, &
               mpi_comm_world, ierror)
       enddo
    endif
    !
    !     All processors listen to answer
    !
    if (myproc .ne. 0) then
       call mpi_recv(globalmax, 1, mpi_double_precision, &
            mpi_any_source, 0, mpi_comm_world, &
            mpi_status_ignore, ierror)
       call mpi_recv(globalmaxproc, 1, my_mpi_integer, &
            mpi_any_source, 0, mpi_comm_world, &
            mpi_status_ignore, ierror)
    endif
    procmax=globalmax
    iproc=globalmaxproc
    !      print *,'getmax result',procmax,iproc,myproc

    return
  end subroutine getmaxofallprocs


  !***********************************************************************

  ! Version improved by Jura Tarus to check that nnodes is divisible
  ! by 2 in all dimensions      

  subroutine Rectangle(box)
    use my_mpi
    use defs

    use para_common

    implicit none
    !     ------------------------------------------------------------------
    !          Variables passed in
    real*8 box(*)

    !          Variables for parallel operation
!include 'para_common.f90'
    !     nprocs = total number of nodes being used
    !     nnodes(1) = number of nodes in x direction (columns)
    !     nnodes(2) = number of nodes in y direction (rows)
    !     ------------------------------------------------------------------
    !          Local variables
    integer maxxnodes,maxynodes,nxnodes,nynodes,nxynodes
    real*8 ratio,bestratio,boxratio
    logical found  !JT
    !     ------------------------------------------------------------------

    if (nprocs == 1) then
       nnodes(1)=1; nnodes(2)=1;
       return
    endif


    ! Find optimal ratio = y-length / x-length

    boxratio = box(2)/box(1)
    bestratio = 1000000.0d0

    found=.false. !JT

    nynodes=nprocs   !JT
    maxxnodes=nprocs  !JT
    nxnodes=1

10  nxynodes = nynodes*nxnodes
    if (nxynodes .gt. nprocs) then
       nynodes=nynodes-1
       if (nynodes .gt. 0) goto 10
    elseif (nxynodes .lt. nprocs) then
       nxnodes=nxnodes+1
       if (nxnodes .lt. maxxnodes) goto 10
    elseif((MOD(nxnodes,2) .eq. 0) .and. (MOD(nynodes,2) .eq. 0)) then !JT
       ratio = DBLE(nynodes)/DBLE(nxnodes)
       if ( ABS(ratio-boxratio) .le. ABS(bestratio-boxratio)) then
          found=.true.                         !JT
          bestratio=ratio
          nnodes(1)=nxnodes
          nnodes(2)=nynodes
          nxnodes=nxnodes+1
          if (nxnodes .lt. maxxnodes) goto 10
       endif
    else                                      !JT
       nxnodes=nxnodes+1                     !JT
       if (nxnodes .lt. maxxnodes) goto 10   !JT
    endif

    if(.not.found) then                                      !JT
       print *,"No suitable procs division found. Stopping"  !JT
       call my_mpi_abort('Proc. div. failed', INT(0))
    end if

    IF(iprint)WRITE(6,*) 'nprocs,nnodes(1..2)=',nprocs,nnodes(1),nnodes(2)

    if (nnodes(1)*nnodes(2) .ne. nprocs) then
       IF(iprint)WRITE(7,1000)nprocs,maxxnodes,maxynodes
       stop 'Could not find a rectangular geometry for the nodes'
    endif
1000 format('Could not find a rectangular geometry for ',i4, &
         ' nodes with a max x and y of ',i4,' and ',i4,' nodes')

  end subroutine Rectangle


  !***********************************************************************
  !  Shuffle atoms to correct node
  !***********************************************************************
  subroutine Shuffle(x0,x1,x2,x3,x4,x5,x0nei,                           &
       atomindex,atype,natoms,pbc,nngbrproc,printnow)
    use datatypes
    use my_mpi
    use defs

    ! Variables for parallel operation
    use para_common
    !     myatoms = number of atoms that my node is responsible for
    !     mxat = the maximum number of atoms on any node
    !     myproc = my relative processor number (0..nprocs-1)
    !     rmn(1..2),rmx(1..2) = x&y min&max boundaries for my nodes region
    !     nprocs = total number of nodes being used
    !     nnodes(1) = number of nodes in x direction (columns)
    !     nnodes(2) = number of nodes in y direction (rows)
    !     buf(8*NPMAX) = buffer space for information from other nodes

    use casc_common

    implicit none

    ! Kai Nordlund 06/12 1996
    ! Code amended by Sebastin Von Alfthan in 2010 to scale it up
    ! and this comment amended by Daniel Landau in 2012
    !
    !  Subroutine goes through all atoms on each node. If the atom
    !  shouldn't be on the current node, it packs it up in the atbuf arrays,
    !  and sends it to all neighboring processors and at the same time
    !  recieves all the atoms the neigbors want to get rid of
    !  
    !  It then looks through the atoms it recieved and if it
    !  finds an atom which belongs to itself, it accepts it and adds
    !  it to the end of the atom array list
    !
    !  This could be done better by figuring out what node the atom
    !  will belong to before sending, and then send it only to that node. 
    !  But as this part is not "mission-critical", we can ignore that.

    !
    !  Take care to see to it that irec and irecproc are moved correctly !
    !

    !     ------------------------------------------------------------------
    !          Variables passed in and out
    integer atomindex(*),atype(*), natoms,irecprocnew
    real*8 x0(*),x0nei(*),x1(*),x2(*),x3(*),x4(*),x5(*)
    real*8 pbc(3)
    logical printnow
    integer nngbrproc(8),d

    !
    !  Any integer data is packed into iatbuf, any floating point data into atbuf
    !

    real*8 atbuf(21*NSHUFFLEMAX)
    integer iatbuf(6*NSHUFFLEMAX)
    real*8 atbuf2(21*NSHUFFLEMAX)
    integer iatbuf2(6*NSHUFFLEMAX)
    !     ------------------------------------------------------------------
    !          Local variables and constants
    integer ndest,nfrom
    real*8 x,dx,y,dy
    real*8 box_w,box_m

    integer i,j,l,i1,i3,j3,n,np,npacked,npacked2,myat2,myatsum,ntag
    integer nshuffled,nreceived,iorig,procorig,blocksize
    logical isendfirst,sputtered

    integer maxshuffle
    data maxshuffle /0/
    save maxshuffle

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !     ------------------------------------------------------------------



    ! Enforce periodic boundaries in periodic dimensions,
    ! as shuffle will fail if they are not strictly enforced.
    ! Might be possible due to e.g. dydtzmax
    do i=1,3*myatoms
       i3=i*3-3
       i1=MOD(i-1,3)+1
       if (x0(i) >= half) then
	  x0(i)=x0(i)-pbc(i1)
       elseif (x0(i) < -half) then
	  x0(i)=x0(i)+pbc(i1)
       endif
    enddo


    myatsum=myatoms
    call my_mpi_isum(myatsum, 1)

    if (myatsum /= natoms) then
       call mpi_barrier(mpi_comm_world, ierror)
       print *,'Shuffle: myproc myatoms',myproc,myatsum
       print *,'Shuffle: rmin rmx',rmn(1),rmn(2),rmx(1),rmx(2)
       print *,'nshuffled,nreceived',nshuffled,nreceived
       write(6,*) 'Wrong n of atoms BEFORE Shuffle',myatsum,natoms
       call my_mpi_abort('Wrong number of atoms BEFORE Shuffle', INT(myproc))
    endif

    
    nshuffled=0
    nreceived=0

    !
    ! Pack and send atoms not in my node to all other nodes
    !
    npacked=0
    if (myatoms == 0) then
       write (6,*) 'Shuffle notice: processor',myproc,'has zero atoms'
       goto 49
    endif
  
    myat2=myatoms
    i=1
    !     Start of atom sending loop
    10  continue
    ! Daniel Landau 28.8.2012:
    ! You should be able to replace the goto-continue thing with this do while, but
    ! I don't have sufficient tests right now to guarantee it, please fix if you can test it
    !do while( i <= myat2)
        i3=i*3-3
        ! if atom is outside my borders:
        if ( (x0(i3+1) .ge. rmx(1)) .or.                                      &
            (x0(i3+1) .lt. rmn(1)) .or.                                      &
            (x0(i3+2) .ge. rmx(2)) .or.                                      &
            (x0(i3+2) .lt. rmn(2)) ) then
            !check if this is a sputtered atom, if it is we move it far away and fix it.
            sputtered=.false.
            do j=1,2
                box_w=rmx(j)-rmn(j) !width of one proces cell, assume it is identical for all processes
                box_m=0.5*(rmx(j)+rmn(j)) !middle of box
                dx=0.0 !dx is the correction for jumping over pbc
                if(pbc(j)==one .and. box_m-x0(i3+j)>half) dx=1.0d0
                if(pbc(j)==one .and. box_m-x0(i3+j)<-half) dx=-1.0d0

                if(x0(i3+j)+dx .ge. rmx(j)+box_w .or. x0(i3+j)+dx .lt. rmn(j)-box_w ) then
                    sputtered=.true.
                end if
            end do
            if (sputtered) then
                !sputtered, lets fix it and place it far away
                x0(i3+1)=half*(rmx(1)+rmn(1)) !x-pos in middle of process cell
                x0(i3+2)=half*(rmx(2)+rmn(2)) !y-pos in middle of process cell
                x0(i3+3)=1.0d20+10*REAL(atomindex(i)) !z-pos far away+some separation
                atype(i)=-ABS(atype(i)) !fix atom
            else
                ! Atom in wrong node, pack it, then discard it by copying all the atoms after it
                ! one step down
                nshuffled=nshuffled+1
                call packatom(i,x0,x0nei,x1,x2,x3,x4,x5,atomindex,atype,           &
                    npacked,atbuf,iatbuf,myproc)
                npacked=npacked+1
                myat2=myat2-1
                do j=i,myat2
                    call copyatom(j+1,j,x0,x0nei,x1,x2,x3,x4,x5,atomindex,atype)
                    if (myproc==irecproc .and. j+1==irec) then
                        irec=irec-1
                    endif
                enddo
                ! The current i now has a new atom that should be handled, so shift
                ! i down, because it will be shifted up after this
                i=i-1
            endif
        end if
        i=i+1
	! Daniel Landau: Use this end do with the previous do while, if you can test it to be correct
	!    end do
	if (i .le. myat2) goto 10
	!     End of atom packing loop
     49 continue

    if (nshuffled .gt. 2000) then
       write(6,'(A,I8,I4)') 'Huge shuffle',nshuffled,myproc
    endif

    myatoms=myat2

    ! exchange atoms with neighboring nodes
    outer: &
    do d=1,8
       do i=1,d-1
          !if there are less than 2 cells in some direction we need to make sure we only do the communication once!
          if(nngbrproc(i)==nngbrproc(d)) cycle outer
       end do
       call mpi_sendrecv(npacked, 1, my_mpi_integer, &
            nngbrproc(d), 2000+d, npacked2, 1, &
            my_mpi_integer, mpi_any_source, 2000+d, &
            mpi_comm_world, mpi_status_ignore, ierror)
       call mpi_sendrecv(atbuf, npacked*21, mpi_double_precision, &
            nngbrproc(d), 3000+d, atbuf2, npacked2*21, &
            mpi_double_precision, mpi_any_source, 3000+d, &
            mpi_comm_world, mpi_status_ignore, ierror)
       call mpi_sendrecv(iatbuf, npacked*4, my_mpi_integer, &
            nngbrproc(d), 4000+d, iatbuf2, npacked2*4, &
            my_mpi_integer, mpi_any_source, 4000+d, &
            mpi_comm_world, mpi_status_ignore, ierror)
       do l=0,npacked2-1
          if ( (atbuf2(l*21+1) .lt. rmx(1)) .and.                         &
               (atbuf2(l*21+1) .ge. rmn(1)) .and.                         &
               (atbuf2(l*21+2) .lt. rmx(2)) .and.                         &
               (atbuf2(l*21+2) .ge. rmn(2)) ) then
             !              Accept atom
             myatoms=myatoms+1
             call unpackatom(myatoms,x0,x0nei,x1,x2,x3,x4,x5,             &
                  atomindex,atype,l,atbuf2,iatbuf2,iorig,procorig)
             nreceived=nreceived+1
          endif
       enddo
    enddo outer



    !
    !     Check that atoms are within bounds, just to feel safe about it
    !     Also find recoil atom
    !
    irec=0; 
    irecprocnew=-1
    do i=1,myatoms
       i3=i*3-3
       if ((x0(i3+1) .ge. rmx(1)) .or.                                    &
            (x0(i3+1) .lt. rmn(1)) .or.                                    &
            (x0(i3+2) .ge. rmx(2)) .or.                                    &
            (x0(i3+2) .lt. rmn(2)) .or.                                    &
            (pbc(1)==one .and.(x0(i3+1)>=half.or.x0(i3+1)<=-half)) .or.     &
            (pbc(2)==one .and.(x0(i3+2)>=half.or.x0(i3+2)<=-half))) then
          write (6,*) 'Severe shuffle error, atom outside bounds'
          write (6,*) x0(i3+1),x0(i3+2),x0(i3+3)
          write (6,*) rmx(1),rmn(1),rmx(2),rmn(2)
          write (6,*) i,myproc,myatoms
       endif

       if (atomindex(i) == iatrec) then
          if (irecproc /= myproc) then
             write (6,'(A,I5,A,I5,I9)')                                   &
                  'Shuffled recoil from',irecproc,' to',myproc,i
          endif
           
          irec=i
          irecprocnew=myproc

       endif
    enddo

    !     Processor which has recoil informs all others

    if (iatrec /= 0) then
       call my_mpi_imax(irecprocnew, 1)
       irecproc=irecprocnew
       call mpi_bcast(irec, 1, my_mpi_integer, irecproc, mpi_comm_world, ierror)
    endif

    !
    !  Get mxat for pair_table calculation
    !
    mxat=myatoms
    mnat=myatoms
    np=myatoms

    call my_mpi_imax(mxat, 1)
    call my_mpi_imin(mnat, 1)
    call my_mpi_isum(nshuffled, 1)
    call my_mpi_isum(nreceived, 1)
    call my_mpi_isum(np, 1)

    if (np /= natoms) then
       call mpi_barrier(mpi_comm_world, ierror)
       print *,'Shuffle: myproc myatoms',myproc,myatoms
       print *,'Shuffle: rmin rmx',rmn(1),rmn(2),rmx(1),rmx(2)
       print *,'nshuffled,nreceived',nshuffled,nreceived
       write(6,*) 'Wrong n of atoms after Shuffle',np,natoms
       call my_mpi_abort('Wrong number of atoms after Shuffle', INT(myproc))
    endif

    if (printnow) then

       IF(iprint)WRITE (6,'(A,2I5,A,2I8)') 'Shuffled',nshuffled,                   &
            nreceived,' atoms over node borders, min/max occ:',           &
            mnat,mxat
    endif

    !      IF(iprint)WRITE(6,*) 
    !     *     'Min and Max node occupation after shuffle',mnat,mxat


    IF(debug)PRINT *,'Shuffle: myproc myatoms mxat',                              &
         myproc,myatoms,mxat

  end subroutine Shuffle

  !***********************************************************************

  subroutine copyatom(i,j,x0,x0nei,x1,x2,x3,x4,x5,atomindex,atype)
    implicit none
    !
    !  Copy atom i stuff to atom j in arrays
    !

    integer i,j
    real*8 x0(*),x0nei(*),x1(*),x2(*),x3(*),x4(*),x5(*)
    integer atype(*),atomindex(*)

    integer i3,j3,ii

    i3=i*3-3
    j3=j*3-3
    do ii=1,3
       x0(j3+ii)=x0(i3+ii)
       x0nei(j3+ii)=x0nei(i3+ii)
       x1(j3+ii)=x1(i3+ii)
       x2(j3+ii)=x2(i3+ii)
       x3(j3+ii)=x3(i3+ii)
       x4(j3+ii)=x4(i3+ii)
       x5(j3+ii)=x5(i3+ii)
    enddo
    atype(j)=atype(i)
    atomindex(j)=atomindex(i)

    return 
  end subroutine copyatom

  !***********************************************************************

  subroutine packatom(i,x0,x0nei,x1,x2,x3,x4,x5,atomindex,atype,        &
       j,atbuf,iatbuf,myproc)
    use my_mpi
    use defs

    implicit none
    !
    !  Pack atom i stuff in atbuf and iatbuf
    !

    integer i,j
    real*8 x0(*),x0nei(*),x1(*),x2(*),x3(*),x4(*),x5(*),atbuf(*)
    integer atype(*),iatbuf(*),atomindex(*)

    integer i3,j4,j21,ii,myproc

    i3=i*3-3
    j4=j*4
    j21=j*21
    if (j > NSHUFFLEMAX) then
       print *,'packatom ERROR: nshufflemax too small',j,j21

       call my_mpi_abort(' nshufflemax too small', INT(myproc))
    endif

    do ii=1,3
       atbuf(j21+0+ii)=x0(i3+ii)
       atbuf(j21+3+ii)=x0nei(i3+ii)
       atbuf(j21+6+ii)=x1(i3+ii)
       atbuf(j21+9+ii)=x2(i3+ii)
       atbuf(j21+12+ii)=x3(i3+ii)
       atbuf(j21+15+ii)=x4(i3+ii)
       atbuf(j21+18+ii)=x5(i3+ii)
    enddo
    iatbuf(j4+1)=atype(i)
    iatbuf(j4+2)=i
    iatbuf(j4+3)=myproc
    iatbuf(j4+4)=atomindex(i)

    return 
  end subroutine packatom

  !***********************************************************************

  subroutine unpackatom(i,x0,x0nei,x1,x2,x3,x4,x5,atomindex,atype,      &
       j,atbuf,iatbuf,iorig,procorig)
    implicit none
    !
    !  Unpack atom i stuff from atbuf and iatbuf at pos j
    !

    integer i,j
    real*8 x0(*),x0nei(*),x1(*),x2(*),x3(*),x4(*),x5(*),atbuf(*)
    integer atype(*),iatbuf(*),atomindex(*),iorig,procorig

    integer i3,j4,j21,ii

    i3=i*3-3
    j4=j*4
    j21=j*21
    do ii=1,3
       x0(i3+ii)=atbuf(j21+ 0+ii)
       x0nei(i3+ii)=atbuf(j21+ 3+ii)
       x1(i3+ii)=atbuf(j21+ 6+ii)
       x2(i3+ii)=atbuf(j21+ 9+ii)
       x3(i3+ii)=atbuf(j21+12+ii)
       x4(i3+ii)=atbuf(j21+15+ii)
       x5(i3+ii)=atbuf(j21+18+ii)
    enddo
    atype(i)=iatbuf(j4+1)   
    iorig=iatbuf(j4+2)
    procorig=iatbuf(j4+3)
    atomindex(i)=iatbuf(j4+4)

    return 
  end subroutine unpackatom


  !***********************************************************************
