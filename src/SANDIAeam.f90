  
  
  !***********************************************************************
  ! EAM density calculation from Sandia spline tables 
  !
  ! For speed, most subroutines from here have been inlined in EAMforces.f
  !
  ! Unused stuff moved to discardedstuff.f
  !
  !***********************************************************************
  
  
  !***********************************************************************
  !
  ! Read in EAM potential
  !
  ! The file form should be the 'universal 3' EAM format 
  !
  ! Containing:
  ! 
  ! LINE NUMBER      DATA
  ! 1                Comment, ignored
  ! 2                ielement, amass, blat, latname; format (i5,2g15.5,a8)
  ! 3                nP, dP, nr, dr, rcutpot; format (*)
  ! 4 - 4+nP-1       F_p, embedding function F(rho)
  ! previous + nr    Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
  ! previous + nr    P_r, electron density rho(r) 
  !
  !
  ! File name conventions: 
  !
  ! For itype1=itype2: If Xx = elementname as character*2
  !     Either eam.Xx.Xx.in
  !     or     xxu3.fcn       (note: name lowercased !)
  !
  ! For itype1 /= itype2: If elements are Xx and Yy
  !            eam.Xx.Yy.in
  !
  !
  !************************************************************************
  subroutine Init_Pot(rcutread,itype1,itype2,potmode,spline,eamnbands)

    use typeparam
    use datatypes
    use my_mpi

    use defs

    use para_common

    implicit none

    !     ------------------------------------------------------------------
    !     Variables passed in and out
    real*8 rcutread
    integer itype1,itype2,potmode,spline,eamnbands

    !     EAM parameters
    common /EAM_params/rcutpot,dr,dri,nr,nP,dP
    common /EAM_pair_params/Vp_r(EAMTABLESZ),dVpdr_r(EAMTABLESZ),Vpsc(EAMTABLESZ)

    common /EAM_P_params/P_r(EAMTABLESZ),dPdr_r(EAMTABLESZ),Psc(EAMTABLESZ)
    common /EAM_Fp_params/Pa(EAMTABLESZ),F_p(EAMTABLESZ),                 &
         dFdp_p(EAMTABLESZ),Fsc(EAMTABLESZ)

!include 'para_common.f90'
    !     ------------------------------------------------------------------
    !     Local variables and constants
    real*8 ra(EAMTABLESZ), Z_r(EAMTABLESZ), amass, blat, phi0,xx
    integer ielement,l1,l2
    character latname*8,el1*2,el2*2

    integer j,i,nr,np
    real*8 pa,f_p,dpdr_r,psc,dfdp_p,fsc,dP,dri,rcutpot,dr,Vpsc
    real*8 p_r,Vp_r,dVpdr_r

    character*20 filename

    logical, save :: firsttime = .true.

    data phi0 /14.39975d0/

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !     ------------------------------------------------------------------

    if (firsttime) then
       IF(iprint)WRITE(6,*) ''
       IF(iprint)WRITE(6,*) 'PARCAS ELEMENTAL EAM VERSION'
       IF(iprint)WRITE(6,*) ''
       if (spline==1) then
          IF(iprint)WRITE(6,*) 'SPLINE INTERPOLATION USED'
       else
          IF(iprint)WRITE(6,*) 'LINEAR INTERPOLATION USED '
          IF(iprint)WRITE(6,*) 'I HOPE YOU KNOW WHAT YOU''RE DOING'
          IF(iprint)WRITE(6,*) ''
       endif

       if (eamnbands>1) then
          IF(iprint)WRITE(6,*) 'eamnbands > 1 not possible in parcas, use parcas_eamal'
          call my_mpi_abort('eamnbands ERROR', int(eamnbands))
       endif
    endif

    if (.not. firsttime) then
       write(6,*) 'Parcas does not support multiple EAM potentials'
       write(6,*) 'You should compile and use parcas_eamal for that'
       call my_mpi_abort('WRONG PARCAS VERSION FOR EAM ALLOY', int(itype1))
    endif

    firsttime=.false.


    ! This reads in files from the form prepared by S.M. Foiles (1985)
    if (itype1 /= itype2) then
       write (6,*) 'SANDIAeam.f: Can''t handle itype1/=itype2 yet'
       write (6,*) 'Please modify the code to do so'
       write (6,*) itype1,itype2
       call my_mpi_abort('Potential inadequacy', int(myproc))
    endif

    el1=element(itype1); l1=len_trim(el1)
    el2=element(itype2); l2=len_trim(el2)
    write(unit=filename,fmt='(A7,A,A1,A,A3)')                             &
         'in/eam.',el1(1:l1),'.',el2(1:l2),'.in'
    IF(iprint)OPEN(5,file=filename,status='old',err=10)
    goto 30  ! Open succesfull, or myproc/=0

10  if (itype1 == itype2) then
       write(unit=filename,fmt='(A3,A,A6)')                               &
            'in/',el1,'u3.fcn'
       ! Lowercase first char of name if in upper case
       if (iachar(filename(4:4)) >= iachar('A') .and.                     &
            iachar(filename(4:4)) <= iachar('Z')) then
          filename(4:4)=achar(iachar(filename(4:4))                       &
               -iachar('A')+iachar('a'))
       endif
       IF(iprint)OPEN(5,file=filename,status='old',err=19)
       goto 30 ! Open succesfull
    endif
19  write(6,*) 'Failed to open EAM file for types',itype1,itype2
    write(6,*) 'Last tried file',filename
    call my_mpi_abort('EAMpotfile open', int(itype1))

30  continue
    IF(iprint)READ(5,*)
    IF(iprint)READ(5,1000) ielement, amass, blat, latname
    IF(iprint)WRITE(6,1000) ielement, amass, blat, latname
1000 format(i5,2g15.5,a8)

    call mpi_bcast(ielement, 1, my_mpi_integer, 0, mpi_comm_world, ierror)
    call mpi_bcast(amass, 1, mpi_double_precision, 0, mpi_comm_world, ierror)
    call mpi_bcast(blat, 1, mpi_double_precision, 0, mpi_comm_world, ierror)
    call mpi_bcast(latname, 8, mpi_character, 0, mpi_comm_world, ierror)

    ! amass used only for checking purposes
    if (mass(itype1) /= amass) then
       call mpi_barrier(mpi_comm_world, ierror)
       write (6,*) 'myproc itype1 mass amass',                            &
            myproc,itype1,mass(itype1),amass
       call my_mpi_abort('mass discrepancy in md.in and pot', int(myproc))
    endif

    !     REST OF THE SUBROUTINE IS TYPE INDEPENDENT. SHOULD CHANGE THIS.

    IF(iprint)READ(5,*) nP, dP, nr, dr, rcutpot

    IF(iprint)WRITE(6,'(I6,G14.6,I6,2G14.6)') nP, dP, nr, dr, rcutpot

    call mpi_bcast(nP, 1, my_mpi_integer, 0, mpi_comm_world, ierror)
    call mpi_bcast(dP, 1, mpi_double_precision, 0, mpi_comm_world, ierror)
    call mpi_bcast(nr, 1, my_mpi_integer, 0, mpi_comm_world, ierror)
    call mpi_bcast(dr, 1, mpi_double_precision, 0, mpi_comm_world, ierror)
    call mpi_bcast(rcutpot, 1, mpi_double_precision, 0, mpi_comm_world, ierror)

    IF(debug)PRINT *,nP,dP,nr,dr,rcutpot
    if (nP>EAMTABLESZ .or. nr>EAMTABLESZ) then
       write (6,*) 'Init_Pot ERROR: Increase EAMTABLESZ in defs.h'
       write (6,*) 'to correspond to EAM table size'
       write (6,*) myproc,nP,nr
       call my_mpi_abort('EAM table size', int(nr))
    endif

    !IF(iprint)READ(5,*) (F_p(j),j=1,nP)
    do j=1,nP
       IF(iprint)READ(5,*) xx
       !print *,j,xx
       F_p(j)=xx
    enddo
    call mpi_bcast(F_p, nP, mpi_double_precision, 0, mpi_comm_world, ierror)
    IF(debug)PRINT *,'F(p) read in',nP
    !IF(iprint)READ(5,*) (Z_r(j),j=1,nr)
    do j=1,nr
       IF(iprint)READ(5,*) xx
       !print *,j,xx
       Z_r(j)=xx
    enddo


    call mpi_bcast(Z_r, nr, mpi_double_precision, 0, mpi_comm_world, ierror)
    IF(debug)PRINT *,'Z(r) read in',nr
    !IF(iprint)READ(5,*) (P_r(j),j=1,nr)
    do j=1,nr
       IF(iprint)READ(5,*) xx
       !print *,j,xx
       P_r(j)=xx
    enddo
    call mpi_bcast(P_r, nr, mpi_double_precision, 0, mpi_comm_world, ierror)
    IF(debug)PRINT *,'P(r) read in',nr

    IF(iprint)CLOSE(5)
    rcutread=rcutpot
    IF(debug)PRINT *,'Pot. read in',rcutread,myproc

    ! Setup P(r), the charge density at a distance r from an atom

    do i=1,nr
       ra(i) = DBLE(i-1)*dr
    enddo
    if (spline==1) then
       call Spl2b2(nr,ra,P_r,dPdr_r,zero,zero)
       do i=1,nr-1
          Psc(i)=six*(P_r(i+1)-P_r(i))/dr -                                   &
               three*(dPdr_r(i+1)+dPdr_r(i))
       enddo
       Psc(nr)=zero
    endif

    ! Setup F(P), the embedding energy for a charge density P

    do i=1,nP
       Pa(i)=DBLE(i-1)*dP
    enddo
    if (spline==1) then
       call Spl2b2(nP,Pa,F_p,dFdp_p,zero,zero)
       do i=1,nP-1
          Fsc(i)=six*(F_p(i+1)-F_p(i))/dP -                                   &
               three*(dFdp_p(i)+dFdp_p(i+1))
       enddo
       Fsc(nP)=zero
    endif
    ! Pair Potential - convert  Z(r)  ==>  Vp(r) = Phi0 * Z(r)*Z(r)/r 

    if (potmode .eq. 2 .and. iprint) then
       write (6,*) 'Potential mode 2 selected, Vp(r) read in directly'
    endif

    !     Set first element to second to avoid infinity for r=0
    Vp_r(1)=Z_r(1)
    dri=one/dr
    do i=2,nr
       if (potmode /= 2) then
          Vp_r(i)=phi0*Z_r(i)**2/ra(i)
       else
          Vp_r(i)=Z_r(i)
       endif
    enddo
    if (potmode==1) Vp_r(1)=Vp_r(2)

    if (spline==1) then
       call Spl1b2(nr,ra,Vp_r,dVpdr_r,zero,zero)
       do i=1,nr-1
          Vpsc(i)=six*(Vp_r(i+1)-Vp_r(i))/dr -                           &
               three*(dVpdr_r(i+1)+dVpdr_r(i))
       enddo
       Vpsc(i)=zero
    endif

  end subroutine Init_Pot
  !***********************************************************************
  subroutine Make_Cross_Pot(rcutread,itype1,itype2,potmode,spline)
    use my_mpi
    use defs
    implicit none

    real*8 rcutread
    integer itype1,itype2,potmode,spline

    write(6,*) 'Plain parcas does not support EAM cross potentials !'
    write(6,*) 'You should compile and use parcas_eamal for that'

    call my_mpi_abort('WRONG PARCAS VERSION FOR EAM ALLOY', int(itype1))

    return
  end subroutine Make_Cross_Pot

  !***********************************************************************
  ! Cubic Spline interpolation routine and setup routines Spl1b2 & Spl2b2
  !***********************************************************************
  !       xa( )   : n knots of (tabulated values)
  !       ya( )   : function at n knots (tabulated values)
  !       x       : variable for interpolation
  !       y       : function at interpolation point x (out)
  !       y1,y2   : 1st and 2nd prime at interpolation point x (out)
  !       y,y1,y2 : input for citerion
  !
  !***********************************************************************
  subroutine Splt2(n,xa,ya,y1a,sc,x,y,y1)
    use defs

    use para_common

    implicit none

    !      IMPLICIT REAL*8 (a-h,o-z)
    !      IMPLICIT INTEGER(i-n)

    integer k,khi,n,klo
    real*8 a,b,h,ab,ylo,yhi,yplo,yphi,ya,y1a,xa,sc,y1,x,y

    dimension xa(n),ya(n),y1a(n),sc(n-1)
!include 'para_common.f90'

    klo=1
    khi=n
1   if (khi-klo .gt. 1) then
       k=(khi+klo)/2
       if(xa(k) .gt. x) then
          khi=k
       else
          klo=k
       endif
       goto 1
    endif
    h=xa(khi)-xa(klo)
    a=(xa(khi)-x)/h
    b=one-a
    ab=a*b
    yplo=y1a(klo)*a
    yphi=y1a(khi)*b
    ylo=ya(klo)*a
    yhi=ya(khi)*b
    y=a*ylo+b*yhi+ab*(two*(ylo+yhi)+h*(yplo-yphi))
    y1=ab*sc(klo)+yplo+yphi

  end subroutine Splt2

  !***********************************************************************
  subroutine Spl1b2(n,xa,ya,y1a,y11,yn1)
    use defs

    use para_common

    implicit none

    ! calculate yp(i) (y1a) : 1st prime at knots
    ! from fortran complement volume 1 / page 51

    ! cubic spline for first boundary condition

    !       xa( ),ya( ) : knots (input)
    !       y1a( )      : 1st prime at knots (output)
    !       y11,yn1     : 1st prime at boundaries (input)
    !       n           : number of knots (input)

    !      IMPLICIT REAL*8 (a-h,o-z)


    integer n
    real*8 xa(n),ya(n),y1a(n),b(EAMTABLESZ)
!include 'para_common.f90'

    integer j,n1
    real*8 h,h1,bt,af,y11,yn1

    y1a(1)=one
    b(1)=y11
    n1=n-1
    do j=2,n1
       h1=xa(j)-xa(j-1)
       h=xa(j+1)-xa(j)
       af=h1/(h1+h)
       bt=three*((one-af)*(ya(j)-ya(j-1))/h1+af*(ya(j+1)-ya(j))/h)
       y1a(j)=-af/(two+(one-af)*y1a(j-1))
       b(j)=(bt-(one-af)*b(j-1))/(two+(one-af)*y1a(j-1))
    enddo
    y1a(n)=yn1
20  y1a(n1)=y1a(n1)*y1a(n1+1)+b(n1)
    n1=n1-1
    if (n1 .gt. 0) goto 20
  end subroutine Spl1b2

  !***********************************************************************
  subroutine Spl2b2(n,xa,ya,y1a,y12,yn2)
    use defs

    use para_common

    implicit none

    ! calculate yp(i) (y1a) : 1st prime at knots

    ! from fortran compliment volume 1 / page 57
    ! cubic spline for second boundary condition

    !    xa( ),ya( ) : knots (input)
    !    y1a( )        : 1st prime at knots (output)
    !    y12,yn2       : 2nd prime at boundaries (input)
    !    n        : number of knots (input)

    !      IMPLICIT REAL*8 (a-h,o-z)
    !      IMPLICIT INTEGER(i-n)

    integer n
    dimension xa(n),ya(n),y1a(n),b(EAMTABLESZ)

!include 'para_common.f90'

    integer j,n1
    real*8 h1,y1,b,bt,af,y,ya,xa,y1a,h,yn2,y12


    h=xa(2)-xa(1)
    y=ya(2)-ya(1)
    y1a(1)=-half
    b(1)=1.5*y/h-0.25*h*y12
    n1=n-1
    do j=2,n1
       h1=h
       y1=y
       h=xa(j+1)-xa(j)
       y=ya(j+1)-ya(j)
       af=h1/(h1+h)
       bt=three*((one-af)*y1/h1+af*y/h)
       y1a(j)=-af/(two+(one-af)*y1a(j-1))
       b(j)=(bt-(one-af)*b(j-1))/(two+(one-af)*y1a(j-1))
    enddo
    y1a(n)=(three*y/h+h*yn2/two-b(n-1))/(two+y1a(n-1))
20  y1a(n1)=y1a(n1)*y1a(n1+1)+b(n1)
    n1=n1-1
    if (n1 .gt. 0) goto 20
  end subroutine Spl2b2

