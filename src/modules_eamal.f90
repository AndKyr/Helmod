module EAM
  implicit none

  ! 
  ! Since in Fortran the innermost index runs fastest, the indexing here is:
  ! (r,type1,type2)
  !

  real*8, allocatable :: rcutpot(:,:),dr(:,:),dri(:,:),dP(:,:)
  integer, allocatable ::  nr(:,:),nP(:,:)

  real*8, allocatable :: pbufeamal(:,:),dbufeamal(:,:)

  integer eamrtablesz,eamptablesz

  ! EAM pair params - pair potential parameters

  real*8, allocatable :: Vp_r(:,:,:),dVpdr_r(:,:,:),Vpsc(:,:,:)

  ! EAM P params - electron density parameters (read P as rho !)
  real*8, allocatable :: P_r(:,:,:,:),dPdr_r(:,:,:,:),Psc(:,:,:,:)

  ! EAM Fp params - Embedding energy parameters 
  real*8, allocatable :: F_p(:,:,:),dFdp_p(:,:,:),Fsc(:,:,:)

end module EAM
