  
  !***********************************************************************
  ! EAM density calculation from Sandia spline tables
  !
  ! For speed, most subroutines from here have been inlined in EAMforces.f
  !
  ! Unused stuff moved to discardedstuff.f
  !
  !***********************************************************************
  
  
  !***********************************************************************
  !
  ! Read in one EAM potential
  !
  ! The file form should be the 'universal 3' EAM format 
  !
  ! Containing for atype1=atype2:
  ! 
  ! LINE NUMBER      DATA
  ! 1                Comment, ignored
  ! 2                ielement, amass, blat, latname; format (i5,2g15.5,a8)
  ! 3                nP, dP, nr, dr, rcutpot; format (*)
  ! 4 - 4+nP-1       F_p, embedding function F(rho)
  ! previous + nr    Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
  ! previous + nr    P_r, electron density rho(r) 
  !
  ! Containing for atype1/=atype2 with iac=1:
  ! 
  ! LINE NUMBER      DATA
  ! 1                Comment, ignored
  ! 2                nr, dr, rcutpot; format (*)
  ! 3 - 3+nr-1       Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
  !
  !
  ! Containing for atype1/=atype2 with iac=4:
  ! 
  ! LINE NUMBER      DATA
  ! 1                Comment, ignored
  ! 2                nr, dr, rcutpot; format (*)
  ! 3 - 3+nr-1       Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
  ! previous + nr    P_r, electron density rho(r) 
  !
  ! File name conventions: 
  !
  ! For itype1=itype2: If Xx = elementname as character*2
  !     Either eam.Xx.Xx.in
  !     or     xxu3.fcn       (note: name lowercased !)
  !
  ! For itype1 /= itype2: If elements are Xx and Yy
  !            eam.Xx.Yy.in
  !
  !* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
  !
  ! Many-band models (introduced in parcas V3.74beta3):
  !
  ! - New parameter eamnbands tells how many bands there are. Band index is n
  ! - Bands are in the form of additional embedding energy functions
  !   for each element i: F_ii^n
  ! - For each new embedding energy function there is a new electron
  !   density rho_ii^n
  ! - The pair potential part is unaffected!!
  !
  ! - Mixed electron density may be problematic, since in at least Wallenius
  !   PRB (2004) paper d-band is handled as ordinary EAM, s-band as mixed
  !   This should be handled with different iac options
  ! - For Wallenius PRB (2004) use iac=6 which makes bands>1 have their
  !   own mixed electron density
  ! - In this case the elemental s-band electron density is ignored,
  !   but it still needs to be in the file for consistency in the read-in routines
  !
  ! - The function read-in format for a 2-band model, iac=6 is:
  !
  !
  ! LINE NUMBER      DATA
  ! 1                Comment, ignored
  ! 2                ielement, amass, blat, latname; format (i5,2g15.5,a8)
  ! 3                nP, dP, nr, dr, rcutpot, nP2, dP2; format (*)
  ! 4 - 4+nP-1       F_p^1, embedding function F^1(rho)
  ! previous + nr    Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
  ! previous + nr    P_r^1, electron density rho^1(r)
  ! previous + nP    F_p^2, embedding function F^2(rho)
  ! previous + nr    P_r^2, electron density rho^2(r)
  !
  ! Containing for atype1/=atype2 with iac=6:
  ! 
  ! LINE NUMBER      DATA
  ! 1                Comment, ignored
  ! 2                nr, dr, rcutpot; format (*)
  ! 3 - 3+nr-1       Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
  ! previous + nr    P_r^2, electron density rho^2(r) 
  !
  !
  !************************************************************************
  subroutine Init_Pot(rcutread,itype1,itype2,potmode,spline,eamnbands)

    use typeparam
    use datatypes
    use my_mpi

    use EAM

    use defs

    use para_common

    implicit none

    !     ------------------------------------------------------------------
    !     Variables passed in and out
    real*8 rcutread
    integer itype1,itype2,potmode,spline,eamnbands

!include 'para_common.f90'
    !     ------------------------------------------------------------------
    !     Local variables and constants

    !**AK**
    real*8, allocatable :: readbuf(:),ra(:),Pa(:),Z_r(:)
    !REAL*8, save :: readbuf(EAMTABLESZ),ra(EAMTABLESZ),Pa(EAMTABLESZ),Z_r(EAMTABLESZ)

    real*8 amass, blat, phi0,xx
    integer ielement,l1,l2,nPmax_local
    character latname*8,el1*2,el2*2

    integer i,j,i1,i2,iband,nnr,nnp

    character*20 filename

    logical, save :: firsttime = .true.

    data phi0 /14.39975d0/

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !     ------------------------------------------------------------------

    if (firsttime) then
       IF(iprint)WRITE(6,*) ''
       IF(iprint)WRITE(6,*) 'PARCAS ALLOY EAM VERSION'
       IF(iprint)WRITE(6,*) ''
       if (eamnbands > 1) then
          IF(iprint)WRITE(6,*) '    - RAN FOR NBAND MODEL WITH N=',eamnbands
          IF(iprint)WRITE(6,*) ''
       endif
    endif

    if (spline /= 1) then
       print *,'parcas_eamal does not yet support linear interpolation'
       call my_mpi_abort('no linear interpolation in eamal', int(spline))
    endif

    if (eamnbands > EAMBANDMAX) then
       IF(iprint)WRITE (6,*) 'ERROR: eamnbands > EAMBANDMAX. Increase EAMBANDMAX'
       call my_mpi_abort('EAMBANDMAX too small', int(EAMBANDMAX))
    endif


    ! This reads in files from the form prepared by S.M. Foiles (1985)
    i1=itype1
    i2=itype2

    if (itype1 /= itype2) then
       IF(iprint)WRITE (6,*) 'Reading in cross EAM pair potential for'
       IF(iprint)WRITE (6,*) 'atom types',itype1,itype2
       if (iac(itype1,itype2) == 4) then
          IF(iprint)WRITE (6,*) 'Also reading in electron density cross term'
       endif
    endif

    el1=element(itype1); l1=len_trim(el1)
    el2=element(itype2); l2=len_trim(el2)
    write(unit=filename,fmt='(A7,A,A1,A,A3)')                             &
         'in/eam.',el1(1:l1),'.',el2(1:l2),'.in'
    IF(iprint)OPEN(5,file=filename,status='old',err=10)
    goto 30  ! Open succesfull, or myproc/=0

10  if (itype1 == itype2) then
       write(unit=filename,fmt='(A3,A,A6)')                               &
            'in/',el1,'u3.fcn'
       ! Lowercase first char of name if in upper case
       if (iachar(filename(4:4)) >= iachar('A') .and.                     &
            iachar(filename(4:4)) <= iachar('Z')) then
          filename(4:4)=achar(iachar(filename(4:4))                       &
               -iachar('A')+iachar('a'))
       endif
       IF(iprint)OPEN(5,file=filename,status='old',err=19)
       goto 30 ! Open succesfull
    endif
19  write(6,*) 'Failed to open EAM file for types',itype1,itype2
    write(6,*) 'Last tried file',filename
    call my_mpi_abort('EAMpotfile open', int(itype1))

30  continue

    IF(iprint)WRITE(6,*) 'Now reading EAM file',filename


    if (firsttime) then
       IF(iprint)WRITE(6,*) 'parcas_eamal allocating 1'
       ! Allocate basic variables
       allocate(nP(itypelow:itypehigh,eamnbands))
       allocate(dP(itypelow:itypehigh,eamnbands))
       allocate(nr(itypelow:itypehigh,itypelow:itypehigh))
       allocate(dr(itypelow:itypehigh,itypelow:itypehigh))
       allocate(dri(itypelow:itypehigh,itypelow:itypehigh))
       allocate(rcutpot(itypelow:itypehigh,itypelow:itypehigh))

       allocate(pbufeamal(3*NPMAX,EAMBANDMAX))
       allocate(dbufeamal(NPMAX,EAMBANDMAX))
    endif

    IF(iprint)READ(5,*)
    if (itype1==itype2) then
       IF(iprint)READ(5,1000) ielement, amass, blat, latname
       IF(iprint)WRITE(6,1000) ielement, amass, blat, latname
1000   format(i5,2g15.5,a8)

       ! ielement and blat ignored, amass used only for checking
       call mpi_bcast(ielement, 1, my_mpi_integer, 0, mpi_comm_world, ierror)
       call mpi_bcast(amass, 1, mpi_double_precision, 0, mpi_comm_world, ierror)
       call mpi_bcast(blat, 1, mpi_double_precision, 0, mpi_comm_world, ierror)
       call mpi_bcast(latname, 8, mpi_character, 0, mpi_comm_world, ierror)

       ! amass check
       if (mass(itype1) /= amass) then
          call mpi_barrier(mpi_comm_world, ierror)
          write (6,*) 'EAM: myproc itype1 mass amass',                    &
               myproc,itype1,mass(itype1),amass
          call my_mpi_abort('mass discrepancy in md.in/pot', int(itype1))
       endif
    endif

    if (itype1==itype2) then
       if (eamnbands == 1) then
          IF(iprint)READ(5,*) nP(i1,1), dP(i1,1), nr(i1,i2),                              &
               dr(i1,i2), rcutpot(i1,i2)
       else if (eamnbands == 2) then
          IF(iprint)READ(5,*) nP(i1,1), dP(i1,1), nr(i1,i2),                              &
               dr(i1,i2), rcutpot(i1,i2),nP(i1,2), dP(i1,2)
       else if (eamnbands == 3) then
          IF(iprint)READ(5,*) nP(i1,1), dP(i1,1), nr(i1,i2),                              &
               dr(i1,i2), rcutpot(i1,i2), nP(i1,2), dP(i1,2), nP(i1,3), dP(i1,3)
       else
          print *,'Cannot read in nP line for more than 3 bands, please'
          print *,'modify SANDIAeam_eamal.f90 to fix'
       endif
       do iband=1,eamnbands
          call mpi_bcast(nP(i1,iband), 1, my_mpi_integer, 0, mpi_comm_world, ierror)
          call mpi_bcast(dP(i1,iband), 1, mpi_double_precision, 0, mpi_comm_world, ierror)
       enddo
    else
       IF(iprint)READ(5,*) nr(i1,i2), dr(i1,i2), rcutpot(i1,i2)
    endif
    call mpi_bcast(nr(i1,i2), 1, my_mpi_integer, 0, mpi_comm_world, ierror)
    call mpi_bcast(dr(i1,i2), 1, mpi_double_precision, 0, mpi_comm_world, ierror)
    call mpi_bcast(rcutpot(i1,i2), 1, mpi_double_precision, 0, mpi_comm_world, ierror)

    IF(debug)PRINT *,nP(i1,1),dP(i1,1),nr(i1,i2),dr(i1,i2),rcutpot(i1,i2)

    nPmax_local=0
    do iband=1,eamnbands
       if (nP(i1,iband) > nPmax_local) nPmax_local=nP(i1,iband)
    enddo
    if (firsttime) then
       IF(iprint)WRITE(6,*) 'parcas_eamal allocating 2'   
       ! Allocate all permanent EAM tables
       ! Reserve some extra space for possible larger tables of later
       ! atom types
       nnr=nr(i1,i2)+EAMTABLESZ
       nnp=nPmax_local+EAMTABLESZ
       eamrtablesz=nnr
       eamptablesz=nnp

       ! Pair potential (r)
       allocate(Vp_r(nnr,itypelow:itypehigh,itypelow:itypehigh))
       allocate(dVpdr_r(nnr,itypelow:itypehigh,itypelow:itypehigh))
       allocate(Vpsc(nnr,itypelow:itypehigh,itypelow:itypehigh))

       ! Electron density (r)
       allocate(P_r(nnr,itypelow:itypehigh,itypelow:itypehigh,eamnbands))
       allocate(dPdr_r(nnr,itypelow:itypehigh,itypelow:itypehigh,eamnbands))
       allocate(Psc(nnr,itypelow:itypehigh,itypelow:itypehigh,eamnbands))

       ! Embedding energy tables (rho)
       allocate(F_p(nnp,itypelow:itypehigh,eamnbands))
       allocate(dFdp_p(nnp,itypelow:itypehigh,eamnbands))
       allocate(Fsc(nnp,itypelow:itypehigh,eamnbands))
    endif

    if (i1==i2) then
       if (nPmax_local>eamptablesz .or. nr(i1,i2)>eamrtablesz) then
          write (6,*) 'Init_Pot ERROR: later EAM P potential has larger'
          write (6,*) 'table size than earlier + EAMTABLE SIZE'
          write (6,*) myproc,i1,i2,nPmax_local,nr(i1,i2)
          call my_mpi_abort('EAMTABLE', int(nr(i1,i2)))
       endif
       !**AK**
       !if (firsttime) then      
       IF(iprint)WRITE(6,*) 'parcas_eamal allocating 3a'
       allocate(Pa(nPmax_local))       
       allocate(readbuf(max(nr(i1,i2),nPmax_local)))
       !endif
    else
       !**AK**
       !if (firsttime) then
       IF(iprint)WRITE(6,*) 'parcas_eamal allocating 3b'
       allocate(readbuf(nr(i1,i2)))
       !endif
    endif
    !**AK**
    !if (firsttime) then
    IF(iprint)WRITE(6,'(a,10i6)') 'parcas_eamal allocating 4',i1,i2,nr(i1,i2)
    allocate(ra(nr(i1,i2)))
    allocate(Z_r(nr(i1,i2)))
    !endif

    if (nr(i1,i2)>eamrtablesz) then
       write (6,*) 'Init_Pot ERROR: later EAM r potential has larger'
       write (6,*) 'table size than earlier + EAMTABLE SIZE'
       write (6,*) myproc,i1,i2,nr(i1,i2),eamrtablesz
       call my_mpi_abort('EAMTABLE', int(nr(i1,i2)))
    endif

    if (i1==i2) then
       !IF(iprint)READ(5,*) (readbuf(j),j=1,nP(i1,1))
       do j=1,nP(i1,1)
          IF(iprint)READ(5,*) xx
          !print *,j,xx
          readbuf(j)=xx
       enddo
       call mpi_bcast(readbuf, nP(i1,1), mpi_double_precision, 0, &
            mpi_comm_world, ierror)
       do j=1,nP(i1,1)
          F_p(j,i1,1)=readbuf(j)
       enddo
       IF(debug)PRINT *,'F(p) read in',nP(i1,1),i1
    endif


    !IF(iprint)READ(5,*) (readbuf(j),j=1,nr(i1,i2))
    do j=1,nr(i1,i2)
       IF(iprint)READ(5,*) xx
       !print *,j,xx
       readbuf(j)=xx
    enddo
    call mpi_bcast(readbuf, nr(i1,i2), mpi_double_precision, 0, &
         mpi_comm_world, ierror)
    do j=1,nr(i1,i2)
       Z_r(j)=readbuf(j)
    enddo
    IF(debug)PRINT *,'Z(r) read in',nr(i1,i2),i1,i2

    if (i1==i2 .or. iac(i1,i2)==4) then
       !IF(iprint)READ(5,*) (readbuf(j),j=1,nr(i1,i2))
       do j=1,nr(i1,i2)
          IF(iprint)READ(5,*) xx
          !print *,j,xx
          readbuf(j)=xx
       enddo
       call mpi_bcast(readbuf, nr(i1,i2), mpi_double_precision, 0, &
            mpi_comm_world, ierror)
       do j=1,nr(i1,i2)
          P_r(j,i1,i2,1)=readbuf(j)
       enddo
       IF(debug)PRINT *,'P(r) read in',nr(i1,i2),i1,i2
    endif

    if (eamnbands > 1) then
       ! Handle many-body model read-in
       do iband=2,eamnbands
          if (i1==i2) then
             !IF(iprint)READ(5,*) (readbuf(j),j=1,nP(i1,iband))
             do j=1,nP(i1,iband)
                IF(iprint)READ(5,*) xx
                !print *,j,xx
                readbuf(j)=xx
             enddo
             call mpi_bcast(readbuf, nP(i1,iband), mpi_double_precision, 0,  &
                  mpi_comm_world, ierror)
             do j=1,nP(i1,iband)
                F_p(j,i1,iband)=readbuf(j)
             enddo
             IF(iprint)WRITE (6,*) 'nband F(p) read in',nP(i1,iband),i1,iband           
          endif
          if (i1==i2 .or. iac(i1,i2)==6) then
             !IF(iprint)READ(5,*) (readbuf(j),j=1,nr(i1,i2))
             do j=1,nr(i1,i2)
                IF(iprint)READ(5,*) xx
                !print *,j,xx
                readbuf(j)=xx
             enddo
             call mpi_bcast(readbuf, nr(i1,i2), mpi_double_precision, 0, &
                  mpi_comm_world, ierror)
             do j=1,nr(i1,i2)
                P_r(j,i1,i2,iband)=readbuf(j)
             enddo
             IF(iprint)WRITE (6,*) 'nband P(r) read in',nr(i1,i2),i1,i2,iband
          endif
       enddo
    endif

    IF(iprint)CLOSE(5)

    rcutread=rcutpot(i1,i2)
    IF(debug)PRINT *,'Pot. read in',rcutread,myproc,i1,i2

    do i=1,nr(i1,i2)
       ra(i) = DBLE(i-1)*dr(i1,i2)
    enddo

    if (i1==i2 .or. iac(i1,i2)==4) then
       ! Setup P(r), the charge density at a distance r from an atom
       call Spl2b2_y4d(eamrtablesz,nr(i1,i2),ra,P_r,dPdr_r,i1,i2,zero,zero,eamnbands,1)
       do i=1,nr(i1,i2)-1
          Psc(i,i1,i2,1)=six*(P_r(i+1,i1,i2,1)-P_r(i,i1,i2,1))/dr(i1,i2) -               &
               three*(dPdr_r(i+1,i1,i2,1)+dPdr_r(i,i1,i2,1))
          !write (91,*) i,P_r(i,i1,i2,1),dPdr_r(i,i1,i2,1),Psc(i,i1,i2,1)
       enddo
    endif
    if (eamnbands > 1) then
       ! Handle many-body model eldens
       do iband=2,eamnbands
          if (i1==i2 .or. iac(i1,i2)==6) then
             ! Setup P(r), the charge density at a distance r from an atom

             call Spl2b2_y4d(eamrtablesz,nr(i1,i2),ra,P_r,dPdr_r,i1,i2,zero,zero,eamnbands,iband)
             do i=1,nr(i1,i2)-1
                Psc(i,i1,i2,iband)=six*(P_r(i+1,i1,i2,iband)-P_r(i,i1,i2,iband))/dr(i1,i2) -               &
                     three*(dPdr_r(i+1,i1,i2,iband)+dPdr_r(i,i1,i2,iband))
                !write (91,*) i,P_r(i,i1,i2,iband),dPdr_r(i,i1,i2,iband),Psc(i,i1,i2,iband)
             enddo
          endif
       enddo
    endif


    if (i1==i2) then     
       !        Setup F(P), the embedding energy for a charge density P
       do iband=1,eamnbands
          do i=1,nP(i1,iband)
             Pa(i)=DBLE(i-1)*dP(i1,iband)
          enddo
          call Spl2b2_y3d(eamptablesz,nP(i1,iband),Pa,F_p,dFdp_p,i1,zero,zero,eamnbands,iband)
          do i=1,nP(i1,iband)-1
             Fsc(i,i1,iband)=six*(F_p(i+1,i1,iband)-F_p(i,i1,iband))/dP(i1,iband) -   &
                  three*(dFdp_p(i,i1,iband)+dFdp_p(i+1,i1,iband))
          enddo
          Fsc(nP(i1,iband),i1,iband)=zero
       enddo
    endif


    ! Pair Potential - convert  Z(r)  ==>  Vp(r) = Phi0 * Z(r)*Z(r)/r 

    if (potmode .eq. 2 .and. iprint) then
       write (6,*) 'Potential mode 2 selected, Vp(r) read in directly'
    endif


    if (potmode /= 2) then
       Vp_r(1,i1,i2)=phi0*Z_r(1)**2/ra(2)
    else
       Vp_r(1,i1,i2)=Z_r(1)
    endif
    dri(i1,i2)=one/dr(i1,i2)
    do i=2,nr(i1,i2)
       if (potmode /= 2) then
          Vp_r(i,i1,i2)=phi0*Z_r(i)**2/ra(i)
       else
          Vp_r(i,i1,i2)=Z_r(i)
       endif
    enddo
    !     Set first element to second to avoid infinity for r=0
    if (potmode==1) Vp_r(1,i1,i2)=Vp_r(2,i1,i2)

    call Spl1b2(eamrtablesz,nr(i1,i2),ra,Vp_r,dVpdr_r,i1,i2,zero,zero)
    do i=1,nr(i1,i2)-1
       Vpsc(i,i1,i2)=six*(Vp_r(i+1,i1,i2)-Vp_r(i,i1,i2))/dr(i1,i2) -       &
            three*(dVpdr_r(i+1,i1,i2)+dVpdr_r(i,i1,i2))
    enddo

    firsttime=.false.

    !**AK**
    if (allocated(readbuf)) deallocate(readbuf)
    if (allocated(ra)) deallocate(ra)
    if (allocated(Pa)) deallocate(Pa)
    if (allocated(Z_r)) deallocate(Z_r)


  end subroutine Init_Pot

  !***********************************************************************
  subroutine Make_Cross_Pot(rcutread,itype1,itype2,potmode)

    use EAM
    use my_mpi
    use defs

    use para_common

    implicit none

    real*8 rcutread
    integer itype1,itype2,potmode

    real*8, allocatable, save :: ra(:),ya(:),ya1(:),ysc(:)
    integer i,ii1,ii2,i1,i2,nmax
    real*8 x,Vp2,dVp2
    logical, save :: firsttime = .true.

!include 'para_common.f90'

    IF(iprint)WRITE(6,'(A,A,2I2)') 'Constructing EAM cross potential',             &
         ' for atom types',itype1,itype2

    i1=itype1; i2=itype2;

    ! The cross potential in EAM is simply the geometric average of 
    ! V1 and V2
    !
    ! In constructing the cross potential, use the thinner grid
    ! to interpolate data into the denser one.
    ! The end result has the dr and nr of the denser grid.
    ! This procedure should also be always symmetric !
    if (dr(i1,i1)<=dr(i2,i2)) then
       nr(i1,i2)=nr(i1,i1)
       dr(i1,i2)=dr(i1,i1)
       dri(i1,i2)=dri(i1,i1)
       rcutpot(i1,i2)=rcutpot(i1,i1)
       ii1=i1
       ii2=i2
    else
       nr(i1,i2)=nr(i2,i2)
       dr(i1,i2)=dr(i2,i2)
       dri(i1,i2)=dri(i2,i2)
       rcutpot(i1,i2)=rcutpot(i2,i2)
       ii1=i2
       ii2=i1            
    endif

    nmax=max(nr(ii2,ii2),nr(i1,i2))
    if (firsttime) then
       IF(iprint)WRITE(6,*) 'parcas_eamal cross pot allocating 4'
       allocate(ra(nmax))
       allocate(ya(nmax))
       allocate(ya1(nmax))
       allocate(ysc(nmax))
    endif

    write(6,'(A,2I2,A,I6,2F10.5)') 'Cross potential',                     &
         i1,i2,' has n dr rcut',nr(i1,i2),dr(i1,i2),rcutpot(i1,i2)

    do i=1,nr(ii2,ii2)
       ra(i) = DBLE(i-1)*dr(ii2,ii2)
       ya(i) = Vp_r(i,ii2,ii2)
       ya1(i) = dVpdr_r(i,ii2,ii2)
       ysc(i) = Vpsc(i,ii2,ii2)
    enddo
    do i=1,nr(i1,i2)
       ! Get Vp_r of V2 at r1 using spline interpolation
       x=DBLE(i-1)*dr(i1,i2)
       if (x<rcutpot(ii2,ii2)) then
          call splt2(nr(ii2,ii2),ra,ya,ya1,ysc,x,Vp2,dVp2)
       else
          Vp2=zero; dVp2=zero;
       endif
       ! Then get geometric average of the two
       if (Vp_r(i,ii1,ii1)>zero .and. Vp2>zero) then
          Vp_r(i,i1,i2)=sqrt(Vp_r(i,ii1,ii1)*Vp2)
       else
          Vp_r(i,i1,i2)=zero
       endif
       if (Vp_r(i,ii1,ii1)<zero .or. Vp2<zero) then
          write(6,*) 'HORROR ERROR: Attempting to create automatic EAM'
          write(6,*) 'cross potential, but one of potentials is < 0'
          write(6,*) 'ii1 ii2 r Vii1 Vp2',x,ii1,ii2,Vp_r(i,ii1,ii1),Vp2
          call my_mpi_abort('iac 3 component negative', int(i))
       endif

    enddo
    do i=1,nr(i1,i2)
       ra(i) = DBLE(i-1)*dr(i1,i2)
    enddo


    call Spl1b2(eamrtablesz,nr(i1,i2),ra,Vp_r,dVpdr_r,i1,i2,zero,zero)
    do i=1,nr(i1,i2)-1
       Vpsc(i,i1,i2)=six*(Vp_r(i+1,i1,i2)-Vp_r(i,i1,i2))/dr(i1,i2) -      &
            three*(dVpdr_r(i+1,i1,i2)+dVpdr_r(i,i1,i2))
       !write(94,*) (i-1)*dr(i1,i1),Vp_r(i,i1,i1)
       !write(95,*) (i-1)*dr(i1,i2),Vp_r(i,i1,i2)
       !write(96,*) (i-1)*dr(i2,i2),Vp_r(i,i2,i2)
    enddo

    rcutread=rcutpot(i1,i2)

    firsttime=.false.

    return
  end subroutine Make_Cross_Pot

  !***********************************************************************
  ! Cubic Spline interpolation routine and setup routines Spl1b2 & Spl2b2
  !***********************************************************************
  !       xa(:)   : n knots of (tabulated values)
  !       ya(:,:,:)   : function at n knots (tabulated values)
  !       x       : variable for interpolation
  !       y       : function at interpolation point x (out)
  !       y1,y2   : 1st and 2nd prime at interpolation point x (out)
  !       y,y1,y2 : input criterion
  !
  ! Actual interpolation done directly in EAMforces.f, no subroutines
  ! used for that for maximum efficiency ! Only initialization routines
  ! should use splt2
  !
  !***********************************************************************
  subroutine Splt2(n,xa,ya,y1a,sc,x,y,y1)
    use defs
    use para_common
    implicit none

    !     IMPLICIT REAL*8(a-h,o-z)
    !     IMPLICIT INTEGER(i-n)

    integer k,khi,n,klo
    real*8 a,b,h,ab,ylo,yhi,yplo,yphi,ya,y1a,xa,sc,y1,x,y

    dimension xa(n),ya(n),y1a(n),sc(n-1)
!include 'para_common.f90'

    klo=1
    khi=n
1   if (khi-klo .gt. 1) then
       k=(khi+klo)/2
       if(xa(k) .gt. x) then
          khi=k
       else
          klo=k
       endif
       goto 1
    endif
    h=xa(khi)-xa(klo)
    a=(xa(khi)-x)/h
    b=one-a
    ab=a*b
    yplo=y1a(klo)*a
    yphi=y1a(khi)*b
    ylo=ya(klo)*a
    yhi=ya(khi)*b
    y=a*ylo+b*yhi+ab*(two*(ylo+yhi)+h*(yplo-yphi))
    y1=ab*sc(klo)+yplo+yphi

  end subroutine Splt2

  !***********************************************************************

  subroutine Spl1b2(sz,n,xa,ya,y1a,i1,i2,y11,yn1)

    use typeparam

    use defs
    use para_common

    implicit none

    ! calculate yp(i) (y1a) : 1st prime at knots
    ! from fortran compliment volume 1 / page 51

    ! cubic spline for first boundary condition

    !       xa(:),ya(:,:,:) : knots (input)
    !       y1a( )      : 1st prime at knots (output)
    !       y11,yn1     : 1st prime at boundaries (input)
    !       n           : number of knots (input)

    !      IMPLICIT REAL*8 (a-h,o-z)


    integer sz,n,i1,i2
    real*8 xa(n)
    real*8 ya(sz,itypelow:itypehigh,itypelow:itypehigh)
    real*8 y1a(sz,itypelow:itypehigh,itypelow:itypehigh)
    !**AK**
    !real*8, allocatable, save :: b(:)
    real*8, allocatable :: b(:)

!include 'para_common.f90'

    integer j,n1
    real*8 h,h1,bt,af,y11,yn1
    logical, save :: firsttime = .true.

    !**AK**
    !if (firsttime) then
    !write(0,*) 'allocating b',n
    allocate(b(n))
    !endif

    y1a(1,i1,i2)=one
    b(1)=y11
    n1=n-1
    do j=2,n1
       h1=xa(j)-xa(j-1)
       h=xa(j+1)-xa(j)
       af=h1/(h1+h)
       bt=three*((one-af)*(ya(j,i1,i2)-ya(j-1,i1,i2))/                     &
            h1+af*(ya(j+1,i1,i2)-ya(j,i1,i2))/h)
       y1a(j,i1,i2)=-af/(two+(one-af)*y1a(j-1,i1,i2))
       b(j)=(bt-(one-af)*b(j-1))/(two+(one-af)*y1a(j-1,i1,i2))
    enddo
    y1a(n,i1,i2)=yn1
20  y1a(n1,i1,i2)=y1a(n1,i1,i2)*y1a(n1+1,i1,i2)+b(n1)
    n1=n1-1
    if (n1 > 0) goto 20

    firsttime=.false.

    !**AK**
    deallocate(b)

  end subroutine Spl1b2

  !***********************************************************************
  subroutine Spl2b2_y3d(sz,n,xa,ya,y1a,i1,y12,yn2,eamnbands,iband)

    use typeparam

    use defs
    use para_common

    implicit none

    ! calculate yp(i) (y1a) : 1st prime at knots

    ! from fortran compliment volume 1 / page 57
    ! cubic spline for second boundary condition

    !    xa( ),ya( ) : knots (input)
    !    y1a( )        : 1st prime at knots (output)
    !    y12,yn2        : 2nd prime at boundaries (input)
    !    n        : number of knots (input)

    !      IMPLICIT REAL*8 (a-h,o-z)
    !      IMPLICIT INTEGER(i-n)

    integer sz,n,i1,eamnbands,iband

    real*8 xa(n)
    real*8 ya(sz,itypelow:itypehigh,eamnbands)
    real*8 y1a(sz,itypelow:itypehigh,eamnbands)
    !**AK**
    !real*8, allocatable, save :: b(:)
    real*8, allocatable :: b(:)

!include 'para_common.f90'

    integer j,n1

    real*8 h1,y1,bt,af,y,h,yn2,y12
    logical, save :: firsttime = .true.

    !**AK**
    !if (firsttime) then
    allocate(b(n))
    !endif

    h=xa(2)-xa(1)
    y=ya(2,i1,iband)-ya(1,i1,iband)
    y1a(1,i1,iband)=-half
    b(1)=1.5*y/h-0.25*h*y12
    n1=n-1
    do j=2,n1
       h1=h
       y1=y
       h=xa(j+1)-xa(j)
       y=ya(j+1,i1,iband)-ya(j,i1,iband)
       af=h1/(h1+h)
       bt=three*((one-af)*y1/h1+af*y/h)
       y1a(j,i1,iband)=-af/(two+(one-af)*y1a(j-1,i1,iband))
       b(j)=(bt-(one-af)*b(j-1))/(two+(one-af)*y1a(j-1,i1,iband))
    enddo
    y1a(n,i1,iband)=(three*y/h+h*yn2/two-b(n-1))/(two+y1a(n-1,i1,iband))
20  y1a(n1,i1,iband)=y1a(n1,i1,iband)*y1a(n1+1,i1,iband)+b(n1)
    n1=n1-1
    if (n1 > 0) goto 20

    firsttime=.false.
    !**AK**
    deallocate(b)

  end subroutine Spl2b2_y3d



  !***********************************************************************
  subroutine Spl2b2_y4d(sz,n,xa,ya,y1a,i1,i2,y12,yn2,eamnbands,iband)

    use typeparam

    use defs
    use para_common
    implicit none

    ! calculate yp(i) (y1a) : 1st prime at knots

    ! from fortran compliment volume 1 / page 57
    ! cubic spline for second boundary condition

    !    xa( ),ya( ) : knots (input)
    !    y1a( )        : 1st prime at knots (output)
    !    y12,yn2        : 2nd prime at boundaries (input)
    !    n        : number of knots (input)

    !      IMPLICIT REAL*8 (a-h,o-z)
    !      IMPLICIT INTEGER(i-n)

    integer sz,n,i1,i2,eamnbands,iband

    real*8 xa(n)
    real*8 ya(sz,itypelow:itypehigh,itypelow:itypehigh,eamnbands)
    real*8 y1a(sz,itypelow:itypehigh,itypelow:itypehigh,eamnbands)
    !**AK**
    !real*8, allocatable, save :: b(:)
    real*8, allocatable :: b(:)

!include 'para_common.f90'

    integer j,n1

    real*8 h1,y1,bt,af,y,h,yn2,y12
    logical, save :: firsttime = .true.

    !**AK**
    !if (firsttime) then
    allocate(b(n))
    !endif

    h=xa(2)-xa(1)
    y=ya(2,i1,i2,iband)-ya(1,i1,i2,iband)
    y1a(1,i1,i2,iband)=-half
    b(1)=1.5*y/h-0.25*h*y12
    n1=n-1
    do j=2,n1
       h1=h
       y1=y
       h=xa(j+1)-xa(j)
       y=ya(j+1,i1,i2,iband)-ya(j,i1,i2,iband)
       af=h1/(h1+h)
       bt=three*((one-af)*y1/h1+af*y/h)
       y1a(j,i1,i2,iband)=-af/(two+(one-af)*y1a(j-1,i1,i2,iband))
       b(j)=(bt-(one-af)*b(j-1))/(two+(one-af)*y1a(j-1,i1,i2,iband))
    enddo
    y1a(n,i1,i2,iband)=(three*y/h+h*yn2/two-b(n-1))/(two+y1a(n-1,i1,i2,iband))
20  y1a(n1,i1,i2,iband)=y1a(n1,i1,i2,iband)*y1a(n1+1,i1,i2,iband)+b(n1)
    n1=n1-1
    if (n1 > 0) goto 20

    firsttime=.false.
    !**AK**
    deallocate(b)

  end subroutine Spl2b2_y4d



