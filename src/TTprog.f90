
! Temperature-time programme by Paul Erhart 17.10 2002, parcas V3.70

!************************************************************
! this subroutine sets the mtemp, temp, and trate values
! for the current time step
!************************************************************
subroutine Set_Temp_Control(time_fs,mtemp,temp,temp0,trate, &
     ntimeini,timeini)
  use Temp_Time_Prog
  use PhysConsts
  use defs  
  implicit none
  real*8,intent(in)   :: time_fs
  integer,intent(out) :: mtemp
  real*8,intent(out)  :: temp,temp0,trate,ntimeini,timeini
  integer      :: step
  integer,save :: laststep=0

  ! first figure out which set of parameters should be used
  do step=1,TT_maxnsteps
     if (time_fs.lt.TT_time(step)) exit
  end do
  step=step-1
  TT_step=step
  ! check if the TPP step has changed since last time
  if (step.gt.laststep) then
     ! now set values
     mtemp = TT_mtemp(step)
     temp  = TT_temp(step)
     trate = TT_trate(step)
     ! for mode 6 and 9 the time before quenching has to
     ! be set to zero explicitly
     if (mtemp.eq.6 .or. mtemp.eq.9) then
        ntimeini=0
        timeini=0.0
        temp0=TT_temp(step-1)
     end if
     ! print out info
     write (6,1001) time_fs,mtemp,temp*invkBeV,temp0*invkBeV,trate*invkBeV
     write (7,1001) time_fs,mtemp,temp*invkBeV,temp0*invkBeV,trate*invkBeV
     laststep=step
  end if
1001 format ('ttp',f11.2,2x,i1,2(2x,f7.1),2x,f7.3)
end subroutine Set_Temp_Control


!************************************************************
! this subroutine checks the internal consistency of the
! temperature-time program, prints a summary, and finally
! converts the temperatures and temperature rates from
! Kelvin to eV.
!************************************************************
subroutine Init_Temp_Time_Prog(tscaleth)
  use Temp_Time_Prog
  use PhysConsts
  use my_mpi
  use defs  
  implicit none
  real*8,intent(in) :: tscaleth
  integer :: i
  real*8  :: maxtemp

  ! header for summary
  write (6,*) ; write (6,1011) ; write (6,1012)
  write (7,*) ; write (7,1011) ; write (7,1012)

  do i=1,TT_maxnsteps
     ! ** consistency checks
     ! check times
     if (i.eq.1) then
        if (TT_time(i).ne.0.0) then
           print *,TT_Time(i)
           call my_mpi_abort('TPP-ERROR: initial time for first step should be 0.0 always', int(0))
        end if
     else
        if (TT_time(i).ge.1.0D30) cycle
        if (TT_time(i).le.TT_time(i-1) .or. TT_time(i).le.0.0) then
           print *,TT_Time(i)
           call my_mpi_abort('TPP-ERROR: invalid value(s) for TTtime', int(0))
        end if
     end if
     ! check mode
     select case (TT_mtemp(i))
     case (0,1,2,3,5,6,7,9)
        ! OK
     case default
        print *,TT_mtemp(i)
        call my_mpi_abort('TTP-ERROR: invalid value for TTmt', int(TT_mtemp(i)))
     end select
     ! no ramping mode initially
     if (i.eq.1 .and. (TT_mtemp(i).eq.6 .or. TT_mtemp(i).eq.9)) then
        print *,TT_mtemp(i)
        call my_mpi_abort('TTP-ERROR: no ramping mode in first step', int(TT_mtemp(i)))
     end if
     ! check for impossible temperatures
     if (TT_temp(i).lt.0 .or. TT_temp(i).gt.200000.0) then
        print *,TT_temp(i)
        call my_mpi_abort('TTP-ERROR: invalid value for TTtem', int(TT_temp(i)))
     end if
     if (TT_mtemp(i).eq.6 .or. TT_mtemp(i).eq.9) then
        ! check wether the ramping rate is non-zero for ramping modes
        if (TT_trate(i).eq.0.0) then
           print *,TT_mtemp(i),TT_trate(i)
           call my_mpi_abort('TPP-ERROR: ramping rate has to be non-zero for ramping modes', int(0))
        end if
        ! print warning if time between succesive TPP steps
        ! is too short too reach target temperature
        if (i.lt.TT_maxnsteps) then
           maxtemp=TT_temp(i-1)+TT_trate(i)*(TT_time(i+1)-TT_time(i))
           if (maxtemp.lt.TT_temp(i)) &
                write (6,901) i ; write (7,901) i
901        format ('WARNING: target temperature cannot be reached in step ',i1)
        end if
     end if
     ! check wether a border is defined if a border temp control is chosen
     if ((TT_mtemp(i).eq.5 .or. TT_mtemp(i).eq.7) &
          .and. tscaleth.le.0.0d0) then
        print *,TT_mtemp(i),tscaleth
        call my_mpi_abort('TPP-ERROR: border temperature control selected but no border region specified', int(0))
     end if
     ! ** print summary
     write (6,1013) TT_time(i),TT_mtemp(i),TT_temp(i),TT_trate(i)
     write (7,1013) TT_time(i),TT_mtemp(i),TT_temp(i),TT_trate(i)
  end do
  ! ** unit conversion
  do i=1,TT_maxnsteps
     TT_temp(i)=kBeV*TT_temp(i)
     TT_trate(i)=kBeV*TT_trate(i)
  end do
  write (6,*) ; write (7,*)
1011 format ('temperature-time program:')
1012 format ('time (fs), mtemp, temp (K), trate (K/fs)')
1013 format (f11.2,3x,i1,3x,f7.1,3x,f7.3)
end subroutine Init_Temp_Time_Prog


