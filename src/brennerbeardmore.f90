!#include "defs.h"

!
! Parcas version of Keith Beardmore's Brenner potential implementation
! 
! Changes for parcas:
!
! Variables not used anywhere in force calc. removed:
! LATYP,
! IMVA,IMKA,IMPA,IMTA
! KE,TE
!
! Neighbour list:
!
! NEBPNT(LTMX),NEBOUR(NLSTMX),POINTR(NLSTMX) 
! 
! Removed common /NEBS/
!
!
! Replaced or removed params:
!
! RX() -> x0()
! PBCIJ, PBCTYP()   -> pbc(3)
! XOFSET,YOFSET,ZOFSET removed
! LX,LY,LZ -> box(3)
! 
! LTMX -> NPMAX
! NEBMAX -> NNMAX
! NEBAVE -> NNMAX
!
! Forces: calculating fx,fy,fz, setting into xnp() at the end
! Removed FX,FY,FZ common
!
! PE() -> Epair(), removed COMMON
!
! Atom types: external: atype 1 C 2 H 3 D 4 T 5 Si 6 Ar 7-  Any
!             internal: KTYP  1 C 2 Si 3 H/D/T 4 Ar 5- Any
! Removed atom type COMMON
!
! Cutoff extension: CONRH2(11) holds cutoffs for any interaction
! where any atom is >4.
!
! Made all subroutines IMPLICIT NONE !
!
!  POSSIBLE PROBLEM: parameters are given as real*4 (e.g. "0.0" "6.0").
!  But the actual variables are real*8. So after the 7th digit
!  the parameters may become garbage. E.g. 4.1305 will be instead
!  4.13049983978271.
! 
! ----------------------------------------------------------------------
! Code first makes some tables
! BNDLEN(NNEBMX),CUTFCN(NNEBMX),CUTDRV(NNEBMX)
! BNDXNM(NNEBMX),BNDYNM(NNEBMX),BNDZNM(NNEBMX)
! BNDTYP(NNEBMX),NEB(NNEBMX)
!
!  in 1st loop over atoms, then does potential calc.
!
! Important note for pressure calc. : DKXC is really equivalent
! with normalized r_jk i.e. RXNJK (compare with RXNIK and RXNIJ). 
!
!  Beardmore internal atom types :
!  KTYP=1    C 
!  KTYP=2    Si
!  KTYP=3    H
!  KTYP=4    INERT, e.g. Ar.
!  KTYP=5-  Any atom interacting with pair potential
!
!
!
!  Added covalent radii for CC, CH and HH for bond order analysis.
!  Using Rasmol distances (see abstree.h):
!  C value: 0.72 Å  H value: 0.32 Å Si 1.2 Å  tolerance: 0.56 Å
!  Ergo: CC 2.0 CH 1.6 HH 1.2 Å
!        CSi 2.48 HSi 2.08 SiSi 2.96

!#######################################################################
!      SUBROUTINE CSIHPT
!#######################################################################
!
!   TERSOFF TYPE C/Si/H POTENTIAL.
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!   A COMBINATION OF:
!   DONALD. BRENNERS'S HYDROCARBON POTENTIAL (1st PARAMETERISATION).
!   PARAMETERS FROM : PHYS. REV. B 42, 9458-9471(1990).
!   PLUS CORRECTIONS : PHYS. REV. B 46, 1948(1990).
!   J. TERSOFF'S MULTICOMPONENT POTENTIAL FOR SI - C SYSTEMS.
!   PARAMETERS FROM : PHYS. REV. B 39, 5566-5568(1989).
!   R. MURTY & H. ATWATER'S Si-H POTENTIAL.
!   PARAMETERS FROM : PAPER PRESENTED AT COSIRES '94.
!
!   USES LINKED LIST AND POINTERS TO REDUCE SIZE OF NEB-LIST.
!   PRE-CALCULATES PAIRWISE TERMS.
!   APPLIES PERIODIC BOUNDARY CONDITIONS IN X, Z & Y WHEN NECESSARY.
!
!   ZEIGLER, BIERSACK & LITMARK'S UNIVERSAL POTENTIAL TO
!   DESCRIBE INTERACTION WITH INERT ELEMENT (e.g. Ar).
!   PARAMETERS FROM : COMPUTER SIMULATION OF ION-SOLID INTERACTIONS,
!   PP 40-42, W. ECKSTEIN, SPRINGER-VERLAG(BERLIN HEIDELBERG), (1991).
!
!   ALGORITHM ASSUMES ATOMS ARE:
!   KTYP=1 (C),KTYP=2 (Si), KTYP=3 (H) OR KTYP=4 (INERT, e.g. Ar).
!
!   FOR C/Si/H POTENTIAL :
!
!       M M M   N N N             The energy of bond I-J is 
!        \|/     \|/           dependent on all atoms that are 
!     M   K       L   N        first or second neighbours of 
!      \   \     /   /         I and J. The resulting forces   
!   M---K---I===J---L---N      act upon all these atoms.
!      /   /     \   \          
!     M   K       L   N           In the code, the atoms and
!        /|\     /|\           bonds are identified as shown
!       M M M   N N N          on the left.
!
!   ZBL UNIVERSAL IS A PAIR POTENTIAL.
!

module Brenner_vars


      integer, parameter :: BCOMBS = 4
      integer, parameter :: BTYPES = 2

      logical :: firsttime=.true.
 
      !   COMMON VARIABLES.
      !REAL*8, allocatable :: FX(:),FY(:),FZ(:)
      INTEGER, allocatable :: KTYP(:)
      INTEGER, allocatable :: iactmode(:) !some old shit, should be fixed
      !     variables for Brenner C-type atoms
!     Andrea
      INTEGER, allocatable :: BNDTYPBRE(:)
      INTEGER, allocatable :: BRETYP(:)
      INTEGER BRETYPI,BRETYPJ,BRETYPK,BRETYPL,IJBPOT,IKBPOT,JLBPOT
!
      INTEGER, allocatable :: DATUM(:)
      INTEGER, allocatable :: NEB(:)
      INTEGER, allocatable :: BNDTYP(:)

      REAL*8, allocatable :: BNDLEN(:),CUTFCN(:),CUTDRV(:)
      REAL*8, allocatable :: fermifcn(:),fermidrv(:)
      REAL*8, allocatable :: BNDXNM(:),BNDYNM(:),BNDZNM(:)
      REAL*8, allocatable :: NTSI(:)


      ! CONRH2 is of length 11 to hold cutoff for read-in pair potentials
      REAL*8 CONRH2(BCOMBS,11),CONRL(BCOMBS,10),CONFCA(BCOMBS,10),CONFC(BCOMBS,10),fermib(BCOMBS,10),fermir(BCOMBS,10)
      REAL*8 CONFA(BCOMBS,6),CONEA(BCOMBS,6),CONFR(BCOMBS,6),CONER(BCOMBS,6),CONRE(BCOMBS,6)
      REAL*8 CONCD(BCOMBS,6),CONC2(BCOMBS,6),COND2(BCOMBS,6),CONH(BCOMBS,6),CONALP(6),CONBET(6)
      REAL*8 CONN(6),CONPE(6),CONAN(6),CONNM(6),CONPF(6),CONCSI
      REAL*8 CONC(4,4),COND(4,4)


      real*8 COVALRAD(4,4)
      REAL*8 :: HCCCF(16,4,4)
      REAL*8 :: HCHCF(16,4,4)

      REAL*8 :: FCCCF(16,4,4,4)

      REAL*8 :: AH(3,4)
      REAL*8 :: AF1(3,4),AF2(3,4)

end module Brenner_vars

      SUBROUTINE Beardmore_Force(x0,atype,xnp,atomindex,natoms,box,pbc, &
          nborlist,nabors,npairs,Epair,Vpair,Ethree,Vmany,           &
          wxx,wyy,wzz,wxxi,wyyi,wzzi, wxxsh,wyysh,wzzsh, atpassflag, nngbrproc,           &
          bondstat,bondstat2,bondstat3,reppotspl,potmode)

    use typeparam
    use datatypes
    use my_mpi
   

    use defs
    use para_common

   use Brenner_vars
      IMPLICIT NONE

      ! Parcas calling parameters
      real*8 x0(*),xnp(*),box(3),pbc(3)
      integer atype(*),natoms,potmode, atomindex(*)

      REAL*8 Epair(*), Vpair, Ethree(*), Vmany

      ! Pressures not yet implemented !
      REAL*8 wxx,wyy,wzz, wxxi(*),wyyi(*),wzzi(*)

      real*8 wxxsh(*),wyysh(*),wzzsh(*)

      ! nborlist() holds the neighbor lists.
      ! Special Brenner version: NABORS and inverse list added
      integer*4 nabors(*)
      INTEGER*4 nborlist(*) !,invnborlist(*)

      integer npairs,bondstat(4,0:NNMAX),bondstat2(4,0:NNMAX)
      integer bondstat3(4,0:NNMAX)
      real*8 reppotspl

      ! parcas version help params
      integer i3,j3,jngbr,jbegin,jend
      real*8 dummy1,dummy2,pbcbox(3)!zero,half(3)


      integer typetranslate(0:6),typetranslatebs(0:6)
      ! atype translation:
      ! parcas number     0  1  2  3  4  5  6  
      ! parcas type:      H  C  H  D  T  Si Ar 
      data typetranslate /3, 1, 3, 3, 3, 2, 4/

      ! atype translation for bondstat:
      ! parcas number       0  1  2  3  4  5  6  
      ! parcas type:        H  C  H  D  T  Si Ar 
      data typetranslatebs /2, 1, 2, 2, 2, 3, 4/

      real*8  r,dx,dy,dz
      integer nbondsi

      !common /covalradcommon/ COVALRAD 



! Atom types: external: atype 1 C 2 H 3 D 4 T 5 Si 6 Ar 7- Any
!             internal: KTYP  1 C 2 Si 3 H/D/T 4 Ar 5- Any

!     PARAMETERS FOR SIZES OF ARRAYS;
      INTEGER NLSTMX,NNEBMX

      PARAMETER( NLSTMX=NPMAX * NNMAX )
      PARAMETER( NNEBMX=NPMAX * NNMAX )

!
      REAL*8 RXI,RYI,RZI,RXIJ,RYIJ,RZIJ
      REAL*8 RLIJ,RLIJR,RLIK,RLJL
      REAL*8 RXNIJ,RYNIJ,RZNIJ,RXNIK,RYNIK,RZNIK,RXNJL,RYNJL,RZNJL
      REAL*8 ARG
      REAL*8 FCIJ,DFCIJR,FCIK,DFCIKR,FCJL,DFCJLR
      REAL*8 FCKM,DFCKMR,FCLN,DFCLNR
      REAL*8 FAIJ,DFAIJR,FRIJ,DFRIJR
      REAL*8 ZIJ,ZJI
      REAL*8 DBIDXI,DBIDYI,DBIDZI,DBIDXJ,DBIDYJ,DBIDZJ
      REAL*8 DBIDXK(NNMAX),DBIDYK(NNMAX),DBIDZK(NNMAX)
      REAL*8 DBJDXI,DBJDYI,DBJDZI,DBJDXJ,DBJDYJ,DBJDZJ
      REAL*8 DBJDXL(NNMAX),DBJDYL(NNMAX),DBJDZL(NNMAX)
      REAL*8 DISJK,DISIL,DKXC,DKYC,DKZC,DLXC,DLYC,DLZC
      !REAL*8 DKXCrvecJ,DKYCrvecJ,DKZCrvecJ,DLXCrvecI,DLYCrvecI,DLZCrvecI
      !REAL*8 DKXCrvecK,DKYCrvecK,DKZCrvecK,DLXCrvecL,DLYCrvecL,DLZCrvecL
      REAL*8 COSTH,DIFLEN
      REAL*8 GFACAN,CARG,GDDAN,QFACAN,QFADAN
      REAL*8 DZFAC,DZDRIJ,DZDRJI,DZDRIK,DZDRJL
      REAL*8 DGDXI,DGDYI,DGDZI,DGDXJ,DGDYJ,DGDZJ
      REAL*8 DGDXK,DGDYK,DGDZK,DGDXL,DGDYL,DGDZL
      REAL*8 DCSDIJ,DCSDJI,DCSDIK,DCSDJK,DCSDIL,DCSDJL
      REAL*8 DCSDXI,DCSDYI,DCSDZI
      REAL*8 DCSDXJ,DCSDYJ,DCSDZJ
      REAL*8 DCSDXK,DCSDYK,DCSDZK
      REAL*8 DCSDXL,DCSDYL,DCSDZL
      REAL*8 BIJ,BJI,BAVEIJ,DFBIJ,DFBJI
      REAL*8 VFAC,HLFVIJ,DFFAC,VFACDN
      real*8 fermihelp,fermihelp2,fermiVr,fermidVr
      REAL*8 NHI,NCI,NHJ,NCJ
      REAL*8 NHIDXI,NHIDYI,NHIDZI
      REAL*8 NHJDXJ,NHJDYJ,NHJDZJ
      REAL*8 NCIDXI,NCIDYI,NCIDZI
      REAL*8 NCJDXJ,NCJDYJ,NCJDZJ
      REAL*8 NTIDXI,NTIDYI,NTIDZI,NTIDXJ,NTIDYJ,NTIDZJ
      REAL*8 NTJDXJ,NTJDYJ,NTJDZJ,NTJDXI,NTJDYI,NTJDZI
      REAL*8 NHIDXK(NNMAX),NHIDYK(NNMAX),NHIDZK(NNMAX)
      REAL*8 NHJDXL(NNMAX),NHJDYL(NNMAX),NHJDZL(NNMAX)
      REAL*8 NCIDXK(NNMAX),NCIDYK(NNMAX),NCIDZK(NNMAX)
      REAL*8 NCJDXL(NNMAX),NCJDYL(NNMAX),NCJDZL(NNMAX)

      REAL*8 NTIDXK(NNMAX),NTIDYK(NNMAX),NTIDZK(NNMAX)
      REAL*8 NTJDXL(NNMAX),NTJDYL(NNMAX),NTJDZL(NNMAX)
      REAL*8 HIJ,HJI,DHDNHI,DHDNHJ,DHDNCI,DHDNCJ
      REAL*8 NTI,NTJ,NCONJ,NCNJDX,NCNJDR
      REAL*8 NCNDXI,NCNDYI,NCNDZI,NCNDXJ,NCNDYJ,NCNDZJ
      REAL*8 NCNDXK(NNMAX),NCNDYK(NNMAX),NCNDZK(NNMAX)
      REAL*8 NCNDXL(NNMAX),NCNDYL(NNMAX),NCNDZL(NNMAX)
      REAL*8 NCNDXM(NNMAX,NNMAX),NCNDYM(NNMAX,NNMAX),NCNDZM(NNMAX,NNMAX)
      REAL*8 NCNDXN(NNMAX,NNMAX),NCNDYN(NNMAX,NNMAX),NCNDZN(NNMAX,NNMAX)
      REAL*8 XIK,XIKDKX,XIKDKY,XIKDKZ,XJL,XJLDLX,XJLDLY,XJLDLZ
      REAL*8 XIKDMX(NNMAX),XIKDMY(NNMAX),XIKDMZ(NNMAX)
      REAL*8 XJLDNX(NNMAX),XJLDNY(NNMAX),XJLDNZ(NNMAX)
      REAL*8 FXIK,DFXIKX,FXJL,DFXJLX
      REAL*8 FIJ,DFDNTI,DFDNTJ,DFDCNJ
      REAL*8 F1,F2,DF1DNT,DF2DNT,FASIH,FRSIH
      REAL*8 DHDNTI,DHDNTJ
      REAL*8 DBIDH,DBJDH,DBIDNI,DBJDNJ
      REAL*8 CUTFNA,CUTDVA,TOTPOT,TOTFCE,POT
!
      INTEGER I,J,K,L,M,N,IJ,IK,JL,KM,LN
      INTEGER K3,L3,M3,N3, itype, jtype, in, in0, np0, nnbors
      integer jj,i2,j2,ii,ii3,myi,dfrom,ifrom,d
      real*8 t1
      INTEGER IKC,JLC,KMC,LNC
      INTEGER KTYPI,KTYPJ,KTYPK,KTYPL
      INTEGER IJPOT,IKPOT,JLPOT
      INTEGER MBTIJ,MBTJI,MBTIJK,MBTJIL
      INTEGER IJPNT,NEBTOT,ISTART,IFINSH
      INTEGER NEBOFI(NNMAX),NEBOFJ(NNMAX)
      INTEGER NEBOFK(NNMAX,NNMAX),NEBOFL(NNMAX,NNMAX)
      INTEGER NUMNBI,NUMNBJ,NUMNBK(NNMAX),NUMNBL(NNMAX)

      integer atpassflag(*),nngbrproc(*)
      integer(kind=mpi_parameters) :: ierror  ! MPI error code
      integer brenner_head, brenner_head_recv, nborlist_end

      !COMMON/TBC1/ CONRH2,CONRL,CONFCA,CONFC,fermib,fermir
      !COMMON/CSH2/ CONFA,CONEA,CONFR,CONER,CONRE
      !COMMON/CSH3/ CONCD,CONC2,COND2,CONH,CONALP,CONBET
      !COMMON/CSH4/ CONN,CONPE,CONAN,CONNM,CONPF,CONCSI
      !COMMON/ZBLU/ CONC,COND

      ! Parcas interface setup stuff
      if (firsttime) then
         allocate(NTSI(NPMAX))
         allocate(DATUM(NPMAX+1))
         allocate(BRETYP(NPMAX))
         allocate(iactmode(NPMAX)) !some old shit, should be fixed
         allocate(KTYP(NPMAX))
         allocate(BNDLEN(NNEBMX))
         allocate(CUTFCN(NNEBMX))
         allocate(CUTDRV(NNEBMX))
         allocate(fermifcn(NNEBMX))
         allocate(fermidrv(NNEBMX))
         allocate(BNDXNM(NNEBMX))
         allocate(BNDYNM(NNEBMX))
         allocate(BNDZNM(NNEBMX)) 
         allocate(BNDTYP(NNEBMX))
         allocate(NEB(NNEBMX))
         allocate(BNDTYPBRE(NNEBMX))

         firsttime=.false.
         print *,'First time in BrennerBeardmore potential'
         print *,'PRESSURELESS VERSION !!!'
      endif

    iactmode=1
    ! Gather the coordinates of all local atoms
    do i = 1,3*myatoms
       buf(i) = x0(i)
    enddo

    do i=1,myatoms
       dirbuf(i)=0
       !i3=i*3-3
       ibuf(i*2-1)=i
       ibuf(i*2)=atype(i)
       !ibuf(i3+3)=atomindex(i)   !! also need to be changed
    enddo
    np0=myatoms

    ! Handle communication: get x info into buf()
    ! For more notes, see stilweb.f90 or EAMforces.f90

    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       ! Loop over directions
        do d=1,8
          j=0; jj=0; 
          do i=1,myatoms
             if (IAND(atpassflag(i),passbit(d)) .ne. 0) then
                j=j+1
                if (j .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow1', int(myproc))
                i3=i*3-3
                j3=j*3-3
                j2=j*2-2
                xsendbuf(j3+1)=x0(i3+1)
                xsendbuf(j3+2)=x0(i3+2)
                xsendbuf(j3+3)=x0(i3+3)
                isendbuf(j2+1)=i
                isendbuf(j2+2)=atype(i)
                !isendbuf(j3+3)=atomindex(i)   !!! for debug, no use anymore.
             endif
          enddo
          !print *,'send',myproc,d,j,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(np0*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(np0*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
          endif
          j=jj
          do i=1,j
             dirbuf(np0+i)=d
          enddo
          np0=np0+j;
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif
    if (np0 .ne. np0pairtable) then
       write (6,*) 'BrennerBeardmore np0 problem ',myproc,np0,np0pairtable
    endif
    !print *,'Node received totally ',myproc,np0-myatoms

    ! Communication done
	!do i=1,3*np0
    !   pbuf(i)=zero
    !enddo
    !do i=1,np0
    !   ebuf(i)=zero     
    !enddo
    pbuf(1:3*np0)=zero
	  ebuf(1:np0)=zero


    do i=1,np0!myatoms   ! <-------------------- Loop over atoms i
		i3=i*3-3
        itype=abs(ibuf(i*2))
        
        if (itype > 6) then
			! Adding Brenner C-type atoms (iactmode > 1)     Andrea
            if (iactmode(i) == 1) then    
               KTYP(i) = itype-2
               BRETYP(i)=1  
            else
               KTYP(i)=1  
               BRETYP(i) = itype-5
            endif
        else
            KTYP(i)=typetranslate(itype)
            BRETYP(i)=1
        endif
    enddo
      
    Vpair = zero
    Vmany = zero
    !do i = 1,myatoms
    !    Epair(i) = zero
    !    Ethree(i) = zero
    !enddo
    do j=0,NNMAX
       do i=1,4
          bondstat(i,j)=0
          bondstat2(i,j)=0
          bondstat3(i,j)=0
        enddo
    enddo

!   CALCULATE THE MAIN PAIRWISE TERMS AND STORE THEM.
!   CALCULATE ZBL PAIR POTENTIALS HERE.

      NEBTOT = 1
      in = 0
      DO I = 1, np0 !myatoms

         i3=I*3-3
         itype=abs(ibuf(I*2))


		 in=in+1; in0=in+1
         nnbors = nborlist(in)
         in=in+nnbors
         
         RXI   = buf(i3+1)!*box(1)
         RYI   = buf(i3+2)!*box(2)
         RZI   = buf(i3+3)!*box(3)
         KTYPI = KTYP(I)
         BRETYPI = BRETYP(I)   ! Andrea

         nbondsi=0

         NTSI(I) = 0.0
         DATUM(I) = NEBTOT
!print *, i, 'jbegin:', nborlist(in0), 'jend:', nborlist(in)
!print *, 'x0(i3+123):', RXI, RYI, RZI
         do jngbr=in0,in
             J     = nborlist(jngbr)
             j3=j*3-3
             
             jtype=abs(ibuf(J*2))
             KTYPJ = KTYP(J)
             BRETYPJ = BRETYP(J)    ! Andrea
 

             ! Take care on non-interacting hydrogens   ! correcting apparent bug here (second KTYPI==3 -> KTYPJ==3)...  Andrea
             if (KTYPI==3 .and. KTYPJ==3 .and. (iactmode(I)==2 .or. iactmode(J)==2)) cycle
     		
         	 !   TAKE CARE OF PBCs HERE.
        	 !   REST OF POTENTIAL USES INTERNUCLEAR VECTORS, NOT POSITIONS.

             RXIJ  = buf(j3+1) - RXI
             if (RXIJ >= half) RXIJ=RXIJ-pbc(1)
             if (RXIJ < -half) RXIJ=RXIJ+pbc(1)
             RXIJ=RXIJ*box(1)

             RYIJ  = buf(j3+2) - RYI 
             if (RYIJ >= half) RYIJ=RYIJ-pbc(2)
             if (RYIJ < -half) RYIJ=RYIJ+pbc(2)
             RYIJ=RYIJ*box(2)

             RZIJ  = buf(j3+3) - RZI 
             if (RZIJ >= half) RZIJ=RZIJ-pbc(3)
             if (RZIJ < -half) RZIJ=RZIJ+pbc(3)
             RZIJ=RZIJ*box(3)

             RLIJ  = RXIJ*RXIJ + RYIJ*RYIJ + RZIJ*RZIJ

             if (itype>6 .or. jtype>6) then
               ! Do nothing, pair potentials not counted
             else
               if (RLIJ <= COVALRAD(typetranslatebs(itype),typetranslatebs(jtype))) then
                  nbondsi=nbondsi+1 
               endif
             endif

            IF( KTYPI  >=  4 .OR. KTYPJ  >=  4 ) THEN
         		!    CALCULATE pair potential and Ar INTERACTIONS HERE.
         		! "SOME SHIT IS HERE, CHECK FOR PARALLEL"
                IF ( I  >  J ) THEN
				!IF ( ibuf(I3+3)  > ibuf(J3+3)   ) THEN
                
                  IJPOT = KTYPI + KTYPJ + 2

                  if (KTYPI > 4 .or. KTYPJ>4) IJPOT=11
                  if (IJPOT>11) IJPOT=11

                  IF( CONRH2(1,IJPOT)  >  RLIJ ) THEN


      		
      				!     WITHIN CUTOFF, SO CALCULATE INTERACTION.
                     RLIJ = SQRT( RLIJ )

                    if ( KTYPI  < 5 .and. KTYPJ  <  5) then

                        ! Ar ZBL interactions
						!     CUTOFF FUNCTION AND DERIVATIVE.
                        IF( RLIJ  <  CONRL(1,IJPOT) ) THEN
                           CUTFNA = 1.0
                           CUTDVA = 0.0
                        ELSE
                           ARG = CONFCA(1,IJPOT)*( RLIJ-CONRL(1,IJPOT) )
                           CUTFNA = 0.5 *  ( 1.0 + COS( ARG ) )
                           CUTDVA = CONFC(1,IJPOT) * SIN( ARG )
                        ENDIF

						!     ZBL UNIVERSAL POTENTIAL.
                        IJPOT = IJPOT - 6
                        TOTPOT = 0.0
                        TOTFCE = 0.0
                        DO N =1,4
                           POT = CONC(IJPOT,N) * EXP ( COND(IJPOT,N) * RLIJ )
                           TOTPOT = TOTPOT + POT
                           TOTFCE = TOTFCE + COND(IJPOT,N) * POT
                        ENDDO
                        TOTPOT = TOTPOT / RLIJ
                        TOTFCE = -CUTFNA * ( TOTFCE / RLIJ - TOTPOT / RLIJ ) -CUTDVA * TOTPOT
     
                        TOTPOT = CUTFNA * TOTPOT / 2.0
                        ! print *,'pot',RLIJ,TOTPOT,TOTFCE

                    else
                        
                        ! Spline pair potential
                        ! Note that there is a different sign convention
                        ! for the forces !

                         call splinereppot(RLIJ,TOTPOT,TOTFCE,1d20,1d20,itype,jtype,dummy1,dummy2)

                         TOTPOT=TOTPOT/2.0d0
                         TOTFCE= -TOTFCE


                         ! print *,'pot2',RLIJ,TOTPOT,TOTFCE

                    endif
                   
                     !if ( dirbuf(j) < 5 ) then 	
                     
                     ebuf(i)  = ebuf(i) + TOTPOT
	                 ebuf(j)  = ebuf(j) + TOTPOT
	                 !endif
	     
	                 TOTFCE = TOTFCE / RLIJ
	                 pbuf(i3+1)=pbuf(i3+1) - TOTFCE*RXIJ
	                 pbuf(i3+2)=pbuf(i3+2) - TOTFCE*RYIJ
	                 pbuf(i3+3)=pbuf(i3+3) - TOTFCE*RZIJ

	                 pbuf(j3+1)=pbuf(j3+1) + TOTFCE*RXIJ
	                 pbuf(j3+2)=pbuf(j3+2) + TOTFCE*RYIJ
	                 pbuf(j3+3)=pbuf(j3+3) + TOTFCE*RZIJ
	                 
                     
                  ENDIF
               ENDIF
            ELSE

				!     STORE PAIR TERMS HERE ( ONLY STORE C, Si & H ATOMS ).
		
               
               IF( KTYPI  ==  1 )THEN
                  IJPOT = KTYPJ
               ELSE IF( KTYPJ  ==  1 )THEN
                  IJPOT = KTYPI
               ELSE
                  IJPOT = KTYPI + KTYPJ
               ENDIF

				! IJBPOT keeps track of parameters for additional "brennertype" atoms /= C
               IF ( BRETYPI == 1 .AND. BRETYPJ == 1 ) THEN
                  IJBPOT = 1
	       		! C-H
               ELSE IF (KTYPI == 3) THEN
                  IJBPOT =  BRETYPJ
               ELSE IF (KTYPJ == 3) THEN
                  IJBPOT = BRETYPI
               ELSE
                 ! C-C  **CHECK THIS** Andrea
                  IF (BRETYPI > BRETYPJ) THEN
                     IJBPOT = (BRETYPI)*(BRETYPI + 1)*0.5 - BRETYPJ + BTYPES  
                  ELSE
                     IJBPOT = (BRETYPJ)*(BRETYPJ + 1)*0.5 - BRETYPI + BTYPES
                  ENDIF
               ENDIF
              
               !if (i == 1 .and. NEBTOT == 1) then
				!print *,'i = ',I,'j = ',J
				!print *,'ijpot = ',IJPOT,'ijbpot = ',IJBPOT
				!print *,'KTYPI = ',KTYPI,'KTYPJ = ',KTYPJ
				!print *,'BRETYPI = ',BRETYPI,'BRETYPJ = ',BRETYPJ
                !endif


               IF( CONRH2(IJBPOT,IJPOT)  >  RLIJ ) THEN

				  !     WITHIN CUTOFF, SO ADD J TO NEB-LIST.
                  NEB(NEBTOT) = J

				  !     BOND-LENGTH AND DIRECTION COSINES.
                  RLIJ           = SQRT( RLIJ )
                  BNDLEN(NEBTOT) = RLIJ
                  BNDXNM(NEBTOT) = RXIJ / RLIJ
                  BNDYNM(NEBTOT) = RYIJ / RLIJ
                  BNDZNM(NEBTOT) = RZIJ / RLIJ
                  BNDTYP(NEBTOT) = IJPOT
                  BNDTYPBRE(NEBTOT) = IJBPOT

				  !     CUTOFF FUNCTION AND DERIVATIVE.
                  IF( RLIJ  <  CONRL(IJBPOT,IJPOT) ) THEN
                     CUTFCN(NEBTOT) = 1.0
                     CUTDRV(NEBTOT) = 0.0
                  ELSE
                     ARG = CONFCA(IJBPOT,IJPOT)*( RLIJ-CONRL(IJBPOT,IJPOT) )
                     CUTFCN(NEBTOT) = 0.5 *  ( 1.0 + COS( ARG ) )
                     CUTDRV(NEBTOT) = CONFC(IJBPOT,IJPOT) * SIN( ARG )
                  ENDIF
				  !print*,'i,j,cutfcn,arg',I,J,CUTFCN(NEBTOT),ARG
		  		  ! Fermi function if needed
                  if (reppotspl > RLIJ) then
                     fermihelp=exp(-fermib(IJBPOT,IJPOT)*(RLIJ-fermir(IJBPOT,IJPOT)))
                     fermifcn(NEBTOT)=1.0d0/(1.0d0+fermihelp)
                     fermidrv(NEBTOT)=fermifcn(NEBTOT)*fermifcn(NEBTOT)*fermihelp*fermib(IJBPOT,IJPOT)
		     		 !print *,'Fermi 0 fermi',IJPOT,RLIJ,fermifcn(NEBTOT),fermidrv(NEBTOT)
		     
                  endif

     
				  !     SUM THE NUMBER OF NEIGHBOURS OF SI ATOMS.
                  IF( KTYPI  ==  2 ) NTSI(I) = NTSI(I) + CUTFCN(NEBTOT)
     
                  NEBTOT = NEBTOT + 1
               ENDIF
            ENDIF

         enddo  ! End of loop over neighbours j
         !if (I==10) WRITE(*,*)'ATOM ',I,' HAS ',NEBTOT-DATUM(I),' NEIGHBOURS.'
         !if (I==10) print *,'Si has neighbours',NTSI(I)

         if (itype <=6) then
               ! Do bond statistics
            bondstat(typetranslatebs(itype),nbondsi)=    &
                bondstat(typetranslatebs(itype),nbondsi)+1
            
            bondstat2(typetranslatebs(itype),NEBTOT-DATUM(I))=    &
                bondstat2(typetranslatebs(itype),NEBTOT-DATUM(I))+1
            
            if (itype >= 0) then
               ! bondstat3: disregard fixed atom bonds
               bondstat3(typetranslatebs(itype),NEBTOT-DATUM(I))=    &
                   bondstat3(typetranslatebs(itype),NEBTOT-DATUM(I))+1
            endif
            !if (nbondsi==0 .or. NEBTOT-DATUM(I)==0) then
            !   print *,'ZERO BONDS:',I,RXI,RYI,RZI,KTYPI
            !endif
         endif

      ENDDO ! End of first loop over atoms i
      DATUM(np0+1) = NEBTOT !DATUM(myatoms+1) = NEBTOT

      !   BEGIN POTENTIAL CALCULATION.
      DO I = 1,myatoms !-1

      	 i3=i*3-3
      	 itype=ibuf(i*2)
         KTYPI = KTYP(I)
         BRETYPI = BRETYP(I)   ! Andrea
		 !print *,'in pot calc '    !Andrea
		 !   ONLY CONSIDER C, Si AND H INTERACTIONS.

         IF( KTYPI  >  3 ) GOTO 2000

         ! Note: because atom types > 4 (Ar or larger) are not taken
         ! into the neighbout list, they also will not be part of any
         ! calculation in the remainder of this subroutine. KN note 8.10 2001.

 		 !   HAVE A LIST OF ALL NON-NEGLIGIBLE BONDS ON ATOM I.
		 !   I==J LOOP. CONSIDER ALL PAIRS OF ATOMS I < J.
         ISTART = DATUM(I)
         IFINSH = DATUM(I+1)-1

         DO IJ = ISTART,IFINSH
            J = NEB(IJ)
         	j3=j*3-3
            jtype=ibuf(j*2)

            if ( dirbuf(J) > 4 ) cycle

      	 	IF( J  >  I .or. dirbuf(J) /=0 ) THEN
				!IF( dirbuf(J) /=0 ) print *, "WWWWWWWWWWWWWWWoW"
            	!if (ibuf(i3+3)==67 .or. ibuf(j3+3)==67) then
            		!print *, myproc, dirbuf(i), dirbuf(j), ibuf(i3+3), ibuf(j3+3)
            	!endif

	            KTYPJ = KTYP(J)
	            BRETYPJ = BRETYP(J)      ! Andrea
	            IJBPOT = BNDTYPBRE(IJ)  !  Andrea
	            IJPOT = BNDTYP(IJ)
	            RLIJ  = BNDLEN(IJ)
	            RLIJR = 1.0 / RLIJ
	            RXNIJ = BNDXNM(IJ)
	            RYNIJ = BNDYNM(IJ)
	            RZNIJ = BNDZNM(IJ)

				!   CUTOFF FUNCTION AND DERIVATIVE.
	            fermihelp=1.0d0
	            if (reppotspl>RLIJ) then
	               fermihelp=fermifcn(IJ)
	            endif
	            FCIJ   = CUTFCN(IJ)*fermihelp
	            DFCIJR = CUTDRV(IJ)*fermihelp
				!print*,'cutfcn, fermihelp',i,j,CUTFCN(IJ),fermihelp

				!   BEGIN TO SUM THE NUMBER OF ATOMS ON I AND J IF BOND CONTAINS CARBON
				!   (NC IS NO. CARBON + NO. SILICON).
	            IF( IJPOT  ==  1 .AND. IJBPOT == 1 )THEN    ! Andrea
					!   CARBON-CARBON
					!print *, 'CARBON-CARBON'
	               NHI    = 0.0
	               NHJ    = 0.0
	               NHIDXI = 0.0
	               NHIDYI = 0.0
	               NHIDZI = 0.0
	               NHJDXJ = 0.0
	               NHJDYJ = 0.0
	               NHJDZJ = 0.0
	               NCI    = 0.0
	               NCJ    = 0.0
	               NCIDXI = 0.0
	               NCIDYI = 0.0
	               NCIDZI = 0.0
	               NCJDXJ = 0.0
	               NCJDYJ = 0.0
	               NCJDZJ = 0.0
				   !   INITIALISE NijConj TERM AND DERIVATIVES.
	               NCONJ  = 1.0
	               NCNDXI = 0.0
	               NCNDYI = 0.0
	               NCNDZI = 0.0
	               NCNDXJ = 0.0
	               NCNDYJ = 0.0
	               NCNDZJ = 0.0
	             ELSE IF( KTYPI == 1 .AND. BRETYPI == 1 .AND. KTYPJ == 3 )THEN  !Andrea
					!   CARBON-HYDROGEN
					!print *, 'CARBON-HYDROGEN'
	               NHI    = 0.0
	               NHIDXI = 0.0
	               NHIDYI = 0.0
	               NHIDZI = 0.0
	               NCI    = 0.0
	               NCIDXI = 0.0
	               NCIDYI = 0.0
	               NCIDZI = 0.0
	             ELSE IF( KTYPI == 3 .AND. KTYPJ == 1 .AND. BRETYPJ == 1 )THEN  ! Andrea
				   !   HYDROGEN-CARBON
				   !print *, 'HYDROGEN-CARBON'
	               NHJ    = 0.0
	               NHJDXJ = 0.0
	               NHJDYJ = 0.0
	               NHJDZJ = 0.0
	               NCJ    = 0.0
	               NCJDXJ = 0.0
	               NCJDYJ = 0.0
	               NCJDZJ = 0.0
	             ENDIF

				 !   MORSE TERMS AND DERIVATIVES (INCLUDE SPLINES FOR Si-H BONDS).
	             FAIJ   = CONFA(IJBPOT,IJPOT) * EXP( CONEA(IJBPOT,IJPOT) * RLIJ)
	             FRIJ   = CONFR(IJBPOT,IJPOT) * EXP( CONER(IJBPOT,IJPOT) * RLIJ)
				 !print *,'Fa_ij = ',FAIJ,'Fr_ij = ',FRIJ          !Andrea
	             DFAIJR = CONEA(IJBPOT,IJPOT) * FAIJ
	             DFRIJR = CONER(IJBPOT,IJPOT) * FRIJ
	             IF( IJPOT  ==  5 )THEN
	               FASIH = FAIJ
	               FRSIH = FRIJ
	               IF( KTYPI  ==  2 )THEN
	                  CALL FSIITP(NTSI(I),F1,F2,DF1DNT,DF2DNT)
	               ELSE
	                  CALL FSIITP(NTSI(J),F1,F2,DF1DNT,DF2DNT)
	               ENDIF
	               FAIJ   = F2 * FASIH
	               FRIJ   = F1 * FRSIH
	               DFAIJR = F2 * DFAIJR
	               DFRIJR = F1 * DFRIJR
	             ENDIF

			 	 !   DECIDE WHICH MANY-BODY PARAMETERS ARE BEING USED.       
	         	 IF     (KTYPI == 1 .AND. KTYPJ == 2) THEN
	            	MBTIJ = 4
	         	 ELSE IF(KTYPI == 2 .AND. KTYPJ == 3) THEN
	            	MBTIJ = 5
	         	 ELSE
	            	MBTIJ = KTYPI
         	 ENDIF

		 	!print *,'i, j, many body parameter = ',I,J,MBTIJ    !Andrea

		 	!   USE SPLINE TO GET VALUE FOR H FOR ANGLE TERMS IF NEEDED.
	         IF( KTYPI  ==  2 ) THEN
	            CALL HSIITP(NTSI(I),CONH(1,5),DHDNTI)
	            NTIDXI = - RXNIJ * DFCIJR
	            NTIDYI = - RYNIJ * DFCIJR
	            NTIDZI = - RZNIJ * DFCIJR

	            NTIDXJ = - NTIDXI
	            NTIDYJ = - NTIDYI
	            NTIDZJ = - NTIDZI

	            DBIDH  = 0.0
	         ENDIF

			 !   CALCULATE COMPONENTS OF BOND ORDER TERM AND DERIVATIVES
			 !   WITH RESPECT TO BOND I-J FOR ATOM I.
            ZIJ    = 0.0
            DBIDXI = 0.0
            DBIDYI = 0.0
            DBIDZI = 0.0
            DBIDXJ = 0.0
            DBIDYJ = 0.0
            DBIDZJ = 0.0

			 !   I--K LOOP.
            IKC = 0
            DO IK =ISTART,IFINSH
			  !   CONSIDER ALL ATOMS BOUND TO I, EXCEPT ATOM J.
               IF( IK  /=  IJ )THEN
                  IKC   = IKC + 1
                  K     = NEB(IK)
                  NEBOFI(IKC) = K
                  KTYPK = KTYP(K)
                  BRETYPK = BRETYP(K)       !Andrea
                  IKBPOT = BNDTYPBRE(IK) 
                  IKPOT = BNDTYP(IK)
                  RLIK  = BNDLEN(IK)
                  RXNIK = BNDXNM(IK)
                  RYNIK = BNDYNM(IK)
                  RZNIK = BNDZNM(IK)

                  FCIK   = CUTFCN(IK)
                  DFCIKR = CUTDRV(IK)

				  !   SUM THE NUMBER OF ATOMS ON I IF CARBON OR SILICON.
                  IF( KTYPI  ==  1 .AND. BRETYPI == 1 ) THEN   !Andrea
                     IF( KTYPK  <=  2  .AND. BRETYPK == 1 )THEN  !Andrea
						!   K-CARBON OR SILICON
                        NCI         = NCI    + FCIK
                        NCIDXK(IKC) = RXNIK  * DFCIKR
                        NCIDYK(IKC) = RYNIK  * DFCIKR
                        NCIDZK(IKC) = RZNIK  * DFCIKR

                        NCIDXI      = NCIDXI - NCIDXK(IKC)
                        NCIDYI      = NCIDYI - NCIDYK(IKC)
                        NCIDZI      = NCIDZI - NCIDZK(IKC)
                       
                        NHIDXK(IKC) = 0.0
                        NHIDYK(IKC) = 0.0
                        NHIDZK(IKC) = 0.0
						!print *,'SUMMING NCI'    !Andrea
                     ELSE
					 !   K-HYDROGEN
                        NHI         = NHI    + FCIK
                        NHIDXK(IKC) = RXNIK  * DFCIKR
                        NHIDYK(IKC) = RYNIK  * DFCIKR
                        NHIDZK(IKC) = RZNIK  * DFCIKR

                        NHIDXI      = NHIDXI - NHIDXK(IKC)
                        NHIDYI      = NHIDYI - NHIDYK(IKC)
                        NHIDZI      = NHIDZI - NHIDZK(IKC)

                        NCIDXK(IKC) = 0.0
                        NCIDYK(IKC) = 0.0
                        NCIDZK(IKC) = 0.0
						!print *,'SUMMING NHI'    !Andrea
                     ENDIF
                  ELSE IF( KTYPI  ==  2 ) THEN
					 !   K-C, SI OR H
                     NTIDXK(IKC) = RXNIK  * DFCIKR
                     NTIDYK(IKC) = RYNIK  * DFCIKR
                     NTIDZK(IKC) = RZNIK  * DFCIKR

                     NTIDXI      = NTIDXI - NTIDXK(IKC)
                     NTIDYI      = NTIDYI - NTIDYK(IKC)
                     NTIDZI      = NTIDZI - NTIDZK(IKC)
                  ENDIF

				  !   SUM NijConj IF C-C BOND AND K  IS CARBON.
                  IF( IJPOT  ==  1 .AND. IJBPOT == 1 ) THEN   ! Andrea
                  IF( KTYPK  ==  1 .AND. BRETYPK == 1 ) THEN  !Andrea
					 !print *, '*** in CCC loop ***'
                     XIK    = 0.0

                     XIKDKX = 0.0
                     XIKDKY = 0.0
                     XIKDKZ = 0.0


					 !   CONSIDER ALL NEBS OF K, EXCEPT I.
					 !   K--M LOOP.
                     KMC = 0
                     DO KM =DATUM(K),DATUM(K+1)-1
                        M  = NEB(KM)
                        IF ( M  /=  I ) THEN
                           KMC = KMC + 1
                           NEBOFK(IKC,KMC) = M

                           FCKM   = CUTFCN(KM)
                           DFCKMR = CUTDRV(KM)

						  !   SUM Xik AND DERIVATIVES. 
                           XIK         = XIK    + FCKM

                           XIKDMX(KMC) = DFCKMR * BNDXNM(KM)
                           XIKDMY(KMC) = DFCKMR * BNDYNM(KM)
                           XIKDMZ(KMC) = DFCKMR * BNDZNM(KM)

                           XIKDKX      = XIKDKX - XIKDMX(KMC)
                           XIKDKY      = XIKDKY - XIKDMY(KMC)
                           XIKDKZ      = XIKDKZ - XIKDMZ(KMC)

                        ENDIF
					 !   END OF K--M LOOP.
                     ENDDO
                     NUMNBK(IKC) = KMC

					 !   SUM F(Xik) AND DERIVATIVES.
                     IF     ( XIK  <=  2.0 ) THEN
                        FXIK   = 1.0
                        DFXIKX = 0.0
                     ELSE IF( XIK  >=  3.0 ) THEN
                        FXIK   = 0.0
                        DFXIKX = 0.0
                     ELSE
                        ARG    = PI  * ( XIK - 2.0 )
                        FXIK   = 0.5 * ( 1.0 + COS( ARG ) )
                        DFXIKX =-0.5 * PI * SIN( ARG )
                     ENDIF

                     NCNJDR = FXIK * DFCIKR
                     NCNJDX = FCIK * DFXIKX
        
                     NCONJ       = NCONJ  + FCIK * FXIK
                     NCNDXI      = NCNDXI - NCNJDR * RXNIK
                     NCNDYI      = NCNDYI - NCNJDR * RYNIK
                     NCNDZI      = NCNDZI - NCNJDR * RZNIK

                     NCNDXK(IKC) = NCNJDR * RXNIK + NCNJDX * XIKDKX
                     NCNDYK(IKC) = NCNJDR * RYNIK + NCNJDX * XIKDKY
                     NCNDZK(IKC) = NCNJDR * RZNIK + NCNJDX * XIKDKZ

                     DO KMC=1,NUMNBK(IKC)
                        NCNDXM(IKC,KMC) = NCNJDX *XIKDMX(KMC)
                        NCNDYM(IKC,KMC) = NCNJDX *XIKDMY(KMC)
                        NCNDZM(IKC,KMC) = NCNJDX *XIKDMZ(KMC)
                     ENDDO
                  ELSE
                     NUMNBK(IKC) = 0
                     NCNDXK(IKC) = 0.0
                     NCNDYK(IKC) = 0.0
                     NCNDZK(IKC) = 0.0

                  ENDIF
                  ENDIF


		  !   DECIDE WHICH MANY-BODY PARAMETERS ARE BEING USED.
         IF     (KTYPI == 2 .AND. KTYPK == 3) THEN
            MBTIJK = 5
         ELSE IF(KTYPI == 3 .AND. KTYPJ == 2 .AND. KTYPK == 2) THEN
            MBTIJK = 6
         ELSE
            MBTIJK = MBTIJ
         ENDIF
				 !print *, 'ktypi, ktypj, ktypk, mbtijk',i,j,ktypi, ktypj, ktypk, mbtijk

				 !   CALCULATE LENGTH DEPENDENT TERMS.
             IF( ( KTYPI == 1 .AND. (KTYPJ /= 3 .OR.  KTYPK /= 3) )     &
		       .OR. ( KTYPI == 2 .AND. ((IJPOT+IKPOT)  <=  6       ) )     &
                       .OR. ( KTYPI == 3 .AND. BRETYPJ /= 1 )   &
		       .OR. ( MBTIJK == 6 ) .OR. ( IJBPOT /= 1 ) ) THEN
					 !print *, 'QFACAN CONSTANT, KTYPI,-J,-K',i,j,k,KTYPI, KTYPJ, KTYPK
                     QFACAN = 1.0
                     QFADAN = 0.0
                  ELSE IF ( MBTIJK  ==  2 .OR. MBTIJK  ==  5 ) THEN
					 !   BETA = 3.0
					 !print *, 'BETA = 3.0'
					 !   EXP( ALPHA * {( Rij - Rik )}^BETA ) & dEXP / d(Rij-Rik).
                     DIFLEN = (RLIJ-CONRE(1,IJPOT)) - (RLIK-CONRE(1,IKPOT))
                     QFACAN = EXP( CONALP(MBTIJK) * DIFLEN * DIFLEN * DIFLEN )
                     QFADAN = CONALP(MBTIJK) * 3.0 * QFACAN * DIFLEN * DIFLEN
                  ELSE
					 !   BETA = 1.0
					 !print *, 'BETA = 1.0'
					 !   EXP( ALPHA * {( Rij - Rik )}^BETA ) & dEXP / d(Rij-Rik).
                     DIFLEN = (RLIJ-CONRE(IJBPOT,IJPOT)) - (RLIK-CONRE(IKBPOT,IKPOT))
                     QFACAN = EXP( CONALP(MBTIJK) * DIFLEN )
                     QFADAN = CONALP(MBTIJK) * QFACAN
                  ENDIF

				   !   CALCULATE ANGLE DEPENDENT TERMS.
                  IF( MBTIJK  /=  3 .OR. IJBPOT /= 1 )THEN    ! Andrea
					 !   COS( THETAijk ), G( THETAijk ) & dG / d{H-COS( THETA )}
                     COSTH  = RXNIK * RXNIJ + RYNIK * RYNIJ + RZNIK * RZNIJ
                     IF( MBTIJK == 1 .OR. MBTIJK == 4 .OR. MBTIJK == 3 ) THEN     !Andrea
                        CARG   = COND2(IKBPOT,MBTIJK) + ( CONH(IKBPOT,MBTIJK) - COSTH )*( CONH(IKBPOT,MBTIJK) - COSTH )
                        GFACAN = CONCD(IKBPOT,MBTIJK) - CONC2(IKBPOT,MBTIJK) / CARG
                        GDDAN  = 2.0 * CONC2(IKBPOT,MBTIJK) * ( CONH(IKBPOT,MBTIJK) - COSTH ) / (CARG*CARG)
					 ! print*,"GFACAN = CONCD(IKBPOT,MBTJIL) - CONC2(IKBPOT,MBTJIL) / CARG",i,j,GFACAN,CONCD(IKBPOT,MBTJIL),CONC2(IKBPOT,MBTJIL),CARG
                     ELSE
                        CARG   = CONH(1,MBTIJK) - COSTH
                        GFACAN = CONC2(1,MBTIJK) + COND2(1,MBTIJK) * CARG*CARG
                        GDDAN  = 2.0 * COND2(1,MBTIJK) * CARG
                     ENDIF


					 !    DIRECTION COSINES OF Rjk = ( Rik - Rij ) / DISjk
                     DKXC  = RXNIK * RLIK - RXNIJ * RLIJ
                     DKYC  = RYNIK * RLIK - RYNIJ * RLIJ
                     DKZC  = RZNIK * RLIK - RZNIJ * RLIJ

                     DISJK = SQRT( DKXC*DKXC+DKYC*DKYC+DKZC*DKZC )
                     DKXC  = DKXC / DISJK
                     DKYC  = DKYC / DISJK
                     DKZC  = DKZC / DISJK

					 !   dCOS( THETAijk ) / dRwh [WHERE w=x,y,z AND h =i,j,k]
                     DCSDIJ =  1.0 / RLIK - COSTH * RLIJR
                     DCSDIK =  RLIJR      - COSTH / RLIK
                     DCSDJK = - DISJK * RLIJR / RLIK

                     DCSDXI = -DCSDIJ*RXNIJ - DCSDIK*RXNIK
                     DCSDYI = -DCSDIJ*RYNIJ - DCSDIK*RYNIK
                     DCSDZI = -DCSDIJ*RZNIJ - DCSDIK*RZNIK

                     DCSDXJ =  DCSDIJ*RXNIJ                - DCSDJK*DKXC
                     DCSDYJ =  DCSDIJ*RYNIJ                - DCSDJK*DKYC
                     DCSDZJ =  DCSDIJ*RZNIJ                - DCSDJK*DKZC

                     DCSDXK =                 DCSDIK*RXNIK + DCSDJK*DKXC
                     DCSDYK =                 DCSDIK*RYNIK + DCSDJK*DKYC
                     DCSDZK =                 DCSDIK*RZNIK + DCSDJK*DKZC


					 !   FCik * EXP * dG( THETAijk ) / dCOS(THETAijk)
                     DZFAC = - FCIK * GDDAN * QFACAN

					 !   ADD DERIVATIVES OF H IF IT IS A SPLINE.
					 !   dETAij/dH = SUM [ FCik * EXP * dG( THETAijk ) / dH ]
                     IF(MBTIJK == 5) DBIDH = DBIDH - DZFAC

					 !   FCik * EXP * dG( THETAijk ) / dRwh [WHERE w=x,y,z AND h =i,j,k]
                     DGDXI = DZFAC * DCSDXI
                     DGDYI = DZFAC * DCSDYI
                     DGDZI = DZFAC * DCSDZI
                     
                     DGDXJ = DZFAC * DCSDXJ
                     DGDYJ = DZFAC * DCSDYJ
                     DGDZJ = DZFAC * DCSDZJ

                     DGDXK = DZFAC * DCSDXK
                     DGDYK = DZFAC * DCSDYK
                     DGDZK = DZFAC * DCSDZK

                  ELSE
                     GFACAN = CONCD(1,3)

                     DGDXI = 0.0
                     DGDYI = 0.0 
                     DGDZI = 0.0

                     DGDXJ = 0.0
                     DGDYJ = 0.0
                     DGDZJ = 0.0

                     DGDXK = 0.0
                     DGDYK = 0.0
                     DGDZK = 0.0

                  ENDIF

	 			 !print *,'CARG = ',CARG,'GFACAN = ',GFACAN ,'i,j',I,J     !Andrea

				 !   SUM ETAij
                  ZIJ    = ZIJ + FCIK * GFACAN * QFACAN
				 !print*,'ZIJ    = ZIJ + FCIK * GFACAN * QFACAN',I,J,ZIJ,FCIK,GFACAN,QFACAN

				 !   FCik * G( THETAijk ) * dEXP / dRij
                  DZDRIJ = GFACAN * FCIK * QFADAN

				 !   G( THETAijk ) * ( dFCik / dRik * EXP + FCik * dEXP / dRik )
                  DZDRIK = GFACAN * ( DFCIKR *QFACAN - FCIK *QFADAN )

				 !   SUM dETAij / dRwh [WHERE w=x,y,z AND h =i,j,k]
				 !   G * ( FCik * dEXP/dRwi + dFCik/dRwi * EXP ) + FCik * EXP * dG/dRwi
                  DBIDXI      =DBIDXI -DZDRIJ*RXNIJ -DZDRIK*RXNIK +DGDXI
                  DBIDYI      =DBIDYI -DZDRIJ*RYNIJ -DZDRIK*RYNIK +DGDYI
                  DBIDZI      =DBIDZI -DZDRIJ*RZNIJ -DZDRIK*RZNIK +DGDZI

				 !   G * FCik * dEXP/dRwj + FCik * EXP * dG/dRwj
                  DBIDXJ      =DBIDXJ +DZDRIJ*RXNIJ               +DGDXJ
                  DBIDYJ      =DBIDYJ +DZDRIJ*RYNIJ               +DGDYJ
                  DBIDZJ      =DBIDZJ +DZDRIJ*RZNIJ               +DGDZJ

				 !   G * ( FCik * dEXP/dRwk + dFCik/dRwk * EXP ) + FCik * EXP * dG/dRwk
                  DBIDXK(IKC) =                      DZDRIK*RXNIK +DGDXK
                  DBIDYK(IKC) =                      DZDRIK*RYNIK +DGDYK
                  DBIDZK(IKC) =                      DZDRIK*RZNIK +DGDZK

               ENDIF
			!   END OF I--K LOOP.
            ENDDO
            NUMNBI = IKC

			!   ADD ON DERIVATIVES OF BOND ORDER TERM DUE TO SPLINE FOR H(NtSi).
            IF( KTYPI  ==  2) THEN
				!   dETAij/dNti = dETAij/dH * dH/dNti
               DBIDNI = DBIDH * DHDNTI
				!   dETAij/dRwh = dETAij/dRwh + dETAij/dNti * dNti/dRwh
               DBIDXI = DBIDXI + DBIDNI * NTIDXI
               DBIDYI = DBIDYI + DBIDNI * NTIDYI
               DBIDZI = DBIDZI + DBIDNI * NTIDZI

               DBIDXJ = DBIDXJ + DBIDNI * NTIDXJ
               DBIDYJ = DBIDYJ + DBIDNI * NTIDYJ
               DBIDZJ = DBIDZJ + DBIDNI * NTIDZJ

               DO IKC = 1,NUMNBI
                  DBIDXK(IKC) = DBIDXK(IKC) + DBIDNI * NTIDXK(IKC)
                  DBIDYK(IKC) = DBIDYK(IKC) + DBIDNI * NTIDYK(IKC)
                  DBIDZK(IKC) = DBIDZK(IKC) + DBIDNI * NTIDZK(IKC)
               ENDDO
            ENDIF

			!   ADD ON Hij(Nhi,Nci) IF THIS IS A C-C OR C-H BOND. ! NOT IF BRETYP /= 1      Andrea
            IF( ( KTYPI == 1 .AND. BRETYPI == 1 .AND. BRETYPJ==1 .AND. KTYPJ /= 2 )  &
               .AND. NHI<4.0 .AND. NCI<4.0 )THEN
               IF( KTYPJ  ==  1 )THEN
				!   C-C BOND.
                  CALL HCCITP(NHI,NCI,HIJ,DHDNHI,DHDNCI)
				!print *,'C-C bond: NHI = ',NHI,'; NCI = ',NCI,'; I: ',I,'; J: ',J    !Andrea
               ELSE
				!   C-H BOND.
                  CALL HCHITP(NHI,NCI,HIJ,DHDNHI,DHDNCI)
				!print *,'C-H bond: NHI = ',NHI,'; NCI = ',NCI,'; I: ',I,'; J: ',J    !Andrea
               ENDIF

				!   ADD CONTRIBUTIONS TO ETAij AND ITS DERIVATIVES.
               ZIJ    = ZIJ    + HIJ
               DBIDXI = DBIDXI + DHDNHI * NHIDXI + DHDNCI * NCIDXI
               DBIDYI = DBIDYI + DHDNHI * NHIDYI + DHDNCI * NCIDYI
               DBIDZI = DBIDZI + DHDNHI * NHIDZI + DHDNCI * NCIDZI

				!   ADD CONTRIBUTIONS FROM DERIVATIVES TO NEIGHBOURS OF I.
               DO IKC = 1,NUMNBI
                  DBIDXK(IKC) = DBIDXK(IKC) +DHDNHI*NHIDXK(IKC) +DHDNCI*NCIDXK(IKC)
                  DBIDYK(IKC) = DBIDYK(IKC) +DHDNHI*NHIDYK(IKC) +DHDNCI*NCIDYK(IKC)
                  DBIDZK(IKC) = DBIDZK(IKC) +DHDNHI*NHIDZK(IKC) +DHDNCI*NCIDZK(IKC)
               ENDDO

            ENDIF

		 !   DO THE SAME FOR TO ATOM J.

		 !   DECIDE WHICH MANY-BODY PARAMETERS ARE BEING USED.
         IF     (KTYPJ == 1 .AND. KTYPI == 2) THEN
            MBTJI = 4
         ELSE IF(KTYPJ == 2 .AND. KTYPI == 3) THEN
            MBTJI = 5
         ELSE
            MBTJI = KTYPJ
         ENDIF

		 !   USE SPLINE TO GET VALUE FOR H FOR ANGLE TERMS IF NEEDED.
         IF( KTYPJ  ==  2 ) THEN
            CALL HSIITP(NTSI(J),CONH(1,5),DHDNTJ)
            NTJDXJ = RXNIJ * DFCIJR
            NTJDYJ = RYNIJ * DFCIJR
            NTJDZJ = RZNIJ * DFCIJR
            NTJDXI = - NTJDXJ
            NTJDYI = - NTJDYJ
            NTJDZI = - NTJDZJ

            DBJDH  = 0.0
         ENDIF

			!   CALCULATE COMPONENTS OF BOND ORDER TERM AND DERIVATIVES
			!   WITH RESPECT TO BOND I-J FOR ATOM J.
            ZJI    = 0.0
            DBJDXI = 0.0
            DBJDYI = 0.0
            DBJDZI = 0.0
            DBJDXJ = 0.0
            DBJDYJ = 0.0
            DBJDZJ = 0.0


			!   J--L LOOP.
            JLC = 0
            DO JL =DATUM(J),DATUM(J+1)-1
               L = NEB(JL)
				!   CONSIDER ALL NEIGHBOURS OF J, EXCEPT I.
                IF( L  /=  I )THEN
                  JLC = JLC + 1
                  NEBOFJ(JLC) = L
                  KTYPL = KTYP(L)
                  BRETYPL = BRETYP(L)
                  JLBPOT = BNDTYPBRE(JL)
                  JLPOT = BNDTYP(JL)
                  RLJL  = BNDLEN(JL)
                  RXNJL = BNDXNM(JL)
                  RYNJL = BNDYNM(JL)
                  RZNJL = BNDZNM(JL)

                  FCJL   = CUTFCN(JL)
                  DFCJLR = CUTDRV(JL)

				  !   SUM THE NUMBER OF ATOMS ON J IF CARBON OR SILICON.
                	IF( KTYPJ  ==  1 .AND. BRETYPJ == 1 ) THEN  !Andrea

                 		IF( KTYPL  <=  2 .AND. BRETYPL == 1 )THEN  !Andrea
							!   L-CARBON OR SILICON
	                        NCJ         = NCJ    + FCJL
	                        NCJDXL(JLC) = RXNJL  * DFCJLR
	                        NCJDYL(JLC) = RYNJL  * DFCJLR
	                        NCJDZL(JLC) = RZNJL  * DFCJLR

	                        NCJDXJ      = NCJDXJ - NCJDXL(JLC)
	                        NCJDYJ      = NCJDYJ - NCJDYL(JLC)
	                        NCJDZJ      = NCJDZJ - NCJDZL(JLC)
	                        NHJDXL(JLC) = 0.0
	                        NHJDYL(JLC) = 0.0
	                        NHJDZL(JLC) = 0.0

                    	ELSE
							!   L-HYDROGEN
	                        NHJ         = NHJ    + FCJL

	                        NHJDXL(JLC) = RXNJL  * DFCJLR
	                        NHJDYL(JLC) = RYNJL  * DFCJLR
	                        NHJDZL(JLC) = RZNJL  * DFCJLR

	                        NHJDXJ      = NHJDXJ - NHJDXL(JLC)
	                        NHJDYJ      = NHJDYJ - NHJDYL(JLC)
	                        NHJDZJ      = NHJDZJ - NHJDZL(JLC)

	                        NCJDXL(JLC) = 0.0
	                        NCJDYL(JLC) = 0.0
	                        NCJDZL(JLC) = 0.0

                 		ENDIF
                	ELSE IF( KTYPJ  ==  2 ) THEN
					 !   K-C, SI OR H
	                     NTJDXL(JLC) = RXNJL  * DFCJLR
	                     NTJDYL(JLC) = RYNJL  * DFCJLR
	                     NTJDZL(JLC) = RZNJL  * DFCJLR

	                     NTJDXJ      = NTJDXJ - NTJDXL(JLC)
	                     NTJDYJ      = NTJDYJ - NTJDYL(JLC)
	                     NTJDZJ      = NTJDZJ - NTJDZL(JLC)
                ENDIF

					!   SUM NijConj IF C-C BOND AND L  IS CARBON.
                  IF( IJPOT  ==  1 .AND. IJBPOT == 1 ) THEN   ! Andrea
                     IF( KTYPL  ==  1 .AND. BRETYPL == 1 ) THEN
                        XJL    = 0.0
                        XJLDLX = 0.0
                        XJLDLY = 0.0
                        XJLDLZ = 0.0
     
						!     CONSIDER ALL NEBS OF L, EXCEPT J.
     
						!     L--N LOOP.
                        LNC = 0
                        DO LN =DATUM(L),DATUM(L+1)-1
                           N = NEB(LN)
                           IF ( N  /=  J ) THEN
                              LNC = LNC + 1
                              NEBOFL(JLC,LNC) = N

                              FCLN   = CUTFCN(LN)
                              DFCLNR = CUTDRV(LN)
     
							  !     SUM Xik AND DERIVATIVES. 
                              XJL         = XJL    + FCLN
                              XJLDNX(LNC) = DFCLNR * BNDXNM(LN)
                              XJLDNY(LNC) = DFCLNR * BNDYNM(LN)
                              XJLDNZ(LNC) = DFCLNR * BNDZNM(LN)
                              
                              XJLDLX      = XJLDLX - XJLDNX(LNC)
                              XJLDLY      = XJLDLY - XJLDNY(LNC)
                              XJLDLZ      = XJLDLZ - XJLDNZ(LNC)

                           ENDIF
						!     END OF L--N LOOP.
                        ENDDO
                        NUMNBL(JLC) = LNC

						!     SUM F(Xik) AND DERIVATIVES.
                        IF     ( XJL  <=  2.0 ) THEN
                           FXJL   = 1.0
                           DFXJLX = 0.0
                        ELSE IF( XJL  >=  3.0 ) THEN
                           FXJL   = 0.0
                           DFXJLX = 0.0
                        ELSE
                           ARG    = PI  * ( XJL - 2.0 )
                           FXJL   = 0.5 * ( 1.0 + COS( ARG ) )
                           DFXJLX =-0.5 * PI * SIN( ARG )
                        ENDIF
     
                        NCNJDR = FXJL * DFCJLR
                        NCNJDX = FCJL * DFXJLX
     
                        NCONJ       = NCONJ  + FCJL * FXJL
                        NCNDXJ      = NCNDXJ - NCNJDR * RXNJL
                        NCNDYJ      = NCNDYJ - NCNJDR * RYNJL
                        NCNDZJ      = NCNDZJ - NCNJDR * RZNJL

                        NCNDXL(JLC) = NCNJDR * RXNJL + NCNJDX * XJLDLX
                        NCNDYL(JLC) = NCNJDR * RYNJL + NCNJDX * XJLDLY
                        NCNDZL(JLC) = NCNJDR * RZNJL + NCNJDX * XJLDLZ
                                                
                        DO LNC=1,NUMNBL(JLC)
                           NCNDXN(JLC,LNC) = NCNJDX *XJLDNX(LNC)
                           NCNDYN(JLC,LNC) = NCNJDX *XJLDNY(LNC)
                           NCNDZN(JLC,LNC) = NCNJDX *XJLDNZ(LNC)
                        ENDDO
                     ELSE
                        NUMNBL(JLC) = 0
                        NCNDXL(JLC) = 0.0
                        NCNDYL(JLC) = 0.0
                        NCNDZL(JLC) = 0.0
                     ENDIF
                  ENDIF

		 !   DECIDE WHICH MANY-BODY PARAMETERS ARE BEING USED.
         IF     (KTYPJ == 2 .AND. KTYPL == 3) THEN
            MBTJIL = 5
         ELSE IF(KTYPJ == 3 .AND. KTYPI == 2 .AND. KTYPL == 2) THEN
            MBTJIL = 6
         ELSE
            MBTJIL = MBTJI
         ENDIF

			!   CALCULATE LENGTH DEPENDENT TERMS.
         IF( ( KTYPJ == 1 .AND. (KTYPI /= 3 .OR.  KTYPL /= 3) )  &
		       .OR. ( KTYPJ == 2 .AND. ((IJPOT+JLPOT)  <=  6       ) )  &
                       .OR. ( KTYPJ == 3 .AND. BRETYPI /= 1 )   &
		       .OR. ( MBTJIL == 6 ) .OR. ( IJBPOT /= 1 ) ) THEN
			 !   CONSTANT  (ALSO FOR ALL BRETYPES  .ANDREA)
			 !print *, 'QFACAN CONSTANT, KTYPI,-J,-K',i,j,k,KTYPI, KTYPJ, KTYPK
                     QFACAN = 1.0
                     QFADAN = 0.0
                  ELSE IF ( MBTJIL  ==  2 .OR. MBTJIL  ==  5 ) THEN
					 !   BETA = 3.0
					 !   EXP( ALPHA * {( Rij - Rjl )}^BETA ) & dEXP / d(Rij-Rjl).
                     DIFLEN = (RLIJ-CONRE(1,IJPOT)) - (RLJL-CONRE(1,JLPOT))
                     QFACAN = EXP( CONALP(MBTJIL) * DIFLEN * DIFLEN * DIFLEN )
                     QFADAN = CONALP(MBTJIL) * 3.0 * QFACAN * DIFLEN * DIFLEN
                  ELSE
					 !   BETA = 1.0
					 !   EXP( ALPHA * {( Rij - Rjl )}^BETA ) & dEXP / d(Rij-Rjl).
                     DIFLEN = (RLIJ-CONRE(IJBPOT,IJPOT)) - (RLJL-CONRE(JLBPOT,JLPOT))
                     QFACAN = EXP( CONALP(MBTJIL) * DIFLEN )
                     QFADAN = CONALP(MBTJIL) *  QFACAN
                  ENDIF

				  !   CALCULATE ANGLE DEPENDENT TERMS.
                  IF( MBTJIL  /=  3 .OR. IJBPOT /= 1)THEN
					 !   COS( THETAjil ), G( THETAjil ) & dG / d{H-COS( THETA )}
                     COSTH  = -(RXNJL  * RXNIJ  + RYNJL * RYNIJ  + RZNJL  * RZNIJ)
                     IF( MBTJIL == 1 .OR. MBTJIL == 4 .OR. MBTJIL == 3 ) THEN
                        CARG   = COND2(JLBPOT,MBTJIL) + ( CONH(JLBPOT,MBTJIL) - COSTH )*( CONH(JLBPOT,MBTJIL) - COSTH )
                        GFACAN = CONCD(JLBPOT,MBTJIL) - CONC2(JLBPOT,MBTJIL) / CARG
						!print*,"GFACAN = CONCD(JLBPOT,MBTJIL) - CONC2(JLBPOT,MBTJIL) / CARG",i,j,GFACAN,CONCD(JLBPOT,MBTJIL),CONC2(JLBPOT,MBTJIL),CARG
                    	GDDAN  = 2.0 * CONC2(JLBPOT,MBTJIL) * ( CONH(JLBPOT,MBTJIL) - COSTH ) / (CARG*CARG)
                     ELSE
                        CARG   = CONH(1,MBTJIL) - COSTH
                        GFACAN = CONC2(1,MBTJIL) + COND2(1,MBTJIL) * CARG*CARG
                        GDDAN  = 2.0 * COND2(1,MBTJIL) * CARG
                     ENDIF

					 !    DIRECTION COSINES OF Ril = ( Rjl - Rji ) / DISjl
                     DLXC  = RXNJL * RLJL + RXNIJ * RLIJ
                     DLYC  = RYNJL * RLJL + RYNIJ * RLIJ
                     DLZC  = RZNJL * RLJL + RZNIJ * RLIJ

                     DISIL = SQRT( DLXC*DLXC+DLYC*DLYC+DLZC*DLZC )
                     DLXC  = DLXC / DISIL
                     DLYC  = DLYC / DISIL
                     DLZC  = DLZC / DISIL


					 !   dCOS( THETAjil ) / dRwh [WHERE w=x,y,z AND h =j,i,l]
                     DCSDJI =  1.0 / RLJL - COSTH * RLIJR
                     DCSDJL =  RLIJR - COSTH / RLJL
                     DCSDIL = -DISIL * RLIJR / RLJL

                     DCSDXJ =  DCSDJI*RXNIJ - DCSDJL*RXNJL 
                     DCSDYJ =  DCSDJI*RYNIJ - DCSDJL*RYNJL 
                     DCSDZJ =  DCSDJI*RZNIJ - DCSDJL*RZNJL 

                     DCSDXI = -DCSDJI*RXNIJ                - DCSDIL*DLXC
                     DCSDYI = -DCSDJI*RYNIJ                - DCSDIL*DLYC
                     DCSDZI = -DCSDJI*RZNIJ                - DCSDIL*DLZC

                     DCSDXL =                 DCSDJL*RXNJL + DCSDIL*DLXC
                     DCSDYL =                 DCSDJL*RYNJL + DCSDIL*DLYC
                     DCSDZL =                 DCSDJL*RZNJL + DCSDIL*DLZC


					 !   FCjl * EXP * dG( THETAjil ) / dCOS(THETAjil)
                     DZFAC = - FCJL * GDDAN * QFACAN

					 !   ADD DERIVATIVES OF H IF IT IS A SPLINE.
					 !   dETAji/dH = SUM [ FCjl * EXP * dG( THETAjil ) / dH ]
                     IF(MBTJIL == 5) DBJDH = DBJDH - DZFAC

					 !   FCjl * EXP * dG( THETAjil ) / dRwh [WHERE w=x,y,z AND h =j,i,l]

                     DGDXJ = DZFAC * DCSDXJ
                     DGDYJ = DZFAC * DCSDYJ
                     DGDZJ = DZFAC * DCSDZJ

                     DGDXI = DZFAC * DCSDXI
                     DGDYI = DZFAC * DCSDYI
                     DGDZI = DZFAC * DCSDZI

                     DGDXL = DZFAC * DCSDXL
                     DGDYL = DZFAC * DCSDYL
                     DGDZL = DZFAC * DCSDZL
                     
                  ELSE
                     GFACAN = CONCD(1,3)

                     DGDXJ = 0.0
                     DGDYJ = 0.0 
                     DGDZJ = 0.0

                     DGDXI = 0.0
                     DGDYI = 0.0
                     DGDZI = 0.0

                     DGDXL = 0.0
                     DGDYL = 0.0
                     DGDZL = 0.0

                  ENDIF

				  !   SUM ETAji
                  ZJI    = ZJI + FCJL * GFACAN * QFACAN
				  !print*," ZJI    = ZJI + FCJL * GFACAN * QFACAN",i,j, ZJI,FCJL,GFACAN,QFACAN

				  !   FCjl * G( THETAjil ) * dEXP / dRji
                  DZDRJI = GFACAN * FCJL * QFADAN

				  !   G( THETAjil ) * ( dFCjl / dRjl * EXP + FCjl * dEXP / dRjl )
                  DZDRJL = GFACAN * ( DFCJLR *QFACAN - FCJL *QFADAN )

				  !   SUM dETAji / dRwh [WHERE w=x,y,z AND h =j,i,l]
				  !   G * ( FCjl * dEXP/dRwj + dFCjl/dRwj * EXP ) + FCjl * EXP * dG/dRwj
                  DBJDXJ      =DBJDXJ +DZDRJI*RXNIJ -DZDRJL*RXNJL +DGDXJ
                  DBJDYJ      =DBJDYJ +DZDRJI*RYNIJ -DZDRJL*RYNJL +DGDYJ
                  DBJDZJ      =DBJDZJ +DZDRJI*RZNIJ -DZDRJL*RZNJL +DGDZJ

				  !   G * FCjl * dEXP/dRwi * FCjl * EXP * dG/dRwi
                  DBJDXI      =DBJDXI -DZDRJI*RXNIJ               +DGDXI
                  DBJDYI      =DBJDYI -DZDRJI*RYNIJ               +DGDYI
                  DBJDZI      =DBJDZI -DZDRJI*RZNIJ               +DGDZI

				  !   G * ( FCjl * dEXP/dRwl + dFCjl/dRwl * EXP ) + FCjl * EXP * dG/dRwl
                  DBJDXL(JLC) =                      DZDRJL*RXNJL +DGDXL
                  DBJDYL(JLC) =                      DZDRJL*RYNJL +DGDYL
                  DBJDZL(JLC) =                      DZDRJL*RZNJL +DGDZL

               ENDIF

			 !   END OF J--L LOOP.
            ENDDO
            NUMNBJ = JLC

			 !   ADD ON DERIVATIVES OF BOND ORDER TERM DUE TO SPLINE FOR H(NtSi).
            IF( KTYPJ  ==  2) THEN
			   !   dETAji/dNtj = dETAji/dH * dH/dNtj
               DBJDNJ = DBJDH * DHDNTJ
			   !   dETAji/dRwh = dETAji/dRwh + dETAji/dNtj * dNtj/dRwh
               DBJDXJ = DBJDXJ + DBJDNJ * NTJDXJ
               DBJDYJ = DBJDYJ + DBJDNJ * NTJDYJ
               DBJDZJ = DBJDZJ + DBJDNJ * NTJDZJ

               DBJDXI = DBJDXI + DBJDNJ * NTJDXI
               DBJDYI = DBJDYI + DBJDNJ * NTJDYI
               DBJDZI = DBJDZI + DBJDNJ * NTJDZI

               DO JLC = 1,NUMNBJ
                  DBJDXL(JLC) = DBJDXL(JLC) + DBJDNJ * NTJDXL(JLC)
                  DBJDYL(JLC) = DBJDYL(JLC) + DBJDNJ * NTJDYL(JLC)
                  DBJDZL(JLC) = DBJDZL(JLC) + DBJDNJ * NTJDZL(JLC)
               ENDDO
            ENDIF

			!   ADD ON Hji(Nhj,Ncj) IF THIS IS A C-C OR H-C BOND.
            IF( ( KTYPJ == 1 .AND. KTYPI /= 2 .AND. BRETYPI == 1 .AND. BRETYPJ == 1 ) &
               .AND. NHJ < 4.0 .AND. NCJ < 4.0 )THEN
				!print *,'ADDING HJI TERMS!!!'
               IF( KTYPI  ==  1 )THEN
				  !   C-C BOND.
                  CALL HCCITP(NHJ,NCJ,HJI,DHDNHJ,DHDNCJ)

               ELSE
				  !   C-H BOND.
                  CALL HCHITP(NHJ,NCJ,HJI,DHDNHJ,DHDNCJ)
               ENDIF

			   !   ADD CONTRIBUTIONS TO ETAji AND ITS DERIVATIVES.
               ZJI    = ZJI    + HJI
			   !print*,'ZJI=ZIJ+HJI',ZJI ,HJI
               DBJDXJ = DBJDXJ + DHDNHJ * NHJDXJ + DHDNCJ * NCJDXJ
               DBJDYJ = DBJDYJ + DHDNHJ * NHJDYJ + DHDNCJ * NCJDYJ
               DBJDZJ = DBJDZJ + DHDNHJ * NHJDZJ + DHDNCJ * NCJDZJ


			   !   ADD CONTRIBUTIONS FROM DERIVATIVES TO NEIGHBOURS OF J.
               DO JLC = 1,NUMNBJ
                  DBJDXL(JLC) = DBJDXL(JLC) +DHDNHJ*NHJDXL(JLC) +DHDNCJ*NCJDXL(JLC)
                  DBJDYL(JLC) = DBJDYL(JLC) +DHDNHJ*NHJDYL(JLC) +DHDNCJ*NCJDYL(JLC)
                  DBJDZL(JLC) = DBJDZL(JLC) +DHDNHJ*NHJDZL(JLC) +DHDNCJ*NCJDZL(JLC)
               ENDDO
            ENDIF

			!   Bij & 0.5 * FCij * FAij * dBij / dETAij 
            IF( MBTIJ  /=  2 .AND. MBTIJ  /=  4 )  THEN
				!   ZIJ CAN BE NEGATIVE IN BRENNER, PLUS DON'T NEED Ni & Ni-1 (Ni=1).
               ARG = 1.0 + ZIJ
               BIJ = ARG ** CONPE(MBTIJ)
				!print*,'ARG = 1.0 + ZIJ ; BIJ = ARG ** CONPE(MBTIJ)'
				!print*,'zij,conpe,bij,arg',i,j,ZIJ,CONPE(MBTIJ),BIJ,ARG
               DFBIJ = CONAN(MBTIJ) * FCIJ * FAIJ * ARG ** CONPF(MBTIJ)
            ELSE IF( ZIJ  <=  0.0 )  THEN
               BIJ = 1.0
               DFBIJ = 0.0
            ELSE 
               ARG = 1.0 + ZIJ ** CONN(MBTIJ)
               BIJ = ARG ** CONPE(MBTIJ)
               DFBIJ = CONAN(MBTIJ) * FCIJ * FAIJ * ( ZIJ ** CONNM(MBTIJ) ) * ( ARG ** CONPF(MBTIJ) )
            ENDIF

			 !   Bji & 0.5 * FCij * FAij * dBji / dETAji 
            IF( MBTJI  /=  2 .AND. MBTJI  /=  4 )  THEN
			   !   ZJI CAN BE NEGATIVE IN BRENNER, PLUS DON'T NEED Nj & Nj-1 (Nj=1).
               ARG = 1.0 + ZJI
               BJI = ARG ** CONPE(MBTJI)
			   !print*,'zji,conpe,bji,arg',i,j,ZJI,CONPE(MBTJI),BJI,ARG
               DFBJI = CONAN(MBTJI) * FCIJ * FAIJ * ARG ** CONPF(MBTJI)
            ELSE IF( ZJI  <=  0.0 )  THEN
               BJI = 1.0
               DFBJI = 0.0
            ELSE 
               ARG = 1.0 + ZJI ** CONN(MBTJI)
               BJI = ARG ** CONPE(MBTJI)
               DFBJI = CONAN(MBTJI) * FCIJ * FAIJ * ( ZJI ** CONNM(MBTJI) ) * ( ARG ** CONPF(MBTJI) )
            ENDIF


			 !   SCALE IF THIS IS A C-SI MIXED BOND.
            IF( IJPOT  ==  2 ) THEN
               BIJ    = BIJ   * CONCSI
               DFBIJ  = DFBIJ * CONCSI
               BJI    = BJI   * CONCSI
               DFBJI  = DFBJI * CONCSI
            ENDIF

			 !   ADD ON Fij(Nti,Ntj,Nconj)/2 IF THIS IS A C-C BOND.
            NTI = NHI + NCI
            NTJ = NHJ + NCJ


            IF( IJPOT == 1 .AND. IJBPOT == 1 .AND. NTI < 4.0 .AND. NTJ < 4.0 )THEN !Andrea
               IF(NCONJ > 2.0)NCONJ = 2.0
			 !print *,'ADDING CONJUGATION TERMS!!!'
               CALL FCCITP(NTI,NTJ,NCONJ,FIJ,DFDNTI,DFDNTJ,DFDCNJ)

               DFDNTI = 0.5 * FCIJ * FAIJ * DFDNTI
               DFDNTJ = 0.5 * FCIJ * FAIJ * DFDNTJ
               DFDCNJ = 0.5 * FCIJ * FAIJ * DFDCNJ

				!   ADD CONTRIBUTION TO BOND ORDER TERM.
               BIJ = BIJ + FIJ

			   !   ADD FORCES DUE TO Fcc.
			   !   - ( 0.5 * FCij * FAij * dFcc/dRwi ).


               pbuf(i3+1)=pbuf(i3+1)-( DFDNTI *(NHIDXI+NCIDXI) +DFDCNJ*NCNDXI )
               pbuf(i3+2)=pbuf(i3+2)-( DFDNTI *(NHIDYI+NCIDYI) +DFDCNJ*NCNDYI )
               pbuf(i3+3)=pbuf(i3+3)-( DFDNTI *(NHIDZI+NCIDZI) +DFDCNJ*NCNDZI )

				!   - ( 0.5 * FCij * FAij * dFcc/dRwj ).
               pbuf(j3+1)=pbuf(j3+1)-( DFDNTJ *(NHJDXJ+NCJDXJ) +DFDCNJ*NCNDXJ )
               pbuf(j3+2)=pbuf(j3+2)-( DFDNTJ *(NHJDYJ+NCJDYJ) +DFDCNJ*NCNDYJ )
               pbuf(j3+3)=pbuf(j3+3)-( DFDNTJ *(NHJDZJ+NCJDZJ) +DFDCNJ*NCNDZJ )

			    !if (ibuf(i3+3)==34 .or. ibuf(j3+3)==34) then
            	!	print '(A,3I2,2F8.5,2I4)', "1810*** ", myproc, dirbuf(i), dirbuf(j), &
            	!		(DFDNTI*(NHIDXI+NCIDXI)+DFDCNJ*NCNDXI), (DFDNTJ*(NHJDXJ+NCJDXJ)+DFDCNJ*NCNDXJ), &
            	!		 ibuf(i3+3), ibuf(j3+3)
            	!endif

				!   CALCULATE FORCES ON NEIGHBOURS OF I.
               DO IKC = 1,NUMNBI
                  K     = NEBOFI(IKC)
                  k3=k*3-3
				  !     - ( 0.5 * FCij * FAij * dFcc/dRwk ).
                  pbuf(k3+1)= pbuf(k3+1) -( DFDNTI *(NHIDXK(IKC)+NCIDXK(IKC)) + DFDCNJ * NCNDXK(IKC))
                  pbuf(k3+2)= pbuf(k3+2) -( DFDNTI *(NHIDYK(IKC)+NCIDYK(IKC)) + DFDCNJ * NCNDYK(IKC))
                  pbuf(k3+3)= pbuf(k3+3) -( DFDNTI *(NHIDZK(IKC)+NCIDZK(IKC)) + DFDCNJ * NCNDZK(IKC))

				  !   FORCES ON SECOND NEIGHBOURS DUE TO CONJUGATION.
                  DO KMC=1,NUMNBK(IKC)
                     M = NEBOFK(IKC,KMC)
                     m3=m*3-3
                     pbuf(m3+1) = pbuf(m3+1) - DFDCNJ*NCNDXM(IKC,KMC)
                     pbuf(m3+2) = pbuf(m3+2) - DFDCNJ*NCNDYM(IKC,KMC)
                     pbuf(m3+3) = pbuf(m3+3) - DFDCNJ*NCNDZM(IKC,KMC)
                     !if (ibuf(m3+3)==34) then
            		 !	print '(A,4I2,F10.5,3I4)', "1830*** ", myproc, dirbuf(i), dirbuf(j), dirbuf(m), &
            		 !	 DFDCNJ*NCNDXM(IKC,KMC), ibuf(i3+3), ibuf(j3+3), ibuf(m3+3)
            		 !endif
                  ENDDO
               ENDDO

			   !   CALCULATE FORCES ON NEIGHBOURS OF J.
               DO JLC = 1,NUMNBJ
                  L     = NEBOFJ(JLC)
                  l3=l*3-3
				  !   - ( 0.5 * FCij * FAij * dFcc/dRwl  ).
                  pbuf(l3+1)= pbuf(l3+1) -( DFDNTJ *(NHJDXL(JLC)+NCJDXL(JLC)) + DFDCNJ * NCNDXL(JLC) )
                  pbuf(l3+2)= pbuf(l3+2) -( DFDNTJ *(NHJDYL(JLC)+NCJDYL(JLC)) + DFDCNJ * NCNDYL(JLC) )
                  pbuf(l3+3)= pbuf(l3+3) -( DFDNTJ *(NHJDZL(JLC)+NCJDZL(JLC)) + DFDCNJ * NCNDZL(JLC) )
                  !if (ibuf(l3+3)==34) then
            	  !		print '(A,4I2,F10.5,3I4)', "1846*** ", myproc, dirbuf(i), dirbuf(j), dirbuf(l), &
            	  !		-( DFDNTJ *(NHJDXL(JLC)+NCJDXL(JLC)) + DFDCNJ * NCNDXL(JLC) ), ibuf(i3+3), ibuf(j3+3), ibuf(l3+3)
            	  !endif

				  !   FORCES ON SECOND NEIGHBOURS DUE TO CONJUGATION.
                  DO LNC=1,NUMNBL(JLC)
                     N = NEBOFL(JLC,LNC)
                     n3=n*3-3
                     pbuf(n3+1) = pbuf(n3+1) - DFDCNJ*NCNDXN(JLC,LNC)
                     pbuf(n3+2) = pbuf(n3+2) - DFDCNJ*NCNDYN(JLC,LNC)
                     pbuf(n3+3) = pbuf(n3+3) - DFDCNJ*NCNDZN(JLC,LNC)
                 	 !if (ibuf(n3+3)==34) then
            		 !	print '(A,4I2,F10.5,3I4)', "1858*** ", myproc, dirbuf(i), dirbuf(j), dirbuf(n), &
            		 !	-DFDCNJ*NCNDXN(JLC,LNC), ibuf(i3+3), ibuf(j3+3), ibuf(n3+3)
            	  	 !endif

                  ENDDO
               ENDDO
            ENDIF ! End of C-C bond if

			!   AVERAGE THE BOND ORDER TERMS FOR ATOMS I AND J.
            BAVEIJ = 0.5 * ( BIJ + BJI )
	
			!   NOW CALCULATE THE POTENTIAL ENERGIES AND FORCES FOR I AND J.
            VFAC   = FRIJ + BAVEIJ * FAIJ
            HLFVIJ = FCIJ * VFAC / 2.0
            ebuf(I)  = ebuf(I) + HLFVIJ
   	        ebuf(J)  = ebuf(J) + HLFVIJ

			!print*,'HLFVIJ = FCIJ * VFAC / 2.0 ; VFAC   = FRIJ + BAVEIJ * FAIJ'
			!print *,'i,j,bij,bji,bij_ave, FRij, FAij,Fcij,hlfvij',I,J,BIJ,BJI,BAVEIJ,FRIJ,FAIJ,FCIJ,HLFVIJ
				    !print *,'Fermi V0',HLFVIJ  ! Should be VFAC!!

			!   dVij / dRij
            DFFAC=( DFRIJR + BAVEIJ * DFAIJR ) * FCIJ + DFCIJR * VFAC

			! splinereppot here??. Yes, add term dF fc Vb and reppot terms!
            if (reppotspl > RLIJ) then
               ! Spline pair potential
               ! Note that there is a different sign convention
               ! for the forces !
               fermiVr=0.0d0
               fermidVr=0.0d0
               call splinereppot(RLIJ,fermiVr,fermidVr,fermib(IJBPOT,IJPOT), &
				fermir(IJBPOT,IJPOT),itype,jtype,fermihelp,fermihelp2)
			   !	print*,'splinereppot',RLIJ,fermiVr,fermidVr,fermib(IJBPOT,IJPOT), &
			   !    fermir(IJBPOT,IJPOT),abs(atype(I)),abs(atype(J)),fermihelp,fermihelp2
               if (abs(fermihelp-fermifcn(IJ)) > 1d-10 ) then
                    print *,'HORROR Fermi ERROR',fermihelp,fermifcn(IJ)
                    STOP 'Fermi'
               endif
               if (abs(fermihelp2-fermidrv(IJ)) > 1d-10 ) then
                    print *,'HORROR Fermi drv ERROR',fermihelp2,fermidrv(IJ)
                    STOP 'Fermi drv'
               endif

                 ! Andrea
				 ! print *,'Fermi (1-F)*Vrep',fermiVr,' (1-F)*dVrep - dF*Vrep',fermidVr
                 
                 ! Add reppot and reppot deriv to ordinary forces

                 ebuf(I)  = ebuf(I) + fermiVr/2.0d0
                 ebuf(J)  = ebuf(J) + fermiVr/2.0d0

				 !print*,'adding fermiVr (i.e. V)/2 to Epair(i),Epair(j)',i,j, fermiVr,Epair(I),Epair(J)
                 pbuf(i3+1)=pbuf(i3+1) + fermidVr*RXNIJ
                 pbuf(i3+2)=pbuf(i3+2) + fermidVr*RYNIJ
                 pbuf(i3+3)=pbuf(i3+3) + fermidVr*RZNIJ

                 pbuf(j3+1)=pbuf(j3+1) - fermidVr*RXNIJ
                 pbuf(j3+2)=pbuf(j3+2) - fermidVr*RYNIJ
                 pbuf(j3+3)=pbuf(j3+3) - fermidVr*RZNIJ
           
			    !if (ibuf(i3+3)==34 .or. ibuf(j3+3)==34) then
            	!	print '(A,3I2,2F10.5,2I4)', "1935*** ", myproc, dirbuf(i), dirbuf(j), &
            	!	fermidVr*RXNIJ, -fermidVr*RXNIJ, ibuf(i3+3), ibuf(j3+3)
            	!endif

                 ! Add dFermi force term

                 fermihelp=fermidrv(IJ)*VFAC*FCIJ

		 		 !print *,'Fermi dF*VB',fermihelp,fermidrv(IJ),HLFVIJ
		 
                 pbuf(i3+1)=pbuf(i3+1) + fermihelp*RXNIJ
                 pbuf(i3+2)=pbuf(i3+2) + fermihelp*RYNIJ
                 pbuf(i3+3)=pbuf(i3+3) + fermihelp*RZNIJ

                 pbuf(j3+1)=pbuf(j3+1) - fermihelp*RXNIJ
                 pbuf(j3+2)=pbuf(j3+2) - fermihelp*RYNIJ
                 pbuf(j3+3)=pbuf(j3+3) - fermihelp*RZNIJ

                 !if (ibuf(i3+3)==34 .or. ibuf(j3+3)==34) then
            	 !	print '(A,3I2,2F10.5,2I4)', "1955*** ", myproc, dirbuf(i), dirbuf(j), &
            	 !	fermihelp*RXNIJ, -fermihelp*RXNIJ, ibuf(i3+3), ibuf(j3+3)
            	 !endif
                 
            endif
	    
	    	!print *,'Fermi F*dVbrenner',DFFAC,-DFBIJ,-DFBJI

			!   - ( dVij / dRwi + 0.5 * FCij * FAij * ( dBij / dRwi + dBji / dRwi )
            pbuf(i3+1)=pbuf(i3+1) -( -DFFAC*RXNIJ  + DFBIJ*DBIDXI + DFBJI*DBJDXI )
            pbuf(i3+2)=pbuf(i3+2) -( -DFFAC*RYNIJ  + DFBIJ*DBIDYI + DFBJI*DBJDYI )
            pbuf(i3+3)=pbuf(i3+3) -( -DFFAC*RZNIJ  + DFBIJ*DBIDZI + DFBJI*DBJDZI )

			!   - ( dVij / dRwj + 0.5 * FCij * FAij * ( dBij / dRwj + dBji / dRwj )
            pbuf(j3+1)=pbuf(j3+1) -( DFFAC*RXNIJ + DFBIJ*DBIDXJ + DFBJI*DBJDXJ )
            pbuf(j3+2)=pbuf(j3+2) -( DFFAC*RYNIJ + DFBIJ*DBIDYJ + DFBJI*DBJDYJ )
            pbuf(j3+3)=pbuf(j3+3) -( DFFAC*RZNIJ + DFBIJ*DBIDZJ + DFBJI*DBJDZJ )

			!if (ibuf(i3+3)==34 .or. ibuf(j3+3)==34) then
            !		print '(A,3I2,2F10.5,2I4)', "1975*** ", myproc, dirbuf(i), dirbuf(j), &
            !		-( -DFFAC*RXNIJ  + DFBIJ*DBIDXI + DFBJI*DBJDXI ), -( DFFAC*RXNIJ + DFBIJ*DBIDXJ + DFBJI*DBJDXJ ), &
            !		 ibuf(i3+3), ibuf(j3+3)
            !endif

			!   CALCULATE FORCES ON NEIGHBOURS OF I.
            DO IKC = 1,NUMNBI
               K = NEBOFI(IKC)
               k3=k*3-3
				!   - ( 0.5 * FCij * FAij * dBij / dRwk ).
               pbuf(k3+1)= pbuf(k3+1) - DFBIJ * DBIDXK(IKC)
               pbuf(k3+2)= pbuf(k3+2) - DFBIJ * DBIDYK(IKC)
               pbuf(k3+3)= pbuf(k3+3) - DFBIJ * DBIDZK(IKC)
               !if (ibuf(k3+3)==34) then
               !		print '(A,4I2,F10.5,3I4)', "1987*** ", myproc, dirbuf(i), dirbuf(j), dirbuf(k), &
               !		 -DFBIJ * DBIDXK(IKC), ibuf(i3+3), ibuf(j3+3), ibuf(k3+3)
               ! endif
            ENDDO


			!   CALCULATE FORCES ON NEIGHBOURS OF J.
            DO JLC = 1,NUMNBJ
               L = NEBOFJ(JLC)
               l3=l*3-3
				!   - ( 0.5 * FCij * FAij * dBji / dRwl ).
               pbuf(l3+1)= pbuf(l3+1) - DFBJI * DBJDXL(JLC)
               pbuf(l3+2)= pbuf(l3+2) - DFBJI * DBJDYL(JLC)
               pbuf(l3+3)= pbuf(l3+3) - DFBJI * DBJDZL(JLC)
                ! if (ibuf(l3+3)==34) then
            	!		print '(A,4I2,F10.5,3I4)', "2001*** ", myproc, dirbuf(i), dirbuf(j), dirbuf(l), &
            	!		- DFBJI * DBJDXL(JLC), ibuf(i3+3), ibuf(j3+3), ibuf(l3+3)
            	!endif
            ENDDO

			!   ADD ON FORCES DUE TO SPLINES IN SI-H MORSE TERMS.
            IF( IJPOT  ==  5 ) THEN
 			 !   FCij * ( dFRij/dNtSi + Bij*dFAij/dNtSi ).
			 ! This was a bug!! ------------->|---->|------->|----------->|
			 !              VFACDN = FCIJ*( DF2DNT*FASIH + DF1DNT*BAVEIJ*FRSIH )
              VFACDN = FCIJ*( DF1DNT*FRSIH + DF2DNT*BAVEIJ*FASIH )
              IF( KTYPI  ==  2 ) THEN
				 !     CALCULATE FORCES ON I AND NEIGHBOURS.
				 !     - FCij * ( dFRij/dRwi + Bij*dFAij/dRwi ).
                  pbuf(i3+1)= pbuf(i3+1) - VFACDN * NTIDXI
                  pbuf(i3+2)= pbuf(i3+2) - VFACDN * NTIDYI
                  pbuf(i3+3)= pbuf(i3+3) - VFACDN * NTIDZI

                  pbuf(j3+1)= pbuf(j3+1) - VFACDN * NTIDXJ
                  pbuf(j3+2)= pbuf(j3+2) - VFACDN * NTIDYJ
                  pbuf(j3+3)= pbuf(j3+3) - VFACDN * NTIDZJ

                  !if (ibuf(i3+3)==34 .or. ibuf(j3+3)==34) then
            	  !	print '(A,3I2,2F10.5,2I4)', "2023*** ", myproc, dirbuf(i), dirbuf(j), &
            	  !	 -VFACDN * NTIDXI, -VFACDN * NTIDXJ, ibuf(i3+3), ibuf(j3+3)
           		  !endif

                  DO IKC = 1,NUMNBI
                     K = NEBOFI(IKC)
                     k3=k*3-3
					 !     - FCij * ( dFRij/dRwk + Bij*dFAij/dRwk ).
                     pbuf(k3+1)= pbuf(k3+1) - VFACDN * NTIDXK(IKC)
                     pbuf(k3+2)= pbuf(k3+2) - VFACDN * NTIDYK(IKC)
                     pbuf(k3+3)= pbuf(k3+3) - VFACDN * NTIDZK(IKC)
                     !if (ibuf(k3+3)==34) then
            		 !	print '(A,4I2,2F10.5,3I4)', "2030*** ", myproc, dirbuf(i), dirbuf(j), dirbuf(k), &
            		 !	-VFACDN * NTIDXK(IKC), ibuf(i3+3), ibuf(j3+3), ibuf(k3+3)
            		 !endif
                  ENDDO
               ELSE
				 !     CALCULATE FORCES ON J AND NEIGHBOURS.
				 !     - FCij * ( dFRij/dRwj + Bij*dFAij/dRwj ).
                  pbuf(j3+1)= pbuf(j3+1) - VFACDN * NTJDXJ
                  pbuf(j3+2)= pbuf(j3+2) - VFACDN * NTJDYJ
                  pbuf(j3+3)= pbuf(j3+3) - VFACDN * NTJDZJ
                  
                  pbuf(i3+1)= pbuf(i3+1) - VFACDN * NTJDXI
                  pbuf(i3+2)= pbuf(i3+2) - VFACDN * NTJDYI
                  pbuf(i3+3)= pbuf(i3+3) - VFACDN * NTJDZI
                  !if (ibuf(i3+3)==34 .or. ibuf(j3+3)==34) then
            	  !		print '(A,3I2,2F10.5,2I4)', "2049*** ", myproc, dirbuf(i), dirbuf(j), &
            	  !	-VFACDN * NTJDXJ, -VFACDN * NTJDXI, ibuf(i3+3), ibuf(j3+3)
            	  !endif
                  DO JLC = 1,NUMNBJ
                     L = NEBOFJ(JLC)
                     l3=l*3-3
					 !     - FCij * ( dFRij/dRwl + Bij*dFAij/dRwl ).
                     pbuf(l3+1)= pbuf(l3+1) - VFACDN * NTJDXL(JLC)
                     pbuf(l3+2)= pbuf(l3+2) - VFACDN * NTJDYL(JLC)
                     pbuf(l3+3)= pbuf(l3+3) - VFACDN * NTJDZL(JLC)
                     !if (ibuf(l3+3)==34) then
            		 !	 print '(A,4I2,2F10.5,3I4)', "2052*** ", myproc, dirbuf(i), dirbuf(j), dirbuf(l), &
            		 !	-VFACDN * NTJDXL(JLC), ibuf(i3+3), ibuf(j3+3), ibuf(l3+3)
            		 !endif
                  ENDDO
               ENDIF
            ENDIF

         ENDIF
		 !   END OF I==J LOOP.
        ENDDO ! End of loop over J
 2000    CONTINUE

      ENDDO ! End of loop over I

    !print *,'Node received totally ',myproc,np0, myatoms
    !do j=1, np0
    !	j3=j*3-3
    !	if (ibuf(j3+3)==34)  print *,'BEFORE ',myproc, pbuf(j3+1), pbuf(j3+2)
	!enddo
    ! Passing back stage
    i=0
    !IF(debug)PRINT *,'SW pass back',myproc,np0,myatoms
    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       ! print *,'Stilweb loop 2, pbuf sendrecv',myproc
       ! Loop over directions
       do d=1,8
          ! Loop over neighbours
          i=0
          do j=myatoms+1,np0
             dfrom=dirbuf(j)+4
             if (dfrom .gt. 8) dfrom=dfrom-8
             if (d .eq. dfrom) then
                i=i+1
                if (i .ge. NPPASSMAX) call my_mpi_abort('NPPASSMAX overflow2', int(myproc))
                i3=i*3-3
                j3=j*3-3
                ifrom=ibuf(j*2-1)
                isendbuf(i)=ifrom
                psendbuf(i)=ebuf(j)
                xsendbuf(i3+1)=pbuf(j3+1)
                xsendbuf(i3+2)=pbuf(j3+2)
                xsendbuf(i3+3)=pbuf(j3+3)
             endif
          enddo
          ! print *,'send',myproc,d,i,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), &
                  d, mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,ii
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,i
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
          endif
          i=ii
          do ii=1,i
             myi=isendbuf2(ii)
             if (myi .gt. myatoms) then
                print *,myproc,myatoms,myi,ii,i
                call my_mpi_abort('BrennerBeardmore i too large', int(myproc))
             endif
             i3=myi*3-3
             ii3=ii*3-3
             ebuf(myi)=ebuf(myi)+psendbuf2(ii)
             pbuf(i3+1)=pbuf(i3+1)+xsendbuf2(ii3+1)
             pbuf(i3+2)=pbuf(i3+2)+xsendbuf2(ii3+2)
             pbuf(i3+3)=pbuf(i3+3)+xsendbuf2(ii3+3)
             !print *, "Send data ",  myproc, myi, xsendbuf2(ii3+1), psendbuf2(ii), ii
          enddo
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif

    !do j=1, np0
    !	j3=j*3-3
    !	if (ibuf(j3+3)==34)  print *,'After ',myproc, pbuf(j3+1), pbuf(j3+2)
	!enddo

     !do j = 1,3*myatoms     
     !  xnp(j) = pbuf(j)/box(MODULO(j+2, 3)+1)
     !enddo

     xnp(1:3*myatoms:3)=pbuf(1:3*myatoms:3)/box(1)
     xnp(2:3*myatoms:3)=pbuf(2:3*myatoms:3)/box(2)
     xnp(3:3*myatoms:3)=pbuf(3:3*myatoms:3)/box(3)

	
	!wxxi(1:myatoms) = (pbuf(1:3*myatoms:3)*x0(1:3*myatoms:3))!/box(1)!*box(1)
	!wyyi(1:myatoms) = (pbuf(2:3*myatoms:3)*x0(2:3*myatoms:3))!/box(2)!*box(2)
	!wzzi(1:myatoms) = (pbuf(3:3*myatoms:3)*x0(3:3*myatoms:3))!/box(3)!*box(3) 	
  	
  	!wxx=SUM(wxxi(1:myatoms))
  	!wyy=SUM(wyyi(1:myatoms))
  	!wzz=SUM(wzzi(1:myatoms))

  	!wxxsh(1:np0)=zero
	!wyysh(1:np0)=zero
	!wzzsh(1:np0)=zero

  	!print *, "PRESS", wxx, wyy, wzz, (wxx+wyy+wzz), box(1), box(2), box(3)
      
     !do j = 1, myatoms    
     !  Epair(j)=ebuf(j)
     !  Vpair=Vpair+ebuf(j)
       !i3=j*3-3
	   !print *, "forces on atom ",j,xnp(i3+1),xnp(i3+2),xnp(i3+3)   !Andrea
	   !print *, "Epair ",j, Epair(j)
     !enddo
     Epair(:myatoms)=ebuf(:myatoms)
     Vpair=SUM(ebuf(:myatoms))

!print *, "Epair ",i, Epair(1), Epair(2), Epair(3), Epair(4), Epair(5)
!do j = 1, myatoms  
!  i3=j*3-3 
!if(j==1) then
!  print *, "forces on atom ",j,xnp(i3+1),xnp(i3+2),xnp(i3+3)   !Andrea
!  print *, "Epair ",j, Epair(j)
!  print *, "pos ",j, x0(i3+1), x0(i3+2), x0(i3+3)
!end if
!end do

      RETURN
      END
!
!#######################################################################
      SUBROUTINE HCHITP(NHI,NCI,HCH,DHCHDH,DHCHDC)
      use defs
      use Brenner_vars
	use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   BICUBIC INTERPOLATION OF Hch.
!   ASSUMES 0.0 <= Nhi,Nci < 4.0
!   COPYRIGHT: KEITH BEARDMORE 30/11/93.
!
!
      REAL*8 X1,X2,HCH,DHCHDH,DHCHDC,SHCH,SHCHDC,COEFIJ
      integer I,J,NHBOX,NCBOX,IBOX

      REAL*8 NHI,NCI
!
!
!      COMMON/HCHP/HCHCF
!
      NHBOX = INT( NHI )
      NCBOX = INT( NCI )
!
!   FIND WHICH BOX WE'RE IN AND CONVERT TO NORMALISED COORDINATES.
      IBOX=1+4*NHBOX+NCBOX
      X1= NHI - NHBOX
      X2= NCI - NCBOX
!
      HCH    = 0.0
      DHCHDH = 0.0
      DHCHDC = 0.0
      DO I=4,1,-1
         SHCH   = 0.0
         SHCHDC = 0.0
         DO J=4,1,-1
            COEFIJ=HCHCF(IBOX,I,J)
                      SHCH   =   SHCH*X2+       COEFIJ
            IF(J > 1)SHCHDC = SHCHDC*X2+ (J-1)*COEFIJ
         ENDDO
                   HCH    = HCH    *X1+       SHCH
         IF(I > 1)DHCHDH = DHCHDH *X1+ (I-1)*SHCH
                   DHCHDC = DHCHDC *X1+       SHCHDC
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE HCCITP(NHI,NCI,HCC,DHCCDH,DHCCDC)
use my_mpi
      use Brenner_vars
      IMPLICIT NONE
!#######################################################################
!
!   BICUBIC INTERPOLATION OF Hcc.
!   ASSUMES 0.0 <= Nhi,Nci < 4.0
!   COPYRIGHT: KEITH BEARDMORE 02/12/93.
!
!
      REAL*8 NHI,NCI,HCC,DHCCDH,DHCCDC,X1,X2,SHCC,SHCCDC,COEFIJ
      integer NHBOX,NCBOX,IBOX,I,J
!
!
!      COMMON/HCCP/HCCCF
!
      NHBOX = INT( NHI )
      NCBOX = INT( NCI )
!
!   FIND WHICH BOX WE'RE IN AND CONVERT TO NORMALISED COORDINATES.
      IBOX=1+4*NHBOX+NCBOX
      X1= NHI - NHBOX
      X2= NCI - NCBOX
!
      HCC    = 0.0
      DHCCDH = 0.0
      DHCCDC = 0.0
      DO I=4,1,-1
         SHCC   = 0.0
         SHCCDC = 0.0
         DO J=4,1,-1
            COEFIJ=HCCCF(IBOX,I,J)
                      SHCC   =   SHCC*X2+       COEFIJ
            IF(J > 1)SHCCDC = SHCCDC*X2+ (J-1)*COEFIJ
         ENDDO
                   HCC    = HCC    *X1+       SHCC
         IF(I > 1)DHCCDH = DHCCDH *X1+ (I-1)*SHCC
                   DHCCDC = DHCCDC *X1+       SHCCDC
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE FCCITP(NTI,NTJ,NCONJ,FCC,DFCCDI,DFCCDJ,DFCCDC)
use my_mpi
      use Brenner_vars
      IMPLICIT NONE
!#######################################################################
!
!   TRICUBIC INTERPOLATION OF Fcc.
!   ASSUMES 0.0 <= NTI,NTJ < 4.0 AND 1 <= NCONJ <=2
!   COPYRIGHT: KEITH BEARDMORE 02/12/93.
!

      REAL*8 FCC,DFCCDI,DFCCDJ,DFCCDC,X1,X2,X3
      REAL*8 SFCC,SFCCDJ,SFCCDC,TFCC,TFCCDC,COEFIJ
      integer NIBOX,NJBOX,IBOX,I,J,K

      REAL*8 NTI,NTJ,NCONJ
!

!
!      COMMON/FCCP/FCCCF
!
      NIBOX = INT( NTI )
      NJBOX = INT( NTJ )
!
!   FIND WHICH BOX WE'RE IN AND CONVERT TO NORMALISED COORDINATES.
      IBOX=1+4*NIBOX+NJBOX
      X1= NTI   - NIBOX
      X2= NTJ   - NJBOX
      X3= NCONJ - 1
!
      FCC    = 0.0
      DFCCDI = 0.0
      DFCCDJ = 0.0
      DFCCDC = 0.0
      DO I=4,1,-1
         SFCC   = 0.0
         SFCCDJ = 0.0
         SFCCDC = 0.0
         DO J=4,1,-1
            TFCC   = 0.0
            TFCCDC = 0.0
            DO K=4,1,-1
               COEFIJ=FCCCF(IBOX,I,J,K)
                         TFCC   =   TFCC*X3+       COEFIJ
               IF(K > 1)TFCCDC = TFCCDC*X3+ (K-1)*COEFIJ
            ENDDO
                      SFCC   = SFCC   *X2+       TFCC
            IF(J > 1)SFCCDJ = SFCCDJ *X2+ (J-1)*TFCC
                      SFCCDC = SFCCDC *X2+       TFCCDC
         ENDDO
                   FCC    = FCC    *X1+       SFCC
         IF(I > 1)DFCCDI = DFCCDI *X1+ (I-1)*SFCC
                   DFCCDJ = DFCCDJ *X1+       SFCCDJ
                   DFCCDC = DFCCDC *X1+       SFCCDC
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE FSIITP(NTSI_spl,F1,F2,DF1DNT,DF2DNT)
use my_mpi
      use Brenner_vars
      IMPLICIT NONE
!#######################################################################
!
!   CUBIC SPLINE INTERPOLATION OF F1(NtSi) & F2(NtSi).
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!
!
      REAL*8 F1,F2,DF1DNT,DF2DNT,XI, NTSI_spl
      integer INTERV,I

!
!      COMMON/FSIP/AF1,AF2

!     FIND INTERVAL AND CONVERT TO NORMALISED COORDINATES.
      IF( NTSI_spl  >=  4.0 )THEN
         INTERV = 3
         XI = 1.0
      ELSE IF( NTSI_spl  >  1.0 )THEN
         INTERV = INT( NTSI_spl )
         XI= NTSI_spl - INTERV
      ELSE
         INTERV = 1
         XI = 0.0
      ENDIF
!
      F1     = AF1(INTERV,4)
      F2     = AF2(INTERV,4)
      DF1DNT = 0.0
      DF2DNT = 0.0
      DO I=3,1,-1
          DF1DNT = DF1DNT *XI+  F1
          DF2DNT = DF2DNT *XI+  F2
          F1     = F1     *XI+  AF1(INTERV,I)
          F2     = F2     *XI+  AF2(INTERV,I)
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE HSIITP(NTSI_spl,H,DHDNT)
use my_mpi
         use Brenner_vars
      IMPLICIT NONE
!#######################################################################
!
!   CUBIC SPLINE INTERPOLATION OF H(NtSi).
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!

      REAL*8 H,DHDNT,XI
      integer INTERV,I

      REAL*8 NTSI_spl
!
!      DIMENSION AH(3,4)
!
!      COMMON/HSIP/AH
!
!   FIND INTERVAL AND CONVERT TO NORMALISED COORDINATES.
      IF( NTSI_spl  >=  4.0 )THEN
         INTERV = 3
         XI = 1.0
      ELSE IF( NTSI_spl  >  1.0 )THEN
         INTERV = INT( NTSI_spl )
         XI= NTSI_spl - INTERV
      ELSE
         INTERV = 1
         XI = 0.0
      ENDIF
!
      H     = AH(INTERV,4)
      DHDNT = 0.0
      DO I=3,1,-1
          DHDNT = DHDNT *XI+  H
          H     = H     *XI+  AH(INTERV,I)
      ENDDO
!
      RETURN
      END










































!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################
!#######################################################################






!#######################################################################
      SUBROUTINE CSHPRM(R1CCin,R2CCin,potmode)
use my_mpi
      use Brenner_vars
      IMPLICIT NONE
!#######################################################################
!
!   CALCULATES THE VALUES OF THE CONSTANTS USED IN C/Si/H POTENTIAL.
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!   A COMBINATION OF:
!   DONALD. BRENNERS'S HYDROCARBON POTENTIAL (1st PARAMETERISATION).
!   PARAMETERS FROM : PHYS. REV. B 42, 9458-9471(1990).
!   PLUS CORRECTIONS : PHYS. REV. B 46, 1948(1990).
!   J. TERSOFF'S MULTICOMPONENT POTENTIAL FOR SI - C SYSTEMS.
!   PARAMETERS FROM : PHYS. REV. B 39, 5566-5568(1989).
!        AND ERRATA : PHYS. REV. B 41, 3248     (1990).
!   R. MURTY & H. ATWATER'S Si-H POTENTIAL.
!   PARAMETERS FROM : PAPER PRESENTED AT COSIRES '94.
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
!
      integer i,j,potmode
      real*8 R1CCin,R2CCin


      !common /covalradcommon/ COVALRAD 

      ! Additions by Kai N: Rasmol covalent radii. Same as in brenner.h
      ! Covalent radii atom types: 1 C 2 H 3 Si
      do i=1,4
          do j=1,4
             COVALRAD(i,j) = 0.0d0
          enddo
       enddo
      COVALRAD(1,1)=2.0d0
      COVALRAD(2,2)=1.2d0
      COVALRAD(1,2)=1.6d0
      COVALRAD(1,3)=2.48d0
      COVALRAD(2,3)=2.08d0
      COVALRAD(3,3)=2.96d0
      do i=1,4
          do j=i,4
            write (6,'(A,2I3,F10.4)') 'Using covalent radii: typei typej r',i,j,COVALRAD(i,j)
            COVALRAD(i,j)=COVALRAD(i,j)**2
            COVALRAD(j,i)=COVALRAD(i,j)
         enddo
      enddo


!   MAKE BRENNER PARAMETERS.
      CALL MKBCH1(R1CCin,R2CCin,potmode)
!
!   MAKE TERSOFF PARAMETERS.
      CALL MKTCSI
!
!   MAKE MURTY/ATWATER PARAMETERS.
      CALL MKMSIH
!
!   SET UP ZBL-UNIVERSAL FOR NOBLE (e.g. Ar) INTERACTIONS.
      CALL ZBLPRM
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKBCH1(R1CCin,R2CCin,potmode)
      use defs
      use Brenner_vars
      use para_common, only : PI
use my_mpi
!#######################################################################
!
!   CALCULATES THE VALUES OF CONSTANTS USED IN C/Si/H POTENTIAL.
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!   BRENNER #1 FOR C-C, C-H AND H-H BONDS.
!   DONALD. BRENNERS'S HYDROCARBON POTENTIAL (1st PARAMETERISATION) *** see below ***.
!   PARAMETERS FROM : PHYS. REV. B 42, 9458-9471(1990).
!   PLUS CORRECTIONS : PHYS. REV. B 46, 1948(1990).
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
      IMPLICIT NONE

      integer potmode
      real*8 R1CCin,R2CCin
!
!    **** 20.4.99 Modified to Brenner II **** 
!
      REAL*8 RECC(BCOMBS),REHH,RECH(BTYPES),DECC(BCOMBS),DEHH,DECH(BTYPES)
      REAL*8 BETACC(BCOMBS),BETAHH,BETACH(BTYPES),SCC(BCOMBS),SHH,SCH(BTYPES)
      REAL*8 DLTAC,DLTAH,ALPACC(BCOMBS),ALPAHH,ALPACH(BTYPES)
      REAL*8 R1CC(BCOMBS),R1CH(BTYPES),R1HH,R2CC(BCOMBS),R2CH(BTYPES),R2HH
      REAL*8 A0(BCOMBS),C02(BCOMBS),D02(BCOMBS),GHH

!
!      REAL*8 CONRH2(BCOMBS,11),CONRL(BCOMBS,10),CONFCA(BCOMBS,10),CONFC(BCOMBS,10),fermib(BCOMBS,10),fermir(BCOMBS,10)
!      REAL*8 CONFA(BCOMBS,6),CONEA(BCOMBS,6),CONFR(BCOMBS,6),CONER(BCOMBS,6),CONRE(BCOMBS,6)
!      REAL*8 CONCD(BCOMBS,6),CONC(BCOMBS,6),COND(BCOMBS,6),CONH(BCOMBS,6),CONALP(6),CONBET(6)
!      REAL*8 CONN(6),CONPE(6),CONAN(6),CONNM(6),CONPF(6),CONCSI
!
!      COMMON/TBC1/ CONRH2,CONRL,CONFCA,CONFC,fermib,fermir
!      COMMON/CSH2/ CONFA,CONEA,CONFR,CONER,CONRE
!      COMMON/CSH3/ CONCD,CONC,COND,CONH,CONALP,CONBET
!      COMMON/CSH4/ CONN,CONPE,CONAN,CONNM,CONPF,CONCSI
!
!      PI = ACOS ( -1.0 )
!
!   VALUES FOR THE PARAMETERS FROM TABLE II.
      DATA RECC(1)   , REHH   , RECH(1)   / 1.39       , 0.74144 , 1.1199  /
      DATA DECC(1)   , DEHH   , DECH(1)   / 6.0        , 4.7509  , 3.6422  /
      DATA BETACC(1) , BETAHH , BETACH(1) / 2.1        , 1.9436  , 1.9583  /
      DATA SCC(1)    , SHH    , SCH(1)    / 1.22       , 2.3432  , 1.69077 /
      DATA DLTAC  , DLTAH           / 0.5        , 0.5               /
      DATA ALPACC(1) , ALPAHH , ALPACH(1) / 0.0        , 4.0     , 4.0     /
      DATA          GHH             /              12.33             /
      DATA R1CC(1)   , R1HH   , R1CH(1)   / 1.70       , 1.1     , 1.3     /
      DATA R2CC(1)   , R2HH   , R2CH(1)   / 2.00       , 1.7     , 1.8     /
      DATA A0(1)                       / 0.00020813                     /
      DATA C02(1)                      / 108900.0                       /
      DATA D02(1)                      / 12.25                          /

!*************************************************************************************   
      !if (potmode > 19) then

      ! Values for Caros Be-C-H system (potmode 83 in parcas)   Andrea

      ! IJBPOT = 3: Be-Be II
      ! IJBPOT = 4: Be-C

         RECC(3) = 2.078799227759839d0
         DECC(3) = 1.035711767714829d0
         BETACC(3) = 1.300000000000000d0
         SCC(3) = 1.889782892624960d0
         ALPACC(3) = 0.0d0
	 ! trcut(i,j)         = 2.535000000000000d0
	 ! dcut(i,j)          = 0.150000000000000d0
         R1CC(3) = 2.385000000000000d0  ! = TRCUT - DCUT in Tersoff_compound
         R2CC(3) = 2.685000000000000d0  ! = TRCUT + DCUT in Tersoff_compound
         A0(3) = 0.000000819587260d0
         C02(3) = 7990.4620160892
         D02(3) = 0.0753112522519187
         CONH(3,1) = -0.760693434073536d0   ! N.B. Opposite sign from Tersoff_compound

         RECC(4) = 1.724298966530114d0
         DECC(4) = 3.909329821145575d0
         BETACC(4) = 1.586760903647416d0
         SCC(4) = 2.766724347332735d0
         ALPACC(4) = 0.0d0         
         R1CC(4) = 2.400000000000000d0  ! = TRCUT - DCUT in Tersoff_compound
         R2CC(4) = 2.800000000000000d0  ! = TRCUT + DCUT in Tersoff_compound
         A0(4) = 0.000030018449777d0
         C02(4) = 3249.466702990
         D02(4) = 0.128381621074000d0
         CONH(4,1) = -0.559996014890212d0     ! N.B. Opposite sign from Tersoff_compound 

      ! IJBPOT = 2: Be-H
         RECH(2) = 1.338000000000000d0
         DECH(2) = 2.60000000000000d0
         BETACH(2) = 2.20000000000000d0
         SCH(2) = 2.50000000d0
         ALPACH(2) = 0.0d0
	 ! trcut(i,j) = 1.80000000000000d0      
	 ! dcut(i,j) =  0.15000000000000d0       
         R1CH(2) = 1.65000000000000d0   ! = TRCUT - DCUT in Tersoff_compound
         R2CH(2) = 1.95000000000000d0   ! = TRCUT + DCUT in Tersoff_compound
      !   A0(-) = 0.19000000000d0
      !   C02(-) = 0.00003249
      !   D02(-) = 0.000016
         CONCD(2,3) = 0.575818750000000d0 ! A0(:) * ( 1.0 + C02(:) / D02(:) )
         CONC2(2,3) = 0.000006173100000d0 ! A0(:) * C02(:)
         COND2(2,3) = 0.000016000000000d0 ! D02(:)
         CONH(2,3) = -1

      !endif
!**************************************************************************

      ! hcparcas V3.15: Overwrite with readin parameters
      R1CC(1)=R1CCin
      R2CC(1)=R2CCin
      
      ! Report possibly modified parameters
      print *,'BrennerBeardmore using R1CC',R1CC
      print *,'BrennerBeardmore using R2CC',R2CC

!   MORSE TERM CONSTANTS (FOR C-C, C-H AND H-H BONDS).
      CONER(:,1) = -SQRT(2.0*SCC(:)) * BETACC(:)
      CONER(1:BTYPES,3) = -SQRT(2.0*SCH(:)) * BETACH(:)
      CONER(1,6) = -SQRT(2.0*SHH) * BETAHH
      CONEA(:,1) = -SQRT(2.0/SCC(:)) * BETACC(:)
      CONEA(1:BTYPES,3) = -SQRT(2.0/SCH(:)) * BETACH(:)
      CONEA(1,6) = -SQRT(2.0/SHH) * BETAHH
      CONFR(:,1) =  ( DECC(:)     / (SCC(:)-1.0) ) * EXP( -CONER(:,1) * RECC(:) )
      CONFR(1:BTYPES,3) = (DECH(:) / (SCH(:)-1.0) ) * EXP( -CONER(1:BTYPES,3) * RECH(:) )
      CONFR(1,6) =  ( DEHH     / (SHH-1.0) ) * EXP( -CONER(1,6) * REHH )
      CONFA(:,1) = -( DECC(:)*SCC(:) / (SCC(:)-1.0) ) * EXP( -CONEA(:,1) * RECC(:) )
      CONFA(1:BTYPES,3) = -(DECH(:)*SCH(:) / (SCH(:)-1.0))*EXP(-CONEA(1:BTYPES,3)*RECH(:) )
      CONFA(1,6) = -( DEHH*SHH / (SHH-1.0) ) * EXP( -CONEA(1,6) * REHH )
      CONRE(:,1) = RECC(:)
      CONRE(1:BTYPES,3) = RECH(:)
      CONRE(1,6) = REHH
!
!   CUTOFF CONSTANTS (FOR C-C, C-H AND H-H BONDS).
      CONRL(:,1)  = R1CC(:)
      CONRL(1:BTYPES,3)  = R1CH(:)
      CONRL(1,6)  = R1HH
      CONRH2(:,1) = R2CC(:)** 2
      CONRH2(1:BTYPES,3) = R2CH(:) ** 2
      CONRH2(1,6) = R2HH ** 2
      CONFCA(:,1) = PI / ( R2CC(:) - R1CC(:) )
      CONFCA(1:BTYPES,3) = PI / ( R2CH(:) - R1CH(:) )
      CONFCA(1,6) = PI / ( R2HH - R1HH )
      CONFC(:,1)  = - 0.5 * CONFCA(:,1)
      CONFC(1:BTYPES,3)  = - 0.5 * CONFCA(1:BTYPES,3)
      CONFC(1,6)  = - 0.5 * CONFCA(1,6)

! Fermi join constants (taken from parcas WCH potential). Used if reppotspl>0
      !C-C
      fermib(1,1) = 8.0d0
      fermir(1,1) = 0.6d0
      !Be-Be
      fermib(3,1) = 15.0d0
      fermir(3,1) = 0.8d0
      !Be-C
      fermib(4,1) = 16.0d0
      fermir(4,1) = 0.7d0
      !C-H
      fermib(1,3) = 10.0d0
      !fermir(3,3) = 0.5d0 what is this??  Andrea
      fermir(1,3) = 0.5d0
      !H-Be
      fermib(2,3) = 15.0d0
      !fermir(3,1) = 0.8d0  what is this??  Andrea
      fermir(2,3) = 0.8d0
      !H-H
      fermib(1,6) = 15.0d0
      !fermir(3,6) = 0.35d0  what is this??  Andrea
      fermir(1,6) = 0.35d0

!
!   BOND ORDER CONSTANTS (FOR C CENTRES ON C-[C or H] BONDS
!                     AND FOR H CENTRES ON H-[C or H] BONDS).
      CONN(1)  = 1.0
      CONN(3)  = 1.0
      CONPE(1) = - DLTAC
      CONPE(3) = - DLTAH
      CONAN(1) = 0.5 * CONPE(1)
      CONAN(3) = 0.5 * CONPE(3)
      CONNM(1) = 0.0
      CONNM(3) = 0.0
      CONPF(1) = CONPE(1) - 1.0
      CONPF(3) = CONPE(3) - 1.0
!
!   BOND ANGLE CONSTANTS (FOR C CENTRES ON C-[C or H] BONDS
!                     AND FOR H CENTRES ON H-[C or H] BONDS).
      CONCD(:,1) = A0(:) * ( 1.0 + C02(:) / D02(:) )
      CONCD(2,1) = CONCD(2,3) ! for Be-H 
      CONCD(1,3) = GHH
      COND2(:,1)  = D02(:)
      COND2(2,1) = COND2(2,3)! for Be-H 
      COND2(1,3)  = 1.0
      CONC2(:,1)  = A0(:) * C02(:)
      CONC2(2,1) = CONC2(2,3) ! for Be-H
      CONC2(1,3)  = 0.0d0
      CONH(1,1)  = -1.0
      CONH(2,1)  = -1.0
      CONH(1,3)  = -1.0
!
!   BOND LENGTH CONSTANTS (FOR C CENTRES ON C-[C or H] BONDS
!                      AND FOR H CENTRES ON H-[C or H] BONDS).
      CONALP(1)   = ALPACH(1)
      CONALP(3)   = ALPAHH
      CONBET(1)   = 1.0
      CONBET(3)   = 1.0
!
!   GENERATE THE COEFFICIENTS FOR
!   THE BI- AND TRI- CUBIC INTERPOLATION FUNCTIONS.
      CALL MKHCH
      CALL MKHCC
      CALL MKFCC
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKTCSI
!#######################################################################
!
!   CALCULATES THE VALUES OF CONSTANTS USED IN C/Si/H POTENTIAL.
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!   TERSOFF Si/C FOR C-Si BONDS.
!   J. TERSOFF'S MULTICOMPONENT POTENTIAL FOR SI - C SYSTEMS.
!   PARAMETERS FROM : PHYS. REV. B 39, 5566-5568(1989).
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
      use defs
      use para_common, only : PI
      use Brenner_vars
use my_mpi
      IMPLICIT NONE
!
      REAL*8 ACC,ACSI,ASISI,BCC,BCSI,BSISI
      REAL*8 LMCC,LMCSI,LMSISI,MUCC,MUCSI,MUSISI
      REAL*8 BETAC,BETASI,NC,NSI,CC,CSI,DC,DSI,HC,HSI
      REAL*8 RCC,RCSI,RSISI,SCC,SCSI,SSISI
      REAL*8 XCSI,RECSI
!      REAL*8 PI
!
!      REAL*8 CONRH2(BCOMBS,11),CONRL(BCOMBS,10),CONFCA(BCOMBS,10),CONFC(BCOMBS,10),fermib(BCOMBS,10),fermir(BCOMBS,10)
!      REAL*8 CONFA(BCOMBS,6),CONEA(BCOMBS,6),CONFR(BCOMBS,6),CONER(BCOMBS,6),CONRE(BCOMBS,6)
!      REAL*8 CONCD(BCOMBS,6),CONC(BCOMBS,6),COND(BCOMBS,6),CONH(BCOMBS,6),CONALP(6),CONBET(6)
!      REAL*8 CONN(6),CONPE(6),CONAN(6),CONNM(6),CONPF(6),CONCSI
!
!      COMMON/TBC1/ CONRH2,CONRL,CONFCA,CONFC,fermib,fermir
!      COMMON/CSH2/ CONFA,CONEA,CONFR,CONER,CONRE
!      COMMON/CSH3/ CONCD,CONC,COND,CONH,CONALP,CONBET
!      COMMON/CSH4/ CONN,CONPE,CONAN,CONNM,CONPF,CONCSI
!
!     PI = ACOS ( -1.0 )
!
!   VALUES FOR THE PARAMETERS FROM TABLE 1.
      DATA ACC   , ASISI  /  1.3936E+3 ,  1.8308E+3 /
      DATA BCC   , BSISI  /  3.4674E+2 ,  4.7118E+2 /
      DATA LMCC  , LMSISI /  3.4879    ,  2.4799    /
      DATA MUCC  , MUSISI /  2.2119    ,  1.7322    /
      DATA BETAC , BETASI /  1.5724E-7 ,  1.1000E-6 /
      DATA NC    , NSI    /  7.2751E-1 ,  7.8734E-1 /
      DATA CC    , CSI    /  3.8049E+4 ,  1.0039E+5 /
      DATA DC    , DSI    /  4.3484E+0 ,  1.6217E+1 /
      DATA HC    , HSI    / -5.7058E-1 , -5.9825E-1 /
      DATA RCC   , RSISI  /  1.8       ,  2.7       /
      DATA SCC   , SSISI  /  2.1       ,  3.0       /
      DATA XCSI           /  0.9776                 /
      DATA RECSI          /  1.85                   /
!
!   INTERPOLATION FOR MIXED BOND CONSTANTS.
       ACSI = (  ACC *  ASISI ) ** 0.5
       BCSI = (  BCC *  BSISI ) ** 0.5
      LMCSI = ( LMCC + LMSISI ) *  0.5
      MUCSI = ( MUCC + MUSISI ) *  0.5
       RCSI = (  RCC *  RSISI ) ** 0.5
       SCSI = (  SCC *  SSISI ) ** 0.5
!
!   MORSE TERM CONSTANTS (FOR C-Si BONDS).
      CONFR(1,2) =  ACSI
      CONFA(1,2) = -BCSI
      CONER(1,2) = -LMCSI
      CONEA(1,2) = -MUCSI
      CONRE(1,2) =  RECSI
!
!   CUTOFF CONSTANTS (FOR C-Si BONDS).
      CONRL(1,2)  = RCSI
      CONRH2(1,2) = SCSI  ** 2
      CONFCA(1,2) = PI / ( SCSI  - RCSI  )
      CONFC(1,2)  = - 0.5 * CONFCA(1,2)

! Fermi join constants (taken from parcas Si and C potentials). Used if reppotspl>0
      fermib(1,2) = sqrt(12.0+8.0)
      fermir(1,2) = (1.6d0+0.6d0)/2.0d0

!
!   BOND ORDER CONSTANTS (FOR C CENTRES ON C-Si BONDS).
      CONN(4)  = NC
      CONPE(4) = -1.0 / ( 2.0 * CONN(4) )
      CONAN(4) = 0.5 * CONN(4) * CONPE(4)
      CONNM(4) = CONN(4)  - 1.0
      CONPF(4) = CONPE(4) - 1.0
!
!   BOND ANGLE CONSTANTS (FOR C CENTRES ON C-Si BONDS).
      CONCD(1,4) = BETAC * ( 1.0 + ( (CC  ** 2) / (DC  ** 2) ) )
      COND2(1,4)  = DC  ** 2
      CONC2(1,4)  = BETAC * (CC  ** 2)
      CONH(1,4)  = HC
!
!   BOND LENGTH CONSTANTS (FOR C CENTRES ON C-Si BONDS).
      CONALP(4)   = 0.0
      CONBET(4)   = 1.0
!
!   MIXED BOND CONSTANT.
      CONCSI    = XCSI
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKMSIH
!#######################################################################
!
!   CALCULATES THE VALUES OF CONSTANTS USED IN C/Si/H POTENTIAL.
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!   MURTY & ATWATER Si/H FOR Si-Si AND SI-H BONDS.
!   R. MURTY & H. ATWATER'S Si-H POTENTIAL.
!   PARAMETERS FROM : PAPER PRESENTED AT COSIRES '94.
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
      use defs
      use para_common, only : PI
      use Brenner_vars
use my_mpi
      IMPLICIT NONE
!
      REAL*8 ASISI,ASIH,BSISI,BSIH
      REAL*8 LMSISI,LMSIH,MUSISI,MUSIH
      REAL*8 ALPASI,APLAH,APLAHB,BETASI,BETAH,BETAHB
      REAL*8 RESISI,RESIH,CSI,CH,CHB,DSI,DH,DHB,HSI,HH,HHB
      REAL*8 RSISI,RSIH,DSISI,DSIH
      REAL*8 NSI,NH,DELTSI,DELTH
!      REAL*8 PI
!
!      REAL*8 CONRH2(BCOMBS,11),CONRL(BCOMBS,10),CONFCA(BCOMBS,10),CONFC(BCOMBS,10),fermib(BCOMBS,10),fermir(BCOMBS,10)
!      REAL*8 CONFA(BCOMBS,6),CONEA(BCOMBS,6),CONFR(BCOMBS,6),CONER(BCOMBS,6),CONRE(BCOMBS,6)
!      REAL*8 CONCD(BCOMBS,6),CONC(BCOMBS,6),COND(BCOMBS,6),CONH(BCOMBS,6),CONALP(6),CONBET(6)
!      REAL*8 CONN(6),CONPE(6),CONAN(6),CONNM(6),CONPF(6),CONCSI
!
!      COMMON/TBC1/ CONRH2,CONRL,CONFCA,CONFC,fermib,fermir
!      COMMON/CSH2/ CONFA,CONEA,CONFR,CONER,CONRE
!      COMMON/CSH3/ CONCD,CONC,COND,CONH,CONALP,CONBET
!      COMMON/CSH4/ CONN,CONPE,CONAN,CONNM,CONPF,CONCSI
!
!      PI = ACOS ( -1.0 )
!
!   VALUES FOR THE PARAMETERS FROM TABLE 1.
      DATA ASISI  , ASIH           /  1830.8  ,  323.54         /
      DATA BSISI  , BSIH           /  471.18  ,  84.18          /
      DATA LMSISI , LMSIH          /  2.4799  ,  2.9595         /
      DATA MUSISI , MUSIH          /  1.7322  ,  1.6158         /
      DATA ALPASI , APLAH , APLAHB /  5.1975  ,  4.0000 ,  0.00 /
      DATA BETASI , BETAH , BETAHB /  3       ,  3      ,  1    /
      DATA RESISI , RESIH          /  2.35    ,  1.475          /
      DATA CSI    , CH    , CHB    /  0.0     ,  0.0216 ,  0.70 /
      DATA DSI    , DH    , DHB    /  0.160   ,  0.27   ,  1.00 /
      DATA HSI    , HH    , HHB    / -0.59826 ,  0.0    , -1.00 /
      DATA RSISI  , RSIH           /  2.85    ,  1.85           /
      DATA DSISI  , DSIH           /  0.15    ,  0.15           /
      DATA NSI    , NH             /  0.78734 ,  1.00           /
      DATA DELTSI , DELTH          /  0.635   ,  0.80469        /
!
!   MORSE TERM CONSTANTS (FOR Si-Si and Si-H BONDS).
      CONFR(1,4) =  ASISI
      CONFR(1,5) =  ASIH
      CONFA(1,4) = -BSISI
      CONFA(1,5) = -BSIH
      CONER(1,4) = -LMSISI
      CONER(1,5) = -LMSIH
      CONEA(1,4) = -MUSISI
      CONEA(1,5) = -MUSIH
      CONRE(1,4) =  RESISI
      CONRE(1,5) =  RESIH
!
!   CUTOFF CONSTANTS (FOR Si-Si and Si-H BONDS).
      CONRL(1,4)  = RSISI - DSISI
      CONRL(1,5)  = RSIH  - DSIH
      CONRH2(1,4) = ( RSISI + DSISI )  ** 2
      CONRH2(1,5) = ( RSIH  + DSIH  )  ** 2
      CONFCA(1,4) = PI / ( 2.0 * DSISI )
      CONFCA(1,5) = PI / ( 2.0 * DSIH  )
      CONFC(1,4)  = - 0.5 * CONFCA(1,4)
      CONFC(1,5)  = - 0.5 * CONFCA(1,5)

! Fermi join constants (Si taken from parcas, SiH determined for this). Used if reppotspl>0
      fermib(1,4) = 12.0d0
      fermir(1,4) = 1.6d0
      fermib(1,5) = 10.0d0
      fermir(1,5) = 0.6d0


!
!   BOND ORDER CONSTANTS (FOR Si CENTRES ON Si-C BONDS
!                         FOR Si CENTRES ON Si-H BONDS
!                     AND FOR BOND CENTRED H).
      CONN(2)  = NSI
      CONN(5)  = NH
      CONN(6)  = NH
      CONPE(2) = -DELTSI
      CONPE(5) = -DELTH
      CONPE(6) = -DELTH
      CONAN(2) = 0.5 * CONN(2) * CONPE(2)
      CONAN(5) = 0.5 * CONN(5) * CONPE(5)
      CONAN(6) = 0.5 * CONN(6) * CONPE(6)
      CONNM(2) = CONN(2)  - 1.0
      CONNM(5) = CONN(5)  - 1.0
      CONNM(6) = CONN(6)  - 1.0
      CONPF(2) = CONPE(2) - 1.0
      CONPF(5) = CONPE(5) - 1.0
      CONPF(6) = CONPE(6) - 1.0
!
!   BOND ANGLE CONSTANTS (FOR Si CENTRES ON Si-C BONDS
!                         FOR Si CENTRES ON Si-H BONDS
!                     AND FOR BOND CENTRED H).
      CONCD(1,2) = 0.0
      CONCD(1,5) = 0.0
      CONCD(1,6) = 0.0
      CONC2(1,2)  = CSI
      CONC2(1,5)  = CH
      CONC2(1,6)  = CHB
      COND2(1,2)  = DSI
      COND2(1,5)  = DH
      COND2(1,6)  = DHB
      CONH(1,2)  = HSI
      CONH(1,5)  = HH
      CONH(1,6)  = HHB
!
!   BOND LENGTH CONSTANTS (FOR Si CENTRES ON Si-C BONDS
!                          FOR Si CENTRES ON Si-H BONDS
!                      AND FOR BOND CENTRED H).
      CONALP(2)   = ALPASI
      CONALP(5)   = APLAH
      CONALP(6)   = APLAHB
      CONBET(2)   = BETASI
      CONBET(5)   = BETAH
      CONBET(6)   = BETAHB
!
!   GENERATE THE COEFFICIENTS FOR
!   THE CUBIC SPLINE INTERPOLATION FUNCTIONS.
      CALL MKF1
      CALL MKF2
      CALL MKH
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKHCH
         use Brenner_vars
use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   GENERATES THE COEFFICIENTS FOR BICUBIC INTERPOLATION OF Hch(Nih,Nic)
!   COPYRIGHT: KEITH BEARDMORE 30/11/93.
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
!
      REAL*8 A,B,HCH,DHCHDH,DHCHDC,D2HCHD
      real*8 powerprod2
      integer IX1,IX2,NP,MP,ICORN,IROW,NX1,NX2,NPOW1,NPOW2
      integer NPOW1M,NPOW2M,ICOL,NHBOX,NCBOX
      integer N,M,IBOX,I,J

      PARAMETER (NP=16)
      PARAMETER (MP=16)
      DIMENSION A(NP,NP),B(NP,MP)
!
      DIMENSION    HCH(0:4,0:4) , DHCHDH(0:4,0:4) , DHCHDC(0:4,0:4) , D2HCHD(0:4,0:4)
!
      DIMENSION IX1(4),IX2(4)

!
!      COMMON/HCHP/HCHCF
!
!   VALUES GIVEN IN TABLE III.
      DATA HCH   /  0.00000 , -0.09840 , -0.28780 , -0.45070 ,  0.00000    &
                 , -0.24790 , -0.33440 , -0.44380 ,  0.00000 ,  0.00000    &
                 , -0.32210 , -0.44490 ,  0.00000 ,  0.00000 ,  0.00000    &
                 , -0.44600 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000    &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
      DATA DHCHDH/  0.00000 ,  0.00000 , -0.17615 ,  0.00000 ,  0.00000    &
                 ,  0.00000 , -0.09795 ,  0.00000 ,  0.00000 ,  0.00000    &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000    &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000    &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
      DATA DHCHDC/  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000    &
                 ,  0.00000 , -0.17325 ,  0.00000 ,  0.00000 ,  0.00000    &
                 , -0.09905 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000    &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000    &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
      DATA D2HCHD/  25*0.0  /
!
!      write(*,'(Changed Hch(0,1)                from -0.17920 to ,
!     &F8.5)')HCH(0,1)
!      write(*,'(and dHch(1,1)/dh                from -0.07640 to ,
!     &F8.5)')DHCHDH(1,1)
!      write(*,'(and dHch(0,2)/dc                from -0.07655 to ,
!     &F8.5)')DHCHDC(0,2)
!      write(*,'(Changed Hch(1,1)                from -0.24770 to ,
!     &F8.5)')HCH(1,1)
      WRITE(*,*)
!
!   CALCULATE 2-D CUBIC PARAMETERS WITHIN EACH BOX.
!
!   NORMALISED COORDINATES.
!     4--<--3
!     |     ^
!     v     |
!     1-->--2
      DATA IX1/0,1,1,0/
      DATA IX2/0,0,1,1/
!
!   FOR EACH BOX, CREATE AND SOLVE THE MATRIX EQUATOION.
!      / values of  \     /              \     / function and \
!    A |  products  | * X | coefficients | = B |  derivative  |
!      \within cubic/     \ of 2D cubic  /     \    values    /
!
!   CONSTRUCT THE MATRIX.
!   THIS IS THE SAME FOR ALL BOXES AS COORDINATES ARE NORMALISED.
!   LOOP THROUGH CORNERS.
      DO ICORN=1,4
         IROW=ICORN
         NX1=IX1(ICORN)
         NX2=IX2(ICORN)
!   LOOP THROUGH POWERS OF VARIABLES.
         DO NPOW1=0,3
            DO NPOW2=0,3
               NPOW1M=NPOW1-1
               IF(NPOW1M < 0)NPOW1M=0
               NPOW2M=NPOW2-1
               IF(NPOW2M < 0)NPOW2M=0
               ICOL=1+4*NPOW1+NPOW2
!   VALUES OF PRODUCTS WITHIN CUBIC AND DERIVATIVES.
               A(IROW ,ICOL)  =powerprod2(1    ,NX1,NPOW1 ,1    ,NX2,NPOW2)
               A(IROW+4 ,ICOL)=powerprod2(NPOW1,NX1,NPOW1M,1    ,NX2,NPOW2)
               A(IROW+8 ,ICOL)=powerprod2(1    ,NX1,NPOW1 ,NPOW2,NX2,NPOW2M)
               A(IROW+12,ICOL)=powerprod2(NPOW1,NX1,NPOW1M,NPOW2,NX2,NPOW2M)

      !A(IROW   ,ICOL)=1.0*(      NX1**NPOW1       *NX2**NPOW2 )
      !A(IROW+4 ,ICOL)=1.0*(NPOW1*NX1**NPOW1M      *NX2**NPOW2 )
      !A(IROW+8 ,ICOL)=1.0*(      NX1**NPOW1 *NPOW2*NX2**NPOW2M)
      !A(IROW+12,ICOL)=1.0*(NPOW1*NX1**NPOW1M*NPOW2*NX2**NPOW2M)
            ENDDO
         ENDDO
      ENDDO
!
!   CONSTRUCT THE 16 R.H.S. VECTORS ( 1 FOR EACH BOX ).
!   LOOP THROUGH BOXES.
      DO NHBOX=0,3
         DO NCBOX=0,3
            ICOL=1+4*NHBOX+NCBOX
            DO ICORN=1,4
               IROW=ICORN
               NX1=IX1(ICORN)+NHBOX
               NX2=IX2(ICORN)+NCBOX
!   VALUES OF FUNCTION AND DERIVATIVES AT CORNER.
               B(IROW   ,ICOL)=  HCH  ( NX1 , NX2 )
               B(IROW+4 ,ICOL)= DHCHDH( NX1 , NX2 )
               B(IROW+8 ,ICOL)= DHCHDC( NX1 , NX2 )
               B(IROW+12,ICOL)=D2HCHD ( NX1 , NX2 )
            ENDDO
         ENDDO
      ENDDO
      N=16
      M=16
!
!   SOLVE BY GAUSS-JORDAN ELIMINATION WITH FULL PIVOTING.
      CALL GAUSSJ(A,N,NP,B,M,MP)
!
!   GET THE COEFFICIENT VALUES.
      DO IBOX=1,16
         ICOL=IBOX
         DO I=1,4
            DO J=1,4
               IROW=4*(I-1)+J
               HCHCF(IBOX,I,J) = B(IROW,ICOL)
            ENDDO
         ENDDO
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKHCC
      use Brenner_vars
use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   GENERATES THE COEFFICIENTS FOR BICUBIC INTERPOLATION OF Hcc(Nih,Nic)
!   COPYRIGHT: KEITH BEARDMORE 02/12/93.
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
!
      integer NP,MP,IX1,IX2,ICORN,IROW,NX1,NX2,NPOW1,NPOW2
      integer NPOW1M,NPOW2M,ICOL,NHBOX,NCBOX,N,M,IBOX,I,J
      REAL*8 A,B,HCC,DHCCDH,DHCCDC,D2HCCD
      real*8 powerprod2

      PARAMETER (NP=16)
      PARAMETER (MP=16)
      DIMENSION A(NP,NP),B(NP,MP)
!
      DIMENSION    HCC(0:4,0:4) , DHCCDH(0:4,0:4) , DHCCDC(0:4,0:4) , D2HCCD(0:4,0:4)
!
      DIMENSION IX1(4),IX2(4)
!      DIMENSION HCCCF(16,4,4)
!
!      COMMON/HCCP/HCCCF
!
!   VALUES GIVEN IN TABLE III.
      DATA HCC   /  0.00000 ,  0.00000 , -0.00610 ,  0.01730 ,  0.00000   &
                 ,  0.00000 , -0.02260 ,  0.01600 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.01490 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
      DATA DHCCDH/  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
      DATA DHCCDC/  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
      DATA D2HCCD/  25*0.0  /
!
!     write(*,'(Changed Hcc(1,0)                from  0.00000 to ,
!     &F8.5)')HCC(1,0)
!      write(*,'(and dHcc(2,0)/dh                from  0.00000 to ,
!     &F8.5)')DHCCDH(2,0)
!      write(*,'(and dHcc(1,1)/dc                from  0.00000 to ,
!     &F8.5)')DHCCDC(1,1)
!      write(*,'(Changed Hcc(2,0)                from -0.00700 to ,
!     &F8.5)')HCC(2,0)
!      WRITE(*,*)
!
!   CALCULATE 2-D CUBIC PARAMETERS WITHIN EACH BOX.
!
!   NORMALISED COORDINATES.
!     4--<--3
!     |     ^
!     v     |
!     1-->--2
      DATA IX1/0,1,1,0/
      DATA IX2/0,0,1,1/
!
!   FOR EACH BOX, CREATE AND SOLVE THE MATRIX EQUATOION.
!      / values of  \     /              \     / function and \
!    A |  products  | * X | coefficients | = B |  derivative  |
!      \within cubic/     \ of 2D cubic  /     \    values    /
!
!   CONSTRUCT THE MATRIX.
!   THIS IS THE SAME FOR ALL BOXES AS COORDINATES ARE NORMALISED.
!   LOOP THROUGH CORNERS.
      DO ICORN=1,4
         IROW=ICORN
         NX1=IX1(ICORN)
         NX2=IX2(ICORN)
!   LOOP THROUGH POWERS OF VARIABLES.
         DO NPOW1=0,3
            DO NPOW2=0,3
               NPOW1M=NPOW1-1
               IF(NPOW1M < 0)NPOW1M=0
               NPOW2M=NPOW2-1
               IF(NPOW2M < 0)NPOW2M=0
               ICOL=1+4*NPOW1+NPOW2
!   VALUES OF PRODUCTS WITHIN CUBIC AND DERIVATIVES.
               A(IROW ,ICOL)  =powerprod2(1    ,NX1,NPOW1 ,1    ,NX2,NPOW2)
               A(IROW+4 ,ICOL)=powerprod2(NPOW1,NX1,NPOW1M,1    ,NX2,NPOW2)
               A(IROW+8 ,ICOL)=powerprod2(1    ,NX1,NPOW1 ,NPOW2,NX2,NPOW2M)
               A(IROW+12,ICOL)=powerprod2(NPOW1,NX1,NPOW1M,NPOW2,NX2,NPOW2M)
               
       !A(IROW   ,ICOL)=1.0*(      NX1**NPOW1       *NX2**NPOW2 )
       !A(IROW+4 ,ICOL)=1.0*(NPOW1*NX1**NPOW1M      *NX2**NPOW2 )
       !A(IROW+8 ,ICOL)=1.0*(      NX1**NPOW1 *NPOW2*NX2**NPOW2M)
       !A(IROW+12,ICOL)=1.0*(NPOW1*NX1**NPOW1M*NPOW2*NX2**NPOW2M)
            ENDDO
         ENDDO
      ENDDO
!
!   CONSTRUCT THE 16 R.H.S. VECTORS ( 1 FOR EACH BOX ).
!   LOOP THROUGH BOXES.
      DO NHBOX=0,3
         DO NCBOX=0,3
            ICOL=1+4*NHBOX+NCBOX
            DO ICORN=1,4
               IROW=ICORN
               NX1=IX1(ICORN)+NHBOX
               NX2=IX2(ICORN)+NCBOX
!   VALUES OF FUNCTION AND DERIVATIVES AT CORNER.
               B(IROW   ,ICOL)=  HCC  ( NX1 , NX2 )
               B(IROW+4 ,ICOL)= DHCCDH( NX1 , NX2 )
               B(IROW+8 ,ICOL)= DHCCDC( NX1 , NX2 )
               B(IROW+12,ICOL)=D2HCCD ( NX1 , NX2 )
            ENDDO
         ENDDO
      ENDDO
      N=16
      M=16
!
!   SOLVE BY GAUSS-JORDAN ELIMINATION WITH FULL PIVOTING.
      CALL GAUSSJ(A,N,NP,B,M,MP)
!
!   GET THE COEFFICIENT VALUES.
      DO IBOX=1,16
         ICOL=IBOX
         DO I=1,4
            DO J=1,4
               IROW=4*(I-1)+J
               HCCCF(IBOX,I,J) = B(IROW,ICOL)
            ENDDO
         ENDDO
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKFCC
         use Brenner_vars
use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   GENERATES THE COEFFICIENTS
!   FOR BICUBIC INTERPOLATION OF Fcc(Nit,Njt,Nconj)
!   COPYRIGHT: KEITH BEARDMORE 02/12/93.
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
!
      integer NP,MP,IX1,IX2,IX3,I,J,K
      integer ICORN,IROW,NX1,NX2,NX3,NPOW1,NPOW2,NPOW3
      integer NPOW1M,NPOW2M,NPOW3M,ICOL,NIBOX,NJBOX,N,M
      integer IBOX
      REAL*8 A,B
      REAL*8 FCC,DFCCDI,DFCCDJ,DFCCDC,D2FDIJ,D2FDIC,D2FDJC,D3FCCD
      real*8 powerprod3
      
      PARAMETER (NP=64)
      PARAMETER (MP=16)
      DIMENSION A(NP,NP),B(NP,MP)
!
      DIMENSION  FCC(0:4,0:4,1:2),  &
	   DFCCDI(0:4,0:4,1:2), DFCCDJ(0:4,0:4,1:2), DFCCDC(0:4,0:4,1:2),   &
	   D2FDIJ(0:4,0:4,1:2), D2FDIC(0:4,0:4,1:2), D2FDJC(0:4,0:4,1:2),   &
	   D3FCCD(0:4,0:4,1:2)
!
      DIMENSION IX1(8),IX2(8),IX3(8)

!
      !COMMON/FCCP/FCCCF
!

!   VALUES GIVEN IN TABLE III.
      DATA FCC   /  0.00000 ,  0.09660 ,  0.04270 , -0.09040 ,  0.00000   &
                 ,  0.09960 ,  0.12640 ,  0.01200 , -0.09030 ,  0.00000   &
                 ,  0.04270 ,  0.01200 ,  0.06050 , -0.04650 ,  0.00000   &
                 , -0.09040 , -0.09030 , -0.04650 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 , -0.02690 , -0.09040 ,  0.00000   &
                 ,  0.00000 ,  0.01080 , -0.03550 , -0.09030 ,  0.00000   &
                 , -0.02690 , -0.03550 ,  0.00000 , -0.04650 ,  0.00000   &
                 , -0.09040 , -0.09030 , -0.04650 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
!
      DATA DFCCDI/  0.00000 ,  0.00000 , -0.09500 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 , -0.10835 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.04515 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 , -0.04520 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 , -0.05055 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.01345 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.02705 ,  0.04515 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
!
      DATA DFCCDJ/  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 , -0.09500 , -0.10835 ,  0.00000 ,  0.04515 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.01345 ,  0.02705 ,  0.00000   &
                 , -0.04520 , -0.05055 ,  0.00000 ,  0.04515 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000   &
                 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000 ,  0.00000/
!
      DATA DFCCDC/ 50 * 0.0 /
      DATA D2FDIJ/ 50 * 0.0 /
      DATA D2FDIC/ 50 * 0.0 /
      DATA D2FDJC/ 50 * 0.0 /
      DATA D3FCCD/ 50 * 0.0 /
!
!      write(*,'(Changed F(0,2,1) & F(2,0,1)     from  0.03200 to ,
!     &F8.5, & ,F8.5)')FCC(0,2,1),FCC(2,0,1)
!      write(*,'(and dF(1,2,1)/di & dF(2,1,1)/dj from  0.00000 to ,
!     &F8.5, & ,F8.5)')DFCCDI(1,2,1),DFCCDJ(2,1,1)
!      write(*,'(Changed F(0,2,2) & F(2,0,2)     from -0.04450 to ,
!     &F8.5, & ,F8.5)')FCC(0,2,2),FCC(2,0,2)
!      write(*,'(and dF(1,2,2)/di & dF(2,1,2)/dj from  0.02225 to ,
!     &F8.5, & ,F8.5)')DFCCDI(1,2,2),DFCCDJ(2,1,2)
!      write(*,'(Changed F(1,1,1)                from  0.15110 to ,
!     &F8.5)')FCC(1,1,1)
!      write(*,'(and dF(2,1,1)/di & dF(1,2,1)/dj from -0.13205 to ,
!     &F8.5, & ,F8.5)')DFCCDI(2,1,1),DFCCDJ(1,2,1)
!      write(*,'(Changed F(1,3,2) & F(3,1,2)     from -0.11300 to ,
!     &F8.5, & ,F8.5)')FCC(1,3,2),FCC(1,3,2)
!      write(*,'(and dF(2,1,2)/di & dF(1,2,2)/dj from -0.06020 to ,
!     &F8.5, & ,F8.5)')DFCCDI(2,1,2),DFCCDJ(1,2,2)
!      write(*,'(and dF(2,3,2)/di & dF(3,2,2)/dj from  0.05650 to ,
!     &F8.5, & ,F8.5)')DFCCDI(2,3,2),DFCCDJ(3,2,2)
!      write(*,'(Changed F(2,2,1)                from  0.07500 to ,
!     &F8.5)')FCC(2,2,1)
!      write(*,'(Changed F(2,3,2) & F(3,2,2)     from -0.04650 to ,
!     &F8.5, & ,F8.5)')FCC(2,3,2),FCC(3,2,2)
!      write(*,'(and dF(1,3,2)/di & dF(3,1,2)/dj from  0.03775 to ,
!     &F8.5, & ,F8.5)')DFCCDI(1,3,2),DFCCDJ(3,1,2)
!
!   CALCULATE 3-D CUBIC PARAMETERS WITHIN EACH BOX.
!
!   NORMALISED COORDINATES.
!
!       8--<--7
!      /|    /|
!     5-->--6 |
!     | 4--<|-3
!     |/    |/
!     1-->--2
      DATA IX1/0,1,1,0,0,1,1,0/
      DATA IX2/0,0,1,1,0,0,1,1/
      DATA IX3/0,0,0,0,1,1,1,1/
!
!   FOR EACH BOX, CREATE AND SOLVE THE MATRIX EQUATOION.
!      / values of  \     /              \     / function and \
!    A |  products  | * X | coefficients | = B |  derivative  |
!      \within cubic/     \ of 3D cubic  /     \    values    /
!
!   CONSTRUCT THE MATRIX.
!   THIS IS THE SAME FOR ALL BOXES AS COORDINATES ARE NORMALISED.
!   LOOP THROUGH CORNERS.
      DO ICORN=1,8
         IROW=ICORN
         NX1=IX1(ICORN)
         NX2=IX2(ICORN)
         NX3=IX3(ICORN)
!   LOOP THROUGH POWERS OF VARIABLES.
         DO NPOW1=0,3
            DO NPOW2=0,3
               DO NPOW3=0,3
                  NPOW1M=NPOW1-1
                  IF(NPOW1M < 0)NPOW1M=0
                  NPOW2M=NPOW2-1
                  IF(NPOW2M < 0)NPOW2M=0
                  NPOW3M=NPOW3-1
                  IF(NPOW3M < 0)NPOW3M=0
                  ICOL=1+16*NPOW1+4*NPOW2+NPOW3
!   VALUES OF PRODUCTS WITHIN CUBIC AND DERIVATIVES.
      A(IROW   ,ICOL)=powerprod3(1    ,NX1,NPOW1 ,1    ,NX2,NPOW2 ,1    ,NX3,NPOW3)
      A(IROW+8 ,ICOL)=powerprod3(NPOW1,NX1,NPOW1M,1    ,NX2,NPOW2 ,1    ,NX3,NPOW3)
      A(IROW+16,ICOL)=powerprod3(1    ,NX1,NPOW1 ,NPOW2,NX2,NPOW2M,1    ,NX3,NPOW3)
      A(IROW+24,ICOL)=powerprod3(1    ,NX1,NPOW1 ,1    ,NX2,NPOW2 ,NPOW3,NX3,NPOW3M)
      A(IROW+32,ICOL)=powerprod3(NPOW1,NX1,NPOW1M,NPOW2,NX2,NPOW2M,1    ,NX3,NPOW3)
      A(IROW+40,ICOL)=powerprod3(NPOW1,NX1,NPOW1M,1    ,NX2,NPOW2 ,NPOW3,NX3,NPOW3M)
      A(IROW+48,ICOL)=powerprod3(1    ,NX1,NPOW1 ,NPOW2,NX2,NPOW2M,NPOW3,NX3,NPOW3M)
      A(IROW+56,ICOL)=powerprod3(NPOW1,NX1,NPOW1M,NPOW2,NX2,NPOW2M,NPOW3,NX3,NPOW3M)

       !A(IROW   ,ICOL)=1.0*
       !&        (       NX1**NPOW1        *NX2**NPOW2        *NX3**NPOW3  )
       ! A(IROW+8 ,ICOL)=1.0*
       !&       ( NPOW1*NX1**NPOW1M       *NX2**NPOW2        *NX3**NPOW3  )
       ! A(IROW+16,ICOL)=1.0*
       !&       (       NX1**NPOW1  *NPOW2*NX2**NPOW2M       *NX3**NPOW3  )
       ! A(IROW+24,ICOL)=1.0*
       !&       (       NX1**NPOW1        *NX2**NPOW2  *NPOW3*NX3**NPOW3M )
       ! A(IROW+32,ICOL)=1.0*
       !&       ( NPOW1*NX1**NPOW1M *NPOW2*NX2**NPOW2M       *NX3**NPOW3  )
       ! A(IROW+40,ICOL)=1.0*
       !&       ( NPOW1*NX1**NPOW1M       *NX2**NPOW2  *NPOW3*NX3**NPOW3M )
       ! A(IROW+48,ICOL)=1.0*
       !&       (       NX1**NPOW1  *NPOW2*NX2**NPOW2M *NPOW3*NX3**NPOW3M )
       ! A(IROW+56,ICOL)=1.0*
       !&       ( NPOW1*NX1**NPOW1M *NPOW2*NX2**NPOW2M *NPOW3*NX3**NPOW3M )

      

               ENDDO
            ENDDO
         ENDDO
      ENDDO
!
!   CONSTRUCT THE 16 R.H.S. VECTORS ( 1 FOR EACH BOX ).
!   LOOP THROUGH BOXES.
      DO NIBOX=0,3
         DO NJBOX=0,3
            ICOL=1+4*NIBOX+NJBOX
            DO ICORN=1,8
               IROW=ICORN
               NX1=IX1(ICORN)+NIBOX
               NX2=IX2(ICORN)+NJBOX
               NX3=IX3(ICORN)+1
!   VALUES OF FUNCTION AND DERIVATIVES AT CORNER.
               B(IROW   ,ICOL)=  FCC  ( NX1 , NX2 , NX3 )
               B(IROW+8 ,ICOL)= DFCCDI( NX1 , NX2 , NX3 )
               B(IROW+16,ICOL)= DFCCDJ( NX1 , NX2 , NX3 )
               B(IROW+24,ICOL)= DFCCDC( NX1 , NX2 , NX3 )
               B(IROW+32,ICOL)=D2FDIJ ( NX1 , NX2 , NX3 )
               B(IROW+40,ICOL)=D2FDIC ( NX1 , NX2 , NX3 )
               B(IROW+48,ICOL)=D2FDJC ( NX1 , NX2 , NX3 )
               B(IROW+56,ICOL)=D3FCCD ( NX1 , NX2 , NX3 )
            ENDDO
         ENDDO
      ENDDO
      N=64
      M=16
!
!   SOLVE BY GAUSS-JORDAN ELIMINATION WITH FULL PIVOTING.
      CALL GAUSSJ(A,N,NP,B,M,MP)
!
!   GET THE COEFFICIENT VALUES.
      DO IBOX=1,16
         ICOL=IBOX
         DO I=1,4
            DO J=1,4
               DO K=1,4
                  IROW=16*(I-1)+4*(J-1)+K
                  FCCCF(IBOX,I,J,K) = B(IROW,ICOL)
               ENDDO
            ENDDO
         ENDDO
      ENDDO
!
      RETURN
      END
!
!----------------------------------------------------------------------------
      real*8 function powerprod2(f1,i1,p1,f2,i2,p2)
      implicit none
      integer f1,i1,p1,f2,i2,p2

      integer t1,t2

      if (.not. (i1==0 .and. p1==0)) then
         t1=i1**p1
      else
         t1=1
      endif
      if (.not. (i2==0 .and. p2==0)) then
         t2=i2**p2
      else
         t2=1
      endif

      powerprod2=1.0*f1*t1*f2*t2
      return
      end

!----------------------------------------------------------------------------
      real*8 function powerprod3(f1,i1,p1,f2,i2,p2,f3,i3,p3)
      implicit none
      integer f1,i1,p1,f2,i2,p2,f3,i3,p3

      integer t1,t2,t3

      if (.not. (i1==0 .and. p1==0)) then
         t1=i1**p1
      else
         t1=1
      endif
      if (.not. (i2==0 .and. p2==0)) then
         t2=i2**p2
      else
         t2=1
      endif
      if (.not. (i3==0 .and. p3==0)) then
         t3=i3**p3
      else
         t3=1
      endif

      powerprod3=1.0*f1*t1*f2*t2*f3*t3
      !print *,f1,i1,p1,f2,i2,p2,f3,i3,p3
      !print *,powerprod3,1.0*f1*i1**p1*f2*i2**p2*f3*i3**p3
      return
      end

!#######################################################################
      SUBROUTINE GAUSSJ(A,N,NP,B,M,MP)
use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   SOLVES AN NxN MATRIX EQUATION WITH M RIGHT HAND SIDE VECTORS
!   USING GAUSS-JORDAN ELIMINATION WITH FULL PIVOTING.
!   INVERSE MATRIX AND SOLUTION VECTORS ARE RETURNED IN PLACE OF INPUTS.
!   ACTUAL ARRAYS FOR A AND B ARE NPxNP AND NPxMP.
!   NMAX MUST BE LARGER THAN N.
!   COPYRIGHT: KEITH BEARDMORE 30/11/93.
!   ALGORITHM FROM NUMERICAL RECIPES P28.
!
!     A(I,J) IS Aij ROW I COLUMN J.
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
!
      REAL*8 A,B
      integer N,NP,M,MP
      REAL*8 BIG,DUM,PIVINV
      integer NMAX,IPIV,INDXR,INDXC,I,J,K,IROW,ICOL,L,LL
      
      PARAMETER (NMAX=64)
      DIMENSION A(NP,NP),B(NP,MP),IPIV(NMAX),INDXR(NMAX),INDXC(NMAX)
      DO J=1,N
         IPIV(J)=0
      ENDDO
      DO I=1,N
         BIG=0.0
         DO J=1,N
            IF(IPIV(J) /= 1)THEN
               DO K=1,N
                  IF(IPIV(K) == 0)THEN
                     IF(ABS(A(J,K)) >= BIG)THEN
                        BIG=ABS(A(J,K))
                        IROW=J
                        ICOL=K
                     ENDIF
                  ELSEIF(IPIV(K) > 1)THEN
                     stop 'Singular matrix'
                  ENDIF
               ENDDO
            ENDIF
         ENDDO
         IPIV(ICOL)=IPIV(ICOL)+1
         IF(IROW /= ICOL)THEN
            DO L=1,N
               DUM=A(IROW,L)
               A(IROW,L)=A(ICOL,L)
               A(ICOL,L)=DUM
            ENDDO
            DO L=1,M
               DUM=B(IROW,L)
               B(IROW,L)=B(ICOL,L)
               B(ICOL,L)=DUM
            ENDDO
         ENDIF
         INDXR(I)=IROW
         INDXC(I)=ICOL
         IF(A(ICOL,ICOL) == 0.0)stop 'Singular matrix'
         PIVINV=1.0/A(ICOL,ICOL)
         A(ICOL,ICOL)=1.0
         DO L=1,N
            A(ICOL,L)=A(ICOL,L)*PIVINV
         ENDDO
         DO L=1,M
            B(ICOL,L)=B(ICOL,L)*PIVINV
         ENDDO
         DO LL=1,N
            IF(LL /= ICOL)THEN
               DUM=A(LL,ICOL)
               A(LL,ICOL)=0.0
               DO L=1,N
                  A(LL,L)=A(LL,L)-A(ICOL,L)*DUM
               ENDDO
               DO L=1,M
                  B(LL,L)=B(LL,L)-B(ICOL,L)*DUM
               ENDDO
            ENDIF
         ENDDO
      ENDDO
      DO L=N,1,-1
         IF(INDXR(L) /= INDXC(L))THEN
            DO K=1,N
               DUM=A(K,INDXR(L))
               A(K,INDXR(L))=A(K,INDXC(L))
               A(K,INDXC(L))=DUM
            ENDDO
         ENDIF
      ENDDO
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKF1
         use Brenner_vars
use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   GENERATES THE COEFFICIENTS FOR CUBIC SPLINE INTERPOLATION OF F1(Nt)
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!
!
      REAL*8 X,Y,Y2,YP1,YPN
      integer N,INTERV
      

      PARAMETER (N=4)
      DIMENSION X(N),Y(N),Y2(N)
!
!      DIMENSION AF1(3,4),AF2(3,4)
!      COMMON/FSIP/AF1,AF2
!
!   VALUES GIVEN IN TABLE I.
      DATA X   / 1.000 , 2.000 , 3.000 , 4.000 /
      DATA Y   / 1.005 , 1.109 , 0.953 , 1.000 /
      DATA YP1 / 0.000 /
      DATA YPN / 0.000 /
!
!   CALCULATE SECOND DERIVATIVE VALUES AT INTERPOLATION POINTS.
      CALL SPLINE(X,Y,N,YP1,YPN,Y2)
!
!   GET THE COEFFICIENT VALUES.
      DO INTERV=1,N-1
         AF1(INTERV,1) =  Y(INTERV)
         AF1(INTERV,2) =  Y(INTERV+1)     -  Y(INTERV)      - Y2(INTERV+1)/6.0 - Y2(INTERV)/3.0
         AF1(INTERV,3) =                    Y2(INTERV)/2.0
         AF1(INTERV,4) = Y2(INTERV+1)/6.0 - Y2(INTERV)/6.0
!      WRITE(*,*)AF1(INTERV,1)+AF1(INTERV,2)+AF1(INTERV,3)+AF1(INTERV,4)
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKF2
      use Brenner_vars
use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   GENERATES THE COEFFICIENTS FOR CUBIC SPLINE INTERPOLATION OF F2(Nt)
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!
!
      REAL*8 X,Y,YP1,YPN,Y2
      integer N,INTERV

      PARAMETER (N=4)
      DIMENSION X(N),Y(N),Y2(N)
!
!      DIMENSION AF1(3,4),AF2(3,4)
!      COMMON/FSIP/AF1,AF2
!
!   VALUES GIVEN IN TABLE I.
      DATA X   / 1.000 , 2.000 , 3.000 , 4.000 /
      DATA Y   / 0.930 , 1.035 , 0.934 , 1.000 /
      DATA YP1 / 0.000 /
      DATA YPN / 0.000 /
!
!   CALCULATE SECOND DERIVATIVE VALUES AT INTERPOLATION POINTS.
      CALL SPLINE(X,Y,N,YP1,YPN,Y2)
!
!   GET THE COEFFICIENT VALUES.
      DO INTERV=1,N-1
         AF2(INTERV,1) =  Y(INTERV)
         AF2(INTERV,2) =  Y(INTERV+1)     -  Y(INTERV)      - Y2(INTERV+1)/6.0 - Y2(INTERV)/3.0
         AF2(INTERV,3) =                    Y2(INTERV)/2.0
         AF2(INTERV,4) = Y2(INTERV+1)/6.0 - Y2(INTERV)/6.0
!      WRITE(*,*)AF2(INTERV,1)+AF2(INTERV,2)+AF2(INTERV,3)+AF2(INTERV,4)
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE MKH
      use Brenner_vars
use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   GENERATES THE COEFFICIENTS FOR CUBIC SPLINE INTERPOLATION OF H(Nt)
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!
!
      REAL*8 X,Y,YP1,YPN,Y2
      integer N,INTERV

      PARAMETER (N=4)
      DIMENSION X(N),Y(N),Y2(N)
!
!      COMMON/HSIP/AH
!
!   VALUES GIVEN IN TABLE I.
      DATA X   /  1.000 ,  2.000 ,  3.000 ,  4.000 /
      DATA Y   / -0.040 , -0.040 , -0.276 , -0.470 /
      DATA YP1 /  0.000 /
      DATA YPN /  0.000 /
!
!   CALCULATE SECOND DERIVATIVE VALUES AT INTERPOLATION POINTS.
      CALL SPLINE(X,Y,N,YP1,YPN,Y2)
!
!   GET THE COEFFICIENT VALUES.
      DO INTERV=1,N-1
         AH(INTERV,1) =  Y(INTERV)
         AH(INTERV,2) =  Y(INTERV+1)     -  Y(INTERV)     - Y2(INTERV+1)/6.0 - Y2(INTERV)/3.0
         AH(INTERV,3) =                    Y2(INTERV)/2.0
         AH(INTERV,4) = Y2(INTERV+1)/6.0 - Y2(INTERV)/6.0
!      WRITE(*,*)AH(INTERV,1)+AH(INTERV,2)+AH(INTERV,3)+AH(INTERV,4)
      ENDDO
!
      RETURN
      END
!
!#######################################################################
      SUBROUTINE SPLINE(X,Y,N,YP1,YPN,Y2)
use my_mpi
      IMPLICIT NONE
!#######################################################################
!
!   GIVEN ARRAYS X AND Y OF LENGTH N CONTAINING A TABULATED FUNCTION,
!   I.E. Yi = F(Xi), WITH X1 < X2 < ... < XN, AND GIVEN VALUES YP1 AND
!   YPN FOR THE FIRST DERIVATIVE OF THE INTERPOLATING FUNCTION AT POINTS
!   1 AND N, RESPECTIVELY, THIS ROUTINE RETURNS AN ARRAY Y2 OF LENGTH N
!   WHICH CONTAINS THE SECOND DERIVATIVES OF THE INTERPOLATING FUNCTION
!   AT THE TABULATED POINTS Xi. IF YP1 AND/OR YPN ARE EQUAL TO 1E30 OR
!   LARGER, THE ROUTINE IS SIGNALLED TO SET THE CORRESPONDING BOUNDARY
!   CONDITION FOR A NATURAL SPLINE, WITH ZERO SECOND DERIVATIVE ON THAT
!   BOUNDARY.
!   COPYRIGHT: KEITH BEARDMORE 31/08/94.
!   ALGORITHM FROM NUMERICAL RECIPES P88.
!
!
      REAL*8 X,Y,YP1,YPN,Y2
      integer N
      REAL*8 U,QN,UN,SIG,P
      integer I,NMAX,K

      PARAMETER (NMAX=4)
      DIMENSION X(N),Y(N),Y2(N),U(NMAX)
      IF( YP1  >  0.99E30 )THEN
         Y2(1) = 0.0
         U(1)  = 0.0
      ELSE
         Y2(1) = -0.5
         U(1)  = (3.0/(X(2)-X(1)))*((Y(2)-Y(1))/(X(2)-X(1))-YP1)
      ENDIF
      DO I=2,N-1
         SIG   = (X(I)-X(I-1))/(X(I+1)-X(I-1))
         P     = SIG*Y2(I-1)+2.0
         Y2(I) = (SIG-1.0)/P
         U(I)  = (6.0*((Y(I+1)-Y(I))/(X(I+1)-X(I))-(Y(I)-Y(I-1)) /(X(I)-X(I-1)))/(X(I+1)-X(I-1))-SIG*U(I-1))/P
      ENDDO
      IF( YPN  >  0.99E30 )THEN
         QN    = 0.0
         UN    = 0.0
      ELSE
         QN    = 0.5
         UN    = (3.0/(X(N)-X(N-1)))*(YPN-(Y(N)-Y(N-1))/(X(N)-X(N-1)))
      ENDIF
      Y2(N) = (UN-QN*U(N-1))/(QN*Y2(N-1)+1.0)
      DO K=N-1,1,-1
         Y2(K) = Y2(K)*Y2(K+1)+U(K)
      ENDDO
      RETURN
      END
!
!#######################################################################
      SUBROUTINE ZBLPRM
use my_mpi
!#######################################################################
!
!   CALCULATES THE VALUES OF THE CONSTANTS USED IN
!   ZEIGLER, BIERSACK & LITMARK'S UNIVERSAL POTENTIAL.
!   COPYRIGHT: KEITH BEARDMORE 12/5/94.
!   PARAMETERS FROM : COMPUTER SIMULATION OF ION-SOLID INTERACTIONS,
!   PP 40-42, W. ECKSTEIN, SPRINGER-VERLAG(BERLIN HEIDELBERG), (1991).
!
!####&<THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.THIS IS 7 TO 72.>
      use defs
      use Brenner_vars
      use para_common, only : PI
      IMPLICIT NONE
!
      REAL*8 C(4),D(4),ZC,ZSI,ZH,ZAR
      REAL*8 E2B4PE,E,ACONST,ABOHR,POW,E2
      REAL*8 R1CAR,R1SIAR,R1HAR,R1ARAR,R2CAR,R2SIAR,R2HAR,R2ARAR
      REAL*8 A(4),CCONST(4)
!
      INTEGER ITYPE,I
!
      !REAL*8 CONRH2(BCOMBS,11),CONRL(BCOMBS,10),CONFCA(BCOMBS,10),CONFC(BCOMBS,10),fermib(BCOMBS,10),fermir(BCOMBS,10)
      !REAL*8 CONC(4,4),COND(4,4)
!
      !COMMON/TBC1/ CONRH2,CONRL,CONFCA,CONFC,fermib,fermir
      !COMMON/ZBLU/ CONC,COND
!
!      PI = ACOS ( -1.0 )
!
!   VALUES FOR THE PARAMETERS.
      DATA C      / 0.028171 , 0.28022 , 0.50986 , 0.18175 /
      DATA D      / 0.20162  , 0.40290 , 0.94229 , 3.1998  /
      DATA ZC     / 6.0            /
      DATA ZSI    / 14.0           /
      DATA ZH     / 1.0            /
      DATA ZAR    / 18.0           /
      DATA E      / 1.60217733E-19 /
      DATA E2B4PE / 2.3070796E-28  /
      DATA ACONST / 0.88534        /
      DATA ABOHR  / 0.530          /
      DATA POW    / 0.23           /
!   CUTOFF FROM 90% TO 110% OF ESTIMATED BOND LENGTH.
      DATA R1CAR  , R1SIAR , R1HAR  , R1ARAR / 2.3850 , 2.7481 , 1.9597 , 3.3795 /
      DATA R2CAR  , R2SIAR , R2HAR  , R2ARAR / 2.9150 , 3.3589 , 2.3953 , 4.1305 /
!
!   E2 = e^2 / 4 Pi Epsilon0 ( IN ANGSTROMS AND eV ).
      E2 = E2B4PE / ( 1E-10 * E )
!
!   CALCULATE BONDLENGTH CONSTANTS.
      A(1) = ACONST * ABOHR / (  ZAR ** POW + ZC  ** POW )
      A(2) = ACONST * ABOHR / (  ZAR ** POW + ZSI ** POW )
      A(3) = ACONST * ABOHR / (  ZAR ** POW + ZH  ** POW )
      A(4) = ACONST * ABOHR / (  ZAR ** POW + ZAR ** POW )
      CCONST(1) = ZAR * ZC  * E2
      CCONST(2) = ZAR * ZSI * E2
      CCONST(3) = ZAR * ZH  * E2
      CCONST(4) = ZAR * ZAR * E2
!
!   CONSTRUCT C-Ar, Si-Ar, H-Ar AND Ar-Ar PARAMETERS.
      DO ITYPE = 1,4
         DO I = 1,4
            CONC(ITYPE,I) =  C(I) * CCONST(ITYPE)
            COND(ITYPE,I) = -D(I) / A(ITYPE)
         ENDDO
      ENDDO
!
!   CUTOFF CONSTANTS.
      CONRL(1,7)   = R1CAR
      CONRL(1,8)   = R1SIAR
      CONRL(1,9)   = R1HAR
      CONRL(1,10)  = R1ARAR
      CONRH2(1,7)  = R2CAR  ** 2
      CONRH2(1,8)  = R2SIAR ** 2
      CONRH2(1,9)  = R2HAR  ** 2
      CONRH2(1,10) = R2ARAR ** 2
      CONFCA(1,7)  = PI / ( R2CAR  - R1CAR  )
      CONFCA(1,8)  = PI / ( R2SIAR - R1SIAR )
      CONFCA(1,9)  = PI / ( R2HAR  - R1HAR  )
      CONFCA(1,10) = PI / ( R2ARAR - R1ARAR )
      CONFC(1,7)   = - 0.5 * CONFCA(1,7)
      CONFC(1,8)   = - 0.5 * CONFCA(1,8)
      CONFC(1,9)   = - 0.5 * CONFCA(1,9)
      CONFC(1,10)  = - 0.5 * CONFCA(1,10)
!
      RETURN
      END



!#######################################################################
!#######################################################################
      
      ! Parcas interface SetCut routine
      ! This is bloody ugly because of hardcoded atom types...

      subroutine Beardmore_SetCut(reppotcutin)
      use typeparam 
      use defs
      use Brenner_vars
      use my_mpi
      
      implicit none

      real*8 reppotcutin

      integer i,j,itype

      !REAL*8 CONRH2(BCOMBS,11),CONRL(BCOMBS,10),CONFCA(BCOMBS,10),CONFC(BCOMBS,10),fermib(BCOMBS,10),fermir(BCOMBS,10)
      !COMMON/TBC1/ CONRH2,CONRL,CONFCA,CONFC,fermib,fermir

      ! Cutoff for possible spline pair potentials
      CONRH2(1,11)=reppotcutin*reppotcutin
 
      ! First do i > j cutoffs, then symmetrisize



      ! H-H cutoffs
     ! rcut(0,0)=sqrt(CONRH2(1,6));
     ! rcut(2,0)=sqrt(CONRH2(1,6));
     ! rcut(3,0)=sqrt(CONRH2(1,6));
     ! rcut(4,0)=sqrt(CONRH2(1,6));
     ! do i=2,4
     !    do j=2,4
     !       rcut(i,j)=sqrt(CONRH2(1,6));
     !    enddo
     ! enddo
      ! H-C
     ! rcut(1,0)=sqrt(CONRH2(1,3));
     ! rcut(2,1)=sqrt(CONRH2(1,3));
     ! rcut(3,1)=sqrt(CONRH2(1,3));
     ! rcut(4,1)=sqrt(CONRH2(1,3));
      ! C-C
      rcut(1,1)=sqrt(CONRH2(1,1));
      if (itypehigh > 4) then
         ! C-Si
         rcut(5,1)=sqrt(CONRH2(1,2));
         ! H-Si
         rcut(5,0)=sqrt(CONRH2(1,5));
         rcut(5,2)=sqrt(CONRH2(1,5));
         rcut(5,3)=sqrt(CONRH2(1,5));
         rcut(5,4)=sqrt(CONRH2(1,5));
         ! Si-Si
         rcut(5,5)=sqrt(CONRH2(1,4));
      endif
      if (itypehigh > 5) then
         ! H-Ar
         rcut(6,0)=sqrt(CONRH2(1,9));
         rcut(6,2)=sqrt(CONRH2(1,9));
         rcut(6,3)=sqrt(CONRH2(1,9));
         rcut(6,4)=sqrt(CONRH2(1,9));
         ! C-Ar
         rcut(6,1)=sqrt(CONRH2(1,7));
         ! Si-Ar
         rcut(6,5)=sqrt(CONRH2(1,8));
         ! Ar-Ar
         rcut(6,6)=sqrt(CONRH2(1,10));
      endif

      if (itypehigh > 6) then
         do itype=7,itypehigh

            do i=0,itype
               rcut(itype,i) = reppotcutin
            enddo
         enddo
         ! Be-Be
         rcut(7,7)=2.685
         ! Be-C
         rcut(7,1)=2.8
         ! Be-H
         rcut(7,0)=1.95
         rcut(7,2)=1.95
         rcut(7,3)=1.95
         rcut(7,4)=1.95
      endif

      do i=itypelow,itypehigh
         do j=i+1,itypehigh
            rcut(i,j)=rcut(j,i)
         enddo
      enddo

      return
      end
