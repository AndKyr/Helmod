  
  !***********************************************************************
  ! Routines to calculate Tersoff potential
  ! see PRB 39, 5566 (1989).
  ! Jamie Morris 6 June 1995, parallel version by Dave Turner - Nov of 1995
  ! Taken into PARCAS from alcmd V3.0 starting Jan 28 1998 - Kai Nordlund
  !
  ! 
  !***********************************************************************
  !
  ! potmode determines mode, name(1) element according to following:
  !
  ! name(1)   potmode      Potential
  ! -------   --------     ----------
  ! Si        5            Tersoff III, with lambda3
  ! Si        6            Tersoff III, no lambda3
  ! Si        7            Tersoff II, with lambda3
  ! Si        8            Tersoff II, no lambda3
  !
  !
  ! This version is an elemental version, i.e. Tersoff alloys can not
  ! be handled by it ! Another version should be made to handle alloys;
  ! it is better to keep the two versions separated for speed purposes 
  !
  
  ! nborlist(i) has following format:
  !
  !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
  !  nborlist(2)    Index of first neighbour of atom 1
  !  nborlist(3)    Index of second neighbour of atom 1
  !   ...
  !  nborlist(N1+1)  Index of last neighbour of atom 1
  !  nborlist(N1+2)  Number of neighbours for atom 2 
  !  nborlist(N1+3)  Index of first neighbour of atom 2
  !   ...
  !   ...           And so on for all N atoms
  !

  subroutine Tersoff_Force(x0,atype,xnp,atomindex,natoms,box,pbc,          &
       nborlist,Epair,Vpair,Ethree,Vmany,wxx,wyy,wzz,wxxi,wyyi,wzzi,       &
       wxxsh,wyysh,wzzsh,natsh,nsh,                                        &
       wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,	           &
       atpassflag,nngbrproc,ECM,moviemode,                                 &
       movietime_val,calc_avgvir,last_step_predict)

    use typeparam
    use datatypes
    use my_mpi

    use defs

    use para_common

    implicit none

    ! ------------------------------------------------------------------
    ! Variables passed in and out

    real*8 x0(*),xnp(*)
    real*8 box(3),pbc(3)
    integer*4 nborlist(*)
    integer atype(*),atomindex(*)
    integer natoms

    real*8 Rcutsw, Epair(*), Vpair, Ethree(*), Vmany
    real*8 wxx,wyy,wzz,wxxi(*),wyyi(*),wzzi(*)
    real*8 swpotmod,sw3mod

    real*8 wxy,wxyi(*),wxz,wxzi(*),wyx,wyxi(*),wyz,wyzi(*),wzx,wzxi(*),wzy,wzyi(*)
    integer moviemode
    logical movietime_val
    logical calc_vir
    logical calc_avgvir
    logical last_step_predict

    real*8 wxxsh(*),wyysh(*),wzzsh(*),ECM(3),dx,dy,dz
    integer natsh(*),nsh,nnsh

    integer atpassflag(*),nngbrproc(*),potmode

    ! Tersoff common blocks

    common /tersoff/tersA,tersB,beta,rlambda,rmu,gc,gd,gh,ters_n,rlambda3_cube
    common /tersoff2/gd2,gd2i,gc2,g2c2
    common /cutoff/trcut,dcut,rmd,rpd,a,halfa
    common /reppot/ reppotcut,bf,rf

    real*8 tersA,tersB,beta,rlambda,rmu,gc,gd,gh,ters_n,rlambda3_cube
    real*8 gd2,gd2i,gc2,g2c2
    real*8 trcut,dcut,rmd,rpd,a,halfa
    real*8 reppotcut,bf,rf

    ! ------------------------------------------------------------------
    ! Local variables and constants

    real*8 xa(3,NNMAX),xab(3,NNMAX),xai(3,NNMAX)
    real*8 ra(NNMAX),rai(NNMAX),ra2i(NNMAX)
    real*8 fc(NNMAX),dfc(NNMAX),z(NNMAX),db(NNMAX)
    real*8 gt(NNMAX*NNMAX),dG(NNMAX*NNMAX),cth(NNMAX*NNMAX)
    real*8 dfcxr(3,NNMAX),xr2(3,NNMAX)


    ! Previously implicit variables
    integer I,J,K,I3,J3,K3,IJ,IK,MJK
    integer NPASS,NNBORS


    real*8 TERS2I,RA2,DFCR,FATT,FREP
    real*8 DFATT,DFREP,BZP,BZP1,BIJ,cpair,cmany,DF,C1
    real*8 CJ,CK,CUT

    real*8 dbfcdgcth,dbfcdgrr,dbjgt,dbkgt,dbfcdg,dbfcjk,dbfckj

!include 'para_common.f90'

    ! ------------------------------------------------------------------
    ! New variables by Kai Nordlund
    integer in,in0,np0

    real*8 help1,help2,help3,V

    real*8 expij(NNMAX*NNMAX),expik(NNMAX*NNMAX),dr(NNMAX*NNMAX)
    real*8 dexpij(NNMAX*NNMAX),dexpik(NNMAX*NNMAX)
    real*8 dbfcgde

    real*8 ghmctheta,denomi
    real*8 theta,s,fermi,dfermi

    real*8 maxr,r

    ! type variables
    integer typei,typeij(NNMAX),typek
    integer iact(NNMAX)

    ! Parallell routine variables
    integer jj,i2,j2,ii,ii3,myi,dfrom,ifrom,d
    real*8 t1

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    ! Gather the coordinates of all local atoms
    do i = 1,3*myatoms
       buf(i) = x0(i)
    enddo
    do i=1,myatoms
       dirbuf(i)=0
       ibuf(i*2-1)=i
       ibuf(i*2)=atype(i)
    enddo
    np0=myatoms


    ! Handle communication: get x info into buf()
    ! For more notes, see stilweb.f90 or EAMforces.f90

    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       ! Loop over directions
       do d=1,8
          j=0; jj=0
          do i=1,myatoms
             if (IAND(atpassflag(i),passbit(d)) .ne. 0) then
                j=j+1
                if (j .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow1', int(myproc))
                i3=i*3-3
                j3=j*3-3
                j2=j*2-2
                xsendbuf(j3+1)=x0(i3+1)
                xsendbuf(j3+2)=x0(i3+2)
                xsendbuf(j3+3)=x0(i3+3)
                isendbuf(j2+1)=i
                isendbuf(j2+2)=atype(i)
             endif
          enddo
          !print *,'send',myproc,d,j,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(np0*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(np0*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
          endif
          j=jj
          do i=1,j
             dirbuf(np0+i)=d
          enddo
          np0=np0+j
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif
    if (np0 .ne. np0pairtable) then
       write (6,*) 'EAMforces np0 problem ',myproc,np0,np0pairtable
    endif
    !print *,'Node received totally ',myproc,np0-myatoms

    ! Communication done

    do i=1,3*np0
       pbuf(i)=zero
    enddo
    do i=1,np0
       ebuf(i)=zero     
    enddo

    Vpair = zero
    Vmany = zero
    do j = 1,myatoms
       Epair(j) = zero
       Ethree(j) = zero
       wxxi(j) = zero
       wyyi(j) = zero
       wzzi(j) = zero
    enddo

    calc_vir = .false.
    if (movietime_val .or. calc_avgvir .or. last_step_predict) then
     if (moviemode == 15 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
      calc_vir = .true.
     endif
    endif

    if (calc_vir) then
       wxy=zero
       wxz=zero
       wyx=zero
       wyz=zero
       wzx=zero
       wzy=zero
       do j = 1,myatoms
          wxyi(j) = zero
          wxzi(j) = zero
          wyxi(j) = zero
          wyzi(j) = zero
          wzxi(j) = zero
          wzyi(j) = zero
       enddo
    endif

    wxx=zero; wyy=zero; wzz=zero;
    do j=1,nsh
       wxxsh(j)=zero
       wyysh(j)=zero
       wzzsh(j)=zero
       natsh(j)=0
    enddo
    maxr=sqrt(box(1)*box(1)+box(2)*box(2)+box(3)*box(3))/2.0

    ters2i = -half/ters_n


    !print *,'tersoff:',myatoms,np0,nborlist(1)

    ! Neighbour list counter
    in=0
    do i = 1,myatoms   ! <-------------------- Loop over atoms i
       i3 = i*3-3

       typei=abs(ibuf(i*2))

       in=in+1; in0=in
       nnbors = nborlist(in)
       in=in+nnbors

       if (nnbors < 0 .or. nnbors > NNMAX) then 
          print *,'Tersoff nnbors HORROR ERROR',nnbors,NNMAX
          print *,i,i3
          call my_mpi_abort('Tersoff nnbors', int(nnbors))
       endif

       !
       ! Calculate f_C and df_C
       !
       do ij = 1,nnbors          ! <-------- First loop over neighbours j
          j = nborlist(in0+ij)
          j3 = j*3-3

          typeij(ij)=abs(ibuf(j*2))
          iact(ij)=iac(typei,typeij(ij))

          if (iact(ij) /= 1) then
             ! Handle other interaction types
             ! Set fc to zero - all other steps will be ignored
             fc(ij)=zero

             if (iact(ij) == 0) cycle
             if (iact(ij) == -1) then
                write (6,*) 'TERSOFF ERROR: IMPOSSIBLE INTERACTION'
                write (6,*) myproc,i,j,typei,typeij(ij)
                call my_mpi_abort('INTERACTION -1', int(myproc))
             endif
          endif

          xa(1,ij) = buf(j3+1)-buf(i3+1)
          if (xa(1,ij) >=  half) xa(1,ij) = xa(1,ij)-pbc(1)
          if (xa(1,ij) < -half) xa(1,ij) = xa(1,ij)+pbc(1)
          xab(1,ij) = box(1)*xa(1,ij)

          xa(2,ij) = buf(j3+2)-buf(i3+2)
          if (xa(2,ij) >=  half) xa(2,ij) = xa(2,ij)-pbc(2)
          if (xa(2,ij) < -half) xa(2,ij) = xa(2,ij)+pbc(2)
          xab(2,ij) = box(2)*xa(2,ij)

          xa(3,ij) = buf(j3+3)-buf(i3+3)
          if (xa(3,ij) >=  half) xa(3,ij) = xa(3,ij)-pbc(3)
          if (xa(3,ij) < -half) xa(3,ij) = xa(3,ij)+pbc(3)
          xab(3,ij) = box(3)*xa(3,ij)

          ra2 = xab(1,ij)*xab(1,ij)+xab(2,ij)*xab(2,ij)+xab(3,ij)*xab(3,ij)

          ra(ij) = SQRT(ra2)

          if (iact(ij)==2) then
             if (ra(ij) <=rpd) then
                call splinereppot(ra(ij),V,df,1d20,1d20,typei,typeij(ij),fermi,dfermi)
                !print *,'reppot',i,j,ra(ij),df
                V=V*half
                df=df*half/ra(ij)
                Vpair = Vpair+V+V
                ebuf(i)=ebuf(i)+V; ebuf(j)=ebuf(j)+V
                c1 = df*xa(1,ij)
                pbuf(i3+1) = pbuf(i3+1)+c1; pbuf(j3+1) = pbuf(j3+1)-c1
                wxxi(i)=wxxi(i)-c1*xa(1,ij)

                if (calc_vir) then
                   wxyi(i)=wxyi(i)-c1*xa(2,ij)
                   wxzi(i)=wxzi(i)-c1*xa(3,ij)
                endif

                c1 = df*xa(2,ij)
                pbuf(i3+2) = pbuf(i3+2)+c1; pbuf(j3+2) = pbuf(j3+2)-c1
                wyyi(i)=wyyi(i)-c1*xa(2,ij)

                if (calc_vir) then
                   wyxi(i)=wyxi(i)-c1*xa(1,ij)
                   wyzi(i)=wyzi(i)-c1*xa(3,ij)
                endif

                c1 = df*xa(3,ij)
                pbuf(i3+3) = pbuf(i3+3)+c1; pbuf(j3+3) = pbuf(j3+3)-c1
                wzzi(i)=wzzi(i)-c1*xa(3,ij)

                if (calc_vir) then
                   wzxi(i)=wzxi(i)-c1*xa(1,ij)
                   wzyi(i)=wzyi(i)-c1*xa(2,ij)
                endif 

             endif
             cycle
          endif
          if (iact(ij)/=1) cycle

          fc(ij) = zero
          dfc(ij) = zero
          if (ra(ij) <= rpd) then
             if (ra(ij) < rmd) then
                fc(ij)=one
             else
                theta=a*(ra(ij)-trcut)
                s = SIN(theta)
                fc(ij)= half*(one-s)
                ! replaced dFcut= -half*a*COS(theta)
                ! using cos theta =sqrt(1-sin^2 theta)
                dfc(ij)= halfa*SQRT(one-s*s)   
             endif
          endif

          if (fc(ij) /= zero) then
             rai(ij) = one/ra(ij)
             dfcr = dfc(ij)*rai(ij)
             ra2i(ij) = one/ra2

             xab(1,ij) = rai(ij)*xab(1,ij)
             xai(1,ij) = rai(ij)*xa(1,ij)
             dfcxr(1,ij) = dfcr*xa(1,ij)
             xr2(1,ij) = xa(1,ij)*ra2i(ij)

             xab(2,ij) = rai(ij)*xab(2,ij)
             xai(2,ij) = rai(ij)*xa(2,ij)
             dfcxr(2,ij) = dfcr*xa(2,ij)
             xr2(2,ij) = xa(2,ij)*ra2i(ij)

             xab(3,ij) = rai(ij)*xab(3,ij)
             xai(3,ij) = rai(ij)*xa(3,ij)
             dfcxr(3,ij) = dfcr*xa(3,ij)
             xr2(3,ij) = xa(3,ij)*ra2i(ij)

             z(ij) = zero
          endif
       enddo                     ! <-------- End of first loop over neighbours j

       !
       ! Calculate g(theta) and dg(theta) and hence zeta_ij and zeta_ik
       !
       mjk = 0
       do ij = 1,nnbors-1        ! <-------- Second loop over neighbours j

          if (fc(ij) /= zero) then
             j = nborlist(in0+ij)
             do ik = ij+1,nnbors            ! <------- Loop over neighbours k

                if (fc(ik) /= zero) then

                   mjk = mjk + 1
                   cth(mjk) = xab(1,ij)*xab(1,ik)+xab(2,ij)*xab(2,ik)+xab(3,ij)*xab(3,ik)

                   ! Inlined function G_theta
                   ghmctheta = gh - cth(mjk)
                   denomi = one/(gd2+ghmctheta*ghmctheta)
                   gt(mjk) = one+(gd2i - denomi)*gc2
                   dG(mjk) = g2c2*denomi*denomi*ghmctheta

                   ! exp(lambda3...) term
                   dr(mjk)=(ra(ij)-ra(ik))
                   expij(mjk)=one
                   expik(mjk)=one
                   dexpij(mjk)=zero
                   dexpik(mjk)=zero
                   if (rlambda3_cube /= zero) then
                      help1 = rlambda3_cube * dr(mjk) * dr(mjk)
                      help2 = help1 * dr(mjk)
                      expij(mjk) = exp(help2)
                      expik(mjk) = one/expij(mjk)

                      help3=three*help1
                      dexpij(mjk)= expij(mjk)*help3
                      dexpik(mjk)= expik(mjk)*help3
                      !print *,'lambda3',i,j,k,dr(mjk),expij(mjk),expik(mjk),dexpij(mjk),dexpik(mjk)
                   endif

                   z(ij) = z(ij) + fc(ik)*gt(mjk)*expij(mjk)
                   z(ik) = z(ik) + fc(ij)*gt(mjk)*expik(mjk)

                endif
             enddo                          ! <------ End of loop over k
          endif
       enddo                     ! <-------- End of second loop over neighbours j

       !
       !  Calculate f_A, f_R and b_ij
       !
       do ij = 1,nnbors          ! <-------- Third loop over neighbours j
          if (fc(ij) /= zero) then
             j = nborlist(in0+ij)
             j3=j*3-3

             Fatt = -tersB*exp(-rmu*ra(ij))
             Frep = tersA*exp(-rlambda*ra(ij))
             dFatt = -Fatt*rmu
             dFrep = -Frep*rlambda

             bzp = (beta*z(ij))**ters_n
             bzp1 = one+bzp
             bij = bzp1**ters2i

             ! Energies and 'two-body' forces done

             cpair=fc(ij)*Frep
             cmany=fc(ij)*bij*Fatt
             ! Send in V=0 here because we split up cpair and cmany
             !V=cpair+cmany
             V=0;
             df=(fc(ij)*(dFrep+bij*dFatt)+dfc(ij)*(Frep+bij*Fatt))

             fermi=one
             dfermi=one
             if (ra(ij) < reppotcut) then
                !print '(A,4F10.3)','rep0',ra(ij),V,df
                call splinereppot(ra(ij),V,df,bf,rf,typei,typeij(ij),fermi,dfermi)
                !if (ra(ij) < 2.2) print '(A,4F10.3)','rep2',ra(ij),V,df,fermi

                df=df+dfermi*(cpair+cmany)

             endif
             ! reppot routine has output (1.0d0-fermi)*Vrepulsive
             ! so it can be added to Vpair without additional
             ! multiplication with the fermi term.
             cpair=cpair*half*fermi+half*V
             cmany=cmany*fermi
             df=df*half*rai(ij)

             Vpair = Vpair+cpair+cpair
             ebuf(i)=ebuf(i)+cpair
             ebuf(j)=ebuf(j)+cpair
             Vmany = Vmany+cmany
             Ethree(i)=Ethree(i)+cmany

             if (z(ij) /= zero) then
                db(ij) = -quarter*cmany*bzp/(bzp1*z(ij))
             else
                db(ij) = zero
             endif

             ! Get ij derivative, i.e. everything except db terms

             c1 = df*xa(1,ij)
             pbuf(i3+1) = pbuf(i3+1)+c1
             pbuf(j3+1) = pbuf(j3+1)-c1
             wxxi(i)=wxxi(i)-c1*xa(1,ij)

             if (calc_vir) then
                wxyi(i)=wxyi(i)-c1*xa(2,ij)
                wxzi(i)=wxzi(i)-c1*xa(3,ij)
             endif

             c1 = df*xa(2,ij)
             pbuf(i3+2) = pbuf(i3+2)+c1
             pbuf(j3+2) = pbuf(j3+2)-c1
             wyyi(i)=wyyi(i)-c1*xa(2,ij)

             if (calc_vir) then
                wyxi(i)=wyxi(i)-c1*xa(1,ij)
                wyzi(i)=wyzi(i)-c1*xa(3,ij)
             endif

             c1 = df*xa(3,ij)
             pbuf(i3+3) = pbuf(i3+3)+c1
             pbuf(j3+3) = pbuf(j3+3)-c1
             wzzi(i)=wzzi(i)-c1*xa(3,ij)

             if (calc_vir) then
                wzxi(i)=wzxi(i)-c1*xa(1,ij)
                wzyi(i)=wzyi(i)-c1*xa(2,ij)
             endif

          endif
       enddo                     ! <-------- End of third loop over neighbours j

       !
       !  Get three-body db derivatives
       !
       mjk = 0 
       do ij = 1,nnbors-1        ! <-------- Fourth loop over neighbours j
          if (fc(ij) /= zero) then
             j = nborlist(in0+ij)
             j3=j*3-3
!!$ 
             do ik = ij+1,nnbors        ! <---- Loop over neighbours k
                if (fc(ik) /= zero) then
                   k = nborlist(in0+ik)
                   k3=k*3-3
                   mjk = mjk + 1

                   dbfcjk=db(ij)*fc(ik)
                   dbfckj=db(ik)*fc(ij)
                   dbjgt = db(ij)*gt(mjk)*expij(mjk)
                   dbkgt = db(ik)*gt(mjk)*expik(mjk)
                   dbfcdG = (dbfcjk*expij(mjk)+dbfckj*expik(mjk))*dG(mjk)
                   dbfcdGcth = dbfcdG*cth(mjk)
                   dbfcdGrr = dbfcdG*rai(ik)*rai(ij)

                   dbfcgde=(dbfcjk*dexpij(mjk)-dbfckj*dexpik(mjk))*gt(mjk)

                   cj=dbkgt*dfcxr(1,ij)+dbfcdGrr*xa(1,ik)-dbfcdGcth*xr2(1,ij)
                   cj=cj+dbfcgde*xai(1,ij)
                   ck=dbjgt*dfcxr(1,ik)+dbfcdGrr*xa(1,ij)-dbfcdGcth*xr2(1,ik)
                   ck=ck-dbfcgde*xai(1,ik)

                   pbuf(i3+1) = pbuf(i3+1)+cj+ck
                   pbuf(j3+1) = pbuf(j3+1)-cj
                   pbuf(k3+1) = pbuf(k3+1)-ck
                   wxxi(i)=wxxi(i)-cj*xa(1,ij)-ck*xa(1,ik)

                   if (calc_vir) then
                      wxyi(i)=wxyi(i)-cj*xa(2,ij)-ck*xa(2,ik)
                      wxzi(i)=wxzi(i)-cj*xa(3,ij)-ck*xa(3,ik)
                   endif

                   cj=dbkgt*dfcxr(2,ij)+dbfcdGrr*xa(2,ik)-dbfcdGcth*xr2(2,ij)
                   cj=cj+dbfcgde*xai(2,ij)
                   ck=dbjgt*dfcxr(2,ik)+dbfcdGrr*xa(2,ij)-dbfcdGcth*xr2(2,ik)
                   ck=ck-dbfcgde*xai(2,ik)

                   pbuf(i3+2) = pbuf(i3+2)+cj+ck
                   pbuf(j3+2) = pbuf(j3+2)-cj
                   pbuf(k3+2) = pbuf(k3+2)-ck
                   wyyi(i)=wyyi(i)-cj*xa(2,ij)-ck*xa(2,ik)

                   if (calc_vir) then
                      wyxi(i)=wyxi(i)-cj*xa(1,ij)-ck*xa(1,ik)
                      wyzi(i)=wyzi(i)-cj*xa(3,ij)-ck*xa(3,ik)
                   endif

                   cj=dbkgt*dfcxr(3,ij)+dbfcdGrr*xa(3,ik)-dbfcdGcth*xr2(3,ij)
                   cj=cj+dbfcgde*xai(3,ij)
                   ck=dbjgt*dfcxr(3,ik)+dbfcdGrr*xa(3,ij)-dbfcdGcth*xr2(3,ik)
                   ck=ck-dbfcgde*xai(3,ik)

                   pbuf(i3+3) = pbuf(i3+3)+cj+ck
                   pbuf(j3+3) = pbuf(j3+3)-cj
                   pbuf(k3+3) = pbuf(k3+3)-ck
                   wzzi(i)=wzzi(i)-cj*xa(3,ij)-ck*xa(3,ik)

                   if (calc_vir) then
                      wzxi(i)=wzxi(i)-cj*xa(1,ij)-ck*xa(1,ik)
                      wzyi(i)=wzyi(i)-cj*xa(2,ij)-ck*xa(2,ik)
                   endif

                endif

             enddo! <---- End of loop over neighbours k
          endif
       enddo                     ! <-------- Fourth loop over neighbours j    

       wxx=wxx+wxxi(i)
       wyy=wyy+wyyi(i)
       wzz=wzz+wzzi(i)

       if (calc_vir) then
          wxy=wxy+wxyi(i)
          wxz=wxz+wxzi(i)
          wyx=wyx+wyxi(i)
          wyz=wyz+wyzi(i)
          wzx=wzx+wzxi(i)
          wzy=wzy+wzyi(i)
       endif

       dx=buf(i3+1)*box(1)-ECM(1)
       dy=buf(i3+2)*box(1)-ECM(2)
       dz=buf(i3+3)*box(1)-ECM(3)
       r=sqrt(dx*dx+dy*dy+dz*dz)

       nnsh=int(1.0d0+r/maxr*(1.0d0*nsh-1.0d0))
       do j=nsh,nnsh,-1
          wxxsh(j)=wxxsh(j)+wxxi(i)
          wyysh(j)=wyysh(j)+wyyi(i)
          wzzsh(j)=wzzsh(j)+wzzi(i)
          natsh(j)=natsh(j)+1
       enddo

    enddo             ! <-------------------- End of loop over atoms i

    do j = 1,3*myatoms     
       xnp(j) = pbuf(j)
    enddo

    do j = 1,myatoms     
       Epair(j)=half*ebuf(j)
       Ethree(j)=Ethree(j)*half
    enddo


    !
    ! Note: wxx should not be summed over processors here, wxxsh should.
    !      
     
    call my_mpi_dsum(wxxsh, nsh)
    call my_mpi_dsum(wyysh, nsh)
    call my_mpi_dsum(wzzsh, nsh)
     
    call my_mpi_isum(natsh, nsh)
     

    Vpair = Vpair*half
    Vmany = Vmany*half


    ! Passing back stage
    i=0
    IF(debug)PRINT *,'SW pass back',myproc,np0,myatoms
    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       ! print *,'Stilweb loop 2, pbuf sendrecv',myproc
       ! Loop over directions
       do d=1,8
          ! Loop over neighbours
          i=0
          do j=myatoms+1,np0
             dfrom=dirbuf(j)+4
             if (dfrom .gt. 8) dfrom=dfrom-8
             if (d .eq. dfrom) then
                i=i+1
                if (i .ge. NPPASSMAX) call my_mpi_abort('NPPASSMAX overflow2', int(myproc))
                ifrom=ibuf(j*2-1)
                isendbuf(i)=ifrom
                i3=i*3-3
                j3=j*3-3
                psendbuf(i)=ebuf(j)
                xsendbuf(i3+1)=pbuf(j3+1)
                xsendbuf(i3+2)=pbuf(j3+2)
                xsendbuf(i3+3)=pbuf(j3+3)
             endif
          enddo
          ! print *,'send',myproc,d,i,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), &
                  d, mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,ii
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,i
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
          endif
          i=ii
          do ii=1,i
             myi=isendbuf2(ii)
             if (myi .gt. myatoms) then
                print *,myproc,myatoms,myi,ii,i
                call my_mpi_abort('SW i too large', int(myproc))
             endif
             i3=myi*3-3
             ii3=ii*3-3
             Epair(myi)=Epair(myi)+half*psendbuf2(ii)
             xnp(i3+1)=xnp(i3+1)+xsendbuf2(ii3+1)
             xnp(i3+2)=xnp(i3+2)+xsendbuf2(ii3+2)
             xnp(i3+3)=xnp(i3+3)+xsendbuf2(ii3+3)
          enddo
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif

    !print *,'Tersoff Vtot:',Vpair,Vmany

  end subroutine Tersoff_Force



  ! ------------------------------------------------------------------------

  subroutine Init_Ter_Pot(rc,potmode,reppotcutin)

    use TYPEPARAM
    use my_mpi
    use defs

    use para_common

    implicit none

    ! ------------------------------------------------------------------
    ! Variables passed in and out
    real*8 rc,reppotcutin
    integer potmode

    ! Tersoff common blocks

    common /tersoff/tersA,tersB,beta,rlambda,rmu,gc,gd,gh,ters_n,rlambda3_cube
    common /tersoff2/gd2,gd2i,gc2,g2c2
    common /cutoff/trcut,dcut,rmd,rpd,a,halfa
    common /reppot/ reppotcut,bf,rf

    real*8 tersA,tersB,beta,rlambda,rmu,gc,gd,gh,ters_n,rlambda3_cube
    real*8 gd2,gd2i,gc2,g2c2
    real*8 trcut,dcut,rmd,rpd,a,halfa
    real*8 reppotcut,bf,rf

!include 'para_common.f90'

    real*8 rlambda3

    real*8 atersA(6),atersB(6),abeta(6),arlambda(6),armu(6),agc(6),agd(6),agh(6)
    real*8 aters_n(6),arlambda3(6),atrcut(6),adcut(6)
    real*8 areppotcut(6),abf(6),arf(6)
    integer i

    !                Si III      Si II      Ge        Ge        C plain   C mod.
    data atersA     /1.8308d3  , 3.2647d3 , 1.769d3  , 1.769d3  , 0.0d0   , 0.0d0   /
    data atersB     /4.7118d2  , 9.5373d1 , 4.1923d2 , 4.1923d2 , 0.0d0   , 0.0d0   /
    data abeta      /1.1000d-6 , 3.3675d-1, 9.0166d-7, 9.0166d-7, 0.0d0   , 0.0d0   /
    data arlambda   /2.4799d0  , 3.2394d0 , 2.4451d0 , 2.4451d0 , 0.0d0   , 0.0d0   /
    data armu       /1.7322d0  , 1.3258d0 , 1.7047d0 , 1.7047d0 , 0.0d0   , 0.0d0   /
    data agc        /1.0039d5  , 4.8381d0 , 1.0643d5 , 1.0643d5 , 0.0d0   , 0.0d0   /
    data agd        /1.6217d1  , 2.0417d0 , 1.5625d1 , 1.5625d1 , 0.0d0   , 0.0d0   /
    data agh        /-5.9825d-1, 0.0d0    ,-4.3884d-1,-4.3884d-1, 0.0d0   , 0.0d0   /
    data aters_n    /7.8734d-1 , 2.2956d1 , 7.5627d-1, 7.5627d-1, 0.0d0   , 0.0d0   /
    data arlambda3  /1.7322d0  , 1.3258d0 , 1.7047d0 , 1.7047d0 , 0.0d0   , 0.0d0   /
    data atrcut     /2.85d0    , 3.0d0    , 2.95d0   , 2.95d0   , 1.95d0  , 2.13d0  /
    data adcut      /0.15d0    , 0.2d0    , 0.15d0   , 0.15d0   , 0.15d0  , 0.33d0  /

    data areppotcut /3.0d0     , 3.2d0    , 3.1d0   , 3.1d0   , 2.0d0   , 2.0d0   /
    data abf        /12.0d0    , 12.0d0   , 12.0d0  , 12.0d0  , 14.0d0  , 14.0d0  /
    data arf        /1.60d0    , 1.60d0   , 1.66d0  , 1.66d0  , 0.95d0  , 0.95d0  /

    if (element(1)=="Si") then
       i=1
    else if (element(1)=="Ge") then
       i=3
    else 
       print *,'Element',element(1),' not supported in Tersoff yet !!'
       call my_mpi_abort('Tersoff element', int(potmode))
    endif
    if (potmode==7 .or. potmode==8) then
       i=i+1
       if (element(1)=='Si') print *,'Si Tersoff potential II (B)'
       if (element(1)=='Ge') print *,'Ge Tersoff potential'
       if (element(1)=='C') print *,'C Tersoff potential with PRL 77, 699 modified max'
    else
       if (element(1)=='Si') print *,'Si Tersoff potential III (C)'
       if (element(1)=='Ge') print *,'Ge Tersoff potential'
       if (element(1)=='C') print *,'C Tersoff potential, original'
    endif

    tersA=atersA(i)
    tersB=atersB(i)
    beta=abeta(i)
    rlambda=arlambda(i)
    rmu=armu(i)
    gc=agc(i)
    gd=agd(i)
    gh=agh(i)
    ters_n=aters_n(i)
    trcut=atrcut(i)
    dcut=adcut(i)

    reppotcut=areppotcut(i)
    if (reppotcutin < reppotcut) reppotcut=reppotcutin
    bf=abf(i)
    rf=arf(i)

    if (potmode==5 .or. potmode==7) then
       rlambda3=arlambda3(i)
       print *,'Tersoff using lambda3',rlambda3
    else if (potmode==6 .or. potmode==8) then
       rlambda3=zero
       print *,'Tersoff NOT using lambda3'
    endif
    rlambda3_cube=rlambda3*rlambda3*rlambda3

    rc = trcut+dcut

    rpd = trcut+dcut
    rmd = trcut-dcut
    a = half*pi/dcut
    halfa = -half*a

    gd2 = gd*gd
    gd2i = one/gd2
    gc2 = gc*gc
    g2c2 = -two*gc2

  end subroutine Init_Ter_Pot
