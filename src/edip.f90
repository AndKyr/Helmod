
  subroutine EDIP_forces(X,atype,A,atomindex,natoms,box,pbc,            &
       nborlist,Epair,Vpair,Ethree,Vthree,wxx,wyy,wzz,wxxi,wyyi,wzzi,   &
       wxxsh,wyysh,wzzsh,natsh,nsh,                                     &
       wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,	        &
       atpassflag,nngbrproc,ECM,moviemode,                              &
       movietime_val,calc_avgvir,last_step_predict)

    use typeparam
    use datatypes
    use my_mpi

    use edippa
    use defs

    use para_common

    implicit none

    !
    !     In the PARCAS interface:
    !     Input:      X       contains positions in A scaled by 1/box,
    !                 natoms  Total number of atoms over all nodes
    !                 myatoms Number of atoms in my node, in para_common.h
    !                 box(3)  Box size (box centered on 0)
    !                 pbc(3)  Periodics: if = 1.0d0 periodic
    !
    !     Output:     A       contains forces in eV/A scaled by 1/box
    !                 Epair   V_2 per atom
    !                 Vpair   total V_2 of all atoms
    !                 Ethree  V_3 per atom
    !                 Vthree  total V_3 of all atoms; Vmany in mdx.f
    !                  
    !

    ! The atom types in ATYPE(*) and typeparam are used in the following way:
    !     
    ! On firsttime, all type combinations are looped through. Recognized
    ! iac(it,jt)==1 combinations are set to the correct IATAB value.
    ! For others, a warning is issued.

    ! nborlist(i) has following format (not same as original code !):
    !
    !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
    !  nborlist(2)    Index of first neighbour of atom 1
    !  nborlist(3)    Index of second neighbour of atom 1
    !   ...
    !  nborlist(N1+1)  Index of last neighbour of atom 1
    !  nborlist(N1+2)  Number of neighbours for atom 2 
    !  nborlist(N1+3)  Index of first neighbour of atom 2
    !   ...
    !   ...           And so on for all N atoms
    !


    !
    ! ------------------------------------------------------------------------
    !     Variables passed in and out

    real*8 X(*),A(*)
    real*8 box(3),pbc(3)
    integer*4 nborlist(*)
    integer atomindex(*)
    integer natoms,atype(*)

    real*8 Epair(*), Vpair, Ethree(*), Vthree
    real*8 wxx,wyy,wzz,wxxi(*),wyyi(*),wzzi(*)

    real*8 wxy,wxyi(*),wxz,wxzi(*),wyx,wyxi(*),wyz,wyzi(*),wzx,wzxi(*),wzy,wzyi(*)
    integer moviemode
    logical movietime_val
    logical calc_vir
    logical calc_avgvir
    logical last_step_predict

    real*8 wxxsh(*),wyysh(*),wzzsh(*),ECM(3)
    integer natsh(*),nsh

    integer atpassflag(*),nngbrproc(*),potmode

    real*8 dttimer
    !
    !     PARAMETERS FOR edip
    !
    !

    integer l,n
    double precision dx,dy,dz,rsqr,asqr
    double precision rinv,rmainv,xinv,xinv3,den,Z,fZ
    double precision dV2j,dV2ijx,dV2ijy,dV2ijz,pZ,dp
    double precision temp0,temp1,temp2
    double precision Qort,muhalf,u5
    double precision rmbinv,winv,dwinv,tau,dtau,lcos,H,dHdx,dhdl,x_
    double precision dV3rij,dV3rijx,dV3rijy,dV3rijz
    double precision dV3rik,dV3rikx,dV3riky,dV3rikz
    double precision dV3l,dV3ljx,dV3ljy,dV3ljz,dV3lkx,dV3lky,dV3lkz
    double precision dV2dZ,dxdZ,dV3dZ
    double precision dEdrl,dEdrlx,dEdrly,dEdrlz
    double precision bmc,cmbinv
    double precision fjx,fjy,fjz,fkx,fky,fkz

    double precision s2_t0(NNMAX)
    double precision s2_t1(NNMAX)
    double precision s2_t2(NNMAX)
    double precision s2_t3(NNMAX)
    double precision s2_dx(NNMAX)
    double precision s2_dy(NNMAX)
    double precision s2_dz(NNMAX)
    double precision s2_r(NNMAX)
    integer n2                
    !   size of s2[]
    integer num2(NNMAX)  
    !   atom ID numbers for s2[]

    double precision s3_g(NNMAX)
    double precision s3_dg(NNMAX)
    double precision s3_rinv(NNMAX)
    double precision s3_dx(NNMAX)
    double precision s3_dy(NNMAX)
    double precision s3_dz(NNMAX)
    double precision s3_r(NNMAX)

    integer n3                
    !   size of s3[]
    integer num3(NNMAX)  
    !   atom ID numbers for s3[]
    ! Janne Nord: simplified usage of sz_sum: every array element
    ! always got the same value, so there is no need to
    ! have it as an array.


    double precision sz_df(NNMAX)
    double precision sz_sum
    double precision sz_dx(NNMAX)
    double precision sz_dy(NNMAX)
    double precision sz_dz(NNMAX)
    double precision sz_r(NNMAX)
    integer nz                
    !   size of sz[]
    integer numz(NNMAX)  
    !   atom ID numbers for sz[]

    integer nj,nk,nl         
    !   indices for the store arrays

    double precision V2, V3, virial, virial_xyz(3)
    double precision L_x_div_2, L_y_div_2, L_z_div_2
    double precision L_x,L_y,L_z

    double precision coord_total

    integer fixZ 
    integer tricks_Zfix


    real*8 iboxx,iboxy,iboxz
    real*8 boxx,boyy,bozz



    !     lparams
    integer k,neip,indi,indj,indk,indl,indn,typei,nneip,latn,typej  
    real*8 tran,trans
    real*8 fermi,dfermi
    integer NP3,NP32

    !
    !     PARAMETERS FOR INTERFACE AND PARALLELLIZATION
    !
    real*8 maxr,r
    integer i,j,jj,i2,i3,j2,j3,ii,ii3,myi,dfrom,ifrom,nnsh,NP,d
    real*8 t1,dummy1,dummy2,trans2,trans3

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

!include 'para_common.f90'
    !-----------------------------------------------------------------------
    !     PARALLELL INTERFACE
    !-----------------------------------------------------------------------
    !
    !     Parallellization principle:
    !     
    !     1� Copy myatoms atom coordinates*box into buf(1:myatoms*3)
    !     2� Get atom coordinates of neighbouring nodes into rest of buf
    !
    !     3� Calculate forces of myatoms into array pbuf
    !     4� The copy forces of myatoms to array A()
    !
    !     5� Send back accelerations of neighbour atoms to them
    !
    !     By calculating forces of all NP atoms in stage 3�, stage 5�
    !     can be omitted. However, this probably is less efficient except
    !     for really huge simulation cells. This approch requires that the 
    !     neighbour list contains neighbours of the atoms from other nodes 
    !     as well; see mdlinkedlist.f
    !
    !
    !
    !     myatoms is the number of atoms in my node
    !     NP is the total number of atoms, including the passed ones
    !
    !

    !     Stage 1�
    NP=myatoms
    do i=1,myatoms
       i3=i*3-3
       buf(i3+1)=X(i3+1)*box(1)
       buf(i3+2)=X(i3+2)*box(2)
       buf(i3+3)=X(i3+3)*box(3)
    enddo
    do i=1,myatoms
       dirbuf(i)=0
       ibuf(i*2-1)=i
       ibuf(i*2)=atype(i)
    enddo


    !
    !     Stage 2�
    !
    !     Pass and receive atoms to different directions.
    !     The passing of atoms in this piece of code should be identical 
    !     in the neighbour list and force calculations.
    !     
    !     In principle the idea is that the input is the local X(myatoms) 
    !     coordinates, and the output is buf(NP), which contains the 
    !     coordinates of the atoms both in this node and within cut_nei
    !     from this in the adjacent nodes.
    !
    !     The atoms which get passed here must be exactly the
    !     same and in the same order in the force calculations
    !     as in the original neighbour list calculation !
    !
    !     To be able to pass back the P information of j pairs,
    !     we also pass and receive the atom indices and direction information
    !     of the atoms in j pairs.
    !     

    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !        Loop over directions
       do d=1,8
          j=0; jj=0
          do i=1,myatoms
             if (IAND(atpassflag(i),passbit(d)) .ne. 0) then
                j=j+1
                if (j .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow1', int(myproc))
                i3=i*3-3
                j3=j*3-3
                j2=j*2-2
                xsendbuf(j3+1)=X(i3+1)*box(1)
                xsendbuf(j3+2)=X(i3+2)*box(2)
                xsendbuf(j3+3)=X(i3+3)*box(3)
                isendbuf(j2+1)=i
                isendbuf(j2+2)=atype(i)
                !                 print *,d,j,X(i3+1),X(i3+2)
             endif
          enddo
          !print *,'send',myproc,d,j,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(NP*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(NP*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(NP*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(NP*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
          endif
          j=jj
          do i=1,j
             dirbuf(NP+i)=d
          enddo
          NP=NP+j
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif
    if (NP .ne. np0pairtable) then
       write (6,*) 'EAMforces np0 problem ',myproc,NP,np0pairtable
    endif
    !print *,'Node received totally ',myproc,NP-myatoms


    !      
    !     Stage 3�: Calculate actual forces
    !

    !-----------------------------------------------------------------------
    !     FORCE CALCULATION 
    !-----------------------------------------------------------------------

    !
    !     In PARCAS interface, use buf instead of X, pbuf instead of A.
    !     pbuf contains actual accelerations, buf actual coordinates
    !     (NOT in PARCAS internal units, scaled by 1/box)
    !
    !     Take care to calculate pressure, total pot. only for atoms in my node !
    !
    !

    !
    !     RNMEF AND RNLEF ARE THE DIFFERENCES BETWEEN DISTANCE
    !     AND THE POTENTIAL CUTOFF
    !
    !
    !  BOUNDARY CONDITIONS
    !  IF PBC EQ 1.0d0, PERIODIC - NOTE THAT DIFFERENT CONDITIONS MAY BE SPECIFIED
    !                            IN X, Y AND Z DIRECTIONS.
    !
    !   IF       DX > BOX2(1)         XL=2*BOX2(1)
    ! -BOX2(1) < DX < BOX2(1)         XL=0
    !            DX < -BOX2(1)        XL=-2*BOX2(1)
    !  I.E. IF A SEPARATION EXCEEDS HALF THE BOX SIDE, IT IS TREATED AS
    !  COMING FROM THE ADJACENT BOX.
    !
    !  N.B. NO PARTICLES ARE MOVED AT THIS STAGE.
    !
    !
    ! Initialize force calculation stuff 
    !
    L_x       = box(1)
    L_y       = box(2)
    L_z       = box(3)

    L_x_div_2 = L_x*0.5D0
    L_y_div_2 = L_y*0.5D0
    L_z_div_2 = L_z*0.5D0

    fixZ = 0
    tricks_Zfix = 5

    NP3 = NP*3
    NP32 = NP3-2

    do N = 1,NP3
       pbuf(N) = zero
    enddo
    do N=1,NP
       ebuf(N) = zero
    enddo
    do N = 1,myatoms*3
       A(N) = 0.0
    enddo
    do N = 1,myatoms
       Epair(N) = zero
       Ethree(N) = zero
       wxxi(N) = zero
       wyyi(N) = zero
       wzzi(N) = zero
    enddo

    calc_vir = .false.
    if (movietime_val .or. calc_avgvir .or. last_step_predict) then
     if (moviemode == 15 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
      calc_vir = .true.
     endif
    endif

    if (calc_vir) then
       wxy=zero
       wxz=zero
       wyx=zero
       wyz=zero
       wzx=zero
       wzy=zero
       do N = 1,myatoms
          wxyi(N) = zero
          wxzi(N) = zero
          wyxi(N) = zero
          wyzi(N) = zero
          wzxi(N) = zero
          wzyi(N) = zero
       enddo
    endif

    Vpair=0.0
    Vthree=0.0
    wxx=0.0
    wyy=0.0
    wzz=0.0
    do j=1,nsh
       wxxsh(j)=zero
       wyysh(j)=zero
       wzzsh(j)=zero
       natsh(j)=0
    enddo
    maxr=sqrt(box(1)*box(1)+box(2)*box(2)+box(3)*box(3))/2.0  
    !     CALCULATE ACCELERATIONS USING NEIGHBOUR LIST
    !     --------------------------------------------
    !

    coord_total=0.0
    !      virial=0.0

    !     write (6,*) 'EDIP force'


    !   COMBINE COEFFICIENTS

    asqr = par_a*par_a
    Qort = sqrt(par_Qo)
    muhalf = par_mu*0.5D0
    u5 = u2*u4
    bmc = par_b-par_c
    cmbinv = 1.0D0/(par_c-par_b)

    iboxx=1/box(1)
    iboxy=1/box(2)
    iboxz=1/box(3)

    boxx=iboxx*iboxx
    boyy=iboxy*iboxy
    bozz=iboxz*iboxz

    !  --- LEVEL 1: OUTER LOOP OVER ATOMS ---

    neip=0

    do i=1, myatoms

       !   RESET COORDINATION AND NEIGHBOR NUMBERS

       Z = 0.0
       n2 = 1
       n3 = 1
       nz = 1        



       !  --- LEVEL 2: LOOP PREPASS OVER PAIRS ---

       neip  = neip+1
       indi  = i*3-2
       typei = abs(ibuf(i*2))
       nneip = nborlist(neip)
       !            neip  = neip+1

       do latn=1, nneip
          neip=neip+1
          j=nborlist(neip)
          typej = abs(ibuf(j*2))
          if(iac(typei,typej)==0) cycle
          if(iac(typei,typej)==-1) then
             write (6,*) 'ERROR: IMPOSSIBLE INTERACTION'
             call my_mpi_abort('INTERACTION -1', int(myproc))
          endif

          indj=j*3-2    
          if (indi==indj) cycle

          dx=buf(indj)-buf(indi)
          if(pbc(1)==1.0d0) then         
             if (dx .gt. L_x_div_2) then
                dx = dx - L_x
             else if (dx .lt. -L_x_div_2) then
                dx = dx + L_x
             end if
          end if ! periodic x

          !  dx periodic

          if(dabs(dx) < par_a) then
             dy = buf(indj+1)-buf(indi+1)
             if(pbc(2)==1.0d0) then 
                if (dy .gt. L_y_div_2) then
                   dy = dy - L_y
                else if (dy .lt. -L_y_div_2) then
                   dy = dy + L_y
                end if
             end if
             !  dy periodic
             if(dabs(dy) < par_a) then
                dz = buf(indj+2)-buf(indi+2)
                if(pbc(3)==1.0d0) then
                   if (dz .gt. L_z_div_2) then
                      dz = dz - L_z
                   else if (dz .lt. -L_z_div_2) then
                      dz = dz + L_z
                   end if
                end if
                !  dy periodic
                if(dabs(dz) < par_a) then
                   rsqr = dx*dx + dy*dy + dz*dz
                   if(rsqr < asqr) then
                      r = sqrt(rsqr)


                      if(iac(typei,typej)==2) then
                         call splinereppot(r,tran,trans,1d20,1d20,typei,typej,dummy1,dummy2)
                         tran=tran*0.5
                         trans=trans*0.5
                         VPair=VPair+tran
                         tran=tran*0.5
                         Epair(i)=Epair(i)+tran
                         Epair(j)=Epair(j)+tran
                         dummy1=trans/r
                         dummy2=dx*dummy1
                         pbuf(indi)=pbuf(indi)+dummy2
                         pbuf(indj)=pbuf(indj)-dummy2
                         wxxi(i)=wxxi(i)+dummy2*dx

                         if (calc_vir) then
                            wxyi(i)=wxyi(i)+dummy2*dy
                            wxzi(i)=wxzi(i)+dummy2*dz
                         endif

                         dummy2=dy*dummy1
                         pbuf(indi+1)=pbuf(indi+1)+dummy2
                         pbuf(indj+1)=pbuf(indj+1)-dummy2
                         wyyi(i)=wyyi(i)+dummy2*dy

                         if (calc_vir) then
                            wyxi(i)=wyxi(i)+dummy2*dx
                            wyzi(i)=wyzi(i)+dummy2*dz
                         endif

                         dummy2=dz*dummy1
                         pbuf(indi+2)=pbuf(indi+2)+dummy2
                         pbuf(indj+2)=pbuf(indj+2)-dummy2
                         wzzi(i)=wzzi(i)+dummy2*dz

                         if (calc_vir) then
                            wzxi(i)=wzxi(i)+dummy2*dx
                            wzyi(i)=wzyi(i)+dummy2*dy
                         endif

                         cycle
                      endif

                      !   PARTS OF TWO-BODY INTERACTION r<par_a

                      num2(n2) = j
                      rinv = 1.0d0/r
                      dx = dx * rinv
                      dy = dy * rinv
                      dz = dz * rinv
                      rmainv = 1.0d0/(r-par_a)
                      s2_t0(n2) = par_cap_A*dexp(par_sig*rmainv)
                      s2_t1(n2) = (par_cap_B*rinv)**par_rh
                      s2_t2(n2) = par_rh*rinv
                      s2_t3(n2) = par_sig*rmainv*rmainv
                      s2_dx(n2) = dx
                      s2_dy(n2) = dy
                      s2_dz(n2) = dz
                      s2_r(n2) = r
                      n2 = n2 + 1

                      !   RADIAL PARTS OF THREE-BODY INTERACTION r<par_b

                      if(r < par_bg)  then

                         num3(n3) = j
                         rmbinv = 1.0d0/(r-par_bg)
                         temp1 = par_gam*rmbinv
                         temp0 = dexp(temp1)
                         s3_g(n3) = temp0
                         s3_dg(n3) = -rmbinv*temp1*temp0
                         s3_dx(n3) = dx
                         s3_dy(n3) = dy
                         s3_dz(n3) = dz
                         s3_rinv(n3) = rinv
                         s3_r(n3) = r
                         n3 = n3 + 1            

                         if(fixZ .eq. 0) then


                            !   COORDINATION AND NEIGHBOR FUNCTION par_c<r<par_b

                            if(r .lt. par_b) then
                               if(r .lt. par_c) then
                                  Z = Z + 1.0d0
                               else
                                  xinv = bmc/(r-par_c)
                                  xinv3 = xinv*xinv*xinv
                                  den = 1.0d0/(1 - xinv3)
                                  temp1 = par_alp*den
                                  fZ = dexp(temp1)
                                  Z = Z + fZ
                                  numz(nz) = j
                                  sz_df(nz) = fZ*temp1*den*3.0*xinv3*xinv*cmbinv   
                                  !   df/dr
                                  sz_dx(nz) = dx
                                  sz_dy(nz) = dy
                                  sz_dz(nz) = dz
                                  sz_r(nz) = r
                                  nz = nz + 1
                               end if
                               !  r < par_C
                            end if
                            !  r < par_b
                         end if
                         !  fixZ .eq. 0
                      end if
                      !  r < par_bg
                   end if
                   !  rsqr < asqr
                end if
                !  dz < par_a
             end if
             !  dy < par_a
          end if
          !  dz < par_a
       end do

       if(fixZ .ne. 0) then

          Z = tricks_Zfix
          coord_total = coord_total + Z
          pZ = par_palp*dexp(-par_bet*Z*Z)
          dp = 0.0

       else

          coord_total = coord_total + Z       


          !   ZERO ACCUMULATION ARRAY FOR ENVIRONMENT FORCES


          sz_sum=0.0



          !   ENVIRONMENT-DEPENDENCE OF PAIR INTERACTION

          temp0 = par_bet*Z
          pZ = par_palp*dexp(-temp0*Z)         
          !   bond order
          dp = -2.0*temp0*pZ         
          !   derivative of bond order

       end if


       !  --- LEVEL 2: LOOP FOR PAIR INTERACTIONS ---

       do nj=1, n2-1

          temp0 = s2_t1(nj) - pZ
          j = num2(nj)
          indj = j*3-2
          typej = abs(ibuf(j+j))        

          !   two-body energy V2(rij,Z) 
          trans = temp0*s2_t0(nj)
          dV2j = - (s2_t0(nj)) * ((s2_t1(nj))*(s2_t2(nj)) + temp0 * (s2_t3(nj)))   

          fermi=one
          dfermi=one

          if (s2_r(nj) < reppotcut) then
             trans2=0
             trans3=0
             call splinereppot(s2_r(nj),trans2,trans3,14.0d0,1.50d0,typei,typej,fermi,dfermi)
             dV2j =dV2j *fermi+0.5*trans3+dfermi*trans
             trans=trans*fermi+0.5*trans2
          endif

          Vpair = Vpair + trans
          Epair(i) = Epair(i) + trans
          !           Epair(j) = Epair(j) + trans


          !   two-body forces

          !   dV2/dr
          dV2ijx = dV2j * s2_dx(nj)
          dV2ijy = dV2j * s2_dy(nj)
          dV2ijz = dV2j * s2_dz(nj)
          pbuf(indi)   = pbuf(indi) + dV2ijx
          pbuf(indi+1) = pbuf(indi+1) + dV2ijy
          pbuf(indi+2) = pbuf(indi+2) + dV2ijz

          pbuf(indj)   = pbuf(indj)   - dV2ijx
          pbuf(indj+1) = pbuf(indj+1) - dV2ijy
          pbuf(indj+2) = pbuf(indj+2) - dV2ijz


          !   dV2/dr contribution to virial

          wxxi(i) = wxxi(i) - s2_r(nj)*(dV2ijx*s2_dx(nj))
          wyyi(i) = wyyi(i) - s2_r(nj)*(dV2ijy*s2_dy(nj))
          wzzi(i) = wzzi(i) - s2_r(nj)*(dV2ijz*s2_dz(nj))
          !              virial = virial - s2_r(nj) * (dV2ijx*s2_dx(nj)+ dV2ijy*s2_dy(nj) + dV2ijz*s2_dz(nj))

          if (calc_vir) then
             wxyi(i)=wxyi(i) - s2_r(nj)*(dV2ijx*s2_dy(nj))
             wxzi(i)=wxzi(i) - s2_r(nj)*(dV2ijx*s2_dz(nj))
             wyxi(i)=wyxi(i) - s2_r(nj)*(dV2ijy*s2_dx(nj))
             wyzi(i)=wyzi(i) - s2_r(nj)*(dV2ijy*s2_dz(nj))
             wzxi(i)=wzxi(i) - s2_r(nj)*(dV2ijz*s2_dx(nj))
             wzyi(i)=wzyi(i) - s2_r(nj)*(dV2ijz*s2_dy(nj))
          endif

          if(fixZ .eq. 0) then


             !  --- LEVEL 3: LOOP FOR PAIR COORDINATION FORCES ---

             dV2dZ = - dp * s2_t0(nj)

             sz_sum =  sz_sum + dV2dZ


          end if
          !  fixZ
       end do

       if(fixZ .ne. 0) then
          winv = Qort*dexp(-muhalf*Z)
          dwinv = 0.0
          temp0 = dexp(-u4*Z)
          tau = u1+u2*temp0*(u3-temp0)
          dtau = 0.0
       else


          !   COORDINATION-DEPENDENCE OF THREE-BODY INTERACTION

          winv = Qort*exp(-muhalf*Z) 
          !   inverse width of angular function
          dwinv = -muhalf*winv       
          !   its derivative
          temp0 = exp(-u4*Z)
          tau = u1+u2*temp0*(u3-temp0) 
          !   -cosine of angular minimum
          dtau = u5*temp0*(2*temp0-u3) 
          !   its derivative
       end if



       !  --- LEVEL 2: FIRST LOOP FOR THREE-BODY INTERACTIONS ---

       do nj=1, n3-2
          j = num3(nj)
          indj=j*3-2

          !  --- LEVEL 3: SECOND LOOP FOR THREE-BODY INTERACTIONS ---

          do nk=nj+1, n3-1
             k = num3(nk)
             indk=k*3-2
             !   angular function h(l,Z)

             lcos = s3_dx(nj) * s3_dx(nk) + s3_dy(nj) * s3_dy(nk)+ s3_dz(nj) * s3_dz(nk)
             x_ = (lcos + tau)*winv
             temp0 = exp(-x_*x_)

             H = par_lam*(1 - temp0 + par_eta*x_*x_)
             dHdx = 2*par_lam*x_*(temp0 + par_eta)

             dhdl = dHdx*winv


             !   three-body energy

             temp1 = s3_g(nj) * s3_g(nk)
             trans = temp1*H
             Ethree(i)=Ethree(i)+trans
             !          Ethree(j)=Ethree(j)+trans
             !          Ethree(k)=Ethree(k)+trans
             VThree=Vthree+trans


             !   (-) radial force on atom j

             dV3rij = s3_dg(nj) * s3_g(nk) * H
             dV3rijx = dV3rij * s3_dx(nj)
             dV3rijy = dV3rij * s3_dy(nj)
             dV3rijz = dV3rij * s3_dz(nj)
             fjx = dV3rijx
             fjy = dV3rijy
             fjz = dV3rijz


             !   (-) radial force on atom k

             dV3rik = s3_g(nj) * s3_dg(nk) * H
             dV3rikx = dV3rik * s3_dx(nk)
             dV3riky = dV3rik * s3_dy(nk)
             dV3rikz = dV3rik * s3_dz(nk)
             fkx = dV3rikx
             fky = dV3riky
             fkz = dV3rikz


             !   (-) angular force on j

             dV3l = temp1*dhdl
             dV3ljx = dV3l * (s3_dx(nk) - lcos * s3_dx(nj)) * s3_rinv(nj)
             dV3ljy = dV3l * (s3_dy(nk) - lcos * s3_dy(nj)) * s3_rinv(nj)
             dV3ljz = dV3l * (s3_dz(nk) - lcos * s3_dz(nj)) * s3_rinv(nj)
             fjx = fjx + dV3ljx
             fjy = fjy + dV3ljy
             fjz = fjz + dV3ljz


             !   (-) angular force on k

             dV3lkx = dV3l * (s3_dx(nj) - lcos * s3_dx(nk)) * s3_rinv(nk)
             dV3lky = dV3l * (s3_dy(nj) - lcos * s3_dy(nk)) * s3_rinv(nk)
             dV3lkz = dV3l * (s3_dz(nj) - lcos * s3_dz(nk)) * s3_rinv(nk)
             fkx = fkx + dV3lkx
             fky = fky + dV3lky
             fkz = fkz + dV3lkz



             !   apply radial + angular forces to i, j, k

             pbuf(indj)   = pbuf(indj) - fjx
             pbuf(indj+1) = pbuf(indj+1) - fjy
             pbuf(indj+2) = pbuf(indj+2) - fjz
             pbuf(indk)   = pbuf(indk) - fkx
             pbuf(indk+1) = pbuf(indk+1) - fky
             pbuf(indk+2) = pbuf(indk+2) - fkz
             pbuf(indi)   = pbuf(indi) + fjx + fkx
             pbuf(indi+1) = pbuf(indi+1) + fjy + fky
             pbuf(indi+2) = pbuf(indi+2) + fjz + fkz

             !   dV3/dR contributions to virial

             !           virial = virial - s3_r(nj) * (fjx*s3_dx(nj)  
             !     &                       + fjy*s3_dy(nj) + fjz*s3_dz(nj))
             !           virial = virial - s3_r(nk) * (fkx*s3_dx(nk)  
             !     &                       + fky*s3_dy(nk) + fkz*s3_dz(nk))

             wxxi(i) = wxxi(i) - s3_r(nj)*(fjx*s3_dx(nj))
             wyyi(i) = wyyi(i) - s3_r(nj)*(fjy*s3_dy(nj))
             wzzi(i) = wzzi(i) - s3_r(nj)*(fjz*s3_dz(nj))
             wxxi(i) = wxxi(i) - s3_r(nk)*(fkx*s3_dx(nk))
             wyyi(i) = wyyi(i) - s3_r(nk)*(fky*s3_dy(nk))
             wzzi(i) = wzzi(i) - s3_r(nk)*(fkz*s3_dz(nk))

             if (calc_vir) then
                wxyi(i)=wxyi(i) - s3_r(nj)*(fjx*s3_dy(nj))
                wxzi(i)=wxzi(i) - s3_r(nj)*(fjx*s3_dz(nj))
                wyxi(i)=wyxi(i) - s3_r(nj)*(fjy*s3_dx(nj))
                wyzi(i)=wyzi(i) - s3_r(nj)*(fjy*s3_dz(nj))
                wzxi(i)=wzxi(i) - s3_r(nj)*(fjz*s3_dx(nj))
                wzyi(i)=wzyi(i) - s3_r(nj)*(fjz*s3_dy(nj))

                wxyi(i)=wxyi(i) - s3_r(nk)*(fkx*s3_dy(nk))
                wxzi(i)=wxzi(i) - s3_r(nk)*(fkx*s3_dz(nk))
                wyxi(i)=wyxi(i) - s3_r(nk)*(fky*s3_dx(nk))
                wyzi(i)=wyzi(i) - s3_r(nk)*(fky*s3_dz(nk))
                wzxi(i)=wzxi(i) - s3_r(nk)*(fkz*s3_dx(nk))
                wzyi(i)=wzyi(i) - s3_r(nk)*(fkz*s3_dy(nk))
             endif

             if(fixZ .eq. 0) then


                !   prefactor for 4-body forces from coordination
                dxdZ = dwinv*(lcos + tau) + winv*dtau
                dV3dZ = temp1*dHdx*dxdZ


                !  --- LEVEL 4: LOOP FOR THREE-BODY COORDINATION FORCES ---


                sz_sum = sz_sum + dV3dZ

             end if
          end do
       end do

       if(fixZ .eq. 0) then
          !  --- LEVEL 2: LOOP TO APPLY COORDINATION FORCES ---

          do nl=1, nz-1

             dEdrl = sz_sum * sz_df(nl)
             dEdrlx = dEdrl * sz_dx(nl)
             dEdrly = dEdrl * sz_dy(nl)
             dEdrlz = dEdrl * sz_dz(nl)
             pbuf(indi)   = pbuf(indi) + dEdrlx
             pbuf(indi+1) = pbuf(indi+1) + dEdrly
             pbuf(indi+2) = pbuf(indi+2) + dEdrlz
             l = numz(nl)
             indl = l*3-2
             pbuf(indl)   = pbuf(indl) - dEdrlx
             pbuf(indl+1) = pbuf(indl+1) - dEdrly
             pbuf(indl+2) = pbuf(indl+2) - dEdrlz


             !   dE/dZ*dZ/dr contribution to virial

             !          virial = virial - sz_r(nl) * (dEdrlx*sz_dx(nl)+ dEdrly*sz_dy(nl) + dEdrlz*sz_dz(nl))
             wxxi(i) = wxxi(i) - sz_r(nl)*(dEdrlx*sz_dx(nl))
             wyyi(i) = wyyi(i) - sz_r(nl)*(dEdrly*sz_dy(nl))
             wzzi(i) = wzzi(i) - sz_r(nl)*(dEdrlz*sz_dz(nl))

             if (calc_vir) then
                wxyi(i)=wxyi(i) - sz_r(nl)*(dEdrlx*sz_dy(nl))
                wxzi(i)=wxzi(i) - sz_r(nl)*(dEdrlx*sz_dz(nl))
                wyxi(i)=wyxi(i) - sz_r(nl)*(dEdrly*sz_dx(nl))
                wyzi(i)=wyzi(i) - sz_r(nl)*(dEdrly*sz_dz(nl))
                wzxi(i)=wzxi(i) - sz_r(nl)*(dEdrlz*sz_dx(nl))
                wzyi(i)=wzyi(i) - sz_r(nl)*(dEdrlz*sz_dy(nl))
             endif

          end do

          !          dx=sz_sum(1)
          !          do nl=1, nz-1
          !             if(sz_sum(nl) .ne. dx) then
          !             WRITE (6,*) 'No OK sitten...'
          !             endif
          !          enddo


       end if

       wxxi(i)=wxxi(i)*boxx
       wyyi(i)=wyyi(i)*boyy
       wzzi(i)=wzzi(i)*bozz

       if (calc_vir) then
          wxyi(i)=wzxi(i)/(box(1)*box(2))
          wxzi(i)=wxzi(i)/(box(1)*box(3))
          wyxi(i)=wyxi(i)/(box(2)*box(1))
          wyzi(i)=wyzi(i)/(box(2)*box(3))
          wzxi(i)=wzxi(i)/(box(3)*box(1))
          wzyi(i)=wzyi(i)/(box(3)*box(2))
       endif

       wxx=wxx+wxxi(i)
       wyy=wyy+wyyi(i)
       wzz=wzz+wzzi(i)

       if (calc_vir) then
          wxy=wxy+wxyi(i)
          wxz=wxz+wxzi(i)
          wyx=wyx+wyxi(i)
          wyz=wyz+wyzi(i)
          wzx=wzx+wzxi(i)
          wzy=wzy+wzyi(i)
       endif


       dx=buf(indi+0)-ECM(1)*iboxx
       dy=buf(indi+1)-ECM(2)*iboxy
       dz=buf(indi+2)-ECM(3)*iboxz
       r=sqrt(dx*dx+dy*dy+dz*dz)

       nnsh=int(1.0d0+r/maxr*(1.0d0*nsh-1.0d0))
       do j=nsh,nnsh,-1
          wxxsh(j)=wxxsh(j)+wxxi(i)
          wyysh(j)=wyysh(j)+wyyi(i)
          wzzsh(j)=wzzsh(j)+wzzi(i)
          natsh(j)=natsh(j)+1
       enddo

    end do  ! End of loop over atoms i

    !          virial = virial / 3.0
    !**********************************************************************



    !       
    !     Note: wxx should not be summed over processors here, wxxsh should.
    !      
     
    call my_mpi_dsum(wxxsh, nsh)
    call my_mpi_dsum(wyysh, nsh)
    call my_mpi_dsum(wzzsh, nsh)
     
    call my_mpi_isum(natsh, nsh)
     
    !
    !     Stage 4�: Copy my forces to array A()
    !
    !     Also scale A to correspond to PARCAS xnp array units
    !

    n3 = 1
    do N = 1,myatoms
       !        Epair(N)=ebuf(N)
       A(n3) = pbuf(n3)*iboxx
       n3=n3+1
       A(n3) = pbuf(n3)*iboxy
       n3=n3+1
       A(n3) = pbuf(n3)*iboxz
       n3=n3+1
    enddo


    !-----------------------------------------------------------------------
    !     PASSING BACK STAGE
    !-----------------------------------------------------------------------

    !
    !     Stage 5�: Pass back accelerations of neighbour atoms to them
    !

    !
    !  From the array dirbuf, we can figure out from which direction an
    !  atom has been received, and from ibuf what index it has had there. Thus
    !  we can send the pbuf and atom index information to other nodes,
    !  and when receiving it know which atom it belongs to.
    !
    i=0
    IF(debug)PRINT *,'edip pass back',myproc,NP,myatoms
    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !        print *,'loop 2, pbuf sendrecv',myproc
       !        Loop over directions
       do d=1,8
          !           Loop over neighbours
          i=0
          do j=myatoms+1,NP
             dfrom=dirbuf(j)+4
             if (dfrom .gt. 8) dfrom=dfrom-8
             if (d .eq. dfrom) then
                i=i+1
                if (i .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow2', int(myproc))
                ifrom=ibuf(j*2-1)
                isendbuf(i)=ifrom
                i3=i*3-3
                j3=j*3-3
                psendbuf(i)=ebuf(j)
                xsendbuf(i3+1)=pbuf(j3+1)
                xsendbuf(i3+2)=pbuf(j3+2)
                xsendbuf(i3+3)=pbuf(j3+3)
             endif
          enddo
          !           print *,'send',myproc,d,i,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !              print *,'received',myproc,d,ii
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !              print *,'received',myproc,d,i
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
          endif
          i=ii
          do ii=1,i
             myi=isendbuf2(ii)
             if (myi .gt. myatoms) then
                print *,myproc,myatoms,myi,ii,i
                call my_mpi_abort('EDIP i too large', int(myproc))
             endif
             i3=myi*3-3
             ii3=ii*3-3
             Epair(myi)=Epair(myi)+psendbuf2(ii)
             A(i3+1)=A(i3+1)+xsendbuf2(ii3+1)/box(1)
             A(i3+2)=A(i3+2)+xsendbuf2(ii3+2)/box(2)
             A(i3+3)=A(i3+3)+xsendbuf2(ii3+3)/box(3)
          enddo
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif



    !-----------------------------------------------------------------------
    !     END OF FORCE CALCULATION
    !-----------------------------------------------------------------------


    !      if (subcmacc) then
    !     
    !     SUBTRACT THE CENTRE OF MASS ACCELERATION
    !     
    !         ACM(1) = 0.0
    !         ACM(2) = 0.0
    !         ACM(3) = 0.0
    !         IATN=0
    !         DO N = 1,myatoms*3-2,3
    !            IATN=IATN+1
    !            ACM(1) = ACM(1) + A(N)
    !            ACM(2) = ACM(2) + A(N+1)
    !            ACM(3) = ACM(3) + A(N+2)
    !         ENDDO
    !     WRITE(6,*)'A',AXR,AYT,AZT
    !         CALL my_mpi_dsum(ACM, 3)
    !         
    !         IATN=0
    !         DO N = 1,myatoms*3-2,3
    !            IATN=IATN+1
    !            A(N) = A(N) - ACM(1)/natoms
    !            A(N+1) = A(N+1) - ACM(2)/natoms
    !            A(N+2) = A(N+2) - ACM(3)/natoms
    !         ENDDO
    !      endif


    !      firsttime=.false.

    return

  end subroutine EDIP_forces

  !-----------------------------------------------------------------------------


  subroutine Init_Edip(reppotcutin)
    use my_mpi
    use typeparam
    use edippa
  use defs  
    implicit none

    integer i,j
    real *8 reppotcutin

    par_cap_A = 5.6714030

    write (6,*) 'EDIP init'

    par_cap_B = 2.0002804
    par_rh    = 1.2085196
    par_a     = 3.1213820
    par_sig   = 0.5774108
    par_lam   = 1.4533109
    par_gam   = 1.1247945
    par_b     = 3.1213820
    par_c     = 2.5609104
    par_mu    = 0.6966326
    par_Qo    = 312.1341346
    par_palp  = 1.4074424   
    par_bet   = 0.0070975
    par_alp   = 3.1083847



    u1 = -0.165799
    u2 = 32.557
    u3 = 0.286198
    u4 = 0.66 

    par_delta =78.7590539
    par_eta = par_delta/par_Qo

    delta_safe = 0.2
    pot_cutoff = par_a
    par_bg=par_a

    do i=itypelow,itypehigh
       do j=itypelow,itypehigh
          if (iac(i,j) == 1) then
             if (element(i).ne.'Si') then
                call my_mpi_abort('Invalid atom type for EDIP', int(i))
             end if
             reppotcut=pot_cutoff
             if(reppotcutin<pot_cutoff) reppotcut=reppotcutin
          end if
          rcut(i,j)=pot_cutoff
       enddo
    enddo

  end subroutine Init_Edip
  !-----------------------------------------------------------------------------
