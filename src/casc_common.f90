
!     ------------------------------------------------------------------
! KN  COMMON parameters for ALCMD - UI
!     ------------------------------------------------------------------
!          Parameters for recoil atom

module casc_common
!     These are read in
      REAL*8 recen,recenmax,rectheta,recphi
      INTEGER irec,iatrec,irecproc,irecstartt,recatype,recatypem
      REAL*8 xrec,yrec,zrec

!     These are just used otherwise
      REAL*8 recv

!      COMMON /REC/ recen,rectheta,recphi,recv,xrec,yrec,zrec,irec,         &
!  &             iatrec,irecproc,irecstartt,recatype,recatypem
      
end module casc_common
