module symbolic_constants
    implicit none

    integer, parameter :: IREC_NO_RECOIL = 0
    integer, parameter :: IREC_ADD_ATOM  = -2
    integer, parameter :: IREC_NEAREST   = -1


end module symbolic_constants


module typeparam
  implicit none
  save
  ! Atom type dependent constants
  ! Allocated after ntypes is determined in read_params
  ! Note: atom type dependent _variables_ (like delta) are
  !  not part of module block

  integer ntype,itypelow,itypehigh
  character*8, allocatable :: element(:)
  character*8 :: substrate

  real*8, allocatable :: mass(:),timeunit(:),vunit(:),aunit(:)
  real*8, allocatable :: atomZ(:) ! This is only needed for elstop straggling calculations
  real*8, allocatable :: rcut(:,:),rcutin(:,:)
  integer, allocatable :: iac(:,:),noftype(:)
  real*8, allocatable :: ionization_potential(:), work_function(:) ! SP, for evaporation

end module typeparam

!some parameters related to datatype sizes
module datatypes
  implicit none

  public
  integer, parameter ::  int32b = SELECTED_INT_KIND(9)
  integer, parameter :: real32b = SELECTED_REAL_KIND(6, 30)
  integer, parameter ::  int64b = SELECTED_INT_KIND(12)
  integer, parameter :: real64b = SELECTED_REAL_KIND(15, 307)
end module datatypes


module basis
  implicit none
  save

  ! Read in atom basis for creating lattice for latflag=5

  integer nreadbasis
  real*8 readoffset(3)
  real*8 readbasis(128,3)
  integer readtype(128)

  real*8 changeprob(128)
  integer changeto(128)

end module basis

module edippa
  implicit none
  save

  real *8 par_cap_A,par_cap_B, par_rh, par_a, par_sig
  real *8 par_lam, par_gam, par_b, par_c, par_delta
  real *8 par_mu, par_Qo, par_palp, par_bet, par_alp
  real *8 delta_safe,pot_cutoff,par_eta,par_bg
  real *8 u1,u2,u3,u4
  real *8 reppotcut
end module edippa


! *** physical constants, conversion factors ***
! Module by Paul Ehrhart/Yinon Ashkenazy

module PhysConsts
  implicit none
  ! Boltzmann constant in eV/K and its inverse (source: NIST)
  real*8, parameter :: kBeV = 8.61734215d-5
  real*8, parameter :: invkBeV = 11604.506153d0   
  ! Boltzmann constant in J/K
  real*8, parameter :: kB = 1.380662d-23
  ! the electron charge (source: NIST)
  real*8, parameter :: e = 1.602176462d-19        
  ! the atomic mass unit (source: NIST)
  real*8, parameter :: u = 1.66054873d-27         
  ! conversion factors for calculation of pressures from virials
  ! 1 Pa = 1 N/m^2 = 1 J/m^3 = 10^-30/e eV/A^3
  ! or 1 eV/A^3 = e*10^30 Pa and finally 1 kbar = 10^8 Pa
  real*8, parameter :: eV_to_kbar = e/1d-30/1d8
  real*8, parameter :: kbar_to_eV = 1.0d0/eV_to_kbar
end module PhysConsts

!************************************************************
! this module holds all values related to the temperature-
! time program, written by Paul Ehrhart for parcas V3.70
!************************************************************
module Temp_Time_Prog
  implicit none
  save
  ! TT_maxnsteps = maximum number of steps allowed in
  !                temperature-time-program
  ! TT_activated = true if temperature-time program is activated
  ! TT_mtemp     = temperature control modes (--> mtemp)
  ! TT_time      = initial time for each step
  ! TT_temp      = target temperatures (--> temp)
  ! TT_trate     = temperature quenching rates (--> trate)
  integer,parameter :: TT_maxnsteps=6
  logical :: TT_activated,TT_tempnewset=.false.
  integer :: TT_step,TT_mtemp(TT_maxnsteps)
  real*8  :: TT_time(TT_maxnsteps)
  real*8  :: TT_temp(TT_maxnsteps),TT_trate(TT_maxnsteps)
end module Temp_Time_Prog

module silicaparam
  implicit none
  ! Parameters for Stillinger-Weber and its modifications
  ! Added by J. Samela 2005
  integer, parameter :: NSW=5
  integer, dimension(0:NSW) :: element_numbers
  real*8, dimension(NSW,NSW) :: paraA, paraB, paraC, paraD, parap, paraq
  real*8, dimension(NSW,NSW) :: paraR, paraEps, paraSigma, paraBigR, paraSigaP
  real*8, dimension(NSW,NSW,NSW) :: ParaSigaT1, ParaSigaT2
  real*8, dimension(NSW,NSW,NSW) :: paragamma1, paragamma2
  real*8, dimension(NSW,NSW,NSW) :: paracostheta, paraAlpha, paralambda
end module silicaparam
