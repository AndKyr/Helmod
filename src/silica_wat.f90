!-------------------------------------------------------------------------------
!MODIFIED VERSION TO INCLUDE Ge BY SCALING Si VALUES; Olli Pakarinen 02/08
! wata - Watanabe potential for silica
!
! Usage
!   Use delta=0.01 or smaller value.
!
! Description
!   Implements the silica potential proposed by Watanabe et al. in
!   Appl. Surf. Sci. 234 (2004) 207-213. Note that there is a wrong 
!   bond softening function in the paper and some wrong parameters, too.
!   The correct function and parameters received by e-mail 
!   from Dr. Watanabe.
!
!   Fortan90 mmodule structure used instead of plain old subroutines, because it 
!   ensures correct delivery of array data from mdx.f90 to this module.
!
!   The code is optimized for speed, not for readability.
!        
! History
!   prior 1997     Code originates from ancient HARWELL code
!   Jan 1997       Modified for PARCAS SW routine by Kai Nordlund
!   Aug 2006       Rewritten for silica by Juha Samela
!   Feb 2008>       Parameters for Ge atoms added for GeO2 and Ge-Si-O etc. by Olli Pakarinen
!        
! Code description
!   Language       Fortran 90
!     
!-------------------------------------------------.-----------------------------

module silica_wat_mod
  use defs
  implicit none

  integer, parameter, private ::   &
       I4 = selected_int_kind(9),  &
       SP = kind(1.0e1),  &
       DP = kind(1.0d1)

  real(kind=DP), parameter :: swsigma = 2.0951_DP  ! SW length unit (��)
  !  real(kind=DP), parameter :: gesigma = 2.3163_DP  ! SW length unit (��) for Ge ! NOT NEEDED?
  real(kind=DP), parameter :: ucut = 1.9_DP        ! Si-Si-Si cutoff (swsigma)

  integer, dimension(:), allocatable, private :: wata_types

  real (kind=DP), parameter :: PII = 3.141592653589793238462643383279502884197_DP
  real (kind=DP), parameter :: TWOPI = 2.0_DP*PII
contains

  !-----------------------------------------------------------------------------
  !
  ! silica_wat_force - Calculates interatomic forces using the Watanabe potential
  !
  ! Method
  !   Calculates acceleration (A) of particles. The particle position are 
  !   given in X, and their interactions through a central force are 
  !   calculated according to the Watanabe potential for silica.
  !   The skeleton for this subroutine is taken from stilweb.f90.
  !
  !   If you modify the parameters, remember (Watanabe et al.):
  !   Although the three-body term was originally introduced 
  !   just to describe the bond bending forces, it has an additional 
  !   role to cancel out long-range two-body interactions 
  !   at the second nearest-neighbor distances. Therefore, 
  !   the cutoff distance of a three-body term should be set 
  !   at a longer value than that of two-body terms. 
  !   In a three-body term for a tight bond angle, 
  !   the long-range component is strong and inevitably causes 
  !   an unnatural steric hindrance. In order to solve 
  !   this problem, we split the three-body term into two components; 
  !   a shortrange term to describe correctly the bond bending
  !   force, and a long-range term to cancel out moderately
  !   the extra two-body interaction at the second nearestneighbor
  !   distances.
  !
  !   There is only the repulsive part of the two-body potential between 
  !   two oxygen atoms. Therefore, O-O-O and Si-O-O interaction 
  !   are omitted.
  !
  !   Parallellization principle:     
  !   1�� Copy myatoms atom coordinates*box into buf(1:myatoms*3)
  !   2�� Get atom coordinates of neighbouring nodes into rest of buf
  !   3�� Calculate forces of myatoms into array pbuf
  !   4�� Copy forces of myatoms to array a()
  !   5�� Send back accelerations of neighbour atoms to them
  !
  !   By calculating forces of all NP atoms in stage 3��, stage 5��
  !   can be omitted. However, this probably is less efficient except
  !   for really huge simulation cells. This approch requires that the 
  !   neighbour list contains neighbours of the atoms from other nodes 
  !   as well; see mdlinkedlist.f
  !
  !   myatoms is the number of atoms in my node
  !   NP is the total number of atoms, including the passed ones
  !
  !
  ! Input values
  !   X            Contains positions in angtroms scaled by 1/box
  !   atype        Parcas atom types
  !   natoms       Total number of atoms over all nodes
  !   nborlist     Neighborlist
  !   myatoms      Number of atoms in my node, in para_common.h
  !   box(3)       Box size (box centered on 0)
  !   pbc(3)       Periodics: if = 1.0d0 periodic
  !   atpassflag   See parallelization documentation
  !   nngbrproc    See parallelization documentatio
  !   reppotcutin  Cut-off distance for rep. pot.
  !
  ! Output values
  !   A            Contains forces in eV/A scaled by 1/box
  !   Epair        V_2 per atom
  !   Vpair        Total V_2 of all atoms
  !   Ethree       V_3 per atom
  !   Vthree       Total V_3 of all atoms; Vmany in mdx.f
  !   wxx          Virials
  !   wyy
  !   wzz
  !   wxxi
  !   wyyi
  !   wzzi
  !   wxxsh        Array of pressures at different shells
  !   wyysh
  !   wzzsh
  !   natsh        Array of temperature at different shells
  !   nsh          Number of shells
  !-----------------------------------------------.-----------------------------

  subroutine silica_wat_force(x,atype,a,natoms,box,pbc,                 &
       nborlist,Epair,Vpair,Ethree,Vthree,wxx,wyy,wzz,wxxi,wyyi,wzzi,   &
       wxxsh,wyysh,wzzsh,natsh,nsh,                                     &
       wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,		&
       atpassflag,nngbrproc,potmode,ECM,reppotcutin,moviemode,          &
       movietime_val,calc_avgvir,last_step_predict)

    use typeparam
    use datatypes
    use my_mpi
    use silicaparam

    use para_common

    implicit none

    ! Arguments
    real(kind=DP), dimension(:), intent(in) :: x
    real(kind=DP), dimension(:), intent(inout) :: a
    real(kind=DP), dimension(3), intent(in) :: box, pbc
    integer(kind=I4), dimension(:), intent(in) ::  nborlist
    integer, dimension(:), intent(in) :: atype
    integer, intent(in) :: natoms

    real(kind=DP), dimension(:), intent(inout) :: Epair, Ethree
    real(kind=DP), intent(inout) :: Vpair, Vthree
    real(kind=DP), intent(inout) :: wxx, wyy, wzz
    real(kind=DP), dimension(:), intent(inout) :: wxxi, wyyi, wzzi
    real(kind=DP), dimension(:), intent(inout) :: wxxsh, wyysh, wzzsh
    integer, intent(in) :: potmode
    real(kind=DP), dimension(3), intent(in) ::  ECM
    real(kind=DP), intent(in) :: reppotcutin

    integer, dimension(:), intent(inout) :: natsh
    integer, dimension(:), intent(in) :: atpassflag, nngbrproc
    integer, intent(in) :: nsh

    ! Scaling parameters for Ge, compared to Si
    real(kind=DP), parameter :: sw2mod2 = 0.82_DP     ! Modification of Ge-Ge S-W potential strength, x*-3.86
    real(kind=DP), parameter :: sw3mod2 = 0.67742_DP  ! Modification of S-W three-body strength, x*31
    real(kind=DP), parameter :: sw2mod3 = 0.90556_DP  ! Si-Ge
    real(kind=DP), parameter :: sw3mod3 = 0.82320_DP  ! Si-Ge

    real(kind=DP), parameter :: GeO_Lscale = 1.74_DP/1.61_DP ! Ge-O bond compared to Si-O, from S-Weber-pot.
    real(kind=DP), parameter :: GeGe_Lscale = 2.181_DP/2.0951_DP  ! Ge-Ge bond compared to Si-Si
    real(kind=DP), parameter :: GeSi_Lscale = 2.1376_DP/2.0951_DP ! Ge-Si bond compared to Si-Si

    real(kind=DP), parameter :: GeGe_Escale = 1.93_DP/2.16722_DP*sw2mod2  ! Ge-Ge compared to Si-Si
    real(kind=DP), parameter :: GeSi_Escale = 2.0451_DP/2.16722_DP*sw2mod3   !Ge-Si compared to Si-Si
    real(kind=DP), parameter :: GeO_Escale = 5.00_DP/6.42_DP            ! GeO2 Ecoh compared to SiO2
    !    real(kind=DP), parameter :: GeO_Escale = 580.0_DP/910.0_DP         ! GeO2 ? compared to SiO2

    ! Scaling for Ge parameters: start with Ding&Andersen PRB 34 (1986) 6987,
    ! add sw2mod and sw3mod values (see stilweb.f90)
    !    EPS(i)=sw2mod(i)*EPS(i)
    !    EPSA(i)=sw2mod(i)*EPSA(i)
    !    ALAM(i)=sw3mod(i)*ALAM(i)
    !    EPSLAM(i)=EPS(i)*ALAM(i)
    ! swpotmod=0.82, sw3mod=0.67742
    ! Aim for c-Ge:
    !a=5.653621              (Expt. 5.66 (?))
    !Epot=-3.1652
    !B = 660 kbar (Experimental B= 810 kbar).



    ! Pair interation parameters, Ge parameters scaled from Si by Ecoh values
    real(kind=DP), parameter :: epsilon = 2.16722_DP  ! = 50 kcal/mol 
    real(kind=DP), parameter :: A_OO    = -12.29_DP*epsilon*0.5_DP
    real(kind=DP), parameter :: A_SiO   = 21.0_DP*epsilon*0.5_DP
    real(kind=DP), parameter :: A_GeO   = 21.0_DP*epsilon*0.5_DP*GeO_Escale !  
    real(kind=DP), parameter :: A_SiSi  = 7.049556277_DP*epsilon*0.5_DP
    real(kind=DP), parameter :: A_GeGe  = 7.049556277_DP*epsilon*0.5_DP*GeGe_Escale !
    real(kind=DP), parameter :: A_GeSi  = 7.049556277_DP*epsilon*0.5_DP*GeSi_Escale !
    real(kind=DP), parameter :: B_OO    = 0.0_DP            ! now hard-coded
    real(kind=DP), parameter :: B_SiO   = 0.038_DP
    real(kind=DP), parameter :: B_GeO   = 0.038_DP ! Scaling needed in A only
    real(kind=DP), parameter :: B_SiSi  = 0.60222_DP
    real(kind=DP), parameter :: B_GeGe  = 0.60222_DP ! Scaling needed in A only
    real(kind=DP), parameter :: B_GeSi  = 0.60222_DP ! Scaling needed in A only
    real(kind=DP), parameter :: p_SiO   = 5.3_DP
    real(kind=DP), parameter :: p_GeO   = 5.3_DP !
    real(kind=DP), parameter :: p_SiSi  = 4.0_DP             ! now hard-coded
    real(kind=DP), parameter :: p_GeGe  = 4.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: p_GeSi  = 4.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: q_OO    = 2.24_DP
    real(kind=DP), parameter :: q_SiO   = -1.1_DP
    real(kind=DP), parameter :: q_GeO   = -1.1_DP !
    real(kind=DP), parameter :: q_SiSi  = 0.0_DP             ! now hard-coded
    real(kind=DP), parameter :: q_GeGe  = 0.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: q_GeSi  = 0.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: r0_OO   = 1.35_DP           ! originally 1.25_DP
    real(kind=DP), parameter :: r0_SiO  = 1.3_DP
    real(kind=DP), parameter :: r0_GeO  = 1.3_DP*GeO_Lscale !
    real(kind=DP), parameter :: r0_SiSi = 1.8_DP
    real(kind=DP), parameter :: r0_GeGe = 1.8_DP*GeGe_Lscale !
    real(kind=DP), parameter :: r0_GeSi = 1.8_DP*GeSi_Lscale !

    ! Si-O interaction softening function parameters  ! Used also for Ge-O
    real(kind=DP), parameter :: p1 = 0.30367_DP
    real(kind=DP), parameter :: p2 = 3.93233_DP
    real(kind=DP), parameter :: p3 = 0.25345_DP
    real(kind=DP), parameter :: p4 = 3.93233_DP
    real(kind=DP), parameter :: p5 = 5.274_DP
    real(kind=DP), parameter :: p6 = 0.712_DP
    real(kind=DP), parameter :: p7 = 0.522_DP
    real(kind=DP), parameter :: p8 = -0.0372_DP
    real(kind=DP), parameter :: p9 = -4.52_DP
    real(kind=DP), parameter :: op7 = 1.0_DP/p7

    ! The old values used in the first version of the Watanabe potential
!!$    real(kind=DP), parameter :: p5 = 0.097_DP
!!$    real(kind=DP), parameter :: p6 = 1.6_DP
!!$    real(kind=DP), parameter :: p7 = 0.3654_DP
!!$    real(kind=DP), parameter :: p8 = 0.1344_DP
!!$    real(kind=DP), parameter :: p9 = 6.4176_DP

    ! Three-body interaction parameters     ! Only energy via myy (1&2) and r0 scaled for Ge
    real(kind=DP), parameter :: alpha_1_SiOSi       = 1.6_DP
    real(kind=DP), parameter :: alpha_1_SiSiO       = 0.3_DP
    real(kind=DP), parameter :: alpha_1_SiSiSi      = -1.35_DP
    real(kind=DP), parameter :: alpha_2_SiSiO       = 0.3_DP
    real(kind=DP), parameter :: alpha_2_SiSiSi      = 0.3_DP

    real(kind=DP), parameter :: costheta0_1_OSiO    = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: costheta0_1_SiOSi   = -0.812_DP
    real(kind=DP), parameter :: costheta0_1_SiSiO   = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: costheta0_1_SiSiSi  = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: costheta0_2_SiSiO   = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: costheta0_2_SiSiSi  = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: gamma_OSi_1_SiOSi   = 0.15_DP
    real(kind=DP), parameter :: gamma_SiO_1_OSiO    = 0.31_DP
    real(kind=DP), parameter :: gamma_SiO_1_SiSiO   = 0.124_DP
    real(kind=DP), parameter :: gamma_SiO_2_SiSiO   = 1.082_DP
    real(kind=DP), parameter :: gamma_SiSi_1_SiSiO  = 0.032_DP
    real(kind=DP), parameter :: gamma_SiSi_2_SiSiO  = 1.01_DP
    real(kind=DP), parameter :: gamma_SiSi_1_SiSiSi = 0.088_DP
    real(kind=DP), parameter :: gamma_SiSi_2_SiSiSi = 1.01_DP
    real(kind=DP), parameter :: ksi_1_SiSiO         = 2.0_DP
    real(kind=DP), parameter :: ksi_2_SiSiSi        = 6.0_DP
    real(kind=DP), parameter :: myy_1_OSiO          = 10.5_DP
    real(kind=DP), parameter :: myy_1_SiOSi         = 2.5_DP
    real(kind=DP), parameter :: myy_1_SiSiSi        = 4.0_DP
    real(kind=DP), parameter :: myy_1_SiSiO         = 3.0_DP
    real(kind=DP), parameter :: myy_2_SiSiO         = 5.878_DP
    real(kind=DP), parameter :: myy_2_SiSiSi        = 5.878_DP

    real(kind=DP), parameter :: myy_1_OGeO          = 10.5_DP*GeO_Escale ! These scale the 3-body potential energy
    real(kind=DP), parameter :: myy_1_GeOGe         = 2.5_DP*GeO_Escale !
    real(kind=DP), parameter :: myy_1_GeOSi         = 2.5_DP*(1+GeO_Escale)/2.0_DP ! Avg. of Si-O and Ge-O
    real(kind=DP), parameter :: myy_1_GeGeGe        = 4.0_DP*GeGe_Escale !
    real(kind=DP), parameter :: myy_1_SiGeSi        = 4.0_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_1_GeSiGe        = 4.0_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_1_GeSiSi        = 4.0_DP*(1+GeSi_Escale)/2.0_DP ! Avg. of Si-Si and Ge-Si
    real(kind=DP), parameter :: myy_1_GeGeSi        = 4.0_DP*(GeGe_Escale+GeSi_Escale)/2.0_DP ! Avg. of Ge-Ge and Ge-Si
    real(kind=DP), parameter :: myy_1_GeGeO         = 3.0_DP*(GeGe_Escale+GeO_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_1_GeSiO         = 3.0_DP*(1+GeSi_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_1_SiGeO         = 3.0_DP*(GeSi_Escale+GeO_Escale)/2.0_DP !

    real(kind=DP), parameter :: myy_2_GeGeGe        = 5.878_DP*GeGe_Escale !
    real(kind=DP), parameter :: myy_2_SiGeSi        = 5.878_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_2_GeSiGe        = 5.878_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_2_GeSiSi        = 5.878_DP*(1+GeSi_Escale)/2.0_DP ! Avg. of Si-Si and Ge-Si
    real(kind=DP), parameter :: myy_2_GeGeSi        = 5.878_DP*(GeGe_Escale+GeSi_Escale)/2.0_DP ! Avg. of Ge-Ge and Ge-Si
    real(kind=DP), parameter :: myy_2_GeGeO         = 5.878_DP*(GeGe_Escale+GeO_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_2_GeSiO         = 5.878_DP*(1+GeSi_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_2_SiGeO         = 5.878_DP*(GeSi_Escale+GeO_Escale)/2.0_DP !

    real(kind=DP), parameter :: nyy_1_SiSiO         = 3.6_DP
    real(kind=DP), parameter :: nyy_2_SiSiSi        = 1.6_DP
    real(kind=DP), parameter :: r0_OSi_1_SiOSi      = 1.2_DP ! 0.2 in the paper is a mistake
    real(kind=DP), parameter :: r0_SiO_1_OSiO       = 1.2_DP ! originally 1.1
    real(kind=DP), parameter :: r0_SiO_1_SiSiO      = 1.10_DP
    real(kind=DP), parameter :: r0_SiO_2_SiSiO      = 1.5_DP
    real(kind=DP), parameter :: r0_SiSi_1_SiSiO     = 1.15_DP
    real(kind=DP), parameter :: r0_SiSi_2_SiSiO     = 1.8_DP
    real(kind=DP), parameter :: r0_SiSi_1_SiSiSi    = 1.0_DP ! originally 1.2
    real(kind=DP), parameter :: r0_SiSi_2_SiSiSi    = ucut ! originally 1.8 
    ! Ge r0 parameters 
    real(kind=DP), parameter :: r0_OGe_1_GeOGe      = 1.2_DP*GeO_Lscale ! 0.2 in the paper a mistake
    real(kind=DP), parameter :: r0_GeO_1_OGeO       = 1.2_DP*GeO_Lscale ! originally 1.1
    real(kind=DP), parameter :: r0_GeO_1_GeGeO      = 1.10_DP*GeO_Lscale
    real(kind=DP), parameter :: r0_GeGe_1_GeGeGe    = 1.0_DP*GeGe_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeGe_1_GeGeO     = 1.15_DP*GeGe_Lscale
    real(kind=DP), parameter :: r0_GeO_2_GeGeO      = 1.5_DP*GeO_Lscale
    real(kind=DP), parameter :: r0_GeGe_2_GeGeO     = 1.8_DP*GeGe_Lscale
    real(kind=DP), parameter :: r0_GeGe_2_GeGeGe    = ucut*GeGe_Lscale ! originally 1.8 
    ! Ge-Si r0 parameters HERE WE ASSUME THAT AT ANOTHER BOND Si/Ge CHANGE DOES NOT CHANGE THINGS 
    ! i.e. for example r0_GeO_1_GeOSi=r0_GeO_1_GeOGe
    real(kind=DP), parameter :: r0_GeSi_1_GeSiGe    = 1.0_DP*GeSi_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeSi_1_SiGeSi    = 1.0_DP*GeSi_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeSi_1_GeSiO     = 1.15_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_GeSiO     = 1.8_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_1_SiGeO     = 1.15_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_SiGeO     = 1.8_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_GeSiGe    = ucut*GeSi_Lscale ! originally 1.8
    real(kind=DP), parameter :: r0_GeSi_2_SiGeSi    = ucut*GeSi_Lscale ! originally 1.8

    real(kind=DP), parameter :: z0_1_SiSiO          = 2.6_DP  ! Same used for Ge ?

    ! Cutoff parameters
    real(kind=DP), parameter :: opswsigma = 1.0_DP/swsigma
    real(kind=DP), parameter :: cutoff2 = 1.8_DP*swsigma    ! pair interaction cutoff (��)
    real(kind=DP), parameter :: cutoff3 = 1.8_DP*swsigma    ! trimer interaction cutoff (��)
    real(kind=DP), parameter :: cutoff2sq = cutoff2*cutoff2
    real(kind=DP), parameter :: cutoff3sq = cutoff2*cutoff3
    real(kind=DP), parameter :: eosigma = epsilon/swsigma
    real(kind=DP), save :: cutoffmax,cutoffmaxsq

    ! Coordination number parameters
    real(kind=DP), parameter :: Dco = 0.1_DP ! original 0.05
    real(kind=DP), parameter :: Rco = 1.15_DP ! original 1.2
    real(kind=DP), parameter :: coordmin = Rco-Dco
    real(kind=DP), parameter :: coordmax = Rco+Dco
    real(kind=DP), parameter :: coordminGeO = (Rco-Dco)*GeO_Lscale
    real(kind=DP), parameter :: coordmaxGeO = (Rco+Dco)*GeO_Lscale

    ! Local variables
    real(kind=DP) :: dx, dy, dz
    real(kind=DP) :: dxij, dyij, dzij, dxijsq, dyijsq, dzijsq
    real(kind=DP) :: dxik, dyik, dzik, dxiksq, dyiksq, dziksq
    real(kind=DP) :: dxjl, dyjl, dzjl, dxjlsq, dyjlsq, dzjlsq
    real(kind=DP) :: rij, rijsq, oprij, rik, riksq, oprik, rjl, rjlsq, rx ! atom separations in ��
    real(kind=DP) :: rijsw, riksw, rjlsw ! atom separations in SW units
    real(kind=DP) :: rijsc ! scaled atom separation for Ge pair potential

    real(kind=DP), dimension(3) :: acm ! center of mass acceleration
    real(kind=DP) :: box2x, box2y, box2z, boxx2, boxy2, boxz2
    real(kind=DP) :: maxr              ! length of the diagonal of the box
    real(kind=DP) :: ph, halfph        ! potential
    real(kind=DP) :: dph               ! force
    real(kind=DP) :: gij, gSi, gO      ! bond softening value
    real(kind=DP) :: xl, xm, yl, ym, zl, zm
    real(kind=DP) :: ai, dai, vir
    real(kind=DP) :: t1, dttimer
    real(kind=DP) :: dummy1, dummy2
    real(kind=DP) :: help1, help2, help3, help4, help5, help6
    real(kind=DP) :: lambda1, lambda2
    real(kind=DP) :: biglambda1, biglambda2, bigtheta1, bigtheta2
    real(kind=DP) :: cosjik, dcos1, dcos2
    real(kind=DP) :: hjik
    real(kind=DP) :: dcos1sq, dcos2sq
    real(kind=DP) :: gamij1, gamij2, gamik1, gamik2
    real(kind=DP) :: coss1, coss2
    real(kind=DP) :: dphij1, dphij2, dphij3, dphik1, dphik2, dphik3
    real(kind=DP) :: dphijx, dphijy, dphijz, dphikx, dphiky, dphikz
    real(kind=DP) :: diff1ij, diff2ij, diff1ik, diff2ik
    real(kind=DP) :: coordination, coordexp
    real(kind=DP) :: lambdatheta1, lambdatheta2
    real(kind=DP) :: zetai, zetaj ! coordination numbers

    integer, parameter :: wataneimax = 100
    real(kind=DP), dimension(wataneimax) :: dxijw, dyijw, dzijw, dxijsqw, dyijsqw, dzijsqw
    real(kind=DP), dimension(wataneimax) :: rijw, rijsqw, rijsww ! Warning, a hard-coded parameter
    integer, dimension(wataneimax) :: parcas_types, atom_numbers
    integer, dimension(NPMAX) :: npositions

    integer :: np     ! the total number of atoms, including the passed ones
    integer :: np3, np32
    integer :: atomi, atomj, atomk, atoml ! numbers of atoms
    integer :: nngbr  ! number of neighbours of the current atom
    integer :: nngbrm ! number of neighbours of a neighbour atom
    integer :: ingbr, jngbr, cngbr, lngbr
    integer :: i, i2, i3, ii, ii3, j, j2, j3, jj, k, m, n, n3, d, myi
    integer :: l, l1, l2, iii
    integer :: nlistposj, nlistposk
    integer :: dfrom, ifrom
    integer :: atypi, atypj, atypk ! Parcas atom tyoes
    integer :: wti, wtj, wtk       ! Watanabe atom types
    integer :: nnsh
    integer :: error
    real(kind=DP) :: bfs, rfs ! temporary variables for Fermi parameters

    logical :: subcmacc = .false.
    logical, save :: firsttime = .true.
    logical, save :: sw

    real*8 wxy,wxyi(*),wxz,wxzi(*),wyx,wyxi(*),wyz,wyzi(*),wzx,wzxi(*),wzy,wzyi(*)
    integer moviemode
    logical movietime_val
    logical calc_vir
    logical calc_avgvir
    logical last_step_predict


    ! This parameter determines how near the cut-off limit the interactions are cut-off.
    ! If the exact cut-off limit is used instead, numerical instabilities may appear.
    real (kind=DP), parameter :: cutofflimit = -1.0d-8

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    ! Parameters for interface and parallellization
!include 'para_common.f90'

    !-----------------------------------------------------------------------------
    ! Section 1 - Copy myatoms atom coordinates*box into buf(1:myatoms*3)
    !-----------------------------------------------------------------------------

    np = myatoms
    do i = 1,myatoms
       i3 = i*3-3
       buf(i3+1) = X(i3+1)*box(1)
       buf(i3+2) = X(i3+2)*box(2)
       buf(i3+3) = X(i3+3)*box(3)
    enddo
    do i = 1,myatoms
       dirbuf(i) = 0
       ibuf(i*2-1) = i
       ibuf(i*2) = atype(i)
    enddo

    !-----------------------------------------------------------------------------
    ! Section 2 - Get atom coordinates of neighbouring nodes into rest of buf
    !
    ! Pass and receive atoms to different directions.
    ! The passing of atoms in this piece of code should be identical 
    ! in the neighbour list and force calculations.
    !     
    ! In principle the idea is that the input is the local x(jyatoms) 
    ! coordinates, and the output is buf(np), which contains the 
    ! coordinates of the atoms both in this node and within cut_nei
    ! from this in the adjacent nodes.
    !
    ! The atoms which get passed here must be exactly the
    ! same and in the same order in the force calculations
    ! as in the original neighbour list calculation !
    !
    ! To be able to pass back the P information of j pairs,
    ! we also pass and receive the atom indices and direction information
    ! of the atoms in j pairs.
    !     
    !-----------------------------------------------------------------------------

    if (nprocs .gt. 1) then
       t1 = mpi_wtime()
       !        Loop over directions
       do d = 1,8
          j = 0; jj = 0
          do i = 1,myatoms
             if (iand(atpassflag(i),passbit(d)) .ne. 0) then
                j = j+1
                if (j .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow1', int(myproc))
                i3 = i*3-3
                j3 = j*3-3
                j2 = j*2-2
                xsendbuf(j3+1) = X(i3+1)*box(1)
                xsendbuf(j3+2) = X(i3+2)*box(2)
                xsendbuf(j3+3) = X(i3+3)*box(3)
                isendbuf(j2+1) = i
                isendbuf(j2+2) = atype(i)
                !                 print *,d,j,X(i3+1),X(i3+2)
             endif
          enddo
          !print *,'send',myproc,d,j,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(NP*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(NP*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(NP*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(NP*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
          endif
          j = jj
          do i = 1,j
             dirbuf(NP+i) = d
          enddo
          NP = NP+j
       enddo
       tmr(33) = tmr(33)+(mpi_wtime()-t1)
    endif
    if (NP .ne. np0pairtable) then
       write (6,*) 'EAMforces np0 problem ',myproc,NP,np0pairtable
    endif
    !print *,'Node received totally ',myproc,NP-myatoms


    !-----------------------------------------------------------------------------
    ! Section 3 - Calculate forces of myatoms into array pbuf
    !
    ! Use buf instead of x, pbuf instead of a.
    ! pbuf contains actual accelerations, buf actual coordinates.
    ! (NOT in PARCAS internal units, scaled by 1/box)
    !
    ! Take care to calculate pressure, total pot. only for atoms in my node !
    !
    ! Boundary conditions:
    !   if pcb eq 1.0d0, periodic - Note that different conditions may be specified
    !                            in x, y and z direction.
    !
    !    if       dx > box2(1)         xl = 2*box2(1)
    !  -box2(1) < dx < box2(1)         xl = 0
    !             dx < -box2(1)        xl = -2*box2(1)
    !   i.e. if a separation exceeds half of the box side, it is treated as coming
    !   from the adjacent box.
    !
    ! Indeces n, m, and l are used instead the indeces i, j, and k common 
    ! in papers.
    !
    ! Loop over third atoms goes from the next atom to the last one, because 
    ! every angle between atoms should be calculated only once. Instead, 
    ! the coordination number loop goes over the all neighbours of an atom.
    !
    ! Configurations of dimers and trimers are calculated in separate code 
    ! blocks to optimize them individually.
    !
    ! In the potential and force calculation Stillinger-Weber length units are 
    ! used instead of ��, which otherwise is the length unit in this subroutine.
    ! Note that the reppot subroutine does not use SW units.
    !
    ! Some parameters are hard-coded to optimize performance.
    !-----------------------------------------------------------------------------

    ! Initialize force calculation stuff 
    np3 = np*3
    np32 = np3-2

    box2x = box(1)/2.0
    box2y = box(2)/2.0
    box2z = box(3)/2.0

    boxx2 = box(1)*2.0
    boxy2 = box(2)*2.0
    boxz2 = box(3)*2.0

    calc_vir = .false.
    if (movietime_val .or. calc_avgvir .or. last_step_predict) then
     if (moviemode == 15 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
      calc_vir = .true.
     endif
    endif


    do n = 1,np3
       pbuf(n) = 0.0
    enddo
    do n = 1,np
       ebuf(n) = 0.0
    enddo
    do n = 1,myatoms*3
       a(n) = 0.0
    enddo
    do n = 1,myatoms
       Epair(n) = 0.0
       Ethree(n) = 0.0
       wxxi(n) = 0.0
       wyyi(n) = 0.0
       wzzi(n) = 0.0
       if (calc_vir) then
          wxyi(n)=zero
          wxzi(n)=zero
          wyxi(n)=zero
          wyzi(n)=zero
          wzxi(n)=zero
          wzyi(n)=zero
       endif

    enddo
    Vpair = 0.0
    Vthree = 0.0
    wxx = 0.0
    wyy = 0.0
    wzz = 0.0
    do j = 1,nsh
       wxxsh(j) = 0.0
       wyysh(j) = 0.0
       wzzsh(j) = 0.0
       natsh(j) = 0
    enddo
    maxr = sqrt(box(1)*box(1)+box(2)*box(2)+box(3)*box(3))/2.0_DP
    vir = 0.0

    if (calc_vir) then
       wxy=zero
       wxz=zero
       wyx=zero
       wyz=zero
       wzx=zero
       wzy=zero
    endif

    if (firsttime .and. nprocs>1) then
       print *,'WARNING: this parcas version has small bug in parallel version'
       print *,'close to domain boundaries in bond softening function'
    endif

    if (firsttime) then
       cutoffmax=0.0d0
       do i = itypelow,itypehigh
          do j = itypelow,itypehigh
             if (rcut(i,j) > cutoffmax) cutoffmax=rcut(i,j)
          enddo
       enddo
       cutoffmaxsq=cutoffmax*cutoffmax
       IF(iprint)WRITE(6,*) 'Silica_Wat maximum cutoff',cutoffmax
    endif


!!$    if (firsttime) then
!!$       allocate( npositions(myatoms), stat=error )
!!$       if ( error /= 0 ) then
!!$          CALL my_mpi_abort('Allocation error in Wata.f90', int(myproc))
!!$       end if
!!$    endif
!!$    firsttime=.false.

    ! Find the neighbourlist positions of atoms
    l1 = 1
    npositions(1) = 1
    do l=1,myatoms
       l2 = nborlist(l1)
       l1 = l1 + l2 + 1
       npositions(l+1) = l1
    end do

    ph = 0.0
    dph = 0.0
    xl = 0.0
    xm = 0.0
    yl = 0.0
    ym = 0.0
    zl = 0.0
    zm = 0.0


    ! Main loop over atoms n starts --------------------------------------------
    nlistposj = 0                                 ! neighbourlist index
    do atomi = 1, myatoms
       i = atomi*3-2                              ! pos. of coordinate x in buf

       atypi = abs(ibuf(atomi*2))                 ! type of atom n
       wti = wata_types(atypi)                    ! local type

       nlistposj = nlistposj + 1          
       nngbr = nborlist(nlistposj)                ! number of neighbours

       if ( nngbr == 0 ) cycle
       if (nngbr < 0) then
          print *,'Neighbourlist HORROR ERROR in wata_forces()!', nngbr
       endif
       if (nngbr > wataneimax) then
          print *,'Neighbour array size exceeded in silica_wat',i,nngbr
          call my_mpi_abort('wataneimax exceeded', int(wataneimax))
       endif


       ! Calculate and save distances to all neighbours of the atom i ----------
       nlistposk = nlistposj
       do ingbr = 1,nngbr
          nlistposk = nlistposk + 1          

          atomj = nborlist(nlistposk)             ! number of atom m
          j = atomj*3-2                           ! pos. of coordinate x in buf
          atom_numbers(ingbr) = atomj

          if ( atomj == atomi ) cycle

          parcas_types(ingbr) = abs(ibuf(atomj*2))

          dxij = buf(i) - buf(j)
          if ( pbc(1) == 1.0d0 ) then               ! periodic x boundary
             xm   = sign(box2x, dxij+box2x) + sign(box2x, dxij-box2x)
             dxij = dxij - xm
          end if
          dxijw(ingbr) = dxij
          dxijsqw(ingbr) = dxij*dxij

          dyij = buf(i+1) - buf(j+1)
          if ( pbc(2) == 1.0d0 ) then               ! periodic y boundary
             ym   = sign(box2y, dyij+box2y) + sign(box2y, dyij-box2y)
             dyij = dyij - ym
          end if
          dyijw(ingbr) = dyij
          dyijsqw(ingbr) = dyij*dyij

          dzij = buf(i+2) - buf(j+2)
          if ( pbc(3) == 1.0d0 ) then               ! periodic z boundary
             zm   = sign(box2z, dzij+box2z) + sign(box2z, dzij-box2z)
             dzij = dzij - zm
          end if
          dzijw(ingbr) = dzij
          dzijsqw(ingbr) = dzij*dzij

          rijsqw(ingbr) = dxijsqw(ingbr)+dyijsqw(ingbr)+dzijsqw(ingbr)
          rijw(ingbr) = sqrt(rijsqw(ingbr))
          rijsww(ingbr) = rijw(ingbr)*opswsigma

       end do


       ! 2-body interaction loop starts ----------------------------------------
       do ingbr = 1,nngbr
          nlistposj = nlistposj + 1          
          atomj = nborlist(nlistposj)             ! number of atom j
          j = atomj*3-2                           ! pos. of coordinate x in buf

          if (i == j) cycle

          atypj = parcas_types(ingbr)             ! Parcas type of atom m
          wtj   = wata_types(atypj)               ! local type

          ! No interaction defined between atoms i and j
          if (iac(atypi,atypj) /=  1) then
             if (iac(atypi,atypj) == 0) cycle
             if (iac(atypi,atypj) <= -1) then
                write (6,*) 'ERROR: Impossible interaction'
                write (6,*) myproc,i,j,ATYPI,ATYPJ
                call my_mpi_abort('Impossible interaction iac = -1', int(myproc))
             endif
          endif


          ! Before parcas V4.02: if ( rijsqw(ingbr) > cutoff2sq ) cycle 
          if ( rijsqw(ingbr) > cutoffmaxsq ) cycle 

          dxij   = dxijw(ingbr)
          dxijsq = dxijsqw(ingbr)
          dyij   = dyijw(ingbr)
          dyijsq = dyijsqw(ingbr)
          dzij   = dzijw(ingbr)
          dzijsq = dzijsqw(ingbr)
          rijsq  = rijsqw(ingbr)
          rij    = rijw(ingbr)                    ! distance between n and m
          rijsw  = rijsww(ingbr)                  ! convert to SW length units
          oprij  = 1.0_DP/rij

          ph = 0.0
          dph = 0.0

          if (iac(atypi,atypj) == 1) then

             if ( wti == 1 .and. wtj == 1 ) then  ! Si-Si 
                help1 = rijsw-r0_SiSi
                if ( help1 > cutofflimit ) goto 100
                help1 = 1.0_DP/help1
                help2 = A_SiSi*exp(help1)
                help3 = rijsw*rijsw               ! hard-coded parameter
                help3 = B_SiSi/(help3*help3)
                help5 = help3-1.0_DP              ! hard-coded parameter
                ph = help5*help2
                dph = -help2*( help3*p_SiSi/rijsw + help5*help1*help1 )
                dph = dph*opswsigma                 ! change length unit
                !                if (atomi==333) then ! .and. atomj==334) then  
                !                   write(*,*) 'atom_i, _j, neighbours of Si-Si pair', atomi,atomj, atom_numbers(cngbr)
                !                end if


             else if ( wti == 2 .and. wtj == 2 ) then       ! O-O
                help1 = rijsw-r0_OO
                if ( help1 > cutofflimit ) goto 100
                help2 = A_OO*exp(1.0_DP/help1)
                help4 = 1.0_DP/rijsw**q_OO
                ph = -help4*help2                 ! hard-coded parameter
                help1 = help1*help1
                dph = help2*help4*( q_OO/rijsw + 1.0_DP/help1 ) ! hard-coded parameter
                dph = dph*opswsigma

             else if ( wti == 3 .and. wtj == 3 ) then       ! Ge-Ge r-SCALING DIRECTLY IN rijsw
                rijsc=rijsw/GeGe_Lscale
                help1 = rijsc-r0_SiSi
                if ( help1 > cutofflimit ) goto 100
                help1 = 1.0_DP/help1
                help2 = A_GeGe*exp(help1)
                help3 = rijsc*rijsc           ! hard-coded parameter
                help3 = B_GeGe/(help3*help3)
                help5 = help3-1.0_DP              ! hard-coded parameter
                ph = help5*help2
                dph = -help2*( help3*p_SiSi/rijsc + help5*help1*help1 )
                dph = dph*opswsigma/GeGe_Lscale                 ! change length unit

             else if (( wti == 1 .and. wtj == 3 ) .or.( wti == 3 .and. wtj == 1 )) then ! Ge-Si   OLLI
                rijsc=rijsw/GeSi_Lscale
                help1 = rijsc-r0_SiSi
                if ( help1 > cutofflimit ) goto 100
                help1 = 1.0_DP/help1
                help2 = A_GeSi*exp(help1)
                help3 = rijsc*rijsc         ! hard-coded parameter
                help3 = B_GeSi/(help3*help3)
                help5 = help3-1.0_DP              ! hard-coded parameter
                ph = help5*help2
                dph = -help2*( help3*p_SiSi/rijsc + help5*help1*help1 )
                dph = dph*opswsigma/GeSi_Lscale                 ! change length unit


             else                                 ! Si/Ge-O or O-Si/Ge 
                ! Calculate the coordination number of atom i
                zetai = 0.0
                do cngbr = 1,nngbr
                   if ( parcas_types(cngbr) == atypi ) cycle
                   !if (atomi==10.and.atomj==18) write(*,*) atomi,atomj, atom_numbers(cngbr)
                   if ( rijsww(cngbr) < coordmin ) then
                      zetai = zetai + 1.0_DP
                   else if ( rijsww(cngbr) >= coordmin .and. rijsww(cngbr) < coordmax ) then
                      help1 = (rijsww(cngbr) - Rco + Dco)/Dco
                      !zetai = zetai + 1.0_DP - 0.5_DP*help1 + sin(PII*help1)/TWOPI ! original
                      ! zetai by Juha Samela, gives 0 at lower limit:
                      !zetai = zetai + 0.5_DP*( 1.0_DP + sin(PII*(help1-0.5_DP)) )
                      ! zetai by Kai Nordlund 26.6.2007, see sicasc/sio2/test/README
                      zetai = zetai + 0.5_DP*( 1.0_DP + COS(help1/2.0_DP*PII) )
        !print *,'DEBUG',rijsww(cngbr),help1,zetai,PII,Rco,Dco
                   endif
                end do


                ! BUG: If atomj>myatoms on parallel processors this gives wrong results!!

                if (potmode == 210) then
                   if (atomj<=myatoms) then

        ! Find number of neighbours of atom j
                      lngbr = nborlist( npositions(atomj) )

        ! Calculate the coordination number of atom j  
                      zetaj = 0.0
                      nlistposk = npositions(atomj)
                      do cngbr = 1,lngbr
                         nlistposk = nlistposk + 1          
                         atoml = nborlist(nlistposk)             ! number of atom l

                         if ( abs(ibuf(atomj*2)) == abs(ibuf(atoml*2)) ) cycle

                         l = atoml*3-2                           ! pos. of coordinate x in buf
                         dxjl = buf(j) - buf(l)
                         if ( pbc(1) == 1.0d0 ) then               ! periodic x boundary
                            xm   = sign(box2x, dxjl+box2x) + sign(box2x, dxjl-box2x)
                            dxjl = dxjl - xm
                         end if
                         dxjlsq = dxjl*dxjl

                         dyjl = buf(j+1) - buf(l+1)
                         if ( pbc(2) == 1.0d0 ) then               ! periodic y boundary
                            ym   = sign(box2y, dyjl+box2y) + sign(box2y, dyjl-box2y)
                            dyjl = dyjl - ym
                         end if
                         dyjlsq = dyjl*dyjl

                         dzjl = buf(j+2) - buf(l+2)
                         if ( pbc(3) == 1.0d0 ) then               ! periodic z boundary
                            zm   = sign(box2z, dzjl+box2z) + sign(box2z, dzjl-box2z)
                            dzjl = dzjl - zm
                         end if
                         dzjlsq = dzjl*dzjl

                         rjlsq = dxjlsq+dyjlsq+dzjlsq
                         rjl = sqrt(rjlsq)
                         rjlsw = rjl*opswsigma

                         if ( rjlsw < coordmin ) then    
                            zetaj = zetaj + 1.0_DP
                         else if ( rjlsw >= coordmin .and. rjlsw < coordmax ) then
                            help1 = (rjlsw - Rco + Dco)/Dco
       !zetaj = zetaj + 1.0_DP - 0.5_DP*help1 + sin(PII*help1)/TWOPI ! original
       !zetaj = zetaj + 0.5_DP*( 1.0_DP + sin(PII*(help1-0.5_DP)) )
       ! zetaj by Kai Nordlund 26.6.2007:
                            zetaj = zetaj + 0.5_DP*( 1.0_DP + COS(help1/2.0_DP*PII) )
                         end if
                      end do

        ! The original bond softening function WITH suboxide penalty
                      gij = 1.0_DP
!!$                if ( zetai > 0.0 .and. zetaj > 0.0 ) then
!!$                   if ( wti == 1 ) then
!!$                      if ( zetai < 4.0_DP ) then
!!$                         gSi = (p1*sqrt(zetai+p2) - p4)*exp(p3/(zetai-4.0_DP)) + p4
!!$                      else
!!$                         gSi = p4
!!$                      end if
!!$                      gO  = p5*exp(p8*(zetaj-p9)*(zetaj-p9))/(exp((p6-zetaj)/p7) + 1.0_DP)
!!$                   else
!!$                      if ( zetaj < 4.0_DP ) then
!!$                         gSi = (p1*sqrt(zetaj+p2) - p4)*exp(p3/(zetaj-4.0_DP)) + p4
!!$                      else
!!$                         gSi = p4
!!$                      end if
!!$                      gO  = p5*exp(p8*(zetai-p9)*(zetai-p9))/(exp((p6-zetai)/p7) + 1.0_DP)
!!$                   end if
!!$                   gij = gSi*gO
!!$                else
!!$                   gij = 1.0_DP
!!$                end if


        ! Bond-softening WITHOUT suboxide penalty
                      if ( zetai > 0.0 .and. zetaj > 0.0 ) then
                         gO=1.0_DP
                         if (( wti == 1) .or. ( wti == 3)) then
                            if ( zetaj > 2.0 ) then
                               gO  = p5*exp(p8*(zetaj-p9)*(zetaj-p9))/(exp((p6-zetaj)*op7) + 1.0_DP)
                            else
                               gO = 1.0_DP
                            end if
                         else
                            if ( zetai > 2.0 ) then
                               gO  = p5*exp(p8*(zetai-p9)*(zetai-p9))/(exp((p6-zetai)*op7) + 1.0_DP)
                            else
                               gO = 1.0_DP
                            end if
                         end if
                         gij = gO
                      else
                         gij = 1.0_DP
                      end if
        ! parallell BUG correction
                   else
                      gij = 1.0_DP
                   endif
                else if (potmode == 211) then
                   gij = 1.0_DP
                else
                   call my_mpi_abort('Unknown silica potmode', int(potmode))
                endif

  !print *,'DEBUG',gij
  ! KN 16.1.2007
  ! Setting gij=1 removes the bond softening function
  !gij=1.0_DP


  !if ( atomi == 10 .and. atomj == 18) write(*,*) '10-18: ', zetai,zetaj,gij
  !if ( atomi == 18 .and. atomj == 10) write(*,*) '18_10: ', zetaj,zetai,gij

  !------------------------------------------------------------------------------------------------------
                if (( wti == 2 .and. wtj == 1 ) .or.( wti == 1 .and. wtj == 2 )) then ! Si-O 
                   ! Pair potential and force
                   help1 = rijsw-r0_SiO
                   if ( help1 > cutofflimit ) goto 100
                   help1 = 1.0_DP/help1
                   help2 = gij*A_SiO*exp(help1)   ! softening applied
                   help3 = B_SiO/rijsw**p_SiO
                   help4 = 1.0_DP/rijsw**q_SiO
                   help5 = help3-help4
                   ph = help5*help2

                   help1 = help1*help1
                   dph = help2*( help4*q_SiO/rijsw - help3*p_SiO/rijsw - help5*help1 )
                   dph = dph*opswsigma

                   !------------------------------------------------------------------------------------------------------                
                else if (( wti == 2 .and. wtj == 3 ) .or.( wti == 3 .and. wtj == 2 )) then ! Ge-O
                   ! Pair potential and force
                   rijsc=rijsw/GeO_Lscale
                   help1 = rijsc-r0_SiO
                   if ( help1 > cutofflimit ) goto 100
                   help1 = 1.0_DP/help1
                   help2 = gij*A_GeO*exp(help1)   ! softening applied
                   help3 = B_GeO/rijsc**p_SiO
                   help4 = 1.0_DP/rijsc**q_SiO
                   help5 = help3-help4
                   ph = help5*help2

                   help1 = help1*help1
                   dph = help2*( help4*q_SiO/rijsc - help3*p_SiO/rijsc - help5*help1 )
                   dph = dph*opswsigma/GeO_Lscale    ! Length scale right
                endif
             endif

             ! Short range repulsive potential times Fermi function 
             if (rij < rcut(atypi,atypj)) then
                t1 = mpi_wtime()
                if ( wti == 1 .and. wtj == 1 ) then
                   bfs = 15.0_DP               ! hard-coded Fermi joining parameters
                   rfs = 1.4_DP
                else if ( (wti == 1 .and. wtj == 2) .or. (wti == 2 .and. wtj == 1) ) then
                   bfs = 15.0_DP
                   rfs = 0.9_DP
                else if ( wti == 2 .and. wtj == 2 ) then
                   bfs = 15.0_DP
                   rfs = 0.5_DP
                else if ( wti == 3 .and. wtj == 3 ) then   
                   ! These Ge parameters from stilweb.f90, checked to be OK 9.6.2008
                   bfs = 15.0_DP               ! hard-coded Fermi joining parameters
                   rfs = 1.5_DP
                else if ( (wti == 3 .and. wtj == 1) .or. (wti == 1 .and. wtj == 3) ) then
                   ! These Ge-Si parameters from stilweb.f90, checked to be OK 9.6.2008
                   bfs = 15.0_DP               ! hard-coded Fermi joining parameters
                   rfs = 1.45_DP
                else if ( (wti == 3 .and. wtj == 2) .or. (wti == 2 .and. wtj == 3) ) then
                   ! GeO parameters optimized 9.6.2008 in parcas V3.90e.
                   bfs = 15.0_DP
                   rfs = 1.0_DP
                else
                   write (6,*) 'ERROR in wata.f90, No Fermi parameters for types'
                   write (6,*) myproc,i,j,wti,wtj, bfs, rfs
                   call my_mpi_abort('ERROR in wata.f90 No Fermi parameters for types', int(myproc))
                end if
                if (rij < reppotcutin) then
                   call splinereppot(rij,ph,dph,bfs,rfs,atypi,atypj,dummy1,dummy2)

                   tmr(32) = tmr(32)+(mpi_wtime()-t1)
                endif
             endif

             ! Short range repulsion only
          else if (iac(atypi,atypj) == 2) then 
             call splinereppot(rij,ph,dph,1d20,1d20,atypi,atypj,dummy1,dummy2)
             ! ij symmetry not used in silica_wat.f90 so this is needed:
             ph=ph/2.0_DP
             dph=dph/2.0_DP

      ! Wrong interaction type or error in algorithm
          else
             write (6,*) 'ERROR in wat.f90 impossible interaction type'
             write (6,*) myproc,i,j,ATYPI,ATYPJ
             call my_mpi_abort('ERROR in silica_wat.f90 or impossible interaction type', int(myproc))
          endif

          halfph = 0.5_DP*ph
          ebuf(atomi) = ebuf(atomi)+halfph
          ebuf(atomj) = ebuf(atomj)+halfph
          VPair = Vpair+ph

          ai = -dph*oprij

          dai = dxij*ai
          pbuf(i) = pbuf(i) + dai
          pbuf(j) = pbuf(j) - dai
          wxxi(atomi) = wxxi(atomi)+dai*dxij

          if (calc_vir) then
             wxyi(atomi)=wxyi(atomi)+dai*dyij
             wxzi(atomi)=wxzi(atomi)+dai*dzij
          endif

          dai = dyij*ai
          pbuf(i+1) = pbuf(i+1) + dai
          pbuf(j+1) = pbuf(j+1) - dai
          wyyi(atomi) = wyyi(atomi)+dai*dyij

          if (calc_vir) then
             wyxi(atomi)=wyxi(atomi)+dai*dxij
             wyzi(atomi)=wyzi(atomi)+dai*dzij
          endif

          dai = dzij*ai
          pbuf(i+2) = pbuf(i+2) + dai
          pbuf(j+2) = pbuf(j+2) - dai
          wzzi(atomi) = wzzi(atomi)+dai*dzij

          if (calc_vir) then
             wzxi(atomi)=wzxi(atomi)+dai*dxij
             wzyi(atomi)=wzyi(atomi)+dai*dyij
          endif


100       if ( wti == 2 .and. wtj == 2 ) cycle   ! no 3-body interaction for O-O

          if (iac(atypi,atypj) /= 1) cycle     ! Handle other interaction types: skip rest

   ! 3-body interaction loop starts -------------------------------------
          nlistposk = nlistposj
          do jngbr=ingbr+1,nngbr
             nlistposk = nlistposk+1
             atomk = nborlist(nlistposk)
             k = atomk*3-2

             if ( k == i .or. k == j ) cycle  

             atypk = parcas_types(jngbr)          ! Paecas type

             if (iac(atypi,atypk) /= 1) cycle     ! Handle other interaction types

             wtk = wata_types(atypk)              ! local type

             if ( wtj == 1 .and. wti == 2 .and. wtk == 2 ) cycle  ! Si-O-O
             if ( wtj == 3 .and. wti == 2 .and. wtk == 2 ) cycle  ! Ge-O-O

             dxik   = dxijw(jngbr)
             dxiksq = dxijsqw(jngbr)
             dyik   = dyijw(jngbr)
             dyiksq = dyijsqw(jngbr)
             dzik   = dzijw(jngbr)
             dziksq = dzijsqw(jngbr)
             riksq  = rijsqw(jngbr)
             rik    = rijw(jngbr)
             riksw  = rijsww(jngbr)
             oprik = 1.0_DP/rik

             lambda1 = 0.0
             bigtheta1 = 0.0
             biglambda1 = 0.0
             gamij1 = 0.0
             gamik1 = 0.0
             coss1 = 0.0
             lambda2 = 0.0
             bigtheta2 = 0.0
             biglambda2 = 0.0
             gamij2 = 0.0
             gamik2 = 0.0
             coss2 = 0.0

             if ( wtj == 1 .and. wti == 2 .and. wtk == 1 )  then ! Si-O-Si 

                diff1ij = rijsw - r0_OSi_1_SiOSi
                if ( diff1ij > cutofflimit ) cycle
                diff1ik = riksw - r0_OSi_1_SiOSi
                if ( diff1ik > cutofflimit ) cycle

                cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                dcos1 = cosjik - costheta0_1_SiOSi
                if ( abs(dcos1) < -cutofflimit ) cycle

                dcos1sq = dcos1*dcos1
                coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiOSi*dcos1sq

                ! Only the short range 3-body interaction
                diff1ij = 1.0_DP/diff1ij
                diff1ik = 1.0_DP/diff1ik
                bigtheta1 = dcos1sq + alpha_1_SiOSi*dcos1sq*dcos1
                lambda1 = myy_1_SiOSi
                gamij1  = gamma_OSi_1_SiOSi*diff1ij
                gamik1  = gamma_OSi_1_SiOSi*diff1ik

                biglambda1 = lambda1*exp( gamij1 + gamik1 )
                gamij1 = gamij1*diff1ij
                gamik1 = gamik1*diff1ik

                lambda2 = 0.0
                bigtheta2 = 0.0
                biglambda2 = 0.0
                gamij2 = 0.0
                gamik2 = 0.0

             else if ( wtj==3 .and. wti==2 .and. wtk==3 ) then ! Ge-O-Ge

                diff1ij = rijsw - r0_OGe_1_GeOGe
                if ( diff1ij > cutofflimit ) cycle
                diff1ik = riksw - r0_OGe_1_GeOGe
                if ( diff1ik > cutofflimit ) cycle

                cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                dcos1 = cosjik - costheta0_1_SiOSi
                if ( abs(dcos1) < -cutofflimit ) cycle

                dcos1sq = dcos1*dcos1
                coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiOSi*dcos1sq

                ! Only the short range 3-body interaction
                diff1ij = 1.0_DP/diff1ij
                diff1ik = 1.0_DP/diff1ik
                bigtheta1 = dcos1sq + alpha_1_SiOSi*dcos1sq*dcos1
                lambda1 = myy_1_GeOGe
                gamij1  = gamma_OSi_1_SiOSi*diff1ij
                gamik1  = gamma_OSi_1_SiOSi*diff1ik

                biglambda1 = lambda1*exp( gamij1 + gamik1 )
                gamij1 = gamij1*diff1ij
                gamik1 = gamik1*diff1ik

                lambda2 = 0.0
                bigtheta2 = 0.0
                biglambda2 = 0.0
                gamij2 = 0.0
                gamik2 = 0.0

             else if (wtj==3 .and. wti==2 .and. wtk==1 ) then ! Ge-O-Si OLLI

                diff1ij = rijsw - r0_OGe_1_GeOGe      ! In these we assume bond lengths independent
                if ( diff1ij > cutofflimit ) cycle
                diff1ik = riksw - r0_OSi_1_SiOSi      ! In these we assume bond lengths independent
                if ( diff1ik > cutofflimit ) cycle

                cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                dcos1 = cosjik - costheta0_1_SiOSi
                if ( abs(dcos1) < -cutofflimit ) cycle

                dcos1sq = dcos1*dcos1
                coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiOSi*dcos1sq

                ! Only the short range 3-body interaction
                diff1ij = 1.0_DP/diff1ij
                diff1ik = 1.0_DP/diff1ik
                bigtheta1 = dcos1sq + alpha_1_SiOSi*dcos1sq*dcos1
                lambda1 = myy_1_GeOSi
                gamij1  = gamma_OSi_1_SiOSi*diff1ij
                gamik1  = gamma_OSi_1_SiOSi*diff1ik

                biglambda1 = lambda1*exp( gamij1 + gamik1 )
                gamij1 = gamij1*diff1ij
                gamik1 = gamik1*diff1ik

                lambda2 = 0.0
                bigtheta2 = 0.0
                biglambda2 = 0.0
                gamij2 = 0.0
                gamik2 = 0.0

             else if (wtj==1 .and. wti==2 .and. wtk==3) then ! Si-O-Ge

                diff1ij = rijsw - r0_OSi_1_SiOSi     ! In these we assume bond lengths independent
                if ( diff1ij > cutofflimit ) cycle
                diff1ik = riksw - r0_OGe_1_GeOGe     ! In these we assume bond lengths independent
                if ( diff1ik > cutofflimit ) cycle

                cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                dcos1 = cosjik - costheta0_1_SiOSi
                if ( abs(dcos1) < -cutofflimit ) cycle

                dcos1sq = dcos1*dcos1
                coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiOSi*dcos1sq

                ! Only the short range 3-body interaction
                diff1ij = 1.0_DP/diff1ij
                diff1ik = 1.0_DP/diff1ik
                bigtheta1 = dcos1sq + alpha_1_SiOSi*dcos1sq*dcos1
                lambda1 = myy_1_GeOSi
                gamij1  = gamma_OSi_1_SiOSi*diff1ij
                gamik1  = gamma_OSi_1_SiOSi*diff1ik

                biglambda1 = lambda1*exp( gamij1 + gamik1 )
                gamij1 = gamij1*diff1ij
                gamik1 = gamik1*diff1ik

                lambda2 = 0.0
                bigtheta2 = 0.0
                biglambda2 = 0.0
                gamij2 = 0.0
                gamik2 = 0.0

             else if ( wtj == 2 .and. wti == 1 .and. wtk == 2 ) then ! O-Si-O

                diff1ij = rijsw - r0_SiO_1_OSiO
                if ( diff1ij > cutofflimit ) cycle
                diff1ik = riksw - r0_SiO_1_OSiO
                if ( diff1ik > cutofflimit ) cycle

                cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                dcos1 = cosjik - costheta0_1_OSiO
                if ( abs(dcos1) < -cutofflimit ) cycle

                dcos1sq = dcos1*dcos1
                coss1   = 2.0_DP*dcos1! + 3.0_DP*alpha_1_OSiO*dcos1sq

                ! Only the short range 3-body interaction
                diff1ij = 1.0_DP/diff1ij
                diff1ik = 1.0_DP/diff1ik
                bigtheta1 = dcos1sq
                lambda1 = myy_1_OSiO
                gamij1  = gamma_SiO_1_OSiO*diff1ij
                gamik1  = gamma_SiO_1_OSiO*diff1ik

                biglambda1 = lambda1*exp( gamij1 + gamik1 )
                gamij1 = gamij1*diff1ij
                gamik1 = gamik1*diff1ik

                lambda2 = 0.0
                bigtheta2 = 0.0
                biglambda2 = 0.0
                gamij2 = 0.0
                gamik2 = 0.0
             else if ( wtj == 2 .and. wti == 3 .and. wtk == 2 ) then ! O-Ge-O 

                diff1ij = rijsw - r0_GeO_1_OGeO
                if ( diff1ij > cutofflimit ) cycle
                diff1ik = riksw - r0_GeO_1_OGeO
                if ( diff1ik > cutofflimit ) cycle

                cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                dcos1 = cosjik - costheta0_1_OSiO
                if ( abs(dcos1) < -cutofflimit ) cycle

                dcos1sq = dcos1*dcos1
                coss1   = 2.0_DP*dcos1! + 3.0_DP*alpha_1_OSiO*dcos1sq

                ! Only the short range 3-body interaction
                diff1ij = 1.0_DP/diff1ij
                diff1ik = 1.0_DP/diff1ik
                bigtheta1 = dcos1sq
                lambda1 = myy_1_OGeO
                gamij1  = gamma_SiO_1_OSiO*diff1ij
                gamik1  = gamma_SiO_1_OSiO*diff1ik

                biglambda1 = lambda1*exp( gamij1 + gamik1 )
                gamij1 = gamij1*diff1ij
                gamik1 = gamik1*diff1ik

                lambda2 = 0.0
                bigtheta2 = 0.0
                biglambda2 = 0.0
                gamij2 = 0.0
                gamik2 = 0.0


             else

                ! Calculate the coordination number of atom i MY��HEMMIN SAADAAN PARIPOTENTIAALISTA
                coordination = 0.0
                do cngbr = 1,nngbr

                   if ( parcas_types(cngbr) /= 2 ) cycle           ! only O atoms calculated

                   if ( rijsww(cngbr) < coordmin ) then
                      coordination = coordination + 1.0_DP
                   else if ( rijsww(cngbr) >= coordmin .and. rijsww(cngbr) < coordmax ) then
                      help1 = (rijsww(cngbr) - Rco + Dco)/Dco
                      !coordination = coordination + 1.0_DP - 0.5_DP*help1 + sin(PII*help1)/TWOPI
                      !coordination = coordination + 0.5_DP*( 1.0_DP + sin(PII*(help1-0.5_DP)) )
                      ! coordination function by Kai Nordlund 26.6.2007:
                      coordination = coordination + 0.5_DP*( 1.0_DP + COS(help1/2.0_DP*PII) )

                   endif

                end do

                !---------------------------------------------------------------------------------------------------
                if ( wtj==1 .and. wti==1 .and. wtk==1 ) then      ! Si-Si-Si  

                   diff2ij = rijsw - r0_SiSi_2_SiSiSi
                   if ( diff2ij > cutofflimit ) cycle
                   diff2ik = riksw - r0_SiSi_2_SiSiSi
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiSi ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiSi*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiSi*dcos2sq
                   diff1ij = rijsw - r0_SiSi_1_SiSiSi
                   diff1ik = riksw - r0_SiSi_1_SiSiSi

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik < cutofflimit ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiSi*dcos1sq*dcos1
                      lambda1 = myy_1_SiSiSi
                      gamij1  = gamma_SiSi_1_SiSiSi*diff1ij
                      gamik1  = gamma_SiSi_1_SiSiSi*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiSi*dcos2sq*dcos2

                   if ( coordination > 0.00001 ) then   ! Kai: was 0.01 before V4.02
                      lambda2 = coordination
                      lambda2 = lambda2*lambda2
                      lambda2 = myy_2_SiSiSi*(1.0_DP + nyy_2_SiSiSi*exp(-ksi_2_SiSiSi*lambda2))
                   else
                      lambda2 = myy_2_SiSiSi
                   end if
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiSi*diff2ij
                   gamik2  = gamma_SiSi_2_SiSiSi*diff2ik
                   !---------------------------------------------------------------------------------------------------
                else if ( wtj==3 .and. wti==1 .and. wtk==1 ) then      ! Ge-Si-Si    

                   diff2ij = rijsw - r0_GeSi_2_GeSiGe   !In these we assume bond lengths independent 
                   if ( diff2ij > cutofflimit ) cycle
                   diff2ik = riksw - r0_SiSi_2_SiSiSi   !In these we assume bond lengths independent
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiSi ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiSi*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiSi*dcos2sq
                   diff1ij = rijsw - r0_GeSi_1_GeSiGe
                   diff1ik = riksw - r0_SiSi_1_SiSiSi

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik < cutofflimit ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiSi*dcos1sq*dcos1
                      lambda1 = myy_1_GeSiSi
                      gamij1  = gamma_SiSi_1_SiSiSi*diff1ij
                      gamik1  = gamma_SiSi_1_SiSiSi*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiSi*dcos2sq*dcos2

                   if ( coordination > 0.01 ) then   ! Kai: ????
                      lambda2 = coordination
                      lambda2 = lambda2*lambda2
                      lambda2 = myy_2_GeSiSi*(1.0_DP + nyy_2_SiSiSi*exp(-ksi_2_SiSiSi*lambda2))
                   else
                      lambda2 = myy_2_GeSiSi
                   end if
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiSi*diff2ij
                   gamik2  = gamma_SiSi_2_SiSiSi*diff2ik
                   !---------------------------------------------------------------------------------------------------
                else if ( wtj==1 .and. wti==1 .and. wtk==3 ) then      ! Si-Si-Ge    

                   diff2ij = rijsw - r0_SiSi_2_SiSiSi   ! In these we assume bond lengths independent 
                   if ( diff2ij > cutofflimit ) cycle
                   diff2ik = riksw - r0_GeSi_2_GeSiGe   ! In these we assume bond lengths independent
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiSi ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiSi*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiSi*dcos2sq
                   diff1ij = rijsw - r0_SiSi_1_SiSiSi
                   diff1ik = riksw - r0_GeSi_1_GeSiGe

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik < cutofflimit ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiSi*dcos1sq*dcos1
                      lambda1 = myy_1_GeSiSi
                      gamij1  = gamma_SiSi_1_SiSiSi*diff1ij
                      gamik1  = gamma_SiSi_1_SiSiSi*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiSi*dcos2sq*dcos2

                   if ( coordination > 0.01 ) then   ! Kai: ????
                      lambda2 = coordination
                      lambda2 = lambda2*lambda2
                      lambda2 = myy_2_GeSiSi*(1.0_DP + nyy_2_SiSiSi*exp(-ksi_2_SiSiSi*lambda2))
                   else
                      lambda2 = myy_2_GeSiSi
                   end if
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiSi*diff2ij
                   gamik2  = gamma_SiSi_2_SiSiSi*diff2ik

                   !---------------------------------------------------------------------------------------------------
                else if ( wtj==1 .and. wti==3 .and. wtk==1 ) then      ! Si-Ge-Si  

                   diff2ij = rijsw - r0_GeSi_2_SiGeSi   ! In these we assume bond lengths independent 
                   if ( diff2ij > cutofflimit ) cycle
                   diff2ik = riksw - r0_GeSi_2_SiGeSi   ! In these we assume bond lengths independent
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiSi ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiSi*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiSi*dcos2sq
                   diff1ij = rijsw - r0_GeSi_1_SiGeSi
                   diff1ik = riksw - r0_GeSi_1_SiGeSi

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik < cutofflimit ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiSi*dcos1sq*dcos1
                      lambda1 = myy_1_SiGeSi
                      gamij1  = gamma_SiSi_1_SiSiSi*diff1ij
                      gamik1  = gamma_SiSi_1_SiSiSi*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiSi*dcos2sq*dcos2

                   if ( coordination > 0.01 ) then   ! Kai: ????
                      lambda2 = coordination
                      lambda2 = lambda2*lambda2
                      lambda2 = myy_2_SiGeSi*(1.0_DP + nyy_2_SiSiSi*exp(-ksi_2_SiSiSi*lambda2))
                   else
                      lambda2 = myy_2_SiGeSi
                   end if
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiSi*diff2ij
                   gamik2  = gamma_SiSi_2_SiSiSi*diff2ik

                   !---------------------------------------------------------------------------------------------------
                else if ( wtj==3 .and. wti==3 .and. wtk==1 ) then      ! Ge-Ge-Si   

                   diff2ij = rijsw - r0_GeGe_2_GeGeGe   ! In these we assume bond lengths independent 
                   if ( diff2ij > cutofflimit ) cycle
                   diff2ik = riksw - r0_GeSi_2_SiGeSi   ! In these we assume bond lengths independent
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiSi ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiSi*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiSi*dcos2sq
                   diff1ij = rijsw - r0_GeGe_1_GeGeGe
                   diff1ik = riksw - r0_GeSi_1_SiGeSi

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik < cutofflimit ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiSi*dcos1sq*dcos1
                      lambda1 = myy_1_GeGeSi
                      gamij1  = gamma_SiSi_1_SiSiSi*diff1ij
                      gamik1  = gamma_SiSi_1_SiSiSi*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiSi*dcos2sq*dcos2

                   if ( coordination > 0.01 ) then   ! Kai: ????
                      lambda2 = coordination
                      lambda2 = lambda2*lambda2
                      lambda2 = myy_2_GeGeSi*(1.0_DP + nyy_2_SiSiSi*exp(-ksi_2_SiSiSi*lambda2))
                   else
                      lambda2 = myy_2_GeGeSi
                   end if
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiSi*diff2ij
                   gamik2  = gamma_SiSi_2_SiSiSi*diff2ik

                   !---------------------------------------------------------------------------------------------------
                else if ( wtj==1 .and. wti==3 .and. wtk==3 ) then      ! Si-Ge-Ge    

                   diff2ij = rijsw - r0_GeSi_2_SiGeSi   ! In these we assume bond lengths independent 
                   if ( diff2ij > cutofflimit ) cycle
                   diff2ik = riksw - r0_GeGe_2_GeGeGe   ! In these we assume bond lengths independent
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiSi ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiSi*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiSi*dcos2sq
                   diff1ij = rijsw - r0_GeSi_1_SiGeSi
                   diff1ik = riksw - r0_GeGe_1_GeGeGe

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik < cutofflimit ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiSi*dcos1sq*dcos1
                      lambda1 = myy_1_GeGeSi
                      gamij1  = gamma_SiSi_1_SiSiSi*diff1ij
                      gamik1  = gamma_SiSi_1_SiSiSi*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiSi*dcos2sq*dcos2

                   if ( coordination > 0.01 ) then   ! Kai: ????
                      lambda2 = coordination
                      lambda2 = lambda2*lambda2
                      lambda2 = myy_2_GeGeSi*(1.0_DP + nyy_2_SiSiSi*exp(-ksi_2_SiSiSi*lambda2))
                   else
                      lambda2 = myy_2_GeGeSi
                   end if
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiSi*diff2ij
                   gamik2  = gamma_SiSi_2_SiSiSi*diff2ik
                   !---------------------------------------------------------------------------------------------------
                else if ( wtj==3 .and. wti==3 .and. wtk==3 ) then      ! Ge-Ge-Ge  

                   diff2ij = rijsw - r0_GeGe_2_GeGeGe
                   if ( diff2ij > cutofflimit ) cycle
                   diff2ik = riksw - r0_GeGe_2_GeGeGe
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiSi ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiSi*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiSi*dcos2sq
                   diff1ij = rijsw - r0_GeGe_1_GeGeGe
                   diff1ik = riksw - r0_GeGe_1_GeGeGe

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik < cutofflimit ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiSi*dcos1sq*dcos1
                      lambda1 = myy_1_GeGeGe
                      gamij1  = gamma_SiSi_1_SiSiSi*diff1ij
                      gamik1  = gamma_SiSi_1_SiSiSi*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiSi*dcos2sq*dcos2

                   if ( coordination > 0.01 ) then   ! Kai: ????
                      lambda2 = coordination
                      lambda2 = lambda2*lambda2
                      lambda2 = myy_2_GeGeGe*(1.0_DP + nyy_2_SiSiSi*exp(-ksi_2_SiSiSi*lambda2))
                   else
                      lambda2 = myy_2_GeGeGe
                   end if
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiSi*diff2ij
                   gamik2  = gamma_SiSi_2_SiSiSi*diff2ik

                   !---------------------------------------------------------------------------------------------------
                else if ( wtj == 1 .and. wti == 1 .and. wtk == 2 ) then ! Si-Si-O

                   diff2ij = rijsw - r0_SiSi_2_SiSiO
                   if ( diff2ij >  cutofflimit) cycle
                   diff2ik = riksw - r0_SiO_2_SiSiO
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiO ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiO*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiO*dcos2sq
                   diff1ij = rijsw - r0_SiSi_1_SiSiO
                   diff1ik = riksw - r0_SiO_1_SiSiO

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik <cutofflimit  ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiO*dcos1sq*dcos1

                      if ( coordination > 0.0 ) then
                         lambda1 = coordination - z0_1_SiSiO
                         lambda1 = lambda1*lambda1
                         lambda1 = myy_1_SiSiO*(1.0_DP + nyy_1_SiSiO*exp(-ksi_1_SiSiO*lambda1))
                      else
                         lambda1 = myy_1_SiSiO
                      end if

                      gamij1  = gamma_SiSi_1_SiSiO*diff1ij
                      gamik1  = gamma_SiO_1_SiSiO*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiO*dcos2sq*dcos2
                   lambda2 = myy_2_SiSiO
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiO*diff2ij
                   gamik2  = gamma_SiO_2_SiSiO*diff2ik

                   !------------------------------------------------------------------------------------------
                else if ( wtj == 2 .and. wti == 1 .and. wtk == 1 ) then ! O-Si-Si

                   diff2ij = rijsw - r0_SiO_2_SiSiO
                   if ( diff2ij > cutofflimit ) cycle
                   diff2ik = riksw - r0_SiSi_2_SiSiO
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiO ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiO*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiO*dcos2sq
                   diff1ij = rijsw - r0_SiO_1_SiSiO
                   diff1ik = riksw - r0_SiSi_1_SiSiO

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik < cutofflimit ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiO*dcos1sq*dcos1
                      if ( coordination > 0.0 ) then
                         lambda1 = coordination - z0_1_SiSiO
                         lambda1 = lambda1*lambda1
                         lambda1 = myy_1_SiSiO*(1.0_DP + nyy_1_SiSiO*exp(-ksi_1_SiSiO*lambda1))
                      else
                         lambda1 = myy_1_SiSiO
                      end if
                      gamij1  = gamma_SiO_1_SiSiO*diff1ij
                      gamik1  = gamma_SiSi_1_SiSiO*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiO*dcos2sq*dcos2
                   lambda2 = myy_2_SiSiO

                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiO_2_SiSiO*diff2ij
                   gamik2  = gamma_SiSi_2_SiSiO*diff2ik

                   !---------------------------------------------------------------------------------------------------
                else if ( wtj == 3 .and. wti == 1 .and. wtk == 2 ) then ! Ge-Si-O

                   diff2ij = rijsw - r0_GeSi_2_GeSiO
                   if ( diff2ij >  cutofflimit) cycle
                   diff2ik = riksw - r0_SiO_2_SiSiO
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiO ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiO*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiO*dcos2sq
                   diff1ij = rijsw - r0_GeSi_1_GeSiO
                   diff1ik = riksw - r0_SiO_1_SiSiO

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik <cutofflimit  ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiO*dcos1sq*dcos1

                      if ( coordination > 0.0 ) then
                         lambda1 = coordination - z0_1_SiSiO
                         lambda1 = lambda1*lambda1
                         lambda1 = myy_1_GeSiO*(1.0_DP + nyy_1_SiSiO*exp(-ksi_1_SiSiO*lambda1))
                      else
                         lambda1 = myy_1_GeSiO
                      end if

                      gamij1  = gamma_SiSi_1_SiSiO*diff1ij
                      gamik1  = gamma_SiO_1_SiSiO*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiO*dcos2sq*dcos2
                   lambda2 = myy_2_GeSiO
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiO*diff2ij
                   gamik2  = gamma_SiO_2_SiSiO*diff2ik

                   !---------------------------------------------------------------------------------------------------
                else if ( wtj == 1 .and. wti == 3 .and. wtk == 2 ) then ! Si-Ge-O

                   diff2ij = rijsw - r0_GeSi_2_GeSiO
                   if ( diff2ij >  cutofflimit) cycle
                   diff2ik = riksw - r0_GeO_2_GeGeO
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiO ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiO*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiO*dcos2sq
                   diff1ij = rijsw - r0_GeSi_1_GeSiO
                   diff1ik = riksw - r0_GeO_1_GeGeO

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik <cutofflimit  ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiO*dcos1sq*dcos1

                      if ( coordination > 0.0 ) then
                         lambda1 = coordination - z0_1_SiSiO
                         lambda1 = lambda1*lambda1
                         lambda1 = myy_1_SiGeO*(1.0_DP + nyy_1_SiSiO*exp(-ksi_1_SiSiO*lambda1))
                      else
                         lambda1 = myy_1_SiGeO
                      end if

                      gamij1  = gamma_SiSi_1_SiSiO*diff1ij
                      gamik1  = gamma_SiO_1_SiSiO*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiO*dcos2sq*dcos2
                   lambda2 = myy_2_SiGeO
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiO*diff2ij
                   gamik2  = gamma_SiO_2_SiSiO*diff2ik
                   !---------------------------------------------------------------------------------------------------
                else if ( wtj == 2 .and. wti == 1 .and. wtk == 3 ) then ! O-Si-Ge

                   diff2ij = rijsw - r0_SiO_2_SiSiO
                   if ( diff2ij >  cutofflimit) cycle
                   diff2ik = riksw - r0_GeSi_2_GeSiO
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiO ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiO*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiO*dcos2sq
                   diff1ij = rijsw - r0_SiO_1_SiSiO
                   diff1ik = riksw - r0_GeSi_1_GeSiO

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik <cutofflimit  ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiO*dcos1sq*dcos1

                      if ( coordination > 0.0 ) then
                         lambda1 = coordination - z0_1_SiSiO
                         lambda1 = lambda1*lambda1
                         lambda1 = myy_1_GeSiO*(1.0_DP + nyy_1_SiSiO*exp(-ksi_1_SiSiO*lambda1))
                      else
                         lambda1 = myy_1_GeSiO
                      end if

                      gamij1  = gamma_SiSi_1_SiSiO*diff1ij
                      gamik1  = gamma_SiO_1_SiSiO*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiO*dcos2sq*dcos2
                   lambda2 = myy_2_GeSiO
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiO*diff2ij
                   gamik2  = gamma_SiO_2_SiSiO*diff2ik

                   !---------------------------------------------------------------------------------------------------
                else if ( wtj == 1 .and. wti == 3 .and. wtk == 2 ) then ! O-Ge-Si

                   diff2ij = rijsw - r0_GeO_2_GeGeO
                   if ( diff2ij >  cutofflimit) cycle
                   diff2ik = riksw - r0_GeSi_2_SiGeO
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiO ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiO*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiO*dcos2sq
                   diff1ij = rijsw - r0_GeO_1_GeGeO
                   diff1ik = riksw - r0_GeSi_1_SiGeO

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik <cutofflimit  ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiO*dcos1sq*dcos1

                      if ( coordination > 0.0 ) then
                         lambda1 = coordination - z0_1_SiSiO
                         lambda1 = lambda1*lambda1
                         lambda1 = myy_1_SiGeO*(1.0_DP + nyy_1_SiSiO*exp(-ksi_1_SiSiO*lambda1))
                      else
                         lambda1 = myy_1_SiGeO
                      end if

                      gamij1  = gamma_SiSi_1_SiSiO*diff1ij
                      gamik1  = gamma_SiO_1_SiSiO*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiO*dcos2sq*dcos2
                   lambda2 = myy_2_SiGeO
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiO*diff2ij
                   gamik2  = gamma_SiO_2_SiSiO*diff2ik

                   !---------------------------------------------------------------------------------------------------
                else if ( wtj == 3 .and. wti == 3 .and. wtk == 2 ) then ! Ge-Ge-O

                   diff2ij = rijsw - r0_GeGe_2_GeGeO
                   if ( diff2ij >  cutofflimit) cycle
                   diff2ik = riksw - r0_GeO_2_GeGeO
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiO ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiO*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiO*dcos2sq
                   diff1ij = rijsw - r0_GeGe_1_GeGeO
                   diff1ik = riksw - r0_GeO_1_GeGeO

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik <cutofflimit  ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiO*dcos1sq*dcos1

                      if ( coordination > 0.0 ) then
                         lambda1 = coordination - z0_1_SiSiO
                         lambda1 = lambda1*lambda1
                         lambda1 = myy_1_GeGeO*(1.0_DP + nyy_1_SiSiO*exp(-ksi_1_SiSiO*lambda1))
                      else
                         lambda1 = myy_1_GeGeO
                      end if

                      gamij1  = gamma_SiSi_1_SiSiO*diff1ij
                      gamik1  = gamma_SiO_1_SiSiO*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiO*dcos2sq*dcos2
                   lambda2 = myy_2_GeGeO
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiO*diff2ij
                   gamik2  = gamma_SiO_2_SiSiO*diff2ik


                   !---------------------------------------------------------------------------------------------------
                else if ( wtj == 2 .and. wti == 3 .and. wtk == 3 ) then ! O-Ge-Ge

                   diff2ij = rijsw - r0_GeO_2_GeGeO
                   if ( diff2ij >  cutofflimit) cycle
                   diff2ik = riksw - r0_GeGe_2_GeGeO
                   if ( diff2ik > cutofflimit ) cycle

                   cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                   dcos1 = cosjik - costheta0_1_SiSiO ! abs value in Ohta et al. but note here
                   if ( abs(dcos1) < -cutofflimit ) cycle
                   dcos2 = dcos1

                   dcos1sq = dcos1*dcos1
                   dcos2sq = dcos2*dcos2
                   coss1   = 2.0_DP*dcos1 + 3.0_DP*alpha_1_SiSiO*dcos1sq
                   coss2   = 2.0_DP*dcos2 + 3.0_DP*alpha_2_SiSiO*dcos2sq
                   diff1ij = rijsw - r0_GeO_1_GeGeO
                   diff1ik = riksw - r0_GeGe_1_GeGeO

                   ! Short range 3-body interaction
                   if ( diff1ij < cutofflimit .and. diff1ik <cutofflimit  ) then
                      diff1ij = 1.0_DP/diff1ij
                      diff1ik = 1.0_DP/diff1ik
                      bigtheta1 = dcos1sq + alpha_1_SiSiO*dcos1sq*dcos1

                      if ( coordination > 0.0 ) then
                         lambda1 = coordination - z0_1_SiSiO
                         lambda1 = lambda1*lambda1
                         lambda1 = myy_1_GeGeO*(1.0_DP + nyy_1_SiSiO*exp(-ksi_1_SiSiO*lambda1))
                      else
                         lambda1 = myy_1_GeGeO
                      end if

                      gamij1  = gamma_SiSi_1_SiSiO*diff1ij
                      gamik1  = gamma_SiO_1_SiSiO*diff1ik

                      biglambda1 = lambda1*exp( gamij1 + gamik1 )
                      gamij1 = gamij1*diff1ij
                      gamik1 = gamik1*diff1ik

                   else
                      lambda1 = 0.0
                      bigtheta1 = 0.0
                      biglambda1 = 0.0
                      gamij1 = 0.0
                      gamik1 = 0.0
                   endif

                   ! Long range 3-body interaction
                   bigtheta2 = dcos2sq + alpha_2_SiSiO*dcos2sq*dcos2
                   lambda2 = myy_2_GeGeO
                   diff2ij = 1.0_DP/diff2ij
                   diff2ik = 1.0_DP/diff2ik
                   gamij2  = gamma_SiSi_2_SiSiO*diff2ij
                   gamik2  = gamma_SiO_2_SiSiO*diff2ik

                   !---------------------------------------------------------------------------------

                else
                   cycle

                endif
             endif

             biglambda2 = lambda2*exp( gamij2 + gamik2 )
             gamij2 = gamij2*diff2ij
             gamik2 = gamik2*diff2ik
             lambdatheta1 = biglambda1*bigtheta1
             lambdatheta2 = biglambda2*bigtheta2

             hjik = epsilon*( lambdatheta1 + lambdatheta2 )

             help1 = biglambda1*coss1 + biglambda2*coss2
             help2 = help1*cosjik

             help3 = eosigma*help1
             help4 = eosigma*help2
             dphij1 = eosigma*( lambdatheta1*gamij1 + lambdatheta2*gamij2)
             dphij2 = help3/rijsw !k
             dphij3 = help4/rijsw
             dphik1 = eosigma*( lambdatheta1*gamik1 + lambdatheta2*gamik2)
             dphik2 = help3/riksw !j
             dphik3 = help4/riksw

             Ethree(atomi) = Ethree(atomi) + hjik
             Vthree = Vthree + hjik

             help3 = dxij*oprij
             help4 = dxik*oprik
             help1 = -dphij1*help3 + dphij2*dxik*oprik - dphij3*help3
             help2 = -dphik1*help4 + dphik2*dxij*oprij - dphik3*help4
             pbuf(i) = pbuf(i) - help1 - help2
             pbuf(j) = pbuf(j) + help1
             pbuf(k) = pbuf(k) + help2
             wxxi(atomi) = wxxi(atomi) - help1*dxij - help2*dxik

             if (calc_vir) then
                wxyi(atomi)=wxyi(atomi) - help1*dyij - help2*dyik
                wxzi(atomi)=wxzi(atomi) - help1*dzij - help2*dzik
             endif

             help3 = dyij*oprij
             help4 = dyik*oprik
             help1 = - dphij1*help3 + dphij2*dyik*oprik - dphij3*help3
             help2 = - dphik1*help4 + dphik2*dyij*oprij - dphik3*help4
             pbuf(i+1) = pbuf(i+1) - help1 - help2
             pbuf(j+1) = pbuf(j+1) + help1
             pbuf(k+1) = pbuf(k+1) + help2
             wyyi(atomi) = wyyi(atomi) - help1*dyij - help2*dyik

             if (calc_vir) then
                wyxi(atomi)=wyxi(atomi) - help1*dxij - help2*dxik
                wyzi(atomi)=wyzi(atomi) - help1*dzij - help2*dzik
             endif

             help3 = dzij*oprij
             help4 = dzik*oprik
             help1 = - dphij1*help3 + dphij2*dzik*oprik - dphij3*help3
             help2 = - dphik1*help4 + dphik2*dzij*oprij - dphik3*help4
             pbuf(i+2) = pbuf(i+2) - help1 - help2
             pbuf(j+2) = pbuf(j+2) + help1
             pbuf(k+2) = pbuf(k+2) + help2
             wzzi(atomi) = wzzi(atomi) - help1*dzij - help2*dzik

             if (calc_vir) then
                wzxi(atomi)=wzxi(atomi) - help1*dxij - help2*dxik
                wzyi(atomi)=wzyi(atomi) - help1*dyij - help2*dyik
             endif

          end do ! end of the 3-body loop over atoms k -------------------------

       end do ! end of the 2-body loop over atoms j ----------------------------

       wxxi(atomi) = wxxi(atomi)/(box(1)*box(1))
       wyyi(atomi) = wyyi(atomi)/(box(2)*box(2))
       wzzi(atomi) = wzzi(atomi)/(box(3)*box(3))

       if (calc_vir) then
          wxyi(atomi)=wxyi(atomi)/(box(1)*box(2))
          wxzi(atomi)=wxzi(atomi)/(box(1)*box(3))
          wyxi(atomi)=wyxi(atomi)/(box(2)*box(1))
          wyzi(atomi)=wyzi(atomi)/(box(2)*box(3))
          wzxi(atomi)=wzxi(atomi)/(box(3)*box(1))
          wzyi(atomi)=wzyi(atomi)/(box(3)*box(2))
       endif

       wxx = wxx+wxxi(atomi)
       wyy = wyy+wyyi(atomi)
       wzz = wzz+wzzi(atomi)

       if (calc_vir) then
          wxy=wxy+wxyi(atomi)
          wxz=wxz+wxzi(atomi)
          wyx=wyx+wyxi(atomi)
          wyz=wyz+wyzi(atomi)
          wzx=wzx+wzxi(atomi)
          wzy=wzy+wzyi(atomi)
       endif

       dx = buf(i)-ecm(1)
       dy = buf(i+1)-ecm(2)
       dz = buf(i+2)-ecm(3)
       rx = sqrt(dx*dx+dy*dy+dz*dz)

       nnsh = int(1.0d0+rx/maxr*(1.0d0*nsh-1.0d0))

       do j = nsh,nnsh,-1
          wxxsh(j) = wxxsh(j)+wxxi(atomi)
          wyysh(j) = wyysh(j)+wyyi(atomi)
          wzzsh(j) = wzzsh(j)+wzzi(atomi)
          natsh(j) = natsh(j)+1
       enddo

    end do ! end of the main loop over atoms i ---------------------------------

    !      write(*,*) 'Total pair energy and three-body energy', Vpair, Vthree   ! PLOT TO SEE BOTH COMPONENTS



    !-----------------------------------------------------------------------------
    ! Section 4 - The copy forces of myatoms to array a()
    !
    ! Also scale A to correspond to PARCAS xnp array units
    !-----------------------------------------------------------------------------
    do N = 1,myatoms
       n3 = N*3-2
       Epair(N) = ebuf(N)
       a(n3  ) = pbuf(n3  )/box(1)
       a(n3+1) = pbuf(n3+1)/box(2)
       a(n3+2) = pbuf(n3+2)/box(3)
    enddo

    !-----------------------------------------------------------------------------
    ! Section 5 - Send back accelerations of neighbour atoms to them
    !
    ! From the array dirbuf, we can figure out from which direction an
    ! atom has been received, and from ibuf what index it has had there. Thus
    ! we can send the pbuf and atom index information to other nodes,
    ! and when receiving it know which atom it belongs to.
    !
    !-----------------------------------------------------------------------------

    ! Note: wxx should not be summed over processors here, wxxsh should.
     
    call my_mpi_dsum(wxxsh, nsh)
    call my_mpi_dsum(wyysh, nsh)
    call my_mpi_dsum(wzzsh, nsh)
     
    call my_mpi_isum(natsh, nsh)
     

    i = 0
    IF(debug)PRINT *,'Watanabe pass back',myproc,NP,myatoms
    if (nprocs .gt. 1) then
       t1 = mpi_wtime()
       !        print *,'Stilweb loop 2, pbuf sendrecv',myproc
       !        Loop over directions
       do d = 1,8
          !           Loop over neighbours
          i = 0
          do j = myatoms+1,NP
             dfrom = dirbuf(j)+4
             if (dfrom .gt. 8) dfrom = dfrom-8
             if (d .eq. dfrom) then
                i = i+1
                if (i .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow2', int(myproc))
                ifrom = ibuf(j*2-1)
                isendbuf(i) = ifrom
                i3 = i*3-3
                j3 = j*3-3
                psendbuf(i) = ebuf(j)
                xsendbuf(i3+1) = pbuf(j3+1)
                xsendbuf(i3+2) = pbuf(j3+2)
                xsendbuf(i3+3) = pbuf(j3+3)
             endif
          enddo
          !           print *,'send',myproc,d,i,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !              print *,'received',myproc,d,ii
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !              print *,'received',myproc,d,i
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
          endif
          i = ii
          do ii = 1,i
             myi = isendbuf2(ii)
             if (myi .gt. myatoms) then
                print *,myproc,myatoms,myi,ii,i
                call my_mpi_abort('SW i too large', int(myproc))
             endif
             i3 = myi*3-3
             ii3 = ii*3-3
             Epair(myi) = Epair(myi)+psendbuf2(ii)
             a(i3+1) = a(i3+1)+xsendbuf2(ii3+1)/box(1)
             a(i3+2) = a(i3+2)+xsendbuf2(ii3+2)/box(2)
             a(i3+3) = a(i3+3)+xsendbuf2(ii3+3)/box(3)
          enddo
       enddo
       tmr(33) = tmr(33)+(mpi_wtime()-t1)
    endif

    if (subcmacc) then

       ! subtract the center of mass acceleration
       acm(1) = 0.0
       acm(2) = 0.0
       acm(3) = 0.0
       atomi = 0                                    ! TARPEETON ?
       do n = 1,myatoms*3-2,3
          atomi = atomi+1                            ! TARPEETON ?
          acm(1) = acm(1) + a(n)
          acm(2) = acm(2) + a(n+1)
          acm(3) = acm(3) + a(n+2)
       enddo
       call my_mpi_dsum(acm, 3)

       atomi = 0                                    ! TARPEETON ?
       do n = 1,myatoms*3-2,3
          atomi = atomi+1                            ! TARPEETON ?
          a(n) = a(n) - acm(1)/natoms
          a(n+1) = a(n+1) - acm(2)/natoms
          a(n+2) = a(n+2) - acm(3)/natoms
       enddo
    endif

    firsttime = .false.

    return

  end subroutine silica_wat_force



  !-----------------------------------------------------------------------------
  !
  ! silica_wat_init - Initialization routine for the module
  !
  ! Output values
  !   Assigns values to the following tables:
  !
  !   Array silica_wat_types maps the Parcas atom types given in md.in file 
  !   to the local type indeces hard-coded in this module.
  !   
  !   Array rcut contains the cut-off distances for each pair of atom types.
  !   Neighbourlist radius is skin*rcutmax, where rcutmax is the max value 
  !   in table rcut.
  !
  ! Warning: Includes hard-coded parameters
  !
  !-----------------------------------------------.-----------------------------

  subroutine init_silica_wat()
    use typeparam
    implicit none

    ! Scaling parameters for Ge, compared to Si
    real(kind=DP), parameter :: GeO_Lscale = 1.74_DP/1.61_DP ! Ge-O bond compared to Si-O, from S-Weber-pot.
    real(kind=DP), parameter :: GeGe_Lscale = 2.181_DP/2.0951_DP ! Ge-Ge bond compared to Si-Si
    real(kind=DP), parameter :: GeSi_Lscale = 2.1376_DP/2.0951_DP ! Ge-Si bond compared to Si-Si

    integer i,j

    allocate(wata_types(itypelow:itypehigh))
    do i = itypelow,itypehigh
       if ( element(i) == 'Si' ) then
          wata_types(i) = 1
       else if ( element(i) == 'O' ) then
          wata_types(i) = 2
       else if ( element(i) == 'Ge' ) then
          wata_types(i) = 3
       else
          wata_types(i) = 0
       endif
       !print *, 'wtype', i, element(i), wata_types(i)
    enddo

    do i = itypelow,itypehigh
       do j = itypelow,itypehigh
          rcut(i,j) = 1.05_DP* swsigma
       enddo
    enddo

    do i = itypelow,itypehigh         ! Now works for others than just Si and O as well (Ge).
       do j = itypelow,itypehigh
          if ( wata_types(i) == 1 .and. wata_types(j) == 1 ) rcut(i,j) = ucut*swsigma !=3.77118_DP
          if ( wata_types(i) == 1 .and. wata_types(j) == 2 ) rcut(i,j) = 1.4_DP*swsigma
          if ( wata_types(i) == 1 .and. wata_types(j) == 3 ) rcut(i,j) = ucut*swsigma*GeSi_Lscale
          if ( wata_types(i) == 2 .and. wata_types(j) == 1 ) rcut(i,j) = 1.4_DP*swsigma
          if ( wata_types(i) == 2 .and. wata_types(j) == 2 ) rcut(i,j) = 1.4_DP*swsigma
          if ( wata_types(i) == 2 .and. wata_types(j) == 3 ) rcut(i,j) = 1.4_DP*swsigma*GeO_Lscale
          if ( wata_types(i) == 3 .and. wata_types(j) == 1 ) rcut(i,j) = ucut*swsigma*GeSi_Lscale
          if ( wata_types(i) == 3 .and. wata_types(j) == 2 ) rcut(i,j) = 1.4_DP*swsigma*GeO_Lscale
          if ( wata_types(i) == 3 .and. wata_types(j) == 3 ) rcut(i,j) = ucut*swsigma*GeGe_Lscale

          ! Should not be used for wata potential, but may be used for external pair pots
          if (rcutin(i,j) >= 0.0d0) then
             rcut(i,j) = rcutin(i,j)
          endif

       enddo
    enddo

  end subroutine init_silica_wat

end module silica_wat_mod
