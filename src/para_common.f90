module para_common


use defs

!
!  Parratio defines how many atoms can be expected to exist
!  outside the present node in the Rneicut wide border region
!  which is passed to neighbouring nodes. Perfect value is 18.
!
!   Because of dump_atoms, buf size must be >= 9*NPMAX
!
!
integer, parameter :: PARRATIO = 5
!     ------------------------------------------------------------------
      REAL*8 tmr(99),rmn(3),rmx(3),dwrk(99),maxsize(3)
      real*8 buf(max(18,PARRATIO)*3*NPMAX/2)
      integer ibuf(max(PARRATIO,18)*2*NPMAX/2),dirbuf(PARRATIO*NPMAX/2),atypebuf
      real*8 pbuf(PARRATIO*3*NPMAX/2),dbuf(PARRATIO*NPMAX/2),ebuf(PARRATIO*NPMAX/2), &
        vbuf(3*NPMAX),abuf(3*NPMAX)
      real*8 xsendbuf(3*NPPASSMAX),xsendbuf2(3*NPPASSMAX)
      real*8 psendbuf(NPPASSMAX),psendbuf2(NPPASSMAX)
      integer isendbuf(2*NPPASSMAX),isendbuf2(2*NPPASSMAX)
      !integer isendbuf(3*NPPASSMAX),isendbuf2(3*NPPASSMAX) !Return it back, when Brenner is done
      integer*8 i8sendbuf(2*NPPASSMAX)
      integer np0pairtable,np0max


!     Dummy variable for AL_ defines, see defs.h
      integer al_dummy



!      COMMON/rpara/ buf(max(18,PARRATIO)*3*NPMAX/2),                     &
!     &     ibuf(max(PARRATIO,18)*2*NPMAX/2),dirbuf(PARRATIO*NPMAX/2),    &
!     &     pbuf(PARRATIO*3*NPMAX/2),dbuf(PARRATIO*NPMAX/2),    &
!     &     ebuf(PARRATIO*NPMAX/2),                                       &
!     &     vbuf(3*NPMAX),abuf(3*NPMAX),                                  &
!     &     xsendbuf(3*NPPASSMAX),xsendbuf2(3*NPPASSMAX),                 &
!     &     psendbuf(NPPASSMAX),isendbuf(2*NPPASSMAX),                    &
!     &     i8sendbuf(2*NPPASSMAX),                                       &
!     &     psendbuf2(NPPASSMAX),isendbuf2(2*NPPASSMAX),                  &
!     &     tmr(99),dwrk(99),maxsize(3),                                  &
!     &     np0pairtable,np0max
!      COMMON/rpara2/ rmn(3),rmx(3)

!     ------------------------------------------------------------------

      INTEGER myproc,nprocs,nnodes(3),myatoms,mxat,mnat,iwrk(99)
      integer nprocsreq,idebug
      ! iprint is .true. for the root processor and it's used to control
      ! printing to stdout.
      LOGICAL iprint




      logical sendfirst(8)
      logical debug

!      COMMON/ipara/ myproc,nprocs,nnodes(3),myatoms,mxat,mnat,iprint,     &
!     &              iwrk(99),debug,idebug,sendfirst(8)

!     ------------------------------------------------------------------
!     'Global' constants

      REAL*8, parameter ::   zero=0.0d0, &
  one=1.0d0, &
  two=2.0d0, &
  three=3.0d0, &
  four=4.0d0, &
  five=5.0d0, &
  six=6.0d0, &
  seven=7.0d0, &
  eight=8.0d0, &
  nine=9.0d0, &
  ten=10.0d0, &
  half=0.5d0, &
  quarter=0.25d0, &
  eighth=0.125d0, &
  pi=3.14159265358979323846264338327950d0
!      COMMON/numbers/ zero,one,two,three,four,five,six,seven,eight,       &
!     &                nine,ten,half,quarter,eighth,pi

!     ------------------------------------------------------------------

      integer passbit(8)
      data passbit /2,4,8,16,32,64,128,256/

      REAL*8 my_dmpi_time


end module para_common





