!***********************************************************************
! Initialize the simulation.
!***********************************************************************

subroutine Md_Init(x0,x1,atomindex,                                   &
     atype,natoms,nfixed,amp,initemp,tdebye,box,pbc,ncells,           &
     latflag,restartmode,mdlatxyz,nvac,vaczmin,fixzmin,fixzmax,fixperbrdr,fixxybrdr, &
     dsliceIn,ECMIn, Fzmaxz,dydtmaxz &
     )

  use typeparam
  use datatypes
  use my_mpi

  use defs  

  use para_common
  use casc_common

  implicit none

  !     ------------------------------------------------------------------
  !          Variables passed in and out
  integer atype(*),natoms,nfixed,ncells(*),atomindex(*),nvac
  real*8 x0(*),x1(*),box(*),pbc(*),amp,vaczmin,fixzmin,fixzmax,fixperbrdr,fixxybrdr
  real*8 Fzmaxz,dydtmaxz  
  real*8 initemp,tdebye
  integer latflag,mdlatxyz,restartmode
  real*8 ECMIn(3),dsliceIn(3) !slicing on restart reading

  !          Variables for parallel operation
!include 'para_common.f90'
  !     myatoms = number of atoms that my node is responsible for
  !     mxat = the maximum number of atoms on any node
  !     myproc = my relative processor number (0..nprocs-1)
  !     rmn(1..2),rmx(1..2) = x & y min & max boundaries for my nodes region
  !     nnodes(1) = number of nodes in x direction (columns)
  !     nnodes(2) = number of nodes in y direction (rows)
  !     ------------------------------------------------------------------
  !          Local variables 
  real*8 r,x,y,z

  integer i,j,n,i3,ii

  integer(kind=mpi_parameters) :: ierror  ! MPI error code

  !     ------------------------------------------------------------------

!include 'casc_common.f90'


  ! Choose the node arrangement to match the box(1)/box(2) ratio as closely
  !   as possible subject to the maximums from the input file.

  call Rectangle(box)

  if (nprocs>1 .and. MOD(nprocs,2)==1) then
     print *,'USE AN EVEN NUMBER OF PROCESSORS'
     print *,'I''ll try an odd number, but beware'
  endif

  ! Divide the region into areas for the different nodes

  i=MOD(myproc,nnodes(1))
  j=myproc/nnodes(1)
  rmn(1)=DBLE(i)/nnodes(1) - half
  rmx(1)=DBLE(i+1)/nnodes(1) - half
  rmn(2)=DBLE(nnodes(2)-j-1)/nnodes(2) - half
  rmx(2)=DBLE(nnodes(2)-j  )/nnodes(2) - half

  ! Figure out whether this is a border cell and thus what
  ! the open boundary condition limits should be

  if (pbc(1)/=1.0d0) then
     if (i==0) rmn(1)=-999.9d30
     if (i==nnodes(1)-1) rmx(1)=999.9d30
  endif
  if (pbc(2)/=1.0d0) then
     if (j==0) rmx(2)=999.9d30
     if (j==nnodes(2)-1) rmn(2)=-999.9d30
  endif

  ! Read in or generate the initial atomic positions
  !      IF (latflag  .eq. 0) THEN 
  !        CALL Fcc_Gen(x0,atomindex,atype,natoms,amp,box,pbc,ncells,nvac)
  !      ELSE IF (latflag.EQ.1.or.latflag.eq.3.or.latflag.eq.4) THEN 
  !        CALL Get_Atoms(x0,x1,atomindex,
  !     *        atype, natoms,amp,box,pbc,latflag)
  !      ELSE IF (latflag .EQ. 2) THEN 
  !        CALL DIA_Gen(x0,atomindex,atype,natoms,amp,box,pbc,ncells,nvac)
  !      ELSE 
  !         IF(iprint)WRITE (6,*) 'Unknown latflag',latflag
  !         CALL my_mpi_abort('latflag', int(latflag))
  !      ENDIF

  nfixed=0
  if (latflag == 1 .or. latflag == 3 .or. latflag == 4) then 
     call Get_Atoms(x0,x1,atomindex,atype,                              &
          natoms,nfixed,amp,initemp,tdebye,box,pbc,latflag,restartmode, &
          mdlatxyz,dsliceIn,ECMIn)
  else 
     call Cryst_Gen(x0,atomindex,atype,natoms,amp,initemp,tdebye,       &
          box,pbc,ncells,nvac,vaczmin,latflag)
  endif

  ! Count atom types
  do i=itypelow,itypehigh
     noftype(i)=0
  enddo
  do i=1,myatoms
     ii=ABS(atype(i))
     noftype(ii)=noftype(ii)+1
     if (ii < itypelow .or. ii > itypehigh) then
        write (6,*) 'ERROR: atom i',i,atomindex(i),'has type',ii
        write (6,*) 'outside of allowed range. Change ntype or atom type'
        call my_mpi_abort('Atom type error', INT(myproc))
     endif
  enddo
  call my_mpi_isum(noftype, itypehigh-itypelow+1)
  if (iprint) then
     write(6,'(A)') 'Number of atoms of different types'
     do i=itypelow,itypehigh
        write(6,'(I3,A,I7)') i,':',noftype(i)
     enddo
     write(6,'(A)') ' '
  endif

  if (fixzmax>-1.0d30) then
     n=0
     do i=1,myatoms
        z=x0(i*3)
        if (z*box(3)>=fixzmin.and.z*box(3)<=fixzmax) then
           if (atype(i) > 0) then
              ! Only fix if not fixed already
              atype(i)=-ABS(atype(i))
              n=n+1
           endif
        endif
     enddo
     call my_mpi_isum(n, 1)
     nfixed=nfixed+n
     IF(iprint)WRITE(6,'(A,2G13.5,A,I7)')                                        &
          'Number of atoms fixed between',                              &
          fixzmin,fixzmax,' is ',n
  endif
  if (fixperbrdr>zero) then
     n=0
     do i=1,myatoms
        i3=i*3-3
        x=x0(i3+1)*box(1)
        y=x0(i3+2)*box(2)
        z=x0(i3+3)*box(3)
        if ((pbc(1)==one.and.(x<-box(1)/2+fixperbrdr .or. x>box(1)/2-fixperbrdr)) .or. &
             (pbc(2)==one.and.(y<-box(2)/2+fixperbrdr .or. y>box(2)/2-fixperbrdr)) .or. &
             (pbc(3)==one.and.(z<-box(3)/2+fixperbrdr .or. z>box(3)/2-fixperbrdr)) ) then
           if (atype(i) > 0) then
              ! Only fix if not fixed already             
              atype(i)=-ABS(atype(i))
              n=n+1
           endif
        endif
     enddo
     call my_mpi_isum(n, 1)
     nfixed=nfixed+n
     IF(iprint)WRITE(6,'(A,G13.5,A,I7)')                                        &
          'Number of atoms fixed within ',fixperbrdr,' of periodic borders is',n
  endif
  if (fixxybrdr>zero) then
     n=0
     do i=1,myatoms
        i3=i*3-3
        x=x0(i3+1)*box(1)
        y=x0(i3+2)*box(2)
        z=x0(i3+3)*box(3)
        if ((x<-box(1)/2+fixxybrdr .or. x>box(1)/2-fixxybrdr) .or. &
             (y<-box(2)/2+fixxybrdr .or. y>box(2)/2-fixxybrdr) ) then
           if (atype(i) > 0) then
              ! Only fix if not fixed already             
              atype(i)=-ABS(atype(i))
              n=n+1
           endif
        endif
     enddo
     call my_mpi_isum(n, 1)
     nfixed=nfixed+n
     IF(iprint)WRITE(6,'(A,G13.5,A,I7)')                                        &
          'Number of atoms fixed within ',fixxybrdr,' of xy borders is',n
  endif

  if (dydtmaxz /= zero) then
     n=0
     do i=1,myatoms
        z=x0(i*3)
        if (z*box(3)>=Fzmaxz) then
           if (atype(i) > 0) then
              ! Only fix if not fixed already
              atype(i)=-abs(atype(i))
              n=n+1
           endif
        endif
     enddo
     call my_mpi_isum(n,1)
     nfixed=nfixed+n
     IF(iprint)WRITE(6,'(A,1G13.5,A,I7)')                                        &
          'Number of atoms fixed for dydtzmax above',                   &
          Fzmaxz,' is ',n
  endif

  IF(iprint)WRITE (6,'(A,I7)')                                                   &
       'Total number of fixed atoms',nfixed

  ! Redivide the atoms between nodes if there is an imbalance

  mnat=myatoms
  mxat=myatoms
  call my_mpi_imin(mnat, 1)
  call my_mpi_imax(mxat, 1)
  if (DBLE(mnat)/mxat .lt. 0.9d0) then
     IF(iprint)WRITE(6,*)'Redivide buggy, not using it'
     !        IF(iprint)WRITE(6,*)'Entering redivide the balance load'
     !        CALL Redivide(x0,atype, natoms)
  endif

  IF(iprint)WRITE(6,*)
  call mpi_barrier(mpi_comm_world, ierror)
  write(6,1020) myproc,rmn(1),rmx(1),rmn(2),rmx(2),myatoms
  call mpi_barrier(mpi_comm_world, ierror)

  IF(iprint)WRITE(6,*)
1020 format(i4,' x =',g15.6,' to',g15.6,' y =',g15.6,' to',g15.6,          &
       i6,' atoms')
   

end subroutine Md_Init


!***********************************************************************
! Generate initial gaussian velocities for atoms
!***********************************************************************
subroutine gaussvel(initemp,np,x1,box)
  use my_mpi
  use defs  

  use para_common
  use random
  implicit none
  !     
  !     Input: initemp = T to set in units of eV (kB*T/e)
  !            np = Number of particles
  !            x1(*) = velocity array, units of v/box
  !            box(3) = box size in A
  !
  !  If initemp is negative, all atoms are given same velocity
  !  Otherwise gaussian distribution is used
  !
!include 'para_common.f90'

  integer np
  real*8 initemp,x1(*),box(*)
!  real*8 gasdev
  real*8 theta,phi
!  real*8 MyRanf

  integer np3,i,nptot
  real*8 temp,vx,vy,vz,std
  integer*4 isnumb

  np3=np*3

  !     Factor 2 due to equipartition theorem
  std = SQRT(2.0*ABS(initemp))
  vx = 0.0d0
  vy = 0.0d0
  vz = 0.0d0
  do i=1,np3,3

     if (initemp .gt. 0.0) then
        !             Gaussian distributed velocities
        x1(i) = std*gasdev(0)/box(1)
        x1(i+1) = std*gasdev(0)/box(2)
        x1(i+2) = std*gasdev(0)/box(3)
     else
        !  TODO: What!? - Same velocity for every atom. Factor sqrt(1.5) is ad hoc
        phi=MyRanf(0)*2.0*pi
        theta=ACOS(COS(pi)+MyRanf(0)*(1.0d0-COS(pi)))
        x1(i) = SQRT(1.5)*std*SIN(theta)*COS(phi)/box(1)
        x1(i+1) = SQRT(1.5)*std*SIN(theta)*SIN(phi)/box(2)
        x1(i+2) = SQRT(1.5)*std*COS(theta)/box(3)
     endif
     vx = vx+x1(i)
     vy = vy+x1(i+1)
     vz = vz+x1(i+2)
  enddo
  call my_mpi_dsum(vx, 1)
  call my_mpi_dsum(vy, 1)
  call my_mpi_dsum(vz, 1)
  nptot=np
  call my_mpi_isum(nptot, 1)
  vx = vx/nptot
  vy = vy/nptot
  vz = vz/nptot
  do i=1,np3,3
     x1(i) = x1(i)-vx
     x1(i+1) = x1(i+1)-vy
     x1(i+2) = x1(i+2)-vz
  enddo

  return
end subroutine gaussvel


!***********************************************************************
! Open and read in the parameter file for the md run
!***********************************************************************
subroutine Readinfile(f,nl,mdinsize)
  use my_mpi
  use defs  

  use para_common

  implicit none
  !
  !  Subroutine is always called in scalar form
  !
  integer nl,mdinsize,i,j
  character*120 sbuf,f(mdinsize)

!include 'para_common.f90'

1005 format (a80)

  open(5,file='in/md.in',status='old')

  nl=1
10 continue
  read(5,1005,end=80) f(nl)
  nl=nl+1
  if (nl .gt. mdinsize) then
     write(6,*) 'md.in array too small, please'
     write(6,*) 'increase mdinsize in read_params()'
     call my_mpi_abort('md.in array too small', INT(myproc))
  endif
  goto 10
80 continue
  nl=nl-1
  IF(iprint)WRITE(6,*) 'Read in',nl,' lines of md.in'

  close(5)

  ! Scan through file and replace any tabs with spaces !
  do i=1,nl
     do j=1,LEN_TRIM(f(i))
        if (ICHAR(f(i)(j:j))==9) f(i)(j:j)=' '
     enddo
  enddo


  IF(iprint)OPEN(7,FILE='out/md.out',STATUS='unknown')


  return
end subroutine Readinfile

!***********************************************************************
! Read in parameters for the md run
!***********************************************************************

subroutine Read_Params(f,nl,                                          &
     nsteps,tmax,deltaini,natoms,box,ncells,                          &
     pbc,mtemp,temp,toll,damp,pscale,remrot,amp,tdebye,latflag,mdlatxyz,nprtbl, &
     ndump,nmovie,timekt,timeEt,timeCh,                               &
     tscaleth,tscalzmin,tscalzmax,tscalxout,tscalyout,                &
     neiskinr,Ekdef,Ekrec,ndefmovie,nliqanal,                         &
     temp0,trate,ntimeini,timeini,nrestart,bpcbeta,               &
     bpctau,bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,bpcmode,                       &
     btctau,restartmode,restartt,potmode,spline,eamnbands,Fpextrap,initemp,nvac,vaczmin,    &
     melstop,melstopst,elstopmin,tmodtau,rsnum,rsmode,endtemp,dtmovie,tmovie,dtslice,  &
     tslice,dslice,ECM,ECMfix,dsliceIn,ECMIn,sw2mod,sw3mod,prandom,mrandom,          &
     timerandom,Fzmaxz,FzmaxYz,FzmaxZz,Fzmaxzr,Fzmaxtyp,dydtmaxz,     &
     fixzmin,fixzmax,fixperbrdr,fixxybrdr,                            &
     reppotcutin,elstopsputlim,timesputlim,nisputlim,tscalsputlim,    &
     taddvel,zaddvel,eaddvel,vaddvel,Emaxbrdr,moviemode,binmode,slicemode,fdamp,        &
     addvelt,addvelp,Tcontrolt,trackx,tracky,trackt,forcesum,sumatype,zipout,avgvir,dydttime, &
     outtype,outzmin,outzmax,outzmin2,outzmax2,virsym,virkbar,virboxsiz,R1CC,R2CC, &
     elfield,timerelE,Erate,rampmode,iabove,fullsolve,aroundapx,mctemp,evpintval,&
     zmax,evpatype,clneicut,elmigrat,qforces, edmethod) ! SP: ED-MD parameters

  use typeparam

  use basis

  use PhysConsts      

  ! PE: start
  use Temp_Time_Prog
  ! PE: end

  use defs  

  use para_common
  use casc_common

  use random

  implicit none

  !KN
  !  Modified this subroutine to enable input/output in arbitrary order
  !  to make using mdsd a little easier. Also provides for
  !  conditional reading in of variables (the readc versions of the
  !  subroutines), in which case a default value is provided in
  !  case the data line isn't found
  !
  !  The input file format must still be 9-char identifier + "="
  !

  !      IMPLICIT REAL*8  (a-h,o-z)
  !      IMPLICIT INTEGER (i-n)

  !     ------------------------------------------------------------------
  !          Variables passed in and out
  character*120 f(*)
  integer nl

  ! maximum number of time steps [out]
  integer :: nsteps
  ! number of atoms [out]
  integer :: natoms
  ! number of cells [out]
  integer ::ncells(3)
  ! temperature control mode, see README.DOCUMENTATION [out]
  integer :: mtemp
  !  Number of vacancies to create [out]
  integer :: nvac
  ! Only do temperature control on this atom type [out]
  integer :: Tcontrolt
  ! used to determine how the initial atom coordinates are obtained [out]
  integer :: latflag
  ! Controls whether mdlat.in or mdlat.in.xyz is read, value 1 means .xyz [out]
  integer :: mdlatxyz
  ! number of steps between pair table calculations [out]
  integer :: nprtbl
  ! number of steps between output to stdout [out]
  integer :: ndump
  ! number of steps between movie output, values <= 0 for no movie output.
  ! The exact positive value is not used when dtmov(1) > 0. [out]
  integer :: nmovie
  ! Number of steps between writing pressures.out [out]
  integer :: ndefmovie

  ! whether to output summed forces, value 1 means yes. Only sum atoms of type sumatype [out]
  integer :: forcesum
  ! atom type to use for force summing [out]
  integer :: sumatype
  ! number of steps between liquid analysis output [out]
  integer :: nliqanal
  ! number of steps between restart output [out]
  integer :: nrestart
  ! maximum timestep [out]
  real*8 :: deltaini
  ! size of simulation box in 3d [out]
  real*8 :: box(3)
  ! Periodic Boundary Conditions, value 1 means periodic and 0 means open [out]
  real*8 :: pbc(3)
  ! Desired temperature [out]
  real*8 :: temp
  ! tolerance for temperature, don't scale if temperature is within toll of temp [out]
  real*8 :: toll
  ! damping factor, TODO: what is it? [out]
  real*8 :: damp
  ! Amplitude of initial displacement (Angstroms), TODO: what is it? [out]
  real*8 :: amp
  real*8 :: vaczmin
  integer pscale,remrot

  real*8 timekt,timeEt,timeCh,fixzmin,fixzmax,fixperbrdr,fixxybrdr,reppotcutin
  real*8 tmax,tscaleth,tscalzmin,tscalzmax,tscalxout,tscalyout
  real*8 neiskinr,Ekdef,Ekrec
  real*8 taddvel,zaddvel,eaddvel,vaddvel,Emaxbrdr,addvelt,addvelp

  real*8 temp0,trate,restartt,initemp,endtemp,timeini,tdebye
  real*8 elstopmin,elstopsputlim,timesputlim,nisputlim,tscalsputlim

  integer ntimeini,potmode,spline,eamnbands,Fpextrap,melstop,melstopst,rsnum,rsmode

  integer avgvir
  integer dydttime
  integer outtype
  real*8 outzmin,outzmax
  real*8 outzmin2,outzmax2
  integer virsym
  integer virkbar
  real*8 virboxsiz

  real*8 bpcbeta,bpctau,bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,tmodtau
  integer bpcmode
  real*8 btctau
  real*8 dtmovie(*),tmovie(*),dtslice(*),tslice(*)
  real*8 dslice(*),ECM(*),sw2mod(3),sw3mod(3)
  real*8 dsliceIn(*),ECMIn(*)
  integer ECMfix
  real*8 prandom,mrandom,timerandom
  real*8 Fzmaxz,FzmaxYz,FzmaxZz,Fzmaxzr,fdamp,dydtmaxz
  integer Fzmaxtyp
  integer moviemode,binmode,slicemode,restartmode

  real*8 trackx,tracky,trackt

  ! Write output to compressed files
  integer zipout

!      BrennerBeardmore params
  real*8 R1CC,R2CC

  ! SP: ED-MD options
  real*8 elfield,Erate,timerelE,mctemp,zmax,clneicut, elmigrat, finite_size_effect
  integer iabove,evpintval,evpatype,fullsolve,aroundapx,rampmode, qforces, edmethod

  !          Variables for parallel operation
!include 'para_common.f90'
  !     nprocsreq = total number of nodes requested for use
  !     nprocs = total number of nodes being used
  !     nnodes(1) = number of nodes in x direction (columns)
  !     nnodes(2) = number of nodes in y direction (rows)
  !     ------------------------------------------------------------------
  !          Local variables 

  character*80 sbuf

  integer i,j,iseed
  real*8 x,ranf
!
!  real*8 MyRanf
!  external MyRanf

  !     ------------------------------------------------------------------

  ! Read input parameters from md.in

!include 'casc_common.f90'

1005 format (a80)
1010 format (a10)
1011 format (a10,i7)
1012 format (a10,f13.5)
1013 format (a79)

  IF(iprint)WRITE(7,*) f(1)
  IF(iprint)WRITE(7,*) f(2)

  if (nprocs .gt. 1) then
     IF(iprint)WRITE(7,1020) nprocs
1020 format('          PARCAS started on ',i4,' processors',/)
  else
     IF(iprint)WRITE(7,*)'        PARCAS started in scalar mode'
  endif

  IF(iprint)WRITE (7,*) 'Reading data from input file of',nl,' lines.'
  ! Avoid excessive stdout output
  !      WRITE (6,*) 'Reading data from input file of',nl,' lines.'

  call readcint('debug ',idebug,f,nl,0)
  debug=.false.
  if (idebug .eq. 1 .or. idebug .eq. 3) debug=.true.
  IF(debug)PRINT *,'Debug mode selected'

  call readint('nsteps ',nsteps,f,nl)
  call readreal('tmax ',tmax,f,nl)
  call readcreal('endtemp ',endtemp,f,nl,0.0d0)
  endtemp=endtemp/invkBeV

  !     Type parameters

  call readcint('ntype ',ntype,f,nl,1)
  ! Set bounds for type arrays
  if (ntype==1) then
     itypelow=1; itypehigh=1
  else 
     itypelow=0; itypehigh=ntype-1
  endif

  IF(iprint)WRITE(6,*) 'Allocating type arrays',itypelow,itypehigh
  ! 1D arrays
  allocate(mass(itypelow:itypehigh))
  allocate(timeunit(itypelow:itypehigh))
  allocate(vunit(itypelow:itypehigh),aunit(itypelow:itypehigh))
  allocate(element(itypelow:itypehigh))
  allocate(noftype(itypelow:itypehigh))
  allocate(atomZ(itypelow:itypehigh))

  ! SP: ED-MD stuff
  allocate(ionization_potential(itypelow:itypehigh)) ! SP
  allocate(work_function(itypelow:itypehigh)) ! SP

  ! 2d arrays
  allocate(rcut(itypelow:itypehigh,itypelow:itypehigh))
  allocate(rcutin(itypelow:itypehigh,itypelow:itypehigh))
  allocate(iac(itypelow:itypehigh,itypelow:itypehigh))


  call readrealarray('mass ',itypelow,itypehigh,mass,f,nl)
  call readstrarray('name ',itypelow,itypehigh,element,f,nl)
  call readcstr('substrate',substrate,f,nl,'NONE')

  call readcrealarray('ionpot ',itypelow,itypehigh,ionization_potential,f,nl,0d0)
  call readcrealarray('wrkfn ',itypelow,itypehigh,work_function,f,nl,0d0)

  ! These are only needed for elstop straggling calculation
  call readcrealarray('atomZ ',itypelow,itypehigh,atomZ,f,nl,-1.0d0)


  call readreal('delta ',deltaini,f,nl)

  ! Automatic symmetry is applied by the .true. variable
  call readcint2darray('iac ',itypelow,itypehigh,                       &
       itypelow,itypehigh,iac,f,nl,1,.true.)
  call readcreal2darray('rcin ',itypelow,itypehigh,itypehigh,           &
       itypelow,itypehigh,rcutin,f,nl,-1.0d0,.true.)

  !     KN general time parameters (not type-dependent)

  call readcreal('timekt ',timekt,f,nl,0.1d0)
  call readcreal('timeEt ',timeEt,f,nl,30.0d0)
  call readcreal('timeCh ',timeCh,f,nl,1.1d0)

  call readcreal('tscaleth ',tscaleth,f,nl,0.0d0)
  call readcreal('tscalzmin',tscalzmin,f,nl,1d30)
  call readcreal('tscalzmax',tscalzmax,f,nl,1d30)
  call readcreal('tscalxout',tscalxout,f,nl,1d30)
  call readcreal('tscalyout',tscalyout,f,nl,1d30)

  call readcreal('Emaxbrdr ',Emaxbrdr,f,nl,1d30)
  call readcreal('neiskinr ',neiskinr,f,nl,1.3d0)

  !     These added by H. Ristolainen 12.6.2008
  call readcint('forcesum ',forcesum,f,nl,0) 
  call readcint('sumatype ',sumatype,f,nl,0)

  call readint('natoms ',natoms,f,nl)

  call readrealarray('box ',1,3,box,f,nl)

  call readintarray('ncell ',1,3,ncells,f,nl)
  if (ncells(1)<=0 .or. ncells(2)<=0 .or. ncells(3)<=0) then
     print *,'ncells() must be larger than 0!'
  endif

  call readrealarray('pb ',1,3,pbc,f,nl)

  call readcint('nvac ',nvac,f,nl,0)
  call readcreal('vaczmin ',vaczmin,f,nl,-1.0d30)

  call readcreal('fixzmin ',fixzmin,f,nl,-1.0d30)
  call readcreal('fixzmax ',fixzmax,f,nl,-1.0d30)
  call readcreal('fixper ',fixperbrdr,f,nl,0.0d0)
  call readcreal('fixxy ',fixxybrdr,f,nl,0.0d0)

  call readcreal('taddvel ',taddvel,f,nl,0.0d0)
  call readcreal('zaddvel ',zaddvel,f,nl,0.0d0)
  call readcreal('eaddvel ',eaddvel,f,nl,0.0d0)
  call readcreal('vaddvel ',vaddvel,f,nl,0.0d0)


  call readcreal('addvelt ',addvelt,f,nl,180.0d0)
  call readcreal('addvelp ',addvelp,f,nl,0.0d0)
  addvelt=addvelt*pi/180.0;
  addvelp=addvelp*pi/180.0;

  call readint('mtemp ',mtemp,f,nl)
  IF(iprint)WRITE (6,'(A,I2,A)')                                                 &
       'Temperature control mode  ',mtemp,' selected'

  call readcint('Tcontrolt',Tcontrolt,f,nl,-1)
  call readcreal('temp0 ',temp0,f,nl,0.0d0)
  call readcint('ntimeini ',ntimeini,f,nl,99999999)
  call readcreal('timeini ',timeini,f,nl,1.0d30)
  call readcreal('initemp ',initemp,f,nl,0.0d0)
  call readcreal('tdebye ',tdebye,f,nl,0.0d0)
  call readreal('temp ',temp,f,nl)
  call readcreal('trate ',trate,f,nl,1.0d0)
  call readcreal('fdamp ',fdamp,f,nl,0.9d0)
  if (mtemp .ne. 3) then
     initemp=initemp/invkBeV
     tdebye=tdebye/invkBeV
     temp=temp/invkBeV
     temp0=temp0/invkBeV
     trate=trate/invkBeV
  endif

  ! read variables for temperature-time program
  TT_activated=.false.
  call readcint('TTact ',i,f,nl,0)
  if (i.ne.0) TT_activated=.true.
  call readcrealarray('TTtim ',1,TT_maxnsteps,TT_time,f,nl,1.0D30)
  call readcintarray('TTmt ',1,TT_maxnsteps,TT_mtemp,f,nl,0) 
  call readcrealarray('TTtem ',1,TT_maxnsteps,TT_temp,f,nl,0.0d0)
  call readcrealarray('TTtr ',1,TT_maxnsteps,TT_trate,f,nl,0.0d0)
  ! consistency check, print out of summary, unit conversion
  if (TT_activated) call Init_Temp_Time_Prog(tscaleth)

  call readcreal('bpcbeta ',bpcbeta,f,nl,0.0d0)
  call readcreal('bpctau ',bpctau,f,nl,100.0d0)
  call readcreal('bpcP0 ',bpcP0,f,nl,0.0d0)  
  bpcP0x=bpcP0
  bpcP0y=bpcP0
  bpcP0z=bpcP0
  call readcreal('bpcP0x ',bpcP0x,f,nl,bpcP0x)
  call readcreal('bpcP0y ',bpcP0y,f,nl,bpcP0y)
  call readcreal('bpcP0z ',bpcP0z,f,nl,bpcP0z)
  call readcint('bpcmode ',bpcmode,f,nl,1)

  call readcreal('bpcextz ',bpcextz,f,nl,0.0d0)      

  call readcreal('tmodtau ',tmodtau,f,nl,0.0d0)
  if (tmodtau .ne. 0.0 .and. bpctau .eq. 0.0) then
     IF(iprint)WRITE (6,*) 'WARNING: tmodtau not meaningful'
     IF(iprint)WRITE (6,*) 'when bpctau=0'
  endif

  call readcreal('btctau ',btctau,f,nl,0.0d0)

  call readreal('toll ',toll,f,nl)
  if (mtemp .ne. 3) toll=toll/invkBeV

  call readreal('amp ',amp,f,nl)

  call readcint('pscale ',pscale,f,nl,0)
  call readcint('remrot ',remrot,f,nl,0)

  call readreal('damp ',damp,f,nl)
  call readcreal('prandom ',prandom,f,nl,0.0d0)
  call readcreal('mrandom ',mrandom,f,nl,10.0d0)
  call readcreal('timerand ',timerandom,f,nl,1.0d20)

  call readcreal('Fzmaxz ',Fzmaxz,f,nl,0.0d0)
  call readcreal('Fzmaxzr ',Fzmaxzr,f,nl,0.0d0)
  call readcreal('FzmaxYz ',FzmaxYz,f,nl,0.0d0) 
  call readcreal('FzmaxZz ',FzmaxZz,f,nl,0.0d0)
  call readcreal('dydtmaxz ',dydtmaxz,f,nl,0.0d0)
  call readcint('Fzmaxtyp ',Fzmaxtyp,f,nl,-1)

  call readcint('avgvir ',avgvir,f,nl,0)
  call readcint('dydttime ',dydttime,f,nl,0)
  call readcint('outtype ',outtype,f,nl,0)
  call readcreal('outzmin ',outzmin,f,nl,0.0d0)
  call readcreal('outzmax ',outzmax,f,nl,0.0d0)
  call readcreal('outzmin2 ',outzmin2,f,nl,0.0d0)
  call readcreal('outzmax2 ',outzmax2,f,nl,0.0d0)
  call readcint('virsym ',virsym,f,nl,0)
  call readcint('virkbar ',virkbar,f,nl,0)
  call readcreal('virboxsiz',virboxsiz,f,nl,1.0d0)

  call readcint('melstop ',melstop,f,nl,0)
  call readcint('melstopst ',melstopst,f,nl,0)   ! Elstop straggling?
  call readcreal('elstopmin ',elstopmin,f,nl,1.0d0)

  call readcreal('sputlim ',timesputlim,f,nl,1.0d0)
  call readcreal('nisputlim',nisputlim,f,nl,1.0d30)
  call readcreal('essputlim',elstopsputlim,f,nl,timesputlim)
  call readcreal('tssputlim',tscalsputlim,f,nl,0.5d0)

  call readint('latflag ',latflag,f,nl)
  call readcint('mdlatxyz ',mdlatxyz,f,nl,0)

  if (latflag == 5) then
     IF(iprint)WRITE (6,*) 'Reading in atom basis' 
     call readint('nbasis ',nreadbasis,f,nl) 

     call readrealarray('offset ',1,3,readoffset,f,nl)

     call readreal2darray('lx ',1,nreadbasis,128,1,3,readbasis,         &
          f,nl,.false.)
     call readcintarray('ltype ',1,nreadbasis,readtype,f,nl,1) 

     call readcrealarray('pchang ',1,nreadbasis,                        &
          changeprob,f,nl,0.d0)
     call readcintarray('changt ',1,nreadbasis,changeto,f,nl,1)

  endif

  call readcint('restartm ',restartmode,f,nl,0)
  call readcreal('restartt ',restartt,f,nl,0.0d0)

  call readint('nprtbl ',nprtbl,f,nl)

  call readint('ndump ',ndump,f,nl)
  call readint('nmovie ',nmovie,f,nl)
  call readcint('moviemode',moviemode,f,nl,1)
  call readcint('binmode  ',binmode,f,nl,31)
  call readcint('slicemode',slicemode,f,nl,1)
  call readcint('zipout ',zipout,f,nl,0)

  call readcrealarray('dtmov ',1,9,dtmovie,f,nl,0.0d0)
  call readcrealarray('tmov ',1,9,tmovie,f,nl,1.0d30)

  call readcrealarray('dslice ',1,3,dslice,f,nl,-1.0d0)
  call readcrealarray('ECM ',1,3,ECM,f,nl,0.0d0)
  call readcint('ECMfix ',ECMfix,f,nl,0)
  call readcrealarray('dtsli ',1,9,dtslice,f,nl,1000.0d0)
  call readcrealarray('tsli ',1,9,tslice,f,nl,1.0d30)

  call readcrealarray('dCutIn ',1,3,dsliceIn,f,nl,-1.0d0)
  call readcrealarray('ECMIn ',1,3,ECMIn,f,nl,0.0d0)

  if ((dslice(1)>=0.0.or.dslice(2)>=0.0.or.dslice(3)>=0.0) .and. slicemode /= 9) then
     IF(iprint)OPEN(11,file='out/slice.movie',status='replace')
  endif


  call readcint('ndefmovie ',ndefmovie,f,nl,0)

  call readcint('nliqanal ',nliqanal,f,nl,0)

  call readcint('nrestart ',nrestart,f,nl,500)

  if (nmovie > 0 .and. moviemode /= 9) then
     IF(iprint)OPEN(10,FILE='out/md.movie',status='replace')
  endif

  if (moviemode == 16 .or. moviemode == 18) then
     IF(iprint)OPEN(16,FILE='out/block_atoms.dat',status='replace')
     IF(iprint)OPEN(18,FILE='out/block_natoms.dat',status='replace')
  endif

  call readcint('mx_Xnodes ',nnodes(1),f,nl,0)
  call readcint('mx_Ynodes ',nnodes(2),f,nl,0)

  !     
  !     KN parameters for recoil atom
  !     
  call readcint('irec ',irec,f,nl,0)
  call readcint('recatype ',recatype,f,nl,1)
  call readcint('recatypem ',recatypem,f,nl,0)

  if (irec /= 0 .and. iprint) then
     write (6,'(A,I5,A)') '*** Recoil atom mode',irec,' chosen ***'
     write (6,'(A,2I5)') '*** recatype,recatypem',recatype,recatypem
  endif
  xrec=1e30
  yrec=1e30
  zrec=1e30
  if (irec .lt. 0) then
     call readreal('xrec ',xrec,f,nl)
     call readreal('yrec ',yrec,f,nl)
     call readreal('zrec ',zrec,f,nl)
  endif

  call readcreal('recen ',recen,f,nl,0.0d0)
  call readcreal('rectheta ',rectheta,f,nl,0.0d0)
  call readcreal('recphi ',recphi,f,nl,0.0d0)
  rectheta=rectheta*pi/180.0
  recphi=recphi*pi/180.0

  !     Parameter to determine whether recoil sequences should be used
  !     If they are, recoil data read in from recoildata.in

  call readcint('rsnum ',rsnum,f,nl,0)
  call readcint('rsmode ',rsmode,f,nl,1)
  !     
  !     Initialize random number generator
  !     To get a different series for each processor, add myproc !
  !     
  call readcint('seed ',iseed,f,nl,2347834)
  x=MyRanf(iseed+myproc*37)

  call readcreal('Ekdef ',Ekdef,f,nl,0.0d0)
  call readcreal('Ekrec ',Ekrec,f,nl,0.1d0)

  call readcint('potmode ',potmode,f,nl,1)
  call readcint('spline ',spline,f,nl,1)
  call readcint('eamnbands',eamnbands,f,nl,1)
  call readcint('Fpextrap ',Fpextrap,f,nl,0)
  call readcreal('reppotcut ',reppotcutin,f,nl,10.0d0)
  call readcrealarray('sw2mod ',1,3,sw2mod,f,nl,1.0d0)
  call readcrealarray('sw3mod ',1,3,sw3mod,f,nl,1.0d0)

  ! track params
  call readcreal('trackx ',trackx,f,nl,0.0d0)
  call readcreal('tracky ',tracky,f,nl,0.0d0)
  call readcreal('trackt ',trackt,f,nl,-1.0d0)
  if (trackt >= 0.0d0) then
     IF(iprint)WRITE(6,*) '*** track mode chosen ***'
     IF(iprint)WRITE(6,*) 'track will be at x y t',trackx,tracky,trackt
  endif

      !
      ! BrennerBeardmore potential modifications
      !
      call readcreal('R1CC ',R1CC,f,nl,1.70d0)
      call readcreal('R2CC ',R2CC,f,nl,2.00d0)

      ! SP: ED-MD options
      call readcreal('clneicut  ',clneicut,f,nl,3.090d0)
      call readcreal('elmigrat  ',elmigrat,f,nl,0.000d0)

      call readcreal('elfield   ',elfield,f,nl,0.0d0)

      call readcreal('Erate     ',Erate,f,nl,1d20)
      call readcreal('timerelE  ',timerelE,f,nl,0.0d0)
      call readcint ('iabove     ',iabove,f,nl,-1)

      call readcreal('mctemp    ',mctemp,f,nl,0.0d0)
      call readcint ('evpintval  ',evpintval,f,nl,0)
      call readcint ('evpatype   ',evpatype,f,nl,0)
      call readcint ('fullsolve  ',fullsolve,f,nl,1)
      call readcint ('aroundapx  ',aroundapx,f,nl,0)
      call readcint ('rampmode   ',rampmode,f,nl,0)
      call readcreal('zmax      ',zmax,f,nl,500.0d0)
      call readcint ('edmethod  ',edmethod, f, nl, 2)
      call readcint ('qforces   ',qforces, f, nl, 1)


  IF(iprint)WRITE(7,*)
end subroutine Read_Params

!***********************************************************************








