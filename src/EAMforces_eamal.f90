!***********************************************************************
! EAM density calculation - for SANDIA, NRL, and etp versions
!***********************************************************************
!
! KN  See file README.forcesubs for some notes on what these do
!

subroutine Calc_P(P, x0,atype,box,pbc,                                &
     npairs,nborlist,atpassflag,nngbrproc,rcutmax,spline,eamnbands,moviemode)

  use defs

  use typeparam
  use datatypes
  use my_mpi

  use EAM

  use para_common

  implicit none
  !
  !     From input x0 (from here and other nodes), return density P
  !     for all atom pairs which interact by EAM
  ! 

  !     ------------------------------------------------------------------
  !          Variables passed in and out
  real*8 P(NPMAX,EAMBANDMAX), x0(*),box(3),pbc(3),rcutmax
  integer npairs,atype(*),spline,eamnbands
  integer*4 nborlist(*)
  integer atpassflag(*),nngbrproc(*)

  !          Variables for parallel operation
!include 'para_common.f90'
  !     myatoms = number of atoms that my node is responsible for
  !     mxat = the maximum number of atoms on any node
  !     buf, ibug, pbufeamal = buffer space for information from other nodes
  !     xsendbuf, isendbuf = temporary passing buffers
  !     ------------------------------------------------------------------
  !          Local variables and constants
  integer j,jj,mij,nbytes,nbr,nnbors,oP,nadr
  real*8 rs,r,xp(3),deni,denj,boxs(3),Psum

  integer m,i3,i2,j3,j2,ndest,i
  real*8 t1,dttimer

  integer d,ii,ifrom,myi,dfrom,np0
  real*8 psumlocal,psumpassed

  integer klo, khi
  real*8 a, b, ab, yplo, yphi, ylo, yhi

  real*8 x

  integer moviemode

  real*8 halfcorr

  integer iband

  ! Atom types for each pair
  integer itype,jtype
  ! Atom types taking into account two possible iac modes for P_r
  integer itype_P,jtype_P

  integer(kind=mpi_parameters) :: ierror  ! MPI error code

  !     ------------------------------------------------------------------


  !
  !  Note that we must pass around P information as well here, not only
  !  x info ! This is because not all atom pairs are included in .f90e P calc.
  !
  !  Checked that this works by comparing psumlocal and psumpassed with
  !  previous version of code. Identical results for 4, 8 and 16 processors !
  !

  halfcorr=one
  if (moviemode == 15 .or. moviemode == 5 .or. moviemode == 6 .or. moviemode == 16 .or. moviemode == 17 .or. moviemode == 18) then
     halfcorr=half
  endif

  do i=1,3
     boxs(i)=box(i)**2
  enddo
  do iband=1,eamnbands
     do i=1,mxat
        P(i,iband)=zero
     enddo

     do i=1,3*np0pairtable
        pbufeamal(i,iband)=zero
     enddo
  enddo
  do i=1,np0pairtable
     dirbuf(i)=0
     i2=i*2
     if (i <= myatoms) then
        ibuf(i2-1)=i
        ibuf(i2)=atype(i)
     else
        ibuf(i2-1)=0
        ibuf(i2)=0
     endif
  enddo
  !
  !  Pass and receive atoms to different directions.
  !  The passing of atoms in this piece of code should be identical 
  !  in the neighbour list and force calculations.
  !  
  !  In principle the idea is that the input is the local x0(myatoms) 
  !  coordinates, and the output is buf(np0), which contains the 
  !  coordinates of the atoms both in this node and within cut_nei
  !  from this in the adjacent nodes.
  !
  !  The atoms which get passed here must be exactly the
  !  same and in the same order in the force calculations
  !  as in the original neighbour list calculation !
  !
  !  To be able to pass back the P information of j pairs,
  !  we also pass and receive the atom indices and direction information
  !  of the atoms in j pairs.
  !

  IF(debug)PRINT *,'EAM 1'
  do i=1,3*myatoms
     buf(i)=x0(i)
  enddo
  np0=myatoms

  if (nprocs > 1) then
     t1=mpi_wtime()
     !         print *,'Calc_P loop 1',myproc
     !     Loop over directions
     do d=1,8
        !            print *,myproc,d,myatoms
        j=0
        do i=1,myatoms
           if (IAND(atpassflag(i),passbit(d)) .ne. 0) then
              j=j+1
              if (j .ge. NPPASSMAX)                                     &
                   call my_mpi_abort('NPPASSMAX overflow1', int(myproc))
              i3=i*3-3
              j3=j*3-3
              j2=j*2-2
              xsendbuf(j3+1)=x0(i3+1)
              xsendbuf(j3+2)=x0(i3+2)
              xsendbuf(j3+3)=x0(i3+3)
              isendbuf(j2+1)=i
              isendbuf(j2+2)=atype(i)
              !                 print *,d,j,x0(i3+1),x0(i3+2)
           endif
        enddo
        !            print *,'send',myproc,d,j,nngbrproc(d)
        if (sendfirst(d)) then
           call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                mpi_comm_world, ierror)
           if (j .gt. 0) then
              call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                   nngbrproc(d), d+8, mpi_comm_world, ierror)
              call mpi_send(isendbuf, j*2, my_mpi_integer, &
                   nngbrproc(d), d+16, mpi_comm_world, ierror)
           endif
           call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                mpi_comm_world, mpi_status_ignore, ierror)
           if (jj .gt. 0) then
              call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                   mpi_any_source, d+8, mpi_comm_world, &
                   mpi_status_ignore, ierror)
              call mpi_recv(ibuf(np0*2+1), jj*2, my_mpi_integer, &
                   mpi_any_source, d+16, mpi_comm_world, &
                   mpi_status_ignore, ierror)
           endif
        else 
           call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                mpi_comm_world, mpi_status_ignore, ierror)
           if (jj .gt. 0) then
              call mpi_recv(buf(np0*3+1), jj*3, mpi_double_precision, &
                   mpi_any_source, d+8, mpi_comm_world, &
                   mpi_status_ignore, ierror)
              call mpi_recv(ibuf(np0*2+1), jj*2, my_mpi_integer, &
                   mpi_any_source, d+16, mpi_comm_world, &
                   mpi_status_ignore, ierror)
           endif
           call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                mpi_comm_world, ierror)
           if (j .gt. 0) then
              call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                   nngbrproc(d), d+8, mpi_comm_world, ierror)
              call mpi_send(isendbuf, j*2, my_mpi_integer, &
                   nngbrproc(d), d+16, mpi_comm_world, ierror)
           endif
        endif
        j=jj

        do i=1,j
           dirbuf(np0+i)=d
        enddo
        np0=np0+j
     enddo
     tmr(15)=tmr(15)+(mpi_wtime()-t1)
  endif
  if (np0 .ne. np0pairtable) then
     write (6,*) 'EAMforces np0 problem ',myproc,np0,np0pairtable
  endif
  !      print *,'Node received totally ',myproc,np0-myatoms

  !  nbands model handled simply by looping over all bands


  do iband=1,eamnbands


     !
     !  Step 1: get P for atom pairs indexed in my node
     !

     !      print *,myproc,nprocs
     IF(debug)PRINT *,'EAM 2'

     mij=0
     psumlocal=0.0d0
     psumpassed=0.0d0

     do i=1,myatoms

        i3=3*i-3
        mij = mij + 1
        nnbors = nborlist(mij)
        do nbr=1,nnbors
           mij=mij+1
           j=nborlist(mij)

           ! Handle only EAM here 
           itype=abs(ibuf(i*2)); jtype=abs(ibuf(j*2));
           if (iac(itype,jtype)/=1.and.iac(itype,jtype)/=3.and.  &
                iac(itype,jtype)/=4.and.iac(itype,jtype)/=6) cycle

           j3=3*j-3

           xp(1)=buf(i3+1)-buf(j3+1)
           xp(2)=buf(i3+2)-buf(j3+2)
           xp(3)=buf(i3+3)-buf(j3+3)

           if (xp(1) .ge.  half) xp(1)=xp(1)-pbc(1)
           if (xp(1) .lt. -half) xp(1)=xp(1)+pbc(1)
           if (xp(2) .ge.  half) xp(2)=xp(2)-pbc(2)
           if (xp(2) .lt. -half) xp(2)=xp(2)+pbc(2)
           if (xp(3) .ge.  half) xp(3)=xp(3)-pbc(3)
           if (xp(3) .lt. -half) xp(3)=xp(3)+pbc(3)

           rs=xp(1)*xp(1)*boxs(1)+xp(2)*xp(2)*boxs(2)+xp(3)*xp(3)*boxs(3)

           if (rs < rcutmax*rcutmax) then
              r=SQRT(rs)

              !             CALL Calc_Den(den, r)

              itype_P=jtype; jtype_P=jtype;
              if (iac(itype,jtype)==4 .or. (iband>1.and.iac(itype,jtype)==6)) then
                 itype_P=itype; jtype_P=jtype;
              endif

              x=r*dri(itype_P,jtype_P)
              klo = INT(x) + 1
              khi = klo + 1
              if (khi<=nr(itype_P,jtype_P)) then
                 a=DBLE(khi-1)-x
                 b=one-a
                 ab=a*b

                 ylo=P_r(klo,itype_P,jtype_P,iband)*a
                 yhi=P_r(khi,itype_P,jtype_P,iband)*b

                 yplo=dPdr_r(klo,itype_P,jtype_P,iband)*a
                 yphi=dPdr_r(khi,itype_P,jtype_P,iband)*b

                 denj=a*ylo+b*yhi+ab*(two*(ylo+yhi)+                       &
                      dr(itype_P,jtype_P)*(yplo-yphi))
              else
                 denj=zero
              endif

              itype_P=itype; jtype_P=itype;
              if (iac(itype,jtype)==4 .or. (iband>1.and.iac(itype,jtype)==6)) then
                 itype_P=jtype; jtype_P=itype;
              endif

              x=r*dri(itype_P,jtype_P)
              klo = INT(x) + 1
              khi = klo + 1
              if (khi<=nr(itype_P,jtype_P)) then
                 a=DBLE(khi-1)-x
                 b=one-a
                 ab=a*b

                 ylo=P_r(klo,itype_P,jtype_P,iband)*a
                 yhi=P_r(khi,itype_P,jtype_P,iband)*b

                 yplo=dPdr_r(klo,itype_P,jtype_P,iband)*a
                 yphi=dPdr_r(khi,itype_P,jtype_P,iband)*b

                 deni=a*ylo+b*yhi+ab*(two*(ylo+yhi)+                       &
                      dr(itype_P,jtype_P)*(yplo-yphi))
              else
                 deni=zero
              endif

              !              END CALL Calc_Den

              ! Note: atom i is affected by j:s density, and vice versa
              ! except if iac==4 or iac==6 in which case everything is mixed..

              pbufeamal(i,iband)=pbufeamal(i,iband)+denj*halfcorr
              pbufeamal(j,iband)=pbufeamal(j,iband)+deni*halfcorr

              ! print *,i,j,deni,denj

              !write(99,'(2I6,2F13.7)') i,j,x0(i*3-1),deni,denj

           endif
        enddo
     enddo

     do j=1,myatoms
        P(j,iband)=pbufeamal(j,iband)
        psumlocal=psumlocal+pbufeamal(j,iband)
        !write (92,*) j,P(j,iband)
        !write(98,'(I6,2F13.7)') j,x0(j*3-1),P(j,iband)
     enddo

  enddo ! End of loop over bands

  IF(debug)PRINT *,'EAM 3'
  !
  !  Step 2: Send and get p information to/from adjacent nodes
  !
  !  From the array dirbuf, we can figure out from which direction an
  !  atom has been received, and from ibuf what index it has had there. Thus
  !  we can send the pbufeamal and atom index information to other nodes,
  !  and when receiving it know which atom it belongs to.
  !
  do iband=1,eamnbands

     i=0
     if (nprocs .gt. 1) then
        t1=mpi_wtime()
        !         print *,'Calc_P loop 2, pbufeamal sendrecv',myproc
        !     Loop over directions
        do d=1,8
           !     Loop over neighbours
           i=0
           do j=myatoms+1,np0
              dfrom=dirbuf(j)+4
              if (dfrom .gt. 8) dfrom=dfrom-8
              if (d .eq. dfrom) then
                 i=i+1
                 if (i .ge. NPPASSMAX)                                     &
                      call my_mpi_abort('NPPASSMAX overflow2', int(myproc))
                 ifrom=ibuf(j*2-1)
                 isendbuf(i)=ifrom
                 psendbuf(i)=pbufeamal(j,iband)
                 psumpassed=psumpassed+pbufeamal(j,iband)
              endif
           enddo
           !            print *,'send',myproc,d,i,nngbrproc(d)
           if (sendfirst(d)) then
              call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                   mpi_comm_world, ierror)
              if (i .gt. 0) then
                 call mpi_send(isendbuf, i, my_mpi_integer, &
                      nngbrproc(d), d+8, mpi_comm_world, ierror)
                 call mpi_send(psendbuf, i, mpi_double_precision, &
                      nngbrproc(d), d+16, mpi_comm_world, ierror)
              endif
              call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                   mpi_comm_world, mpi_status_ignore, ierror)
              if (ii .gt. 0) then
                 call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                      mpi_any_source, d+8, mpi_comm_world, &
                      mpi_status_ignore, ierror)
                 call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                      mpi_any_source, d+16, mpi_comm_world, &
                      mpi_status_ignore, ierror)
              endif
           else 
              call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                   mpi_comm_world, mpi_status_ignore, ierror)
              if (ii .gt. 0) then
                 call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                      mpi_any_source, d+8, mpi_comm_world, &
                      mpi_status_ignore, ierror)
                 call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                      mpi_any_source, d+16, mpi_comm_world, &
                      mpi_status_ignore, ierror)
              endif
              call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                   mpi_comm_world, ierror)
              if (i .gt. 0) then
                 call mpi_send(isendbuf, i, my_mpi_integer, &
                      nngbrproc(d), d+8, mpi_comm_world, ierror)
                 call mpi_send(psendbuf, i, mpi_double_precision, &
                      nngbrproc(d), d+16, mpi_comm_world, ierror)
              endif
           endif
           i=ii

           do ii=1,i
              myi=isendbuf2(ii)
              if (myi .gt. myatoms) then
                 print *,myproc,myatoms,myi,ii,i
                 call my_mpi_abort('CALC_P i too large', int(myproc))
              endif
              P(myi,iband)=P(myi,iband)+psendbuf2(ii)
           enddo
        enddo
        tmr(15)=tmr(15)+(mpi_wtime()-t1)
     endif

  enddo! End of loop over band passing


  if (debug) then
     write (6,99) 'For proc',myproc,' local rho sum',psumlocal,         &
          ' passed rho sum',psumpassed
99   format (A,I3,A,G13.6,A,G13.6)
  endif

  IF(debug)PRINT *,'EAM 4'

   


end subroutine Calc_P

!***********************************************************************
! EAM F[p(r)] calculation
!***********************************************************************
subroutine Calc_Fp(atype,Fp,dFpdp,Vmany, P,spline,Fpextrap,eamnbands)

  use defs

  use EAM
  use my_mpi
  use para_common
  implicit none

  !
  !     For input rho array P, return F[p] and d(F[p])/dp
  !

  !      IMPLICIT REAL*8  (a-h,o-z)
  !      IMPLICIT INTEGER (i-n)

  !     ------------------------------------------------------------------
  !          Variables passed in and out
  integer atype(*),spline,Fpextrap,eamnbands

  real*8 Fp(NPMAX,EAMBANDMAX),dFpdp(NPMAX,EAMBANDMAX),Vmany, P(NPMAX,EAMBANDMAX)



  !     ------------------------------------------------------------------

  !          Variables for parallel operation
!include 'para_common.f90'
  !     myatoms = number of atoms that my node is responsible for
  !     ------------------------------------------------------------------

  integer i,i1,iband
  integer khi,klo
  real*8 a,b,h,ab,ylo,yhi,yplo,yphi,x,xx
  logical overflow,underflow

  Vmany=zero

  do iband=1,eamnbands

     do i=1,myatoms
        i1=abs(atype(i))

        if (P(i,iband)==zero) then
           ! Must be impurity or sputtered atom
           Fp(i,iband)=zero
           dFpdp(i,iband)=zero
           cycle
        endif

        x=P(i,iband)
        xx=P(i,iband)/dP(i1,iband)
        klo = INT(xx) + 1
        khi = klo + 1

	underflow=.false.
	if (x < zero) then
	  print *,'EAM negative electron density??',i,x
	  if (Fpextrap/=1) then
	     print *,'... proceeding with rho=zero.'
	     x=zero
	     xx=zero
	     klo=1
	     khi=2
	  else
	     print *,'... doing downwards extrapolation.'
	     klo=1
	     khi=2
	     underflow=.true.
	  endif
       endif
       
       overflow=.false.
       if (khi>nP(i1,iband)) then
           print *,'EAM Fp overflow',i,i1,P(i,iband),dP(i1,iband),nP(i1,iband),khi
           if (Fpextrap==1) then
              ! This will effect a linear extrapolation
              khi=nP(i1,iband)
              klo=khi-1
              overflow=.true.
           else
              call my_mpi_abort('EAM Fp overflow', int(khi))
           endif
        endif

        h=dP(i1,iband)
        a=((khi-1)*dP(i1,iband)-x)/dP(i1,iband)
        b=one-a

        ylo=F_p(klo,i1,iband)*a
        yhi=F_p(khi,i1,iband)*b

        if (.not. overflow .and. .not. underflow) then 
           ab=a*b
           yplo=dFdp_p(klo,i1,iband)*a
           yphi=dFdp_p(khi,i1,iband)*b
           Fp(i,iband)=a*ylo+b*yhi+ab*(two*(ylo+yhi)+h*(yplo-yphi))
           dFpdp(i,iband)=ab*Fsc(klo,i1,iband)+yplo+yphi
           ! write (93,*) i,i1,P(i),Fp(i),dFpdp(i)
        else
           Fp(i,iband)=ylo+yhi
           dFpdp(i,iband)=(F_p(khi,i1,iband)-F_p(klo,i1,iband))/dP(i1,iband)
           print *,'linear extrap',Fp(i,iband),dFpdp(i,iband)
        endif

        Vmany = Vmany + Fp(i,iband)
     enddo

      

  enddo ! End of loop over bands

end subroutine Calc_Fp

!***********************************************************************
! Calculate the force on each atom
!***********************************************************************
subroutine Calc_Force(xnp,Epair,Vpair,wxx,wyy,wzz,wxxi,wyyi,wzzi,     &
     x0,atype,dFpdp,box,pbc,npairs,nborlist,                          &
     wxxsh,wyysh,wzzsh,natsh,nsh,                                     &
     wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,	      &
     atpassflag,nngbrproc,ECM,rcutmax,spline,eamnbands,moviemode,     &
     movietime_val,calc_avgvir,last_step_predict)

  use defs

  use typeparam
  use datatypes
  use my_mpi
  use EAM

  use para_common

  implicit none

  !
  ! Force calculation: The force xnp(i) obtained is scaled by 1/box,
  ! i.e. if F is the real force in units of eV/A, 
  !
  !  xnp(i) = F(i)/box(idim) 
  !
  !     ------------------------------------------------------------------
  !          Variables passed in and out
  integer atype(*),npairs,spline,eamnbands
  integer*4 nborlist(*)
  real*8 xnp(*),Epair(*),Vpair, x0(*),dFpdp(NPMAX,EAMBANDMAX),rcutmax,box(3),pbc(3)
  real*8 wxx,wyy,wzz,wxxi(*),wyyi(*),wzzi(*)


  real*8 wxy,wxyi(*),wxz,wxzi(*),wyx,wyxi(*),wyz,wyzi(*),wzx,wzxi(*),wzy,wzyi(*)
  integer moviemode
  logical movietime_val
  logical calc_vir
  logical calc_avgvir
  logical last_step_predict

  integer nsh
  integer natsh(*)
  real*8 wxxsh(*),wyysh(*),wzzsh(*),ECM(3),dx,dy,dz

  integer atpassflag(*),nngbrproc(*)

  !          Variables for parallel operation
!include 'para_common.f90'
  !     myatoms = number of atoms that my node is responsible for
  !     mxat = the maximum number of atoms on any node
  !     buf(8*NPMAX) = buffer space for information from other nodes
  !     ------------------------------------------------------------------
  !          Local variables and constants
  integer nbr,nnbors,j,jj,mij,nadr,nbytes,iband
  real*8 V_ij,Vp_ij,vij2,r,rs,xp(6),boxs(3)
  integer mijptr

  integer j3,j2,i3,i2,m,i,ndest,nsend,itype,jtype
  real*8 tmp,dendi,dendj,t1,dttimer,maxr
  real*8 rcutmaxs,dummy1,dummy2

  integer nnsh

  integer d,ii,ifrom,myi,dfrom,np0,ii3
  !
  !  Parameters for inlined calc_dend and calc_pair
  !
  real*8 x,a,b,ab
  real*8 ylo,yhi,yplo,yphi

  integer klo,khi

  ! Atom types taking into account two possible iac modes for P_r
  integer itype_P,jtype_P      

  integer(kind=mpi_parameters) :: ierror  ! MPI error code

  real*8 halfcorr

  halfcorr=one
  if (moviemode == 15 .or. moviemode == 5 .or. moviemode == 6 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
     halfcorr=half
  endif

  !     ------------------------------------------------------------------

  !
  !     For paralell passing routines, using the same pbufeamal as calc_P for
  !     forces to prevent excessive waste of memory
  !
  !     How this used to work:
  !
  !     ipass=0:     Get Epair(i) and xnp(i) for my local atom pairs
  !     ipass=1:     1� Send and receive my atom coordinates and dFpdp
  !                  2� Calculate Epair and xnp into buf
  !     ipass=2:     1� Send and receive atoms, dFpdp, Epair, xnp
  !     to npass-1   2� Calculate Epair and xnp into buf
  !     ipass=npass: 1� Send final Epair and xnp.
  !
  !     How this now works:
  !
  !     � First sendrecv x0, atom indices and dFpdp of atoms in my node
  !     � Calculate all Epairs and forces  
  !     � Sendrecv Epair and forces for atoms not in my node
  !     � Sum up the received Epairs and forces in correct nodes
  !
  !     Passing buffers (all defined in para_common.h):
  !
  !     atom coords       - buf()
  !     atom indices      - ibuf()     | interlaced, index at i*2-1
  !     atom types        - ibuf()     | atype at i*2
  !     derivatives dFpdp - dbufeamal()
  !     forces xnp        - pbufeamal()
  !     energies Epair    - ebuf()
  !

  rcutmaxs=rcutmax*rcutmax
  do i=1,3
     boxs(i)=box(i)**2
  enddo
  maxr=sqrt(3.0)*0.5

  ! Initialize forces
  calc_vir = .false.
  if (movietime_val .or. calc_avgvir .or. last_step_predict) then
   if (moviemode == 15 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
    calc_vir = .true.
   endif
  endif

  do i=1,mxat
     Epair(i)=zero
     wxxi(i)=zero
     wyyi(i)=zero
     wzzi(i)=zero
     if (calc_vir) then
        wxyi(i)=zero
        wxzi(i)=zero
        wyxi(i)=zero
        wyzi(i)=zero
        wzxi(i)=zero
        wzyi(i)=zero
     endif

  enddo
  do i=1,3*mxat
     xnp(i)=zero
  enddo
  Vpair=zero
  wxx=zero
  wyy=zero
  wzz=zero

  if (calc_vir) then
     wxy=zero
     wxz=zero
     wyx=zero
     wyz=zero
     wzx=zero
     wzy=zero
  endif

  do j=1,nsh
     wxxsh(j)=zero
     wyysh(j)=zero
     wzzsh(j)=zero
     natsh(j)=0
  enddo

  do iband=1,eamnbands      
     do i=1,np0pairtable
        ! dirbuf(i)=0
        if (i .le. myatoms) then
           dbufeamal(i,iband)=dFpdp(i,iband)
           ! ibuf(i*2-1)=i
           ! ibuf(i*2)=atype(i)
        else
           dbufeamal(i,iband)=zero
           ! ibuf(i*2-1)=0
           ! ibuf(i*2)=0
        endif
     enddo
  enddo
  do i=1,np0pairtable      
     ebuf(i)=zero
  enddo

  do i=1,3*np0pairtable
     !         if (i .le. 3*myatoms) then
     !            buf(i)=x0(i)
     !         else
     !            buf(i)=zero
     !         endif
     pbuf(i)=zero
  enddo

  !
  !  Pass and receive atoms to different directions.
  !  The passing of atoms in this piece of code should be identical 
  !  in the neighbour list and force calculations.
  !  
  !  In principle the idea is that the input is the local x0(myatoms) 
  !  coordinates, and the output is buf(np0), which contains the 
  !  coordinates of the atoms both in this node and within cut_nei
  !  from this in the adjacent nodes.
  !
  !  The atoms which get passed here must be exactly the
  !  same and in the same order in the force calculations
  !  as in the original neighbour list calculation !
  !
  !  To be able to pass back the force information of j pairs,
  !  we also pass and receive the atom indices and direction information
  !  of the atoms in j pairs.
  !
  !  NOTE: most passing buffers have already correct contents from
  !  calc_P, so no need to redo passing here !
  !
  call mpi_barrier(mpi_comm_world, ierror)

  np0=myatoms

  if (nprocs .gt. 1) then

     do iband=1,eamnbands

        np0=myatoms

        t1=mpi_wtime()
        !         print *,'Calc_Force loop 1',myproc
        !*     Loop over directions
        do d=1,8
           !            print *,myproc,d,myatoms
           j=0
           do i=1,myatoms
              if (IAND(atpassflag(i),passbit(d)) .ne. 0) then
                 j=j+1
                 if (j .ge. NPPASSMAX)                                     &
                      call my_mpi_abort('NPPASSMAX overflow', int(myproc))
                 !                  i3=i*3-3
                 !                  j3=j*3-3
                 !                  j2=j*2-2
                 !                  xsendbuf(j3+1)=x0(i3+1)
                 !                  xsendbuf(j3+2)=x0(i3+2)
                 !                  xsendbuf(j3+3)=x0(i3+3)
                 psendbuf(j)=dFpdp(i,iband)
                 !                  isendbuf(j2+1)=i
                 !                  isendbuf(j2+2)=atype(i)
                 !c                 print *,d,j,x0(i3+1),x0(i3+2)
              endif
           enddo
           !c            print *,'send',myproc,d,j,nngbrproc(d)
           if (sendfirst(d)) then
              call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                   mpi_comm_world, ierror)
              if (j .gt. 0) then
                 !                 CALL mpi_send(xsendbuf, j*3, mpi_double_precision, nngbrproc(d), d+8, mpi_comm_world, ierror)
                 call mpi_send(psendbuf, j, mpi_double_precision, &
                      nngbrproc(d), d+16, mpi_comm_world, ierror)
                 !                 CALL mpi_send(isendbuf, 2*j, my_mpi_integer, nngbrproc(d), d+24, mpi_comm_world, ierror)
              endif
              call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                   mpi_comm_world, mpi_status_ignore, ierror)
              !              print *,'received',myproc,d,j
              if (jj .gt. 0) then
                 !                 CALL mpi_recv(xsendbuf, j*3, mpi_double_precision, mpi_any_source, d+8, mpi_comm_world, mpi_status_ignore, ierror)
                 call mpi_recv(dbufeamal(np0+1,iband), jj, mpi_double_precision, &
                      mpi_any_source, d+16, mpi_comm_world, &
                      mpi_status_ignore, ierror)
                 !                 CALL mpi_recv(isendbuf, 2*j, my_mpi_integer, mpi_any_source, d+24, mpi_comm_world, mpi_status_ignore, ierror)
              endif
           else
              call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                   mpi_comm_world, mpi_status_ignore, ierror)
              !              print *,'received',myproc,d,j
              if (jj .gt. 0) then
                 !                 CALL mpi_recv(xsendbuf, j*3, mpi_double_precision, mpi_any_source, d+8, mpi_comm_world, mpi_status_ignore, ierror)
                 call mpi_recv(dbufeamal(np0+1,iband), jj, mpi_double_precision, &
                      mpi_any_source, d+16, mpi_comm_world, &
                      mpi_status_ignore, ierror)
                 !                 CALL mpi_recv(isendbuf, 2*j, my_mpi_integer, mpi_any_source, d+24, mpi_comm_world, mpi_status_ignore, ierror)
              endif
              call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                   mpi_comm_world, ierror)
              if (j .gt. 0) then
                 !                 CALL mpi_send(xsendbuf, j*3, mpi_double_precision, nngbrproc(d), d+8, mpi_comm_world, ierror)
                 call mpi_send(psendbuf, j, mpi_double_precision, &
                      nngbrproc(d), d+16, mpi_comm_world, ierror)
                 !                 CALL mpi_send(isendbuf, 2*j, my_mpi_integer, nngbrproc(d), d+24, mpi_comm_world, ierror)
              endif
           endif
           j=jj
           np0=np0+j
        enddo
     enddo
     tmr(17)=tmr(17)+(mpi_wtime()-t1)
  endif

  if (np0 .ne. np0pairtable) then
     write (6,*) 'Calc_Force np0 problem ',myproc,np0,np0pairtable
  endif
  !      print *,'Node received totally ',myproc,np0-myatoms

  !
  !  Step 1: get Epair and xnp for all atom pairs 
  !

  mij=0
  do i=1,myatoms
     i3=3*i-3
     mij = mij + 1
     nnbors = nborlist(mij)

     dx=buf(i3+1)-ECM(1)/box(1)
     dy=buf(i3+2)-ECM(2)/box(2)
     dz=buf(i3+3)-ECM(3)/box(3)
     r=sqrt(dx*dx+dy*dy+dz*dz)

     nnsh=int(1.0d0+r/maxr*(1.0d0*nsh-1.0d0))

     do nbr=1,nnbors
        mij=mij+1
        j=nborlist(mij)

        itype=abs(ibuf(i*2))
        jtype=abs(ibuf(j*2))

        if (iac(itype,jtype)/=1 .and. iac(itype,jtype)/=3 .and. &
             iac(itype,jtype)/=4) then
           ! Handle other interaction types
           if (iac(itype,jtype) == 0) cycle
           if (iac(itype,jtype) == -1) then
              write (6,*) 'ERROR: IMPOSSIBLE INTERACTION'
              write (6,*) myproc,i,j,itype,jtype
              call my_mpi_abort('INTERACTION -1', int(myproc))
           endif
        endif

        j3=j*3-3

        xp(1)=buf(i3+1)-buf(j3+1)
        xp(2)=buf(i3+2)-buf(j3+2)
        xp(3)=buf(i3+3)-buf(j3+3)

        if (xp(1) .ge.  half) xp(1)=xp(1)-pbc(1)
        if (xp(1) .lt. -half) xp(1)=xp(1)+pbc(1)
        if (xp(2) .ge.  half) xp(2)=xp(2)-pbc(2)
        if (xp(2) .lt. -half) xp(2)=xp(2)+pbc(2)
        if (xp(3) .ge.  half) xp(3)=xp(3)-pbc(3)
        if (xp(3) .lt. -half) xp(3)=xp(3)+pbc(3)

        rs=xp(1)*xp(1)*boxs(1)+                                         &
             xp(2)*xp(2)*boxs(2)+                                       &
             xp(3)*xp(3)*boxs(3)

        if (rs < rcutmaxs) then
           r=SQRT(rs)

           ! Force calc
           if (iac(itype,jtype) == 2) then
              V_ij=zero; tmp=zero;
              call splinereppot(r,V_ij,tmp,1d20,1d20,itype,jtype,dummy1,dummy2)
              ! print *,'rep',i,j,r,V_ij,tmp
              tmp=tmp/r

           else

              ! Inlined subroutines to increase efficiency 
              ! CALL Calc_Dend(dend, r)
              ! For alloys, you need to calculate both 
              ! drho_i/dr_i and drho_j/dr_i !?

              ! drho_j/dr_i

              ! Be very careful with cutoff's here - alloys can have
              ! different cutoff values !!

              ! Need to calculate densities in all bands here
              tmp=zero
              do iband=1,eamnbands
                 itype_P=jtype; jtype_P=jtype;
                 if (iac(itype,jtype)==4  .or. (iband>1 .and. iac(itype,jtype)==6)) then
                    itype_P=itype; jtype_P=jtype;
                 endif

                 x=r*dri(itype_P,jtype_P)
                 klo = INT(x) + 1
                 khi = klo + 1
                 if (khi<=nr(itype_P,jtype_P)) then
                    a=DBLE(khi-1)-x
                    b=one-a
                    ab=a*b
                    yplo=dPdr_r(klo,itype_P,jtype_P,iband)*a
                    yphi=dPdr_r(khi,itype_P,jtype_P,iband)*b
                    dendj=ab*Psc(klo,itype_P,jtype_P,iband)+yplo+yphi
                    !write (96,*) r,dendj,itype_P,jtype_P
                 else
                    dendj=zero
                 endif

                 itype_P=itype; jtype_P=itype;
                 if (iac(itype,jtype)==4 .or. (iband>1 .and. iac(itype,jtype)==6)) then
                    itype_P=jtype; jtype_P=itype;
                 endif

                 ! drho_i/dr_i
                 x=r*dri(itype_P,jtype_P)
                 klo = INT(x) + 1
                 khi = klo + 1
                 if (khi<=nr(itype_P,jtype_P)) then
                    a=DBLE(khi-1)-x
                    b=one-a
                    ab=a*b
                    yplo=dPdr_r(klo,itype_P,jtype_P,iband)*a
                    yphi=dPdr_r(khi,itype_P,jtype_P,iband)*b
                    dendi=ab*Psc(klo,itype_P,jtype_P,iband)+yplo+yphi
                 else
                    dendi=zero
                 endif
                 !print *,r,dendi,dendj

                 tmp=tmp+(dendj*dFpdp(i,iband) + dendi*dbufeamal(j,iband))/r
              enddo ! End of loop over bands

              ! CALL Calc_Pair(V_ij,Vp_ij, r)
              ! dV2/dr
              x=r*dri(itype,jtype)
              klo = INT(x) + 1
              khi = klo + 1
              a=DBLE(khi-1)-x
              b=one-a
              ab=a*b
              if (khi<=nr(itype,jtype)) then
                 ylo=Vp_r(klo,itype,jtype)*a
                 yhi=Vp_r(khi,itype,jtype)*b
                 yplo=dVpdr_r(klo,itype,jtype)*a
                 yphi=dVpdr_r(khi,itype,jtype)*b
                 V_ij=a*ylo+b*yhi+ab*(two*(ylo+yhi)+          &
                      dr(itype,jtype)*(yplo-yphi))
                 Vp_ij=ab*Vpsc(klo,itype,jtype)+yplo+yphi
              else
                 V_ij=zero
                 Vp_ij=zero
              endif

              ! Force from pair potential
              tmp=halfcorr*tmp+halfcorr*Vp_ij/r

           endif
           vij2=V_ij*half*halfcorr
           ebuf(i)=ebuf(i)+vij2
           ! print *,'vij2',vij2
           ebuf(j)=ebuf(j)+vij2

           !KN
           ! This calculates the force in the right direction, 
           ! and scales with box size
           !
           xp(4)=xp(1)*tmp
           xp(5)=xp(2)*tmp
           xp(6)=xp(3)*tmp
           xnp(i3+1)=xnp(i3+1)-xp(4)
           xnp(i3+2)=xnp(i3+2)-xp(5)
           xnp(i3+3)=xnp(i3+3)-xp(6)

           pbuf(j3+1)=pbuf(j3+1)+xp(4)
           pbuf(j3+2)=pbuf(j3+2)+xp(5)
           pbuf(j3+3)=pbuf(j3+3)+xp(6)

           ! Note: because of ij symmetry this is actually
           ! an error !

           wxxi(i)=wxxi(i)-xp(4)*xp(1)
           wyyi(i)=wyyi(i)-xp(5)*xp(2)
           wzzi(i)=wzzi(i)-xp(6)*xp(3)

           if (calc_vir) then
              wyxi(i)=wyxi(i)-xp(5)*xp(1)
              wzxi(i)=wzxi(i)-xp(6)*xp(1)
              wxyi(i)=wxyi(i)-xp(4)*xp(2)
              wzyi(i)=wzyi(i)-xp(6)*xp(2)
              wxzi(i)=wxzi(i)-xp(4)*xp(3)
              wyzi(i)=wyzi(i)-xp(5)*xp(3)
           endif
        endif
     enddo
     do j=nsh,nnsh,-1
        wxxsh(j)=wxxsh(j)+wxxi(i)
        wyysh(j)=wyysh(j)+wyyi(i)
        wzzsh(j)=wzzsh(j)+wzzi(i)
        natsh(j)=natsh(j)+1
     enddo
  enddo

  call my_mpi_dsum(wxxsh, nsh)
  call my_mpi_dsum(wyysh, nsh)
  call my_mpi_dsum(wzzsh, nsh)
  call my_mpi_isum(natsh, nsh)
  !
  !  E and xnp for atom pairs in this node
  !
  do j=1,myatoms
     wxx=wxx+wxxi(j)
     wyy=wyy+wyyi(j)
     wzz=wzz+wzzi(j)
     Epair(j)=Epair(j)+ebuf(j)

     if (calc_vir) then
        wxy=wxy+wxyi(j)
        wxz=wxz+wxzi(j)
        wyx=wyx+wyxi(j)
        wyz=wyz+wyzi(j)
        wzx=wzx+wzxi(j)
        wzy=wzy+wzyi(j)
     endif

  enddo
  do j=1,3*myatoms
     xnp(j)=xnp(j)+pbuf(j)
  enddo

  !
  !  Step 2: Send and get e and f information to/from adjacent nodes
  !
  !  From the array dirbuf, we can figure out from which direction an
  !  atom has been received, and from ibuf what index it has had there. Thus
  !  we can send the pbuf and atom index information to other nodes,
  !  and when receiving it know which atom it belongs to.
  !
  IF(debug)PRINT *,'Calc_Force sendback',np0

  call mpi_barrier(mpi_comm_world, ierror)
  i=0
  if (nprocs .gt. 1) then
     t1=mpi_wtime()
     !         print *,'Calc_P loop 2, pbuf sendrecv',myproc
     !        Loop over directions
     nsend=myatoms
     do d=1,8
        !           Loop over neighbours
        i=0
        do j=myatoms+1,np0
           j3=j*3-3
           dfrom=dirbuf(j)+4
           if (dfrom .le. 4)                                            &
                call my_mpi_abort('dfrom HORROR ERROR !', int(myproc))
           if (dfrom .gt. 8) dfrom=dfrom-8
           if (d .eq. dfrom) then
              i=i+1
              if (i .ge. NPPASSMAX)                                     &
                   call my_mpi_abort('NPPASSMAX overflow2', int(myproc))
              i3=i*3-3
              ifrom=ibuf(j*2-1)
              isendbuf(i)=ifrom
              psendbuf(i)=ebuf(j)
              xsendbuf(i3+1)=pbuf(j3+1)
              xsendbuf(i3+2)=pbuf(j3+2)
              xsendbuf(i3+3)=pbuf(j3+3)
              nsend=nsend+1
           endif
        enddo
        !            print *,'send',myproc,d,i,nngbrproc(d)
        ! Safe communication model - I hope
        if (sendfirst(d)) then
           call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                mpi_comm_world, ierror)
           if (i>0) then
              call mpi_send(isendbuf, i, my_mpi_integer, &
                   nngbrproc(d), d+8, mpi_comm_world, ierror)
              call mpi_send(psendbuf, i, mpi_double_precision, &
                   nngbrproc(d), d+16, mpi_comm_world, ierror)
              call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                   nngbrproc(d), d+24, mpi_comm_world, ierror)
           endif
           call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d,&
                mpi_comm_world, mpi_status_ignore, ierror)
           if (ii>0) then
              call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                   mpi_any_source, d+8, mpi_comm_world, &
                   mpi_status_ignore, ierror)
              call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                   mpi_any_source, d+16, mpi_comm_world, &
                   mpi_status_ignore, ierror)
              call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                   mpi_any_source, d+24, mpi_comm_world, &
                   mpi_status_ignore, ierror)
           endif
        else 
           call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                mpi_comm_world, mpi_status_ignore, ierror)
           if (ii>0) then
              call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                   mpi_any_source, d+8, mpi_comm_world, &
                   mpi_status_ignore, ierror)
              call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                   mpi_any_source, d+16, mpi_comm_world, &
                   mpi_status_ignore, ierror)
              call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                   mpi_any_source, d+24, mpi_comm_world, &
                   mpi_status_ignore, ierror)
           endif
           call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                mpi_comm_world, ierror)
           if (i>0) then
              call mpi_send(isendbuf, i, my_mpi_integer, &
                   nngbrproc(d), d+8, mpi_comm_world, ierror)
              call mpi_send(psendbuf, i, mpi_double_precision, &
                   nngbrproc(d), d+16, mpi_comm_world, ierror)
              call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                   nngbrproc(d), d+24, mpi_comm_world, ierror)
           endif
        endif
        i=ii

        do ii=1,i
           myi=isendbuf2(ii)
           if (myi .gt. myatoms) then
              print *,myproc,myatoms,myi,ii,i
              call my_mpi_abort('EAMforce i too large', int(myproc))
           endif
           ii3=ii*3-3
           i3=myi*3-3
           Epair(myi)=Epair(myi)+psendbuf2(ii)
           xnp(i3+1)=xnp(i3+1)+xsendbuf2(ii3+1)
           xnp(i3+2)=xnp(i3+2)+xsendbuf2(ii3+2)
           xnp(i3+3)=xnp(i3+3)+xsendbuf2(ii3+3)
        enddo
     enddo
     if (nsend .ne. np0) then
        write (6,*) 'WARNING: calc_force not sending out as many'
        write (6,*) 'atoms as it received. Something fishy !?!'
        write (6,*) myproc,myatoms,nsend,np0
     endif

     tmr(17)=tmr(17)+(mpi_wtime()-t1)
  endif

  Vpair=zero
  do i=1,myatoms
     Vpair=Vpair+Epair(i)
     !write(97,'(I6,4F13.7)')i,Epair(i),xnp(i*3-2),xnp(i*3-1),xnp(i*3)
  enddo

  call mpi_barrier(mpi_comm_world, ierror)

   

end subroutine Calc_Force
