
module tersoff_common
  ! Tersoff_compound common block and definitions
  
  integer, parameter :: MXTT = 4

!  COMMON /tersoff_compound/tersA,tersB,beta,rlambda,rmu,gc,gd,gh,ters_n,rlambda3_cube,rlambda3
!  COMMON /tersoff_compound2/gd2,gd2i,gc2,g2c2,ters2i,xi,tersoffomega
!  COMMON /cutoff/trcut,dcut,rmd,rpd,a,halfa,r_ter_max
!  COMMON /reppot/ reppotcut,bf,rf

  ! Place logical variable albepot last to avoid unnecessary warnings
  ! on Alphas.
!    COMMON /albe/ albegamma,albecossign,albe2mu,albe2mu3,albepot,exponentone
  
  real*8 tersA(0:MXTT,0:MXTT),tersB(0:MXTT,0:MXTT),beta(0:MXTT,0:MXTT),&
       & rlambda(0:MXTT,0:MXTT),rmu(0:MXTT,0:MXTT),gc(0:MXTT,0:MXTT),&
       & gd(0:MXTT,0:MXTT),gh(0:MXTT,0:MXTT),ters_n(0:MXTT,0:MXTT),&
       & rlambda3_cube(0:MXTT,0:MXTT),rlambda3(0:MXTT,0:MXTT)
  real*8 gd2(0:MXTT,0:MXTT),gd2i(0:MXTT,0:MXTT),gc2(0:MXTT,0:MXTT),&
       & g2c2(0:MXTT,0:MXTT),ters2i(0:MXTT,0:MXTT),xi(0:MXTT,0:MXTT)
  real*8 trcut(0:MXTT,0:MXTT),dcut(0:MXTT,0:MXTT),rmd(0:MXTT,0:MXTT),&
       & rpd(0:MXTT,0:MXTT),a(0:MXTT,0:MXTT),halfa(0:MXTT,0:MXTT),&
       & r_ter_max(0:MXTT,0:MXTT)
  real*8 tersoffomega(0:MXTT,0:MXTT,0:MXTT)
    
  real*8 reppotcut(0:MXTT,0:MXTT),bf(0:MXTT,0:MXTT),rf(0:MXTT,0:MXTT)

  logical albepot
  logical exponentone
  real*8 albegamma(0:MXTT,0:MXTT),albecossign(0:MXTT,0:MXTT)
  real*8 albe2mu(0:MXTT,0:MXTT),albe2mu3(0:MXTT,0:MXTT,0:MXTT)
  
end module tersoff_common
