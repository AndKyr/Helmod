
  subroutine Silica_Force(X,atype,A,atomindex,natoms,box,pbc,           &
       nborlist,Epair,Vpair,Ethree,Vthree,wxx,wyy,wzz,wxxi,wyyi,wzzi,   &
       wxxsh,wyysh,wzzsh,natsh,nsh,                                     &
       wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,		&
       atpassflag,nngbrproc,potmode,ECM,reppotcutin,moviemode,          &
       movietime_val,calc_avgvir,last_step_predict)

    use typeparam
    use datatypes
    use my_mpi

    use silicaparam

    use defs

    use para_common

    implicit none

    !
    !     This subroutine is an implementation of silica potential 
    !     published in Ohta et al., J. Chem. Phys. 115 (2001) 6679-6690. 
    !     The potential is a modification of Stillinger-Weber potential.
    !     Therefore, this subroutine is an evolution version of the stilweb.f90 
    !     routine.
    !
    !     Code originates from ancient HARWELL code, from which it was modified
    !     for PARCAS by Kai Nordlund in Jan 1997. Modifications for silica 
    !     potential by Juha Samela 2005. 
    !
    !
    !     In the PARCAS interface:
    !     Input:      X       contains positions in A scaled by 1/box,
    !                 natoms  Total number of atoms over all nodes
    !                 myatoms Number of atoms in my node, in para_common.h
    !                 box(3)  Box size (box centered on 0)
    !                 pbc(3)  Periodics: if = 1.0d0 periodic
    !
    !     Output:     A       contains forces in eV/A scaled by 1/box
    !                 Epair   V_2 per atom
    !                 Vpair   total V_2 of all atoms
    !                 Ethree  V_3 per atom
    !                 Vthree  total V_3 of all atoms; Vmany in mdx.f
    !                  
    !

    ! The atom types in ATYPE(*) and typeparam are used in the following way:
    !     
    ! On firsttime, all type combinations are looped through. Recognized
    ! iac(it,jt)==1 combinations are set to the correct IATAB value.
    ! For others, a warning is issued.

    ! nborlist(i) has following format (not same as original code !):
    !
    !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
    !  nborlist(2)    Index of first neighbour of atom 1
    !  nborlist(3)    Index of second neighbour of atom 1
    !   ...
    !  nborlist(N1+1)  Index of last neighbour of atom 1
    !  nborlist(N1+2)  Number of neighbours for atom 2 
    !  nborlist(N1+3)  Index of first neighbour of atom 2
    !   ...
    !   ...           And so on for all N atoms
    !

    !
    !     THIS SUBROUTINE CALCULATES THE ACCELERATIONS (A) OF PARTICLES
    !     IN A MOLECULAR DYNAMICS RUN. THE PARTICLE POSITIONS ARE GIVEN
    !     IN X, AND THEIR INTERACTIONS THROUGH A CENTRAL FORCE ARE CALCULATED
    !     ACCORDING TO TWO AND THREE-BODY POTENTIAL FOR SILICON OR GERMANIUM
    !     (STILLINGER-WEBER). 
    !
    !     Si pot.: Stillinger and Weber, PRB 31 (1985) 5262
    !     Ge pot.: Stillinger and Weber, ??
    ! 

    !
    ! ------------------------------------------------------------------------
    !     Variables passed in and out

    real*8 X(*),A(*)
    real*8 box(3),pbc(3)
    integer*4 nborlist(*)
    integer atomindex(*)
    integer natoms,atype(*)

    real*8 Epair(*), Vpair, Ethree(*), Vthree
    real*8 wxx,wyy,wzz,wxxi(*),wyyi(*),wzzi(*)
    real*8 reppotcutin

    real*8 wxy,wxyi(*),wxz,wxzi(*),wyx,wyxi(*),wyz,wyzi(*),wzx,wzxi(*),wzy,wzyi(*)
    integer moviemode
    logical movietime_val
    logical calc_vir
    logical calc_avgvir
    logical last_step_predict

    real*8 wxxsh(*),wyysh(*),wzzsh(*),ECM(3),dx,dy,dz
    integer natsh(*),nsh

    integer atpassflag(*),nngbrproc(*),potmode

    real*8 dttimer
    !
    !     PARAMETERS FOR STILLINGER_WEBER FORCE CALCULATION
    !

    logical subcmacc
    data subcmacc /.false./

    real*8 VIR

    real*8 AI,ACM(3)
    real*8 BOX2X,BOX2Y,BOX2Z
    real*8 BOXX2,BOXY2,BOXZ2
    real*8 PH,DPH,PH1,PH2,HALFPH
    real*8 COSMNL,COSS,DAI,DPHN,DPHM,DPHL

    real*8 DXNL,DXNLS,DXNM,DXNMS,dxml,dxmls
    real*8 DYNL,DYNLS,DYNLSM,DYNM,DYNMS,DYNMSM,dyml,dymls,dymlsm
    real*8 DZNL,DZNLS,DZNLSM,DZNM,DZNMS,DZNMSM,dzml,dzmls,dzmlsm
    real*8 XL,XM,YL,YM,ZL,ZM

    real*8 HCOS,HMNL,HKMNL,HKMNLL,HKMNLM
    real*8 RNL,RNLA,RNLEF,RNLSQ,RNM,RNMA,RNMEF,RNMSQ,R0SQ(5),rml,rmlsq

    integer K,K1,N,M,L,IATN,IATM,IATL,IATYP1,IATYP2,IATYP3,NP3,NP32
    integer IAIND,IAIND2

    integer ingbr,jngbr,nngbr,n3
    real*8 help1,help2

    integer, allocatable, save :: IATAB(:,:)
    logical, save :: firsttime,sw
    data firsttime /.true./

    !
    !     PARAMETERS FOR INTERFACE AND PARALLELLIZATION
    !

!include 'para_common.f90'

    real*8 maxr,r
    integer i,j,jj,i2,i3,j2,j3,ii,ii3,myi,dfrom,ifrom,nnsh,NP,d
    real*8 t1,dummy1,dummy2

    !-------------------------------------------------------------------------------!

    !     Fermi joining parameters. 
    !     Si-Si 15,1.4 checked: gave threshold displ. energy 18 eV,
    !     looked good, energy conserved with both ZBL and DMol
    real*8 bf(5),rf(5)
    data bf /15.0, 15.0, 15.0, 15.0, 15.0/  ! TO BE MODIFIED FOR O-SI AND O-O
    data rf /1.4, 1.5, 1.45, 1.4, 1.4/

    !
    !     Ugly repulsive potential cutoff to speed things up a bit  
    !     At 2.2, Si fermi function derivative is ~1e-4
    real*8 reppotcut(5)
    data reppotcut /2.2, 2.3, 2.25, 1.0d0, 1.9/ ! Si-O and O-O added

    integer :: a1, a2, a3, a1tmp, a2tmp, a3tmp
    real*8 paraEpsA
    real*8 gsmooth ! pair interaction smoothing function
    real*8 :: tmp
    real*8, parameter :: p1 = 0.097d0
    real*8, parameter :: p2 = 1.6d0
    real*8, parameter :: p3 = 0.3654d0
    real*8, parameter :: p4 = 0.1344d0
    real*8, parameter :: p5 = 6.4176d0
    real*8 paraSiga0
    real*8 zsmooth, smoothmin, smoothmax
    integer si, sj, snngbr, siatm, sk

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !-------------------------------------------------------------------------------

    !-----------------------------------------------------------------------
    !     PARALLELL INTERFACE
    !-----------------------------------------------------------------------
    !
    !     Parallellization principle:
    !     
    !     1� Copy myatoms atom coordinates*box into buf(1:myatoms*3)
    !     2� Get atom coordinates of neighbouring nodes into rest of buf
    !
    !     3� Calculate forces of myatoms into array pbuf
    !     4� The copy forces of myatoms to array A()
    !
    !     5� Send back accelerations of neighbour atoms to them
    !
    !     By calculating forces of all NP atoms in stage 3�, stage 5�
    !     can be omitted. However, this probably is less efficient except
    !     for really huge simulation cells. This approch requires that the 
    !     neighbour list contains neighbours of the atoms from other nodes 
    !     as well; see mdlinkedlist.f
    !
    !
    !
    !     myatoms is the number of atoms in my node
    !     NP is the total number of atoms, including the passed ones
    !
    !
    !     Stage 1�
    NP=myatoms
    do i=1,myatoms
       i3=i*3-3
       buf(i3+1)=X(i3+1)*box(1)
       buf(i3+2)=X(i3+2)*box(2)
       buf(i3+3)=X(i3+3)*box(3)
    enddo
    do i=1,myatoms
       dirbuf(i)=0
       ibuf(i*2-1)=i
       ibuf(i*2)=atype(i)
    enddo

    !
    !     Stage 2�
    !
    !     Pass and receive atoms to different directions.
    !     The passing of atoms in this piece of code should be identical 
    !     in the neighbour list and force calculations.
    !     
    !     In principle the idea is that the input is the local X(myatoms) 
    !     coordinates, and the output is buf(NP), which contains the 
    !     coordinates of the atoms both in this node and within cut_nei
    !     from this in the adjacent nodes.
    !
    !     The atoms which get passed here must be exactly the
    !     same and in the same order in the force calculations
    !     as in the original neighbour list calculation !
    !
    !     To be able to pass back the P information of j pairs,
    !     we also pass and receive the atom indices and direction information
    !     of the atoms in j pairs.
    !     

    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !        Loop over directions
       do d=1,8
          j=0; jj=0
          do i=1,myatoms
             if (iand(atpassflag(i),passbit(d)) .ne. 0) then
                j=j+1
                if (j .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow1', int(myproc))
                i3=i*3-3
                j3=j*3-3
                j2=j*2-2
                xsendbuf(j3+1)=X(i3+1)*box(1)
                xsendbuf(j3+2)=X(i3+2)*box(2)
                xsendbuf(j3+3)=X(i3+3)*box(3)
                isendbuf(j2+1)=i
                isendbuf(j2+2)=atype(i)

             endif
          enddo

          if (sendfirst(d)) then
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             if (jj > 0) then
                call mpi_recv(buf(NP*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(NP*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             if (jj > 0) then
                call mpi_recv(buf(NP*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(NP*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
          endif
          j=jj
          do i=1,j
             dirbuf(NP+i)=d
          enddo
          NP=NP+j
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif
    if (NP .ne. np0pairtable) then
       write (6,*) 'EAMforces np0 problem ',myproc,NP,np0pairtable
    endif
    !print *,'Node received totally ',myproc,NP-myatoms


    !      
    !     Stage 3�: Calculate actual forces
    !

    !-----------------------------------------------------------------------
    !     FORCE CALCULATION 
    !-----------------------------------------------------------------------

    !
    !     In PARCAS interface, use buf instead of X, pbuf instead of A.
    !     pbuf contains actual accelerations, buf actual coordinates
    !     (NOT in PARCAS internal units, scaled by 1/box)
    !
    !     Take care to calculate pressure, total pot. only for atoms in my node !
    !
    !

    !
    !     RNMEF AND RNLEF ARE THE DIFFERENCES BETWEEN DISTANCE
    !     AND THE POTENTIAL CUTOFF
    !
    !
    !  BOUNDARY CONDITIONS
    !  IF PBC EQ 1.0d0, PERIODIC - NOTE THAT DIFFERENT CONDITIONS MAY BE SPECIFIED
    !                            IN X, Y AND Z DIRECTIONS.
    !
    !   IF       DX > BOX2(1)         XL=2*BOX2(1)
    ! -BOX2(1) < DX < BOX2(1)         XL=0
    !            DX < -BOX2(1)        XL=-2*BOX2(1)
    !  I.E. IF A SEPARATION EXCEEDS HALF THE BOX SIDE, IT IS TREATED AS
    !  COMING FROM THE ADJACENT BOX.
    !
    !  N.B. NO PARTICLES ARE MOVED AT THIS STAGE.
    !
    !
    ! Initialize force calculation stuff 
    !

    NP3 = NP*3
    NP32 = NP3-2

    BOX2X=box(1)/2.0
    BOX2Y=box(2)/2.0
    BOX2Z=box(3)/2.0

    BOXX2=box(1)*2.0
    BOXY2=box(2)*2.0
    BOXZ2=box(3)*2.0

    do N = 1,NP3
       pbuf(N) = zero
    enddo
    do N=1,NP
       ebuf(N) = zero
    enddo
    do N = 1,myatoms*3
       A(N) = 0.0
    enddo

    calc_vir = .false.
    if (movietime_val .or. calc_avgvir .or. last_step_predict) then
     if (moviemode == 15 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
      calc_vir = .true.
     endif
    endif

    do N = 1,myatoms
       Epair(N) = zero
       Ethree(N) = zero
       wxxi(N) = zero
       wyyi(N) = zero
       wzzi(N) = zero

       if (calc_vir) then
          wxyi(N)=zero
          wxzi(N)=zero
          wyxi(N)=zero
          wyzi(N)=zero
          wzxi(N)=zero
          wzyi(N)=zero
       endif

    enddo
    Vpair=0.0
    Vthree=0.0
    wxx=0.0
    wyy=0.0
    wzz=0.0

    if (calc_vir) then
       wxy=zero
       wxz=zero
       wyx=zero
       wyz=zero
       wzx=zero
       wzy=zero
    endif

    do j=1,nsh
       wxxsh(j)=zero
       wyysh(j)=zero
       wzzsh(j)=zero
       natsh(j)=0
    enddo
    maxr=sqrt(box(1)*box(1)+box(2)*box(2)+box(3)*box(3))/2.0
    VIR = 0.0

    !
    !     K holds index of neighbour list
    !
    K=0

    if (firsttime) then

       allocate(IATAB(itypelow:itypehigh,itypelow:itypehigh))
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
             IATAB(i,j)=1
             if (iac(i,j) == 1) then
                if (element(i)=='Si'.and.element(j)=='Si') then
                   IATAB(i,j)=1
                else if (element(i)=='Si'.and.element(j)=='O') then
                   IATAB(i,j)=4
                else if (element(i)=='O'.and.element(j)=='Si') then
                   IATAB(i,j)=4
                else if (element(i)=='O'.and.element(j)=='O') then
                   IATAB(i,j)=5
                else
                   write (6,*) 'Silica_Force error: can''t handle pair'
                   write(6,*) element(i),element(j)
                   write(6,*) 'Please extend stilweb.f arrays'
                   call my_mpi_abort('Silica_Force', int(myproc))
                endif
             endif
          enddo
       enddo


       !        
       !  Modify potential parameters
       !
       do i=1,5
          if (reppotcutin /= 10.0d0) then
             reppotcut(i)=reppotcutin
          endif
       enddo


    endif


    !-------------------------------------------------------------------------------
    !
    !     CALCULATE ACCELERATIONS USING NEIGHBOUR LIST
    !     --------------------------------------------
    !

    PH=0.0
    XL=0.0
    XM=0.0
    YL=0.0
    YM=0.0
    ZL=0.0
    ZM=0.0

    do IATN=1,myatoms             !  <------- main loop over atoms N start
       N=IATN*3-2
       IATYP1=abs(ibuf(IATN*2))

       K = K + 1          
       nngbr = nborlist(K)
       if (nngbr .lt. 0) then
          print *,'Silica force: nborlist HORROR ERROR !',nngbr
       endif

       do ingbr=1,nngbr       !  <----- loop over neighbours  M start

          K = K + 1
          IATM=nborlist(K)

          M = IATM*3-2

          if (N == M) cycle

          IATYP2=abs(ibuf(IATM*2))

          sw=.true.
          if (iac(IATYP1,IATYP2) /= 1) then
             sw=.false.
             ! Handle other interaction types
             if (iac(IATYP1,IATYP2) == 0) cycle
             if (iac(IATYP1,IATYP2) == -1) then
                write (6,*) 'ERROR: IMPOSSIBLE INTERACTION'
                write (6,*) myproc,i,j,IATYP1,IATYP2
                call my_mpi_abort('INTERACTION -1', int(myproc))
             endif
          endif

          IAIND=IATAB(IATYP1,IATYP2) ! 3body - if 
          DXNM = buf(N) - buf(M)
          if(PBC(1).ne.1.0d0) goto 140
          XM=sign(BOX2X,DXNM+BOX2X)+sign(BOX2X,DXNM-BOX2X)
          DXNM=DXNM-XM
140       DXNMS=DXNM*DXNM

          a1 = element_numbers(iatyp1)
          a2 = element_numbers(iatyp2)
!!!paraSiga0 = paraSigaP(a1,a2)*paraSigaP(a1,a2)

          !           SKIP ROUND THE CALCULATION IF SEPARATION EXCEEDS R0.
!!!if(dxnms > paraSiga0) cycle
!!!dynmsm = paraSiga0 - dxnms

          DYNM = buf(N+1) - buf(M+1)
          if(PBC(2).ne.1.0d0) goto 150
          YM=sign(BOX2Y,DYNM+BOX2Y)+sign(BOX2Y,DYNM-BOX2Y)
          DYNM=DYNM-YM
150       DYNMS=DYNM*DYNM
!!!if(DYNMS.gt.DYNMSM) cycle
!!!DZNMSM = DYNMSM- DYNMS
          DZNM = buf(N+2) - buf(M+2)
          if(PBC(3).ne.1.0d0) goto 160
          ZM=sign(BOX2Z,DZNM+BOX2Z)+sign(BOX2Z,DZNM-BOX2Z)
          DZNM=DZNM-ZM
160       DZNMS=DZNM*DZNM
!!!if(DZNMS.gt.DZNMSM) cycle
          RNMSQ = DXNMS + DYNMS + DZNMS
          RNM = sqrt(RNMSQ)

          rnmef = rnm - paraSigaP(a1,a2) ! r_ij - sigma_ij*A_ij

          PH=0.0
          DPH=0.0

          ! CHECK FOR CUTOFF
          ! According to stilweb publication, also 3-body
          ! part should be skipped if this is less than cutoff.
          ! However, this parametrization requires 3-body interaction, 
          ! otherwise bad discontinuities appear.

          if (rnmef < -0.003 .or. iac(IATYP1,IATYP2)==2) then ! 0.003 is chosen to ensure numerical stability

             !  TWO PARTICLE POTENTIAL

             ! Use Newtons third law for 2-body part; take care
             ! to pick exactly half of all interactions
             if (DXNM>0.0.or.(DXNM==0.0.and.DYNM>0.0).or.                    &
                  (DXNM==0.0.and.DYNM==0.0.and.DZNM>0.0)) then

                if (iac(IATYP1,IATYP2)==2) then
                   call splinereppot(rnm,ph,dph,1d20,1d20,iatyp1,iatyp2,dummy1,dummy2)

                else

                   ! Pair interaction smoothing function
                   ! See J. Chem. Phys. 115 (2001) 6679-6690
                   ! This implementation should be changed if Cl and F atoms 
                   ! are present (change if-statements like if ( a1==2 .and. a2/=2 )

                   smoothmin = paraBigR(1,2) - paraD(1,2)
                   smoothmax = paraBigR(1,2) + paraD(1,2)

                   ! The base atom is oxygen, the second atom is not
                   if ( a1==2 .and. a2==1 ) then

                      ! Find non-oxygen neighbours of a1
                      k1 = K
                      zsmooth = 0.0
                      do jngbr=ingbr+1,nngbr
                         K1=K1+1          
                         IATL=nborlist(K1)
                         L=IATL*3-2
                         if( L==N .or. L==M ) cycle
                         IATYP3=abs(ibuf(IATL*2))
                         a3 = element_numbers(iatyp3)

                         ! non oxygen found
                         ! Now only Si atoms accepted because no F or Cl present
                         if ( a3 == 1 ) then
                            DXNL = buf(N) - buf(L)
                            if (PBC(1) == 1.0d0) then
                               XL=sign(BOX2X,DXNL+BOX2X)+sign(BOX2X,DXNL-BOX2X)
                               DXNL=DXNL-XL
                            end if
                            DXNLS=DXNL*DXNL

                            DYNL = buf(N+1) - buf(L+1)
                            if (PBC(2) == 1.0d0) then
                               YL=sign(BOX2Y,DYNL+BOX2Y)+sign(BOX2Y,DYNL-BOX2Y)
                               DYNL=DYNL-YL
                            end if
                            DYNLS=DYNL*DYNL

                            DZNL = buf(N+2) - buf(L+2)
                            if (PBC(3) == 1.0d0) then
                               ZL=sign(BOX2Z,DZNL+BOX2Z)+sign(BOX2Z,DZNL-BOX2Z)
                               DZNL=DZNL-ZL
                            end if
                            DZNLS=DZNL*DZNL

                            RNLSQ = DXNLS + DYNLS + DZNLS
                            RNL = sqrt(RNLSQ)

                            if ( rnl < smoothmin ) then
                               zsmooth = zsmooth + 1.0d0
                            else if ( rnl >= smoothmin .and. rnl < smoothmax ) then
                               zsmooth = zsmooth + 1.0d0 - &
                                    (rnl-smoothmin)/(2.0d0*paraBigR(1,2)) + &
                                    sin( 3.14159d0*(rnl-smoothmin)/paraD(1,2))/6.28318d0
                            endif

                         end if
                      end do

                      ! Smoothing function
                      if ( zsmooth == 0.0 ) then
                         gsmooth = 1.0d0
                      else
                         tmp = zsmooth - p5
                         gsmooth =  p1*exp(p4*tmp*tmp)/(exp((p2-zsmooth)/p3)+1.0d0)
                      end if

                      ! The base atom is not oxygen but the second atom is
                   else  if ( a1==1 .and. a2==2 ) then

                      ! Find number of neighbours for atom M (=a2)
                      sj = 1
                      do si=1,iatm
                         snngbr = nborlist(sj)  ! number of neighbours
                         sj = sj + snngbr + 1
                      end do
                      siatm = sj - snngbr - 1   ! place of atom M (a2) on neigbourlist

                      ! Find non-oxygen neighbours of a2
                      zsmooth = 0.0
                      sk = 0
                      do jngbr=1,snngbr
                         sk = sk + 1
                         IATL=nborlist(siatm+sk)
                         L=IATL*3-2
                         if( L==N .or. L==M ) cycle
                         IATYP3=abs(ibuf(IATL*2))
                         a3 = element_numbers(iatyp3)

                         ! non oxygen found
                         ! Now only Si atoms accepted because no F or Cl present
                         if ( a3 == 1 ) then

                            DXML = buf(M) - buf(L)
                            if (PBC(1) == 1.0d0) then
                               XL=sign(BOX2X,DXML+BOX2X)+sign(BOX2X,DXML-BOX2X)
                               DXML=DXML-XL
                            end if
                            DXMLS=DXML*DXML

                            DYML = buf(M+1) - buf(L+1)
                            if (PBC(2) == 1.0d0) then
                               YL=sign(BOX2Y,DYML+BOX2Y)+sign(BOX2Y,DYML-BOX2Y)
                               DYML=DYML-YL
                            end if
                            DYMLS=DYML*DYML

                            DZML = buf(M+2) - buf(L+2)
                            if (PBC(3) == 1.0d0) then
                               ZL=sign(BOX2Z,DZML+BOX2Z)+sign(BOX2Z,DZML-BOX2Z)
                               DZML=DZML-ZL
                            end if
                            DZMLS=DZML*DZML

                            rmlsq = DXMLS + DYMLS + DZMLS
                            rml = sqrt(rmlsq)

                            if ( rml < smoothmin ) then
                               zsmooth = zsmooth + 1.0d0
                            else if ( rml >= smoothmin .and. rml < smoothmax ) then
                               zsmooth = zsmooth + 1.0d0 - &
                                    (rml-smoothmin)/(2.0d0*paraBigR(1,2)) + &
                                    sin( 3.14159d0*(rml-smoothmin)/paraD(1,2))/6.28318d0
                            endif

                         end if
                      end do

                      ! Smoothing function
                      if ( zsmooth == 0.0 ) then
                         gsmooth = 1.0d0
                      else
                         tmp = zsmooth - p5
                         gsmooth =  p1*exp(p4*tmp*tmp)/(exp((p2-zsmooth)/p3)+1.0d0)
                      end if

                   else
                      gsmooth = 1.0d0
                   end if

                   ! Pair potential
                   paraEpsA = paraA(a1,a2)*paraEps(a1,a2)*gsmooth
                   ph1      = paraEpsA*exp( paraC(a1,a2)*paraSigma(a1,a2)/rnmef )
                   help1    = paraB(a1,a2)*(paraSigma(a1,a2)/rnm)**parap(a1,a2)
                   help2    = (paraSigma(a1,a2)/rnm)**paraq(a1,a2)
                   ph  = (help1-help2)*ph1

                   ! Pair force
                   dph = (parap(a1,a2)*help1 - paraq(a1,a2)*help2)/rnm + &
                        (help1-help2)*paraC(a1,a2)*paraSigma(a1,a2)/(rnmef*rnmef)
                   dph = -ph1*dph

                   !                 add repulsive pair potential times fermi function
                   !                 (13.4.1993 Kai Nordlund)
                   if (rnm < reppotcut(iaind)) then
                      t1=mpi_wtime()
                      call splinereppot(rnm,ph,dph,                          &
                           bf(iaind),rf(iaind),iatyp1,iatyp2,dummy1,dummy2)
                      tmr(32)=tmr(32)+(mpi_wtime()-t1)
                   endif

                endif

                ! CALCULATE PHYSICAL PROPERTIES
                ! DAI = (X/R) (D POT/DR) = VECTOR COMPONENT OF FORCE/ACCELERATIONS

                HALFPH=PH/2.0
                ebuf(IATN)=ebuf(IATN)+HALFPH
                ebuf(IATM)=ebuf(IATM)+HALFPH
                VPair=Vpair+PH

                AI=-RNM*DPH

                AI = AI/RNMSQ
                DAI = DXNM*AI
                pbuf(N) = pbuf(N) + DAI
                pbuf(M) = pbuf(M) - DAI
                wxxi(IATN)=wxxi(IATN)+DAI*DXNM

                if (calc_vir) then
                   wxyi(IATN)=wxyi(IATN)+DAI*DYNM
                   wxzi(IATN)=wxzi(IATN)+DAI*DZNM
                endif

                DAI = DYNM*AI
                pbuf(N+1) = pbuf(N+1) + DAI
                pbuf(M+1) = pbuf(M+1) - DAI
                wyyi(IATN)=wyyi(IATN)+DAI*DYNM

                if (calc_vir) then
                   wyxi(IATN)=wyxi(IATN)+DAI*DXNM
                   wyzi(IATN)=wyzi(IATN)+DAI*DZNM
                endif

                DAI = DZNM*AI
                pbuf(N+2) = pbuf(N+2) + DAI
                pbuf(M+2) = pbuf(M+2) - DAI
                wzzi(IATN)=wzzi(IATN)+DAI*DZNM

                if (calc_vir) then
                   wzxi(IATN)=wzxi(IATN)+DAI*DXNM
                   wzyi(IATN)=wzyi(IATN)+DAI*DYNM
                endif

             endif

          endif

          if (iac(IATYP1,IATYP2) /= 1) cycle

          K1=K
          do jngbr=ingbr+1,nngbr      ! <---- three body loop over atoms L

             K1=K1+1          
             IATL=nborlist(K1)
             L=IATL*3-2
             if (N == L .or. N == M) cycle

             IATYP3=abs(ibuf(IATL*2))

             ! Handle other interaction types
             if (iac(IATYP1,IATYP3) /= 1) cycle

             ! a1 is the type of the middle atom
             a3 = element_numbers(iatyp3)
             !if ( a1==2 .and. a2==2 .and. a3==2 ) cycle ! case O-O-O
             if ( (a1==2 .and. a2==2) .or. (a1==2 .and. a3==2) ) cycle ! 0-0-Si, Si-O-O, O-O-O
             a1tmp=a1; a2tmp=a2; a3tmp=a3

             IAIND2=IATAB(IATYP1,IATYP3)

             DXNL = buf(N) - buf(L)
             if (PBC(1).ne.1.0d0) goto 540
             XL=sign(BOX2X,DXNL+BOX2X)+sign(BOX2X,DXNL-BOX2X)
             DXNL=DXNL-XL
540          DXNLS=DXNL*DXNL


             ! T�M�N MERKITYS ?
!!!if ( paraSigaP(a1tmp,a3tmp) < paraSigaP(a1tmp,a2tmp) ) then
!!!   paraSiga0 = paraSigaP(a1tmp,a3tmp)*paraSigaP(a1tmp,a3tmp)
!!!else
!!!   paraSiga0 = paraSigaP(a1tmp,a2tmp)*paraSigaP(a1tmp,a2tmp)
!!!end if
!!!!paraSiga0 = paraSigaP(a1tmp,a3tmp)*paraSigaP(a1tmp,a2tmp)
!!!if (dxnls.gt. paraSiga0) cycle
!!!dynlsm = paraSiga0 - dxnls


             DYNL = buf(N+1) - buf(L+1)
             if (PBC(2).ne.1.0d0) goto 550
             YL=sign(BOX2Y,DYNL+BOX2Y)+sign(BOX2Y,DYNL-BOX2Y)
             DYNL=DYNL-YL
550          DYNLS=DYNL*DYNL
!!!if (DYNLS.gt.DYNLSM) cycle

!!!DZNLSM = DYNLSM-DYNLS
             DZNL = buf(N+2) - buf(L+2)
             if (PBC(3).ne.1.0d0) goto 560
             ZL=sign(BOX2Z,DZNL+BOX2Z)+sign(BOX2Z,DZNL-BOX2Z)
             DZNL=DZNL-ZL
560          DZNLS=DZNL*DZNL
!!!if (DZNLS.gt.DZNLSM) cycle

             RNLSQ = DXNLS + DYNLS + DZNLS
             RNL = sqrt(RNLSQ)

             rnmef = rnm - paraSigaT1(a2tmp,a1tmp,a3tmp)
             rnlef = rnl - paraSigaT2(a2tmp,a1tmp,a3tmp)

             !              THREE PARTICLE POTENTIAL
             !              
             !              CHECK FOR CUTOFF (SKIP OVER IF EXCEEDED)

             if (rnlef > -0.03) cycle  ! Numerical errors if -0.03<rnlef<0.0
             if (rnmef > -0.03) cycle

             COSMNL=(DXNM*DXNL+DYNM*DYNL+DZNM*DZNL)/(RNM*RNL)
             coss = cosmnl - paracostheta(a2tmp,a1tmp,a3tmp)

             !              potential
             hmnl = sqrt(paraEps(a1tmp,a2tmp)*paraEps(a1tmp,a3tmp))* &
                  paraLambda(a2tmp,a1tmp,a3tmp)* &
                  exp( paragamma1(a2tmp,a1tmp,a3tmp)* &
                  paraSigma(a1tmp,a2tmp)/rnmef + &
                  paragamma2(a2tmp,a1tmp,a3tmp)*paraSigma(a1tmp,a3tmp)/rnlef)* &
                  abs(coss)**paraAlpha(a2tmp,a1tmp,a3tmp)

             !              physical properties
             Ethree(IATN) = Ethree(IATN) + hmnl
             Vthree = Vthree + hmnl

             rnma   = (paragamma1(a2tmp,a1tmp,a3tmp)*paraSigma(a1tmp,a2tmp)*hmnl)/ &
                  (rnmef*rnmef*rnm)
             rnla   = (paragamma2(a2tmp,a1tmp,a3tmp)*paraSigma(a1tmp,a3tmp)*hmnl)/ &
                  (rnlef*rnlef*rnl)
             hcos   = paraAlpha(a2tmp,a1tmp,a3tmp)*hmnl/coss
             hkmnl  = hcos/(rnm*rnl)
             hkmnlm = hcos*cosmnl/rnmsq
             hkmnll = hcos*cosmnl/rnlsq

             DPHM=-RNMA*DXNM+HKMNL*DXNL-HKMNLM*DXNM
             DPHL=-RNLA*DXNL+HKMNL*DXNM-HKMNLL*DXNL
             DPHN=-DPHM-DPHL
             pbuf(N)=pbuf(N)+DPHN
             pbuf(M)=pbuf(M)+DPHM
             pbuf(L)=pbuf(L)+DPHL
             wxxi(IATN)=wxxi(IATN)-DPHM*DXNM-DPHL*DXNL

             if (calc_vir) then
                wxyi(IATN)=wxyi(IATN)-DPHM*DYNM-DPHL*DYNL
                wxzi(IATN)=wxzi(IATN)-DPHM*DZNM-DPHL*DZNL
             endif

             DPHM=-RNMA*DYNM+HKMNL*DYNL-HKMNLM*DYNM
             DPHL=-RNLA*DYNL+HKMNL*DYNM-HKMNLL*DYNL
             DPHN=-DPHM-DPHL
             pbuf(N+1)=pbuf(N+1)+DPHN
             pbuf(M+1)=pbuf(M+1)+DPHM
             pbuf(L+1)=pbuf(L+1)+DPHL
             wyyi(IATN)=wyyi(IATN)-DPHM*DYNM-DPHL*DYNL

             if (calc_vir) then
                wyxi(IATN)=wyxi(IATN)-DPHM*DXNM-DPHL*DXNL
                wyzi(IATN)=wyzi(IATN)-DPHM*DZNM-DPHL*DZNL
             endif

             DPHM=-RNMA*DZNM+HKMNL*DZNL-HKMNLM*DZNM
             DPHL=-RNLA*DZNL+HKMNL*DZNM-HKMNLL*DZNL
             DPHN=-DPHM-DPHL
             pbuf(N+2)=pbuf(N+2)+DPHN
             pbuf(M+2)=pbuf(M+2)+DPHM
             pbuf(L+2)=pbuf(L+2)+DPHL
             wzzi(IATN)=wzzi(IATN)-DPHM*DZNM-DPHL*DZNL

             if (calc_vir) then
                wzxi(IATN)=wzxi(IATN)-DPHM*DXNM-DPHL*DXNL
                wzyi(IATN)=wzyi(IATN)-DPHM*DYNM-DPHL*DYNL
             endif

          enddo              !  <---- end of three body loop over atoms L

       enddo                  !  <---- end of 2-body loop over atoms M

       wxxi(IATN)=wxxi(IATN)/(box(1)*box(1))
       wyyi(IATN)=wyyi(IATN)/(box(2)*box(2))
       wzzi(IATN)=wzzi(IATN)/(box(3)*box(3))

       if (calc_vir) then
          wxyi(IATN)=wxyi(IATN)/(box(1)*box(2))
          wxzi(IATN)=wxzi(IATN)/(box(1)*box(3))
          wyxi(IATN)=wyxi(IATN)/(box(2)*box(1))
          wyzi(IATN)=wyzi(IATN)/(box(2)*box(3))
          wzxi(IATN)=wzxi(IATN)/(box(3)*box(1))
          wzyi(IATN)=wzyi(IATN)/(box(3)*box(2))
       endif

       wxx=wxx+wxxi(IATN)
       wyy=wyy+wyyi(IATN)
       wzz=wzz+wzzi(IATN)

       if (calc_vir) then
          wxy=wxy+wxyi(IATN)
          wxz=wxz+wxzi(IATN)
          wyx=wyx+wyxi(IATN)
          wyz=wyz+wyzi(IATN)
          wzx=wzx+wzxi(IATN)
          wzy=wzy+wzyi(IATN)
       endif

       dx=buf(N)-ECM(1)
       dy=buf(N+1)-ECM(2)
       dz=buf(N+2)-ECM(3)
       r=sqrt(dx*dx+dy*dy+dz*dz)

       nnsh=int(1.0d0+r/maxr*(1.0d0*nsh-1.0d0))

       do j=nsh,nnsh,-1
          wxxsh(j)=wxxsh(j)+wxxi(IATN)
          wyysh(j)=wyysh(j)+wyyi(IATN)
          wzzsh(j)=wzzsh(j)+wzzi(IATN)
          natsh(j)=natsh(j)+1
       enddo

180    continue               !  <-------- end of main loop over atoms N

    enddo

    !
    !     Note: wxx should not be summed over processors here, wxxsh should.
    !      
     
    call my_mpi_dsum(wxxsh, nsh)
    call my_mpi_dsum(wyysh, nsh)
    call my_mpi_dsum(wzzsh, nsh)
     
    call my_mpi_isum(natsh, nsh)
     
    !
    !     Stage 4�: Copy my forces to array A()
    !
    !     Also scale A to correspond to PARCAS xnp array units
    !
    do N = 1,myatoms
       n3=N*3-2
       Epair(N)=ebuf(N)
       A(n3  ) = pbuf(n3  )/box(1)
       A(n3+1) = pbuf(n3+1)/box(2)
       A(n3+2) = pbuf(n3+2)/box(3)
    enddo


    !-----------------------------------------------------------------------
    !     PASSING BACK STAGE
    !-----------------------------------------------------------------------

    !
    !     Stage 5�: Pass back accelerations of neighbour atoms to them
    !

    !
    !  From the array dirbuf, we can figure out from which direction an
    !  atom has been received, and from ibuf what index it has had there. Thus
    !  we can send the pbuf and atom index information to other nodes,
    !  and when receiving it know which atom it belongs to.
    !
    i=0
    IF(debug)PRINT *,'SW pass back',myproc,NP,myatoms
    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !        print *,'Stilweb loop 2, pbuf sendrecv',myproc
       !        Loop over directions
       do d=1,8
          !           Loop over neighbours
          i=0
          do j=myatoms+1,NP
             dfrom=dirbuf(j)+4
             if (dfrom .gt. 8) dfrom=dfrom-8
             if (d .eq. dfrom) then
                i=i+1
                if (i .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow2', int(myproc))
                ifrom=ibuf(j*2-1)
                isendbuf(i)=ifrom
                i3=i*3-3
                j3=j*3-3
                psendbuf(i)=ebuf(j)
                xsendbuf(i3+1)=pbuf(j3+1)
                xsendbuf(i3+2)=pbuf(j3+2)
                xsendbuf(i3+3)=pbuf(j3+3)
             endif
          enddo
          !           print *,'send',myproc,d,i,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !              print *,'received',myproc,d,ii
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !              print *,'received',myproc,d,i
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), &
                  d, mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
          endif
          i=ii
          do ii=1,i
             myi=isendbuf2(ii)
             if (myi .gt. myatoms) then
                print *,myproc,myatoms,myi,ii,i
                call my_mpi_abort('SW i too large', int(myproc))
             endif
             i3=myi*3-3
             ii3=ii*3-3
             Epair(myi)=Epair(myi)+psendbuf2(ii)
             A(i3+1)=A(i3+1)+xsendbuf2(ii3+1)/box(1)
             A(i3+2)=A(i3+2)+xsendbuf2(ii3+2)/box(2)
             A(i3+3)=A(i3+3)+xsendbuf2(ii3+3)/box(3)
          enddo
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif



    !-----------------------------------------------------------------------
    !     END OF FORCE CALCULATION
    !-----------------------------------------------------------------------


    if (subcmacc) then
       !     
       !     SUBTRACT THE CENTRE OF MASS ACCELERATION
       !     
       ACM(1) = 0.0
       ACM(2) = 0.0
       ACM(3) = 0.0
       IATN=0
       do N = 1,myatoms*3-2,3
          IATN=IATN+1
          ACM(1) = ACM(1) + A(N)
          ACM(2) = ACM(2) + A(N+1)
          ACM(3) = ACM(3) + A(N+2)
       enddo
       !     WRITE(6,*)'A',AXR,AYT,AZT
       call my_mpi_dsum(ACM, 3)

       IATN=0
       do N = 1,myatoms*3-2,3
          IATN=IATN+1
          A(N) = A(N) - ACM(1)/natoms
          A(N+1) = A(N+1) - ACM(2)/natoms
          A(N+2) = A(N+2) - ACM(3)/natoms
       enddo
    endif

    !      print *,'swend'

    firsttime=.false.
    return

  end subroutine Silica_Force

  !-----------------------------------------------------------------------------

  subroutine Init_Silica_Pot()
    use my_mpi
    use typeparam
    use silicaparam
    implicit none

    character*12, parameter :: silicaparameters = 'in/silica.in'
    integer i,j,k
    real*8 sigma
    integer iatab
    integer :: npara, nline
    character*80 :: line
    real*8, dimension(64):: values
    integer :: status

    ! Find out which element numbers the user has given to the particular 
    ! elements that this potential can handle. Notice that the order of 
    ! elements in parameter tables is pre-defined and thus also the 
    ! ifs should be in the same order.
    if ( itypelow<0 .or. itypehigh>5 ) then
       write (6,*) 'Stilweb error: element number out of range'
       write(6,*) itypelow, '- ', itypehigh
       write(6,*) 'Please modify module silicaparam in modules.f90 and silica.f90'
       call my_mpi_abort('stilwebpot', int(itypelow))
    end if
    element_numbers = -1
    do i=itypelow,itypehigh
       if (element(i)=='Si') then
          element_numbers(i) = 1
       else if (element(i)=='O') then
          element_numbers(i) = 2
       else if (element(i)=='Cl') then
          element_numbers(i) = 3
       else if (element(i)=='F') then
          element_numbers(i) = 4
       else if (element(i)=='Ge') then
          element_numbers(i) = 5
       else
          element_numbers(i) = -1  ! Element not included to .f90is potential
       endif
    enddo


    paraA=0.0;paraB=0.0;paraC=0.0;parap=0.0;paraq=0.0
    paraR=0.0;paraD=0.0;paraEps=0.0;paraSigma=0.0
    paragamma1=0.0;paragamma2=0.0;paralambda=0.0;paracostheta=0.0
    paraBigR=0.0;paraSigaP=0.0;paraSigaT1=0.0;paraSigaT2=0.0;paraAlpha=0.0


    ! Read parameter file silica.in
    open(unit=25,file=silicaparameters,action='read',iostat=status)
    if ( status /= 0 ) then
       write(6,*) 'Silica parameter file could not be opened for reading!'
       call my_mpi_abort('silicapot', int(status))
    end if


    npara = 0; nline = 0
    do
       nline = nline + 1
       read(25,*,iostat=status) line

       if (status < 0) then
          exit
       else if ( status > 0 ) then
          write(6,*) 'Error while reading the silica parameter file in line:', nline
          call my_mpi_abort('silicapot', int(nline))
       end if

       if (line == '') cycle
       if (line(1:1) == '#') cycle ! comment line

       npara = npara + 1
       read(unit=line,fmt=*,iostat=status) values(npara)
       if (status /= 0) then
          write(6,*) 'Invalid value in the silica parameter file in line:', nline
          call my_mpi_abort('silicapot', int(nline))
       endif
    enddo
    close(25)

    do i=1,npara
       write(*,*) i,values(i)
    end do

    sigma = values(1) ! �

    ! Fill parameter table

    ! Si-Si
    paraA(1,1)     = values(2)
    paraB(1,1)     = values(3)
    paraC(1,1)     = values(4)
    parap(1,1)     = values(5)
    paraq(1,1)     = values(6)
    paraEps(1,1)   = values(7)
    paraSigma(1,1) = values(8)*sigma
    paraBigR(1,2)  = 0.0
    paraD(1,2)     = 0.0
    paraSigaP(1,1) = values(9)*sigma

    ! Si-O
    paraA(1,2)     = values(10)
    paraB(1,2)     = values(11)
    paraC(1,2)     = values(12)
    parap(1,2)     = values(13)
    paraq(1,2)     = values(14)
    paraEps(1,2)   = values(15)
    paraSigma(1,2) = values(16)*sigma
    paraBigR(1,2)  = values(17)
    paraD(1,2)     = values(18)
    paraSigaP(1,2) = values(19)*sigma

    ! 0-Si (symmetrical)
    paraA(2,1)     = paraA(1,2)
    paraB(2,1)     = paraB(1,2)
    paraC(2,1)     = paraC(1,2)
    parap(2,1)     = parap(1,2)
    paraq(2,1)     = paraq(1,2)
    paraEps(2,1)   = paraEps(1,2)
    paraSigma(2,1) = paraSigma(1,2)
    paraBigR(2,1)  = paraBigR(1,2)
    paraD(2,1)     = paraD(1,2)
    paraSigaP(2,1) = paraSigaP(1,2)

    ! O-O
    paraA(2,2)     = values(20)
    paraB(2,2)     = values(21)
    paraC(2,2)     = values(22)
    parap(2,2)     = values(23)
    paraq(2,2)     = values(24)
    paraEps(2,2)   = values(25)
    paraSigma(2,2) = values(26)*sigma
    paraBigR(2,2)  = values(27)
    paraD(2,2)     = values(28)
    paraSigaP(2,2) = values(29)*sigma

    ! Si-Si-Si
    paralambda(1,1,1)   = values(30)
    paragamma1(1,1,1)   = values(31)
    paragamma2(1,1,1)   = values(32)
    paracostheta(1,1,1) = values(33)
    paraAlpha(1,1,1)    = 2.0d0*values(34)
    paraSigaT1(1,1,1)   = values(35)*sigma
    paraSigaT2(1,1,1)   = values(36)*sigma

    ! Si-Si-O
    paralambda(1,1,2)   = values(37)
    paragamma1(1,1,2)   = values(38)
    paragamma2(1,1,2)   = values(39)
    paracostheta(1,1,2) = values(40)
    paraAlpha(1,1,2)    = 2.0d0*values(41)
    paraSigaT1(1,1,2)   = values(42)*sigma
    paraSigaT2(1,1,2)   = values(43)*sigma

    ! O-Si-Si
    paralambda(2,1,1)   = values(44)
    paragamma1(2,1,1)   = values(45)
    paragamma2(2,1,1)   = values(46)
    paracostheta(2,1,1) = values(47)
    paraAlpha(2,1,1)    = 2.0d0*values(48)
    paraSigaT1(2,1,1)   = values(49)*sigma
    paraSigaT2(2,1,1)   = values(50)*sigma

    ! Si-O-Si
    paralambda(1,2,1)   = values(51)
    paragamma1(1,2,1)   = values(52)
    paragamma2(1,2,1)   = values(53)
    paracostheta(1,2,1) = values(54)
    paraAlpha(1,2,1)    = 2.0d0*values(55)
    paraSigaT1(1,2,1)   = values(56)*sigma
    paraSigaT2(1,2,1)   = values(57)*sigma

    ! O-Si-O
    paralambda(2,1,2)   = values(58)
    paragamma1(2,1,2)   = values(59)
    paragamma2(2,1,2)   = values(60)
    paracostheta(2,1,2) = values(61)
    paraAlpha(2,1,2)    = 2.0d0*values(62)
    paraSigaT1(2,1,2)   = values(63)*sigma
    paraSigaT2(2,1,2)   = values(64)*sigma

    ! Si-O-O and O-O-O no interaction, all parameters zero and  
    ! cycle statements in code


    ! Cutoff distances
    do i=itypelow,itypehigh
       do j=itypelow,itypehigh
          if (iac(i,j) == 1) then
             if (element(i)=='Si'.and.element(j)=='Si') then
                rcut(i,j)=paraSigaP(1,1)
             else if (element(i)=='Si'.and.element(j)=='O') then
                rcut(i,j)=paraSigaP(1,2)
             else if (element(i)=='O'.and.element(j)=='Si') then
                rcut(i,j)=paraSigaP(2,1)
             else if (element(i)=='O'.and.element(j)=='O') then
                rcut(i,j)=paraSigaP(2,2)
             else
                write (6,*) 'Silica potential error: can''t handle pair'
                write(6,*) element(i),element(j)
                write(6,*) 'Please extend size of arrays in silica.f90'
                call my_mpi_abort('silicapot', int(i))
             endif
          else if (iac(i,j) == 2) then
             rcut(i,j)=paraSigaP(1,1)
          endif
       enddo
    enddo

  end subroutine Init_Silica_Pot
