  
  !******************************************************************
  
  logical function movietime(istep,nmovie,time_fs,                      &
       tmovie,dtmovie,tmax,restartt)
  use defs  
    implicit none
    !
    !     Determine whether it is time to output the movie:
    !     if nmovie<0 no movie
    !     otherwise:
    !          ? First time always (we wanna see the premiere after all !)
    !          ? if dtmovie(1) <= 0 use nmovie values
    !          ? if dtmovie(1) > 0 use tmovie, dtmovie logic
    !
    !     Modifications by Tommi Jarvi, 28.10.2008:
    !      -On rare occasions, it happened that prevtime got a value larger
    !       than nextmovietime so that movie output stopped at a random time.
    !      -Corrected the problem by changing the logic.
    !     Modifications by Tommi Jarvi, 5.1.2009:
    !      -Changed so that whenever dtmovie is changed, the movie is output.
    !      -Note that the maximum index for tmovie(i) is 9.
    !      -Note also that tmovie(1:9) by default has to be set very large. Otherwise
    !       the output would not be predictable.


    ! --- variables

    ! input variables
    integer istep,nmovie
    real*8 time_fs,tmovie(*),dtmovie(*),tmax,restartt
    ! internal variables
    integer i  ! loops
    integer currenti  ! current i in dtmovie(i)
    data currenti /1/  ! start from the first one obviously
    save currenti
    real*8 nexttime  ! the next time at which the movie should be output
    data nexttime /0.0/
    save nexttime


    ! --- begin

    ! on first step, step nexttime to time when simulation begins
    if(istep==0 .and. restartt > 0.0) then
       nexttime = restartt
    endif

    ! default: no movie
    movietime=.false.

    ! no output for nmovie < 0 or after the simulation should have ended
    ! (last frame output separately)
    if (nmovie <=0) return
    if (time_fs >= tmax) return

    ! --- BEGIN: check whether movie is output at this timestep

    ! - always take movie at first time step
    if (istep==0) then
       movietime=.true.

       ! - take movies according to nmovie
    else if(dtmovie(1)<=0.0) then
       if (MOD(istep,nmovie) == 0) then
          movietime=.true.
       endif

       ! - take movies according to dtmovie scheme
    else
       ! Check if we should take the movie
       !  (and if so, calculate the time for the
       !   next movie frame)
       if(time_fs > nexttime) then
          movietime=.true.
       endif
    endif

    ! --- Calculate new nexttime for dtmovie scheme
    !     -default to previous + current dtmovie
    !     -always take movie when changing dtmovie
    !     -only move to higher dtmovie index
    !     -a dtmovie index can be skipped if the interval
    !      between the tmovies is small
    if(dtmovie(1) > 0.0 .and. movietime) then
       ! default
       nexttime = nexttime + dtmovie(currenti)

       ! check whether a tmovie is reached before nexttime
       do i=currenti,9
          if (nexttime >= tmovie(i)) then
             currenti = i+1
             nexttime = tmovie(i)
          endif
       enddo
    endif

    ! --- END: check whether movie is output at this timestep

    return
  end function movietime


  !******************************************************************
  logical function slicetime(istep,time_fs,tslice,dtslice)
  use defs  
    implicit none
    !
    !     Determine whether it is time to output the slice movie:
    !          ? if dtslice(1)<0 no slice movie
    !          ? First time always (we wanna see the premiere after all !)
    !          ? if dtslice(1) >= 0 use tslice, dtslice logic
    !
    !     For cascades, center ECM is obtained automatically as energy
    !     center of mass. For non-cascades, it can be read in; thus
    !     ECM, dslice can be used to get any fixed cell region in a
    !     a non-cascade case.
    !
    !     Modifications by Tommi Jarvi, 5.1.2009:
    !      -See movietime() above.
    !      -The only difference is that there is no
    !       check for time_fs >= tmax as there was none
    !       in the original version.
    !

    ! --- variables

    ! input variables
    integer istep
    real*8 time_fs,tslice(*),dtslice(*)

    ! internal variables
    integer i  ! loops
    integer currenti  ! current i in dtmovie(i)
    data currenti /1/  ! start from the first one obviously
    save currenti
    real*8 nexttime  ! the next time at which the movie should be output
    data nexttime /0.0/
    save nexttime


    ! --- begin

    ! no movie for dtslice(1) <= 0
    if (dtslice(1)<=0.0) return

    ! default: no slice movie
    slicetime=.false.

    ! always take movie at first time step
    if (istep==0) then
       slicetime=.true.
    endif

    ! check if we want the movie
    if(time_fs > nexttime) then
       slicetime=.true.
    endif

    ! --- Calculate new nexttime for dtslice scheme
    !     -See movietime() above, the logic here is the same
    if(dtslice(1) > 0.0 .and. slicetime) then
       ! default
       nexttime = nexttime + dtslice(currenti)
       !    WRITE(*,*) "slicenexttime",nexttime,time_fs, dtslice(currenti),currenti
       ! check whether a tslice is reached before nexttime
       do i=currenti,9
          if (nexttime >= tslice(i)) then
             currenti = i+1
             nexttime = tslice(i)
          endif
       enddo
    endif

    return
  end function slicetime


  !******************************************************************

  subroutine Dump_Atoms(x0,x1,box,atomindex,                            &
       atype,P,Fp,Epair,Ethree,delta,aflag,AnySemi,wxxi,wyyi,wzzi,EAM)

    use typeparam
    use datatypes
    use my_mpi

    use defs

    use para_common

    implicit none

    !     -------------------------------------------------------------
    real*8 x0(*),x1(*),box(*),Epair(*),Ethree(*)
    real*8 P(NPMAX,EAMBANDMAX),Fp(NPMAX,EAMBANDMAX)
    real*8 wxxi(*),wyyi(*),wzzi(*)
    integer atype(*),atomindex(*)
    real*8 delta(itypelow:itypehigh)
    logical aflag,AnySemi

    logical EAM


    real*8 boxs(3)

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     myproc = my relative processor number (0..nprocs-1)
    !     nprocs = total number of nodes being used
    !     buf(8*NPMAX) = buffer space for information from other nodes
    !     -------------------------------------------------------------
    !          Local variables
    integer o0,o1,o2,o3

    integer i,k,n,i3
    integer node

    integer(kind=mpi_parameters) :: ierror  ! MPI error code
    !     -------------------------------------------------------------
    logical :: file_exists

    boxs(1)=box(1)*box(1)
    boxs(2)=box(2)*box(2)
    boxs(3)=box(3)*box(3)


    ! Dump out the atom positions and velocities into
    ! mdlat.out for restarts.  Simply move out/mdlat.out to in/mdlat.in
    ! and change the latflag in in/md.in to 3

    ! Due to historical reasons (and for possible future
    ! extension), two "1":s are output after the atom type

    !   atoms.out contains the real coords along with their final energies

    IF(iprint)WRITE(6,'(A,3(F16.10,1X))') 'mdlat output for boxsize',box(1),box(2),box(3)
    do i=itypelow,itypehigh
       IF(iprint)WRITE(6,'(A,I3,A,3F13.8)') 'For atype',i,' to get v [A/fs] from mdlat, multiply by', &
            vunit(i)*box(1), vunit(i)*box(2), vunit(i)*box(3)
    enddo

    inquire(file = "out/mdlat.out", exist = file_exists)
    if (iprint .and. file_exists) call system('mv out/mdlat.out out/mdlat_prev.out')

    IF(iprint)OPEN(8,file='out/mdlat.out',status='replace')
    if (aflag) then
       IF(iprint)OPEN(9,file='out/atoms.out',status='replace')

       ! Removed this defining line since its more trouble than what it's
       ! really worth...
       !         if (AnySemi) then
       !            IF(iprint)WRITE(9,*) 'Final XYZ positions of the atoms'                  &
       !                 ,'    V3     ','           ','    V2     '
       !
       !         else
       !            IF(iprint)WRITE(9,*) ' Final XYZ positions of the atoms'                 &
       !                 ,'  Rho      ','  F(rho)   ','   Epair   '                 
       !              endif
    endif


    ! atoms.out output is:
    ! EAM: x y z [?] rho F(rho)[eV] Epair[eV] index Wxx Wyy Wzz [eV] atype
    ! Semic: x y z [?] Ethree[eV] 0 Epair[eV] index Wxx Wyy Wzz [eV] atype
    !
    ! Because V is not always well defined, Virial W is output in eV
    !
    ! To get kbar from virial, multiply by ev_to_kbar = e/1d-30/1d8
    ! and then divide by the atomic volume Omega.

    do i=1,myatoms
       if ((AnySemi .eqv. .true.) .and. (aflag .eqv. .true.) .and. (EAM .eqv. .false.)) Fp(i,1)=zero
       ! if (atype(i) /= 1) print *,'atype',myproc,i,atype(i)
       i3=3*i-3
       ! Use larger output field if atom is very far
       if ( ABS(x0(i3+1)) >= 100.0d0 .or. &
            ABS(x0(i3+2)) >= 100.0d0 .or. &
            ABS(x0(i3+3)) >= 100.0d0) then
          IF(iprint)WRITE(8,1001)(x0(i3+k),k=1,3),                                &
               atype(i),1,1,(x1(i3+k)/delta(ABS(atype(i))),k=1,3),       &
               atomindex(i)
       else
          IF(iprint)WRITE(8,1000)(x0(i3+k),k=1,3),                                &
               atype(i),1,1,(x1(i3+k)/delta(ABS(atype(i))),k=1,3),       &
               atomindex(i)
       endif
       if (aflag) then
          ! Use larger output field if atom is very far
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then

             if ((AnySemi .eqv. .true. ) .and. (EAM .eqv. .false.)) then
                IF(iprint)WRITE(9,1011)(x0(i3+k)*box(k),k=1,3)                           &
                     ,Ethree(i),Fp(i,1),Epair(i),atomindex(i),                              &
                     wxxi(i)*boxs(1),wyyi(i)*boxs(2),wzzi(i)*boxs(3),atype(i)
             else
                IF(iprint)WRITE(9,1011)(x0(i3+k)*box(k),k=1,3)                           &
                     ,P(i,1),Fp(i,1),Epair(i),atomindex(i),                              &
                     wxxi(i)*boxs(1),wyyi(i)*boxs(2),wzzi(i)*boxs(3),atype(i)
             endif
          else
             if ((AnySemi .eqv. .true. ) .and. (EAM .eqv. .false.)) then         
                IF(iprint)WRITE(9,1010)(x0(i3+k)*box(k),k=1,3)                           &
                     ,Ethree(i),Fp(i,1),Epair(i),atomindex(i),                              &
                     wxxi(i)*boxs(1),wyyi(i)*boxs(2),wzzi(i)*boxs(3),atype(i)
             else
                IF(iprint)WRITE(9,1010)(x0(i3+k)*box(k),k=1,3)                           &
                     ,P(i,1),Fp(i,1),Epair(i),atomindex(i),                              &
                     wxxi(i)*boxs(1),wyyi(i)*boxs(2),wzzi(i)*boxs(3),atype(i)
             endif
          endif
       endif
    enddo
    n=0

    do node=1,nprocs-1
       call mpi_barrier(mpi_comm_world, ierror)
       if (node .eq. myproc) then
          call mpi_send(myatoms, 1, my_mpi_integer, 0, node, &
               mpi_comm_world, ierror)
          if (myatoms .gt. 0) then
             call mpi_send(x0, 3*myatoms, mpi_double_precision, &
                  0, node, mpi_comm_world, ierror)
             call mpi_send(atype, myatoms, my_mpi_integer, &
                  0, node, mpi_comm_world, ierror)
             call mpi_send(x1, 3*myatoms, mpi_double_precision, &
                  0, node, mpi_comm_world, ierror)
             call mpi_send(atomindex, myatoms, my_mpi_integer, &
                  0, node, mpi_comm_world, ierror)
             if (aflag) then
                if ((AnySemi .eqv. .true. ) .and. (EAM .eqv. .false.)) then
                   call mpi_send(Ethree, myatoms, mpi_double_precision, &
                        0, node, mpi_comm_world, ierror)
                else
                   call mpi_send(P, myatoms, mpi_double_precision, &
                        0, node, mpi_comm_world, ierror)
                endif
                call mpi_send(Fp, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(Epair, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wxxi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wyyi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wzzi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             endif
          endif
       endif
       if (myproc .eq. 0) then
          call mpi_recv(n, 1, my_mpi_integer, mpi_any_source, node, &
               mpi_comm_world, mpi_status_ignore, ierror)
          if (n .gt. 0) then
             o0=3*n
             o1=6*n
             o2=7*n
             o3=8*n
             call mpi_recv(buf, 3*n, mpi_double_precision, &
                  mpi_any_source, node, mpi_comm_world, &
                  mpi_status_ignore, ierror)
             call mpi_recv(ibuf, n, my_mpi_integer, &
                  mpi_any_source, node, mpi_comm_world, &
                  mpi_status_ignore, ierror)
             call mpi_recv(buf(o0+1), 3*n, mpi_double_precision, &
                  mpi_any_source, node, mpi_comm_world, &
                  mpi_status_ignore, ierror)
             call mpi_recv(dirbuf, n, my_mpi_integer, &
                  mpi_any_source, node, mpi_comm_world, &
                  mpi_status_ignore, ierror)
             if (aflag) then
                call mpi_recv(buf(o1+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o2+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o3+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(pbuf(1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(pbuf(n+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(pbuf(2*n+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          endif
          do i=1,n
             i3=3*i-3

             if ((AnySemi .eqv. .true.) .and. (aflag .eqv. .true.) .and. (EAM .eqv. .false.)) buf(o2+i)=zero
             if (delta(ABS(ibuf(i)))==0.0) then
                print *,'dump problem',i,ibuf(i),delta(ABS(ibuf(i)))
             endif
             if ( ABS(buf(i3+1)) >= 100.0d0 .or. &
                  ABS(buf(i3+2)) >= 100.0d0 .or. &
                  ABS(buf(i3+3)) >= 100.0d0) then

                IF(iprint)WRITE(8,1001)(buf(i3+k),k=1,3),                            &
                     ibuf(i),1,1,(buf(o0+i3+k)/delta(ABS(ibuf(i))),k=1,3),  &
                     dirbuf(i)
             else
                IF(iprint)WRITE(8,1000)(buf(i3+k),k=1,3),                            &
                     ibuf(i),1,1,(buf(o0+i3+k)/delta(ABS(ibuf(i))),k=1,3),  &
                     dirbuf(i)
             endif
             if (aflag) then
                ! Use larger output field if atom is very far
                if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(x0(i3+3)*box(3)) >= 10000.0d0) then

                   IF(iprint)WRITE(9,1011)(buf(i3+k)*box(k),k=1,3)                      &
                        ,buf(o1+i),buf(o2+i),buf(o3+i),dirbuf(i)               &
                        ,pbuf(i)*boxs(1),pbuf(n+i)*boxs(2),pbuf(2*n+i)*boxs(3),ibuf(i)
                else
                   IF(iprint)WRITE(9,1010)(buf(i3+k)*box(k),k=1,3)                      &
                        ,buf(o1+i),buf(o2+i),buf(o3+i),dirbuf(i)               &
                        ,pbuf(i)*boxs(1),pbuf(n+i)*boxs(2),pbuf(2*n+i)*boxs(3),ibuf(i)
                endif

             endif
          enddo
       endif
    enddo
    if (aflag) then
        
       IF(iprint)CLOSE(9)
    endif
     
    IF(iprint)CLOSE(8)

1000 format(3(1x,f12.8),i3,2i2,3g13.5,i10)
1001 format(3(1x,f20.8),i3,2i2,3g13.5,i10)      
1010 format(5(1x,f12.6),f15.6,i10,3(1x,g10.4),1x,i2)
1011 format(5(1x,f20.6),f15.6,i10,3(1x,g12.4),1x,i2)

  end subroutine Dump_Atoms


  !******************************************************************
  subroutine Slice_Movie(x0,x1,x2,delta,atype,Ekin,box,atomindex,          &
       dslice,ECM,istep,time_fs,slicemode)

    use typeparam
    use datatypes
    use my_mpi

    use defs

    use para_common

    implicit none

    real*8 x0(*),x1(*),x2(*),Ekin(*),box(3),delta(itypelow:itypehigh)
    integer atype(*),atomindex(*),istep
    real*8 dslice(3),ECM(3),time_fs
    integer slicemode

    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     myproc = my relative processor number (0..nprocs-1)
    !     nprocs = total number of nodes being used
    !     -------------------------------------------------------------
    !          Local variables

    integer i,k,n,n3,n2,i3,i2
    integer ntot,node

    real*8 dx,dy,dz,halfb(3)

    integer itype
    real*8 tmp

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !     -------------------------------------------------------------


    !     Dump an atom region around ECM defined by dslice
    !     to slice.movie
    !
    !     The atom selection criteria is as follows:
    !     For each dimension q=1,2,3
    !        - If dslice(q)<0 accept atom in this dimension
    !        - Else if ECM(q)-dslice(q) < x_q < ECM(q)+dslice(q) (with periodics)
    !          accept atom i in this dimension
    !        - If atom is accepted in each dimension, put it in buffer, send
    !          to processor one for printing
    !

    !     Find atoms within dslice from ECM

    n=0
    halfb(1)=box(1)/2.0; halfb(2)=box(2)/2.0; halfb(3)=box(3)/2.0;
    do i=1,myatoms
       i3=i*3-3
       dx=ABS(x0(i3+1)*box(1)-ECM(1)); ! if (dx>halfb(1)) dx=box(1)-dx;
       dy=ABS(x0(i3+2)*box(2)-ECM(2)); ! if (dy>halfb(2)) dy=box(2)-dy;
       dz=ABS(x0(i3+3)*box(3)-ECM(3)); ! if (dz>halfb(3)) dz=box(3)-dz;
       if ((dslice(1)<0.0.or.dx<dslice(1)).and.                           &
            (dslice(2)<0.0.or.dy<dslice(2)).and.                          &
            (dslice(3)<0.0.or.dz<dslice(3))) then
          ! Atom accepted, put in buffer 
          ! (pbuf size is >=3*NPMAX, i.e. enough )
          n=n+1
          if (n>PARRATIO*3*NPMAX/2) print *,'Slice OVERFLOW',myproc,n
          n3=n*3-3
          n2=n*2-2
          pbuf(n3+1)=x0(i3+1)*box(1)
          pbuf(n3+2)=x0(i3+2)*box(2)
          pbuf(n3+3)=x0(i3+3)*box(3)

          ! ----- added by Juha Samela
          if ( slicemode == 2 ) then
             itype=ABS(atype(i))
             vbuf(n3+1)=x1(i3+1)*box(1)/delta(itype)*vunit(itype)
             vbuf(n3+2)=x1(i3+2)*box(2)/delta(itype)*vunit(itype)
             vbuf(n3+3)=x1(i3+3)*box(3)/delta(itype)*vunit(itype)

             tmp=delta(itype)*delta(itype)/2.0d0
             abuf(n3+1)=x2(i3+1)*box(1)/tmp*aunit(itype)
             abuf(n3+2)=x2(i3+2)*box(2)/tmp*aunit(itype)
             abuf(n3+3)=x2(i3+3)*box(3)/tmp*aunit(itype)
          end if
          ! ----- end

          ebuf(n)=Ekin(i)
          ibuf(n2+1)=atomindex(i)
          ibuf(n2+2)=atype(i)
       endif
    enddo

    ntot=n
    call my_mpi_isum(ntot, 1)
    IF(iprint)WRITE(6,'(A,I7)') ' nprinted',ntot
     

    IF(iprint)WRITE(11,'(I10)') ntot
    IF(iprint)WRITE(11,1010) istep,time_fs,                                        &
         ECM(1),ECM(2),ECM(3),dx,dy,dz
1010 format(' Sliceframe number ',i7,g11.4,' fs center',                   &
         3F9.3,' size',3F9.3)

    do node=0,nprocs-1
       if (node>0 .and. node == myproc) then  ! No send for node==0
          call mpi_send(n, 1, my_mpi_integer, 0, node, mpi_comm_world, ierror)
          if (n > 0) then
             call mpi_send(pbuf, 3*n, mpi_double_precision, &
                  0, node, mpi_comm_world, ierror)
             call mpi_send(ebuf, n, mpi_double_precision, &
                  0, node, mpi_comm_world, ierror)
             call mpi_send(ibuf, 2*n, my_mpi_integer, &
                  0, node, mpi_comm_world, ierror)
             ! ----- added by Juha Samela
             if ( slicemode == 2 ) then
                call mpi_send(vbuf, 3*n, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(abuf, 3*n, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             end if
             ! ----- end
          endif
       endif
       if (myproc == 0) then
          if (node>0) then                    ! No send for node==0
             call mpi_recv(n, 1, my_mpi_integer, mpi_any_source, node, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             if (n > 0) then
                call mpi_recv(pbuf, 3*n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ebuf, n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf, 2*n, my_mpi_integer, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                ! ----- added by Juha Samela
                if ( slicemode == 2 ) then
                   call mpi_recv(vbuf, n, mpi_double_precision, &
                        mpi_any_source, node, mpi_comm_world, &
                        mpi_status_ignore, ierror)
                   call mpi_recv(abuf, n, mpi_double_precision, &
                        mpi_any_source, node, mpi_comm_world, &
                        mpi_status_ignore, ierror)
                end if
                ! ----- end
             endif
          endif
          do i=1,n
             i3=3*i-3
             i2=2*i-2
             ! ----- added
             if ( slicemode == 2 ) then
                IF(iprint)WRITE(11,1021) element(ABS(ibuf(i2+2))),                     &
                     (pbuf(i3+k),k=1,3),                                      &
                     ibuf(i2+2),ebuf(i),ibuf(i2+1),                           &
                     (vbuf(i3+k),k=1,3),                                      &
                     (abuf(i3+k),k=1,3)
             else 
                IF(iprint)WRITE(11,1020) element(ABS(ibuf(i2+2))),                     &
                     (pbuf(i3+k),k=1,3),                                      &
                     ibuf(i2+2),ebuf(i),ibuf(i2+1)
             end if
1020         format(a2,f11.5,1x,f11.5,1x,f11.5,1x,i2,g13.5,1x,i9)
1021         format(a2,f11.5,1x,f11.5,1x,f11.5,1x,i2,g13.5,1x,i9,6(1x,f11.5))
             ! ----- end
          enddo
       endif
    enddo

     

  end subroutine Slice_Movie

  !************************************************************************
  subroutine Movie(x0,x1,xnp,xq,delta,atype,atomindex,natoms,box,istep,time, &
               P,Fp,Epair,Ethree,Ekin,wxxi,wyyi,wzzi,             &
               wxyi,wxzi,wyxi,wyzi,wzxi,wzyi,	  &
               AnySemi,moviemode,EAM,	&
               outtype,outzmin,outzmax,outzmin2,outzmax2,virsym,virkbar,virboxsiz)

    use typeparam
    use datatypes
    use my_mpi

    use defs

    use para_common

    implicit none

    real*8 x0(*),x1(*),xnp(*),xq(*), box(*),delta(itypelow:itypehigh)
    real*8 P(NPMAX,EAMBANDMAX),Fp(NPMAX,EAMBANDMAX),Epair(*),Ethree(*),Ekin(*)
    real*8 wxxi(*),wyyi(*),wzzi(*)

    real*8 wxyi(*),wxzi(*),wyxi(*),wyzi(*),wzxi(*),wzyi(*)
    logical EAM
    integer outtype
    real*8 outzmin,outzmax
    real*8 outzmin2,outzmax2
    integer virnatoms
    integer virsym
    integer virkbar
    real*8 virkbarfactor
    real*8 virboxsiz


    logical AnySemi

    integer atype(*),atomindex(*),moviemode


    !          Variables for parallel operation
!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     myproc = my relative processor number (0..nprocs-1)
    !     nprocs = total number of nodes being used
    !     buf(8*NPMAX) = buffer space for information from other nodes
    !     -------------------------------------------------------------

    integer i,k,n,i3,itype,o0,o1,i4
    integer isrc,myat2,node,istep,natoms
    real*8 time,epot

    real*8 boxs(3)

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    real*8 dh
    real*8 e 
    real*8 eV_to_kbar
    real*8 dh_omega
    real*8 u
    real*8 tenten
    real*8 vir_help1
    real*8 vir_help2


    e = 1.602176462d-19  
    eV_to_kbar = e/1d-30/1d8
    dh=box(1)*box(2)*box(3)
    dh_omega=dh*virboxsiz/natoms
    u = 1.66054873d-27 
    tenten = 1d10
    vir_help1 = eV_to_kbar/dh_omega
    vir_help2 = u*tenten/e

    virnatoms = 0

    if (virkbar == 0) then
       virkbarfactor = 1
    else
       virkbarfactor = vir_help1
    endif


    boxs(1)=box(1)*box(1)
    boxs(2)=box(2)*box(2)
    boxs(3)=box(3)*box(3)

    IF(iprint)WRITE(10,*) natoms
    IF(iprint)WRITE(10,1010) istep,time,box(1),box(2),box(3)
    
1010 format(' Frame number ',i7,g14.5,' fs boxsize',3(F11.6,1X))

    do i=1,myatoms
       i3=3*i-3
       itype=ABS(atype(i))
       if (moviemode == 0) then
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then          
             IF(iprint)WRITE(10,901)                                                   &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i)
          else
             IF(iprint)WRITE(10,900)                                                   &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i)
          endif
       else if (moviemode == 1) then
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then                     
             IF(iprint)WRITE(10,1001)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i)
          else
                  if(itype == 2) then
                          print *, "Invalid element type",itype,i,atype(2)
                          stop
                  end if
             IF(iprint)WRITE(10,1000)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i)
          endif
       else if (moviemode == 2) then
          if (delta(itype)==0.0) then
             print *,'movie problem',i,atype(i),delta(itype)
          endif
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then          
             IF(iprint)WRITE(10,1101)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  (x1(i3+k)*box(k)/delta(itype)*vunit(itype),k=1,3)
          else
             IF(iprint)WRITE(10,1100)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  (x1(i3+k)*box(k)/delta(itype)*vunit(itype),k=1,3)
          endif
       else if (moviemode == 3 .or. moviemode==4) then
          if ((AnySemi .eqv. .true. ) .and. (EAM .eqv. .false.)) then
             epot=Ethree(i)+Epair(i)
          else
             epot=Fp(i,1)+Epair(i)
          end if
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then
             if (moviemode==3) then
                IF(iprint)WRITE(10,1201) element(itype),(x0(i3+k)*box(k),k=1,3),epot
             else
                IF(iprint)WRITE(10,1211) element(itype),(x0(i3+k)*box(k),k=1,3),atype(i),epot
             endif
          else
             if (moviemode==3) then
                IF(iprint)WRITE(10,1200) element(itype),(x0(i3+k)*box(k),k=1,3),epot
             else
                IF(iprint)WRITE(10,1210) element(itype),(x0(i3+k)*box(k),k=1,3),atype(i),epot
             endif
          endif
       else if (moviemode==5) then
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then          
             IF(iprint)WRITE(10,1101)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,wyyi(i)*virkbarfactor,wzzi(i)*virkbarfactor
          else
             IF(iprint)WRITE(10,1100)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,wyyi(i)*virkbarfactor,wzzi(i)*virkbarfactor
          endif
       else if (moviemode==6) then
          if (delta(itype)==0.0) then
             print *,'movie problem',i,atype(i),delta(itype)
          endif
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then          
             IF(iprint)WRITE(10,1301)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  (x1(i3+k)*box(k)/delta(itype)*vunit(itype),k=1,3),          &
                  (wxxi(i)+wyyi(i)+wzzi(i)*virkbarfactor)
          else
             IF(iprint)WRITE(10,1300)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  (x1(i3+k)*box(k)/delta(itype)*vunit(itype),k=1,3),          &
                  (wxxi(i)+wyyi(i)+wzzi(i)*virkbarfactor)
          endif
       else if (moviemode == 7) then
          if ((AnySemi .eqv. .true. ) .and. (EAM .eqv. .false.)) then
             epot=Ethree(i)+Epair(i)
          else
             epot=Fp(i,1)+Epair(i)
          end if

          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then
             IF(iprint)WRITE(10,1221) element(itype),(x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),epot,Ekin(i)             
          else
             IF(iprint)WRITE(10,1220) element(itype),(x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),epot,Ekin(i)
          endif

       else if (moviemode==8) then
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then          
             IF(iprint)WRITE(10,91101)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  (xnp(i3+k)*box(k),k=1,3)
          else
             IF(iprint)WRITE(10,91100)                                                  &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  (xnp(i3+k)*box(k),k=1,3)
          endif

       else if (moviemode==15) then
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then
            if (virsym == 0) then
                IF(iprint)WRITE(10,11016)                                     &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyxi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    & 
                  wzyi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor,    &
                  wzxi(i)*virkbarfactor
            else
                IF(iprint)WRITE(10,11116)                                     &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,      &
                  wyyi(i)*virkbarfactor,      &
                  wzzi(i)*virkbarfactor,      &
                  wxyi(i)*virkbarfactor,      &
                  wyzi(i)*virkbarfactor,      &
                  wxzi(i)*virkbarfactor
            endif

          else
            if (virsym == 0) then
               IF(iprint)WRITE(10,11006)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    & 
                  wyxi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wzyi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor,    &
                  wzxi(i)*virkbarfactor
            else
                IF(iprint)WRITE(10,11106)                                     &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor
            endif
          endif
       ! SP ED-MD charges
       else if (moviemode==20) then
           if(nprocs > 1) then
             stop "Can't output movie with charges when >1 core is used (because the info is not available to the nodes"
           end if
           i4=i*4-4
	   if ( abs(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
		abs(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
		abs(x0(i3+3)*box(3)) >= 10000.0d0) then
               if(iprint) WRITE(10,1401) element(itype),(x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),xq(i4+1)             
	   else
               if(iprint) WRITE(10,1400) element(itype),(x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),xq(i4+1)
           endif
         
       else if (moviemode==16) then
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then                     
             IF(iprint)WRITE(10,1001)                                         &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i)
          else
             IF(iprint)WRITE(10,1000)                                         &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i)
          endif

          if (outtype == 0) then
           if(atype(i) < 0) then
            virnatoms = virnatoms + 1
            if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then
             if (virsym == 0) then     
               IF(iprint)WRITE(16,11016)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyxi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wzyi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor,    &
                  wzxi(i)*virkbarfactor
             else 
               IF(iprint)WRITE(16,11116)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor
             endif

            else
             if (virsym == 0) then  
               IF(iprint)WRITE(16,11006)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyxi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wzyi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor,    &
                  wzxi(i)*virkbarfactor
             else
               IF(iprint)WRITE(16,11106)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor
             endif


            endif
           endif
          endif

          if (outtype == 1) then
           if(x0(i3+3)*box(3) >= outzmin .AND. x0(i3+3)*box(3) <= outzmax .or. &
             (x0(i3+3)*box(3) >= outzmin2 .AND. x0(i3+3)*box(3) <= outzmax2) ) then
            virnatoms = virnatoms + 1
            if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then 
             if (virsym == 0) then    
               IF(iprint)WRITE(16,11016)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyxi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wzyi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor,    &
                  wzxi(i)*virkbarfactor
             else
               IF(iprint)WRITE(16,11116)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor
             endif
            else
             if (virsym == 0) then 
               IF(iprint)WRITE(16,11006)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    &
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyxi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wzyi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor,    &
                  wzxi(i)*virkbarfactor
              else
               IF(iprint)WRITE(16,11106)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*virkbarfactor,    & 
                  wyyi(i)*virkbarfactor,    &
                  wzzi(i)*virkbarfactor,    &
                  wxyi(i)*virkbarfactor,    &
                  wyzi(i)*virkbarfactor,    &
                  wxzi(i)*virkbarfactor
              endif

            endif
           endif
          endif

       else if (moviemode==17) then
          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then 

            if (virsym == 0) then    
             IF(iprint)WRITE(10,11016)                                        &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyxi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wzyi(i)*vir_help1, &
                  wxzi(i)*vir_help1, &
                  wzxi(i)*vir_help1
            else
             IF(iprint)WRITE(10,11116)                                        &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wxzi(i)*vir_help1         
            endif

          else
            if (virsym == 0) then  
              IF(iprint)WRITE(10,11006)                                       &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyxi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wzyi(i)*vir_help1, &
                  wxzi(i)*vir_help1, &
                  wzxi(i)*vir_help1
            else
              IF(iprint)WRITE(10,11106)                                       &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wxzi(i)*vir_help1
            endif
          endif

       else if (moviemode==18) then

          if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then                     
             IF(iprint)WRITE(10,1001)                                         &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i)
          else
             IF(iprint)WRITE(10,1000)                                         &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i)
          endif

          if (outtype == 0) then
           if (atype(i) < 0) then
            virnatoms = virnatoms + 1
            if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then
             if (virsym == 0) then     
               IF(iprint)WRITE(16,11016)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyxi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wzyi(i)*vir_help1, &
                  wxzi(i)*vir_help1, &
                  wzxi(i)*vir_help1
              else
               IF(iprint)WRITE(16,11116)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wxzi(i)*vir_help1
              endif
            else
             if (virsym == 0) then 
               IF(iprint)WRITE(16,11006)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyxi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wzyi(i)*vir_help1, &
                  wxzi(i)*vir_help1, &
                  wzxi(i)*vir_help1
              else
               IF(iprint)WRITE(16,11106)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wxzi(i)*vir_help1
              endif

            endif
           endif
          endif

          if (outtype == 1) then
           if(x0(i3+3)*box(3) >= outzmin .AND. x0(i3+3)*box(3) <= outzmax .or. &
             (x0(i3+3)*box(3) >= outzmin2 .AND. x0(i3+3)*box(3) <= outzmax2)) then
            virnatoms = virnatoms + 1
            if ( ABS(x0(i3+1)*box(1)) >= 10000.0d0 .or. &
               ABS(x0(i3+2)*box(2)) >= 10000.0d0 .or. &
               ABS(x0(i3+3)*box(3)) >= 10000.0d0) then
             if (virsym == 0) then     
               IF(iprint)WRITE(16,11016)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyxi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wzyi(i)*vir_help1, &
                  wxzi(i)*vir_help1, &
                  wzxi(i)*vir_help1
             else
               IF(iprint)WRITE(16,11116)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wxzi(i)*vir_help1
             endif
            else
             if (virsym == 0) then
               IF(iprint)WRITE(16,11006)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyxi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wzyi(i)*vir_help1, &
                  wxzi(i)*vir_help1, &
                  wzxi(i)*vir_help1
              else
               IF(iprint)WRITE(16,11106)                                      &
                  element(itype),                                             &
                  (x0(i3+k)*box(k),k=1,3),atype(i),atomindex(i),              &
                  wxxi(i)*vir_help1, &
                  wyyi(i)*vir_help1, &
                  wzzi(i)*vir_help1, &
                  wxyi(i)*vir_help1, &
                  wyzi(i)*vir_help1, &
                  wxzi(i)*vir_help1
              endif

            endif
           endif
          endif

       endif
    enddo

    n=0
    isrc=0
    myat2=0
    do node=1,nprocs-1
       call mpi_barrier(mpi_comm_world, ierror)
       if (myproc .eq. node) then
          call mpi_send(myatoms, 1, my_mpi_integer, &
               0, node, mpi_comm_world, ierror)
          if (myatoms > 0) then
             call mpi_send(x0, 3*myatoms, mpi_double_precision, &
                  0, node, mpi_comm_world, ierror)
             call mpi_send(atomindex, myatoms, my_mpi_integer, &
                  0, node, mpi_comm_world, ierror)
             call mpi_send(atype, myatoms, my_mpi_integer, &
                  0, node, mpi_comm_world, ierror)
             if (moviemode==2) then
                call mpi_send(x1, 3*myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             !Corrected from "else if (moviemode==3) then" to "else if (moviemode==3 .or. moviemode==4) then"
             else if (moviemode==3 .or. moviemode==4) then
                if ((AnySemi .eqv. .true. ) .and. (EAM .eqv. .false.)) then                
                   call mpi_send(Ethree, myatoms, mpi_double_precision, &
                        0, node, mpi_comm_world, ierror)
                else
                   call mpi_send(Fp, myatoms, mpi_double_precision, &
                        0, node, mpi_comm_world, ierror)
                end if
                call mpi_send(Epair, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             else if (moviemode==5) then
                call mpi_send(wxxi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wyyi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wzzi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             else if (moviemode==6) then
                call mpi_send(x1, 3*myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wxxi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wyyi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wzzi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             else if (moviemode==7) then
                if ((AnySemi .eqv. .true. ) .and. (EAM .eqv. .false.)) then                
                   call mpi_send(Ethree, myatoms, mpi_double_precision, &
                        0, node, mpi_comm_world, ierror)
                else
                   call mpi_send(Fp, myatoms, mpi_double_precision, &
                        0, node, mpi_comm_world, ierror)
                end if
                call mpi_send(Epair, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(Ekin, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             else if (moviemode==8) then
                call mpi_send(xnp, 3*myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             else if (moviemode==15 .or. moviemode == 16 .or. moviemode == 17 .or. moviemode == 18) then
                call mpi_send(wxxi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wyyi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wzzi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wxyi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wyxi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wyzi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wzyi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wxzi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
                call mpi_send(wzxi, myatoms, mpi_double_precision, &
                     0, node, mpi_comm_world, ierror)
             endif
          endif
       endif
       if (myproc .eq. 0) then
          call mpi_recv(n, 1, my_mpi_integer, mpi_any_source, node, &
               mpi_comm_world, mpi_status_ignore, ierror)
          if (6*n .gt. MAX(18,PARRATIO)/2*NPMAX) then
             write(6,*) 'buf overflow in movie()',n,MAX(18,PARRATIO)/2*NPMAX
             call my_mpi_abort('Movie buf', INT(n))
          endif
          o0=3*n
          o1=6*n          
          if (n .gt. 0) then
             call mpi_recv(buf, 3*n, mpi_double_precision, &
                  mpi_any_source, node, mpi_comm_world, &
                  mpi_status_ignore, ierror)
             call mpi_recv(ibuf, n, my_mpi_integer, &
                  mpi_any_source, node, mpi_comm_world, &
                  mpi_status_ignore, ierror)
             call mpi_recv(dirbuf, n, my_mpi_integer, &
                  mpi_any_source, node, mpi_comm_world, &
                  mpi_status_ignore, ierror)
             if (moviemode==2) then
         ! receive velocities
                call mpi_recv(buf(o0+1), 3*n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             ! Corrected from " else if (moviemode==3) then" to " else if (moviemode==3 .or. moviemode==4) then"
             else if (moviemode==3 .or. moviemode==4) then
                ! receive P/Fp
                call mpi_recv(buf(o0+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                ! receive Epair
                call mpi_recv(buf(o1+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             else if (moviemode==5) then
                call mpi_recv(buf(o0+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+2*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             else if (moviemode==6) then
                ! receive velocities
                call mpi_recv(buf(o0+1), 3*n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+3*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+4*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+5*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             else if (moviemode==7) then
                call mpi_recv(buf(o0+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+2*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             else if (moviemode==8) then 
                ! receive forces
                call mpi_recv(buf(o0+1), 3*n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             else if (moviemode==15 .or. moviemode == 16 .or. moviemode == 17 .or. moviemode == 18) then
                call mpi_recv(buf(o0+1), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+2*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror) 
                call mpi_recv(buf(o0+1+3*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+4*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+5*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+6*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+7*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(buf(o0+1+8*n), n, mpi_double_precision, &
                     mpi_any_source, node, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          endif
          do i=1,n
             itype=ABS(dirbuf(i))
             i3=3*i-3
             if (moviemode == 0) then
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then
                   IF(iprint)WRITE(10,901)                                           &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i)
                else
                   IF(iprint)WRITE(10,900)                                           &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i)
                endif
             else if (moviemode == 1) then
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then
                   IF(iprint)WRITE(10,1001)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i)
                else
                   IF(iprint)WRITE(10,1000)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i)
                endif
             else if (moviemode == 2) then
                if (delta(itype)==0.0) then
                   print *,'movie problem',i,ibuf(i),delta(itype)
                endif
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then           
                   IF(iprint)WRITE(10,1101)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        (buf(o0+i3+k)*box(k)/delta(itype)*vunit(itype),k=1,3)
                else
                   IF(iprint)WRITE(10,1100)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        (buf(o0+i3+k)*box(k)/delta(itype)*vunit(itype),k=1,3)
                endif
             else if (moviemode==3 .or. moviemode==4) then
                epot=buf(o0+i)+buf(o1+i)
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 100000.0d0) then
                   if (moviemode==3) then
                      IF(iprint)WRITE(10,1201) element(itype),(buf(i3+k)*box(k),k=1,3),epot
                   else
                      IF(iprint)WRITE(10,1211) element(itype),(buf(i3+k)*box(k),k=1,3),dirbuf(i),epot
                   endif
                else
                   if (moviemode==3) then
                      IF(iprint)WRITE(10,1200) element(itype),(buf(i3+k)*box(k),k=1,3),epot
                   else
                      IF(iprint)WRITE(10,1210) element(itype),(buf(i3+k)*box(k),k=1,3),dirbuf(i),epot
                   endif

                endif
             else if (moviemode == 5) then
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then           
                   IF(iprint)WRITE(10,1101)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,buf(o0+n+i)*virkbarfactor,buf(o0+2*n+i)*virkbarfactor
                else
                   IF(iprint)WRITE(10,1100)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,buf(o0+n+i)*virkbarfactor,buf(o0+2*n+i)*virkbarfactor
                endif
             else if (moviemode == 6) then
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then           
                   IF(iprint)WRITE(10,1301)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        (buf(o0+3*(i-1)+k)*box(k)/delta(itype)*vunit(itype),k=1,3), &
                        (buf(o0+3*n+i)+buf(o0+4*n+i)+buf(o0+5*n+i)*virkbarfactor)
                else
                   IF(iprint)WRITE(10,1300)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        (buf(o0+3*(i-1)+k)*box(k)/delta(itype)*vunit(itype),k=1,3), &
                        (buf(o0+3*n+i)+buf(o0+4*n+i)+buf(o0+5*n+i)*virkbarfactor)
                endif
             else if (moviemode == 7) then
                epot=buf(o0+i)+buf(o0+n+i)
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then           
                   IF(iprint)WRITE(10,1221)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),epot,buf(o0+2*n+i)
                else
                   IF(iprint)WRITE(10,1220)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),epot,buf(o0+2*n+i)
                endif

             else if (moviemode==8) then
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then           
                   IF(iprint)WRITE(10,91101)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        (buf(o0+i3+k)*box(k),k=1,3)
                else
                   IF(iprint)WRITE(10,91100)                                          &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        (buf(o0+i3+k)*box(k),k=1,3)
                endif
             else if (moviemode == 15) then
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then  

                 if (virsym == 0) then
                     IF(iprint)WRITE(10,11016)                              &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+4*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+6*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor,    &
                        buf(o0+8*n+i)*virkbarfactor
                  else
                     IF(iprint)WRITE(10,11116)                              &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor
                  endif
                else
                  if (virsym == 0) then
                    IF(iprint)WRITE(10,11006)                               &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+4*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+6*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor,    &
                        buf(o0+8*n+i)*virkbarfactor
                  else
                    IF(iprint)WRITE(10,11106)                               &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,       &
                        buf(o0+n+i)*virkbarfactor,     &
                        buf(o0+2*n+i)*virkbarfactor,   &
			buf(o0+3*n+i)*virkbarfactor,   &
                        buf(o0+5*n+i)*virkbarfactor,   &
                        buf(o0+7*n+i)*virkbarfactor
                  endif

                endif

             else if (moviemode == 16) then

                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then
                   IF(iprint)WRITE(10,1001)                                 &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i)
                else
                   IF(iprint)WRITE(10,1000)                                 &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i)
                endif

              if (outtype == 0) then
               if (dirbuf(i) < 0) then
                virnatoms = virnatoms + 1
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then
                  if (virsym == 0) then           
                   IF(iprint)WRITE(16,11016)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+4*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+6*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor,    &
                        buf(o0+8*n+i)*virkbarfactor
                  else
                    IF(iprint)WRITE(16,11116)                               &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor
                  endif
                else
                  if (virsym == 0) then
                   IF(iprint)WRITE(16,11006)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+4*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+6*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor,    &
                        buf(o0+8*n+i)*virkbarfactor
                  else
                   IF(iprint)WRITE(16,11106)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor
                  endif
 
                endif
               endif
              endif

              if (outtype == 1) then
               if(buf(i3+3)*box(3) >= outzmin .AND. buf(i3+3)*box(3) <= outzmax .or. &
                 (buf(i3+3)*box(3) >= outzmin2 .AND. buf(i3+3)*box(3) <= outzmax2)) then
                virnatoms = virnatoms + 1
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then  
                 if (virsym == 0) then         
                   IF(iprint)WRITE(16,11016)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+4*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+6*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor,    &
                        buf(o0+8*n+i)*virkbarfactor
                 else
                   IF(iprint)WRITE(16,11116)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor
                 endif
                else
                  if (virsym == 0) then
                   IF(iprint)WRITE(16,11006)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+4*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+6*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor,    &
                        buf(o0+8*n+i)*virkbarfactor
                  else
                   IF(iprint)WRITE(16,11106)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*virkbarfactor,        &
                        buf(o0+n+i)*virkbarfactor,      &
                        buf(o0+2*n+i)*virkbarfactor,    &
			buf(o0+3*n+i)*virkbarfactor,    &
                        buf(o0+5*n+i)*virkbarfactor,    &
                        buf(o0+7*n+i)*virkbarfactor 
                  endif
                endif
               endif
              endif

             else if (moviemode == 17) then
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then
                 if (virsym == 0) then           
                   IF(iprint)WRITE(10,11016)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+4*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+6*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1, &
                        buf(o0+8*n+i)*vir_help1
                  else
                   IF(iprint)WRITE(10,11116)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1
                 endif
                else
                 if (virsym == 0) then  
                   IF(iprint)WRITE(10,11006)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+4*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+6*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1, &
                        buf(o0+8*n+i)*vir_help1
                  else
                   IF(iprint)WRITE(10,11106)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1
                  endif
                endif

             else if (moviemode == 18) then
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then
                   IF(iprint)WRITE(10,1001)                                 &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i)
                else
                   IF(iprint)WRITE(10,1000)                                 &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i)
                endif

              if (outtype == 0) then
               if (dirbuf(i) < 0) then
                virnatoms = virnatoms + 1
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then     
                  if (virsym == 0) then      
                   IF(iprint)WRITE(16,11016)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+4*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+6*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1, &
                        buf(o0+8*n+i)*vir_help1
                  else
                   IF(iprint)WRITE(16,11116)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1
                  endif
                else
                  if (virsym == 0) then
                   IF(iprint)WRITE(16,11006)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+4*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+6*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1, &
                        buf(o0+8*n+i)*vir_help1
                  else
                   IF(iprint)WRITE(16,11106)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1
                  endif
                endif
               endif
              endif 
 
              if (outtype == 1) then
               if(buf(i3+3)*box(3) >= outzmin .AND. buf(i3+3)*box(3) <= outzmax .or. &
                 (buf(i3+3)*box(3) >= outzmin2 .AND. buf(i3+3)*box(3) <= outzmax2) ) then
                virnatoms = virnatoms + 1
                if ( ABS(buf(i3+1)*box(1)) >= 10000.0d0 .or. &
                     ABS(buf(i3+2)*box(2)) >= 10000.0d0 .or. &
                     ABS(buf(i3+3)*box(3)) >= 10000.0d0) then  
                 if (virsym == 0) then         
                   IF(iprint)WRITE(16,11016)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+4*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+6*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1, &
                        buf(o0+8*n+i)*vir_help1
                  else
                   IF(iprint)WRITE(16,11116)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1
                  endif
                else
                  if (virsym == 0) then
                   IF(iprint)WRITE(16,11006)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+4*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+6*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1, &
                        buf(o0+8*n+i)*vir_help1
                  else
                   IF(iprint)WRITE(16,11006)                                &
                        element(itype),                                     &
                        (buf(i3+k)*box(k),k=1,3),dirbuf(i),ibuf(i),         &
                        buf(o0+i)*vir_help1,     &
                        buf(o0+n+i)*vir_help1,   &
                        buf(o0+2*n+i)*vir_help1, &
			buf(o0+3*n+i)*vir_help1, &
                        buf(o0+5*n+i)*vir_help1, &
                        buf(o0+7*n+i)*vir_help1
                  endif

                endif
               endif
              endif 

             endif

          enddo
       endif
    enddo

if (myproc .eq. 0) then
 if (moviemode == 16 .or. moviemode == 18) then
  IF(iprint)WRITE(18,555) virnatoms
 endif
endif


900 format(a2,1x,f8.2,1x,f8.2,1x,f8.2,1x,i2,1x,i9)
901 format(a2,1x,f18.2,1x,f18.2,1x,f18.2,1x,i2,1x,i9)
1000 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,i2,1x,i9)
1001 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,i2,1x,i9)
1100 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,i2,1x,i9,1x,f11.7,1x,f11.7,1x,f11.7)

11006 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,i2,1x,i9,1x,f14.7,1x,f14.7,1x,f14.7,   &
1x,f14.7,1x,f14.7,1x,f14.7,1x,f14.7,1x,f14.7,1x,f14.7)
11106 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,i2,1x,i9,1x,f14.7,1x,f14.7,1x,f14.7,   &
1x,f14.7,1x,f14.7,1x,f14.7)

1101 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,i2,1x,i9,1x,f11.7,1x,f11.7,1x,f11.7)

11016 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,i2,1x,i9,1x,f14.7,1x,f14.7,1x,f14.7,   &
1x,f14.7,1x,f14.7,1x,f14.7,1x,f14.7,1x,f14.7,1x,f14.7)
11116 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,i2,1x,i9,1x,f14.7,1x,f14.7,1x,f14.7,   &
1x,f14.7,1x,f14.7,1x,f14.7)

555 format(i9)

1200 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,f12.6)
1201 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,f12.6)
1210 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,i2,1x,f12.6)
1211 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,i2,1x,f12.6)
1220 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,i2,1x,i9,1x,f12.6,1x,f12.6)
1221 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,i2,1x,i9,1x,f12.6,1x,f12.6)
1300 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,i2,1x,i9,1x,f11.7,1x,f11.7,1x,f11.7,1x,f11.7)
1301 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,i2,1x,i9,1x,f11.7,1x,f11.7,1x,f11.7,1x,f11.7)
1400 FORMAT(a2,1x,f8.2,1x,f8.2,1x,f8.2,1x,i2,1x,i8,1x,f10.6) ! SP ED-MD charges
1401 FORMAT(a2,1x,f18.2,1x,f18.2,1x,f18.2,1x,i2,1x,i8,1x,f10.6)
91100 format(a2,1x,f12.6,1x,f12.6,1x,f12.6,1x,i2,1x,i9,3(1x,g18.10))
91101 format(a2,1x,f22.6,1x,f22.6,1x,f22.6,1x,i2,1x,i9,3(1x,g18.10))

     

  end subroutine Movie
