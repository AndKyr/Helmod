module defs
  implicit none
  integer, parameter :: NPMAX = 1100000
  integer, parameter :: NPPASSMAX = 10000

  integer, parameter :: MAXREC = 100000

  integer, parameter :: NNMAX = 100

  integer, parameter :: NSHUFFLEMAX = 10000

  integer, parameter :: EAMTABLESZ = 100005
  integer, parameter :: EAMBANDMAX = 2 
  
  integer, parameter :: BUPRIGHT = 4
  integer, parameter :: BUPLEFT = 256
  integer, parameter :: BUP = 2
  integer, parameter :: BRIGHT = 8
  integer, parameter :: BDOWNRIGHT = 16
  integer, parameter :: BDOWNLEFT = 64
  integer, parameter :: BDOWN = 32
  integer, parameter :: BLEFT = 128

  integer, parameter :: IUP = 1
  integer, parameter :: IUPRIGHT = 2
  integer, parameter :: IRIGHT = 3
  integer, parameter :: IDOWNRIGHT = 4
  integer, parameter :: IDOWN = 5
  integer, parameter :: IDOWNLEFT = 6
  integer, parameter :: ILEFT = 7
  integer, parameter :: IUPLEFT = 8

end module
