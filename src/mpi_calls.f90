!/***********************************************************************
!  These are the MPI communication subroutines for 
!  the Classical Molecular Dynamics program
!
!  The calling subroutines are defined in defs.h
!
!  Note the various possible SEND routines. Atleast in SGI systems
!  the basic Send is very problematic. Hence use non-buffering
!  send (AL_NBSEND, MPI_Ssend) as much as possible.
!
!  On the other hand, in Cray systems the basic Send appears to be
!  faster.
!
!
!  Notes on Fortran version:
!    Vesa Kolhinen 2008-11-24
!
!  The original C file is translated in Fortran. 
!
!  Void pointers cannot be used here. Therefore, separate versions
!  of send/receive routines for real and integer arrays are included. 
!  These are named as MY_MPI_...R and MY_MPI_...I, respectively. 
!  All the source were referring to these must be modified accordingly.
!  A script create_mpi-fortran.run (and create_mpi-fortran.awk) is 
!  included for this; see comments .f90erein.
! 
!
!
!***********************************************************************/


!wrapper module for mpi library

module my_mpi
  !it is better to use "use mpi", but if unavailable then use mpif.h
    !USE mpi
  use datatypes
  implicit none
  Include 'mpif.h'

  public 
  integer, parameter :: mpi_parameters=int32b
  integer(kind=mpi_parameters) :: my_mpi_integer

  interface my_mpi_dsum
     module procedure my_mpi_dsum1, my_mpi_dsum2
  end interface

  interface my_mpi_dmax
     module procedure my_mpi_dmax1, my_mpi_dmax2
  end interface

  interface my_mpi_dmin
     module procedure my_mpi_dmin1, my_mpi_dmin2
  end interface

  interface my_mpi_isum
     module procedure my_mpi_isum1, my_mpi_isum2
  end interface

  interface my_mpi_imax
     module procedure my_mpi_imax1, my_mpi_imax2
  end interface

  interface my_mpi_imin
     module procedure my_mpi_imin1, my_mpi_imin2
  end interface

contains 
  subroutine my_mpi_init()
    implicit none
    integer:: test=123

    if (BIT_SIZE(test) == 32) then
       my_mpi_integer=MPI_INTEGER
    else if (BIT_SIZE(test) == 64) then
       my_mpi_integer=MPI_INTEGER8
    else
       call my_mpi_abort("Unknown integer size",BIT_SIZE(test))
    end if

  end subroutine my_mpi_init

  subroutine my_mpi_abort(str, tag)
    implicit none
    character(len=*), intent(in) :: str
    integer, intent(in) :: tag

    integer, parameter :: error_file=20
    integer(kind=mpi_parameters) :: ierror
    integer :: i

    write(*,*) "MY_MPI_STOP EXECUTED with tag ", tag
    write(*,*) "Message: ", str

    open(unit=error_file,file="errors.out")
    write(error_file,*) "Message: ", str
    close(error_file)

    call MPI_ABORT(MPI_COMM_WORLD, INT(tag,mpi_parameters), ierror)
  end subroutine my_mpi_abort

  subroutine my_mpi_dsum2(buf, count)
    implicit none
    double precision, dimension(*), intent(inout) :: buf
    integer, intent(in) :: count
    integer :: i
    real*8, dimension(:), allocatable :: out
    integer(kind=mpi_parameters)  :: ierror

    allocate(out(1:count), stat=ierror)

    !  if (.not. allocated(out) ) then
    if (ierror .ne. 0) then
       write(*,*) "MY_MPI_DSUM allocation error !!"
       call MPI_ABORT(MPI_COMM_WORLD,INT(count,mpi_parameters),ierror)
    endif

    call MPI_ALLREDUCE(buf, out,INT(count,mpi_parameters) , MPI_DOUBLE_PRECISION, MPI_SUM, &
         MPI_COMM_WORLD, ierror)
    forall (i=1:count) buf(i) = out(i)
    deallocate(out, stat=ierror)
  end subroutine my_mpi_dsum2

  subroutine my_mpi_dmax2(buf, count)
    implicit none

    real*8, dimension(*) :: buf
    integer :: count
    integer :: i
    real*8, dimension(:), allocatable :: out
    integer(kind=mpi_parameters)  :: ierror

    allocate(out(1:count), stat=ierror)

    !  if (.not. allocated(out) ) then
    if (ierror .ne. 0) then
       write(*,*) "MY_MPI_DMAX allocation error !!"
       call MPI_ABORT(MPI_COMM_WORLD,INT(count,mpi_parameters),ierror)
    endif

    call MPI_ALLREDUCE(buf, out, INT(count,mpi_parameters), &
         MPI_DOUBLE_PRECISION, MPI_MAX, &
         MPI_COMM_WORLD, ierror)
    forall (i=1:count) buf(i) = out(i)

    deallocate(out, stat=ierror)
  end subroutine my_mpi_dmax2

  subroutine my_mpi_dmin2(buf, count)
    implicit none

    real*8, dimension(*) :: buf
    integer :: count
    integer :: i
    real*8, dimension(:), allocatable :: out
    integer(kind=mpi_parameters)  :: ierror

    allocate(out(1:count), stat=ierror)

    !  if (.not. allocated(out) ) then
    if (ierror .ne. 0) then
       write(*,*) "MY_MPI_DMIN allocation error !!"
       call MPI_ABORT(MPI_COMM_WORLD,INT(count,mpi_parameters),ierror)
    endif

    call MPI_ALLREDUCE(buf, out,INT(count,mpi_parameters) , MPI_DOUBLE_PRECISION,&
         MPI_MIN, &
         MPI_COMM_WORLD, ierror)
    forall (i=1:count) buf(i) = out(i)

    deallocate(out, stat=ierror)
  end subroutine my_mpi_dmin2

  subroutine my_mpi_isum2(buf, count)
    implicit none

    integer, dimension(*) :: buf
    integer :: count
    integer :: i
    integer, dimension(:), allocatable :: out
    integer(kind=mpi_parameters)  :: ierror

    allocate(out(1:count), stat=ierror)

    !  if (.not. allocated(out) ) then
    if (ierror .ne. 0) then
       write(*,*) "MY_MPI_ISUM allocation error !!"
       call MPI_ABORT(MPI_COMM_WORLD,INT(count,mpi_parameters),ierror)
    endif

    call MPI_ALLREDUCE(buf, out,INT(count,mpi_parameters) , MY_MPI_INTEGER, MPI_SUM, &
         MPI_COMM_WORLD, ierror)
    forall (i=1:count) buf(i) = out(i)

    deallocate(out, stat=ierror)
  end subroutine my_mpi_isum2

  subroutine my_mpi_imax2(buf, count)
    implicit none

    integer, dimension(*) :: buf
    integer :: count
    integer :: i
    integer, dimension(:), allocatable :: out
    integer(kind=mpi_parameters)  :: ierror

    allocate(out(1:count), stat=ierror)

    !  if (.not. allocated(out) ) then
    if (ierror .ne. 0) then
       write(*,*) "MY_MPI_IMAX allocation error !!"
       call MPI_ABORT(MPI_COMM_WORLD,INT(count,mpi_parameters),ierror)
    endif

    call MPI_ALLREDUCE(buf, out,INT(count,mpi_parameters) , MY_MPI_INTEGER, MPI_MAX, &
         MPI_COMM_WORLD, ierror)
    forall (i=1:count) buf(i) = out(i)

    deallocate(out, stat=ierror)
  end subroutine my_mpi_imax2

  subroutine my_mpi_imin2(buf, count)
    implicit none

    integer, dimension(*) :: buf
    integer :: count
    integer :: i
    integer, dimension(:), allocatable :: out
    integer(kind=mpi_parameters)  :: ierror

    allocate(out(1:count), stat=ierror)

    !  if (.not. allocated(out) ) then
    if (ierror .ne. 0) then
       write(*,*) "MY_MPI_IMIN allocation error !!"
       call MPI_ABORT(MPI_COMM_WORLD,INT(count,mpi_parameters),ierror)
    endif

    call MPI_ALLREDUCE(buf, out, INT(count,mpi_parameters), MY_MPI_INTEGER, MPI_MIN, &
         MPI_COMM_WORLD, ierror)
    forall (i=1:count) buf(i) = out(i)

    deallocate(out, stat=ierror)
  end subroutine my_mpi_imin2

  !
  ! Scalar wrappers
  !

  subroutine my_mpi_dsum1(s, count)
    implicit none
    double precision, intent(inout) :: s
    integer, intent(in) :: count
    double precision, dimension(1) :: buf

    if (count /= 1) then
       call my_mpi_abort('my_mpi_dsum count /= 1 with a scalar', 0)
    end if

    buf(1) = s
    call my_mpi_dsum2(buf, count)
    s = buf(1)
  end subroutine my_mpi_dsum1

  subroutine my_mpi_dmax1(s, count)
    implicit none
    double precision, intent(inout) :: s
    integer, intent(in) :: count
    double precision, dimension(1) :: buf

    if (count /= 1) then
       call my_mpi_abort('my_mpi_dmax count /= 1 with a scalar', 0)
    end if

    buf(1) = s
    call my_mpi_dmax2(buf, count)
    s = buf(1)
  end subroutine my_mpi_dmax1

  subroutine my_mpi_dmin1(s, count)
    implicit none
    double precision, intent(inout) :: s
    integer, intent(in) :: count
    double precision, dimension(1) :: buf

    if (count /= 1) then
       call my_mpi_abort('my_mpi_dmin count /= 1 with a scalar', 0)
    end if

    buf(1) = s
    call my_mpi_dmin2(buf, count)
    s = buf(1)
  end subroutine my_mpi_dmin1

  subroutine my_mpi_isum1(s, count)
    implicit none
    integer, intent(inout) :: s
    integer, intent(in) :: count
    integer, dimension(1) :: buf

    if (count /= 1) then
       call my_mpi_abort('my_mpi_dsum count /= 1 with a scalar', 0)
    end if

    buf(1) = s
    call my_mpi_isum2(buf, count)
    s = buf(1)
  end subroutine my_mpi_isum1

  subroutine my_mpi_imax1(s, count)
    implicit none
    integer, intent(inout) :: s
    integer, intent(in) :: count
    integer, dimension(1) :: buf

    if (count /= 1) then
       call my_mpi_abort('my_mpi_imax count /= 1 with a scalar', 0)
    end if

    buf(1) = s
    call my_mpi_imax2(buf, count)
    s = buf(1)
  end subroutine my_mpi_imax1

  subroutine my_mpi_imin1(s, count)
    implicit none
    integer, intent(inout) :: s
    integer, intent(in) :: count
    integer, dimension(1) :: buf

    if (count /= 1) then
       call my_mpi_abort('my_mpi_imiv count /= 1 with a scalar', 0)
    end if

    buf(1) = s
    call my_mpi_imin2(buf, count)
    s = buf(1)
  end subroutine my_mpi_imin1

end module my_mpi
