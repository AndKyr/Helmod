  
  subroutine splinereppot(r,V,dV,bf,rf,ti_in,tj_in,fermi,dfermi)

    use typeparam
    use my_mpi

  use defs  
    implicit none

    real*8 r; 
    real*8 V,dV;
    real*8 bf,rf;
    real*8 fermi,dfermi
    integer ti_in,tj_in; 


    ! Spline repulsive potential subroutine, taken from ~/md/potential/tersoff.c
    !
    ! - first time each pair is requested, read in potential and derivatives 
    !   from reppot.name.namej.in
    !
    ! - Assumes symmetric interactions, always uses ti < tj
    !
    ! - Evaluates reppot and fermi fits it to the input attractive potential,
    ! - Except if bf==1d20 => returns only reppot
    !
    ! - always: return V and dV
    !
    ! - interpolation of V and dV done with cubic splines. See Numerical
    !   Recipes, ch. 3.3
    !
    ! - As of parcas V3.76, hcparcas V3.24, dV obtained from spline derivative
    !   which mdrange and hcparcas tests showed is clearly better.
    !
    ! - Screening version: input scaled by screenfact/r for spline interpolation,
    !   result scaled back again. Thus the interpolated factor is really the
    !   screening function, which is less strongly repulsive and thus better
    !   suited for spline interpolation.
    !
    !   screenfact in here is simply 14.399758 (i.e. Z1 and Z2 are missing)
    !
    !   The screening factor calc. was tested for Kr->Pt up to an energy
    !   of 10 MeV an seemed to work perfectly - no nasty spline oscillations.

    real*8 V0,dV0;    ! Input attractive potential 
    real*8 V1,dV1;    ! Repulsive potential 

    integer ti,tj

    real*8 expterm;

    real*8, allocatable, save :: x(:,:,:)   ! x array  
    real*8, allocatable, save :: VR(:,:,:)  ! Repulsive potential  

    ! Estimation of second derivative of VR  for spline int. 
    real*8, allocatable, save ::  VR2(:,:,:)

    ! Spline interpolation temporary arrays and help variables 
    real*8, allocatable, save ::  u(:)       ! Spline help array 

    integer, allocatable, save :: Nmax(:,:)
    integer, save :: klo,khi;  

    integer ::  N  ! Number of readin potential points

    real*8 :: screenfact=14.399758;
    real*8 screenhelp

    real*8 sig,p,qn,un;
    integer k;

    real*8 h,a,b,rlo,rhi;

    ! Other variables 

    logical, save :: firsttime=.true.;
    logical, allocatable, save :: firstpair(:,:);

    character*120 buf;
    integer i,j,l;
    real*8 rin,Vin,rinprev;

    real*8 r0,deltar;

    real*8 temp,atemp,btemp,htemp,dVspline,dVlinear;

    integer, save :: reppotmax=10000;

    ! Use symmetric interactions to reduce need of files

    ti=ti_in; tj=tj_in;
    if (ti > tj) then
       tj=ti_in; ti=tj_in;
    endif

    ! Array indexing starts from 0, not 1 !

    V0=V; dV0=dV;

    ! On firsttime for each pair read in potential data 
    if (firsttime) then
       firsttime=.false.
       klo=0; khi=reppotmax
       ! write (6,*) 'Allocating reppot arrays',reppotmax,itypelow,itypehigh
       allocate(x(0:reppotmax,itypelow:itypehigh,itypelow:itypehigh))
       allocate(VR(0:reppotmax,itypelow:itypehigh,itypelow:itypehigh))

       allocate(VR2(0:reppotmax,itypelow:itypehigh,itypelow:itypehigh))
       allocate(u(0:reppotmax))

       allocate(Nmax(itypelow:itypehigh,itypelow:itypehigh))
       allocate(firstpair(itypelow:itypehigh,itypelow:itypehigh))
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
             firstpair(i,j)=.true.
          enddo
       enddo

    endif

    if (firstpair(ti,tj)) then

       firstpair(ti,tj)=.false.

       write(unit=buf,fmt='(A10,A,A1,A,A3)') 'in/reppot.',  &
            trim(element(ti)),'.',trim(element(tj)),'.in'

       open(4,file=buf,status='old',err=10);
       goto 30
10     if (ti==1 .and. tj==1) then
          write(unit=buf,fmt='(A12)') 'in/reppot.in'
          open(4,file=buf,status='old',err=20);
          goto 30
       endif

20     write (6,*) 'splinereppot ERROR: No reppot for atom pair',ti,tj
       call my_mpi_abort('no reppot', int(ti))

30     print *,'Reading in pair pot for atom pair i j, r',ti,tj,r
       print *,'from file',buf

       i=0; l=0;
       rinprev=0.0;
       do
          read (4,fmt='(A)',end=50) buf

          if (buf(1:1)=='%'.or.buf(1:1)=='#'.or.buf(1:1)=='!'.or.   &
               len_trim(buf)==0) cycle

          l=l+1
          read(unit=buf,fmt=*) rin,Vin

          i=i+1;
          if (i>=reppotmax) then
             print  *,'reppot ERROR: too many reppot points',ti,tj
             call my_mpi_abort('reppotmax', int(reppotmax))
          endif
          x(i,ti,tj)=rin;
          VR(i,ti,tj)=Vin/screenfact*rin;
       enddo

50     N=i; Nmax(ti,tj)=i;

       print *,'reppot() Read in repulsive potential points',N

       close(4)

       do j=N+1,reppotmax
          x(j,ti,tj)=x(N,ti,tj)+1.0d0*(j-N)*(x(N,ti,tj)-x(N-1,ti,tj));
          VR(j,ti,tj)=0.0d0;

          VR2(j,ti,tj)=0.0d0;
          u(j)=0.0d0;
       enddo

       if (x(1,ti,tj) < 1d-20) then
          print *,'splinereppot error: first x point in file should not be zero',x(1,ti,tj)     
          call my_mpi_abort('splinereppot 0', int(0))
       endif

       ! Obtain 0 point estimate by linear extrapolation
       ! Since screening factors are used, this should be pretty good

       ! First estimate derivative from first 2 points
       dVlinear=(VR(2,ti,tj)-VR(1,ti,tj))/(x(2,ti,tj)-x(1,ti,tj));

       x(0,ti,tj)=0.0d0;
       VR(0,ti,tj)=VR(1,ti,tj)-dVlinear*(x(2,ti,tj)-x(1,ti,tj));

       !
       ! Initialize spline stuff for spline interpolation of VR 
       !
       ! Input needed: vector VR, size REPPOTMAX
       ! Natural spline boundary conditions are used (mdrange tests
       ! showed this is clearly the best) !
       ! Note that array bounds are 0 and N,
       ! not 1 and N ! 
       !
       ! Output: VR2, estimate of second derivative of VR (corresponds
       ! to Y2 in Numerical Recipes)
       !

       VR2(0,ti,tj)=0.0d0;
       u(0)=0.0d0;

       do i=1,N
          sig=(x(i,ti,tj)-x(i-1,ti,tj))/(x(i+1,ti,tj)-x(i-1,ti,tj));
          p=sig*VR2(i-1,ti,tj)+2.0;
          VR2(i,ti,tj)=(sig-1.0d0)/p;
          u(i)=(6.0*((VR(i+1,ti,tj)-VR(i,ti,tj))/(x(i+1,ti,tj)-x(i,ti,tj)) - &
               (VR(i,ti,tj)-VR(i-1,ti,tj)) &
               /(x(i,ti,tj)-x(i-1,ti,tj)))/ &
               (x(i+1,ti,tj)-x(i-1,ti,tj))-sig*u(i-1))/p
       enddo

       qn=0.0d0;
       un=0.0d0;

       VR2(N,ti,tj)=(un-qn*u(N-1))/(qn*VR2(N-1,ti,tj)+1.0d0);
       do k=N-1,0,-1
          VR2(k,ti,tj)=VR2(k,ti,tj)*VR2(k+1,ti,tj)+u(k);
          ! print *,k,x(k,ti,tj),VR2(k,ti,tj),u(k)
       enddo

       print *,'Spline initialization done for atom pair',ti,tj
       print *,'Estimate of Vrepulsive screen at r=0',VR(0,ti,tj)

    endif! (firstpair(ti,tj)

    if (r>=x(Nmax(ti,tj),ti,tj)) then
       V=0.0d0;
       dV=0.0d0;
       return;  ! No reppot 
    endif

    V1=0.0; 
    dV1=0.0; 

    if (r>=x(Nmax(ti,tj),ti,tj)) return;  ! No reppot 

    ! Obtain array limits klo and khi 

    if (.not. ((khi-klo==1).and.x(khi,ti,tj)>r.and.x(klo,ti,tj)<r)) then
       ! bisect your way to the correct point 
       klo=0; 
       khi=Nmax(ti,tj);
       do
          if (khi-klo <= 1) exit
          k=(khi+klo)/2;
          if (x(k,ti,tj) > r) then 
             khi=k;
          else 
             klo=k;
          endif
       enddo
    endif

    if (khi > Nmax(ti,tj)) then
       print *,'Warning: reppot outside array',khi,Nmax(ti,tj),r
    endif
    rlo=x(klo,ti,tj);
    rhi=x(khi,ti,tj);

    ! Spline interpolate V 

    h=rhi-rlo;
    temp=1/h;
    a=(rhi-r)*temp;
    b=(r-rlo)*temp;

    atemp=a*a;
    btemp=b*b;
    htemp=h*h/6.0;

    V1=a*VR(klo,ti,tj)+b*VR(khi,ti,tj)+ &
         ((atemp*a-a)*VR2(klo,ti,tj)+ &
         (btemp*b-b)*VR2(khi,ti,tj))*htemp;

    ! Compute spline derivative, eq. 3.3.5 in Numerical recipes 

    dVspline=((VR(khi,ti,tj)-VR(klo,ti,tj))-   &
         ((3*atemp-1)*VR2(klo,ti,tj)-(3*btemp-1)*VR2(khi,ti,tj))*htemp)*temp;

    !print *,khi,VR(khi,ti,tj),VR(klo,ti,tj),3*atemp-1,VR2(klo,ti,tj),3*btemp-1,VR2(khi,ti,tj),htemp,h

    ! Correct for screening

    screenhelp=screenfact/r;
    dV1=screenhelp*(r*dVspline - V1)/r;
    V1 = V1 * screenhelp;

    !dV1=dVspline   ! For no screening
    ! print *,r,V1,dV1
    ! STOP

    if (bf /= 1d20) then

       if (bf <= 0.0d0 .or. rf <= 0.0d0) then
          print *,'splinereppot: attempt to use crazy Fermi params',bf,rf
          print *,'for atom types',ti,tj
          call my_mpi_abort('Invalid Fermi params', int(ti))
       endif

       expterm=exp(-bf*(r-rf));
       fermi=1.0d0/(1.0d0+expterm);
       dfermi=fermi*fermi*expterm*bf;

       V=(1.0d0-fermi)*V1+fermi*V0;
       !print *,'debug reppot',r,(1.0d0-fermi)*V1,+fermi*V0

       dV=(1.0d0-fermi)*dV1-dfermi*V1+fermi*dV0+dfermi*V0;
       !print *,'debug reppot',r,(1.0d0-fermi)*dV1,-dfermi*V1,+fermi*dV0,+dfermi*V0

    else 
       V=V1;
       dV=dV1;
    endif

    !print '(A,9F8.2)','Vr',r,V,dV,V0,dV0,V1,dV1,fermi,dfermi

  end subroutine SPLINEREPPOT


