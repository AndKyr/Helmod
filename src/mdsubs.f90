!***********************************************************************
! Support subroutines for all versions of the Classical MD code.
!     Temp_Control(), Predict(), Correct(), 
!
! Pair_Table() moved to mdlinkedlist.f
! FCC_Gen(), Read_Atoms moved to mdlattice.f
! Dump_Atoms and Movie() moved to mdoutput.f

!***********************************************************************
! Temperature control
!***********************************************************************

subroutine Temp_Control(heat, mtemp,temp,toll,trans,poten,tote,       &
     heatbrdr,transbrdr,temp0,trate,deltat_fs,ntimeini,timeini,       &
     btctau,istep,time_fs,time_thisrec,rsnum)
  use  Temp_Time_Prog
  use defs  

  use para_common

  implicit none

  !      IMPLICIT REAL*8  (a-h,o-z)
  !      IMPLICIT INTEGER (i-n)
  !
  !  KN Main input is values trans and transbrdr, which are used to calculate
  !  the temperature or border temperature. Note that the routine may be called
  !  with transmoving instead of trans !      
  !      
  ! KN Main output is parameter heat, which determines how much temperature
  ! should be changed. It scales directly the velocities (hence the sqrt)
  ! 
  ! Modes of temperature control determined by mtemp
  !
  !     0     No temperature control (output heat=1) 
  !     1     Scale linearly towards desired value according to temp and toll *
  !     2     Quench - Set all velocities to zero => motion by a, a'... only
  !     3     Energy control
  !     4     As 1, but only for first 20 steps 
  !     5     As 1, but only for border atoms *
  !     6     Cool/heat from temp0 to temp at rate trate (K/fs) * �
  !     7     As 5, but not if not periodic ! *
  !     8     Border+quench; as 7 initially, then quench for all atoms �
  !     9     As 6 but cooling done by scaling down velocities of atoms 
  !           that have v�F<0 � (A.Kuronen, April 2001)      
  !
  !  * Possible btctau influence, see below:
  !
  !  � Quench modes: Initially normal scale for ntimeini steps or the time
  !    timeini, whichever is less. After that slow quench of all atoms at
  !    quench rate trate (K/fs).
  ! 
  !  If btctau is non-zero, velocity scaling factor is 
  !  sqrt(1+deltat_fs/btctau*(T0/T-1) rather than the ordinary T0/T.
  !  according to the Berendsen model,  J. Chem. Phys. 81 (1984) 3684
  !  This is true for mode 1 and 6 only prior to quench
  !


  !     ------------------------------------------------------------------
  !          Variables passed in and out
  integer mtemp
  real*8 heat, temp,toll,trans,poten,tote

  real*8 heatbrdr,transbrdr

  real*8 temp0,trate,deltat_fs,btctau,timeini,time_fs
  real*8 time_thisrec,time
  integer ntimeini,istep,rsnum
  !     ------------------------------------------------------------------
  !          Local variables and constants
  real*8 tempn, ratio1, tempnew, dtemp, tempmi, temppl, transn
  save poten_old,tempnew
!include 'para_common.f90'

  real*8 maxratio,minratio,poten_old

  integer ntime

  !     ------------------------------------------------------------------

  ! Linear temperature control

  maxratio = 1.2d0
  minratio = 0.9d0
  poten_old=zero
  heat=one
  heatbrdr=one
  ratio1=one

  ntime=istep

  time=time_fs
  !     If rsnum>0, use time_thisrec instead of time_fs for mtemp=6/8
  if (rsnum>0) then
     time=time_thisrec
  endif

  if (mtemp .eq. 0) return

  if (mtemp .eq. 1 .or. (ntime < 20 .and. mtemp .eq. 4)) then
     tempn=two*trans/three
     ratio1=one
     if (tempn /= zero) then
        if (btctau .eq. 0.0) then
           ratio1=SQRT(temp/tempn)
        else
           ratio1=SQRT(1.0d0+deltat_fs/btctau*(temp/tempn-1.0d0))
        endif
     endif

     tempmi=temp-toll
     temppl=temp+toll
     if (ratio1 .gt. maxratio) ratio1=maxratio
     if (tempn < tempmi .or. tempn .gt. temppl) then
        heat=heat*ratio1
        !         IF(iprint)WRITE(7,*)tempn,myproc,heat
     endif
  endif

  ! "cold" relaxation - if energy goes up, set all velocities to zero

  if (mtemp .eq. 2)  then
     tempn=two*trans/three
     if ( (poten - poten_old .gt. toll) .or. tempn > temp) then
        heat = zero
        IF(iprint)WRITE(7,1010) poten,poten_old
        poten_old=poten
     endif
  endif
  if (poten < poten_old) poten_old=poten
1010 format(' poten=',f10.5,'  old Vpot = ',f10.5,'  Vel set to 0')

  ! Energy control

  if (mtemp .eq. 3) then
     ratio1=one
     transn=temp-poten
     if (trans .gt. zero .and. transn .gt. zero) then
        ratio1=SQRT(transn/trans)
     endif
     tempmi=temp-toll
     temppl=temp+toll
     if (tote < tempmi .or. tote .gt. temppl) then
        heat=heat*ratio1
        !         IF(iprint)WRITE(7,*) tote,heat,transn,trans
     endif
  endif

  ! Surface temperature control

  if (mtemp .eq. 5 .or. mtemp .eq. 7) then
     tempn=two*transbrdr/three
     ratio1=one
     if (tempn .ne. zero) then
        if (btctau .eq. 0.0) then
           ratio1=SQRT(temp/tempn)
        else
           ratio1=SQRT(1.0d0+deltat_fs/btctau*(temp/tempn-1.0d0))
        endif
     endif
     if (ratio1 .gt. maxratio) ratio1=maxratio
     if (ratio1 < minratio) ratio1=minratio
     heatbrdr=heatbrdr*ratio1
     !print *,'tdebug',trans,transbrdr,tempn,temp,heatbrdr
  endif

  !  Complex control: first set to temp0, then transform to temp at rate trate
  !  or quench using the v�F<0 method (mtemp==9, A.Kuronen, April 2001)

  if (mtemp .eq. 6 .or. mtemp .eq. 9) then
     tempn=two*trans/three
     if (ntime .eq. 1 .and. tempn > 0.0 ) tempnew = tempn
     if (ntime < ntimeini .and. time < timeini) then
        ratio1=one
        if (tempn .ne. zero) then
           if (btctau .eq. 0.0) then
              ratio1=SQRT(temp0/tempn)
           else
              ratio1=SQRT(1.0d0+deltat_fs/btctau*(temp0/tempn-1.0d0))
           endif
        endif
        tempnew=temp0
     else
        if (mtemp==6) then
           if (TT_activated .and. .not. TT_tempnewset) then
              tempnew=TT_temp(TT_step-1)
              TT_tempnewset=.true.
           end if
           dtemp=trate*deltat_fs
           if (tempn > temp+dtemp) then
              tempnew=tempnew-dtemp
           else if (tempn < temp-dtemp) then
              tempnew=tempnew+dtemp
           else
              tempnew=temp
           endif
           if (tempnew < 0.0) tempnew=0.0
           if (tempn .ne. zero) ratio1=sqrt(tempnew/tempn)
        else
           ratio1=1.0d0
        end if
     endif
     if (ratio1 .gt. maxratio) ratio1=maxratio
     heat=heat*ratio1
     !         print *,tempn,temp0,temp,tempnew,dtemp,trate,deltat_fs
  endif

  !     Complex border control: first control borders, then quench all
  !     at rate trate

  if (mtemp .eq. 8) then
     tempn=two*trans/three
     if (ntime < ntimeini .and. time < timeini) then
        tempn=two*transbrdr/three
        ratio1=one
        if (tempn .ne. zero) then
           if (btctau .eq. 0.0) then
              ratio1=SQRT(temp0/tempn)
           else
              ratio1=SQRT(1.0d0+deltat_fs/btctau*(temp0/tempn-1.0d0))
           endif
        endif
        if (ratio1 .gt. maxratio) ratio1=maxratio
        if (ratio1 < minratio) ratio1=minratio
        heatbrdr=heatbrdr*ratio1
        tempnew=two*trans/three
     else
        dtemp=trate*deltat_fs
        if (tempn > temp+dtemp) then
           tempnew=tempnew-dtemp
        else if (tempn < temp-dtemp) then
           tempnew=tempnew+dtemp
        else
           tempnew=temp
        endif
        if (tempnew < 0.0) tempnew=0.0
        if (tempn .ne. zero) ratio1=sqrt(tempnew/tempn)
        if (ratio1 .gt. maxratio) ratio1=maxratio
        heat=heat*ratio1
     endif
  endif

! mtemp 1 and then mtemp 5
  if (mtemp .eq. 10) then
     ! mtemp 1
     if (ntime < ntimeini .and. time < timeini) then
        tempn=two*trans/three
        ratio1=one
        if (tempn /= zero) then
           if (btctau .eq. 0.0) then
              ratio1=SQRT(temp/tempn)
           else
              ratio1=SQRT(1.0d0+deltat_fs/btctau*(temp/tempn-1.0d0))
           endif
        endif

        tempmi=temp-toll
        temppl=temp+toll
        if (ratio1 .gt. maxratio) ratio1=maxratio
        if (tempn < tempmi .or. tempn .gt. temppl) then
           heat=heat*ratio1
           !         IF(iprint)WRITE(7,*)tempn,myproc,heat
        endif
     ! mtemp 5
     else
        tempn=two*transbrdr/three
        ratio1=one
        if (tempn .ne. zero) then
           if (btctau .eq. 0.0) then
              ratio1=SQRT(temp/tempn)
           else
              ratio1=SQRT(1.0d0+deltat_fs/btctau*(temp/tempn-1.0d0))
           endif
        endif
        if (ratio1 .gt. maxratio) ratio1=maxratio
        if (ratio1 < minratio) ratio1=minratio
        heatbrdr=heatbrdr*ratio1
     endif
  endif

  IF(debug)PRINT *,'Temp',tempn,ratio1


end subroutine Temp_Control

! ************************************************************
! Berendsen pressure control
! Moved from mdx.f90 into this subroutine by Paul Ehrhart
!     
! ***********************************************************

subroutine Press_Control(bpcbeta,bpctau,bpcmode,tmodtau,temp,  &
     bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,wxx,wyy,wzz,Pxx,Pyy,Pzz,istep,deltat_fs,time_fs,  &
     transv,box,boxs,box0,ncells,unitcell,dh,dh0,dhprev,x1,x2,x3,x4,x5)

  use PhysConsts
  use my_mpi

  use defs  

  use para_common

  implicit none

  ! *** variables passed in and out ***

  ! parameters of BPC
  real*8,intent(in)    :: bpcbeta,bpctau,tmodtau
  integer,intent(in)   :: bpcmode
  ! desired and current pressures, desired temperature
  real*8,intent(in)    :: bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,wxx,wyy,wzz
  real*8,intent(out)   :: Pxx,Pyy,Pzz
  real*8,intent(inout) :: temp
  ! time step information
  real*8,intent(in)    :: deltat_fs,time_fs
  real*8,intent(in)    :: transv(*)
  integer,intent(in)   :: istep
  ! system size
  integer,intent(in)   :: ncells(3)
  real*8,intent(inout) :: box(3),dh,dh0
  real*8,intent(inout) :: boxs(3),box0(3)
  real*8,intent(out)   :: unitcell(3),dhprev
  ! derivatives of the positions
  real*8,intent(inout) :: x1(*),x2(*),x3(*),x4(*),x5(*)
  real*8 :: boxprev(3)


  ! *** variables for parallel operation ***

!include 'para_common.f90'

  ! *** local variables ***

  real*8  :: help(3),help1
  integer :: i,j,i3

  ! Because of the fact that x0 is already
  ! scaled by the box size, no change of the internal coordinates is 
  ! necessary
  !
  ! Note that the virial wxx is Sigma fijx�rijx,
  ! with no factor 0.5 in front !

  if (bpcbeta /= 0.0d0) then

     help(1)=wxx
     help(2)=wyy
     help(3)=wzz
     call my_mpi_dsum(help, 3)

     Pxx=eV_to_kbar*(boxs(1)*help(1)+transv(1))/dh
     Pyy=eV_to_kbar*(boxs(2)*help(2)+transv(2))/dh
     Pzz=eV_to_kbar*(boxs(3)*help(3)+transv(3))/dh

     ! Don't change size during first 10 steps
     if (bpctau /= 0.0  .and. bpcmode/=0 .and. istep>10) then
        if (bpcmode==1) then        
           help(1)=1.0d0-bpcbeta*deltat_fs/three/bpctau*(bpcP0x-Pxx)
           help(2)=1.0d0-bpcbeta*deltat_fs/three/bpctau*(bpcP0y-Pyy)
           help(3)=1.0d0-bpcbeta*deltat_fs/three/bpctau*(bpcP0z-Pzz)

        else if (bpcmode==2) then
           help(1)=1.0d0-bpcbeta*deltat_fs/three/bpctau*   &
                (bpcP0-(Pxx+Pyy+Pzz)/three)
           help(2)=help(1)
           help(3)=help(1)
        else if (bpcmode==3) then
           help(1)=1.0d0-bpcbeta*deltat_fs/three/bpctau*(bpcP0x-Pxx)
           help(2)=1.0d0-bpcbeta*deltat_fs/three/bpctau*(bpcP0y-Pyy)
           help(3)=1.0d0;
        else if (bpcmode==4) then
           help(1)=1.0d0;
           help(2)=1.0d0;
           help(3)=1.0d0-bpcbeta*deltat_fs/three/bpctau*(bpcP0z-Pzz);
        else if (bpcmode==5) then
           help(1)=1.0d0-bpcbeta*deltat_fs/three/bpctau*(bpcP0x-Pxx)
           help(2)=1.0d0;
           help(3)=1.0d0;
        else
           call my_mpi_abort('Unknown bpcmode', int(bpcmode))
        endif

        IF(debug)PRINT *,'pres.',(help(i),i=1,3)
        box(1)=box(1)*help(1)
        box(2)=box(2)*help(2)
        box(3)=box(3)*help(3)

        dhprev=dh
        dh=box(1)*box(2)*box(3)
        boxs(1)=box(1)*box(1)
        boxs(2)=box(2)*box(2)
        boxs(3)=box(3)*box(3)

        unitcell(1)=box(1)/ncells(1)
        unitcell(2)=box(2)/ncells(2)
        unitcell(3)=box(3)/ncells(3)
        !
        !  Since the x1-5 vectors contain the box size,
        !  they must all be corrected  ! 
        !
        do i=1,myatoms
           i3=i*3-3
           do j=1,3
              ! if (atype(i)<0) x0(i3+j)=x0(i3+j)/help(j)
              x1(i3+j)=x1(i3+j)/help(j)
              x2(i3+j)=x2(i3+j)/help(j)
              x3(i3+j)=x3(i3+j)/help(j)
              x4(i3+j)=x4(i3+j)/help(j)
              x5(i3+j)=x5(i3+j)/help(j)
           enddo
        enddo

        if (tmodtau/=0.0.and.time_fs>10.0*ABS(tmodtau)) then
           help1=(1.0d0+(dhprev-dh)/dh0*deltat_fs*1000.0/tmodtau)
           temp=temp*help1
        endif

     endif
  endif

  if (bpcextz /= 0.0d0) then

     boxprev(1)=box(1)
     boxprev(2)=box(2)
     boxprev(3)=box(3)

     box(3)=box0(3)*(1.0d0+bpcextz*time_fs)

     boxs(1)=box(1)*box(1)
     boxs(2)=box(2)*box(2)
     boxs(3)=box(3)*box(3)

     unitcell(1)=box(1)/ncells(1)
     unitcell(2)=box(2)/ncells(2)
     unitcell(3)=box(3)/ncells(3)
     !
     !  Since the x1-5 vectors contain the box size,
     !  they must all be corrected  ! 
     !
     help(1)=box(1)/boxprev(1)
     help(2)=box(2)/boxprev(2)
     help(3)=box(3)/boxprev(3)
     do i=1,myatoms
        i3=i*3-3
        do j=1,3
    ! if (atype(i)<0) x0(i3+j)=x0(i3+j)/help(j)
           x1(i3+j)=x1(i3+j)/help(j)
           x2(i3+j)=x2(i3+j)/help(j)
           x3(i3+j)=x3(i3+j)/help(j)
           x4(i3+j)=x4(i3+j)/help(j)
           x5(i3+j)=x5(i3+j)/help(j)
        enddo
     enddo

     if (tmodtau/=0.0.and.time_fs>10.0*ABS(tmodtau)) then
        help1=(1.0d0+(dhprev-dh)/dh0*deltat_fs*1000.0/tmodtau)
        temp=temp*help1
     endif

  endif

end subroutine Press_Control



!***********************************************************************
! update positions, calculate kinetic energy
!
! Returns among other things:
!     Ekin()       Kinetic energy of each atom i (eV)
!     trans        Kinetic energy of all atoms (eV)
!     transv       Kinetic energy xyz componenets (eV)
!     transbrdr    Kinetic energy of border atoms (eV)
!     nbrdr        Number of border atoms
!     Emax         Maximum kinetic energy of all atoms (eV)
!     vmax         Maximum velocity of all atoms (�/fs)
!     Emaxnosput   Maximum kinetic energy of atoms within original cell (eV)
!     vmaxnosput   Maximum velocity of atoms within original cell (�/fs)
!
!***********************************************************************

subroutine Predict(x0,x1,x2,x3,x4,x5,atype,                           &
     trans,transin,natomsin,transv,heat,box,pbc,delta,                &
     Emax,Emaxnosput,vmax,vmaxnosput,heatbrdr,transbrdr,              &
     tscaleth,tscalzmin,tscalzmax,tscalxout,tscalyout,                &
     x0nei,xneimax,xneimax2,                                          &
     Ekin,nbrdr,mtemp,timesputlim,nisputlim,Elosssum,                 &
     Emaxbrdr,Fzmaxz,FzmaxYz,dydtmaxz,tscalsputlim,fdamp,P,Fp,Epair,AnySemi,   &
     Tcontrolt,EAM) 


  use typeparam
  use my_mpi

  use defs  

  use para_common
  use casc_common

  implicit none

  !     Variables passed in and out
  real*8 x0(*), x1(*), x2(*), x3(*), x4(*), x5(*)
  integer atype(*)
  real*8 box(3), pbc(3)
  real*8 trans,transin, transv(3), heat, delta(itypelow:itypehigh)
  integer natomsin

  real*8 Emax,vmax,heatbrdr,transbrdr,tscaleth
  real*8 tscalzmin,tscalzmax,tscalxout,tscalyout,timesputlim,nisputlim
  real*8 Emaxnosput,vmaxnosput,Ekin(*),Elosssum(*),Emaxbrdr,Fzmaxz,FzmaxYz,dydtmaxz
  real*8 tscalsputlim,fdamp      
  integer nbrdr,mtemp,Tcontrolt
  real*8 P(*),Fp(*),Epair(*)
  logical AnySemi

  logical EAM

  !     Variables for parallel operation
!include 'para_common.f90'
  !     myatoms = number of atoms that my node is responsible for

!include 'casc_common.f90'

  !     ------------------------------------------------------------------
  !          Local variables and constants

  real*8 tlim(3),E,v,help1,help2,heatfact,epot
  real*8 Ebefore,Eafter,x1store(3)
  real*8 deltasq,d,boxs(3),boxsoverdeltasq(3)


  integer i,j,j2,i1,i2,i3,nscale,typej
  logical ifixed,isborderat,notsputtered

  integer itime
  data itime /0/
  save itime

  !     Variables for neighbor list displacement calc.

  real*8 xneimax,xneimax2
  real*8 x0nei(*),xneisqmax,xneisqmax2
  real*8 xneisq

  !     ------------------------------------------------------------------

  itime=itime+1

  trans=zero
  transin=zero
  natomsin=0
  transv(1)=zero; transv(2)=zero; transv(3)=zero; 
  transbrdr=zero
  nscale=0
  nbrdr=0
  do i=1,3
     tlim(i)=half-tscaleth/box(i)
  enddo

  Emax=-1.0d0
  Emaxnosput=-1.0d0
  vmax=-1.0d0
  vmaxnosput=-1.0d0
  xneisqmax=0.0d0
  xneisqmax2=0.0d0
  boxs(1)=box(1)*box(1); 
  boxs(2)=box(2)*box(2); 
  boxs(3)=box(3)*box(3);

  heatfact=1.0d0-heat*heat

  do j=1,myatoms
     v=zero
     typej=abs(atype(j))
     d=delta(typej)
     deltasq=d*d
     boxsoverdeltasq(1)=boxs(1)/deltasq
     boxsoverdeltasq(2)=boxs(2)/deltasq
     boxsoverdeltasq(3)=boxs(3)/deltasq

     xneisq=zero
     i1=j*3-2
     i2=j*3-1
     i3=j*3-0

     ifixed=.false.
     if (atype(j)<0) ifixed=.true.

     ! Calculate E before border T scale
     help2=      x1(i1)*x1(i1)*boxsoverdeltasq(1)
     help2=help2+x1(i2)*x1(i2)*boxsoverdeltasq(2)
     help2=help2+x1(i3)*x1(i3)*boxsoverdeltasq(3)
     Ebefore=half*help2;

     !
     ! KN Determine what are border atoms
     ! These criteria should be identically copied below!!
     ! 
     ! This is terribly complex - but there is a reason for everything

     isborderat=.false.
     if (x0(i3)*box(3)>tscalzmin.and.x0(i3)*box(3)<tscalzmax) then
        isborderat=.true.
     endif
     if (x0(i1)*box(1)<-tscalxout .or. x0(i1)*box(1)>tscalxout) then
        isborderat=.true.
     endif
     if (x0(i2)*box(2)<-tscalyout .or. x0(i2)*box(2)>tscalyout) then
        isborderat=.true.
     endif

     if (x0(i1)>tlim(1).or.x0(i1)<=-tlim(1) .or.                &
          x0(i2)>tlim(2).or.x0(i2)<=-tlim(2) .or.               &
          x0(i3)>tlim(3).or.x0(i3)<=-tlim(3)) then

        ! Don't scale at open borders
        if ((mtemp==7.or.mtemp==8).and. pbc(1)/=1.0d0 .and.          &
             (x0(i1)>tlim(1).or.x0(i1)<-tlim(1))) goto 20
        if ((mtemp==7.or.mtemp==8).and.pbc(2)/= 1.0d0 .and.         &
             (x0(i2)>tlim(2).or.x0(i2)<-tlim(2))) goto 20
        if ((mtemp==7.or.mtemp==8).and.pbc(3)/= 1.0d0 .and.         &
             (x0(i3)>tlim(3).or.x0(i3)<-tlim(3))) goto 20

        isborderat=.true.
     endif
20   continue

     if (ifixed) isborderat=.false.
     if (myproc == irecproc .and. j == irec) isborderat=.false.
     if (x0(i1) < -tscalsputlim .or. x0(i1) > tscalsputlim .or.      &
          x0(i2) < -tscalsputlim .or. x0(i2) > tscalsputlim .or.      &
          x0(i3) < -tscalsputlim .or. x0(i3) > tscalsputlim ) then
        isborderat=.false.
     endif

     ! If Tcontrolt is used, all of the above is ignored!
     if (Tcontrolt >= 0) then
        isborderat=.false.
        if (typej == Tcontrolt) isborderat=.true.
     endif

     if (isborderat) then
        ! heatbrdr is = 1 except if mtemp=5, 7 or 8,
        ! as determined in Temp_Control().
        x1(i1)=heatbrdr*x1(i1)
        x1(i2)=heatbrdr*x1(i2)
        x1(i3)=heatbrdr*x1(i3)
        nscale=nscale+1
     endif

     ! Calculate E after border T scale
     help2=      x1(i1)*x1(i1)*boxsoverdeltasq(1)
     help2=help2+x1(i2)*x1(i2)*boxsoverdeltasq(2)
     help2=help2+x1(i3)*x1(i3)*boxsoverdeltasq(3)
     Eafter=half*help2;

     ! Get heat loss sums. 
     if (isborderat) then
        Elosssum(1)=Elosssum(1)+Ebefore-Eafter
     endif

     ! Now use Eafter as start of E calc for overall E loss
     Ebefore=Eafter

     do j2=1,3
        i=(j-1)*3+j2
        x1(i)= heat*x1(i)
        ! Store this velocity for T scaling loss calculation
        x1store(j2)=x1(i)
        !     
        ! KN This is the Gear algorithm
        !
        if (.not. ifixed .and..not.( (FzmaxYz/=zero .or. dydtmaxz/=zero)  .and. j2==3 .and. x0(i)*box(3)>=Fzmaxz)) then
           help1=x1(i)+x2(i)+x3(i)+x4(i)+x5(i)
           x0(i)=x0(i)+help1
           x0nei(i)=x0nei(i)+help1
           xneisq=xneisq+x0nei(i)*x0nei(i)*boxs(j2)
           x1(i)=x1(i)+two*x2(i)+three*x3(i)+four*x4(i)+five*x5(i)
        else
           x1(i)=zero
        endif
        x2(i)=   x2(i)+three*x3(i)+ six*x4(i)+ ten*x5(i)
        x3(i)=               x3(i)+four*x4(i)+ ten*x5(i)
        x4(i)=                          x4(i)+five*x5(i)

        if (mtemp==2) x1(i)=zero

        if (mtemp==9.and.j2==1) then
           if (x1(i)*x2(i)+x1(i+1)*x2(i+1)+x1(i+2)*x2(i+2)<zero) then
              x1(i  )=fdamp*x1(i  )
              x1(i+1)=fdamp*x1(i+1)
              x1(i+2)=fdamp*x1(i+2)
           end if
        end if

        help2=x1(i)*x1(i)*boxsoverdeltasq(j2) ! boxs(j2)/deltasq

        if (x0(i) >= half) then
           x0(i)=x0(i)-pbc(j2)
        elseif (x0(i) < -half) then
           x0(i)=x0(i)+pbc(j2)
        endif

        transv(j2)=transv(j2)+help2
        v=v+help2

        ! This incredibly stupid thing prevented an -O bug in Absoft f90 V5.0 !
        !if (j==myatoms+j) print *,j,i,ifixed,v,x0(i),x1(i),boxsoverdeltasq(j2)

     enddo ! End of loop over dimensions 1 - 3

     ! Calculate E after T scale, but excluding the solution of the equations of motion
     help2=      x1store(1)*x1store(1)*boxsoverdeltasq(1)
     help2=help2+x1store(2)*x1store(2)*boxsoverdeltasq(2)
     help2=help2+x1store(3)*x1store(3)*boxsoverdeltasq(3)
     Eafter=half*help2;

     ! Get heat loss sum 
     Elosssum(2)=Elosssum(2)+Ebefore-Eafter

     ! KN Get max and next to max displacement
     if (xneisq > xneisqmax) then
        xneisqmax2=xneisqmax
        xneisqmax=xneisq
        !        print *,'max1',xneisq,xneisqmax,xneisqmax2  
     else 
        if (xneisq > xneisqmax2) then
           xneisqmax2=xneisq
           !              print *,'max2',xneisq,xneisqmax,xneisqmax2
        endif
     endif

     ! KN get max E and v (be careful with the units !)

     E=half*v
     v=sqrt(v)*vunit(typej)
     Ekin(j)=E
     trans=trans+E
     if (E > Emax) Emax=E
     if (v > vmax) vmax=v

     if (.not.(x0(i1)<-timesputlim .or. x0(i1)>timesputlim .or.    &
          x0(i2)<-timesputlim .or. x0(i2)>timesputlim .or.         &
          x0(i3)<-timesputlim .or. x0(i3)>timesputlim)) then
        ! Atom inside timesputlim
        notsputtered=.true.
        if (nisputlim < 1.0d30) then
           ! If atoms have just been shuffled, this may go wrong since P,Fp and Epair
           ! are from previous time step. But even at worst this only makes the
           ! time step shorter monentaneously, i.e. this is not a big problem..

           if (AnySemi .eqv. .true. .and. EAM .eqv. .false.) then
              epot=P(j)+Epair(j)
           else
              epot=Fp(j)+Epair(j)
           end if
           if (x0(i1)<-nisputlim .or. x0(i1)>nisputlim .or.    &
                x0(i2)<-nisputlim .or. x0(i2)>nisputlim .or.         &
                x0(i3)<-nisputlim .or. x0(i3)>nisputlim) then
              ! Atom outside nisputlim
              ! Count as sputtered if epot==0
              if (epot==zero) then
                 notsputtered=.false.
              endif
           endif
        endif

        if (notsputtered) then
           transin=transin+E
           natomsin=natomsin+1         
           if (E > Emaxnosput) Emaxnosput=E
           if (v > vmaxnosput) vmaxnosput=v
        endif
     endif

     !
     ! KN Determine what are border atoms
     ! 
     ! These criteria should match the ones above exactly.

     isborderat=.false.
     if (x0(i3)*box(3)>tscalzmin.and.x0(i3)*box(3)<tscalzmax) then
        isborderat=.true.
     endif
     if (x0(i1)*box(1)<-tscalxout .or. x0(i1)*box(1)>tscalxout) then
        isborderat=.true.
     endif
     if (x0(i2)*box(2)<-tscalyout .or. x0(i2)*box(2)>tscalyout) then
        isborderat=.true.
     endif

     if (x0(i1)>tlim(1).or.x0(i1)<=-tlim(1) .or.                &
          x0(i2)>tlim(2).or.x0(i2)<=-tlim(2) .or.               &
          x0(i3)>tlim(3).or.x0(i3)<=-tlim(3)) then

        ! Don't scale at open borders
        if ((mtemp==7.or.mtemp==8).and. pbc(1)/=1.0d0 .and.          &
             (x0(i1)>tlim(1).or.x0(i1)<-tlim(1))) goto 30
        if ((mtemp==7.or.mtemp==8).and.pbc(2)/= 1.0d0 .and.         &
             (x0(i2)>tlim(2).or.x0(i2)<-tlim(2))) goto 30
        if ((mtemp==7.or.mtemp==8).and.pbc(3)/= 1.0d0 .and.         &
             (x0(i3)>tlim(3).or.x0(i3)<-tlim(3))) goto 30

        isborderat=.true.
     endif
30   continue

     if (ifixed) isborderat=.false.
     if (myproc == irecproc .and. j == irec) isborderat=.false.
     if (x0(i1) < -tscalsputlim .or. x0(i1) > tscalsputlim .or.      &
          x0(i2) < -tscalsputlim .or. x0(i2) > tscalsputlim .or.      &
          x0(i3) < -tscalsputlim .or. x0(i3) > tscalsputlim ) then
        isborderat=.false.
     endif

     ! If Tcontrolt is used, all of the above is ignored!
     if (Tcontrolt >= 0) then
        isborderat=.false.
        if (typej == Tcontrolt) isborderat=.true.
     endif


     ! Get border kinetic energy
     if (isborderat) then
        transbrdr=transbrdr+E
        nbrdr=nbrdr+1
        if (E > Emaxbrdr) then
           write(6,'(//,A,I8,A,F10.2,A,F10.2)') 'ERROR: atom # ',j,' Ekin ',E,' > Emaxbrdr',Emaxbrdr
           write(6,'(A,3F12.3)') 'Atom position ',  &
                x0(i1)*box(1),x0(i2)*box(2),x0(i3)*box(3)
                     
           call my_mpi_abort('Emaxbrdr exceeded', int(myproc))
        endif
     endif

  enddo

  call my_mpi_dmax(vmax, 1)
  call my_mpi_dmax(Emax, 1)
  call my_mpi_dmax(Emaxnosput, 1)
  call my_mpi_dmax(vmaxnosput, 1)
  IF(debug)PRINT *,'Emax',Emax,Emaxnosput

  xneimax=sqrt(xneisqmax)
  xneimax2=sqrt(xneisqmax2)

  call my_mpi_dmax(xneimax, 1)
  call my_mpi_dmax(xneimax2, 1)

   

end subroutine Predict

!***********************************************************************
! Correct particle positions (periodic boundary conditions)
!***********************************************************************
subroutine Correct(x0,x1,x2,x3,x4,x5,atype,xnp,pbc,deltas,x0nei,      &
     box,mtemp,Fzmaxz,FzmaxYz,dydtmaxz)

  use typeparam

  use defs  

  use para_common

  implicit none



  !     ------------------------------------------------------------------
  !          Variables passed in and out
  real*8 x0(*),x1(*),x2(*),x3(*),x4(*),x5(*), xnp(*),pbc(3)
  integer atype(*),mtemp
  real*8 deltas(itypelow:itypehigh),x0nei(*)
  real*8 box(3),Fzmaxz,FzmaxYz,dydtmaxz

  !     ------------------------------------------------------------------
  !          Local variables and constants
  real*8 f02,f12,f32,f42,f52
!include 'para_common.f90'

  integer i,i1,iat
  !     ------------------------------------------------------------------

  f02=3.0d0/16.0d0
  f12=251.0d0/360.0d0
  f32=11.0d0/18.0d0
  f42=1.0d0/6.0d0
  f52=1.0d0/60.0d0

   
   
   

  do i=1,3*myatoms
     iat=int((i+2)/3)
     i1=MOD(i-1,3)+1
     !
     ! Only place where time step enters solution of equations of motion !
     ! 
     xnp(i)=x2(i)-deltas(abs(atype(iat)))*xnp(i)
     !
     !  Gear algorithm corrector step
     !
     if (atype(iat)>=0 .and..not.( (FzmaxYz/=zero .or. dydtmaxz/=zero) .and. i1==3 .and. x0(i)*box(3)>=Fzmaxz)) then
        x0(i)=x0(i)-xnp(i)*f02
        x1(i)=x1(i)-xnp(i)*f12
     endif
     x2(i)=x2(i)-xnp(i)
     x3(i)=x3(i)-xnp(i)*f32
     x4(i)=x4(i)-xnp(i)*f42
     x5(i)=x5(i)-xnp(i)*f52

     x0nei(i)=x0nei(i)-xnp(i)*f02

     if (mtemp==2) x1(i)=zero

     !
     !     Enforce periodic boundaries
     !
     if (pbc(i1) .eq. 1.0d0 .and. ABS(x0(i)) .ge. 1.5d0) then
        write(6,100) myproc,i,x0(i),xnp(i)
        write(7,100) myproc,i,x0(i),xnp(i)
100     format('PROBLEM IN CORRECT: node,i,x0(i),xnp(i)=',              &
             2i5,2f20.10)
     endif

     if (x0(i) .ge. half) then
        x0(i)=x0(i)-pbc(i1)
     elseif (x0(i) < -half) then
        x0(i)=x0(i)+pbc(i1)
     endif
  enddo

   

end subroutine Correct



!***********************************************************************
!
! Scale total momentum/velocity of cell to 0
!
!  Note: be careful with this, it may cause trouble with open
!  boundaries or fixed atoms...
!
!  Be careful with units: should do conversion in real v units,
!  not the internal ones (might cause trouble for different masses !)
!
!***********************************************************************

subroutine ConserveTotalMomentum(x1,natoms,pscale,delta,atype,box)

  use typeparam  
  use my_mpi

  use defs  

  use para_common
  use casc_common

  implicit none

  real*8,intent(inout) :: x1(*)
  real*8,intent(in)    :: delta(itypelow:itypehigh),box(3)
  integer,intent(in)   :: natoms,pscale,atype(NPMAX)
  integer :: i,i3
  real*8  :: help1,help2,help3,help4,vsum(3)

  ! *** variables for parallel operation ***

  ! myatoms = number of atoms that my node is responsible for
!include 'para_common.f90'
!include 'casc_common.f90'

  vsum(1)=zero;
  vsum(2)=zero;
  vsum(3)=zero; 
  do i=1,myatoms
     i3=i*3-3
     help1=one/delta(abs(atype(i)))*vunit(abs(atype(i)))
     if (pscale==1) help1=mass(abs(atype(i)))*help1

     vsum(1)=vsum(1)+x1(i3+1)*help1*box(1)
     vsum(2)=vsum(2)+x1(i3+2)*help1*box(2)
     vsum(3)=vsum(3)+x1(i3+3)*help1*box(3)
  enddo
  call my_mpi_dsum(vsum, 3)
  vsum(1)=vsum(1)/natoms
  vsum(2)=vsum(2)/natoms
  vsum(3)=vsum(3)/natoms
  do i=1,myatoms
     if (myproc .ne. irecproc .or. i .ne. irec) then
        i3=i*3-3
        help1=mass(atype(i))/delta(abs(atype(i)))*vunit(abs(atype(i)))
        if (pscale==2) help1=one/delta(abs(atype(i)))*vunit(abs(atype(i)))

        x1(i3+1)=x1(i3+1)-vsum(1)/help1/box(1)
        x1(i3+2)=x1(i3+2)-vsum(2)/help1/box(2)
        x1(i3+3)=x1(i3+3)-vsum(3)/help1/box(3)
     endif
  enddo
  IF(debug)PRINT *,'Vel. sums',x1(1),vsum(1)/help1/box(1),vsum(2),vsum(3)

end subroutine ConserveTotalMomentum


!***********************************************************************

subroutine RemoveAngularMomentum(x0,x1,delta,natoms,atype,box)

  use typeparam  
  use my_mpi

  use defs  

  use para_common
  use casc_common

  implicit none
  !     Variables passed in and out
  real*8 x0(*), x1(*),delta(itypelow:itypehigh)
  integer atype(*),natoms
  real*8 box(3)

  !     Local variables
  integer i,i3
  real*8  vsum(3),Ix,Iy,Iz,Itot,Nx,Ny,Nz
  real*8  vx,vy,vz,xcm,ycm,zcm,x_t,y_t,z_t
  real*8  distance,msum,factor,rescale

  ! *** variables for parallel operation ***

  ! myatoms = number of atoms that my node is responsible for
!include 'para_common.f90'
!include 'casc_common.f90'

  if (nprocs > 1) then
     print *,'"subroutine RemoveAngularMomentum does not work in parallel yet'
     call my_mpi_abort('remrot not parallel', int(nprocs))
  endif

  !find the center of mass
  vsum(1)=zero;
  vsum(2)=zero;
  vsum(3)=zero;
  msum=zero;
  do i=1,natoms
     i3=i*3-3
     vsum(1)=vsum(1)+x0(i3+1)*box(1)*mass(atype(i))
     vsum(2)=vsum(2)+x0(i3+2)*box(2)*mass(atype(i))
     vsum(3)=vsum(3)+x0(i3+3)*box(3)*mass(atype(i))
     msum=msum+mass(atype(i))
  enddo
  xcm=vsum(1)/(msum*box(1))   !center of mass - x  
  ycm=vsum(2)/(msum*box(2))   !center of mass - y  
  zcm=vsum(3)/(msum*box(3))   !center of mass - z  

  !Now find the total angular momentum
  Ix=zero;
  Iy=zero;
  Iz=zero;
  do i=1,natoms
     i3=i*3-3
     Ix=Ix+((x0(i3+2)-ycm)*x1(i3+3)-(x0(i3+3)-zcm)*x1(i3+2))*mass(atype(i))*box(2)*box(3)/delta(atype(i))*vunit(atype(i))
     Iy=Iy+((x0(i3+3)-zcm)*x1(i3+1)-(x0(i3+1)-xcm)*x1(i3+3))*mass(atype(i))*box(1)*box(3)/delta(atype(i))*vunit(atype(i))
     Iz=Iz+((x0(i3+1)-xcm)*x1(i3+2)-(x0(i3+2)-ycm)*x1(i3+1))*mass(atype(i))*box(2)*box(1)/delta(atype(i))*vunit(atype(i))
  enddo
  Itot=SQRT(Ix**2+Iy**2+Iz**2)
  !Now the total momentum, and the axis of rotation (parallel to the angular momentum vector) are known

  do i=1,natoms
     i3=i*3-3
     x_t=(xcm*box(1)+Ix)/box(1)
     y_t=(ycm*box(2)+Iy)/box(2)
     z_t=(zcm*box(3)+Iz)/box(3)
     !x_t and xcm are two points on the axis of rotation
     !find the normal to the plane common to the axis of rotation and the point x0(i)

     Nx=(x0(i3+2)*(z_t-zcm)+y_t*(zcm-x0(i3+3))+ycm*(x0(i3+3)-z_t))*box(2)*box(3)
     Ny=(x0(i3+3)*(x_t-xcm)+z_t*(xcm-x0(i3+1))+zcm*(x0(i3+1)-x_t))*box(1)*box(3)
     Nz=(x0(i3+1)*(y_t-ycm)+x_t*(ycm-x0(i3+2))+xcm*(x0(i3+2)-y_t))*box(1)*box(2)
     !Nx,Ny,Nz are the components of the vector perpendicular to the plane

     !vx,vy,vz are the component of the velocity vector parallel to Nx,Ny,Nz
     factor=(x1(i3+1)*Nx*box(1)+x1(i3+2)*Ny*box(2)+x1(i3+3)*Nz*box(3))/(Nx*Nx+Ny*Ny+Nz*Nz)/delta(atype(i))*vunit(atype(i))
     vx=factor*Nx
     vy=factor*Ny
     vz=factor*Nz

     !find the distance between the point x0(i) and the axis of rotation
     distance=SQRT(((x_t-xcm)*(xcm-x0(i3+1))*box(1)*box(1))**2+  &
          &((y_t-ycm)*(ycm-x0(i3+2))*box(2)*box(2))**2+ &
          &((z_t-zcm)*(zcm-x0(i3+3))*box(3)*box(3))**2)/SQRT(((x_t-xcm)*box(1))**2+ &
          &((y_t-ycm)*box(2))**2+((z_t-zcm)*box(3))**2)


     rescale=(Itot/(natoms*mass(atype(i))*distance))/SQRT(vx**2+vy**2+vz**2)
     x1(i3+1)=x1(i3+1)-vx*rescale/box(1)*delta(atype(i))/vunit(atype(i))
     x1(i3+2)=x1(i3+2)-vy*rescale/box(2)*delta(atype(i))/vunit(atype(i))
     x1(i3+3)=x1(i3+3)-vz*rescale/box(3)*delta(atype(i))/vunit(atype(i))
  enddo
end subroutine RemoveAngularMomentum



!******************************************************************
subroutine Check_Sum(words,array,nelements)
  use my_mpi
  use defs  

  use para_common

  implicit none

  !      IMPLICIT REAL*8  (a-h,o-z)
  !      IMPLICIT INTEGER (i-n)

  !     -------------------------------------------------------------
  character*20 words
  real*8 array(*),sum(2)
  integer nelements, i

  !          Variables for parallel operation
!include 'para_common.f90'
  !     -------------------------------------------------------------

  sum(1)=zero
  sum(2)=zero
  do i=1,nelements
     sum(1)=sum(1)+array(i)
     sum(2)=sum(2)+array(i)*array(i)
  enddo
  call my_mpi_dsum(sum, 2)
  IF(iprint)WRITE(6,1000) words,sum(1),sum(2)
1000 format(1x,a20,2f24.16)

  ! An ancient Chinese programmer once said; "In every program, there 
  ! must be atleast one bug, because only the computer gods are perfect".

  !     PRINT *,'Cochroach'

end subroutine Check_Sum



!******************************************************************
subroutine calc_tot_vel(box,atype,delta,natoms,x1,x1_tot_x,x1_tot_y,x1_tot_z)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

real*8 x1(*),delta(itypelow:itypehigh)
integer atype(*)
real*8 box(3)
integer natoms
real*8 x1_tot_x
real*8 x1_tot_y
real*8 x1_tot_z
integer itype
integer i,i3

x1_tot_x = zero
x1_tot_y = zero
x1_tot_z = zero

do i=1,myatoms
   i3=3*i-3
   itype=ABS(atype(i))
   x1_tot_x = x1_tot_x + x1(i3+1)*box(1)/delta(itype)*vunit(itype)
   x1_tot_y = x1_tot_y + x1(i3+2)*box(2)/delta(itype)*vunit(itype)
   x1_tot_z = x1_tot_z + x1(i3+3)*box(3)/delta(itype)*vunit(itype)
enddo
call my_mpi_dsum(x1_tot_x, 1)
call my_mpi_dsum(x1_tot_y, 1)
call my_mpi_dsum(x1_tot_z, 1)
x1_tot_x = x1_tot_x/natoms
x1_tot_y = x1_tot_y/natoms
x1_tot_z = x1_tot_z/natoms

end subroutine calc_tot_vel

!******************************************************************

subroutine calc_avg_diag_vir(box,wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,atomindex)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

integer atomindex(*)
real*8 box(3)
real*8 boxs(3)
integer i
real*8 wxxiavg(*),wxxi(*)
real*8 wyyiavg(*),wyyi(*)
real*8 wzziavg(*),wzzi(*)

boxs(1)=box(1)*box(1)
boxs(2)=box(2)*box(2)
boxs(3)=box(3)*box(3)

do i=1,myatoms
   wxxiavg(atomindex(i)) = wxxiavg(atomindex(i)) + wxxi(i)*boxs(1)
   wyyiavg(atomindex(i)) = wyyiavg(atomindex(i)) + wyyi(i)*boxs(2)
   wzziavg(atomindex(i)) = wzziavg(atomindex(i)) + wzzi(i)*boxs(3)
enddo

end subroutine calc_avg_diag_vir

!******************************************************************

subroutine calc_avg_nondiag_vir(box,atype,delta,x1,x1_tot_x,x1_tot_y,x1_tot_z, &
                            wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,        &
                            wxyiavg,wxyi,wxziavg,wxzi,                     &
                            wyxiavg,wyxi,wyziavg,wyzi,                     &
                            wzxiavg,wzxi,wzyiavg,wzyi,                     &
                            moviemode,vir_help2,atomindex)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

integer atomindex(*)
real*8 x1(*),delta(itypelow:itypehigh)
integer atype(*)
real*8 box(3)
real*8 boxs(3)
real*8 x1_tot_x
real*8 x1_tot_y
real*8 x1_tot_z
integer itype
integer i,i3
real*8 wxxiavg(*),wxxi(*)
real*8 wyyiavg(*),wyyi(*)
real*8 wzziavg(*),wzzi(*)
real*8 wxyiavg(*),wxyi(*)
real*8 wxziavg(*),wxzi(*)
real*8 wyxiavg(*),wyxi(*)
real*8 wyziavg(*),wyzi(*)
real*8 wzxiavg(*),wzxi(*)
real*8 wzyiavg(*),wzyi(*)
integer moviemode
real*8 vir_help2
real*8 vel_x
real*8 vel_y
real*8 vel_z


boxs(1)=box(1)*box(1)
boxs(2)=box(2)*box(2)
boxs(3)=box(3)*box(3)


do i=1,myatoms
   wxxiavg(atomindex(i)) = wxxiavg(atomindex(i)) + wxxi(i)*boxs(1)
   wyyiavg(atomindex(i)) = wyyiavg(atomindex(i)) + wyyi(i)*boxs(2)
   wzziavg(atomindex(i)) = wzziavg(atomindex(i)) + wzzi(i)*boxs(3)
   wxyiavg(atomindex(i)) = wxyiavg(atomindex(i)) + wxyi(i)*box(1)*box(2)
   wxziavg(atomindex(i)) = wxziavg(atomindex(i)) + wxzi(i)*box(1)*box(3)
   wyxiavg(atomindex(i)) = wyxiavg(atomindex(i)) + wyxi(i)*box(2)*box(1)
   wyziavg(atomindex(i)) = wyziavg(atomindex(i)) + wyzi(i)*box(2)*box(3)
   wzxiavg(atomindex(i)) = wzxiavg(atomindex(i)) + wzxi(i)*box(3)*box(1)
   wzyiavg(atomindex(i)) = wzyiavg(atomindex(i)) + wzyi(i)*box(3)*box(2)
   if (moviemode == 17 .or. moviemode == 18) then
      itype=ABS(atype(i))
      i3=3*i-3
      vel_x = x1(i3+1)*box(1)/delta(itype)*vunit(itype)
      vel_y = x1(i3+2)*box(2)/delta(itype)*vunit(itype)
      vel_z = x1(i3+3)*box(3)/delta(itype)*vunit(itype)
   
      wxxiavg(atomindex(i)) = wxxiavg(atomindex(i)) + mass(itype)*vir_help2*(vel_x-x1_tot_x)*(vel_x-x1_tot_x)
      wyyiavg(atomindex(i)) = wyyiavg(atomindex(i)) + mass(itype)*vir_help2*(vel_y-x1_tot_y)*(vel_y-x1_tot_y)
      wzziavg(atomindex(i)) = wzziavg(atomindex(i)) + mass(itype)*vir_help2*(vel_z-x1_tot_z)*(vel_z-x1_tot_z)
  
      wxyiavg(atomindex(i)) = wxyiavg(atomindex(i)) + mass(itype)*vir_help2*(vel_x-x1_tot_x)*(vel_y-x1_tot_y)
      wyxiavg(atomindex(i)) = wyxiavg(atomindex(i)) + mass(itype)*vir_help2*(vel_y-x1_tot_y)*(vel_x-x1_tot_x)
      wyziavg(atomindex(i)) = wyziavg(atomindex(i)) + mass(itype)*vir_help2*(vel_y-x1_tot_y)*(vel_z-x1_tot_z)
      wzyiavg(atomindex(i)) = wzyiavg(atomindex(i)) + mass(itype)*vir_help2*(vel_z-x1_tot_z)*(vel_y-x1_tot_y)
      wxziavg(atomindex(i)) = wxziavg(atomindex(i)) + mass(itype)*vir_help2*(vel_x-x1_tot_x)*(vel_z-x1_tot_z)
      wzxiavg(atomindex(i)) = wzxiavg(atomindex(i)) + mass(itype)*vir_help2*(vel_z-x1_tot_z)*(vel_x-x1_tot_x)
   endif
enddo

end subroutine calc_avg_nondiag_vir

!******************************************************************

subroutine calc_diag_vir(box,wxxi,wyyi,wzzi)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

real*8 box(3)
real*8 boxs(3)
integer i
real*8 wxxi(*),wyyi(*),wzzi(*)

boxs(1)=box(1)*box(1)
boxs(2)=box(2)*box(2)
boxs(3)=box(3)*box(3)

do i=1,myatoms
   wxxi(i) = wxxi(i)*boxs(1)
   wyyi(i) = wyyi(i)*boxs(2)
   wzzi(i) = wzzi(i)*boxs(3)
enddo

end subroutine calc_diag_vir

!******************************************************************

subroutine calc_nondiag_vir(box,atype,delta,x1,x1_tot_x,x1_tot_y,x1_tot_z, &
                            wxxi,wyyi,wzzi,wxyi,wxzi,wyxi,wyzi,wzxi,wzyi,  &
                            moviemode,vir_help2)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

real*8 x1(*),delta(itypelow:itypehigh)
integer atype(*)
real*8 box(3)
real*8 boxs(3)
real*8 x1_tot_x
real*8 x1_tot_y
real*8 x1_tot_z
integer itype
integer i,i3
real*8 wxxi(*),wyyi(*),wzzi(*)
real*8 wxyi(*),wxzi(*)
real*8 wyxi(*),wyzi(*)
real*8 wzxi(*),wzyi(*)
integer moviemode
real*8 vir_help2
real*8 vel_x
real*8 vel_y
real*8 vel_z

boxs(1)=box(1)*box(1)
boxs(2)=box(2)*box(2)
boxs(3)=box(3)*box(3)

do i=1,myatoms
   wxxi(i) = wxxi(i)*boxs(1)
   wyyi(i) = wyyi(i)*boxs(2)
   wzzi(i) = wzzi(i)*boxs(3)
   wxyi(i) = wxyi(i)*box(1)*box(2)
   wxzi(i) = wxzi(i)*box(1)*box(3)
   wyxi(i) = wyxi(i)*box(2)*box(1)
   wyzi(i) = wyzi(i)*box(2)*box(3)
   wzxi(i) = wzxi(i)*box(3)*box(1)
   wzyi(i) = wzyi(i)*box(3)*box(2)
   if (moviemode == 17 .or. moviemode == 18) then
      itype=ABS(atype(i))
      i3=3*i-3
      vel_x = x1(i3+1)*box(1)/delta(itype)*vunit(itype)
      vel_y = x1(i3+2)*box(2)/delta(itype)*vunit(itype)
      vel_z = x1(i3+3)*box(3)/delta(itype)*vunit(itype)

      wxxi(i) = wxxi(i) + mass(itype)*vir_help2*(vel_x-x1_tot_x)*(vel_x-x1_tot_x)
      wyyi(i) = wyyi(i) + mass(itype)*vir_help2*(vel_y-x1_tot_y)*(vel_y-x1_tot_y)
      wzzi(i) = wzzi(i) + mass(itype)*vir_help2*(vel_z-x1_tot_z)*(vel_z-x1_tot_z)

      wxyi(i) = wxyi(i) + mass(itype)*vir_help2*(vel_x-x1_tot_x)*(vel_y-x1_tot_y)
      wyxi(i) = wyxi(i) + mass(itype)*vir_help2*(vel_y-x1_tot_y)*(vel_x-x1_tot_x)
      wyzi(i) = wyzi(i) + mass(itype)*vir_help2*(vel_y-x1_tot_y)*(vel_z-x1_tot_z)
      wzyi(i) = wzyi(i) + mass(itype)*vir_help2*(vel_z-x1_tot_z)*(vel_y-x1_tot_y)
      wxzi(i) = wxzi(i) + mass(itype)*vir_help2*(vel_x-x1_tot_x)*(vel_z-x1_tot_z)
      wzxi(i) = wzxi(i) + mass(itype)*vir_help2*(vel_z-x1_tot_z)*(vel_x-x1_tot_x)
   endif
enddo

end subroutine calc_nondiag_vir

!******************************************************************

subroutine avg_calc_diag_vir(wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,vir_istep_new,vir_istep_old,atomindex)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

integer atomindex(*)
integer i
real*8 wxxiavg(*),wxxi(*)
real*8 wyyiavg(*),wyyi(*)
real*8 wzziavg(*),wzzi(*)
integer vir_istep_new, vir_istep_old

do i=1,myatoms
   wxxi(i) = wxxiavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wyyi(i) = wyyiavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wzzi(i) = wzziavg(atomindex(i))/(vir_istep_new - vir_istep_old)
enddo

end subroutine avg_calc_diag_vir


!******************************************************************

subroutine avg_calc_nondiag_vir(wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,        &
                                wxyiavg,wxyi,wxziavg,wxzi,                     &
                                wyxiavg,wyxi,wyziavg,wyzi,                     &
                                wzxiavg,wzxi,wzyiavg,wzyi,                     &
                                vir_istep_new,vir_istep_old,atomindex)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

integer atomindex(*)
integer i
integer vir_istep_new, vir_istep_old
real*8 wxxiavg(*),wxxi(*)
real*8 wyyiavg(*),wyyi(*)
real*8 wzziavg(*),wzzi(*)
real*8 wxyiavg(*),wxyi(*)
real*8 wxziavg(*),wxzi(*)
real*8 wyxiavg(*),wyxi(*)
real*8 wyziavg(*),wyzi(*)
real*8 wzxiavg(*),wzxi(*)
real*8 wzyiavg(*),wzyi(*)

do i=1,myatoms
   wxxi(i) = wxxiavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wyyi(i) = wyyiavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wzzi(i) = wzziavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wxyi(i) = wxyiavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wxzi(i) = wxziavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wyxi(i) = wyxiavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wyzi(i) = wyziavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wzxi(i) = wzxiavg(atomindex(i))/(vir_istep_new - vir_istep_old)
   wzyi(i) = wzyiavg(atomindex(i))/(vir_istep_new - vir_istep_old)
enddo

end subroutine avg_calc_nondiag_vir

!******************************************************************

subroutine zero_avg_diag_vir(wxxiavg,wyyiavg,wzziavg,atomindex)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

integer atomindex(*)
integer i
real*8 wxxiavg(*),wyyiavg(*),wzziavg(*)

do i=1,myatoms
   wxxiavg(atomindex(i)) = zero
   wyyiavg(atomindex(i)) = zero
   wzziavg(atomindex(i)) = zero
enddo

end subroutine zero_avg_diag_vir

!******************************************************************

subroutine zero_avg_nondiag_vir(wxxiavg,wyyiavg,wzziavg,wxyiavg,wxziavg,   &
                                wyxiavg,wyziavg,wzxiavg,wzyiavg,atomindex)

use PhysConsts
use my_mpi
use defs  
use para_common
use typeparam  
use casc_common

implicit none

integer atomindex(*)
integer i
real*8 wxxiavg(*),wyyiavg(*),wzziavg(*)
real*8 wxyiavg(*),wxziavg(*),wyxiavg(*)
real*8 wyziavg(*),wzxiavg(*),wzyiavg(*)

do i=1,myatoms
   wxxiavg(atomindex(i)) = zero
   wyyiavg(atomindex(i)) = zero
   wzziavg(atomindex(i)) = zero
   wxyiavg(atomindex(i)) = zero
   wxziavg(atomindex(i)) = zero
   wyxiavg(atomindex(i)) = zero
   wyziavg(atomindex(i)) = zero
   wzxiavg(atomindex(i)) = zero
   wzyiavg(atomindex(i)) = zero
enddo

end subroutine zero_avg_nondiag_vir





















