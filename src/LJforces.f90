
module LJforces
  implicit none
  
  real*8, parameter :: LJcut = 9.0

contains

  subroutine LJ_Force(X,atype,A,atomindex,natoms,box,pbc,          &
       nborlist,Epair,Vpair,Ethree,Vthree,wxx,wyy,wzz,wxxi,wyyi,wzzi,   &
       wxxsh,wyysh,wzzsh,natsh,nsh,                                     &
       wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,		&
       atpassflag,nngbrproc,potmode,sw2mod,sw3mod,ECM,reppotcutin, &
       moviemode,movietime_val,calc_avgvir,last_step_predict)

    use defs

    use typeparam
    use datatypes
    use my_mpi

    use para_common

    implicit none

    !
    ! Lennard-Jones potential and force routine.
    ! Adopted from stilweb.f90 and LJ_pair (in LJforces.f90_old).
    ! 
    !       A. Kuronen Aug, 1999
    !


    !
    !     Code originates from ancient HARWELL code, from which it was modified
    !     for PARCAS by Kai Nordlund in Jan 1997
    !

    !
    !     In the PARCAS interface:
    !     Input:      X       contains positions in A scaled by 1/box,
    !                 natoms  Total number of atoms over all nodes
    !                 myatoms Number of atoms in my node, in para_common.h
    !                 box(3)  Box size (box centered on 0)
    !                 pbc(3)  Periodics: if = 1.0d0 periodic
    !
    !     Output:     A       contains forces in eV/A scaled by 1/box
    !                 Epair   V_2 per atom
    !                 Vpair   total V_2 of all atoms
    !                 Ethree  V_3 per atom
    !                 Vthree  total V_3 of all atoms; Vmany in mdx.f
    !                  
    !

    ! The atom types in ATYPE(*) and typeparam are used in the following way:
    !     
    ! On firsttime, all type combinations are looped through. Recognized
    ! iac(it,jt)==1 combinations are set to the correct IATAB value.
    ! For others, a warning is issued.

    ! nborlist(i) has following format (not same as original code !):
    !
    !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
    !  nborlist(2)    Index of first neighbour of atom 1
    !  nborlist(3)    Index of second neighbour of atom 1
    !   ...
    !  nborlist(N1+1)  Index of last neighbour of atom 1
    !  nborlist(N1+2)  Number of neighbours for atom 2 
    !  nborlist(N1+3)  Index of first neighbour of atom 2
    !   ...
    !   ...           And so on for all N atoms
    !

    !
    !     THIS SUBROUTINE CALCULATES THE ACCELERATIONS (A) OF PARTICLES
    !     IN A MOLECULAR DYNAMICS RUN. THE PARTICLE POSITIONS ARE GIVEN
    !     IN X, AND THEIR INTERACTIONS THROUGH A CENTRAL FORCE ARE CALCULATED
    !     ACCORDING TO TWO AND THREE-BODY POTENTIAL FOR SILICON OR GERMANIUM
    !     (STILLINGER-WEBER). 
    !
    !     Si pot.: Stillinger and Weber, PRB 31 (1985) 5262
    !     Ge pot.: Stillinger and Weber, ??
    ! 

    !
    ! ------------------------------------------------------------------------
    !     Variables passed in and out

    real*8 X(*),A(*)
    real*8 box(3),pbc(3)
    integer*4 nborlist(*)
    integer atomindex(*)
    integer natoms,atype(*)

    real*8 Epair(*), Vpair, Ethree(*), Vthree
    real*8 wxx,wyy,wzz,wxxi(*),wyyi(*),wzzi(*)
    real*8 sw2mod(3),sw3mod(3),reppotcutin  

    real*8 wxy,wxyi(*),wxz,wxzi(*),wyx,wyxi(*),wyz,wyzi(*),wzx,wzxi(*),wzy,wzyi(*)
    integer moviemode
    logical movietime_val
    logical calc_vir
    logical calc_avgvir
    logical last_step_predict

    real*8 wxxsh(*),wyysh(*),wzzsh(*),ECM(3),dx,dy,dz
    integer natsh(*),nsh

    integer atpassflag(*),nngbrproc(*),potmode

    real*8 dttimer
    !
    !     PARAMETERS FOR STILLINGER_WEBER FORCE CALCULATION
    !

    logical subcmacc
    data subcmacc /.false./

    real*8 VIR

    real*8 AI,ACM(3)
    real*8 BOX2X,BOX2Y,BOX2Z
    real*8 BOXX2,BOXY2,BOXZ2
    real*8 PH,DPH,PH1,PH2,HALFPH
    real*8 COSMNL,COSS,DAI,DPHN,DPHM,DPHL

    real*8 DXNL,DXNLS,DXNM,DXNMS
    real*8 DYNL,DYNLS,DYNLSM,DYNM,DYNMS,DYNMSM
    real*8 DZNL,DZNLS,DZNLSM,DZNM,DZNMS,DZNMSM
    real*8 XL,XM,YL,YM,ZL,ZM

    real*8 HCOS,HMNL,HKMNL,HKMNLL,HKMNLM
    real*8 RNL,RNLA,RNLEF,RNLSQ,RNM,RNMA,RNMEF,RNMSQ,R0SQ(3)

    integer K,K1,N,M,L,IATN,IATM,IATL,IATYP1,IATYP2,IATYP3,NP3,NP32
    integer IAIND,IAIND2

    integer ingbr,jngbr,nngbr,n3
    real*8 help1,help2

    !
    !     Si & Ge potential parameters: Column 1 is for Si-Si
    !                                   Column 2 is for Ge-Ge
    !                                   Column 3 is for Si-Ge
    !
    !     Future extension: 4 C, 5 C-Si, 6 C-Ge, 7 Sn etc.
    !
    !     Si parameters are from Stillinger and Weber PRB 31 (1985) 6987.
    !     Ge parameters are from Ding and Andersen PRB 34 (1986) 6987.
    !     Alternative Ge (lambda=21,eps=1.918): Wang and Stroud PRB 38 (1988) 1384
    !
    !     The strength of the entire potential epsilon can be modified with the
    !     swpotmod readin parameter, and the strength of the three-body part (ALAM)
    !     independently with the sw3mod readin parameter
    !
    integer, allocatable, save :: IATAB(:,:)
    logical, save :: firsttime,lj
    data firsttime /.true./      


    !
    !     PARAMETERS FOR INTERFACE AND PARALLELLIZATION
    !

!include 'para_common.f90'
    !     ------------------------------------------------------------------

    real*8 maxr,r
    integer i,j,jj,i2,i3,j2,j3,ii,ii3,myi,dfrom,ifrom,nnsh,NP,d
    real*8 t1,dummy1,dummy2

    !
    !     Fermi joining parameters. 
    !     Si-Si 15,1.4 checked: gave threshold displ. energy 18 eV,
    !     looked good, energy conserved with both ZBL and DMol
    !
    real*8 bf(3),rf(3)
    data bf /15.0, 15.0, 15.0/
    data rf /1.4, 1.5, 1.45/

    !
    !     Ugly repulsive potential cutoff to speed things up a bit  
    !     At 2.2, Si fermi function derivative is ~1e-4
    !
    real*8 reppotcut(3)
    data reppotcut /0.0, 0.0, 0.0/       !/2.2, 2.3, 2.25/

    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    !-----------------------------------------------------------------------
    !     PARALLELL INTERFACE
    !-----------------------------------------------------------------------
    !
    !     Parallellization principle:
    !     
    !     1� Copy myatoms atom coordinates*box into buf(1:myatoms*3)
    !     2� Get atom coordinates of neighbouring nodes into rest of buf
    !
    !     3� Calculate forces of myatoms into array pbuf
    !     4� The copy forces of myatoms to array A()
    !
    !     5� Send back accelerations of neighbour atoms to them
    !
    !     By calculating forces of all NP atoms in stage 3�, stage 5�
    !     can be omitted. However, this probably is less efficient except
    !     for really huge simulation cells. This approch requires that the 
    !     neighbour list contains neighbours of the atoms from other nodes 
    !     as well; see mdlinkedlist.f
    !
    !
    !
    !     myatoms is the number of atoms in my node
    !     NP is the total number of atoms, including the passed ones
    !
    !

    !     Stage 1�
    NP=myatoms
    do i=1,myatoms
       i3=i*3-3
       buf(i3+1)=X(i3+1)*box(1)
       buf(i3+2)=X(i3+2)*box(2)
       buf(i3+3)=X(i3+3)*box(3)
    enddo
    do i=1,myatoms
       dirbuf(i)=0
       ibuf(i*2-1)=i
       ibuf(i*2)=atype(i)
    enddo


    !
    !     Stage 2�
    !
    !     Pass and receive atoms to different directions.
    !     The passing of atoms in this piece of code should be identical 
    !     in the neighbour list and force calculations.
    !     
    !     In principle the idea is that the input is the local X(myatoms) 
    !     coordinates, and the output is buf(NP), which contains the 
    !     coordinates of the atoms both in this node and within cut_nei
    !     from this in the adjacent nodes.
    !
    !     The atoms which get passed here must be exactly the
    !     same and in the same order in the force calculations
    !     as in the original neighbour list calculation !
    !
    !     To be able to pass back the P information of j pairs,
    !     we also pass and receive the atom indices and direction information
    !     of the atoms in j pairs.
    !     

    !write(0,'(a,i5)') 'Entering LJforces',myproc

    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !        Loop over directions
       do d=1,8
          j=0; jj=0
          do i=1,myatoms
             if (IAND(atpassflag(i),passbit(d)) .ne. 0) then
                j=j+1
                if (j .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow1', int(myproc))
                i3=i*3-3
                j3=j*3-3
                j2=j*2-2
                xsendbuf(j3+1)=X(i3+1)*box(1)
                xsendbuf(j3+2)=X(i3+2)*box(2)
                xsendbuf(j3+3)=X(i3+3)*box(3)
                isendbuf(j2+1)=i
                isendbuf(j2+2)=atype(i)
                !                 print *,d,j,X(i3+1),X(i3+2)
             endif
          enddo
          !print *,'send',myproc,d,j,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             ! print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(NP*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(NP*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(jj, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !print *,'received',myproc,d,j
             if (jj > 0) then
                call mpi_recv(buf(NP*3+1), jj*3, mpi_double_precision, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(ibuf(NP*2+1), jj*2, my_mpi_integer, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(j, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (j .gt. 0) then
                call mpi_send(xsendbuf, j*3, mpi_double_precision, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(isendbuf, j*2, my_mpi_integer, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
             endif
          endif
          j=jj
          do i=1,j
             dirbuf(NP+i)=d
          enddo
          NP=NP+j
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif
    if (NP .ne. np0pairtable) then
       write (6,*) 'EAMforces np0 problem ',myproc,NP,np0pairtable
    endif
    !print *,'Node received totally ',myproc,NP-myatoms


    !      
    !     Stage 3�: Calculate actual forces
    !

    !-----------------------------------------------------------------------
    !     FORCE CALCULATION 
    !-----------------------------------------------------------------------

    !
    !     In PARCAS interface, use buf instead of X, pbuf instead of A.
    !     pbuf contains actual accelerations, buf actual coordinates
    !     (NOT in PARCAS internal units, scaled by 1/box)
    !
    !     Take care to calculate pressure, total pot. only for atoms in my node !
    !
    !

    !
    !     RNMEF AND RNLEF ARE THE DIFFERENCES BETWEEN DISTANCE
    !     AND THE POTENTIAL CUTOFF
    !
    !
    !  BOUNDARY CONDITIONS
    !  IF PBC EQ 1.0d0, PERIODIC - NOTE THAT DIFFERENT CONDITIONS MAY BE SPECIFIED
    !                            IN X, Y AND Z DIRECTIONS.
    !
    !   IF       DX > BOX2(1)         XL=2*BOX2(1)
    ! -BOX2(1) < DX < BOX2(1)         XL=0
    !            DX < -BOX2(1)        XL=-2*BOX2(1)
    !  I.E. IF A SEPARATION EXCEEDS HALF THE BOX SIDE, IT IS TREATED AS
    !  COMING FROM THE ADJACENT BOX.
    !
    !  N.B. NO PARTICLES ARE MOVED AT THIS STAGE.
    !
    !
    ! Initialize force calculation stuff 
    !

    NP3 = NP*3
    NP32 = NP3-2

    BOX2X=box(1)/2.0
    BOX2Y=box(2)/2.0
    BOX2Z=box(3)/2.0

    BOXX2=box(1)*2.0
    BOXY2=box(2)*2.0
    BOXZ2=box(3)*2.0

    do N = 1,NP3
       pbuf(N) = zero
    enddo
    do N=1,NP
       ebuf(N) = zero
    enddo
    do N = 1,myatoms*3
       A(N) = 0.0
    enddo

    calc_vir = .false.
    if (movietime_val .or. calc_avgvir .or. last_step_predict) then
     if (moviemode == 15 .or. moviemode == 16 .or. moviemode==17 .or. moviemode == 18) then
      calc_vir = .true.
     endif
    endif

    do N = 1,myatoms
       Epair(N) = zero
       Ethree(N) = zero
       wxxi(N) = zero
       wyyi(N) = zero
       wzzi(N) = zero

       if (calc_vir) then
          wxyi(N)=zero
          wxzi(N)=zero
          wyxi(N)=zero
          wyzi(N)=zero
          wzxi(N)=zero
          wzyi(N)=zero
       endif

    enddo

    Vpair=0.0
    Vthree=0.0
    wxx=0.0
    wyy=0.0
    wzz=0.0

    if (calc_vir) then
       wxy=zero
       wxz=zero
       wyx=zero
       wyz=zero
       wzx=zero
       wzy=zero
    endif

    do j=1,nsh
       wxxsh(j)=zero
       wyysh(j)=zero
       wzzsh(j)=zero
       natsh(j)=0
    enddo
    maxr=sqrt(box(1)*box(1)+box(2)*box(2)+box(3)*box(3))/2.0
    VIR = 0.0

    !
    !     K holds index of neighbour list
    !
    K=0

    if (firsttime) then

       write(0,*) 'TYPES',itypelow,itypehigh
       allocate(IATAB(itypelow:itypehigh,itypelow:itypehigh))
       IATAB=1

    endif


    !     Cutoff radius used to be min(Rcutsw,R0_potential), now SIGA

    do i=1,3
       R0SQ(i)=LJcut*LJcut !SIGA(i)*SIGA(i)
       ! if (Rcutswsq < R0SQ(i)) R0SQ(i)=Rcutswsq
    enddo

    !
    !     CALCULATE ACCELERATIONS USING NEIGHBOUR LIST
    !     --------------------------------------------
    !

    PH=0.0
    XL=0.0
    XM=0.0
    YL=0.0
    YM=0.0
    ZL=0.0
    ZM=0.0

    do IATN=1,myatoms             !  <------- main loop over atoms N start
       N=IATN*3-2

       IATYP1=abs(ibuf(IATN*2))
       K = K + 1          
       nngbr = nborlist(K)

       if (nngbr .lt. 0) then
          print *,'LJ nborlist HORROR ERROR !',nngbr
       endif

       do ingbr=1,nngbr       !  <----- loop over neighbours  M start
          K = K + 1          
          IATM=nborlist(K)
          M = IATM*3-2

          if (N == M) cycle

          IATYP2=abs(ibuf(IATM*2))

          lj=.true.
          if (iac(IATYP1,IATYP2) /= 1) then
             lj=.false.
             ! Handle other interaction types
             if (iac(IATYP1,IATYP2) == 0) cycle
             if (iac(IATYP1,IATYP2) == -1) then
                write (6,*) 'ERROR: IMPOSSIBLE INTERACTION'
                write (6,*) myproc,i,j,IATYP1,IATYP2
                call my_mpi_abort('INTERACTION -1', int(myproc))
             endif
          endif

          IAIND=IATAB(IATYP1,IATYP2) ! 3body - if 
          DXNM = buf(N) - buf(M)
          if(PBC(1).ne.1.0d0) goto 140
          XM=SIGN(BOX2X,DXNM+BOX2X)+SIGN(BOX2X,DXNM-BOX2X)
          DXNM=DXNM-XM
140       DXNMS=DXNM*DXNM

          !           SKIP ROUND THE CALCULATION IF SEPARATION EXCEEDS R0.

          if(DXNMS.gt.R0SQ(IAIND)) cycle
          DYNMSM = R0SQ(IAIND) - DXNMS
          DYNM = buf(N+1) - buf(M+1)
          if(PBC(2).ne.1.0d0) goto 150
          YM=SIGN(BOX2Y,DYNM+BOX2Y)+SIGN(BOX2Y,DYNM-BOX2Y)
          DYNM=DYNM-YM
150       DYNMS=DYNM*DYNM
          if(DYNMS.gt.DYNMSM) cycle
          DZNMSM = DYNMSM- DYNMS
          DZNM = buf(N+2) - buf(M+2)
          if(PBC(3).ne.1.0d0) goto 160
          ZM=SIGN(BOX2Z,DZNM+BOX2Z)+SIGN(BOX2Z,DZNM-BOX2Z)
          DZNM=DZNM-ZM
160       DZNMS=DZNM*DZNM
          if(DZNMS.gt.DZNMSM) cycle
          RNMSQ = DXNMS + DYNMS + DZNMS
          RNM = SQRT(RNMSQ)

          !           TWO PARTICLE POTENTIAL

          PH=0.0
          DPH=0.0

          !           Use Newtons third law for 2-body part; take care
          !           to pick exactly half of all interactions

          !write(0,'(a,2i6,g14.8)') 'no cycle: ',iatn,iatm,rnm
          !write(0,'(a,3g14.8)') '        : ',dxnm,dynm,dznm

          !if (DXNM>0.0.or.(DXNM==0.0.and.DYNM>0.0).or.                    &
          !     (DXNM==0.0.and.DYNM==0.0.and.DZNM>0.0)) then
          if (1==1) then


             if (iac(IATYP1,IATYP2)==2) then

                call splinereppot(rnm,ph,dph,1d20,1d20,iatyp1,iatyp2,dummy1,dummy2)

             else

                !                 Lennard-Jones pair potential

                call LJ_Pair(PH,DPH,RNM,IATYP1,IATYP2)

                !                 add repulsive pair potential times fermi function
                !                 (13.4.1993 Kai Nordlund)

                if (rnm < reppotcut(iaind)) then
                   t1=mpi_wtime()
                   call splinereppot(rnm,ph,dph,                          &
                        bf(iaind),rf(iaind),iatyp1,iatyp2,dummy1,dummy2)
                   tmr(32)=tmr(32)+(mpi_wtime()-t1)
                endif
             endif

             !              CALCULATE PHYSICAL PROPERTIES
             !              DAI = (X/R) (D POT/DR) = VECTOR COMPONENT OF FORCE/ACCELERATIONS

             HALFPH=PH/2.0
             ebuf(IATN)=ebuf(IATN)+HALFPH
             ebuf(IATM)=ebuf(IATM)+HALFPH
             VPair=Vpair+PH

             AI=-RNM*DPH

             AI = AI/RNMSQ
             DAI = DXNM*AI
             pbuf(N) = pbuf(N) + DAI
             pbuf(M) = pbuf(M) - DAI
             wxxi(IATN)=wxxi(IATN)+DAI*DXNM

             if (calc_vir) then
                wxyi(IATN)=wxyi(IATN)+DAI*DYNM
                wxzi(IATN)=wxzi(IATN)+DAI*DZNM
             endif

             DAI = DYNM*AI
             pbuf(N+1) = pbuf(N+1) + DAI
             pbuf(M+1) = pbuf(M+1) - DAI
             wyyi(IATN)=wyyi(IATN)+DAI*DYNM

             if (calc_vir) then
                wyxi(IATN)=wyxi(IATN)+DAI*DXNM
                wyzi(IATN)=wyzi(IATN)+DAI*DZNM
             endif

             DAI = DZNM*AI
             pbuf(N+2) = pbuf(N+2) + DAI
             pbuf(M+2) = pbuf(M+2) - DAI
             wzzi(IATN)=wzzi(IATN)+DAI*DZNM

             if (calc_vir) then
                wzxi(IATN)=wzxi(IATN)+DAI*DXNM
                wzyi(IATN)=wzyi(IATN)+DAI*DYNM
             endif

          endif

170       continue

          if (iac(IATYP1,IATYP2) /= 1) cycle


       enddo                  !  <---- end of 2-body loop over atoms M

       ! Corrected the division with box^2 from all being box(1) 18.3.2013
       wxxi(IATN)=wxxi(IATN)/(box(1)*box(1))
       wyyi(IATN)=wyyi(IATN)/(box(2)*box(2))
       wzzi(IATN)=wzzi(IATN)/(box(3)*box(3))

       if (calc_vir) then
          wxyi(IATN)=wxyi(IATN)/(box(1)*box(2))
          wxzi(IATN)=wxzi(IATN)/(box(1)*box(3))
          wyxi(IATN)=wyxi(IATN)/(box(2)*box(1))
          wyzi(IATN)=wyzi(IATN)/(box(2)*box(3))
          wzxi(IATN)=wzxi(IATN)/(box(3)*box(1))
          wzyi(IATN)=wzyi(IATN)/(box(3)*box(2))
       endif

       wxx=wxx+wxxi(IATN)
       wyy=wyy+wyyi(IATN)
       wzz=wzz+wzzi(IATN)

       if (calc_vir) then
          wxy=wxy+wxyi(IATN)
          wxz=wxz+wxzi(IATN)
          wyx=wyx+wyxi(IATN)
          wyz=wyz+wyzi(IATN)
          wzx=wzx+wzxi(IATN)
          wzy=wzy+wzyi(IATN)
       endif

       dx=buf(N)-ECM(1)/box(1)
       dy=buf(N+1)-ECM(2)/box(2)
       dz=buf(N+2)-ECM(3)/box(3)
       r=sqrt(dx*dx+dy*dy+dz*dz)

       nnsh=int(1.0d0+r/maxr*(1.0d0*nsh-1.0d0))
       do j=nsh,nnsh,-1
          wxxsh(j)=wxxsh(j)+wxxi(IATN)
          wyysh(j)=wyysh(j)+wyyi(IATN)
          wzzsh(j)=wzzsh(j)+wzzi(IATN)
          natsh(j)=natsh(j)+1
       enddo

180    continue               !  <-------- end of main loop over atoms N
    enddo

    !
    !     Note: wxx should not be summed over processors here, wxxsh should.
    !      
     
    call my_mpi_dsum(wxxsh, nsh)
    call my_mpi_dsum(wyysh, nsh)
    call my_mpi_dsum(wzzsh, nsh)
     
    call my_mpi_isum(natsh, nsh)
     
    !
    !     Stage 4�: Copy my forces to array A()
    !
    !     Also scale A to correspond to PARCAS xnp array units
    !
    do N = 1,myatoms
       n3=N*3-2
       Epair(N)=ebuf(N)
       A(n3  ) = pbuf(n3  )/box(1)
       A(n3+1) = pbuf(n3+1)/box(2)
       A(n3+2) = pbuf(n3+2)/box(3)
    enddo


    !-----------------------------------------------------------------------
    !     PASSING BACK STAGE
    !-----------------------------------------------------------------------

    !
    !     Stage 5�: Pass back accelerations of neighbour atoms to them
    !

    !
    !  From the array dirbuf, we can figure out from which direction an
    !  atom has been received, and from ibuf what index it has had there. Thus
    !  we can send the pbuf and atom index information to other nodes,
    !  and when receiving it know which atom it belongs to.
    !
    i=0
    IF(debug)PRINT *,'SW pass back',myproc,NP,myatoms
    if (nprocs .gt. 1) then
       t1=mpi_wtime()
       !        print *,'LJ loop 2, pbuf sendrecv',myproc
       !        Loop over directions
       do d=1,8
          !           Loop over neighbours
          i=0
          do j=myatoms+1,NP
             dfrom=dirbuf(j)+4
             if (dfrom .gt. 8) dfrom=dfrom-8
             if (d .eq. dfrom) then
                i=i+1
                if (i .ge. NPPASSMAX)                                     &
                     call my_mpi_abort('NPPASSMAX overflow2', int(myproc))
                ifrom=ibuf(j*2-1)
                isendbuf(i)=ifrom
                i3=i*3-3
                j3=j*3-3
                psendbuf(i)=ebuf(j)
                xsendbuf(i3+1)=pbuf(j3+1)
                xsendbuf(i3+2)=pbuf(j3+2)
                xsendbuf(i3+3)=pbuf(j3+3)
             endif
          enddo
          !           print *,'send',myproc,d,i,nngbrproc(d)
          if (sendfirst(d)) then
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), d, &
                  mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !              print *,'received',myproc,d,ii
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
          else
             call mpi_recv(ii, 1, my_mpi_integer, mpi_any_source, d, &
                  mpi_comm_world, mpi_status_ignore, ierror)
             !              print *,'received',myproc,d,i
             if (ii > 0) then
                call mpi_recv(isendbuf2, ii, my_mpi_integer, &
                     mpi_any_source, d+8, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(psendbuf2, ii, mpi_double_precision, &
                     mpi_any_source, d+16, mpi_comm_world, &
                     mpi_status_ignore, ierror)
                call mpi_recv(xsendbuf2, ii*3, mpi_double_precision, &
                     mpi_any_source, d+24, mpi_comm_world, &
                     mpi_status_ignore, ierror)
             endif
             call mpi_send(i, 1, my_mpi_integer, nngbrproc(d), &
                  d, mpi_comm_world, ierror)
             if (i .gt. 0) then
                call mpi_send(isendbuf, i, my_mpi_integer, &
                     nngbrproc(d), d+8, mpi_comm_world, ierror)
                call mpi_send(psendbuf, i, mpi_double_precision, &
                     nngbrproc(d), d+16, mpi_comm_world, ierror)
                call mpi_send(xsendbuf, i*3, mpi_double_precision, &
                     nngbrproc(d), d+24, mpi_comm_world, ierror)
             endif
          endif
          i=ii
          do ii=1,i
             myi=isendbuf2(ii)
             if (myi .gt. myatoms) then
                print *,myproc,myatoms,myi,ii,i
                call my_mpi_abort('SW i too large', int(myproc))
             endif
             i3=myi*3-3
             ii3=ii*3-3
             Epair(myi)=Epair(myi)+psendbuf2(ii)
             A(i3+1)=A(i3+1)+xsendbuf2(ii3+1)/box(1)
             A(i3+2)=A(i3+2)+xsendbuf2(ii3+2)/box(2)
             A(i3+3)=A(i3+3)+xsendbuf2(ii3+3)/box(3)
          enddo
       enddo
       tmr(33)=tmr(33)+(mpi_wtime()-t1)
    endif



    !-----------------------------------------------------------------------
    !     END OF FORCE CALCULATION
    !-----------------------------------------------------------------------


    if (subcmacc) then
       !     
       !     SUBTRACT THE CENTRE OF MASS ACCELERATION
       !     
       ACM(1) = 0.0
       ACM(2) = 0.0
       ACM(3) = 0.0
       IATN=0
       do N = 1,myatoms*3-2,3
          IATN=IATN+1
          ACM(1) = ACM(1) + A(N)
          ACM(2) = ACM(2) + A(N+1)
          ACM(3) = ACM(3) + A(N+2)
       enddo
       !     WRITE(6,*)'A',AXR,AYT,AZT
       call my_mpi_dsum(ACM, 3)

       IATN=0
       do N = 1,myatoms*3-2,3
          IATN=IATN+1
          A(N) = A(N) - ACM(1)/natoms
          A(N+1) = A(N+1) - ACM(2)/natoms
          A(N+2) = A(N+2) - ACM(3)/natoms
       enddo
    endif

    !      print *,'swend'

    firsttime=.false.

    !write(0,'(a,i5)') 'Leaving LJforces',myproc
    return

  end subroutine LJ_Force

  !-----------------------------------------------------------------------------

  !***********************************************************************
  ! Calculate the pair potential from LJ
  !***********************************************************************

  subroutine LJ_Pair(Vp,dVpdr,r,i,j)

    use defs

    use PhysConsts

    use para_common

    implicit none

    !     ------------------------------------------------------------------
    !          Variables passed in and out
    real*8 Vp,dVpdr,r
    integer :: i,j
    !     ------------------------------------------------------------------
    !          Local variables and constants

    integer,parameter :: ITMAX=2
    real*8 :: e0(ITMAX,ITMAX),r0(ITMAX,ITMAX),Rcut(ITMAX,ITMAX)
    real*8 :: e04(ITMAX,ITMAX),e024(ITMAX,ITMAX)

    real*8 r6,r12,x,q,qp,Rn,rnf,ee
    real*8 x2,ri,RcutmRni,onemx,onemx3,onep3xp6x2

!include 'para_common.f90'
    !     ------------------------------------------------------------------
    !          Approximately 46 flops for r < Rcut

    logical firsttime
    data firsttime /.true./

    save rnf,r0,e0,e04,e024,Rcut,firsttime

    if (firsttime) then

       rnf=0.7d0

       !The factor 1.275 is needed in order to get the minimum at the right position
       !r0(1,1)=1.0d0 * 3.92/sqrt(2.0)* 2.0**(1.0d0/6.0)/1.275  !a(Pt)=3.92 �
       !r0(2,2)=1.0d0 * 3.528/sqrt(2.0)* 2.0**(1.0d0/6.0)/1.275  !a(Ag)=4.09 �

       !e0(1,1)=1.0d0
       !e0(2,2)=1.0d0

       ! LJ parameters for from Kittel table 3.4
       r0(1,1)=3.40
       r0(2,2)=3.98

       e0(1,1)=167d-23/e
       e0(2,2)=320d-23/e

       Rcut(1,1)=LJcut
       Rcut(2,2)=rCut(1,1)

       ! Cross terms

       e0(1,2)=2.0*sqrt(e0(1,1)*e0(2,2))*r0(1,1)**3.0*r0(2,2)**3.0/(r0(1,1)**6.0+r0(2,2)**6.0)
       e0(2,1)=e0(1,2)

       r0(1,2)=((r0(1,1)**6.0+r0(2,2)**6.0)/2.0)**(1.0d0/6.0)
       r0(2,1)=r0(1,2)

       Rcut(1,2)=Rcut(1,1)
       Rcut(2,1)=Rcut(1,1)

       e04=4.0*e0
       e024=-24.0*e0

       firsttime=.false.

       write(0,'(///a/,2(/a8,4g12.6)//)') 'LENNARD-JONES parameters','sigma',r0,'epsilon',e0

    end if

    Vp=zero
    dVpdr=zero
    Rn=rnf*Rcut(i,j)
    ri=one/r
    x=r0(i,j)*ri
    x2=x*x
    r6=x2*x2*x2
    r12=r6*r6

    Vp=e04(i,j)*(r12-r6)
    dVpdr=e024(i,j)*(two*r12-r6)*ri

    q=one
    qp=zero
    if (r .ge. Rcut(i,j)) q=zero
    if (r .gt. Rn .and. r .lt. Rcut(i,j)) then
       RcutmRni=one/(Rcut(i,j)-Rn)
       x=(r-Rn)*RcutmRni
       onemx=(one-x)
       onemx3=onemx**3
       onep3xp6x2=one+three*x+six*x*x
       q=onemx3 * onep3xp6x2
       qp=onemx3 * (12.0d0*x+three) - three*onemx**2 * onep3xp6x2
       qp=qp*RcutmRni
    endif
    dVpdr=dVpdr*q+qp*Vp
    Vp=Vp*q

    !write(0,*) 'LJ',r,Vp

  end subroutine LJ_Pair



  !-----------------------------------------------------------------------------

  !***********************************************************************
  ! Initialize Lennard-Jones parameters
  !***********************************************************************

  subroutine Init_LJ_Pot(rc,i,j,potmode,reppotcutin)

    use defs

    use TYPEPARAM

    use para_common

    implicit none

    ! Variables passed in and out
    real*8 rc,reppotcutin
    integer i,j,potmode

!include 'para_common.f90'

    rc=LJcut

  end subroutine Init_LJ_Pot
end module LJforces
