  !***********************************************************************
  !     PARCAS MD  - PARallell CAScade Molecular Dynamics code
  !
  !***********************************************************************
  !     Classical molecular dynamics using embedded atom potentials.
  !     Two dimensional spatial decomposition used for parallel computers.
  !     Parallel "Ames Lab" version by Dave Turner & Jamie Morris - 1995.
  !
  !     Really major modifications, including scalable parallellism
  !     by Kai Nordlund - 1996 -
  !     Renamed to PARCAS MD on June 1, 1996.
  !
  !
  !     As of parcas V5.0, 6.3.2012, full Fortran MPI implementation
  !     made by Sebastian von Alfthan at the Finnish IT Centre for
  !     Science and Cray is the default.  Merging of branched versions
  !     was done by Daniel Landau.
  !
  !     Works under MPI   --> Any machine or group of machines that use the
  !                           Message Passing Interface communications library
  !
  !***********************************************************************
  !
  ! Modifications done with purpose to be able to simulate collision
  ! cascades starting on May 6, 1996
  !
  !  For notes on changes, see file modification_history and the svn log
  !
  !***********************************************************************
  !  On units: using typical MD units where all internal masses are 1:
  !
  ! The length units here are scaled by the box sides so that within the
  ! program, x always ranges between -0.5 and 0.5. This means that
  ! distances (x0), velocities (x1), accelerations (x2) etc. must
  ! be scaled by box(i) to get real units, where i is 1, 2 or 3.
  !
  ! Note that in these MD INTERNAL UNITS, the v, a, t units still are
  ! functions of the velocity. See Kai Nordlund Master's Thesis for an
  ! explanation on this (in Swedish). The parameters timeunit, vunit,
  ! aunit etc. (see Init_Units) can be used for unit transformations 
  !
  ! The time step is also part of the length vectors, according to the Gear
  ! algorithm unit system: x0 = x0, x1=x1*deltat, x2=x2*deltat^2/2!,
  ! x3=x3*deltat^3/3! etc. Thus, if x is the real length, x' the "real"
  ! velocity (in internal MD units !), etc., we have
  !
  !  x0(i) = x  /box(i)*delta^0/0! = x /box(i)
  !  x1(i) = x' /box(i)*delta^1/1! = x'/box(i)*delta
  !  x2(i) = x''/box(i)*delta^2/2! 
  !
  !  ... etc. until x5(i) 
  !
  !
  !  To get real eV-�-fs units, multiply e.g. x' by vunit(atype(i))
  !
  ! The force xnp(i) obtained from the force calculation is scaled in there,
  ! so if F is the real force in units of eV/A, 
  !
  !  xnp(i) = F/box(idim) 
  !
  ! The virials are in units of wxx=(real virial in eV)/box^2
  !
  !***********************************************************************
  !
  !  File numbers, names and contents (all .in are in dir in/, all .out in out/)
  !
  !     4    reppot...in    Reppot input file
  !     5    eam...in       EAM input file
  !     5    mdlat.in       Atom coord/restart file
  !     5    md.in          Parameter input file
  !     6    (stdout)       Main output file
  !     7    md.out         Main physical output file 
  !     8    mdlat.out      Restart output file
  !     9    atoms.out      Final atom coord and energy output file
  !     9    elstop.in      Elstop input file
  !     10   md.movie       Movie output file, in xyz format
  !     11   slice.movie    Slice around ECM movie output file, in xyz format
  !     12   movie.*        Binary output movie file, see documentation/README.DOCUMENTATION
  !     17   pressures.out  Pressure output file
  !     21   liquidat.out   Liquid output file
  !     22   recoilat.out   Recoil output file
  !     23   tersoff...in   Tersoff parameter files 
  !     25   silica.in      Parameter file for silica input files
  !     59   track.in       Input file for track heating data
  !
  !
  !***********************************************************************
  

  program PARCAS

    use defs

    use TYPEPARAM
    use symbolic_constants

    use PhysConsts

    use Temp_Time_Prog, only: TT_activated
    use silica_wat_mod

    use datatypes
    use my_mpi
    use binout 

    use LJforces

    use para_common
    use casc_common

    use random
    use helmod, only: handle_elfield, set_globs, set_params
    use libfemocs, only: femocs
    use qforces_mod, only: Qforces
    use evaporation
    implicit none


    character(*), parameter :: vstring = 'PARCAS V5.05'
  



    !***********************************************************************
    ! Declarations
    !***********************************************************************

    !     Help variables and their ilk

    real*8 x,y,z,r,rmin
    real*8 vx,vy,vz,v,vmax2

    integer i,j,k,i1,i2,i3,iat

    real*8 help1,help2,help3,help4,help5
    real*8 help(3)

    !     Miscellaneous variables

    integer npairs,ihours,imin,mtemp,natoms,nfixed,ndump,latflag,mdlatxyz,nvac
    integer Tcontrolt
    integer nmovie,nmovie0,moviemode,binmode,slicemode,restartmode
    integer nprtbl,nsteps,istep,isteppcalc
    integer natomsin
    real*8 dtmovie(9),tmovie(9),dtslice(9),tslice(9),vaczmin
    real*8 ECM(3),dslice(3)     !slicing on output
    real*8 ECMIn(3),dsliceIn(3) !slicing on restart reading
    integer ECMfix

    real*8 tempp,tote0,dnum 
    real*8 denom,avgtemp,avgtempmoving,pressure,tote
    real*8 wxx,wyy,wzz,wxxi(NPMAX),wyyi(NPMAX),wzzi(NPMAX)

    real*8 wxy,wxyi(NPMAX),wxz,wxzi(NPMAX)
    real*8 wyx,wyxi(NPMAX),wyz,wyzi(NPMAX)
    real*8 wzx,wzxi(NPMAX),wzy,wzyi(NPMAX)
    real*8 wxxiavg(NPMAX),wyyiavg(NPMAX),wzziavg(NPMAX)
    real*8 wxyiavg(NPMAX),wxziavg(NPMAX)
    real*8 wyxiavg(NPMAX),wyziavg(NPMAX)
    real*8 wzxiavg(NPMAX),wzyiavg(NPMAX)
    integer avgvir
    logical calc_avgvir
    integer vir_istep_old
    integer vir_istep_new
    integer dydttime
    integer outtype
    real*8 outzmin,outzmax
    real*8 outzmin2,outzmax2
    integer virsym
    integer virkbar
    real*8 x1_tot_x
    real*8 x1_tot_y
    real*8 x1_tot_z
    integer itype
    real*8 dh_omega
    real*8 tenten
    real*8 vel_x
    real*8 vel_y
    real*8 vel_z
    real*8 vir_help1
    real*8 vir_help2
    real*8 wxxi2(NPMAX),wyyi2(NPMAX),wzzi2(NPMAX)
    real*8 virboxsiz
    logical last_step_predict

    real*8 pt,dfpdp,Pave
    real*8 P,Fp,Ekin
    real*8 Epair,VPair,Ethree
    real*8 x0,x1,x2,x3,x4,x5,xnp
    real*8 sumfx,sumfy,sumfz  ! Variables for summing the forces in each direction
    integer nsummed
    integer forcesum,sumatype
    real*8 tsec
    real*8 temp,toll 
    real*8 trans,transin,transmoving,transv(3),Vmany
    real*8 rcutmax,avgkin,avgkinmoving,avgenergy,avgpot
    real*8 heat,poten,dttimer,t1,tmain1,cut_nei,cut_neisq
    real*8 dh,dhorig,dhprev 
    real*8 factor,energ,commtime

    real*8, allocatable :: delta(:),deltas(:)
    real*8 :: deltaratio,deltaini,deltamax_fs

    real*8 tmax,tscaleth,rstmax
    real*8 tscalzmin,tscalzmax,tscalxout,tscalyout
    real*8 x0nei,xneimax,xneimax2,neiskinr
    real*8 fixzmin,fixzmax,fixperbrdr,fixxybrdr

    ! Velocity adding (addvel) variables
    real*8 taddvel,zaddvel,eaddvel,vaddvel,addvelt,addvelp
    logical :: doneaddvel=.false.

    real*8 initemp,temp0,trate,restartt,Ftot,endtemp,timeini
    real*8 damp,amp,tdebye,reppotcutin,reppotspl
    integer pscale,remrot

    integer ntimeini

    integer potmode,spline,eamnbands,Fpextrap
    real*8 fdamp ! A.Kuronen: related to mtemp==9 option

    integer :: zipout
    logical :: indir_exists

    !
    !  Atom indices must have a separate array to work correctly in
    !  parallell mode when atoms are passed around nodes !
    !
    integer atomindex(NPMAX)

    ! The number of atoms each node can handle is dependent on the amount
    ! of memory available on the chosen machine.  NPMAX is set in defs.f90
    ! Use enough nodes so that NPMAX > natoms/nprocs

    ! X0-X5 = positions and their first thru fifth time derivatives

    dimension x0(3*NPMAX),x1(3*NPMAX),x2(3*NPMAX),                        &
         x3(3*NPMAX),x4(3*NPMAX),x5(3*NPMAX),                        &
         x0nei(3*NPMAX)


    ! Force, Charge density, Pair and Embedding energy for each atom 

    dimension xnp(3*NPMAX),P(NPMAX,EAMBANDMAX),dFpdp(NPMAX,EAMBANDMAX)
    dimension Fp(NPMAX,EAMBANDMAX),Ekin(NPMAX)

    dimension Epair(NPMAX),Ethree(NPMAX)

    !
    !  Total velocity vector of cell for zero scaling of cell movement
    !
    real*8 vsum(3)

    !
    ! Berendsen pressure control (bpc) variables. See Berendsen et. al.,
    ! J. Chem. Phys. 81 (1984) 3684
    !
    real*8 bpcbeta,bpctau,bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz
    integer bpcmode
    logical doprescalc
    real*8 Pxx,Pyy,Pzz

    ! Numerical pressure calc vars
    integer prescheat
    real*8 numpratio,box0(3),V1(3),V2,V3(3),E1(3),E2,E3(3),numw(3)


    !
    !     Time constant of T modification for determining melting point of
    !     a combined liquid-crystalline cell. If liquid density is less
    !     than solid, use positive, otherwise negative. 
    !     Typical value ~ 30 fs for FCC, ~ -300 fs for Si.
    !
    real*8 tmodtau

    !
    ! Berendsen temperature control (btc) variables. See Berendsen et. al.,
    ! J. Chem. Phys. 81 (1984) 3684
    !
    real*8 btctau

    ! Random force parameters: if prandom==0.0 dont use
    real*8 prandom,mrandom,timerandom
    integer :: nrandom = 0

    ! Parameters for top level force for C44 calculation
    real*8 Fzmaxz,FzmaxYz,FzmaxZz,Fzmaxzr
    integer nFzmax,Fzmaxtyp
    real*8 dydtmaxz
    logical dydtatom

    ! Elstop and sputtering variables
    ! FDe(1) is for non-sputtered atoms
    ! FDe(2) is for sputtered atoms
    integer melstop,melstopst,natelstop,natelstopfar
    real*8 elstopmin,FDe(2),FDesum(2),elstopsputlim
    real*8 tscalsputlim
    logical doelstop,elstopsput

    ! Atom type indices

    integer atype(NPMAX)
    INTEGER iactmode(NPMAX)
    real*8 xp(3),avgp(3)

    ! Nborlist() holds the neighbor lists.

    integer*4 nabors(NPMAX+1)
    INTEGER*4 nborlist(NNMAX*NPMAX),invnborlist(NNMAX*NPMAX)
    integer bondstat(4,0:NNMAX),bondstat2(4,0:NNMAX)
    integer bondstat3(4,0:NNMAX)
    real*8, dimension(3) :: rec_pos


!include 'para_common.f90'
    !     myatoms = number of atoms that my node is responsible for
    !     mxat = the maximum number of atoms on any node
    !     myproc = my relative processor number (0..nprocs-1)
    !     rmn(1-2),rmx(1-2)= x,y min,max boundaries for my nodes region
    !     nprocs = total number of nodes being used
    !     nnodes(1) = number of nodes in x direction (columns)
    !     nnodes(2) = number of nodes in y direction (rows)
    !     buf(8*NPMAX) = buffer space for information from other nodes

    !
    !  Parameters for cascades: energy, xrec, yrec, zrec etc. etc.
    !
!include 'casc_common.f90'

    !
    !     Parameters for recoil sequences (=rs), i.e. multiple recoils in 
    !     one run. If rsnum>0, rs data is read in from file recoildata.in
    !

    !     Number of rsnums, readin parametes If 0 rs not used !
    integer rsnum,rsmode
    logical force_update_pairtable ! Force pairtable update after a new recoil.

    !     Other rs variables
    integer rs_index

    !     File parameters and other miscellania

    integer, parameter :: mdinsize = 300
    character*120 mdinfile(mdinsize)
    integer ninlines,m

    logical :: printnow=.true.
    logical :: firsttime=.true.
    logical :: firstprint=.true.

    logical EAM,Stilweb,Tersoff,Tersoff_Compound,LJ,AnySemi,EDIP,BrennerBeardmore
    logical Silica,Silica_Wat
    logical SortNeiList
    real*8 sw2mod(3),sw3mod(3)
    data sw2mod /one,one,one/
    data sw3mod /one,one,one/

    ! Cutoff modification parameters for BrennerBeardmore
    real*8 R1CC,R2CC
    integer nrestart

    ! Maximum kinetic energy, velocity and Force for all particles in node
    real*8 Emax,vmax,Fmax,Fmaxnosput
    ! Maximum E disregarding sputtered atoms
    real*8 Emaxnosput,vmaxnosput
    ! Total energy losses by E scaling (for each processor separately)
    ! index 1 is borders, index 2 is total
    real*8 Elosssum(2)

    !     Kinetic energy at borders defined by tscaleth
    real*8 heatbrdr,transbrdr
    real*8 tlim(3),dampbrdr
    integer nbrdr
    ! Maximum allowed Ekin at cell border region
    real*8 Emaxbrdr 

    !
    !     Variables for defect recognition.
    !
    real*8 Ekdef,Ekrec
    integer ndefmovie
    integer nliqanal
    !
    !     Array of temperature and pressures at different shells
    !     Shells are arranged spherically, so that the maximum shell contains
    !     all atoms
    !
    integer nsh
    parameter (nsh=18)
    integer natsh(nsh)
    real*8 Tsh(nsh),wxxsh(nsh),wyysh(nsh),wzzsh(nsh)

    !     Maximum distance any atom may move before neighbor list update, 
    !     cut_nei-rcut
    real*8 maxdist 

    !     Time parameters
    !
    !     The time step is selected using the Et and kt criteria in Nordlund,
    !     Comp. Mat. Sci 3 (1995) 448-  . timekt and timeEt are read in.
    !     timeunit(ntype) is the conversion factor of internal units to fs.
    !
    real*8 timekt,timeEt,timeCh
    real*8 time_fs,time_thisrec,deltat_fs
    real*8 timesputlim,nisputlim
    integer:: isum_array(100)
    real*8 :: dsum_array(100)
    !***********************************************************************

    ! Box(1..3)   = size of supercell in x & y & z directions (in Angstroms)
    ! Boxs(1..3)  = Box(1..3)**2
    ! Pb(1..3)    = {0,1}  0 -> nonperiodic, 1 -> periodic in that direction
    ! Ncells(1..3)= number of unit cells in the supercell in each direction

    real*8 box,boxs,pbc,unitcell

    integer ncells
    dimension  box(3),boxs(3),pbc(3), ncells(3),unitcell(3)

    !***********************************************************************
    !
    !   Atom passing variables for new passing algorithm (KN Sep 3 1996-)
    !   for atom passing in neighbour list and force calculation. Don't
    !   confuse this with the atom shuffling, which is an entirely
    !   different thing !
    !
    !   The passbit array is defined in para_common.h
    !
    ! � Passing direction names are defined in defs.h
    !
    ! � passlimmin and max give the passing maximum and minimum limits
    !   where 1 is x and 2 is y. Units are scaled units !

    real*8 passlimmin(2),passlimmax(2)      

    !    atpassflag contains bitwise information on whether any given
    !    atom should be passed in any given direction. This array is
    !    recalculated at every neighbour list calculation, immediately
    !    after each shuffle. npass contains the number of atoms to be
    !    passed in each direction.

    integer atpassflag(NPMAX)
    integer npass(8)
    !    nngbrproc contains the node numbers of the neighbouring
    !    processors of this processor

    integer nngbrproc(8)

    ! track parameters
    real*8 trackx,tracky,trackt
    real*8 trackrmax
    logical :: trackstarted=.false.


    !***********************************************************************
    !     Function declarations

    logical, external :: movietime,slicetime
!    real*8, external :: MyRanf


    !***********************************************************************

    logical :: movietime_val,slicetime_val !the functions save their state, we need to call them once per iteration
    integer(kind=mpi_parameters) :: ierror  ! MPI error code
    integer(kind=mpi_parameters) :: my_rank, my_size

    ! SP: ED-MD options
    real*8 :: current_elfield,elfield,Erate,timerelE,mctemp,zmax,clneicut,elmigrat
    integer :: iabove,evpintval,evpatype,fullsolve,aroundapx,rampmode, use_qforces, edmethod
    
    integer, parameter :: max_tot_atoms = NPMAX
    real*8 :: xq(max_tot_atoms*4) ! Charge
    real*8, parameter :: qscreen = 0.68d0,qrcut = 30.0d0
    integer, parameter :: ncellp(3) = [2,2,2]
    real*8, allocatable :: E_field(:,:,:,:)
    real*8 :: rcorner(3)
    real*8 :: Vqq
    integer :: atoms_per_core(0:31)
    real*8 x0_all(max_tot_atoms*3)
    real*8 x1_all(max_tot_atoms*3)
    real*8 xnp_all(max_tot_atoms*3)
    real*8 Epair_all(max_tot_atoms)
    integer atype_all(max_tot_atoms)
    integer displ(0:63)
    integer iproc
    real*8 curtime, com_overhead
    logical :: evptime

    ! Femocs related variables
    type(femocs)        :: fem
    integer             :: success, femocs_started

    real*8              :: force_norm, force_norm2, vel_norm
    integer, parameter  :: fid = 1564
    integer             :: close_status
    !***********************************************************************

    !     ------------------------------------------------------------------
    !     Begin of program
    !     ------------------------------------------------------------------

    !nprocsreq=1

    call mpi_init(ierror)
    call mpi_comm_rank(mpi_comm_world, my_rank, ierror)
    call mpi_comm_size(mpi_comm_world, my_size, ierror)
    call my_mpi_init()
    ! Save the values to the module.
    myproc = my_rank
    nprocs = my_size

    if (myproc==0) then
       call system("mkdir -p out")
    end if

    iprint = .false.
    if (nprocs == 1 .or. myproc .eq. 0) then
       iprint = .true.
    endif

    if (iprint) then
      inquire(file="./in/.", exist=indir_exists)
      ! WARNING: System call, won't work on systems without tar
      if(.not.(indir_exists)) call system("tar xzf in.tgz") ! Extract in/-directory from tgz-file if it does not exist already
      call Readinfile(mdinfile,ninlines,mdinsize)
    end if

    IF(iprint)WRITE(6,*) ' Started ',vstring
    IF(iprint)WRITE(6,*) 

    if (nprocs > 1) then
       IF(iprint)WRITE(6,1010) nprocs
1010   format('    PARCAS started on ',i4,' processors')
    else
       IF(iprint)WRITE(6,*)'    PARCAS started in scalar mode'
    endif
    if (iprint) then
       print*,' For compilation used: NPmax =', NPMAX, 'NNmax =', NNMAX
    endif

    call mpi_barrier(mpi_comm_world, ierror)
    call mpi_bcast(ninlines, 1, my_mpi_integer, 0, mpi_comm_world, ierror)

    force_update_pairtable=.false.



    ! Read in initial parameters

    call Read_Params(mdinfile,ninlines,                                    &
         nsteps,tmax,deltaini,natoms,box,ncells,                          &
         pbc,mtemp,temp,toll,damp,pscale,remrot,amp,tdebye,latflag,mdlatxyz,nprtbl, &
         ndump,nmovie,timekt,timeEt,timeCh,                               &
         tscaleth,tscalzmin,tscalzmax,tscalxout,tscalyout,                &
         neiskinr,Ekdef,Ekrec,ndefmovie,nliqanal,                         &
         temp0,trate,ntimeini,timeini,nrestart,bpcbeta,               &
         bpctau,bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,bpcmode,               &
         btctau,restartmode,restartt,potmode,spline,eamnbands,Fpextrap,initemp,nvac,vaczmin,    &
         melstop,melstopst,elstopmin,tmodtau,rsnum,rsmode,endtemp,dtmovie,tmovie,dtslice,  &
         tslice,dslice,ECM,ECMfix,dsliceIn,ECMIn,sw2mod,sw3mod,prandom,mrandom,          &
         timerandom,Fzmaxz,FzmaxYz,FzmaxZz,Fzmaxzr,Fzmaxtyp, dydtmaxz,    &
         fixzmin,fixzmax,fixperbrdr,fixxybrdr,                            &
         reppotcutin,elstopsputlim,timesputlim,nisputlim,tscalsputlim,    &
         taddvel,zaddvel,eaddvel,vaddvel,Emaxbrdr,moviemode,binmode,slicemode,fdamp,        &
         addvelt,addvelp,Tcontrolt,trackx,tracky,trackt,forcesum,sumatype,zipout,avgvir,dydttime, &
         outtype,outzmin,outzmax,outzmin2,outzmax2,virsym,virkbar,virboxsiz,R1CC,R2CC, &
         elfield,timerelE,Erate,rampmode,iabove,fullsolve,aroundapx,mctemp,&
         evpintval,zmax,evpatype,clneicut,elmigrat, use_qforces, edmethod) ! SP: ED-MD parameters
                        
    ! binary output initialization 
    if (moviemode == 9 .or. slicemode == 9 .or. restartmode == 9 ) then
       call initBinaryMode(pbc(1), pbc(2), pbc(3)) 
       !init binary writing once we know periodicity
       !initialize filesequence data for each type of output
       if (moviemode == 9) then
          !full system snapshots, write out in real*4
          call initFilesequence(1,binmode,"out/movie.",0,4)
       endif
       if (slicemode == 9) then
          !slice snapshots, write out in real*4
          if ((dslice(1)>=0.0.or.dslice(2)>=0.0.or.dslice(3)>=0.0)) then
             call initFilesequence(2,binmode,"out/slice.",0,4)
          endif
       endif
       if (restartmode == 9) then
          !instead of mdlat stuff
          !mode 28, write out velocities (and of course index and atype)
          !write out as real*8
          call initFilesequence(3,28,"out/restart",1,8)
       end if
    end if


    !     Handle type-dependent parameters 

    ! Allocate some type-dependent arrays
    allocate(delta(itypelow:itypehigh))
    allocate(deltas(itypelow:itypehigh))

    IF(debug)PRINT *,'Init_Units',itypelow,itypehigh
    call Init_Units


    ! Initialize the simulation, including reading in/constructing of atoms

    IF(debug)PRINT *,'Md_init',myproc

    call Md_Init(x0,x1,atomindex,                                         &
         atype,natoms,nfixed,amp,initemp,tdebye,box,pbc,ncells,           &
         latflag,restartmode,mdlatxyz,nvac,vaczmin,fixzmin,fixzmax,fixperbrdr, &
         fixxybrdr,dsliceIn,ECMIn, Fzmaxz,dydtmaxz)

    !     Initialize time variables

    deltat_fs=1d30;
    do i=itypelow,itypehigh
       ! Only account for atoms really present in system
       if ((noftype(i) > 0 .or. (irec /= 0 .and. recatype == i)) ) then
          IF(iprint)WRITE (6,*)                                                  &
               'Found atoms of type',i,' for time step calc.'
          if (deltaini*timeunit(i) < deltat_fs) then
             deltat_fs=deltaini*timeunit(i)
          endif
       endif
    enddo
    deltamax_fs=deltat_fs
    IF(iprint)WRITE (6,'(A,F13.6,A)')                                              &
         'Max. time step',deltamax_fs,' fs'

    ! Initialize delta() arrays - Emax etc. ignored
    if (irec == IREC_NO_RECOIL) then
       call Get_Tstep(0,zero,zero,timekt,timeEt,timeCh,                   &
            deltat_fs,deltamax_fs,delta,deltas,deltaratio,rstmax)
    endif

    ! Give Gaussian initial velocities to atoms
    !
    !  At this stage, x1 does not have deltat units yet (see below)

    if (initemp .ne. 0.0 .and.                                            &
         .not. (latflag .eq. 3 .or. latflag .eq. 4)) then
       call gaussvel(initemp,myatoms,x1,box)
       if (initemp .gt. 0.0) then
          IF(iprint)WRITE (6,'(A,F8.1,A,I8,A)')                                     &
               'Gave gaussian initial temperature',                        &
               initemp*invkBeV,' to all atoms'
       else
          IF(iprint)WRITE (6,'(A,F8.1,A,I8,A)')                                  &
               'Gave equal initial temperature',                        &
               -initemp*invkBeV,' to all atoms'
       endif
    endif

    !     Potential initialization
    EAM=.false.
    Stilweb=.false.
    Tersoff=.false.
    Tersoff_Compound=.false.
    EDIP=.false.
    Silica=.false.
    Silica_Wat=.false.
    AnySemi=.false.
    LJ=.false.  
    BrennerBeardmore=.false.    
    
    if (potmode .eq. 0) then
       IF(iprint)WRITE (6,*)
       IF(iprint)WRITE (6,*) 'Lennard-Jones potential is now implemented again!'
       IF(iprint)WRITE (6,*)
       LJ=.true.
    else if (potmode .eq. 1 .or. potmode .eq. 2) then
       IF(iprint)WRITE (6,*) 'EAM potential'
       EAM=.true.
    else if (potmode==3 .or. potmode == 4 .or. (potmode>=300.and.potmode<=399)) then
       IF(iprint)WRITE (6,*) 'Stillinger-Weber potential'
       Stilweb=.true.
    else if (potmode>=5 .and. potmode<=8) then
       IF(iprint)WRITE (6,*) 'Tersoff potential'
       Tersoff=.true.
    else if ((potmode>=10 .and. potmode<=29) .or. (potmode>=40 .and. potmode<=99)) then
       IF(iprint)WRITE (6,*) 'Tersoff Compound potential'
       Tersoff_Compound=.true.
    else if (potmode>=30 .and. potmode<=39) then
       IF(iprint)WRITE (6,*) 'EDIP Si'
       EDIP=.true.
    else if (potmode>=200 .and. potmode<210) then
       IF(iprint)WRITE (6,*) 'Silica Ohta potential'
       Silica=.true.
    else if (potmode>=210 .and. potmode<220) then
       IF(iprint)WRITE (6,*) 'Silica Watanabe potential'
       Silica_Wat=.true.
    else if (potmode==101) then
       IF(iprint)WRITE (6,*) 'BrennerBeardmore potential'
       BrennerBeardmore=.true.
    else
       IF(iprint)WRITE (6,*) 'Unknown potmode',potmode
       call my_mpi_abort('Unknown potmode', INT(potmode))
    endif
    AnySemi=Tersoff.or.Stilweb.or.Tersoff_Compound.or.EDIP.or.Silica.or.Silica_Wat.or.BrennerBeardmore
    SortNeiList=Stilweb.or.Silica.or.Silica_Wat


    if (moviemode == 15 .or. moviemode == 5 .or. moviemode == 6 .or. moviemode == 16 .or. moviemode == 17 .or. moviemode == 18) then
       AnySemi=.true.
    endif

    do i=itypelow,itypehigh
       do j=itypelow,itypehigh
          rcut(i,j)=zero
       enddo
    enddo

    if (LJ) then
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
             if (iac(i,j) == 1) then
                call Init_LJ_Pot(rcut(i,j),i,j,potmode,reppotcutin)
             else
                rcut(i,j)=zero
             endif
          enddo
       enddo
    endif

    if (EAM) then
       IF(debug)PRINT *,'Init_pot',myproc,potmode
       !        rcut is read in for LJ and set in Init_Pot for EAM, Tersoff, Stilweb
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
             if (iac(i,j) == 1 .or. iac(i,j)==4 .or. iac(i,j)==6) then
                call Init_Pot(rcut(i,j),i,j,potmode,spline,eamnbands)
             else
                rcut(i,j)=zero   ! Ignored anyway
             endif
          enddo
       enddo
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
             if (iac(i,j) == 3) then
                call Make_Cross_Pot(rcut(i,j),i,j,potmode,spline)
             endif
          enddo
       enddo
    endif
    if (Stilweb) then
       ! rcut is set inside this
       call Init_SW_Pot(potmode)
    endif
    if (Tersoff) then
       call Init_Ter_Pot(rcut(1,1),potmode,reppotcutin)
    endif
    if (BrennerBeardmore) then
       ! Initialize rcuts
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
          rcut(i,j)=zero
          enddo
       enddo
       call CSHPRM(R1CC,R2CC)
       call Beardmore_SetCut(reppotcutin)
    endif


    if (EDIP) then
       call Init_Edip(reppotcutin)
    endif
    if (Silica) then
       ! rcut is set inside this
       call Init_Silica_Pot()
    endif
    if (Silica_Wat) then
       ! rcut is set inside this
       call init_silica_wat()
    endif

    if (Tersoff_Compound) then
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
             if (iac(i,j) == 1) then
                call Init_Ter_Compound_Pot(rcut(i,j),i,j,potmode,reppotcutin)
             else
                rcut(i,j)=zero
             endif
          enddo
       enddo
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
             do k=itypelow,itypehigh
                if (iac(i,j) == 1 .and. iac(i,k) == 1 .and. iac(j,k) == 1) then
                   call Init3_Ter_Compound_Pot(i,j,k,potmode)
                endif
             enddo
          enddo
       enddo
    endif

    rcutmax = max(maxval(rcut), maxval(rcutin))

    if (Tersoff_Compound) then
       do i=itypelow,itypehigh
          do j=itypelow,itypehigh
             if (iac(i,j) /= 1) then
                if (rcutin(i,j) >= 0.0d0) then
                   rcut(i,j)=rcutin(i,j)
                else
                   rcut(i,j)=rcutmax
                endif
                !call Set_Ter_Max(i,j,rcutmax)
                call Set_Ter_Max(i,j,rcut(i,j))
             endif
          enddo
       enddo
    endif

    do i=itypelow,itypehigh
       do j=itypelow,itypehigh
          IF(iprint)WRITE(6,1011) rcut(i,j),i,j
          IF(iprint)WRITE(7,1011) rcut(i,j),i,j
1011      format(' Cutoff = ',f10.6,' for atypes',2I2)
       enddo
    enddo
    cut_nei=neiskinr*rcutmax
    maxdist=cut_nei-rcutmax-2.0*timekt
    cut_neisq=cut_nei*cut_nei

    xneimax=zero
    xneimax2=zero

    tempp=zero

    !
    !  Initialize atom passing routines
    !
    !      CALL Pass_Sequence(npass,passlist,cut_nei,box)
    call mpi_barrier(mpi_comm_world, ierror)
    if (nprocs > 1) then
       call Pass_Init(cut_nei,box,passlimmin,passlimmax,                  &
            nngbrproc,natoms)
    endif

    if (debug .and. nprocs .gt. 1) then
       IF(iprint)WRITE(6,*)
       write(6,1111) myproc,myatoms,(nngbrproc(i),i=1,8)
       IF(iprint)WRITE(6,*)
1111   format(' Node',i4,' has',i6,' atoms and node neighbours',8i3)
    endif

    dh=box(1)*box(2)*box(3)
    dhprev=dh
    dhorig=dh
    boxs(:)=box(:)*box(:)
    box0(:)=box(:)

    unitcell(:)=box(:)/ncells(:)

    ! Zero sums and arrays 

    do i=1,myatoms
       Ekin(i)=0.0d0
       atpassflag(i)=0
    enddo

    vir_istep_old = 0
    last_step_predict = .false.

    if (avgvir == 0) then
       calc_avgvir = .false.
    else
       calc_avgvir = .true.
    endif
 
    dh_omega=dh*virboxsiz/natoms
    tenten = 1d10
    vir_help1 = eV_to_kbar/dh_omega
    vir_help2 = u*tenten/e

    if (calc_avgvir) then      
       if (moviemode == 5 .or. moviemode == 6) then
          call zero_avg_diag_vir(wxxiavg,wyyiavg,wzziavg,atomindex)
       else if (moviemode == 15 .or. moviemode == 16 .or. moviemode == 17 .or. moviemode == 18) then
          call zero_avg_nondiag_vir(wxxiavg,wyyiavg,wzziavg,wxyiavg,wxziavg,   &
                                    wyxiavg,wyziavg,wzxiavg,wzyiavg,atomindex)
       endif
    endif

    if (natoms >= NPMAX) then
       if (calc_avgvir) then
          write (6,*) 'ERROR: NPMAX smaller than natoms,',NPMAX,natoms
          call my_mpi_abort('NPMAX TO SMALL', INT(0))
       endif
    endif

    factor=zero
    istep=0
    isteppcalc=0
    energ=zero

    trans=zero
    transmoving=zero
    transin=zero
    poten=zero
    transv(1)=zero; transv(2)=zero; transv(3)=zero
    transbrdr=zero
    avgkin=zero
    avgkinmoving=zero
    avgpot=zero
    avgenergy=zero
    avgp(1)=zero; avgp(2)=zero; avgp(3)=zero; pt=zero;
    FDesum(1)=zero; FDesum(2)=zero
    Elosssum(1)=zero; Elosssum(2)=zero
    do i=1,99
       tmr(i)=zero
    enddo

    time_fs=zero
    time_thisrec=zero
    if (latflag == 3 .or. latflag == 4) time_fs=restartt

    nmovie0=nmovie

    !     Recoil sequence initializations
    rs_index=0
    rstmax=1d30

    !***********************************************************************
    !
    !               Loop over Recoil Events Begins Here
    !
    !***********************************************************************

100 if (rsnum > 0 .and. rsmode >0) then
       rs_index=rs_index+1
       IF(iprint)WRITE(6,'(A,A,I8,A)') ' --------------------',                    &
            ' recoil ',rs_index,' --------------------- '
       call getnextrec(rsnum,rs_index,irec,xrec,yrec,zrec,                     &
            rectheta,recphi,recen,rstmax)
       if (rstmax <= 0.0d0.and. rsmode==1) then
          print *,'ERROR: cannot handle <=0 time difference in recoilspec for recoil',rs_index
          call my_mpi_abort('rstmax error', INT(0))
       endif
       if (rsmode==2) then
          ! rstmax should be some large value just not to confuse time step selection
          rstmax=1d30
       endif
       if (rs_index==1) then
          recenmax=1
       else
          if (recen > recenmax) then
             recenmax=recen
          endif
       endif

       nmovie=nmovie0
       time_thisrec=0.0
       force_update_pairtable=.true.
       IF(iprint)WRITE(6,'(A,G15.6)') 'This recoil tmax',rstmax
    endif
    !
    !     KN  Give recoil velocity to atom irec
    !
    !     irec modes:
    !
    !     -2     Add an atom to the system at (xrec,yrec,zrec)
    !     -1     Search for nearest atom to given position
    !      0     No recoil event done
    !     >0     Make atom # irec the recoil atom
    !
    !     If latflag==3 and mode<0 find most energetic not sputtered
    !     atom and make it the recoil
    !
    irecproc=-1
    iatrec=0
    if (irec .ne. IREC_NO_RECOIL .and. recen .gt. 0.0) then
       rmin=0.0
       if (latflag .ne. 3) then
          if (recen>1.0d0 .and. (mtemp==1 .or. mtemp==6 .or. mtemp==9)) then
             write(6,'(//,A,I3)') 'WARNING: TEMPERATURE MODE',mtemp
             write(6,'(A,//)') 'CHOSEN WITH CASCADE. IS THIS OK?'
          endif
          IF(iprint)WRITE(6,'(/,A,I3,3F12.2)')                                     &
               'Initializing recoil',irec,xrec,yrec,zrec
          IF(iprint)WRITE(6,'(A,2F13.2,A,F13.1)')                                  &
               'with theta phi',rectheta,recphi,' E=',recen
       endif

       if (irec==IREC_NEAREST .and. latflag /= 3) then
           !        ((irec == IREC_ADD_ATOM .or. irec == IREC_NEAREST) .and. latflag==3)) then
           rmin=1e30
           do i=1,myatoms
               rec_pos = (/ xrec, yrec, zrec /)
               r = sqrt(dot_product( x0(i*3-2: i*3) * box(:) - rec_pos(:), &
                   x0(i*3-2: i*3) * box(:) - rec_pos(:)))
               if (r .lt. rmin) then
                   if (recatype<0 .and. atype(i) /= ABS(recatype)) cycle
                   irec=i
                   iatrec=atomindex(i)
                   rmin=r
               endif
           enddo
           !
           !  Find node in which the real r minimum is
           !
           call mpi_barrier(mpi_comm_world, ierror)
           call getminofallprocs(rmin,irecproc)
           ! Distribute irecproc and irec to every processor

           call mpi_bcast(irec, 1, my_mpi_integer, &
               irecproc, mpi_comm_world, ierror)
           call mpi_bcast(iatrec, 1, my_mpi_integer, &
               irecproc, mpi_comm_world, ierror)

           IF(iprint)WRITE (6,*) 'Recoil atom',irec,iatrec

       else if ((irec == IREC_ADD_ATOM .or. irec == IREC_NEAREST) .and. latflag==3) then
           IF(iprint)WRITE (6,*)                                                 &
               'RESTART mode, searching for recoil atom'

           ! For restart, we must find the maximum energy atom and
           !              ! pretend it was the recoil atom to get the correct energy
           vmax2=-1e30
           do i=1,myatoms
               i3=i*3-3
               !                 Disregard obviously sputtered atoms.
                !                  if (x0(i3+1)<-1.0d0 .or. x0(i3+1)>1.0d0 .or. 
                !     *                 x0(i3+2)<-1.0d0 .or. x0(i3+2)>1.0d0 .or. 
                !     *                 x0(i3+3)<-1.0d0 .or. x0(i3+3)>1.0d0) cycle

               vx=x1(i3+1)*box(1)
               vy=x1(i3+2)*box(2)
               vz=x1(i3+3)*box(3)
               v=SQRT(vx*vx+vy*vy+vz*vz)
               if (v>vmax2) then
                   irec=i
                   iatrec=atomindex(i)
                   vmax2=v
               endif
           enddo
           !
           ! Find node in which the real v maximum is
           ! and inform everyone on it.
           !
           call mpi_barrier(mpi_comm_world, ierror)
           call getmaxofallprocs(vmax2,irecproc)
           call mpi_bcast(irec, 1, my_mpi_integer, irecproc, &
               mpi_comm_world, ierror)
           call mpi_bcast(iatrec, 1, my_mpi_integer, irecproc, &
               mpi_comm_world, ierror)
           recen=0.5*vmax2*vmax2

           if (myproc == irecproc) then
               write (6,170) irec,iatrec,irecproc,recen
170            format (' Atom',I6,I8,' on node',I4,                      &
                   ' interpreted as recoil atom with E',G13.5)
               write (6,171) x0(irec*3-2)*box(1),                        &
                   x0(irec*3-1)*box(2),x0(irec*3)*box(3)
171            format (' Initial recoil position (A)',3F12.4)

               ! We shouldnt modify the pseudo-recoil atom type !
               ! atype(irec)=recatype
               write (6,172) atype(irec)
172            format (' Pseudo-recoil atom has type',I2)
                 
           endif

          if (myproc == irecproc) then
             write (6,180) irec,iatrec,irecproc,rmin
180          format ('Found recoil atom',I6,I8,' on node',I3,             &
                  ' at displ.',F10.3)
          endif

       else if (irec == IREC_ADD_ATOM) then
          if (latflag /= 3) then
             natoms=natoms+1
             call mpi_bcast(natoms, 1, my_mpi_integer, 0, &
                  mpi_comm_world, ierror)
             !              Figure out to which node atom should be added
             x=xrec/box(1)
             y=yrec/box(2)
             z=zrec/box(3)

             if (pbc(1).eq.1.0d0.and.(x.lt.-half.or.x.ge.half)) then
                print *,'ERROR: Cant place recoil outside cell'
                print *,'with periodic x',xrec
                call my_mpi_abort('xrec outside', INT(xrec))
             endif
             if (pbc(2).eq.1.0d0.and.(y.lt.-half.or.y.ge.half)) then
                print *,'ERROR: Cant place recoil outside cell'
                print *,'with periodic y',yrec
                call my_mpi_abort('yrec outside', INT(yrec))
             endif
             if (pbc(3).eq.1.0d0.and.(z.lt.-half.or.z.ge.half)) then
                print *,'ERROR: Cant place recoil outside cell'
                print *,'with periodic z',zrec
                call my_mpi_abort('zrec outside', INT(zrec))
             endif

             if ((x.ge.rmn(1).and.x.lt.rmx(1))                            &
                  .or.(pbc(1).ne.1.0d0.and.                                 &
                  ((x.le.-half.and.rmn(1).eq.-half) .or.                  &
                  (x.ge.half.and. rmx(1).eq. half)))) then

                if ((y.ge.rmn(2).and.y.lt.rmx(2))                         &
                     .or.(pbc(2).ne.1.0d0.and.                              &
                     ((y.le.-half.and.rmn(2).eq.-half) .or.               &
                     (y.ge.half.and. rmx(2).eq. half)))) then

                   myatoms=myatoms+1 

                   print '(A,I4,A,3F9.3)',                                &
                        'Adding recoil atom on node',                     &
                        myproc,' at',xrec,yrec,zrec
                   print '(A,3I8)','Atomindex, internal index, atype',    &
                        natoms,myatoms,recatype

                   IF(debug)PRINT *,x,y,z
                   x0(3*myatoms-2)=x
                   x0(3*myatoms-1)=y
                   x0(3*myatoms-0)=z

                   atype(myatoms)=recatype
                   atomindex(myatoms)=natoms

                   irec=myatoms
                   iatrec=atomindex(irec)
                   irecproc=myproc
                endif
             endif

             IF(debug)PRINT *,'Distributing recoil',myproc,irecproc
             !              Distribute irecproc and irec to every processor
             !              Since irecproc>0 only in one processor, use IMAX 
             call my_mpi_imax(irecproc, 1)
             call mpi_bcast(irec, 1, my_mpi_integer, irecproc, &
                  mpi_comm_world, ierror)
             call mpi_bcast(iatrec, 1, my_mpi_integer, irecproc, &
                  mpi_comm_world, ierror)
          endif
       else ! irec > 0
          call my_mpi_abort('only irec modes -2, -1, and 0 are supported!', INT(irec))
!          if (nprocs .gt. 1) then
!             write (6,*) 'Warning: In parallell mode, using'
!             write (6,*) 'irec > 0 may not be sensible'
!             write (6,*) 'I hope you know what you''re doing...'
!          else
!             irecproc=0
!             iatrec=irec
!             if (recatype >= 0) then
!                if (recatypem==0) then
!                   atype(irec) = recatype
!                endif
!             endif
!          endif
       endif

       mxat=myatoms
       call my_mpi_imax(mxat, 1)
       m=myatoms
       call my_mpi_isum(m, 1)
       if (m /= natoms) then
          IF(iprint)WRITE(6,*) 'After RECOIL atom selection:'
          IF(iprint)WRITE(6,*) 'Total number of atoms in nodes'
          IF(iprint)WRITE(6,*) 'is not what it should be'
          write(6,*) myproc,natoms,m,irec,irecproc
          call my_mpi_abort(' natoms wrong in main', INT(myproc))
       endif

       call mpi_barrier(mpi_comm_world, ierror)
       IF(iprint)WRITE (6,'(A,2I8,I4)')                                            &
            'irec iatrec irecproc',irec,iatrec,irecproc

       if (myproc == irecproc) then 
          if (recatype>=0 .and. .not. latflag==3) then
             if (recatypem==0) then               
                write (6,'(A,I3)') 'Recoil atom type originally was',atype(irec)
                write (6,'(A,I3)') 'Setting recoil atom type to',recatype
                atype(irec)=recatype
             else
                recatype=atype(irec)
                write (6,'(A,I3)') 'recatypem /= 0 so not changing recatype'
                write (6,'(A,I3)') 'Recoil atom has type',recatype
             endif
          else
             recatype=atype(irec)
             write (6,'(A,I3)') 'Recoil atom has type',recatype
          endif
       endif
       if (nprocs > 1) then
          if (myproc/=irecproc) recatype=-1
          call my_mpi_imax(recatype, 1)
       endif
       if (recatype < 0) then
          print *,'Recoil atom fixed, are you crazy? ',myproc,recatype
          call my_mpi_abort('recoil atom fixed', INT(recatype))
       endif
       if (recatype < itypelow .or. recatype > itypehigh) then
          print *,'Recoil atom type does not exist, are you mad?',recatype,itypelow,itypehigh
          call my_mpi_abort('recoil atom type nonexistent', INT(recatype))
       endif

       vmax=SQRT(two*recen)*vunit(recatype)
       vmaxnosput=vmax

       if (rsmode==2 .and. recenmax>recen) then
          vmax=SQRT(two*recenmax)*vunit(recatype)
          vmaxnosput=vmax
          IF(iprint)WRITE (6,*) 'Using recenmax',recenmax,' for tstep'
       endif


       call Get_Tstep(irec,vmax,0.0d0,timekt,timeEt,timeCh,               &
            deltat_fs,deltamax_fs,delta,deltas,deltaratio,rstmax)

       IF(iprint)WRITE (6,*) 'After recoil creation tstep',deltat_fs,'fs for v=',vmax

       if (rs_index>1) then
          !
          !  Correct x1 and x2 units for the new time step (!!)
          !
!          do i=1,3*myatoms
             x1(:)=x1(:)*deltaratio
             x2(:)=x2(:)*deltaratio**2
             x3(:)=x3(:)*deltaratio**3
             x4(:)=x4(:)*deltaratio**4
             x5(:)=x5(:)*deltaratio**5
!          enddo
       endif


       !
       !        Give recoil atom located above recoil velocity
       !

       if (myproc == irecproc) then 
          if (irec > myatoms) then
             write (6,*) 'Error: irec > myatoms',irec,myatoms
             write (6,*) 'on node',irecproc
             call my_mpi_abort('irec > myatoms', INT(myproc))
          endif
          if (latflag .ne. 3) then
             write (6,190) irec,iatrec,recen
190          format (' Giving recoil energy to atom',I6,I8,G13.5)
             write (6,191) x0(irec*3-2)*box(1),                           &
                  x0(irec*3-1)*box(2),x0(irec*3)*box(3)
191          format (' Initial recoil position (A)',3F12.4)


             ! In internal units, mass==1 !
             help1=delta(recatype)*SQRT(2.0*recen)
             x1(irec*3-2) = help1*SIN(rectheta)*COS(recphi)/box(1)
             x1(irec*3-1) = help1*SIN(rectheta)*SIN(recphi)/box(2)
             x1(irec*3-0) = help1*COS(rectheta)/box(3)
             Ekin(irec)=recen
             Emax=recen
             Emaxnosput=recen
             write (6,192)                                                &
                  x1(irec*3-2)*box(1)/delta(recatype)*vunit(recatype),    &
                  x1(irec*3-1)*box(2)/delta(recatype)*vunit(recatype),    &
                  x1(irec*3-0)*box(3)/delta(recatype)*vunit(recatype)
192          format (' Initial recoil vx vy vz (A/fs)',3G13.5)
             print *,'recoil',x1(irec*3-2),x1(irec*3-1),x1(irec*3-0)
             if (ECMfix == 0) then
                ECM(1)=x0(irec*3-2)*box(1)
                ECM(2)=x0(irec*3-1)*box(2)
                ECM(3)=x0(irec*3-0)*box(3)
             else
                write (6,'(A,3F10.4)') 'Using fixed ECM position',      &
                     ECM(1),ECM(2),ECM(3)
             endif


              

          endif

       endif
       if (rs_index<=1) then
          IF(iprint)WRITE(6,*)                                                        &
               'Recoil position will be written to recoilat.out'
          IF(iprint)OPEN(22,file='out/recoilat.out',status='replace')
          IF(iprint)WRITE(6,*) ' '
       endif
    endif

    if (rs_index<=1) then
       if (nliqanal > 0) then
          IF(iprint)OPEN(21,file='out/liquidat.out',status='replace')
       endif

       IF(iprint)OPEN(17,file='out/pressures.out',status='replace')
    endif

    if (rs_index <= 1) then
       ! Set velocities appropriately: scale by delta for restart !
       do i=1,3*myatoms
          iat=INT((i+2)/3)
          x0nei(i)=zero
          if (.not.(latflag .eq. 3 .or. latflag .eq. 4)                      &
               .and. initemp .eq. 0.0) then
             if (myproc .ne. irecproc .or. INT((i+2)/3) .ne. irec) then
                x1(i)=0.0
             endif
          else
             if ((myproc/=irecproc .or. INT((i+2)/3)/=irec)                  &
                  .or. latflag==3) then
                x1(i)=x1(i)*delta(ABS(atype(iat)))
             endif
          endif
          x2(i)=zero
          x3(i)=zero
          x4(i)=zero
          x5(i)=zero
       enddo
    endif

    IF(iprint)WRITE(7,*)
    IF(iprint)WRITE(7,*) ' Starting the main loop.'

195 if(firsttime) tmr(1)=mpi_wtime()

    call mpi_barrier(mpi_comm_world, ierror)      

    if (rsnum>0 .and. rsmode==2) then
       if (rs_index < rsnum) goto 100
    endif



    !***********************************************************************
    !
    !                  Main Loop Begins Here
    !
    !***********************************************************************
200 continue

    tmain1=mpi_wtime()

    ! Calculate the pair table every nprtbl steps,
    ! or if xneimax+xneimax2 is greater than maxdist

    ! 19.3.2013 changes
    movietime_val=movietime(istep,nmovie,time_fs,tmovie,dtmovie,tmax,restartt)
    slicetime_val=slicetime(istep,time_fs,tslice,dtslice) 
    
    if (istep >= dydttime) then
    if (dydtmaxz/=zero) then
       do i=1,3*myatoms   
          ! Apply strain rate dy/dt
          if (mod(i,3)==2) then 
             if (x0(i+1)*box(3) >= Fzmaxz) then
                x0(i)=x0(i)+deltat_fs*dydtmaxz
             endif
          endif
       enddo
       if (movietime_val .or. slicetime_val) then
	  force_update_pairtable=.true.
       endif
    endif
    endif

    !
    !  This has to be in the beginning because of possible redistribution
    !  of atoms (otherwise the Ekin etc. calculations would go wrong)
    !
    IF(debug)PRINT *,'Pair_table',myproc
    if ( ((MOD(istep,nprtbl) .eq. 0) .and. (istep .ne. nsteps)) .or.       &
         (xneimax+xneimax2 .gt. maxdist) .or. &
         force_update_pairtable) then

       !KN
       !        Shuffle atoms between nodes; this is necessary in cascades where
       !        atoms may move quite a lot
       !        To be really exact, the heat scaling should be redone, since 
       !        shuffling the number of atoms changes the new heat values
       ! 
       force_update_pairtable=.false.
       t1=mpi_wtime()
       call mpi_barrier(mpi_comm_world, ierror)
       IF(debug)PRINT *,'Shuffle',myproc
        
       if (nprocs > 1) then
          call Shuffle(x0,x1,x2,x3,x4,x5,x0nei,                           &
               atomindex,atype,natoms,pbc,nngbrproc,printnow)
       endif
       tmr(11)=tmr(11)+(mpi_wtime()-t1)

       t1=mpi_wtime()
       if (debug) then
          IF(iprint)WRITE(6,221) istep,xneimax,xneimax2,maxdist
221       format (' Calculating the pair table, step',I6,' dxmax1/2 ',    &
               2F8.3,' limit',F6.2)
       endif
       call mpi_barrier(mpi_comm_world, ierror)
       IF(debug)PRINT *,'pairtable',myproc
        
       call Pair_Table(npairs,nabors,nborlist,invnborlist, natoms,x0,                        &
               cut_neisq,box,pbc,x0nei,                                      &
               passlimmin,passlimmax,atpassflag,npass,nngbrproc,             &
               atomindex,AnySemi,BrennerBeardmore,SortNeiList,printnow)

       printnow=.false.
       tmr(4)=tmr(4)+(mpi_wtime()-t1)
       IF(debug)PRINT *,'pairtable done',myproc
        
    endif






    ! track velocity adding
    ! Note that Get_Tstep is also called in there      
    if (trackt >= zero .and. .not. trackstarted .and. time_fs > trackt) then
       call addtrack(x0,x1,x2,x3,x4,x5,atype,box,boxs,                    &
            trackx,tracky,trackt,                                         &
            irec,vmaxnosput,timekt,timeEt,timeCh,                         &
            deltat_fs,deltamax_fs,delta,deltas,deltaratio)
       trackstarted=.true.
    endif


    ! Adding a velocity/energy downwards to some atoms
    ! Note that Get_Tstep is also called in there
    if ((vaddvel>zero.or.eaddvel > zero) .and. .not. doneaddvel .and. time_fs >= taddvel) then

       call addvel(x0,x1,x2,x3,x4,x5,atype,box,boxs,                     &
            eaddvel,vaddvel,zaddvel,doneaddvel,                                     &
            irec,vmaxnosput,timekt,timeEt,timeCh,                           &
            deltat_fs,deltamax_fs,delta,deltas,deltaratio,addvelt,addvelp)
    endif


    ! Temperature control

    IF(debug)PRINT *,'Temperature control',myproc
    heat=one
    heatbrdr=one
    if (istep .gt. 0) then
       t1=mpi_wtime()
       if (TT_activated) then
          ! set mtemp, temp, and trate according to
          ! temperature-time program
          call Set_Temp_Control(time_fs,mtemp,temp,temp0,trate, &
               ntimeini,timeini)
       end if
       call Temp_Control(heat,mtemp,temp,toll,transmoving,poten,tote,      &
            heatbrdr,transbrdr,temp0,trate,deltat_fs,ntimeini,             &
            timeini,btctau,istep,time_fs,time_thisrec,rsnum)
       tmr(2)=tmr(2)+(mpi_wtime()-t1)
    endif

205 continue

    ! Update positions and calculate the kinetic energy
    IF(debug)PRINT *,'Predict',myproc
    t1=mpi_wtime()
    call Predict(x0,x1,x2,x3,x4,x5,atype,                                 &
         trans,transin,natomsin,transv,heat,box,pbc,delta,                &
         Emax,Emaxnosput,vmax,vmaxnosput,heatbrdr,transbrdr,              &
         tscaleth,tscalzmin,tscalzmax,tscalxout,tscalyout,                &
         x0nei,xneimax,xneimax2,                                          &
         Ekin,nbrdr,mtemp,timesputlim,nisputlim,Elosssum,                 &
         Emaxbrdr,Fzmaxz,FzmaxYz,dydtmaxz,tscalsputlim,fdamp,P,Fp,Epair,AnySemi,   &
         Tcontrolt,EAM)


    tmr(3)=tmr(3)+(mpi_wtime()-t1)

    !do i=1,myatoms
    ! if (Ekin(i) > 5.0) print *,'Ekinhigh',i,Ekin(i)
    !enddo

    !
    ! KN  See file forcesubs on some notes of the operation of these
    !

    if ((nsteps > 0 .and. istep + 1 .ge. nsteps) .or. &
       (time_fs + deltat_fs .ge. tmax) .or. &
       (rsnum>0 .and. rsmode==1 .and. time_thisrec +deltat_fs .ge. rstmax)) then
       last_step_predict = .true.
    endif

    if (LJ) then
       IF(debug)PRINT *,'LJ',myproc
       t1=mpi_wtime()
       call LJ_Force(x0,atype,xnp,atomindex,natoms,box,pbc,          &
            nborlist,Epair,Vpair,Ethree,Vmany,wxx,wyy,wzz,wxxi,wyyi,wzzi, &
            wxxsh,wyysh,wzzsh,natsh,nsh,                                  &
            wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,        &
            atpassflag,nngbrproc,potmode,sw2mod,sw3mod,ECM,reppotcutin,   &
            moviemode,movietime_val,calc_avgvir,last_step_predict)
       tmr(31)=tmr(31)+(mpi_wtime()-t1)
    endif

    if (EAM) then
       !        Begin EAM force calculation

       IF(debug)PRINT *,'Calc_P',myproc
       t1=mpi_wtime()
       call Calc_P(P, x0,atype,box,pbc,npairs,nborlist,                   &
            atpassflag,nngbrproc,rcutmax,spline,eamnbands,moviemode)
       tmr(5)=tmr(5)+(mpi_wtime()-t1)

       !        Get the embedding energy F and its derivative for each atom

       IF(debug)PRINT *,'Calc_Fp',myproc
       t1=mpi_wtime()
       call Calc_Fp(atype,Fp,dFpdp,Vmany, P,spline,Fpextrap,eamnbands)
       tmr(6)=tmr(6)+(mpi_wtime()-t1)

       !        Calculate the force on each atom

       IF(debug)PRINT *,'Calc_Force',myproc
       t1=mpi_wtime()
       call Calc_Force(xnp,Epair,Vpair,wxx,wyy,wzz,wxxi,wyyi,wzzi,        &
            x0,atype,dFpdp,box,pbc,npairs,nborlist,                       &
            wxxsh,wyysh,wzzsh,natsh,nsh,                                  &
            wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,	  &
            atpassflag,nngbrproc,ECM,rcutmax,spline,eamnbands,            &
            moviemode,movietime_val,calc_avgvir,last_step_predict)
       tmr(7)=tmr(7)+(mpi_wtime()-t1)

    endif

    if (Stilweb) then
       IF(debug)PRINT *,'Stilweb',myproc
       t1=mpi_wtime()
       call Stilweb_Force(x0,atype,xnp,atomindex,natoms,box,pbc,          &
            nborlist,Epair,Vpair,Ethree,Vmany,wxx,wyy,wzz,wxxi,wyyi,wzzi, &
            wxxsh,wyysh,wzzsh,natsh,nsh,                                  &
            wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,	  &
            atpassflag,nngbrproc,potmode,sw2mod,sw3mod,ECM,reppotcutin,   &
            moviemode,movietime_val,calc_avgvir,last_step_predict)
       tmr(31)=tmr(31)+(mpi_wtime()-t1)
    endif

    if (Tersoff) then
       IF(debug)PRINT *,'Tersoff',myproc
       t1=mpi_wtime()
       call Tersoff_Force(x0,atype,xnp,atomindex,natoms,box,pbc,          &
            nborlist,Epair,Vpair,Ethree,Vmany,wxx,wyy,wzz,wxxi,wyyi,wzzi, &
            wxxsh,wyysh,wzzsh,natsh,nsh,                                  &
            wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,        &
            atpassflag,nngbrproc,ECM,moviemode, &
            movietime_val,calc_avgvir,last_step_predict)
       tmr(31)=tmr(31)+(mpi_wtime()-t1)
    endif

      if (BrennerBeardmore) then
	 !doprescalc=.true.
	 !if (prescheat>0 .and. istep>1) then
	    ! Use +1 to make compatible with P calc below
	 !   if (mod(istep+1,prescheat)/=0) doprescalc=.false.
	 !endif
         IF(debug)PRINT *,'BrennerBeardmore',myproc
         t1=mpi_wtime()
         call Beardmore_Force(x0,atype,xnp,atomindex,natoms,box,pbc, &
          nborlist,nabors,npairs,Epair,Vpair,Ethree,Vmany,           &
          wxx,wyy,wzz,wxxi,wyyi,wzzi, wxxsh,wyysh,wzzsh, atpassflag, nngbrproc,           &
          bondstat,bondstat2,bondstat3,reppotspl,potmode)
         tmr(31)=tmr(31)+(mpi_wtime()-t1)

!        print *,' Total energy',Vpair,Vmany

!	help(1)=zero; help(2)=zero; help(3)=zero;
!       do i=1,natoms
!          j=3*(i-1)
!          print*,xnp(j+1),xnp(j+2),xnp(j+3)
!	  help(1)=help(1)+xnp(j+1)*xnp(j+1)
!	  help(2)=help(2)+xnp(j+2)*xnp(j+2)
!	  help(3)=help(3)+xnp(j+3)*xnp(j+3)
!       enddo
!       print *,'AVE SQ FORCE',help(1),help(2),help(3)
       
      endif

    if (Tersoff_Compound) then
       IF(debug)PRINT *,'Tersoff_Compund',myproc
       t1=mpi_wtime()
       call Tersoff_Compound_Force(x0,atype,xnp,atomindex,natoms,box,pbc, &
            nborlist,Epair,Vpair,Ethree,Vmany,wxx,wyy,wzz,wxxi,wyyi,wzzi, &
            wxxsh,wyysh,wzzsh,natsh,nsh,                                  &
            wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,        &
            atpassflag,nngbrproc,ECM,moviemode, &
            movietime_val,calc_avgvir,last_step_predict)
       tmr(31)=tmr(31)+(mpi_wtime()-t1)

    endif

    if (EDIP) then
       IF(debug)PRINT *,'EDIP',myproc
       t1=mpi_wtime()
       call EDIP_forces(x0,atype,xnp,atomindex,natoms,box,pbc,            &
            nborlist,Epair,Vpair,Ethree,Vmany,wxx,wyy,wzz,wxxi,wyyi,wzzi, &
            wxxsh,wyysh,wzzsh,natsh,nsh,                                  &
            wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,        &
            atpassflag,nngbrproc,ECM,moviemode, &
            movietime_val,calc_avgvir,last_step_predict)
       tmr(31)=tmr(31)+(mpi_wtime()-t1)
    endif

    if (Silica) then
       IF(debug)PRINT *,'Silica',myproc
       t1=mpi_wtime()
       call Silica_Force(x0,atype,xnp,atomindex,natoms,box,pbc,          &
            nborlist,Epair,Vpair,Ethree,Vmany,wxx,wyy,wzz,wxxi,wyyi,wzzi, &
            wxxsh,wyysh,wzzsh,natsh,nsh,                                  &
            wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,	  &
            atpassflag,nngbrproc,potmode,ECM,reppotcutin,moviemode,       &
            movietime_val,calc_avgvir,last_step_predict)
       tmr(31)=tmr(31)+(mpi_wtime()-t1)
    endif

    if (Silica_Wat) then
       IF(debug)PRINT *,'Watanabe',myproc
       t1=mpi_wtime()
       call silica_wat_force(x0,atype,xnp,natoms,box,pbc,          &
            nborlist,Epair,Vpair,Ethree,Vmany,wxx,wyy,wzz,wxxi,wyyi,wzzi, &
            wxxsh,wyysh,wzzsh,natsh,nsh,                                  &
            wxy,wxyi,wxz,wxzi,wyx,wyxi,wyz,wyzi,wzx,wzxi,wzy,wzyi,	&
            atpassflag,nngbrproc,potmode,ECM,reppotcutin,moviemode,   &
            movietime_val,calc_avgvir,last_step_predict)
       tmr(31)=tmr(31)+(mpi_wtime()-t1)
    endif

    do i=1,myatoms
       wxxi2(i) = wxxi(i)
       wyyi2(i) = wyyi(i)
       wzzi2(i) = wzzi(i)   
    enddo

    ! SP: ED-MD routine
    evptime = evpintval > 0 ! Check tht evptime /= 0
    if(evptime) evptime = time_fs > timerelE .and. mod(istep,evpintval) == 0
    if(time_fs >= timerelE .and. abs(elfield) > 0.0d0) then
        call mpi_gather(myatoms,1,mpi_integer,atoms_per_core,1,mpi_integer,0,mpi_comm_world,ierror)
        displ = 0
        do iproc=1,nprocs-1
            displ(iproc) = sum(atoms_per_core(0:iproc-1))
        end do
        call mpi_gatherv(x0,myatoms*3,mpi_double_precision,x0_all, &
          atoms_per_core*3,displ*3,mpi_double_precision,0,mpi_comm_world,ierror)
        call mpi_gatherv(x1,myatoms*3,mpi_double_precision,x1_all, &
          atoms_per_core*3,displ*3,mpi_double_precision,0,mpi_comm_world,ierror)
        call mpi_gatherv(atype,myatoms,mpi_integer,atype_all, &
          atoms_per_core,displ,mpi_integer,0,mpi_comm_world,ierror)
        call mpi_gatherv(xnp, myatoms*3, mpi_double_precision, xnp_all, atoms_per_core*3,&
          displ*3, mpi_double_precision, 0, mpi_comm_world, ierror)
        call mpi_gatherv(Epair, myatoms, mpi_double_precision, Epair_all, atoms_per_core,&
          displ, mpi_double_precision, 0, mpi_comm_world, ierror)

        ! helmod or helmod-femocs solver ?
        if(myproc == 0 .and. (edmethod == 0 .or. edmethod == 1)) then
            if (istep == 0) call set_params(timerelE, elmigrat, Erate, &
                elfield, evpatype, fullsolve, aroundapx, rampmode)
            call set_globs(time_fs, box, ncells, deltat_fs, timeunit, &
                        evptime .or. movietime_val, iabove)
        
            call handle_elfield(x0_all, x1_all, myatoms, nborlist, xq, &
                        atype_all, E_field, rcorner, xnp_all, current_elfield)
          
            if(use_qforces == 1) call Qforces(x0_all,xnp_all,box,pbc,Epair_all,Vpair,Vqq, xq, qrcut, qscreen, natoms, debug)

        ! femocs solver ?
        elseif(myproc == 0 .and. edmethod == 2) then

            ! create femocs object only once
            if (femocs_started == 0) then
                fem = femocs("in/md.in")
                femocs_started = 1
            endif

            ! import atoms to Femocs
            call fem%import_parcas(success, myatoms, x0_all, box, nborlist)
            ! run solver
            call fem%run(success, istep, time_fs)
            ! export forces in Parcas units
            call fem%export_data(success, xnp_all, myatoms, "parcas_force")
            ! export total potential energy of atoms without contribution of Lorentz interaction
            call fem%export_data(success, Epair_all, 1, "pair_potential_sum")
            Vpair = Epair_all(1)
            ! export potential energy per atom (pair-potential)
            call fem%export_data(success, Epair_all, myatoms, "pair_potential")
            ! export charges and forces in SI units
            call fem%export_data(success, xq, myatoms, "charge_force")
            ! export atom types
            call fem%export_data(success, real(atype_all, 8), myatoms, "atom_type")
            ! scale velocities
            call fem%export_data(success, x1_all, myatoms, "parcas_velocity")
        endif


        ! write resulting forces and charges to file
        if (debug) then
            open(fid, file = "out/parcas.xyz", action = 'write')
            write(fid,*) myatoms
            write(fid,*) 'Properties=index:I:1:pos:R:3:Fx:R:1:Fy:R:1:Fz:R:1:Fnorm:R:1:', &
                'charge:R:1:force:R:3:force_norm:R:1:Vx:R:1:Vy:R:1:Vz:R:1:Vnorm:R:1:atype:I:1'

            do i = 1, myatoms
                i3 = 3 * i - 2
                i4 = 4 * i - 3
                force_norm = sqrt(sum( xnp_all(i3:i3+2) * xnp_all(i3:i3+2)*box*box ))
                force_norm2 = sqrt(sum( xq(i4+1:i4+3) * xq(i4+1:i4+3) ))
                vel_norm = sqrt(sum( x1_all(i3:i3+2) * x1_all(i3:i3+2)*box*box ))
                write(fid,'(I8,16ES13.5,I8)') i, x0_all(i3:i3+2)*box, xnp_all(i3:i3+2)*box, &
                    force_norm, xq(i4:i4+3), force_norm2, x1_all(i3:i3+2)*box, vel_norm, atype_all(i)
            enddo

            close(fid, iostat=close_status)
        endif


        ! x0 remains unchaged, so no need to scatter
        call mpi_scatterv(x0_all,atoms_per_core*3,displ*3,mpi_double_precision, &
          x0, myatoms*3, mpi_double_precision,0,mpi_comm_world,ierror)
        call mpi_scatterv(x1_all,atoms_per_core*3,displ*3,mpi_double_precision, &
          x1, myatoms*3, mpi_double_precision,0,mpi_comm_world,ierror)
        call mpi_scatterv(atype_all,atoms_per_core,displ,mpi_integer, &
          atype, myatoms, mpi_integer, 0, mpi_comm_world, ierror)
        call mpi_scatterv(xnp_all,atoms_per_core*3,displ*3,mpi_double_precision, &
          xnp, myatoms*3, mpi_double_precision,0,mpi_comm_world,ierror)
        call mpi_scatterv(Epair_all,atoms_per_core,displ,mpi_double_precision, &
          Epair, myatoms, mpi_double_precision, 0, mpi_comm_world, ierror)
    end if

    ! Damping and random force applying
    Ftot=zero
    nrandom=0
    nFzmax=0

    do i=1,3*myatoms

       ! Test
       !print *,'Force',i,abs(xnp(i)*box(mod(i,3)+1))

       if (damp /= zero) xnp(i)=xnp(i)-damp*x1(i)
       if (FzmaxYz/=zero) then
          ! Apply zmax Yz force for C44 calc. (cf. Kittel)
          if (MOD(i,3)==2) then 
             if (x0(i+1)*box(3) >= Fzmaxz .or. (Fzmaxtyp>=0.and.atype(INT(i/3))==Fzmaxtyp)) then
                xnp(i)=xnp(i)+FzmaxYz/box(2)
                !print *,i,x0(i+1),xnp(i),FzmaxYz/box(2)
                nFzmax=nFzmax+1
             endif
          endif
       endif
       if (FzmaxZz/=zero) then
          ! Apply zmax Zz force for Y110 or Y111 calc. (cf. Hei98)
          if (MOD(i,3)==0) then 
             if (x0(i)*box(3) >= Fzmaxz .or. (Fzmaxtyp>=0.and.atype(INT(i/3))==Fzmaxtyp)) then
                if (Fzmaxzr<=zero .or. (SQRT(x0(i-2)**2*boxs(1)+x0(i-1)**2*boxs(2))<Fzmaxzr) &
                     & .or. (Fzmaxtyp>=0.and.atype(INT(i/3))==Fzmaxtyp) ) then

                   xnp(i)=xnp(i)+FzmaxZz/box(3)
                   !print *,i,x0(i),xnp(i),FzmaxZz/box(3),atype(int(i/3))
                   nFzmax=nFzmax+1
                endif
             endif
          endif
       endif

       Ftot=Ftot+xnp(i)*xnp(i)
       if (prandom /= 0.0d0 .and. time_fs < timerandom) then
          if (MyRanf(0) < prandom) then
             xnp(i)=mrandom*xnp(i)
             nrandom=nrandom+1
          endif
       endif
    enddo
    isum_array(1)=nrandom
    isum_array(2)=nFzmax
    call my_mpi_isum(isum_array, 2)
    nrandom=isum_array(1)
    nFzmax=isum_array(2)

    IF(debug)PRINT *,myproc,Ftot
    if (FzmaxYz/=zero .and. MOD(istep,ndump*10)==0) then
       IF(iprint)WRITE(6,*) 'FzmaxYz ',FzmaxYz,' applied on ',nFzmax,'atoms'
    endif
    if (FzmaxZz/=zero .and. MOD(istep,ndump*10)==0) then
       IF(iprint)WRITE(6,*) 'FzmaxZz ',FzmaxZz,' applied on ',nFzmax,'atoms'
       if (Fzmaxzr>zero) then
          IF(iprint)WRITE(6,*) '    .... inside r=',Fzmaxzr
       endif
    endif
    if (prandom/=0.0d0 .and. MOD(istep,ndump*10)==0) then
       IF(iprint)WRITE(6,*) 'Random force applied',                                &
            nrandom,' times'
    endif

    if (melstop /= 0) then
       !        Subtract the electronic stopping
       IF(debug)PRINT *,'elstop',melstop
       t1=mpi_wtime()
       FDe(1)=zero; FDe(2)=zero;
       natelstop=0
       natelstopfar=0
       if (melstop == 1) then
          if (myproc .eq. irecproc) then
             i=irec
             natelstop=1
             call subelstop(i,x1,Ekin,atype,boxs,delta,FDe(1),natoms,box,melstopst)
          endif
       else if (melstop >= 2) then
          do i=1,myatoms
             i3=i*3-3
             doelstop=.false.
             elstopsput=.false.
             if (melstop == 2) doelstop=.true.
             if (melstop == 3 .and. Ekin(i)>=elstopmin) doelstop=.true.
             ! Special handling of sputtered atoms
             if (melstop == 4 .and. Ekin(i)>=elstopmin) then
                doelstop=.true.
                if ((ABS(x0(i3+1))>0.5 .or.                      &
                     ABS(x0(i3+2))>0.5 .or.                      &
                     ABS(x0(i3+3))>0.5)) then
                   doelstop=.false.
                endif
                if ((ABS(x0(i3+1))>elstopsputlim .or.            &
                     ABS(x0(i3+2))>elstopsputlim .or.            &
                     ABS(x0(i3+3))>elstopsputlim)) then
                   doelstop=.true.
                   elstopsput=.true.
                   natelstopfar=natelstopfar+1
                endif
             endif
             if (melstop == 5 .and. Ekin(i)>=elstopmin) then
                doelstop=.true.
                if ((ABS(x0(i3+1))>elstopsputlim .or.    &
                     ABS(x0(i3+2))>elstopsputlim .or.                       &
                     ABS(x0(i3+3))>elstopsputlim)) then
                   doelstop=.false.
                endif
             endif
             if (doelstop) then
                if (elstopsput) then
                   call subelstop(i,x1,Ekin,atype,boxs,delta,FDe(2),natoms,box,melstopst)
                else
                   call subelstop(i,x1,Ekin,atype,boxs,delta,FDe(1),natoms,box,melstopst)
                endif
                natelstop=natelstop+1
             endif
          enddo
       endif
       call my_mpi_dsum(FDe, 2)
       isum_array(1)=natelstop
       isum_array(2)=natelstopfar
       call my_mpi_isum(isum_array, 2)
       natelstop=isum_array(1)
       natelstopfar=isum_array(2)

       FDesum(1)=FDesum(1)+FDe(1); FDesum(2)=FDesum(2)+FDe(2)
       if (MOD(istep,ndump*10) .eq. 0) then
          IF(iprint)WRITE(6,397) 'Total and last FDe = ',                          &
               FDesum(1),FDesum(2),FDe(1),FDe(2),' eV for',natelstop,' atoms including ',        &
               natelstopfar,' sputtered atoms.'

       endif
397    format(A,4(1X,F20.3),A,I8,A,I8,A)
       tmr(31)=tmr(31)+(mpi_wtime()-t1)
    endif


    
    !KN
    !     Get miscellaneous properties 
    !

    IF(debug)PRINT *,'getcascproperties',myproc,nborlist(1)
    t1=mpi_wtime()
    Fmax=0.0; Fmaxnosput=0.0;
    call getcascproperties(istep,irec,time_fs,                              &
         Fmax,Fmaxnosput,timesputlim,nisputlim,                             &
         Ekdef,Ekrec,ndefmovie,nborlist,xnp,x0,atomindex,Epair,P,Fp,Ekin,box,pbc, &
         Tsh,wxxsh,wyysh,wzzsh,natsh,nsh,nliqanal,unitcell,                &
         atpassflag,nngbrproc,natoms,irecproc,ECM,ECMfix,dslice,latflag, &
         EAM,AnySemi)

    tmr(12)=tmr(12)+(mpi_wtime()-t1)

    !    print *,'Ekins ',Ekin(1),Ekin(2)
    if(evpintval /= 0) then
        if(nprocs > 1) then
            print *, "Evaporation calculation doesn't work with MPI yet"
            stop
        end if

        ! Remove atoms after field is at full strength
        if(current_elfield >= elfield) then
            if(mod(istep, evpintval) == 0) then
                call evaporate_atom(xq,x0,x1,atype,box,E_field,rcorner,&
                  mctemp,myatoms,Fp,Epair,ncells*ncellp, &
                  evpatype,zmax ,mass, ionization_potential, work_function)
            end if
        end if
    end if ! evpintval /= 0

    istep=istep+1
    time_fs=time_fs+deltat_fs
    time_thisrec=time_thisrec+deltat_fs

    if (movietime_val) then
       vir_istep_new = istep
    endif

    if ((calc_avgvir .eqv. .true.) .and. (moviemode == 17 .or. moviemode == 18)) then
       call calc_tot_vel(box,atype,delta,natoms,x1,x1_tot_x,x1_tot_y,x1_tot_z)
    endif

    if (calc_avgvir) then
       if (moviemode == 5 .or. moviemode == 6) then
          call calc_avg_diag_vir(box,wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,atomindex)
       else if (moviemode == 15 .or. moviemode == 16 .or. moviemode == 17 .or. moviemode == 18) then
          call calc_avg_nondiag_vir(box,atype,delta,x1,x1_tot_x,x1_tot_y,x1_tot_z, &
                                    wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,        &
                                    wxyiavg,wxyi,wxziavg,wxzi,                     &
                                    wyxiavg,wyxi,wyziavg,wyzi,                     &
                                    wzxiavg,wzxi,wzyiavg,wzyi,                     &
                                    moviemode,vir_help2,atomindex)
       endif
    endif

    !     OUTPUT OF MOVIE OR MOVIE SLICE
    IF(debug)PRINT *,'Pair_table & shuffle update for write',myproc

    ! 19.3.2013 Changes
    !we can only call movietime and slicetime once per iteration as they save their state
!    movietime_val=movietime(istep,nmovie,time_fs,tmovie,dtmovie,tmax,restartt)
!    slicetime_val=slicetime(istep,time_fs,tslice,dtslice) 
!    if (  movietime_val .or. &
!         ( slicetime_val .and. &
!         (dslice(1)>=0.0.or.dslice(2)>=0.0.or.dslice(3)>=0.0))) then
       ! Shuffle atoms between nodes; this is necessary here to make sure atoms are in their processor box 
       ! when writing out system to disk (in order to enable porper reading)
!       if (nprocs > 1) then
!          call Shuffle(x0,x1,x2,x3,x4,x5,x0nei,                           &
!               atomindex,atype,natoms,pbc,nngbrproc,printnow)
!
!          call Pair_Table(npairs,nborlist, natoms,x0,                        &
!               cut_neisq,box,pbc,x0nei,                                      &
!               passlimmin,passlimmax,atpassflag,npass,nngbrproc,             &
!               atomindex,AnySemi,SortNeiList,printnow)
!       end if
!    end if
    t1=mpi_wtime()
    IF(debug)PRINT *,'Movie',myproc
    if (movietime_val) then
       if (moviemode /= 9) then
          IF(iprint)WRITE(6,'(A,A,F11.2)') ' ',                                    &
               'Saving positions to md.movie at t=',time_fs

          if (moviemode == 5 .or. moviemode == 6 .or. moviemode == 15 .or. moviemode == 16 .or. &
              moviemode == 17 .or. moviemode == 18) then
             if (calc_avgvir) then
                if (moviemode == 5 .or. moviemode == 6) then
                   call avg_calc_diag_vir(wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,vir_istep_new,vir_istep_old,atomindex)
                else
                   call avg_calc_nondiag_vir(wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,        &
                                             wxyiavg,wxyi,wxziavg,wxzi,                     &
                                             wyxiavg,wyxi,wyziavg,wyzi,                     &
                                             wzxiavg,wzxi,wzyiavg,wzyi,                     &
                                             vir_istep_new,vir_istep_old,atomindex)
                endif
                vir_istep_old = vir_istep_new
             else
                if (moviemode == 17 .or. moviemode == 18) then
                   call calc_tot_vel(box,atype,delta,natoms,x1,x1_tot_x,x1_tot_y,x1_tot_z)
                endif
                if (moviemode == 5 .or. moviemode == 6) then
                   call calc_diag_vir(box,wxxi,wyyi,wzzi)
                endif
                if (moviemode == 15 .or. moviemode == 16 .or. moviemode == 17 .or. moviemode == 18) then
                   call calc_nondiag_vir(box,atype,delta,x1,x1_tot_x,x1_tot_y,x1_tot_z, &
                                         wxxi,wyyi,wzzi,wxyi,wxzi,wyxi,wyzi,wzxi,wzyi,  &
                                         moviemode,vir_help2)
                endif
             endif
          endif

          call Movie(x0,x1,xnp,xq,delta,atype,atomindex,natoms,box,istep,    &
               time_fs,P,Fp,Epair,Ethree,Ekin,wxxi,wyyi,wzzi,             &
               wxyi,wxzi,wyxi,wyzi,wzxi,wzyi,	  &
               AnySemi,moviemode,EAM,	&
               outtype,outzmin,outzmax,outzmin2,outzmax2,virsym,virkbar,virboxsiz)

          if (calc_avgvir) then
             if (moviemode == 5 .or. moviemode == 6) then
                call zero_avg_diag_vir(wxxiavg,wyyiavg,wzziavg,atomindex)
             else
                call zero_avg_nondiag_vir(wxxiavg,wyyiavg,wzziavg,wxyiavg,wxziavg,   &
                                          wyxiavg,wyziavg,wzxiavg,wzyiavg,atomindex)
             endif
          endif

       else if ((AnySemi .eqv. .true.) .and. (EAM .eqv. .false.)) then
          IF(iprint)WRITE(6,'(A,A,F11.2)') ' ',                                    &
               'Saving binary atom data at t=',time_fs
           
          call binarysave(1,myatoms,istep,atype,atomindex,               &
               time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
               Epair,Ethree,Ekin,delta,.false.)
       else
          IF(iprint)WRITE(6,'(A,A,F11.2)') ' ',                                    &
               'Saving binary atom data at t=',time_fs
           
          call binarysave(1,myatoms,istep,atype,atomindex,               &
               time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
               Epair,Fp(:,1),Ekin,delta,.false.)
       endif
       IF(iprint)WRITE(6,*)' ... Done !'
    endif

    IF(debug)PRINT *,'Slice',myproc

    if ((dslice(1)>=0.0.or.dslice(2)>=0.0.or.dslice(3)>=0.0).and.         &
         slicetime_val) then
       call mpi_barrier(mpi_comm_world, ierror)
       if (slicemode /= 9) then    
          IF(iprint)WRITE(6,'(A,F11.2)')                                              &
               'Printing slice region at t=',time_fs
           
          call Slice_Movie(x0,x1,x2,delta,atype,Ekin,box,atomindex,          &
               dslice,ECM,istep,time_fs,slicemode)
          IF(iprint)WRITE(6,*)' ... Done !'
       else 
          if ((AnySemi .eqv. .true.) .and. (EAM .eqv. .false.)) then
             IF(iprint)WRITE(6,'(A,A,F11.2)') ' ',                                    &
                  'Saving binary atom data at t=',time_fs
              
             call binarysave(2,myatoms,istep,atype,atomindex,               &
                  time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
                  Epair,Ethree,Ekin,delta,.true.,dslice,ECM)
          else
             IF(iprint)WRITE(6,'(A,A,F11.2)') ' ',                                    &
                  'Saving binary atom data at t=',time_fs
              
             call binarysave(2,myatoms,istep,atype,atomindex,               &
                  time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
                  Epair,Fp(:,1),Ekin,delta,.true.,dslice,ECM)
          endif
       end if
    end if
    tmr(9)=tmr(9)+(mpi_wtime()-t1)

    dsum_array(1)=Vpair
    dsum_array(2)=Vmany
    dsum_array(3)=trans
    dsum_array(4)=transin
    dsum_array(5)=transbrdr
    dsum_array(6:8)=transv(:)

    call my_mpi_dsum(dsum_array, 8)
    Vpair=dsum_array(1)
    Vmany=dsum_array(2)
    trans=dsum_array(3)
    transin=dsum_array(4)
    transbrdr=dsum_array(5)
    transv(:)=dsum_array(6:8)


    isum_array(1)=natomsin      
    isum_array(2)=nbrdr
    call my_mpi_isum(isum_array, 2)      
    natomsin=isum_array(1)      
    nbrdr=isum_array(2)      


    ! Modifications here by Heikki Ristolainen 20.5.2008
    ! This sums the forces acting on the atoms of atype=sumatype

    if (forcesum==1) then
     if (istep==1 .or. mod(istep,ndump*10)==0) then
       
       sumfx=0.0d0
       sumfy=0.0d0
       sumfz=0.0d0
       nsummed=0

       do i=1,myatoms
          i3=3*i-3
          if (atype(i)==sumatype) then
             sumfx=sumfx+xnp(i3+1)*box(1)
             sumfy=sumfy+xnp(i3+2)*box(2)
             sumfz=sumfz+xnp(i3+3)*box(3)
             nsummed=nsummed+1
          endif
       enddo

       call my_mpi_dsum(sumfx, 1)
       call my_mpi_dsum(sumfy, 1)
       call my_mpi_dsum(sumfz, 1)
       call my_mpi_isum(nsummed, 1)

       if (time_fs < 1d8 .and. istep < 100000000) then
          IF(iprint)WRITE(7,1033) istep,time_fs,natoms,sumfx,                            &
               sumfy,sumfz,nsummed
          IF(iprint)WRITE(6,1033) istep,time_fs,natoms,sumfx,                                &
               sumfy,sumfz,nsummed
       else 
          IF(iprint)WRITE(7,1034) istep,time_fs,natoms,sumfx,                            &
               sumfy,sumfz,nsummed
          IF(iprint)WRITE(6,1034) istep,time_fs,natoms,sumfx,                                &
               sumfy,sumfz,nsummed
       endif
       
     endif
    endif


    ! Correct particle positions (periodic boundary conditions)

    IF(debug)PRINT *,'Correct',myproc
    t1=mpi_wtime()
    call Correct(x0,x1,x2,x3,x4,x5,atype,xnp,pbc,deltas,x0nei,            &
         box,mtemp,Fzmaxz,FzmaxYz,dydtmaxz)
    tmr(8)=tmr(8)+(mpi_wtime()-t1)


    !
    ! KN Calculate new time step
    !

    IF(debug)PRINT *,'Get Tstep',myproc,vmax,vmaxnosput

    call Get_Tstep(irec,vmaxnosput,Fmaxnosput,timekt,timeEt,timeCh,      &
         deltat_fs,deltamax_fs,delta,deltas,deltaratio,rstmax)

    if (irec /= 0 .and. MOD(istep,10*ndump)==0) then
       IF(iprint)WRITE (6,30) deltat_fs,Emaxnosput,vmaxnosput,Fmax
30     format ('Deltat ',G13.5,' fs, Emax vmax Fmax',3G12.5)
    endif

    !
    !  Correct x1 and x2 units for the new time step (!!)
    !
    help2=deltaratio*deltaratio
    help3=deltaratio*help2
    help4=deltaratio*help3
    help5=deltaratio*help4
    do i=1,3*myatoms
       x1(i)=x1(i)*deltaratio
       x2(i)=x2(i)*help2
       x3(i)=x3(i)*help3
       x4(i)=x4(i)*help4
       x5(i)=x5(i)*help5
    enddo

    poten=Vmany+Vpair

    if (pscale>0) then
       call ConserveTotalMomentum(x1,natoms,pscale,delta,atype,box)
    endif

    !Remove total angular momentum every 'remrot' time steps
    if (remrot>0) then
       if (MOD(istep,remrot)==0) then
          call RemoveAngularMomentum(x0,x1,delta,natoms,atype,box)
       endif
    endif

    avgpot=avgpot+poten
    avgkin=avgkin+trans
    if (nfixed /= natoms) avgkinmoving=avgkinmoving+trans/(natoms-nfixed)*natoms
    avgenergy=avgenergy+poten+trans

    avgp(1)=avgp(1)+wxx/dh
    avgp(2)=avgp(2)+wyy/dh
    avgp(3)=avgp(3)+wzz/dh
    isteppcalc=isteppcalc+1
    pt=eV_to_kbar*avgkin*two/three/isteppcalc/dh

    if (istep==1 .or. MOD(istep,ndump*10)==0) then
       help(1)=Elosssum(1); help(2)=Elosssum(2);
       call my_mpi_dsum(help, 2)

       IF(iprint)WRITE(6,398) 'btcloss at',time_fs,' borders:',                    &
            help(1),' overall: ',help(2)
398    format (A,F12.2,A,F12.3,A,F12.3)
       IF(iprint)WRITE(6,'(A,F12.3,A,F12.3,A,F12.3)') 'At t',time_fs,             &
            ' Ekin outside timesputlim',trans-transin,' inside',transin
    endif
    IF(debug)PRINT *,'avep',isteppcalc
    if (MOD(istep,ndump*100)==0) then
       help(1)=avgp(1)
       help(2)=avgp(2)
       help(3)=avgp(3)
       call my_mpi_dsum(help, 3)
       pressure=(help(1)*boxs(1)+help(2)*boxs(2)+                         &
            help(3)*boxs(3))/three/isteppcalc
       Pave=1602*pressure+pt
       IF(iprint)WRITE(6,997)' Average pressure (kBar):  ',                        &
            Pave,' over',isteppcalc,' steps'
       if (tmodtau/=0.0.and.time_fs>10*ABS(tmodtau)) then
          IF(iprint)WRITE(6,399) 'time ',time_fs,' Average Tmoving (K):',                &
               avgkinmoving/isteppcalc/natoms/1.5*invkBeV
399       format(a,f12.2,a15,f12.4)
       endif
    endif


    call Press_Control(bpcbeta,bpctau,bpcmode,tmodtau,temp,      &
         bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,wxx,wyy,wzz,Pxx,Pyy,Pzz,     &
         istep,deltat_fs,time_fs,transv,                         &
         box,boxs,box0,ncells,unitcell,dh,dhorig,dhprev,x1,x2,x3,x4,x5)

    if (iprint .and. (istep == 1 .or. MOD(istep,10*ndump) .eq. 0)) then
       write (6,401) 'bpc P',Pxx,Pyy,Pzz,' sz',box(1),box(2),box(3),dh
       write (7,401) 'bpc P',Pxx,Pyy,Pzz,' sz',box(1),box(2),box(3),dh
401    format (a,3f10.4,a,3f9.3,f14.2)
       if (tmodtau .ne. 0.0) then
          write (6,'(A,F11.2)') 'tmodtau desired T',temp*invkBeV
       endif
    endif

    ! Total energy of single particle=tote

    transmoving=zero; if (nfixed /= natoms) transmoving=trans/(natoms-nfixed)
    trans=trans/natoms
    if (natomsin>0) transin=transin/natomsin
    if (nbrdr>0) transbrdr=transbrdr/nbrdr
    poten=poten/natoms
    tote=trans+poten
    !     tempp = actual particle temperature FOR MOVING ATOMS
    tempp=two*transmoving/three
    energ=energ+tote


    if (istep == 1 .or. MOD(istep,ndump) == 0) then
       t1=mpi_wtime()
       if (firstprint) then
          IF(iprint)WRITE(6,*)'    Step     Time    Atoms ',                       &
               '     Temp           Epot_ave   Total En      Change'
          IF(iprint)WRITE(7,*)'    Step     Time    Atoms ',                       &
               '     Temp           Epot_ave   Total En      Change'

       endif
       if (firstprint) tote0=tote
       if (time_fs < 1d8 .and. istep < 100000000) then
          IF(iprint)WRITE(7,1030) istep,time_fs,natoms,                            &
               tempp*invkBeV,poten,tote,tote-tote0
          IF(iprint)WRITE(6,1030) istep,time_fs,natoms,                                &
               tempp*invkBeV,poten,tote,tote-tote0
       else
          IF(iprint)WRITE(7,1032) istep,time_fs,natoms,                            &
               tempp*invkBeV,poten,tote,tote-tote0
          IF(iprint)WRITE(6,1032) istep,time_fs,natoms,                                &
               tempp*invkBeV,poten,tote,tote-tote0
       endif

1030   format('ec ',i8,f12.2,i10,1x,f20.4,f12.4,f12.4,f11.4)
1031   format('ei ',i11,1x,f15.2,i10,1x,f13.4,f12.4,f12.4,f11.4)
1032   format('ec ',i11,1x,f15.2,i10,1x,f13.4,f12.4,f12.4,f11.4)
1033   format('forces ',i8,f12.2,i8,1x,g20.10,g20.10,g20.10,i10)
1034   format('forces ',i11,1x,f15.2,i8,1x,g20.10,g20.10,g20.10,i10)

       firstprint=.false.
       printnow=.true.
       if (istep==1 .or. MOD(istep,10*ndump) == 0) then
          ! print results for atoms inside only
          IF(iprint)WRITE(6,1031) istep,time_fs,natomsin,                           &
               two*transin/three*invkBeV,poten,transin+poten,              &
               transin+poten-tote0
       endif

       tmr(9)=tmr(9)+(mpi_wtime()-t1)
    endif


    if (nrestart > 0 .and. istep > 1 .and. MOD(istep,nrestart) == 0) then
       !        Shuffle atoms between nodes; this is necessary here to make sure atoms are in their processor box
       call mpi_barrier(mpi_comm_world, ierror)
       if (nprocs > 1) then
          call Shuffle(x0,x1,x2,x3,x4,x5,x0nei,                           &
               atomindex,atype,natoms,pbc,nngbrproc,printnow)

          call Pair_Table(npairs,nabors,nborlist,invnborlist, natoms,x0,                        &
               cut_neisq,box,pbc,x0nei,                                      &
               passlimmin,passlimmax,atpassflag,npass,nngbrproc,             &
               atomindex,AnySemi,BrennerBeardmore,SortNeiList,printnow)
       end if

       t1=mpi_wtime()
       IF(iprint)WRITE (6,'(A,F14.4,3F15.7)') 'Restart: t size',                      &
            time_fs,box(1),box(2),box(3)
       if (restartmode /= 9) then
          call Dump_Atoms(x0,x1,box,atomindex,                            &
               atype,P,Fp,Epair,Ethree,delta,.false.,AnySemi,wxxi2,wyyi2,wzzi2,EAM)
       else
          if ((AnySemi .eqv. .true.) .and. (EAM .eqv. .false.)) then
             call binarysave(3,myatoms,istep,atype,atomindex,               &
                  time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
                  Epair,Ethree,Ekin,delta,.false.)
          else
             call binarysave(3,myatoms,istep,atype,atomindex,               &
                  time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
                  Epair,Fp(:,1),Ekin,delta,.false.)
          end if
       end if
       tmr(9)=tmr(9)+(mpi_wtime()-t1)
    endif

    IF(debug)PRINT *,'----------------------',myproc
    IF(debug)PRINT *,' '

     
     

    firsttime=.false.

    tmr(99)=tmr(99)+(mpi_wtime()-tmain1)

    !     Check various ending criteria
    if (nsteps > 0 .and. istep .ge. nsteps) goto 998
    if (time_fs .ge. tmax) goto 999
    if (tempp .lt. endtemp) goto 999
    if (rsnum>0 .and. rsmode==1 .and. time_thisrec .ge. rstmax) goto 999

    goto 200
    !***********************************************************************
    !
    !          Main Loop Ends Here
    !
    !***********************************************************************



999 if (rsnum > 0 .and. rsmode==1) then
       if (rs_index < rsnum) goto 100
    endif

998 continue
    !***********************************************************************
    !
    !               Loop over Recoil Events Ends Here
    !
    !***********************************************************************

    !     Finish main timer

    tmr(1)=mpi_wtime()-tmr(1)
    if (tmr(1) .le. zero) then
       tmr(1)=tmr(99)
    endif

    ! Print out final averages

    if (bpcbeta .ne. 0.0 .and. iprint) then
       write (6,401) 'bpc P',Pxx,Pyy,Pzz,                                 &
            ' sz',box(1),box(2),box(3),dh
       write (7,401) 'bpc P',Pxx,Pyy,Pzz,                                 &
            ' sz',box(1),box(2),box(3),dh
901    format (a,3f10.4,a,3f9.3,f11.2)
    endif

    if (time_fs < 1d8 .and. istep < 100000000) then
       IF(iprint)WRITE(7,1030) istep,time_fs,natoms,                               &
            tempp*invkBeV,poten,tote,tote-tote0
       IF(iprint)WRITE(6,1030) istep,time_fs,natoms,                               &
            tempp*invkBeV,poten,tote,tote-tote0
    else
       IF(iprint)WRITE(7,1032) istep,time_fs,natoms,                               &
            tempp*invkBeV,poten,tote,tote-tote0
       IF(iprint)WRITE(6,1032) istep,time_fs,natoms,                               &
            tempp*invkBeV,poten,tote,tote-tote0
    endif

    call mpi_barrier(mpi_comm_world, ierror)
    IF(iprint)WRITE(6,*)
    IF(iprint)WRITE(6,*) ' Done with main loop.'
    if (nmovie .gt. 0) then
       if (moviemode /= 9) then
          IF(debug)PRINT *,'Movie',myproc

          vir_istep_new = istep

          if ((movietime_val .eqv. .false.) .and. (moviemode == 17 .or. moviemode == 18)) then
             call calc_tot_vel(box,atype,delta,natoms,x1,x1_tot_x,x1_tot_y,x1_tot_z)
          endif

          if (moviemode == 5 .or. moviemode == 6 .or. moviemode == 15 .or. moviemode == 16 .or. &
              moviemode == 17 .or. moviemode == 18) then
             if (calc_avgvir) then
                if ((movietime_val .eqv. .false.) .and. (vir_istep_new /= vir_istep_old)) then 
                   if (moviemode == 5 .or. moviemode == 6) then
                      call avg_calc_diag_vir(wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,vir_istep_new,vir_istep_old,atomindex)
                   else
                      call avg_calc_nondiag_vir(wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi,        &
                                                wxyiavg,wxyi,wxziavg,wxzi,                     &
                                                wyxiavg,wyxi,wyziavg,wyzi,                     &
                                                wzxiavg,wzxi,wzyiavg,wzyi,                     &
                                                vir_istep_new,vir_istep_old,atomindex)
                   endif  
                endif
                vir_istep_old = vir_istep_new
             else if (movietime_val .eqv. .false.) then
                if (moviemode == 5 .or. moviemode == 6) then
                   call calc_diag_vir(box,wxxi,wyyi,wzzi)
                else
                   call calc_nondiag_vir(box,atype,delta,x1,x1_tot_x,x1_tot_y,x1_tot_z, &
                                         wxxi,wyyi,wzzi,wxyi,wxzi,wyxi,wyzi,wzxi,wzyi,  &
                                         moviemode,vir_help2)               
                endif
             endif
          endif

          call Movie(x0,x1,xnp,xq,delta,atype,atomindex,natoms,box,istep,    &
               time_fs,P,Fp,Epair,Ethree,Ekin,wxxi,wyyi,wzzi,             &
               wxyi,wxzi,wyxi,wyzi,wzxi,wzyi,	  &
               AnySemi,moviemode,EAM,	&
               outtype,outzmin,outzmax,outzmin2,outzmax2,virsym,virkbar,virboxsiz)

          if (calc_avgvir) then
             if (moviemode == 5 .or. moviemode == 6) then
                call zero_avg_diag_vir(wxxiavg,wyyiavg,wzziavg,atomindex)
             else
                call zero_avg_nondiag_vir(wxxiavg,wyyiavg,wzziavg,wxyiavg,wxziavg,   &
                                          wyxiavg,wyziavg,wzxiavg,wzyiavg,atomindex)
             endif
          endif

          IF(iprint)WRITE(6,*)' Done saving positions to md.movie.'
          close(10)
       else if ((AnySemi .eqv. .true.) .and. (EAM .eqv. .false.)) then
          call binarysave(1,myatoms,istep,atype,atomindex,               &
               time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
               Epair,Ethree,Ekin,delta,.false.)
          IF(iprint)WRITE(6,*)' Saved all binary atom data.'
       else
          call binarysave(1,myatoms,istep,atype,atomindex,               &
               time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
               Epair,Fp(:,1),Ekin,delta,.false.)
          IF(iprint)WRITE(6,*)' Saved all binary atom data.'
       endif
    endif

    if (iprint .and. nliqanal .gt. 0) then
       close(21)
    endif

    IF(iprint)CLOSE(17)
    if (irec .gt. 0) then
       IF(iprint)CLOSE(22)
    endif


    if (nrestart >= 0) then
       IF(iprint)WRITE (6,'(A,F14.4,3F15.7)') 'Restart: t size',                   &
            time_fs,box(1),box(2),box(3)

       if (restartmode /= 9) then
          call Dump_Atoms(x0,x1,box,atomindex,                               &
               atype,P,Fp,Epair,Ethree,delta,.true.,AnySemi,wxxi2,wyyi2,wzzi2,EAM)
       else
          if ((AnySemi .eqv. .true.) .and. (EAM .eqv. .false.)) then
             call binarysave(3,myatoms,istep,atype,atomindex,               &
                  time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
                  Epair,Ethree,Ekin,delta,.false.)
          else
             call binarysave(3,myatoms,istep,atype,atomindex,               &
                  time_fs,deltat_fs,box,x0,x1,wxxi2,wyyi2,wzzi2,                &
                  Epair,Fp(:,1),Ekin,delta,.false.)
          end if
       end if

       IF(iprint)WRITE(6,*)' Done writing out atoms.out and mdlat.out'
       IF(iprint)WRITE(6,*)
        
    endif

    call my_mpi_dsum(avgp, 3)
    avgp(1)=avgp(1)*boxs(1)
    avgp(2)=avgp(2)*boxs(2)
    avgp(3)=avgp(3)*boxs(3)

    help(1)=Elosssum(1); help(2)=Elosssum(2);
    call my_mpi_dsum(help, 2)

    if (iprint) then
       if (isteppcalc > 0) then
          factor=one/natoms/isteppcalc
          avgtemp=avgkin/1.5*factor
          avgtempmoving=avgkinmoving/1.5*factor
          avgkin=avgkin*factor
          pt=(natoms*1602*avgtemp)/dh

          write(7,*) 

          write(6,990)' Final potential energy:  ',poten*natoms
          write(7,990)' Final potential energy:  ',poten*natoms
          write(7,990)' Average total energy:    ',avgenergy*factor
          write(7,990)' Average kinetic energy:  ',avgkin
          write(7,990)' Average potential energy:',avgpot*factor
          write(7,990)' Average temperature (K): ',avgtemp*invkBeV
          write(7,990)' Average temp_moving (K): ',avgtempmoving*invkBeV
          write(7,991)' Total electronic stop.:  ',FDesum(1),FDesum(2)
          write(6,991)' Total electronic stop.:  ',FDesum(1),FDesum(2)

          write(6,991)' Total btc energy losses: ',help(1),help(2)   
          write(7,991)' Total btc energy losses: ',help(1),help(2)   

          write(7,*)


          write(7,990)' Average sigma_xx (kBar):  ',                      &
               1602*avgp(1)/isteppcalc+pt
          write(7,990)' Average sigma_yy (kBar):  ',                      &
               1602*avgp(2)/isteppcalc+pt
          write(7,990)' Average sigma_zz (kBar):  ',                      &
               1602*avgp(3)/isteppcalc+pt
          pressure=(avgp(1)+avgp(2)+avgp(3))/three/isteppcalc
          write(7,997)' Average pressure (kBar):  ',                      &
               1602*pressure+pt,' over',isteppcalc,' steps'

          write(6,990)' Average temperature (K): ',avgtemp*invkBeV
          write(6,990)' Average temp_moving (K): ',avgtempmoving*invkBeV
          write(6,990)' Average sigma_xx (kBar):  ',                      &
               1602*avgp(1)/isteppcalc+pt
          write(6,990)' Average sigma_yy (kBar):  ',                      &
               1602*avgp(2)/isteppcalc+pt
          write(6,990)' Average sigma_zz (kBar):  ',                      &
               1602*avgp(3)/isteppcalc+pt
          write(6,997)' Average pressure (kBar):  ',                      &
               1602*pressure+pt,' over',isteppcalc,' steps'
       else
          write (6,*) 'No P obtained, simulate longer'
       endif

        
       IF(iprint)CLOSE(7)
    endif

990 format(' ',a30,f15.6)
991 format(' ',a30,f15.6,1x,f15.6)
993 format(' Average sigma_xx (kBar):  ',f12.6)
994 format(' Average sigma_yy (kBar):  ',f12.6)
995 format(' Average sigma_zz (kBar):  ',f12.6)
996 format(' Average pressure (kBar):  ',f12.6)
997 format(' ',a30,f15.6,a,i10,a)


    !     Output timing information

    if (iprint .and. (tmr(1) /= 0.0d0) ) then
       open(9,file='out/time',status='replace')
       write(9,*)'****************************************************'
       dnum=100.0d0/tmr(1)
       denom=DBLE(istep)/1000.0d0
       tmr(10)=tmr(1)
       do i=2,14
          if (i .ne. 10) tmr(10)=tmr(10)-tmr(i)
       enddo
       tmr(10)=tmr(10)-tmr(31)
       commtime=tmr(15)+tmr(16)+tmr(17)+tmr(33)

       write(9,1023)natoms,nprocs,nnodes(1),nnodes(2)
       write(9,*)
       ihours=INT(tmr(1)/3600)
       imin=INT((tmr(1)-ihours*3600.0)/60)
       tsec=tmr(1)-ihours*3600.0-imin*60

       write(9,2209) ihours,imin,tsec
2209   format(/,10x,i5,' hours  ',i2,' mins  ',f5.2,' secs')

       write(9,1020)tmr(1)/denom,tmr(1)/(60*denom)
       write(9,*)
       write(9,1021)dnum*tmr(2),'temperature control  ',tmr(2)/denom
       write(9,1021)dnum*tmr(3),'coordinate prediction',tmr(3)/denom
       write(9,1021)dnum*tmr(4),'pair table evaluation',tmr(4)/denom
       if (tmr(4) > 0) then
          write(9,1024) 100*tmr(24)/tmr(4),'linkcell',tmr(24)/denom
          write(9,1024) 100*tmr(25)/tmr(4),'getpairs',tmr(25)/denom
       endif
       if (nprocs .gt. 1 .and. tmr(4) .gt. 0.0) then
          write(9,1022)100*tmr(16)/tmr(4),tmr(16)/denom
       endif

       write(9,1021)dnum*tmr(5),'calc charge density  ',tmr(5)/denom
       if (nprocs .gt. 1 .and. tmr(5) .gt. 0.0) then
          write(9,1022)100*tmr(15)/tmr(5),tmr(15)/denom
       endif
       write(9,1021)dnum*tmr(6),'calc embedding energy',tmr(6)/denom

       write(9,1021)dnum*tmr(7),'calc EAM forces      ',tmr(7)/denom
       if (EAM .and. nprocs .gt. 1 .and. tmr(7).gt.0.0) then
          write(9,1022)100*tmr(17)/tmr(7),tmr(17)/denom
       endif

       if (tmr(31) > 0.0) then
          write(9,1021) dnum*tmr(31),'calc Semicon force  ',tmr(31)/denom
          write(9,1024) 100*tmr(32)/tmr(31),'reppot',tmr(32)/denom
          write(9,1024) 100*tmr(33)/tmr(31),'communication',tmr(33)/denom
       endif

       write(9,1021)dnum*tmr(8),'coordinate correction',tmr(8)/denom
       write(9,1021)dnum*tmr(11),'shuffle among nodes',tmr(11)/denom
       write(9,1021)dnum*tmr(12),'getcascproperties',tmr(12)/denom
       write(9,1021)dnum*tmr(13),'analyzeint',tmr(13)/denom
       write(9,1021)dnum*tmr(14),'elstop subtraction',tmr(14)/denom
       write(9,1021)dnum*tmr(9),'main loop output     ',tmr(9)/denom
       write(9,1021)dnum*tmr(10),'other main loop time ',tmr(10)/denom
       write(9,*)
       if (nprocs .gt. 1) then
          write(9,1021)dnum*(commtime),                                     &
               'overall communication',                                    &
               (commtime)/denom

          write(9,*)
       endif
       write (9,1028) natoms,istep,tmr(1),tmr(1)/natoms/istep
       write (6,1028) natoms,istep,tmr(1),tmr(1)/natoms/istep
       close(9)
    endif

1020 format(f10.3,' ms per time step',f8.2,' min/1000 steps')
1021 format(f7.2,'% of time spent in ',a21,f10.3,' ms/time-step')
1022 format(10x,f7.2,'% communication ',f10.3)
1023 format(/,i10,' atoms on ',i4,' nodes ',i4,' cols by ',i4,' rows')
1024 format(10x,f7.2,'% of that in ',a15,f10.3,' ms/time-step')
1028 format(/,'For',i8,' atoms and',i9,' steps used',f10.2,' s =>',        &
         e9.4,' s/step/nat')

    if (nprocs .gt. 1) then
       call mpi_barrier(mpi_comm_world, ierror)
       call mpi_finalize(ierror)

    endif

    if (iprint .and. zipout > 0) then
      call system("tar czf out.tgz out")
    end if
  end program PARCAS
