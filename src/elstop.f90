  
  
  subroutine subelstop(i,x1,Ekin,atype,boxs,delta,FDe,natoms,box,melstopst)

    use typeparam
    use my_mpi
  use defs  

    implicit none
    !
    !     Subtract elstop for one atom i in my node
    !
    !     NOTE: this routine is not necessarily called by all processors
    !
    !     Also sums up FDe, the total energy lost in electronic stopping
    !
    integer i
    real*8 x1(*),Ekin(*),boxs(3),box(3),delta(itypelow:itypehigh),FDe
    integer atype(*)

    integer i3,atypei,natoms,melstopst
    real*8 v,vSI,deltav,d,deltar
    real*8, external :: elstop

    real*8 E0,E1


    i3=i*3-3
    atypei=atype(i)
    d=delta(abs(atypei))
    E0=0.5*(x1(i3+1)*x1(i3+1)*boxs(1)+                                    &
         x1(i3+2)*x1(i3+2)*boxs(2)+                                       &
         x1(i3+3)*x1(i3+3)*boxs(3))/(d*d)

    if (E0 == 0.0d0) return;

    v=sqrt(2.0*E0)
    vSI=v*vunit(abs(atypei))*1.0d5

    ! Calculate deltar using x=vt for elstop straggling calculations
    deltar=v*delta(abs(atypei))

    deltav=delta(abs(atypei))*elstop(vSI,abs(atypei),natoms,box,melstopst,deltar)

    ! print *,i,atypei,v,vSI,deltav,delta(atypei)

    x1(i3+1)=x1(i3+1)*(1.0d0-deltav/v)
    x1(i3+2)=x1(i3+2)*(1.0d0-deltav/v)
    x1(i3+3)=x1(i3+3)*(1.0d0-deltav/v)

    E1=0.5*(x1(i3+1)*x1(i3+1)*boxs(1)+                                    &
         x1(i3+2)*x1(i3+2)*boxs(2)+                                       &
         x1(i3+3)*x1(i3+3)*boxs(3))/(d*d)

    Ekin(i)=E1

    FDe=FDe+E0-E1
    !      print *,i,E0,E1,v,vSi,elstop(vSI),deltav,FDe
     

    return 
  end subroutine subelstop

  ! ------------------------------------------------------------------------

  real*8 function elstop(vel,itype,natoms,box,melstopst,deltar)

    use typeparam
    use my_mpi

    use defs
    use para_common

    use random

    implicit none
    !
    !     PARCAS version of elstop subraction. Code taken from old
    !     MOLDY range code, based on original implementation by Kai Nordlund.
    !
    !     NOTE: this routine is not necessarily called by all processors
    !
    !     Returns the electronic stopping in eV/A for an input velocity in m/s
    !     
    !     When called for the first time, the subroutine reads the electronic
    !     stopping power from elstop.in into the internal arrays V and S
    !     V should contain velocities in m/s, S the stopping power in eV/A.
    !      
    !     File name convention: elstop.element(type).in
    !                           if itype==1 and ntype==1, elstop.in possible
    !
    !     The argument vel should be in m/s !
    !     If vel is too large for elstop file warning is printed
    !
    !     The subroutine returns the stopping power in eV/A by means of a linear
    !     interpolation between the points in the arrays V and S.
    !

!include 'para_common.f90'


    real*8 vel,box(3)
    integer itype,natoms,melstopst

    real*8, allocatable, save :: V(:,:),S(:,:)
    integer, allocatable, save :: nmax(:)
    integer i
    integer, save :: ul,ll
    data ll /1/
    data ul /2/
    logical, save :: firsttime = .true.
    logical, allocatable, save :: firsttime_type(:)
    integer :: stopsize = 500
    character*40 filename

    !
    !     Elstop straggling parameters
    !        
    !      integer esseed            ! It's better not to mess up the other 
    !      data esseed /345662/      ! seed, so let's use an own.
    !      real*8 deltar,omega
    !      real*8 sdr
    !      data sdr /0.0/
    !      integer ntnext
    !      data ntnext /0/

    real*8 partdens,deltar,unitl,om,gd
    real*8, save :: effectiveZ2,stragglingsum,stragglingn
    real*8, external :: LSSOmega

    if (firsttime) then
       firsttime=.false.
       print *,'elstop allocating'
       allocate(firsttime_type(itypelow:itypehigh))
       allocate(nmax(itypelow:itypehigh))
       do i=itypelow,itypehigh
          firsttime_type(i)=.true.
       enddo
       allocate(V(stopsize,itypelow:itypehigh))
       allocate(S(stopsize,itypelow:itypehigh))
       if (melstopst == 1) then
          IF(iprint)WRITE(6,*) '*** Including straggling of electronic stopping in elstop calcs.'
          stragglingsum=zero
          effectiveZ2=zero
          do i=itypelow,itypehigh
             if (noftype(i)>0) then
                if (atomZ(i)<=zero) then
                   print *,'ERROR: elstop straggling needs Z for all atom types'
                   call my_mpi_abort('elstop straggling needs Z2',i)
                endif
                effectiveZ2=noftype(i)*atomZ(i)
             endif
          enddo
          effectiveZ2=effectiveZ2/natoms
          IF(iprint)WRITE(6,*) ' Elstop straggling calculation using effective Z2',effectiveZ2
       endif
    endif

    if (firsttime_type(itype)) then
       firsttime_type(itype)=.false.

       write(unit=filename,fmt='(A10,A,A1,A,A3)')                         &
            'in/elstop.',trim(element(itype))                             &
            ,'.',trim(substrate),'.in'
       open(9,file=filename,status='old',err=4)
       goto 10
4      if (itype==1 .and. ntype==1) then
          filename='in/elstop.in'
          open(9,file=filename,status='old',err=19)
       else
          goto 19
       endif

10     i=1
11     read(9,*,end=12,err=19) V(i,itype),S(i,itype)
       ! print *,'es read',V(i,itype),S(i,itype)
       i=i+1
       if (i .ge. stopsize) then
          write (6,*) 'Elstop array too big',stopsize
          call my_mpi_abort('elstop array size', int(myproc))
       endif
       goto 11
12     close(9)
       nmax(itype)=i-1

       write (6,'(A,I3,A,I3,A)')                                          &
            '*** first time in elstop in proc ',myproc,', ',              &
            nmax(itype),' data points read in'
       write (6,'(A,I2,1X,A,A,1X,A,G13.5)')                               &
            '*** Elstop atom type and file',itype,                        &
            trim(element(itype)),filename,', v=',vel
        

    endif
    goto 20
19  write(6,*) 'Elstop file open/read error. Exiting',filename
    call my_mpi_abort('elstop read', int(myproc))

20  continue
    elstop=0.0

    if (vel > V(nmax(itype),itype)) then
       write (6,*) 'Velocity out of range for type',itype
       write (6,*) 'Velocity is',vel,' max vel',V(nmax(itype),itype)
       write (6,*) 'proc',myproc,' nmax',nmax(itype)
       do i=1,nmax(itype)
          write (6,'(F13.3,2X,F13.3)') V(i,itype),S(i,itype)
       enddo
       elstop=S(nmax(itype),itype)
       return
    endif

    !
    !  The potential need not be with even intervals in V, but it should be ordered
    !  by increasing V. Therefore, binary search is the fastest way to locate vel:
    !  But first check whether we are in the right range already.

    if (ll >= nmax(itype)) then
       ll=1; ul=2;
    endif

    if (.not.(ul==ll+1.and.vel>V(ll,itype).and.vel<V(ul,itype))) then
       ul=nmax(itype)
       ll=1
30     i=(ul-ll)/2
       if (vel .lt. V(ll+i,itype)) then
          ul=ll+i
       else
          ll=ll+i
       endif
       if (ul-ll > 1) goto 30
    endif
    if (ul<=ll .or. ul > nmax(itype)) then
       print *,'elstop',ll,ul,itype,nmax(itype),vel
       call my_mpi_abort('ELSTOP HORROR', int(ul))
    endif
    !
    !  And now for the linear interpolation !
    !
    elstop=(S(ul,itype)-S(ll,itype))/                                     &
         (V(ul,itype)-V(ll,itype))*(vel-V(ll,itype))+S(ll,itype)

    !
    !     Elstop straggling stuff
    !


    if (melstopst == 1) then  ! straggling version
       partdens=1.0d0*natoms/(box(1)*box(2)*box(3))
       unitl=1d-10;
       if (atomZ(itype) <= zero) then
          print *,'ERROR: elstop straggling needs Z1 for all recoil atom types'
          call my_mpi_abort('elstop straggling needs Z',i)
       endif
       om=LSSOmega(atomZ(itype),effectiveZ2,vel,partdens,deltar,unitl)
       gd=gasdev(0)

       elstop=elstop+om/deltar*gd
       stragglingsum=stragglingsum+abs(om/deltar*gd)
       stragglingn=stragglingn+1.0d0
       if (mod(int(stragglingn),1000) == 1) then
          IF(iprint)WRITE(6,*) 'Latest and |average| elstop straggling',om/deltar*gd,om,stragglingsum/stragglingn
       endif
       !write (10,*) elstop,om,deltar,partdens,dr
    endif

    return
  end function elstop

  !-----------------------------------------------------------------------------
  !
  !     STRAGGLING version (23.6.1993 Kai Nordlund), upgraded to parcas 7.12.2011
  !
  !     The straggling of the electronic stopping is taken in account using
  !     the model presented in A. Luukkainen and M. Hautala, Rad. Eff. 59 (1982).
  !
  !  This means a gaussian distribution of the stopping,
  !
  !  S = S_mean+ omega/dr*gasdev(0,1),
  !
  !  where dr is the movement of the atom since the calculation was last done,
  !  S_mean is the expectation value of the straggling taken from the elstop file,
  !  and omega is a complex function of esZ1,esZ2,vel, the atomic density and dr.
  !
  !  The length of dr should not affect the final result, due to purely mathematical
  !  reasons.
  !

  !

  real*8 function LSSOmega(Z1,Z2,vel,N,deltar,unitl)
  use defs  
    implicit none

    !  vel in SI units !
    !  Here N stands for particle density

    real*8 x,N,omegaB,L,Z1,Z2,vel,deltar,unitl
    real*8 :: Bohrvel=2187700.0   ! SI units 
    real*8 :: e=1.602189e-19,eps0=8.854188e-12,PI=3.14159265358979

    ! This version corresponds exactly to MDRANGE
    ! based on equation (4) in Hautala, Rad. Eff. 59 (1982) 113

    ! N * deltar is in units of 1/A^2, i.e. correct for this to get SI 
    !print *,Z1,Z2,N,deltar,unitl
    omegaB=Z1*e/(4*PI*eps0)*sqrt(4.0*PI*Z2*N*deltar/(unitl*unitl))

    x=vel**2/(Bohrvel**2*Z2)   ! divide with Bohr velocity

    L=1.0d0
    if (x .lt. 2.3) L=0.5*(1.36*sqrt(x)-0.016*sqrt(x*x*x))
    LSSOmega=omegaB*sqrt(L)
    !print *,vel,LSSomega,x,L

    return
  end function LSSOmega


  !-----------------------------------------------------------------------------
