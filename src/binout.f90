module binout
  use my_mpi
  use datatypes
  use defs  
  implicit none
  public

  integer, parameter :: fileversion=4  !what fileversion does this support 
  ! Integer and real types used.

  ! Prototype values.

  real(kind=real32b)   ::r4
  real(kind=real32b),dimension(2)::r42
  real(kind=real64b)   ::r8
  integer(kind=int32b) ::i4
  integer(kind=int64b) ::i8
  character(len=4)     ::c4

  integer(kind=int32b), parameter :: protoInt  = z'11223344'
  real(kind=real32b),   parameter :: protoReal = 721409.0 / 1048576.0
  integer(kind=mpi_parameters), save :: rank  

  ! Record length for one 32-bit integer or real.
  integer, save              :: RECL32 = 0

  ! Per-atom fields.
  integer, parameter         :: maxFields = 11
  ! Atom and field caches (if fewer than maxField fields then actually more atoms can be handled)
  ! FIXME!!! The correct way is to read the fixed part of   
  ! input header and then allocate a buffer big enough.
  integer, parameter         :: maxAtom = 2000000
  ! FIXME!!! The correct way is to read the fixed part of   
  ! input header and then allocate a buffer big enough.
  ! 125 000 is large enough for current generation
  integer, parameter         :: maxCpus = 125000

  ! Atom type mapping. Limited by the file format.
  integer(kind=int32b), save :: typeSymbol(-128:127)
  !buffer for atom data
  real(kind=real32b),allocatable,dimension(:),save::buffer
  real(kind=real32b),allocatable,dimension(:),save::header

  !used to gather number of atoms on each proc and to compute offsets in file
  integer,allocatable,save::atomsperproc(:)
  integer*8,allocatable,save::offsets(:)

  double precision,allocatable,save :: rmnperproc(:, :)
  double precision,allocatable,save :: rmxperproc(:, :)

  ! Periodic boundary conditions?
  logical, save              :: periodic(1:3)

  ! .TRUE. if byte order has to be swapped while reading.
  logical, save :: swap_needed = .false.

  !describes  the fields to be saved and what the current frame number is for each group of files (e.g. restarts, movies,...)
  type :: filedescr

     character(len=22)    :: filename
     integer              :: realsize   ! Size of a floating-point value
     ! in bytes: 4 for floats,
     ! 8 for doubles
     integer              :: frame   
     integer              :: overwrite
     integer              :: fields 
     ! Field names saved in the file header.
     character(len=4)     :: fieldName(1:maxFields)
     character(len=4)     :: fieldUnit(1:maxFields)
     integer              :: saveEpot   ! +1
     integer              :: saveEkin   ! +2
     integer              :: saveVx     ! +4
     integer              :: saveVy     ! +8
     integer              :: saveVz     ! +16
     integer              :: saveWxx    ! +32
     integer              :: saveWyy    ! +64
     integer              :: saveWzz    ! +128
     ! File name and file name components.
  end type filedescr

  type(filedescr)::fds(5) !1...5 simultanseous outputs supported

contains

  ! Functions swap32 and swap64 swap the byte order of a word, which
  ! is needed for endian conversions. The code might look like a bit
  ! clumsy but it compiles to rather nice machine code. ;)

  pure function swap32(x) result(y)
    integer(kind=int32b), intent(in) :: x
    integer(kind=int32b) :: y

    y = IOR(IOR(ISHFT(x, -24), &
         ISHFT(IAND(x, z'00ff0000'), -8)), &
         IOR(ISHFT(IAND(x, z'0000ff00'), 8), &
         ISHFT(x, 24)))
  end function swap32

  pure function swap64(x) result(y)
    integer(kind=int64b), intent(in) :: x
    integer(kind=int64b) :: y

    y = IOR(IOR(IOR(ISHFT(x, -56), &
         ISHFT(IAND(x, z'00ff000000000000'), -40)), &
         IOR(ISHFT(IAND(x, z'0000ff0000000000'), -24), &
         ISHFT(IAND(x, z'000000ff00000000'), -8))), &
         IOR(IOR(ISHFT(IAND(x, z'00000000ff000000'), 8), &
         ISHFT(IAND(x, z'0000000000ff0000'), 24)), &
         IOR(ISHFT(IAND(x, z'000000000000ff00'), 40), &
         ISHFT(x, 56))))
  end function swap64

  pure function get_int32(x) result(y)
    real(kind=real32b), dimension(:), intent(in) :: x
    integer(kind=int32b) :: y

    y = TRANSFER(x(1), i4)
    if (swap_needed) then
       y = swap32(y)
    end if
  end function get_int32

  pure function get_int64(x) result(y)
    real(kind=real32b), dimension(:), intent(in) :: x
    integer(kind=int64b) :: y

    y = TRANSFER(x(1:2), i8)
    if (swap_needed) then
       y = swap64(y)
    end if
  end function get_int64

  pure function get_real32(x) result(y)
    real(kind=real32b), dimension(:), intent(in) :: x
    real(kind=real32b) :: y

    y = TRANSFER(get_int32(x), r4)
  end function get_real32

  pure function get_real64(x) result(y)
    real(kind=real32b), dimension(:), intent(in) :: x
    real(kind=real64b) :: y

    y = TRANSFER(get_int64(x), r8)
  end function get_real64

  ! Check record length (and do other sanity checks).
  ! Return .FALSE. if everything is OK.
  function invalidReclen()
    use defs  
    implicit none
    logical :: invalidReclen

    integer(kind=int32b) :: i32
    real(kind=real32b)   :: r32
    integer              :: r

    ! Initialize to keep picky compilers quiet.
    i32 = 0
    r32 = 0

    ! Assume failure, so RETURN is sufficient.
    invalidReclen = .true.

    ! Save the expected record length first.
    inquire(iolength=r) i32
    RECL32 = r

    ! Matches the real type?
    inquire(iolength=r) r32
    if (r /= RECL32) return


    ! Is i32 32-bit two's complement integer?
    i32 = -16909061
    if (BIT_SIZE(i32) /= 32) return
    if (IBITS(i32, 24, 8) /= 254) return
    if (IBITS(i32, 16, 8) /= 253) return
    if (IBITS(i32,  8, 8) /= 252) return
    if (IBITS(i32,  0, 8) /= 251) return

    ! Is IEEE 754 single precision r32?
    r32 = protoReal
    i32 = TRANSFER(r32, i32)
    if (i32 /= 1060118544 .and. &
         i32 /= 809439264  .and. &
         i32 /= 537935664  .and. &
         i32 /= 270544959) return

    ! Everything is all right.
    invalidReclen = .false.
    return 
  end function invalidReclen



  subroutine initFilesequence(fileid,modein,name,overwrite,realsize)
    use typeparam
    use defs  
    implicit none
    integer,intent(in) :: modein,fileid,realsize
    character(len=*),intent(in)::name
    integer,intent(in)::overwrite

    integer::mode

    if(fileid > SIZE(fds) .or. fileid<1) then
       call my_mpi_abort('Invalid fileid for binary output.', INT(fileid))
    end if

    fds(fileid)%overwrite=overwrite
    fds(fileid)%filename=name
    fds(fileid)%frame=0
    fds(fileid)%realsize=realsize

    ! Clear all fields.
    fds(fileid)%fields   = 0
    fds(fileid)%saveEpot = 0
    fds(fileid)%saveEkin = 0
    fds(fileid)%saveVx   = 0
    fds(fileid)%saveVy   = 0
    fds(fileid)%saveVz   = 0
    fds(fileid)%saveWxx  = 0
    fds(fileid)%saveWyy  = 0
    fds(fileid)%saveWzz  = 0

    ! Select which fields are to be saved (and where).

    if (modein < 0) then
       mode = NOT(0)       ! All
    else
       mode=modein
    end if

    if (IAND(mode, 1) /= 0) then
       fds(fileid)%fields  = fds(fileid)%fields + 1
       fds(fileid)%saveEpot= fds(fileid)%fields
       fds(fileid)%fieldName(fds(fileid)%fields) = "Epot"
       fds(fileid)%fieldUnit(fds(fileid)%fields) = "eV"
    end if

    if (IAND(mode, 2) /= 0) then
       fds(fileid)%fields  = fds(fileid)%fields + 1
       fds(fileid)%saveEkin= fds(fileid)%fields
       fds(fileid)%fieldName(fds(fileid)%fields) = "Ekin"
       fds(fileid)%fieldUnit(fds(fileid)%fields) = "eV"
    end if

    if (IAND(mode, 4) /= 0) then
       fds(fileid)%fields  = fds(fileid)%fields + 1
       fds(fileid)%saveVx  = fds(fileid)%fields
       fds(fileid)%fieldName(fds(fileid)%fields) = "V.x"
       fds(fileid)%fieldUnit(fds(fileid)%fields) = "A/fs"
    end if

    if (IAND(mode, 8) /= 0) then
       fds(fileid)%fields  = fds(fileid)%fields + 1
       fds(fileid)%saveVy  = fds(fileid)%fields
       fds(fileid)%fieldName(fds(fileid)%fields) = "V.y"
       fds(fileid)%fieldUnit(fds(fileid)%fields) = "A/fs"
    end if

    if (IAND(mode, 16) /= 0) then
       fds(fileid)%fields  = fds(fileid)%fields + 1
       fds(fileid)%saveVz  = fds(fileid)%fields
       fds(fileid)%fieldName(fds(fileid)%fields) = "V.z"
       fds(fileid)%fieldUnit(fds(fileid)%fields) = "A/fs"
    end if

    if (IAND(mode, 32) /= 0) then
       fds(fileid)%fields  = fds(fileid)%fields + 1
       fds(fileid)%saveWxx = fds(fileid)%fields
       fds(fileid)%fieldName(fds(fileid)%fields) = "W.xx"
       fds(fileid)%fieldUnit(fds(fileid)%fields) = "eV"
    end if

    if (IAND(mode, 64) /= 0) then
       fds(fileid)%fields  = fds(fileid)%fields + 1
       fds(fileid)%saveWyy = fds(fileid)%fields
       fds(fileid)%fieldName(fds(fileid)%fields) = "W.yy"
       fds(fileid)%fieldUnit(fds(fileid)%fields) = "eV"
    end if

    if (IAND(mode, 128) /= 0) then
       fds(fileid)%fields  = fds(fileid)%fields + 1
       fds(fileid)%saveWzz = fds(fileid)%fields
       fds(fileid)%fieldName(fds(fileid)%fields) = "W.zz"
       fds(fileid)%fieldUnit(fds(fileid)%fields) = "eV"
    end if
  end subroutine initFilesequence


  ! Initialize atom caches and do some sanity checking
  !
  subroutine initBinaryMode(perx, pery, perz)
    use typeparam
    use defs  
    implicit none
    integer(kind=mpi_parameters) ::cpu,cpus,err
    real*8  :: perx, pery, perz   ! 1.0d0 if periodic.
    integer :: i, n

    !these only need to be done once
    call MPI_Comm_size(MPI_COMM_WORLD,cpus,err)
    call MPI_Comm_rank(MPI_COMM_WORLD,cpu,err)   
    rank = cpu

    ! Verify proper record lengths.
    if (invalidReclen()) then
       call my_mpi_abort('Invalid integer and real types for binary output.', INT(cpu))
    end if
    ! This format can only support up to [+-]127 atom types.
    if (itypehigh > 127 .or. itypelow < -127) then
       call my_mpi_abort('Too many atom types for binary output.', INT(cpu))
    end if

    if(cpus .gt. maxCpus) then
       call my_mpi_abort('Too many cpus used, please increase maxCpus in binout.f90', INT(err))
    end if

    !allocate buffers
    allocate(buffer((5+maxfields)*maxAtom))
    allocate(header(28+2*maxfields+(itypehigh-itypelow)+13*maxCpus))

    allocate(atomsperproc(cpus))
    allocate(offsets(cpus))
    allocate(rmnperproc(1:3, cpus))
    allocate(rmxperproc(1:3, cpus))

    !FIXME these should be freed, or saved
    ! Periodic boundary conditions?
    periodic(1) = (perx > 0.99) .and. (perx < 1.01)
    periodic(2) = (pery > 0.99) .and. (pery < 1.01)
    periodic(3) = (perz > 0.99) .and. (perz < 1.01)
    return
  end subroutine initBinaryMode


  ! read a frame of atoms from a restart.
  ! number of CPUs can be the same or different from the restart
  subroutine binaryread(filename,readvelocity,append,totatoms,atoms,atype, aindex,  &
       coord, velocity,dslice,ECM)

    use typeparam

    use random

    use defs  
    implicit none
    character(len=*),intent(in)::filename
    integer,  intent(in)  :: readvelocity,append
    integer,  intent(inout) :: totatoms !total atoms on all procs
    integer,  intent(inout) ::  atoms !local atoms for proc
    integer,  intent(inout) ::  atype(*), aindex(*)
    real*8,   intent(inout) :: coord(*), velocity(*)

    real*8, intent(in),optional::  dslice(3),ECM(3)

    real(kind=real64b), dimension(3) :: scale_factor
    real(kind=real64b), dimension(3) :: tmprmn, tmprmx

    logical:: doslice
    integer :: u
    integer ::stat(MPI_STATUS_SIZE)
    integer*8 ::fileatoms
    integer ::filecpus,fileitypehigh,fileitypelow,fields,vxi,vyi,vzi,ekini,hi,i,j
    integer ::generateVelocities
    integer(kind=mpi_parameters) :: cpus,err
    integer,allocatable ::fileatomsperproc(:)
    integer :: readstrategy
    integer :: filerealsize
    integer*8:: offset
    real*8 ::filebox(3)
    real*8        :: squared(1:3)
    real*8        :: vscale(1:3,-16:15)
    real*8       :: dx,dy,dz

    double precision :: t1,t2,t3,t4,t5,avetime,maxtime
    double precision :: tmp
    double precision, allocatable :: filermn(:, :), filermx(:, :)

    logical:: useatom
    integer :: isDone
    integer :: nr_good
    integer, allocatable :: goodi(:)
    integer(kind=mpi_parameters), allocatable :: sizes(:)
    integer (kind=int64b), allocatable :: fileoffsets(:)
    integer(kind=mpi_parameters) :: fileview
    integer :: numReadAtoms,temp
    integer :: tempa_i8,tempb_i8
    real*8 rmn, rmx
    real*8,parameter:: smallr8 = 1.0e-10

    real*8   ::ekin,vel,trialVel
!    real*8, external :: MyRanf


    common/rpara2/ rmn(3), rmx(3)

    if(dslice(1) .gt. 0 .or. dslice(2) .gt. 0 .or. dslice(3) .gt. 0) then
       doslice=.true.
    else
       doslice=.false.
    end if


    t1=MPI_WTIME()   
    call MPI_Comm_size(MPI_COMM_WORLD,cpus,err)
    call MPI_Comm_rank(MPI_COMM_WORLD,rank,err)

    if (rank  .eq. 0 ) then
       write(*,*) "slice",doslice,dslice(1),dslice(2),dslice(3) 
    end if

    call MPI_File_open(MPI_COMM_WORLD,filename,&
         IOR(MPI_MODE_RDONLY,MPI_MODE_UNIQUE_OPEN),MPI_INFO_NULL,u,err)
    if (err /= 0) then
       call my_mpi_abort('Error opening input file.', INT(err))
    end if


    !root process read collectively the header (+ a part of the beginning of the file)
    if(rank==0) then
       call MPI_File_Read(u,header,SIZE(header,1)*4,MPI_BYTE,stat,err)
    end if
    !broadcast header data to others
    call MPI_Bcast(header,SIZE(header,1)*4,MPI_BYTE,0,MPI_COMM_WORLD,err)

    !FIXME, here reallocate header if too small (maxcpus fix)

    ! Figure out the input's byte order.
    tmp = header(1)
    if (tmp == protoReal) then
       swap_needed = .false.
    else if (swap32(TRANSFER(tmp, i4)) == TRANSFER(protoReal, i4)) then
       swap_needed = .true.
    else
       ! Either the current machine or the input is middle endian. Funny.
       call my_mpi_abort('Unsupported input byte order.', 0)
    end if

    fields = get_int32(header(12:12)) 
    fileatoms = get_int64(header(13:14)) 
    fileitypelow = get_int32(header(15:15))
    fileitypehigh = get_int32(header(16:16))
    filecpus = get_int32(header(17:17)) 
    filerealsize = get_int32(header(4:4))
    filebox(1) = ABS(get_real64(header(22:23)))
    filebox(2) = ABS(get_real64(header(24:25)))
    filebox(3) = ABS(get_real64(header(26:27)))



    if(filecpus .gt. maxCpus) then
       call my_mpi_abort('Too many cpus in input file, please increase maxCpus in binout.f90', INT(err))
    end if


    if(fileitypelow /=itypelow .or. fileitypehigh /=itypehigh ) then
       call my_mpi_abort('binaryread: types do not match', INT(err))
    end if

    hi = 28 ! Number of bytes in header so far, hi == header index

    !find fields storing the velocity and ekin
    vxi=-1
    vyi=-1
    vzi=-1
    ekini=-1
    if(fields>0) then   
       do i=1,fields
          if("V.x" == TRANSFER(header(hi),c4)) then
             vxi=i
          end if
          if("V.y" == TRANSFER(header(hi),c4)) then
             vyi=i
          end if
          if("V.z" == TRANSFER(header(hi),c4)) then
             vzi=i
          end if
          if("Ekin" == TRANSFER(header(hi),c4)) then
             ekini=i
          end if
          hi=hi+2
       end do
    end if


    if ( readvelocity==1 ) then
       if(vxi<0 .or. vyi<0 .or. vzi<0 ) then
          if( ekini<0 ) then
             call my_mpi_abort('binaryread: No velocities or kinetic energies in inputfile, cannot read or generate velocities', &
                  INT(1))
          else
             if(rank==0) then
                write(*,*) "WARNING: generating random velocities based on kinetic energies"
             end if
             generateVelocities=1
          end if
       else
          generateVelocities=0
       end if
    end if

    allocate(fileatomsperproc(0:filecpus-1))
    allocate(filermn(1:3, 1:filecpus))
    allocate(filermx(1:3, 1:filecpus))

    hi=28+2*fields+(fileitypehigh-fileitypelow+1)
    do i=0,filecpus-1
       fileatomsperproc(i) = get_int32(header(hi+i*13:hi+i*13))

       filermn(1, i+1) = get_real64(header(hi+i*13+1:hi+i*13+2))
       filermx(1, i+1) = get_real64(header(hi+i*13+3:hi+i*13+4))
       filermn(2, i+1) = get_real64(header(hi+i*13+5:hi+i*13+6))
       filermx(2, i+1) = get_real64(header(hi+i*13+7:hi+i*13+8))
       filermn(3, i+1) = get_real64(header(hi+i*13+9:hi+i*13+10))
       filermx(3, i+1) = get_real64(header(hi+i*13+11:hi+i*13+12))
    end do


    ! readstrategy = 0 read in relevant ares of the file (some extra atoms need to be discarded)
    ! readstrategy = 1 all READ in parallel (num cpus same as in file)
    if(cpus==filecpus) then
       readstrategy=1
    else
       readstrategy=0
    end if


    if(readstrategy==1) then
       !compute displacements and read in file for this process
       offset=get_int64(header(7:8))  ! Atom data offset
       if(filerealsize==4) then
          do i=1,rank
             offset=offset+(6_8+fields)*4_8*fileatomsperproc(i-1)
          end do
          call MPI_File_read_at_all(u,offset,buffer,4*(6+fields)*fileatomsperproc(rank),MPI_BYTE,MPI_STATUS_IGNORE,err)
       else if (filerealsize==8) then
          do i=1,rank
             offset=offset+(9_8+2*fields)*4_8*fileatomsperproc(i-1)
          end do
          call MPI_File_read_at_all(u,offset,buffer,4*(9+2*fields)*fileatomsperproc(rank),MPI_BYTE,MPI_STATUS_IGNORE,err)
       end if
       numReadAtoms=fileatomsperproc(rank)
    else
       ! Determine read areas.
       ! Read in the data.
       ! Filter the data.

       allocate(fileoffsets(1:filecpus))
       allocate(sizes(1:filecpus))
       allocate(goodi(1:filecpus))

       ! Calculate every part's size in bytes.
       if (filerealsize == 4) then
          sizes(1:filecpus) = 4 * (6 + fields) * fileatomsperproc(0:filecpus-1)
       else
          sizes(1:filecpus) = 4 * (9 + 2 * fields) * fileatomsperproc(0:filecpus-1)
       end if

       ! Calculate every part's offset in bytes.
       fileoffsets(1) = get_int64(header(7:8))
       do i = 2, filecpus
          fileoffsets(i) = sizes(i-1) + fileoffsets(i-1)
       end do

       if (doslice) then
          ! scale_factor is used because when slicing, rmn and filermn etc. are not synced with each other
          ! It used when determining which processors pieces to read in from file, when discarding individual
          ! atoms and finally it used to scale the coordinates
          scale_factor(:) = dslice(:) * 2.0 / filebox(:)
       end if

       ! Determine which input parts might contain particles for this process.
       nr_good = 0
       if (.not. doslice) then
          do i = 1, filecpus
             if ((filermx(1, i) >= rmn(1)-smallr8 .and. filermn(1, i) <= rmx(1)+smallr8 ) .and. &
                  (filermx(2, i) >= rmn(2)-smallr8  .and. filermn(2, i) <= rmx(2)+smallr8 )) then
                nr_good = nr_good + 1
                goodi(nr_good) = i
             end if
          end do
       else ! slicing
          if (ECM(1) == 0 .and. ECM(2) == 0) then
             where (rmn < -0.5)
                tmprmn = -0.5
             elsewhere
                tmprmn = rmn
             end where

             where (rmx > 0.5)
                tmprmx = 0.5
             elsewhere
                tmprmx = rmx
             end where

             do i = 1, filecpus
                if (filermx(1, i)  >= scale_factor(1) * tmprmn(1) .and. &
                     filermn(1, i) <= scale_factor(1) * tmprmx(1) .and. &
                     filermx(2, i) >= scale_factor(2) * tmprmn(2) .and. &
                     filermn(2, i) <= scale_factor(2) * tmprmx(2)) then
                   nr_good = nr_good + 1
                   goodi(nr_good) = i
                end if             
             end do
          else
             call my_mpi_abort('Input slicing not yet implemented for x- or y-shifted slicing.', 1)
          end if
       end if

       call mpi_type_create_hindexed(nr_good, sizes(goodi(1:nr_good)), &
            fileoffsets(goodi(1:nr_good)), mpi_byte, fileview, err)
       call mpi_type_commit(fileview,err)
       call mpi_file_set_view(u, 0, mpi_byte, fileview, 'native', mpi_info_null, err)
       call mpi_file_read_all(u, buffer, SUM(sizes(goodi(1:nr_good))), &
            mpi_byte, mpi_status_ignore, err)
       numReadAtoms=SUM(fileatomsperproc(goodi(1:nr_good)-1))
       call mpi_type_free(fileview,err)
       deallocate(fileoffsets)
       deallocate(sizes)
       deallocate(goodi)
    end if
    call MPI_File_close(u,err)

    !still need to fix delta(i), that is done later, elsewhere, in code 
    ! FIXME this is ugly, we should be able to fix everything here if at all possible
    do i = 1, fileitypehigh
       vscale(1, i) = 1.0d0/(vunit(i)* filebox(1))
       vscale(2, i) = 1.0d0/(vunit(i)* filebox(2))
       vscale(3, i) = 1.0d0/(vunit(i)* filebox(3))
       vscale(1:3,-i) = vscale(1:3, i)
    end do

    if( append /=1) then
       atoms=0
       totatoms=0
    end if

    hi=0 ! hi == buffer index, reuse header index var
    j = 1 + atoms
    do i = 1,numReadAtoms
       aindex(j) = get_int64(buffer(hi+1:hi+2))
       atype(j) = get_int32(buffer(hi+3:hi+3))

       if(filerealsize==4) then
          !scale coordinates to reduced units
          coord(j*3-2)=buffer(hi+4)/filebox(1)
          coord(j*3-1)=buffer(hi+5)/filebox(2)
          coord(j*3-0)=buffer(hi+6)/filebox(3)

          if(generateVelocities==1) then        
             ekin =  buffer(hi+6+ekini)
          else 
             !read in velocities
             velocity(j*3-2)= buffer(hi+6+vxi) 
             velocity(j*3-1)= buffer(hi+6+vyi) 
             velocity(j*3)= buffer(hi+6+vzi) 
          end if
          hi=hi+(6+fields)
       else if(filerealsize==8) then
          !scale coordinates to reduced units
          coord(j*3-2) = get_real64(buffer(hi+4:hi+5))/filebox(1)     
          coord(j*3-1) = get_real64(buffer(hi+6:hi+7))/filebox(2)     
          coord(j*3-0) = get_real64(buffer(hi+8:hi+9))/filebox(3)     

          if(generateVelocities==1) then
             ekin =  get_real64(buffer(hi+8+2*ekini:hi+9+2*ekini))
          else
             velocity(j*3-2) = get_real64(buffer(hi+8+2*vxi:hi+9+2*vxi)) 
             velocity(j*3-1) = get_real64(buffer(hi+8+2*vyi:hi+9+2*vyi)) 
             velocity(j*3) = get_real64(buffer(hi+8+2*vzi:hi+9+2*vzi))
          end if
          hi=hi+(9+2*fields)
       end if
       !generate velocities if they could not be read
       if(generateVelocities==1) then

          vel = SQRT(2.0*ekin/mass(atype(j)))*9.822694727d-2  ! Velocity of this kinetic energy in fs/A
          !generate velocity components giving absolute velocity vel using a trial and error method
          isDone=0
          do while (isDone .eq. 0 ) 
             velocity(j*3-2) = 2.0*MyRanf(0)-1.0d0
             velocity(j*3-1) = 2.0*MyRanf(0)-1.0d0
             velocity(j*3) = 2.0*MyRanf(0)-1.0d0
             trialVel=SQRT(velocity(j*3-2)*velocity(j*3-2)+ &
                  velocity(j*3-1)*velocity(j*3-1)+&
                  velocity(j*3)*velocity(j*3))
             if(trialVel .lt. 0.99 ) then
                isDone=1
                !scale velocities to the required velocity (to fs/A)
                velocity(j*3-2) = velocity(j*3-2)* vel / trialVel 
                velocity(j*3-1) = velocity(j*3-1)* vel / trialVel 
                velocity(j*3) = velocity(j*3)* vel / trialVel
             end if
          enddo
       end if

       velocity(j*3-2) = velocity(j*3-2)* vscale(1, atype(j))
       velocity(j*3-1) = velocity(j*3-1)* vscale(2, atype(j))
       velocity(j*3) = velocity(j*3)* vscale(3, atype(j))


       useatom=.false.
       if (readstrategy==1) then
          useatom=.true.
       else if(readstrategy==0) then
          if (.not. doslice) then
             ! Check whether the particle is within this process' box. Accept
             ! the particle only if it is.
             ! Also accept if it seems to be outside of system 
             ! (this should not happen though)
             if (coord(j*3-2) > rmn(1) .and. coord(j*3-2) <= rmx(1) .and. &
                  coord(j*3-1) > rmn(2) .and. coord(j*3-1) <= rmx(2)) then
                useatom=.true.
             else if(rmn(1)==-0.5 .and. coord(j*3-2)<=-0.5) then
                useatom=.true.
             else if(rmn(2)==-0.5 .and. coord(j*3-1)<=-0.5) then
                useatom=.true.
             else if(rmx(1)==0.5 .and. coord(j*3-2)>=0.5) then
                useatom=.true.
             else if(rmx(2)==0.5 .and. coord(j*3-1)>=0.5) then
                useatom=.true.
             end if
          else
             ! Reminder: scale_factor(:) == (dslice(:) * 2.0 / filebox(:))
             if (coord(j*3-2) > scale_factor(1)*rmn(1) .and. coord(j*3-2) <= scale_factor(1)*rmx(1) .and. &
                  coord(j*3-1) > scale_factor(2)*rmn(2) .and. coord(j*3-1) <= scale_factor(2)*rmx(2)) then
                useatom=.true.
             end if
             ! Accepted atoms seemingly inside the region. When using
             ! open boundaries, rmn is set to -999d30 for the
             ! processor in the edge This is not what we want
             ! here. For boundary processes, reject more atoms
             if(rmn(1) < -0.5 .and. coord(j*3-2)<=-0.5*scale_factor(1)) then
                useatom=.false.
             else if(rmn(2) < -0.5 .and. coord(j*3-1)<=-0.5*scale_factor(2)) then
                useatom=.false.
             else if(rmx(1) > 0.5 .and. coord(j*3-2)>=0.5*scale_factor(1)) then
                useatom=.false.
             else if(rmx(2) > 0.5 .and. coord(j*3-1)>=0.5*scale_factor(2)) then
                useatom=.false.
             end if
          end if
       end if

       !is inside processors box. Now check if it is inside slice
       if(useatom .and. doslice) then
          !     Use an atom region around ECM defined by dslice
          !     The atom selection criteria is as follows:
          !     For each dimension q=1,2,3
          !        - If dslice(q)<0 accept atom in this dimension
          !        - Else if ECM(q)-dslice(q) < x_q < ECM(q)+dslice(q) (with periodics)
          !          accept atom i in this dimension
          !        - If atom is accepted in each dimension, put it in buffer, send
          !          to processor one for printing
          dx=ABS(coord(j*3-2)*filebox(1)-ECM(1)); 
          dy=ABS(coord(j*3-1)*filebox(2)-ECM(2)); 
          dz=ABS(coord(j*3)*filebox(3)-ECM(3)); 

          if ((dslice(1)<0.0.or.dx<dslice(1)).and.     &
               (dslice(2)<0.0.or.dy<dslice(2)).and.     &
               (dslice(3)<0.0.or.dz<dslice(3))) then
             useatom=.true.

             ! Center the coordinates to new center of mass
             coord(j*3-2 : j*3-0) = coord(j*3-2 : j*3-0) - ECM(:) / filebox(:)
             ! scale so that coord \in [-0.5, 0.5]
             coord(j*3-2 : j*3-0) = coord(j*3-2 : j*3-0) / scale_factor(:)
          else
             useatom=.false.
          end if
       end if

       if (useatom) then
          j=j+1
       end if

    end do

    atoms = j - 1


    deallocate(fileatomsperproc)
    deallocate(filermn)
    deallocate(filermx)
    t5=MPI_WTIME()

    call MPI_Reduce(t5-t1,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,err)
    call MPI_Reduce(t5-t1,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,MPI_COMM_WORLD,err)
    avetime=avetime/cpus
    if(rank==0) then
       write(*,*) "IO-read tot time",avetime,maxtime
    end if



    !compute number of atoms read in
    tempa_i8=j-1
    call MPI_Allreduce(tempa_i8,tempb_i8,1,MPI_INTEGER8,MPI_SUM,MPI_COMM_WORLD,err)

    !totatoms now contains total number of atoms read in (and atoms read in earlier if appending)
    totatoms = totatoms + tempb_i8

    !cehck nmber of atoms read in
    if(fileatoms .ne. tempb_i8) then
       if(doslice) then
          if(rank==0) then
             write(*,*) "Slicing restart file: fileatoms,readatoms:",fileatoms,tempb_i8
          end if
       else
          !if no slicing, then all atoms in file should have been read in
          call binarysave(3,atoms,0,atype,aindex,0.0_8,0.0_8,filebox,coord,velocity,&
               velocity,velocity,velocity, & ! correspond to wxxi inputm, not used
               velocity,velocity,velocity,velocity, & !stand-ins
               .false.)

          call my_mpi_abort('Did not read in all atoms in file (new restart in out for debugging), fatal error',&
               INT(tempb_i8-fileatoms))
       end if
    end if

  end subroutine binaryread


  ! Save a frame of atoms.
  subroutine binarysave(fileid,atoms, frameid, atype, aindex, &
       time_fs, duration_fs, box, &
       coord, velocity, &
       wxxi, wyyi, wzzi, &
       Epair, Enonpair, Ekin, delta, &
       doslice,dslice,ECM)

    use typeparam


    use defs  
    implicit none

    integer,  intent(in) ::  fileid,atoms, frameid, atype(*), aindex(*)
    real*8,   intent(in) :: time_fs, duration_fs, box(*)
    real*8,   intent(in) :: coord(*), velocity(*)
    real*8,   intent(in) :: wxxi(*), wyyi(*), wzzi(*)
    real*8,   intent(in) :: Epair(*), Enonpair(*), Ekin(*), delta(*)
    logical, intent(in):: doslice
    real*8, intent(in),optional::  dslice(3),ECM(3)
    real*8        :: squared(1:3)
    real*8        :: vscale(1:3,-16:15)
    real*8       :: dx,dy,dz

    real*8        ::halfb(3)
    integer       :: i,fi,hi 
    integer       :: iostrategy
    double precision:: t1,t2,t3,t4,t5,t6,t7,t8,avetime,maxtime
    integer*8:: offset
    integer*8:: totatoms 
    integer(kind=mpi_parameters)::cpus,err
    integer:: u
    ! INTEGER(kind=mpi_parameters):: ioinfo
    integer:: pos,pos2,outatoms
    integer::fields
    character(len=22)::realfilename
    logical:: useatom
    integer(kind=mpi_parameters) :: ionprocs  !number of io processes
    integer(kind=mpi_parameters) :: ioprocess  !denotes if this is a process writing out data
    integer(kind=mpi_parameters) ::  iocommunicator !communicator that does the IO

    real*8 rmn, rmx
    common/rpara2/ rmn(3), rmx(3)

    t1=MPI_WTIME()

    do i=1,3
       halfb(i)=box(i)/2
       squared(i)=box(i)*box(i)
    end do
    fields=fds(fileid)%fields
    squared(1)=box(1)

    call MPI_Comm_size(MPI_COMM_WORLD,cpus,err)

    ! Velocity scale (for each axis, for each atom type).
    ! CHECKME: in original binout version the velocity was scaled by box**2, this does not seem to 
    ! CHECKME: make sense?
    if (fds(fileid)%saveVx > 0 .or. fds(fileid)%saveVy > 0 .or. fds(fileid)%saveVz > 0) then
       do i = 1, itypehigh
          vscale(1:3, i) = (vunit(i) / delta(i))  * box(1:3)
          vscale(1:3,-i) = vscale(1:3, i)
       end do
    end if


    t2=MPI_WTIME()
    !stuff the buffer with data and compute how many atoms we will write to disk
    !not the fastest way to do it, with all the if()s, but we would anyway need to 
    !check for each atoms if they are ok. This part should not be a bottleneck in the I/O
    pos=0
    outatoms=0
    do i = 1,atoms
       useatom=.true.
       if(doslice) then
          !     Use an atom region around ECM defined by dslice
          !     The atom selection criteria is as follows:
          !     For each dimension q=1,2,3
          !        - If dslice(q)<0 accept atom in this dimension
          !        - Else if ECM(q)-dslice(q) < x_q < ECM(q)+dslice(q) (with periodics)
          !          accept atom i in this dimension
          !        - If atom is accepted in each dimension, put it in buffer, send
          !          to processor one for printing
          dx=ABS(coord(i*3-2)*box(1)-ECM(1)); 
          dy=ABS(coord(i*3-1)*box(2)-ECM(2)); 
          dz=ABS(coord(i*3)*box(3)-ECM(3)); 

          if ((dslice(1)<0.0.or.dx<dslice(1)).and.     &
               (dslice(2)<0.0.or.dy<dslice(2)).and.     &
               (dslice(3)<0.0.or.dz<dslice(3))) then
             useatom=.true.
          else
             useatom=.false.
          end if
       end if

       !first write out aindex and atype
       if(useatom) then
          outatoms=outatoms+1
          !FIXME, when aindex is an integer*8 then we do not
          !FIXME, need i8 in the following
          i8=aindex(i)
          buffer(pos+1:pos+2)= TRANSFER(i8,r42)
          buffer(pos+3)= TRANSFER(atype(i),r4)
          !then scalar fields, two cases depending on realsize
          !4 byte data
          if(fds(fileid)%realsize==4) then
             buffer(pos+4)=coord(i*3-2)*box(1)
             buffer(pos+5)=coord(i*3-1)*box(2)
             buffer(pos+6)=coord(i*3)*box(3)

             if (fds(fileid)%saveEpot > 0) then
                buffer(pos+6+fds(fileid)%saveEpot) =  Epair(i) + Enonpair(i)
             end if
             if (fds(fileid)%saveEkin > 0) then
                buffer(pos+6+fds(fileid)%saveEkin) = Ekin(i)
             end if
             if (fds(fileid)%saveVx > 0) then
                buffer(pos+6+fds(fileid)%saveVx) = velocity(i*3-2) * vscale(1, atype(i))
             end if
             if (fds(fileid)%saveVy > 0) then
                buffer(pos+6+fds(fileid)%saveVy) = velocity(i*3-1) * vscale(2, atype(i))
             end if
             if (fds(fileid)%saveVz > 0) then
                buffer(pos+6+fds(fileid)%saveVz) = velocity(i*3) * vscale(3, atype(i))
             end if
             if (fds(fileid)%saveWxx > 0) then
                buffer(pos+6+fds(fileid)%saveWxx) = wxxi(i) * squared(1)
             end if
             if (fds(fileid)%saveWyy > 0) then
                buffer(pos+6+fds(fileid)%saveWyy) = wyyi(i) * squared(2)
             end if
             if (fds(fileid)%saveWzz > 0) then
                buffer(pos+6+fds(fileid)%saveWzz) = wzzi(i) * squared(3)
             end if
             pos=pos+(6+fields)
             if (pos +(6+fields) > SIZE(buffer,1) ) then
                !FIXME, fail more gracefully
                call my_mpi_abort('buffer in binout is too small, sorry!.', INT(err))
             end if
             !8 byte data
          else if( fds(fileid)%realsize==8) then
             buffer(pos+4:pos+5)=TRANSFER(coord(i*3-2)*box(1),r42)
             buffer(pos+6:pos+7)=TRANSFER(coord(i*3-1)*box(2),r42)
             buffer(pos+8:pos+9)=TRANSFER(coord(i*3)*box(3),r42)

             if (fds(fileid)%saveEpot > 0) then
                pos2=pos+8+2*fds(fileid)%saveEpot
                buffer(pos2:pos2+1) =  TRANSFER(Epair(i) + Enonpair(i),r42)
             end if
             if (fds(fileid)%saveEkin > 0) then
                pos2=pos+8+2*fds(fileid)%saveEkin
                buffer(pos2:pos2+1) = TRANSFER(Ekin(i),r42)
             end if
             if (fds(fileid)%saveVx > 0) then
                pos2=pos+8+2*fds(fileid)%saveVx
                buffer(pos2:pos2+1) = TRANSFER(velocity(i*3-2) * vscale(1, atype(i)),r42)
             end if
             if (fds(fileid)%saveVy > 0) then
                pos2=pos+8+2*fds(fileid)%saveVy
                buffer(pos2:pos2+1) = TRANSFER(velocity(i*3-1) * vscale(2, atype(i)),r42)
             end if
             if (fds(fileid)%saveVz > 0) then
                pos2=pos+8+2*fds(fileid)%saveVz
                buffer(pos2:pos2+1) = TRANSFER(velocity(i*3) * vscale(3, atype(i)),r42)
             end if
             if (fds(fileid)%saveWxx > 0) then
                pos2=pos+8+2*fds(fileid)%saveWxx
                buffer(pos2:pos2+1) = TRANSFER(wxxi(i) * squared(1),r42)
             end if
             if (fds(fileid)%saveWyy > 0) then
                pos2=pos+8+2*fds(fileid)%saveWyy
                buffer(pos2:pos2+1) = TRANSFER(wyyi(i) * squared(2),r42)
             end if
             if (fds(fileid)%saveWzz > 0) then
                pos2=pos+8+2*fds(fileid)%saveWzz
                buffer(pos2:pos2+1) = TRANSFER(wzzi(i) * squared(3),r42)
             end if
             pos=pos+(9+2*fields)
             if (pos +(9+2*fields) > SIZE(buffer,1) ) then
                !FIXME, fail more gracefully
                call my_mpi_abort('buffer in binout is too small, sorry!.', INT(err))
             end if
          end if
       end if
    end do

    t3=MPI_WTIME()
    !collect number of outatoms to rank=0
    call MPI_Gather(outatoms,1,MY_MPI_INTEGER,atomsperproc,1,MY_MPI_INTEGER,0,MPI_COMM_WORLD,err)

    call MPI_Gather(rmn, 3, mpi_double_precision, rmnperproc, &
         3, mpi_double_precision, 0, mpi_comm_world, err)
    call MPI_Gather(rmx, 3, mpi_double_precision, rmxperproc, &
         3, mpi_double_precision, 0, mpi_comm_world, err)



    if(rank==0) then   
       offsets=atomsperproc
       totatoms=SUM(offsets) !important to sum the 64bit offset array, might run out of 32bit integers
       !continue with offsets after size of header is clear
       !write header data

       header(1)= protoReal
       header(2)= TRANSFER(protoInt,r4) 
       header(3)= TRANSFER(fileversion,r4) !version for file
       header(4)= TRANSFER(fds(fileid)%realsize,r4) !4 for reals, 8 for doubles
       header(5:6) = 0  ! Description offset, in bytes
       header(7:8) = 0  ! Atom data offset, in bytes
       header(9)= TRANSFER(frameid,r4) !which frame in a series of frames
       header(10)= TRANSFER(0,r4) !part number, fixed to 0 as we write one file
       header(11) = TRANSFER(1,r4) !number of parts, fixed to 1 as we write one file
       header(12)= TRANSFER(fields,r4)
       header(13:14)= TRANSFER(totatoms,r42) 
       header(15)  = TRANSFER(itypelow,r4)
       header(16) = TRANSFER(itypehigh,r4)
       header(17) = TRANSFER(cpus,r4)   

       header(18:19)= TRANSFER(time_fs * 1.0d-15,r42)
       header(20:21)= TRANSFER(duration_fs * 1.0d-15,r42)
       header(22:23)= TRANSFER(ABS(box(1)),r42)
       header(24:25)= TRANSFER(ABS(box(2)),r42)
       header(26:27)= TRANSFER(ABS(box(3)),r42)
       if (periodic(1)) header(22:23)=TRANSFER(ABS(-box(1)),r42)
       if (periodic(2)) header(24:25)=TRANSFER(ABS(-box(2)),r42)
       if (periodic(3)) header(26:27)=TRANSFER(ABS(-box(3)),r42)

       hi=28

       !note that fieldnames should be exactly 4 characters
       if(fields>0) then
          do i=1,fields
             header(hi)   = TRANSFER(fds(fileid)%fieldName(i),r4)
             header(hi+1) = TRANSFER(fds(fileid)%fieldUnit(i),r4)
             hi=hi+2
          end do
       end if
       do i=itypelow,itypehigh
          header(hi)=TRANSFER(element(ABS(atype(i))),r4)
          hi=hi+1
       end do
       do i=1,cpus
          header(hi) = TRANSFER(atomsperproc(i),r4)
          header(hi+1:hi+2) = TRANSFER(rmnperproc(1, i), r42)  ! X min
          header(hi+3:hi+4) = TRANSFER(rmxperproc(1, i), r42)  ! X max
          header(hi+5:hi+6) = TRANSFER(rmnperproc(2, i), r42)  ! Y min
          header(hi+7:hi+8) = TRANSFER(rmxperproc(2, i), r42)  ! Y max
          header(hi+9:hi+10) = TRANSFER(rmnperproc(3, i), r42)  ! Z max
          header(hi+11:hi+12) = TRANSFER(rmxperproc(3, i), r42)  ! Z max
          hi=hi+13
       end do
       hi=hi-1 !correct hi to compute offsets and size of header to write

       ! The size of the header is known now. Set the description and atom
       ! data offsets. The offsets are the same since no description is
       ! written for now.
       header(7:8) = TRANSFER(INT(hi * 4_8, KIND=int64b), r42)
       header(5:6) = header(7:8)  ! There is no description.

       ! Compute displacements in the file for all processes. Process 0
       ! writes straight after the header, process 1 writes straight
       ! after process 0's data and so on. All the offsets must be in
       ! 64-bit integers.
       offsets(1)=hi*4_8 !header size
       if(fds(fileid)%realsize==4) then
          do i=2,cpus
             offsets(i)=(6_8+fields)*4_8*atomsperproc(i-1)+offsets(i-1)
          end do
       else if(fds(fileid)%realsize==8) then  
          do i=2,cpus
             offsets(i)=(9_8+2_8*fields)*4_8*atomsperproc(i-1)+offsets(i-1)
          end do
       end if
    end if

    !FIXME, in principle only processes in iocommunicator need this piece of information
    call MPI_Scatter(offsets,1,MPI_INTEGER8,offset,1,MPI_INTEGER8,0,MPI_COMM_WORLD,err)   

    t4=MPI_WTIME()      


    !create io communicator
    if (rank==0 .or. outatoms > 0) then
       ioprocess=1
    else
       !do not create communicator for other processes
       ioprocess=MPI_UNDEFINED
    end if

    call MPI_COMM_SPLIT(MPI_COMM_WORLD,ioprocess,rank,iocommunicator,err)


    t5=MPI_WTIME()
    !only processes doing IO are involved here
    if(ioprocess==1)then
       call MPI_Comm_size(iocommunicator,ionprocs,err)

       ! Create file name for this frame.
       if (fds(fileid)%overwrite> 0) then
          realfilename=fds(fileid)%filename
       else
          !FIXME, name is cut off
          write (realfilename, '(A10,I7.7)') fds(fileid)%filename, fds(fileid)%frame
       end if

       if(rank==0) then
          call MPI_File_delete(realfilename,MPI_INFO_NULL,err)
          !Only root creates the file, this reduces the number of metadataoperations at scale
          open(file=realfilename,unit=u)
          close(u)
       end if

       !wait for file to be created
       call MPI_Barrier(iocommunicator,err)
       !open file
       call MPI_File_open(iocommunicator,realfilename,&
            IOR(MPI_MODE_WRONLY,MPI_MODE_UNIQUE_OPEN),&
            MPI_INFO_NULL,u,err)
       if (err /= 0) then
          call my_mpi_abort('Error writing binary output file.', INT(err))
       end if
       t6=MPI_WTIME()

       if(rank==0) then
          call MPI_File_write_at(u,0_8,header,4*hi,MPI_BYTE,MPI_STATUS_IGNORE,err)
       end if

       if(fds(fileid)%realsize==4) then
          call MPI_File_write_at_all(u,offset,buffer,4*(6+fields)*outatoms,MPI_BYTE,MPI_STATUS_IGNORE,err)
       else   if(fds(fileid)%realsize==8) then
          call MPI_File_write_at_all(u,offset,buffer,4*(9+2*fields)*outatoms,MPI_BYTE,MPI_STATUS_IGNORE,err)
       end if

       if (err /= 0) then
          call my_mpi_abort('Error writing binary output file data with collective MPI-IO.', INT(err))
       end if
       t7=MPI_WTIME()
       call MPI_File_close(u,err)

       t8=MPI_WTIME()



       call MPI_Reduce(t8-t1,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,iocommunicator,err)
       call MPI_Reduce(t8-t1,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,iocommunicator,err)
       avetime=avetime/cpus
       if(rank==0) then
          write(*,'(I3," ",A,2E11.4,I7)') fileid,"IO tot time ",avetime,maxtime,ionprocs
       end if

       call MPI_Reduce(t6-t5,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,iocommunicator,err)
       call MPI_Reduce(t6-t5,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,iocommunicator,err)
       avetime=avetime/cpus
       if(rank==0) then
          write(*,'(I3," ",A,2E11.4,I7)') fileid,"IO fopen time",avetime,maxtime,ionprocs
       end if

       call MPI_Reduce(t7-t6,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,iocommunicator,err)
       call MPI_Reduce(t7-t6,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,iocommunicator,err)
       avetime=avetime/cpus
       if(rank==0) then
          write(*,'(I3," ",A,2E11.4,I7)') fileid,"IO write time",avetime,maxtime,ionprocs
       end if

       call MPI_Reduce(t8-t7,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,iocommunicator,err)
       call MPI_Reduce(t8-t7,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,iocommunicator,err)
       avetime=avetime/cpus
       if(rank==0) then
          write(*,'(I3," ",A,2E11.4,I7)') fileid,"IO fclose time",avetime,maxtime,ionprocs
       end if

       call MPI_Comm_free(iocommunicator,err)
    end if
    ! Frame saved successfully.
    fds(fileid)%frame = fds(fileid)%frame + 1
    !barrier not needed but lets keep it here to avoid any nasty bugs or apparent load imbalance elsewhere
    call MPI_Barrier(MPI_COMM_WORLD,err)
    return

  end subroutine binarysave







end module binout
