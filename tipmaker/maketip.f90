module crystal
!real, parameter :: cubic_unit_cell(3,3) = transpose(reshape([ &
real, parameter :: cubic_unit_cell(3,3) = reshape([ &
        1.0, 0.0, 0.0, &
        0.0, 1.0, 0.0, &
        0.0, 0.0, 1.0], [3,3])

!real, parameter :: fcc100_basis(3,4) = transpose(reshape([ &
!        0.0, 0.0, 0.5, 0.5, &
!        0.0, 0.5, 0.0, 0.5, &
!        0.0, 0.5, 0.5, 0.0], [4,3]))
real, parameter :: fcc100_basis(3,4) = reshape([ &
0.0, 0.0, 0.0, &
0.0, 0.5, 0.5, &
0.5, 0.0, 0.5, &
0.5, 0.5, 0.0], [3,4])

!real, parameter :: fcc110_basis(3,2) = transpose(reshape([ &
!        0.0, 0.5, &
!        0.0, 0.5, &
!        0.0, 0.5], [2,3]))
real, parameter :: fcc110_basis(3,2) = reshape([ &
0.0,0.0,0.0, &
0.5,0.5,0.5],[3,2])

!real, parameter :: fcc100_size(3,3) = transpose(reshape([ &
real, parameter :: fcc100_size(3,3) = reshape([ &
        1.0, 0.0, 0.0, &
        0.0, 1.0, 0.0, &
        0.0, 0.0, 1.0], [3,3])

!real, parameter :: fcc110_size(3,3) = transpose(reshape([ &
!        1.0, 0.0,       0.0, &
!        0.0, 1.0/sqrt(2.0), 0.0, &
!        0.0, 0.0,       1.0/sqrt(2.0)], [3,3]))
real, parameter :: fcc110_size(3,3) = reshape([ &
1.0,0.0,0.0, &
0.0,1.0/sqrt(2.0), 0.0, &
0.0, 0.0, 1.0/sqrt(2.0)], [3,3])


contains
subroutine make_lattice(unit_vectors, lattice_size, lattice, basis)
implicit none
real, intent(in) :: unit_vectors(3,3)
integer, intent(in) :: lattice_size(3)
real, allocatable, intent(out) :: lattice(:,:)
real, optional, intent(in) :: basis(:,:)

integer :: atoms_per_unit_cell
integer :: i, j, k, natom

if(present(basis)) then
        atoms_per_unit_cell = size(basis,2)
else
        atoms_per_unit_cell = 1
end if

allocate(lattice(3, product(lattice_size)*atoms_per_unit_cell))
natom = 1
do k = 0, lattice_size(3)-1; do j = 0, lattice_size(2)-1; do i = 0, lattice_size(1)-1
        if(present(basis)) then
                lattice(:,natom:natom + atoms_per_unit_cell - 1) = ( spread(matmul(unit_vectors, &
                        [i,j,k]), 2, atoms_per_unit_cell) + basis )
        else
                lattice(:,natom) = (matmul(unit_vectors, [i,j,k]))
        end if
        natom = natom + atoms_per_unit_cell
end do; end do; end do
end subroutine

subroutine make_FCC(lattice_size, a, lattice, orientation, center)
implicit none
integer, intent(in) :: lattice_size(3)
real, intent(in) :: a(3,3)
real, allocatable, intent(out) :: lattice(:,:)
integer, optional :: orientation
logical,optional :: center
integer :: orientation2

if (present(orientation)) then
        orientation2 = orientation
else
        orientation = 100
end if

select case (orientation2)
case (100)
        call make_lattice(a, lattice_size, lattice, matmul(transpose(a),fcc100_basis))
case (110)
        call make_lattice(a, lattice_size, lattice, matmul(transpose(a),fcc110_basis))
end select

! Center
if(present(center) .and. center) then
lattice(1,:) = lattice(1,:) + a(1,1)/2.0*(-lattice_size(1)) !+1.0/2.0)
lattice(2,:) = lattice(2,:) + a(2,2)/2.0*(-lattice_size(2)) !+1.0/2.0)
lattice(3,:) = lattice(3,:) + a(3,3)/2.0*(-lattice_size(3)) !+1.0/2.0)
end if
end subroutine
end module

program maketip
use crystal
use getopt_m
implicit none
integer :: nx, ny, nz, orientation
real :: h, depth, r, sa, T, a(3,3), box(3)
real :: max_width, wx,wy, baseh, tansa, conetop, realdepth, rsq, x, y, z, rnd
integer :: nvalid, n, nbaseatoms, natsdone, natoms0,i,clock
logical :: valid, dir_exists
real, allocatable :: baseatoms(:,:)
integer, allocatable :: seed(:)
integer :: iabove

logical :: square

character(len=128) :: buf
character(len=2) :: atype
character(len=64) :: dirname
real :: mass, ionpot, wrkfn

integer, parameter :: outfile = 23, mdinfile = 24

type(option_s) :: opts(2)
opts(1) = option_s("square",.false.,'s')
opts(2) = option_s("type",.true.,'t')
square = .false.
atype = "Cu"
do
        select case(getopt("st:",opts))
                case(char(0))
                        exit
                case('s')
                        square = .true.
                case('t')
                        atype = optarg(1:2)
        end select
end do

call random_seed(size = n)
allocate(seed(n))
call system_clock(count=clock)
i = 0 ! Avoid compiler warning
seed = clock + 37 * [(i - 1, i = 1, n)]
call random_seed(put = seed)
deallocate(seed)

if (command_argument_count() - (optind - 1) < 8) then
  print *, "Usage: ./maketip [--element element_name (Cu,Al,Fe,Ta)] [--square] orientation(100 or 110) unitcellsx unitcellsy ", &
    "unitcellsz apex_radius_Å depth_under_tip_Å shank_angle_deg temperature_K"
  stop
end if

call get_command_argument(optind+0,buf); read(buf,*) orientation
call get_command_argument(optind+1,buf); read(buf,*) nx
call get_command_argument(optind+2,buf); read(buf,*) ny
!call get_command_argument(optind+3,buf); read(buf,*) h
call get_command_argument(optind+3,buf); read(buf,*) nz
call get_command_argument(optind+4,buf); read(buf,*) r
call get_command_argument(optind+5,buf); read(buf,*) depth
call get_command_argument(optind+6,buf); read(buf,*) sa
call get_command_argument(optind+7,buf); read(buf,*) T

if(.not.(square)) then
write(dirname,'("o",I0,"x",I0,"y",I0,"z",I0,"r",I0,"d",I0,"sa",I0,"T",I0)') &
  orientation, nx, ny, nz, nint(r), nint(depth), nint(sa), nint(T)
else
write(dirname,'("o",I0,"x",I0,"y",I0,"z",I0,"r",I0,"d",I0,"sa",I0,"T",I0,"s")') &
  orientation, nx, ny, nz, nint(r), nint(depth), nint(sa), nint(T)
end if

sa = sa * 3.1415926535/180.0 ! Convert degrees to radians

inquire(file=dirname,exist=dir_exists)
if (dir_exists) then
        print *, "Directory ",trim(dirname)," already exists. Remove it first."
        stop
end if

call system('cp -r dir.template'//' '//trim(dirname))

! For Cu!!!
a = 0
select case (atype)
case ("Cu")
        a(1,1) = 3.61500 + 0.66430757E-04*T + 0.62101176E-08*T**2.0 + 0.58180793E-11*T**3.0
        mass = 63.546
        ionpot = 7.73
        wrkfn = 4.6
case ("Fe")
        a(1,1) = 2.807
        mass = 55.847
        ionpot = 7.90
        wrkfn = 4.4
case ("Al")
        a(1,1) = 4.05
        mass = 26.9815
        ionpot = 5.986
        wrkfn = 4.28
case ("Ta")
        a(1,1) = 4.05
        mass = 180.94788
        ionpot = 7.89
        wrkfn = 4.25
case default
        stop "Unknown atom type"
end select
a(2,2) = a(1,1)
a(3,3) = a(1,1)

!if(orientation == 100) then
!        if(mod(nx,2) /= 0) then
!                print *, "WARNING: nx should be a multiple of 2"
!        end if
!        if(mod(ny,2) /= 0) then
!                print *, "WARNING: ny should be a multiple of 2"
!        end if
!end if
!if(orientation == 110) then
!        print *, "Using 110 orientation"
!        a = matmul(a,fcc110_size)
!        if(mod(nx,2) /= 0) then
!                print *, "WARNING: nx should be a multiple of 2"
!        end if
!        if(mod(nint(ny*sqrt(2.0)),2) /= 0) then
!                print *, "WARNING: ny*sqrt(2) should be a multiple of 2"
!        end if
!end if

print *, 'Using lattice constant ', a(1,1),"T =",T

h = (nz*a(3,3)-depth)

max_width = 2*(r + (h-r)*tan(sa))

wx = nx*a(1,1)
wy = ny*a(2,2)

if (2*max_width > wx .or. 2*max_width > wy) then
        print *, "WARNING: Tip base will not fit on the surface"
end if

if (2*max_width > wx/2.0 .or. 2*max_width > wy/2.0) then
        print *, "WARNING: You probably want to make the surface a bit bigger"
end if

if(depth > 0) then
        realdepth = depth !ceiling(depth/a(3,3)+1)*a(3,3)
else
        realdepth = 0
end if

! Calculate simulation box size
!nz = ceiling((depth + h)/a(3,3))
box = [nx*a(1,1), ny*a(2,2), nz*a(3,3)]

call make_FCC([nx,ny,nz],a,baseatoms,orientation,.true.)
nbaseatoms = size(baseatoms(1,:))

baseh = minval(baseatoms(3,:))+realdepth
conetop = maxval(baseatoms(3,:))-r
rsq = r**2
tansa = tan(sa)
nvalid = 0
! Count valid atoms
do n=1, nbaseatoms
        x = baseatoms(1,n)
        y = baseatoms(2,n)
        z = baseatoms(3,n)
        valid = .false.

        if (z >= conetop) then ! Spherical cap
                if (x**2+y**2+(z-conetop)**2 <= rsq*1.01) valid = .true.
        elseif (z>=baseh) then
                if (x**2+y**2 <= (r+tansa*(conetop-z))**2*1.01) valid = .true.
        else
                valid = .true.
        end if

        if (valid) then
                nvalid = nvalid + 1
        end if
end do

open(unit=outfile,file=trim(dirname)//'/in/mdlat.in.xyz')
write (outfile,*) nvalid
write (outfile,*) nx,ny,h,depth,r,sa*180.0/3.14159,T,'boxsize',box


natoms0=nvalid
! Loop over atoms again and write out valid ones
nvalid = 0
natsdone = 0
do n=1, nbaseatoms
        x = baseatoms(1,n)
        y = baseatoms(2,n)
        z = baseatoms(3,n)
        natsdone = natsdone + 1
        if (mod(natsdone,10000) == 0) print '(I0," ",I0," ",F0.1,"%")', natsdone,natoms0,real(natsdone)/real(natoms0)*100
        valid = .false.

        if (z >= conetop) then ! Spherical cap
                if (x**2+y**2+(z-conetop)**2 <= rsq*1.01) valid = .true.
        elseif (z>=baseh) then
                if (x**2+y**2 <= (r+tansa*(conetop-z))**2*1.01) valid = .true.
        else
                valid = .true.
        end if

        if (valid) then
                nvalid = nvalid + 1
                !if (baseatoms(1,n) > 1000) print *, "Something is wrong",baseatoms(:,n)
                ! Output tip with a little offset to match grid
                write (outfile,'(A, " ", F0.5, " ",F0.5," ",F0.5," ",I0)') trim(atype), &
                  baseatoms(1,n)+a(1,1)/4, baseatoms(2,n)+a(2,2)/4,baseatoms(3,n)+a(3,3)/4, 1
        end if
end do

close(outfile)

iabove = 2**ceiling(log2(dble(nz*2*2/1))/log2(2d0))
if(orientation == 110) then
! iabove should be power of two also after change in grid size

end if
call random_number(rnd)

open(unit=mdinfile,file=trim(dirname)//'/in/md.in',access='APPEND')

write (mdinfile,'("# Generated FEMOCS parameters")') 
write (mdinfile,'("#--------------------")')
write (mdinfile,'("t_ambient = ",F6.1,"  # temperature at the bottom of simulation cell [K]")') T
write (mdinfile,'("latconst  = ",F6.3,"  # lattice constant [A]")') a(1,1)
write (mdinfile,'("radius    = ",I4,  "    # inner radius of coarsening cylinder [A]")') nint(r)+int(a(1,1))

write (mdinfile,*) ""
write (mdinfile,'("# Generated PARCAS parameters")')
write (mdinfile,'("#--------------------")')
write (mdinfile,'("temp     = ",F6.1)') T
write (mdinfile,'("initemp  = ",F6.1)') T
write (mdinfile,'("mctemp   = ",F6.1)') 55.0
write (mdinfile,'("box(1)   = ",F8.3)') box(1)
write (mdinfile,'("box(2)   = ",F8.3)') box(2)
write (mdinfile,'("box(3)   = ",F8.3)') box(3)
write (mdinfile,'("ncell(1) = ",I4)') nx
write (mdinfile,'("ncell(2) = ",I4)') ny
write (mdinfile,'("ncell(3) = ",I4)') nz
write (mdinfile,'("fixzmin  = ",F8.3)') -box(3)/2.0-4
write (mdinfile,'("fixzmax  = ",F8.3)') -box(3)/2.0+4
write (mdinfile,'("seed     = ",I5)') nint(rnd*99999)
write (mdinfile,'("iabove   = ",I4)') iabove
write (mdinfile,'("natoms   = ",I8)') nvalid
write (mdinfile,'("mass(1)  = ",F8.4)') mass
write (mdinfile,'("name(1)  = ",A)') atype
write (mdinfile,'("ionpot(1)= ",F8.4)') ionpot
write (mdinfile,'("#==========================================")')
close(mdinfile)

contains
double precision elemental function log2(x)
double precision, intent(in) :: x
log2 = log(x)/log(2d0)
end function
end program maketip
